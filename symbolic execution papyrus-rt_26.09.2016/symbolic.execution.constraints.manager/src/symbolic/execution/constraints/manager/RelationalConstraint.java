package symbolic.execution.constraints.manager;

import java.util.Set;

import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;


/**
 * Basic relational operators: EQUALS, NOT EQUALS, LESS, GREATER, NOT LESS, NOT GREATER.
 * @author zurowska
 *
 */
public class RelationalConstraint extends Constraint{
	private Expression left;
	private CompareOperators op;
	private Expression right;
	
	public RelationalConstraint(Expression left, CompareOperators op, Expression right){
		this.right = right;
		this.op = op;
		this.left = left;
	}
	
	public Expression getLeft() {
		return left;
	}
	public CompareOperators getOp() {
		return op;
	}
	public Expression getRight() {
		return right;
	}
	
	
	@Override
	public String print() {		
		return "(" + left.print() + " " +op.toString() + " " + right.print() + ")";
	}

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression newLeft = left.replaceVariables(valuation);
		Expression newRight = right.replaceVariables(valuation);
				
		
		Constraint cst = evaluateConstraint(newLeft, newRight);
		
		return cst;
	}
	
	private Constraint evaluateConstraint(Expression newLeft, Expression newRight) {
		if (newLeft instanceof Constant && newRight instanceof Constant){
			Constant leftConst = (Constant) newLeft;
			Constant rightConst = (Constant) newRight;
			
			if (leftConst.getType().equals(Integer.class) && rightConst.getType().equals(Integer.class)){
				int valueLeft = (Integer) leftConst.getValue();
				int valueRight = (Integer) rightConst.getValue();				
				
				boolean result = false;
				switch (op) {
				case eq : result = (valueLeft==valueRight); break;
				case neq : result = (valueLeft!=valueRight); break;
				case ge : result = (valueLeft > valueRight); break;
				case geq : result = (valueLeft >= valueRight); break;
				case le : result = (valueLeft < valueRight); break;
				case leq : result = (valueLeft <= valueRight); break;				
				};
				
				if (result){
					return SymbolicFactory.createTrue();					
				} else {
					return SymbolicFactory.createFalse();
				}
			} else if (leftConst.getType().equals(Long.class) && rightConst.getType().equals(Long.class)){
				Long valueLeft = (Long) leftConst.getValue();
				Long valueRight = (Long) rightConst.getValue();
				
				boolean result = false;
				switch (op) {
				case eq : result = (valueLeft==valueRight); break;
				case neq : result = (valueLeft!=valueRight); break;
				case ge : result = (valueLeft > valueRight); break;
				case geq : result = (valueLeft >= valueRight); break;
				case le : result = (valueLeft < valueRight); break;
				case leq : result = (valueLeft <= valueRight); break;				
				};
				
				if (result){
					return SymbolicFactory.createTrue();					
				} else {
					return SymbolicFactory.createFalse();
				}
			} else 	if (leftConst.getType().equals(Float.class) && rightConst.getType().equals(Float.class)){
				Float valueLeft = (Float) leftConst.getValue();
				Float valueRight = (Float) rightConst.getValue();
				
				boolean result = false;
				switch (op) {
				case eq : result = (valueLeft==valueRight); break;
				case neq : result = (valueLeft!=valueRight); break;
				case ge : result = (valueLeft > valueRight); break;
				case geq : result = (valueLeft >= valueRight); break;
				case le : result = (valueLeft < valueRight); break;
				case leq : result = (valueLeft <= valueRight); break;				
				};
				
				if (result){
					return SymbolicFactory.createTrue();					
				} else {
					return SymbolicFactory.createFalse();
				}
			} if (leftConst.getType().equals(Double.class) && rightConst.getType().equals(Double.class)){
				Integer valueLeft = (Integer) leftConst.getValue();
				Integer valueRight = (Integer) rightConst.getValue();
				
				boolean result = false;
				switch (op) {
				case eq : result = (valueLeft==valueRight); break;
				case neq : result = (valueLeft!=valueRight); break;
				case ge : result = (valueLeft > valueRight); break;
				case geq : result = (valueLeft >= valueRight); break;
				case le : result = (valueLeft < valueRight); break;
				case leq : result = (valueLeft <= valueRight); break;				
				};
				
				if (result){
					return SymbolicFactory.createTrue();					
				} else {
					return SymbolicFactory.createFalse();
				}
			}
			
		}
		return SymbolicFactory.createRelationalConstraint(newLeft,op,newRight);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this){
			return true;
		}
		if (o instanceof RelationalConstraint){
			RelationalConstraint cst = (RelationalConstraint) o;
			
			return cst.op.equals(op) &&
				cst.left.equals(left) &&
				cst.right.equals(right);
		}
		return super.equals(o);
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {
		Set<SymbolicVariable> rightVars = right.collectVariables();
		if (left == null){
			return rightVars;
		}
		Set<SymbolicVariable> leftVars = left.collectVariables();
		leftVars.addAll(rightVars);
		return leftVars;
	}
}
