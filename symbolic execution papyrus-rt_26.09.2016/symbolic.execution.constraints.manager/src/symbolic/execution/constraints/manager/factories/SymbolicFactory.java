package symbolic.execution.constraints.manager.factories;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

public class SymbolicFactory {
	
	private static Engine engine;
	
	public static void setEngine(Engine engineSpecific){
		engine = engineSpecific;
	}
	
	public static SymbolicVariable createSymbolicVariable(String name){
		return engine.createVariable(name);
	}
	
	public static SymbolicVariable createSymbolicVariable(String name, Class type){
		return engine.createVariable(name, type);
	}
	
	public static BinaryExpression createBinaryExpression
			(Expression left, ArithmeticOperators op, Expression right){		
		return engine.createBinaryExpression(left, op, right);
	}
	
	public static UnaryExpression createUnaryExpression(ArithmeticOperators op, Expression expr){
		return engine.createUnaryExpression(op,expr);
	}

	public static LogicalConstraint createLogicalConstraint
			(Expression left, LogicalOperators op, Expression right){
		return engine.createLogicalConstraint(left, op, right);
	}
	
	public static RelationalConstraint createRelationalConstraint
			(Expression left, CompareOperators op, Expression right){
		return engine.createRelationalConstraint(left,op,right);
	}
	
	public static Constant createConstant(Class type, Object value){
		return engine.createConstant(type, value);
	}
	
	public static Solver createSolver(){
		return engine.createSolver();
	}
	
	public static True createTrue(){
		return engine.createTrue();
	}
	
	public static False createFalse(){
		return engine.createFalse();
	}
}
