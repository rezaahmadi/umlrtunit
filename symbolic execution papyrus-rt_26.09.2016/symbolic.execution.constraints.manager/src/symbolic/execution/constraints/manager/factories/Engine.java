package symbolic.execution.constraints.manager.factories;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

/**
 * An abstract class that must be implemented by the constraint solving library.
 * @author zurowska
 *
 */
public abstract class Engine {

	public abstract SymbolicVariable createVariable(String name);

	public abstract SymbolicVariable createVariable(String name, Class type);

	
	public abstract BinaryExpression createBinaryExpression(Expression left, ArithmeticOperators op, Expression right);

		
	public abstract RelationalConstraint createRelationalConstraint(Expression left,
			CompareOperators op, Expression right);
	public abstract LogicalConstraint createLogicalConstraint(Expression left, LogicalOperators op, Expression right);


	public abstract Constant createConstant(Class type, Object value);

	public abstract Solver createSolver();

	public abstract UnaryExpression createUnaryExpression(ArithmeticOperators op,
			Expression expr);

	public abstract True createTrue();

	public abstract False createFalse();

}
