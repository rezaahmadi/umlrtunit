package symbolic.execution.constraints.manager;

//import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

/**
 * Basic logical constraints : AND, OR, IMPLY, XOR
 * @author zurowska
 *
 */
public class LogicalConstraint extends Constraint{
	private Constraint left;
	private LogicalOperators op;
	private Constraint right;
	
	
	public LogicalConstraint(Constraint left, LogicalOperators op, Constraint right){
		this.left = left;
		this.op = op;
		this.right = right;
	}
	
	public Constraint getLeft() {
		return left;
	}
	public LogicalOperators getOp() {
		return op;
	}
	public Constraint getRight() {
		return right;
	}
	
	
	@Override
	public String print() {		
		return "(" + ((left==null) ? "" : left.print()) + " " + op.toString()+ " " + right.print() +")";
	}

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression newLeft = left;
		if (left != null){
			newLeft = left.replaceVariables(valuation);
		}
		Expression newRight = right.replaceVariables(valuation);
		
		return evaluateConstraint(newLeft, newRight);
		
	}
	
	private Expression evaluateConstraint(Expression newLeft,
			Expression newRight) {
		if (newRight instanceof True || newRight instanceof False){
			boolean rightValue = (newRight instanceof True);
			if (newLeft==null){
				
				if (op.equals(LogicalOperators.not)){
					if(rightValue)
						return SymbolicFactory.createFalse();
					else
						return SymbolicFactory.createTrue();
				} else {
					return SymbolicFactory.createLogicalConstraint(newLeft, op, newRight);
				}
				
			}else{
				if (newLeft instanceof True || newLeft instanceof False){
					boolean leftValue = newLeft instanceof True;
					
					boolean result = false;
					
					switch (op){
					case and : result = (rightValue && leftValue); break;
					case imply : result = (!rightValue || leftValue); break;
					case or : result = (rightValue || leftValue); break;
					case xor : result = (rightValue && !leftValue) || (!rightValue && leftValue);break;				
					};
					
					if (result){
						return SymbolicFactory.createTrue();					
					} else {
						return SymbolicFactory.createFalse();
					}
				} else {
					return SymbolicFactory.createLogicalConstraint(newLeft, op, newRight);
				}
				
				
			}
			
		}
		
		return SymbolicFactory.createLogicalConstraint(newLeft, op, newRight);
	}

	@Override
	public boolean equals(Object o) {
		if (o==this){
			return true;
		}
		if (o instanceof LogicalConstraint){
			LogicalConstraint cst = (LogicalConstraint) o;
			
			return cst.op.equals(op) &&
				(left==null ? cst.left==null : cst.left.equals(left)) &&
				(cst.right.equals(right));
		}
		return super.equals(o);
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {				
		Set<SymbolicVariable> rightVars = right.collectVariables();
		if (left == null){
			return rightVars;
		}
		Set<SymbolicVariable> leftVars = left.collectVariables();
		leftVars.addAll(rightVars);
		return leftVars;
	}
	
}
