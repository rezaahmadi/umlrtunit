package symbolic.execution.constraints.manager;

import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.utils.IPrinter;


/**
 * Class that represents all expressions and constraints stored and sent to the solver.
 * 
 * @author zurowska
 *
 */

public abstract class Expression implements IPrinter {
	
	
	/**
	 * Replaces variables based on the mapping
	 * @param mapping
	 * @return
	 */
	public Expression replaceVariables(Map<SymbolicVariable, ? extends Expression> mapping){
		if (mapping == null || mapping.size() == 0){
			return this;
		}
		SymbolicValuation valuation = new SymbolicValuation(mapping);		
		return replaceVariables(valuation);
	}
	
	/**
	 * Replaces variables based on the valuation
	 * @param valuation
	 * @return
	 */
	public abstract Expression replaceVariables(SymbolicValuation valuation);
	
	
	/**
	 * Collects variables in this expression
	 * @return
	 */
	public abstract Set<SymbolicVariable> collectVariables();
	
	@Override
	public String toString() {		
		return print();
	}
	
	

}
