package symbolic.execution.constraints.manager;

import java.util.HashSet;
import java.util.Set;

public class True extends Constraint{

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {		
		return this;
	}

	@Override
	public String print() {
		return "TRUE";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof True){
			return true;
		}
		return false;
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {		
		return new HashSet<SymbolicVariable>();
	}
	
	

}
