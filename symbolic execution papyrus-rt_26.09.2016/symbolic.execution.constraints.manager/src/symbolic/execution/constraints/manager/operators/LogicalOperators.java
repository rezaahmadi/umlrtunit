package symbolic.execution.constraints.manager.operators;

public enum LogicalOperators {
	and ("&"), or ("|"), imply("->"), xor("^"), not("!");
	
	private final String symbol;
	
	private LogicalOperators(String symbol){
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {		
		return symbol;
	}
}
