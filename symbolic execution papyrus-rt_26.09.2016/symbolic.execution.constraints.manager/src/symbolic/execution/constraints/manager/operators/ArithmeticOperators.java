package symbolic.execution.constraints.manager.operators;

public enum ArithmeticOperators {
	plus("+"), 
	minus("-"), 
	div("/"), 
	mod("%"), 
	mult("*");
	
	private final String symbol;
	
	ArithmeticOperators(String symbol){
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {		
		return symbol;
	}
}
