package symbolic.execution.constraints.manager.operators;

public enum CompareOperators {
	eq("=="), neq("!="), leq("<="), le("<"), ge(">"), geq(">=");
	
	private final String symbol;
	
	private CompareOperators(String symbol){
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {		
		return symbol;
	}
}
