package symbolic.execution.constraints.manager;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Solver enables checking solvability of sets of constraints.
 * 
 * @author zurowska
 *
 */
public abstract class Solver {
	
	public abstract boolean isSolvable(List<Constraint> constraints);

	public abstract boolean isSolvable(List<Constraint> constraints, Constraint condition, boolean isTrue);

	public abstract boolean isSolvable(List<Constraint> constraints, Set<Constraint> constraintsCheck, boolean checkTrue);

	public abstract Map<SymbolicVariable, Constant> solve(List<Constraint> constraints,
			List<SymbolicVariable> variables);		

}
