package symbolic.execution.constraints.manager;

import java.util.HashSet;
import java.util.Set;

public class SymbolicVariable extends Expression {

	protected String name;
	
	protected Class<?> type;	
	
	protected PassiveObject owner;
	
		
	public SymbolicVariable(String name) {
		this.name = name;
	}

	public SymbolicVariable(String name, Class type) {
		this.name = name;
		this.type = type;
	}
	
	
	public void setOwner(PassiveObject owner){
		this.owner = owner;
	}
	
	public PassiveObject getOwner(){
		return owner;
	}
	public String getName(){
		return name;
	}

	public void setType(Class<?> type){
		this.type = type;
	}
	
	public Class getType(){
		return type;
	}
	
	@Override
	public String toString() {
		return name;
	}	
	
	@Override
	public String print() {		
		return name;
	}

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		//System.out.println("Replacing variable: " + name + "  with  " + valuation.getValue(this) );
		if (valuation.getValue(this) != null){			
			return valuation.getValue(this);
		}
		return this;
	}
	
	@Override
	public boolean equals(Object o) {
		//same object
		if (o==this)
			return true;
		//diff objects must agree for names
		if (o instanceof SymbolicVariable){
			SymbolicVariable otherVariable = (SymbolicVariable) o;
			
			if (otherVariable.name == null || name == null){
				return false;
			}
			return otherVariable.name.equals(name);
		}
		return false;
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {
		Set<SymbolicVariable> vars = new HashSet<SymbolicVariable>();
		vars.add(this);
		return vars;
	}

}
