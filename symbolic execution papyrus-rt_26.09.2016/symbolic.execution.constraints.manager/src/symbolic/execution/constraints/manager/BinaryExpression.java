package symbolic.execution.constraints.manager;

import java.util.Set;

import symbolic.execution.constraints.manager.operators.ArithmeticOperators;

/**
 * Expression that has two parts and an arithmetic operator.
 * @author zurowska
 *
 */
public class BinaryExpression extends Expression {
	protected Expression left;
	protected ArithmeticOperators op;
	protected Expression right;
	
	public BinaryExpression(Expression left, ArithmeticOperators op, Expression right){
		this.left = left;
		this.right = right;
		this.op = op;
	}
	
	public Expression getLeft() {
		return left;
	}
	
	public Expression getRight(){
		return right;
	}

	public ArithmeticOperators getOp() {		
		return op;
	}
	
	@Override
	public String print() {
		//JDA: modified to avoid null pointer exception.
		String lstr = "(null)";
		String rstr = "(null)";
		if (left != null) {
			lstr = left.print();
		}
		if (right != null) {
			rstr = right.print();
		}
		return "(" + lstr + op.toString() + rstr + ")";
	}
	
	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {		
		Expression newLeft = left.replaceVariables(valuation);
		Expression newRight = right.replaceVariables(valuation);		
		Expression evaluated = evaluate(newLeft, newRight);
		return evaluated;
	}

	/**
	 * Returns an evaluated expression - based on the operator. Evaluation is possible if left and right part are constants.
	 * @param newLeft
	 * @param newRight
	 * @return
	 */
	private Expression evaluate(Expression newLeft, Expression newRight) {
		if (newLeft instanceof Constant && newRight instanceof Constant){
			Constant leftConstant = (Constant) newLeft;
			Constant rightConstant = (Constant) newRight;
			
			if (leftConstant.getType().equals(Integer.class) && rightConstant.getType().equals(Integer.class)){				
				Integer valueLeft = (Integer)leftConstant.getValue();
				Integer valueRight = (Integer)rightConstant.getValue();
				Integer result = null;
				switch(op){
				case plus : result = new Integer(valueLeft + valueRight); break;
				case minus : result = new Integer(valueLeft - valueRight); break;
				case mult : result = new Integer(valueLeft * valueRight); break;
				case div : result = new Integer(valueLeft / valueRight); break;
				case mod : result = new Integer(valueLeft % valueRight); break;
				}
				
				return new Constant(Integer.class, result);
			}
			
			if (leftConstant.getType().equals(Long.class) && rightConstant.getType().equals(Long.class)){				
				Long valueLeft = (Long)leftConstant.getValue();
				Long valueRight = (Long)rightConstant.getValue();
				Long result = null;
				switch(op){
				case plus : result = new Long(valueLeft + valueRight); break;
				case minus : result = new Long(valueLeft - valueRight); break;
				case mult : result = new Long(valueLeft * valueRight); break;
				case div : result = new Long(valueLeft / valueRight); break;
				case mod : result = new Long(valueLeft % valueRight); break;
				}
				
				return new Constant(Long.class, result);
			}
			
			if (leftConstant.getType().equals(Float.class) && rightConstant.getType().equals(Float.class)){				
				Float valueLeft = (Float)leftConstant.getValue();
				Float valueRight = (Float)rightConstant.getValue();
				Float result = null;
				switch(op){
				case plus : result = new Float(valueLeft + valueRight); break;
				case minus : result = new Float(valueLeft - valueRight); break;
				case mult : result = new Float(valueLeft * valueRight); break;
				case div : result = new Float(valueLeft / valueRight); break;
				case mod : result = new Float(valueLeft % valueRight); break;
				}
				
				return new Constant(Float.class, result);
			}
			
			if (leftConstant.getType().equals(Double.class) && rightConstant.getType().equals(Double.class)){				
				Double valueLeft = (Double)leftConstant.getValue();
				Double valueRight = (Double)rightConstant.getValue();
				Double result = null;
				switch(op){
				case plus : result = new Double(valueLeft + valueRight); break;
				case minus : result = new Double(valueLeft - valueRight); break;
				case mult : result = new Double(valueLeft * valueRight); break;
				case div : result = new Double(valueLeft / valueRight); break;
				case mod : result = new Double(valueLeft % valueRight); break;
				}
				
				return new Constant(Float.class, result);
			}
			
			
		}
		
		return new BinaryExpression(newLeft, op, newRight);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this){
			return true;
		}
		if (o instanceof BinaryExpression){
			BinaryExpression expr = (BinaryExpression) o;			
			return expr.op.equals(op) && 
				(left == null ? expr.left==null : expr.left.equals(left) ) && 
				expr.right.equals(right);
		}
		return false;
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {	
		Set<SymbolicVariable> leftVars = left.collectVariables();
		Set<SymbolicVariable> rightVars = right.collectVariables();
		leftVars.addAll(rightVars);
 		return leftVars;
	}
	
	
	
}
