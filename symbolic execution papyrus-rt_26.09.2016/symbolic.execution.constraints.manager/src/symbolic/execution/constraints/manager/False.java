package symbolic.execution.constraints.manager;

import java.util.HashSet;
import java.util.Set;

public class False extends Constraint{

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {		
		return this;
	}

	@Override
	public String print() {
		return "FALSE";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof False){
			return true;
		}
		return false;
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {		
		return new HashSet<SymbolicVariable>();
	}

}
