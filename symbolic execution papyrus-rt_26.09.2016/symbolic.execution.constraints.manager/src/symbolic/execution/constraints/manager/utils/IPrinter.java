package symbolic.execution.constraints.manager.utils;

public interface IPrinter {
	
	String print();

}
