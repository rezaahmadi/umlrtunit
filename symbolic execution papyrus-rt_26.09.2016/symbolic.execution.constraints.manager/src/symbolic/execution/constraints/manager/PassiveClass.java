package symbolic.execution.constraints.manager;

import java.util.Collection;
import java.util.HashMap;
//import java.util.List;
import java.util.Map;

import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class PassiveClass {
	String name;
	
	public PassiveClass(String name){
		this.name = name;		
	}
	
	private Map<String, Class> fields = new HashMap<String, Class>();

	public PassiveObject createObject(String name) {
		PassiveObject obj = new PassiveObject(name, this);
		
		for (String fieldName : fields.keySet()){
			Class fieldType = fields.get(fieldName);
			SymbolicVariable fieldSv = SymbolicFactory.createSymbolicVariable(name+"."+fieldName,fieldType);
			
			obj.addField(fieldName, fieldSv);
		}
		
		return obj;
	}

	public String getName() {
		return name;
	}
	
	public void addFieldDesc(String name, Class type){
		fields.put(name, type);
	}

	public Collection<String> getFieldNames() {		
		return fields.keySet();
	}

	public java.lang.Class getType(String fieldName) {		
		return fields.get(fieldName);
	}

}
