package symbolic.execution.constraints.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class SymbolicValuation {
	
	private Map<SymbolicVariable, Expression> mapping;
	
	public SymbolicValuation(){
		mapping = new HashMap<SymbolicVariable,Expression>();
	}
	
	
	public SymbolicValuation(Set<SymbolicVariable> vars){
		this();
		for (SymbolicVariable v : vars){
			mapping.put(v, v);
		}
	}

	public SymbolicValuation(SymbolicValuation other) {	
		this();
		for (SymbolicVariable var : other.mapping.keySet()){
			mapping.put(var, other.mapping.get(var));
		}
	}
	
	public SymbolicValuation(Map<SymbolicVariable, ? extends Expression> mapping) {
		this.mapping = new HashMap<SymbolicVariable, Expression>(mapping);		
	}



	public void setValue(SymbolicVariable var, Expression value){
		mapping.put(var, value);
	}

	public Expression getValue(SymbolicVariable var){
		return mapping.get(var);
	}
	
	@Override
	public String toString() {		
		StringBuffer sb = new StringBuffer();
		
		for (SymbolicVariable p : mapping.keySet()){
			sb.append(p.toString());
			sb.append("<-");
			sb.append((mapping.get(p)).print());
			sb.append(";");
		}
		return sb.toString();		
	}


	public Set<SymbolicVariable> getVariables() {
		return mapping.keySet();
	}
}
