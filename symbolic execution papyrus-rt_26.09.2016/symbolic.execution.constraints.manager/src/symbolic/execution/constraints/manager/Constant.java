package symbolic.execution.constraints.manager;

import java.util.HashSet;
import java.util.Set;

/**
 * A constant that has a certain type and value.
 * @author zurowska
 *
 */
public class Constant extends Expression{
	
	private Class<?> type;
	
	private Object value;
	
	public Constant(Class type, Object value){
		this.type = type;
		this.value = value;
	}
	
	public Class getType(){
		return type;
	}
	
	@Override
	public String toString() {	
		if (value == null){
			return "null";
		}
		return value.toString();
	}

	@Override
	public String print() {		
		return value.toString();
	}

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {		
		return this;
	}

	public Object getValue() {		
		return value;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this){
			return true;
		}
		if (o instanceof Constant){
			Constant c = (Constant) o;
			return c.value.equals(value);
		}
		return false;
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {
		return new HashSet<SymbolicVariable>();
	}
}
