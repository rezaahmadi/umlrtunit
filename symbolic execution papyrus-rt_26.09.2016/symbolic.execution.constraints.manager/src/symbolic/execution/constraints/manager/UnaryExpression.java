package symbolic.execution.constraints.manager;

import java.util.Set;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;

/**
 * Represents an unary arithmetic operator. 
 * @author zurowska
 *
 */
public class UnaryExpression extends Expression{
	private ArithmeticOperators op;
	private Expression expr;
	
	public UnaryExpression(ArithmeticOperators op, Expression expr){
		this.expr = expr;
		this.op = op;
	}
	
	public Expression getExpression(){
		return expr;
	}
	
	public ArithmeticOperators getOp(){
		return op;
	}
	
	@Override
	public String print() {
		return op.toString() + expr.print();
	}

	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression newExpr = expr.replaceVariables(valuation);
		Expression eval = evaluate(newExpr);
		return eval;
	}
	
	private Expression evaluate(Expression newExpr) {
		Expression result = null;
		if (newExpr instanceof Constant){
			Constant constant = (Constant) newExpr;
			
			if (constant.getType().equals(Integer.class)){
				Integer value = (Integer) constant.getValue();
				
				Integer iresult = null;
				switch (op){
				case minus: iresult = new Integer(-value);
				break;
				case plus: iresult = value;
				}
				return new Constant(Integer.class,iresult);
			}
			
		}
		//JDA: added the following.
		//TODO: JDA: add additional cases if necessary.
		else {
				Expression svResult = null;
				switch (op){
				case minus: svResult = new UnaryExpression(this.op, newExpr);
				break;
				case plus: svResult = newExpr;
				break;
				}
				return svResult;
				
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this){
			return true;
		}
		if(o instanceof UnaryExpression){
			UnaryExpression otherExpr = (UnaryExpression) o;
			
			return otherExpr.op.equals(op) && 
				otherExpr.expr.equals(expr);
		}
		return super.equals(o);
	}

	@Override
	public Set<SymbolicVariable> collectVariables() {		
		return expr.collectVariables();
	}
	
}
