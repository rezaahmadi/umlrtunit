package symbolic.execution.constraints.manager;

import java.util.Collection;
import java.util.HashMap;
//import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class PassiveObject extends SymbolicVariable{
	
	private PassiveClass passiveClass;
	
	public Map<String, SymbolicVariable> fields = new HashMap<String, SymbolicVariable>();

	public Map<SymbolicVariable,Expression> fieldsValues = new HashMap<SymbolicVariable, Expression>();
	
	public PassiveObject(String name, PassiveClass passiveClass) {
		super(name);
		this.passiveClass = passiveClass;
	}

	public Collection<SymbolicVariable> getFieldVars() {
		return fields.values();
	}

	public void addField(String fieldName, SymbolicVariable fieldSv) {
		fields.put(fieldName, fieldSv);
		fieldsValues.put(fieldSv, fieldSv);
		
		fieldSv.setOwner(this);
		
	}

	public SymbolicVariable getVarField(String name) {		
		return fields.get(name);
	}

	public Set<String> getFieldNames() {
		return fields.keySet();
		
	}

	public PassiveClass getPassiveClass() { 
		return passiveClass;
	}

	public void setValue(SymbolicVariable varField, Expression exprField) {
		fieldsValues.put(varField, exprField);		
	}

	public Expression getValue(String fieldName) {
		SymbolicVariable var = fields.get(fieldName);
		return fieldsValues.get(var);
	}

}
