package symbolic.execution.code.utils;
import symbolic.execution.code.parser.CPPParserConstants;



public class TypeGenerator implements CPPParserConstants{
	
	public static Class generate(int type){
		if (type < -5){
			return null;
		}
		switch (type){		
		case INT : return Integer.class;
		case LONG : return Long.class;
		case SHORT : return Byte.class;
		case FLOAT : return Float.class;
		case DOUBLE : return Double.class;
		default : return Object.class;
		}
	}

}
