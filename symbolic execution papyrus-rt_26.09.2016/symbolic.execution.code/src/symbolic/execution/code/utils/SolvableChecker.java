package symbolic.execution.code.utils;

import java.util.List;

import choco.Choco;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.solver.Solver;

public class SolvableChecker {
	
	public static boolean check(List<Constraint> pc, Constraint constraint, boolean checkTrue){
		Model m = new CPModel();
		
		m.addConstraints(pc.toArray(new Constraint[pc.size()]));
		if (checkTrue){
			m.addConstraint(constraint);
		}else{			
			m.addConstraint(Choco.not(constraint));
		}
		
		Solver s = new CPSolver();
		s.read(m);
		boolean solvable = s.solve();
				
		return solvable;
	}

}
