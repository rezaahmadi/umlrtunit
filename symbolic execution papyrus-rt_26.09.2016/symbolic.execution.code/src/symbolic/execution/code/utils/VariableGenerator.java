package symbolic.execution.code.utils;

import symbolic.execution.code.model.cpp.OperatorKind;
import choco.Choco;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerConstantVariable;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.real.RealConstantVariable;
import choco.kernel.model.variables.real.RealExpressionVariable;

public class VariableGenerator {
	
	public static Variable makeExpression(Variable left, Variable right, OperatorKind kind){
		if (left instanceof IntegerConstantVariable &&
				right instanceof IntegerConstantVariable){
			IntegerConstantVariable leftC = (IntegerConstantVariable) left;
			IntegerConstantVariable rightC = (IntegerConstantVariable) right;
			
			switch(kind){
			case plus : return Choco.constant(leftC.getValue() + rightC.getValue());
			case minus : return Choco.constant(leftC.getValue() - rightC.getValue());
			case mult : return Choco.constant(leftC.getValue() * rightC.getValue());
			case div : return Choco.constant(leftC.getValue() / rightC.getValue());
			case mod : return Choco.constant(leftC.getValue() % rightC.getValue());
			}
		}
		
		if (left instanceof RealConstantVariable &&
				right instanceof RealConstantVariable){
			RealConstantVariable leftC = (RealConstantVariable) left;
			RealConstantVariable rightC = (RealConstantVariable) right;
			
			switch(kind){
			case plus : return Choco.constant(leftC.getValue() + rightC.getValue());
			case minus : return Choco.constant(leftC.getValue() - rightC.getValue());
			case mult : return Choco.constant(leftC.getValue() * rightC.getValue());
			case div : return Choco.constant(leftC.getValue() / rightC.getValue());
			case mod : return Choco.constant(leftC.getValue() % rightC.getValue());
			}
		}
		if (left instanceof IntegerExpressionVariable && 
				right instanceof IntegerExpressionVariable){
			IntegerExpressionVariable leftV = (IntegerExpressionVariable)left;
			IntegerExpressionVariable rightV = (IntegerExpressionVariable)right;
			return makeIntExpression(leftV, rightV, kind);
		}
		
		if (left instanceof RealExpressionVariable && 
				right instanceof RealExpressionVariable){
			RealExpressionVariable leftV = (RealExpressionVariable)left;
			RealExpressionVariable rightV = (RealExpressionVariable)right;
			return makeRealExpression(leftV, rightV, kind);
		}
		
		return null;
	}
	
	public static Variable makeIntExpression(IntegerExpressionVariable left, IntegerExpressionVariable right, OperatorKind kind){
		switch (kind){
		case plus : return Choco.plus(left, right);
		case minus : return Choco.minus(left, right);
		case mult : return Choco.mult(left, right);
		case div : return Choco.div(left, right);
		case mod : return Choco.mod(left, right);
		default : return left;
		}
	}
	
	public static Variable makeRealExpression(RealExpressionVariable left, RealExpressionVariable right, OperatorKind kind){
		switch (kind){
		case plus : return Choco.plus(left, right);
		case minus : return Choco.minus(left, right);
		case mult : return Choco.mult(left, right);
		//case div : return Choco.div(left, right);
		//case mod : return Choco.mod(left, right);
		default : return left;
		}
	}

}
