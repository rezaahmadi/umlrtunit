package symbolic.execution.code.engine;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.List;

//import symbolic.execution.code.utils.ConstraintPrinter;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicValuation;


/**
 * Stores the context of an execution
 * @author zurowska
 *
 */
public class Execution {
	private SymbolicValuation vals;
	private List<Constraint> pc;	
	private List<SendAction> outputSequence;
	private List<TimerAction> timersSet;
	private List<IncarnatePartAction> incarnateActions;
	private List<DestroyAction> destroyActions;
	private Expression output;
	private boolean isDone = false;
	
	//public Execution(){}

	public Execution(SymbolicValuation vals, List<Constraint> pc) {
		this.vals = vals;
		this.pc = pc;
		this.outputSequence = new ArrayList<SendAction>();
		this.timersSet = new ArrayList<TimerAction>();
		this.incarnateActions = new ArrayList<IncarnatePartAction>();
		this.destroyActions = new ArrayList<DestroyAction>();
	}

	public Execution(Execution execution) {
		vals = new SymbolicValuation(execution.getValuation());
		pc = new ArrayList<Constraint>(execution.getPC());
		outputSequence = new ArrayList<SendAction>(execution.getOutputSequence());
		timersSet = new ArrayList<TimerAction>(execution.getTimersSet());
		incarnateActions = new ArrayList<IncarnatePartAction>(execution.getIncarnateActions());
		destroyActions = new ArrayList<DestroyAction>(execution.getDestroyActions());
	}

	public List<SendAction> getOutputSequence() {		
		return outputSequence;
	}

	public SymbolicValuation getValuation() {
		return vals;		
	}
	
	public String printConstraints(){
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (Constraint c : pc){
			sb.append(c.print());
			sb.append(";");
		}
		sb.append("]");
		
		return sb.toString();
		
	}

	public List<Constraint> getPC() {
		return pc;
	}

	public void addSendAction(SendAction action) {
		outputSequence.add(action);		
	}
	
	@Override
	public String toString() {
		StringBuffer cString = new StringBuffer();
		cString.append("[");
		for (Constraint c : pc){
			cString.append(c.print());
			cString.append(",");
		}
		cString.append("]");
		return "VALS = " + vals + "; PC = " +cString.toString() + "; SEQ = " + outputSequence.toString() + 
			"; OUT = " + (output ==null ? "nil" : output.print()) +"; INCARNATE =" + incarnateActions;		
	}

	public void setOutput(Expression output) {
		this.output = output;
		
	}

	public Expression getOutput() {		
		return output;
	}
	
	public void makeDone(){
		isDone = true;
	}

	public boolean isDone() { 
		return isDone;
	}

	public void addTimerAction(TimerAction action) {
		timersSet.add(action);
		
	}

	public List<TimerAction> getTimersSet() {
		return timersSet;
	}
	
	public void addIncarnateAction(IncarnatePartAction action){
		incarnateActions.add(action);
	}

	public List<IncarnatePartAction> getIncarnateActions() {
		return incarnateActions;
	}
	
	public void addDestroyAction(DestroyAction action){
		destroyActions.add(action);
	}

	public List<DestroyAction> getDestroyActions() {
		return destroyActions;
	}
}
