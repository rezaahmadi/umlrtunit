package symbolic.execution.code.engine;

//public class TimerAction {
public class TimerAction {
	
	private String timerName;
	
	public TimerAction(String timerName){
		this.timerName = timerName;
	}

	public String getTimerName() {
		return timerName;
	}

}
