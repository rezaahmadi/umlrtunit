package symbolic.execution.code.engine;

public class IncarnatePartAction {
	
	private String partName;

	public IncarnatePartAction(String partName) {
		super();
		this.partName = partName;
	}

	public String getPartName() {
		return partName;
	}
	
	@Override
	public String toString() {		
		return partName;
	}

}
