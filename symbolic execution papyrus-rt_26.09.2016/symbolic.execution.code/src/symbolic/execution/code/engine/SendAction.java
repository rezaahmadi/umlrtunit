package symbolic.execution.code.engine;

import symbolic.execution.constraints.manager.Expression;

public class SendAction {
	private String port;
	private String signal;
	
	private Expression expr;

	public SendAction(String port, String signal, Expression expr) {
		super();
		this.port = port;
		this.signal = signal;
		this.expr = expr;
	}
	
	@Override
	public String toString() {		
		return port+"."+signal+"("+ (expr==null ?"": expr.print()) +")";
	}

	public String getPort() {
		return port;
	}

	public String getSignal() {
		return signal;
	}

	public Expression getExpr() {
		return expr;
	}
	
	
	
	
}
