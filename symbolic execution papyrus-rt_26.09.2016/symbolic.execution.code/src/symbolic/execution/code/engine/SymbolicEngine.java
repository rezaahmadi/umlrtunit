package symbolic.execution.code.engine;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
//import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import symbolic.execution.code.model.cpp.Program;
import symbolic.execution.code.model.cpp.Statement;
import symbolic.execution.code.parser.CPPParser;
//import symbolic.execution.code.parser.ParseException;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.SymbolicValuation;
import symbolic.execution.constraints.manager.SymbolicVariable;



public class SymbolicEngine {
	private Program program;
	private SymbolicValuation valuation;
	private List<Constraint> pc;
	
	private Set<Execution> results;
	
	public SymbolicEngine (String code, Set<SymbolicVariable> vars, Set<SymbolicVariable> inVars, Set<PassiveClass> pClasses){
		program = new Program();
		program.addVariables(vars);
		program.addInputVariables(inVars);
		program.addPassiveClasses(pClasses);
		
		String before = null;
		 try {
			 before = "InputStream input = createStream(code);";
			 InputStream input = createStream(code);
			 before = "CPPParser parser = new CPPParser(input);";
			 CPPParser parser = new CPPParser(input);
			 //JDA: added the following.
			 parser.setCode(code);
			 before = "parser.setProgram(program);";
			 parser.setProgram(program);
			 before = "parser.translation_unit();";
			 parser.translation_unit();	
			 //System.out.println("parsed ");
		 } catch (Throwable e) {
			 //JDA: Made the following more expressive
			 System.out.println("JDA: SymbolicEngine.SymbolicEngine(1) -- ERRROR!! before = [[" + before + "]].");
			 System.out.println(code);
			 System.out.println(e);
			 e.printStackTrace();
			 throw new IllegalArgumentException("SymbolicEngine encountered parse error in code\n:" +
					code + "\nDetails:\n" + e.getMessage());
			 //JDA: end change
		 }
		 	
		 //System.out.println(program.printStatements());
		 //System.out.println("parsed");
		 valuation = new SymbolicValuation(program.getVariables());
		 pc = new ArrayList<Constraint>();
		    
	}
	


	public SymbolicEngine(Program program) {
		this.program = program;
		
		valuation = new SymbolicValuation(program.getVariables());
		pc = new ArrayList<Constraint>();
	}
	
	
	public SymbolicEngine(String code, Set<SymbolicVariable> attributes,
			Set<SymbolicVariable> inputs) {
		program = new Program();
		program.addVariables(attributes);
		program.addInputVariables(inputs);		
						
		 try {
			 InputStream input = createStream(code);
			 CPPParser parser = new CPPParser(input);
			 parser.setProgram(program);
			 parser.translation_unit();	
			 //System.out.println("parsed ");
		 } catch (Throwable e) {
			 System.out.println("JDA: SymbolicEngine.SymbolicEngine (3) -- ERRROR!!");
			 System.out.println(code);
			 System.out.println(e);
		 }
		 	
		 //System.out.println(program.printStatements());
		 //System.out.println("parsed");
		 valuation = new SymbolicValuation(program.getVariables());
		 pc = new ArrayList<Constraint>();
	}



	public void execute(){		
		Execution initial = new Execution(valuation,pc);
		
		//System.out.println("INITIAL VALUATION" + initial.getValuation());
		
		try {
			for (Statement s : program.getStatements() ){		
				results = s.executeSymbolic(initial);			
			}
			
		} catch(Throwable e){
			//JDA: added conditional rethrow
			if (e instanceof IllegalArgumentException) {
				throw (IllegalArgumentException) e;
			}
			if (e instanceof java.lang.ArithmeticException) {
				throw (ArithmeticException) e;
			}
			e.printStackTrace();
		}
				
	}
	
	public Set<Execution> getResults(){
		return results;
	}

	private InputStream createStream(String code) {
		char[] charArray = code.toCharArray();
		byte[] byteArray = new byte[charArray.length];
		for (int i=0; i <byteArray.length; i++){
			byteArray[i] = (byte) charArray[i];
		}
		
		return new ByteArrayInputStream(byteArray);
	}
		
}
