package symbolic.execution.code.engine;

import choco.kernel.model.variables.Variable;

public class SymbolicValue {
	private Variable expr;
	
	public SymbolicValue(Variable expr){
		this.expr = expr;
	}
	
	public Variable getExpression(){
		return expr;
	}

}
