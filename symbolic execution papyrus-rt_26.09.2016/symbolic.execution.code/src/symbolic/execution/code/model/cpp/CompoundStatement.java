package symbolic.execution.code.model.cpp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import choco.kernel.model.constraints.Constraint;
import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.parser.ParseException;

public class CompoundStatement extends Statement {
	
	/**list of statements in this compound statement*/
	List<Statement> statements = new ArrayList<Statement>();
	
	public void addStatement(Statement s){
		statements.add(s);
	}
	public CompoundStatement() {
		
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{\n");
		for (Statement s : statements){
			if (s!=null){
				sb.append(s.toString());
				sb.append(";\n");
			}
		}
		sb.append("}\n");
		return sb.toString();
	}
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//start the execution from the first statement
		Set<Execution> execs = executeStatement(execution, 0);
		
		return execs;
	}
	
	private Set<Execution> executeStatement(Execution execution, int statement){
		Set<Execution> result = new HashSet<Execution>();
			
		//if all executed
		if (statement >= statements.size() || execution.isDone()){
			result.add(execution);
			return result;
		}
						
		//get the current execution statement 
		Statement st = statements.get(statement);
		try {
			//execute the current
			Set<Execution> execs = st.executeSymbolic(execution);		
		
			//for each result of the execution
			for (Execution e : execs){
				
				//execute the next statement
				Set<Execution> resultExec = executeStatement(e, statement+1);
				result.addAll(resultExec);
			}
		} catch (Exception e){
			
			//JDA: added rethrow of e.
			String str = "!!! Error in statement: " + st + ".\n"+ e.getMessage();
			if (e instanceof NullPointerException) {
				System.out.println("!! ERROR --> " + st);
				throw new NullPointerException(str);
			}
			if (e instanceof IllegalArgumentException) {
				System.out.println("!! ERROR --> " + st);
				throw new IllegalArgumentException(str);
			}
			if (e instanceof java.lang.ArithmeticException) {
				throw (java.lang.ArithmeticException) e;
			}
		}
		return result;
	}
}
