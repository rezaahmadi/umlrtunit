package symbolic.execution.code.model.cpp;

//import java.util.ArrayList;
import java.util.HashSet;
//import java.util.List;
import java.util.Set;

//import parser.absconparseur.PredicateTokens.RelationalOperator;

//import choco.Choco;



import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.utils.ConstraintPrinter;
//import symbolic.execution.code.utils.SolvableChecker;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

public class IfStatement extends Statement {
	/** condition in if statement */
	private Constraint expr;
	
	/** if part */
	private Statement ifStatement;
	
	/** else part */
	private Statement elseStatement;
	
	/** solver */
	private Solver solver;
	
	public IfStatement(Expression expr, Statement ifStatement,
			Statement elseStatement) {
		super();
				
		if (expr instanceof Constraint) {
			this.expr = (Constraint) expr;
			
		} else if (expr instanceof SymbolicVariable){
			CompareOperators kind = CompareOperators.eq;
			Expression exprRight = SymbolicFactory.createConstant(Boolean.class, new Boolean(true));
			this.expr = SymbolicFactory.createRelationalConstraint(expr,kind,exprRight);
		}
		
		this.ifStatement = ifStatement;
		this.elseStatement = elseStatement;
		
		solver = SymbolicFactory.createSolver();
	}
	
	@Override
	public String toString() {		
		return "if (" + expr.print() + ") \n " + 
			ifStatement + 
			(elseStatement!=null ? "else \n " + elseStatement : "");
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		//build constraints from condition	
		
		Constraint condition = (Constraint) expr.replaceVariables(execution.getValuation());		
		
		//check if true solvable		
		boolean trueSolvable = solver.isSolvable(execution.getPC(), condition, true);
		//System.out.println("True solvable " + trueSolvable);
		
		//check if false solvable
		boolean falseSolvable = solver.isSolvable(execution.getPC(), condition, false);
		//System.out.println("false solvable " + falseSolvable);
				
		if (trueSolvable){
			if (falseSolvable){					
				//case 1: both possible				
				Execution oldExec = new Execution(execution);				
				
				//assume true
				execution.getPC().add(condition);
				Set<Execution> ifExecs = ifStatement.executeSymbolic(execution);
				result.addAll(ifExecs);				
				
				//assume false
				if (elseStatement != null){
					Constraint notCondition = SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, condition);
										
					oldExec.getPC().add(notCondition);
					
					Set<Execution> elseExecs = elseStatement.executeSymbolic(oldExec);
					result.addAll(elseExecs);
				} else {
					oldExec.getPC().add(SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, condition));
					result.add(oldExec);
				}
			} else {				
				//case 2: always true
				Set<Execution> ifExecs = ifStatement.executeSymbolic(execution);
				result.addAll(ifExecs);
			}
		} else {
			//case 3: always false
			if (elseStatement != null){								
				Set<Execution> elseExecs = elseStatement.executeSymbolic(execution);
				result.addAll(elseExecs);
			} else {
				result.add(execution);
			}
		}
		
		return result;
	}
}
