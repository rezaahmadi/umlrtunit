package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.constraints.manager.Expression;

public class ReturnStatement extends Statement {
	private Expression output;

	public ReturnStatement(Expression output){
		this.output = output;
	}
	
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
				
		//execution over 
		execution.makeDone();
		
		//evaluate output
		Expression evaluated = output.replaceVariables(execution.getValuation());
		execution.setOutput(evaluated);
				
		//return the only execution
		result.add(execution);
		return result;
	}
	
	@Override
	public String toString() {		
		return "return " + output.print();
	}

}
