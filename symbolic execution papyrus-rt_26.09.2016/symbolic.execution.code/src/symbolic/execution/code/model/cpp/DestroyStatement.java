package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.DestroyAction;
import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.engine.IncarnatePartAction;

public class DestroyStatement extends Statement {

	private String partName;
	
	
	
	public DestroyStatement(String partName) {
		super();
		this.partName = partName;
	}



	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		
		//add send action to a sequence
		DestroyAction action = new DestroyAction(partName);
		execution.addDestroyAction(action);
		
		//return the only execution
		result.add(execution);		
		return result;
	}

}
