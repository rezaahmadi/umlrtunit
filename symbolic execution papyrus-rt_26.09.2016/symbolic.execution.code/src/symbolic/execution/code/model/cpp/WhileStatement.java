package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

public class WhileStatement extends Statement{
	
	/** condition in the while loop*/
	private Constraint condition;
	/** statements in the loop */	
	private Statement doStatements;
	
	private Solver solver;
	
	private int bound = 10;
	
	public WhileStatement(Expression condition,
			Statement doStatements) {
		super();
		
		solver = SymbolicFactory.createSolver();
		
		if(condition instanceof Constraint){
			this.condition = (Constraint)condition;
			this.doStatements = doStatements;
		}
	}


	@Override
	public String toString() {	
		StringBuffer sb = new StringBuffer();
		sb.append("while (");
		sb.append(condition);
		sb.append(")\n");		
		sb.append(doStatements.toString());
		sb.append("\n");
		return sb.toString();
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> results = new HashSet<Execution>();
		
		//if bound has been exhausted then return
		if(bound==0){			
			results.add(execution);
			return results;
		}
		bound--;
				
		//evaluate condition in the while loop
		Constraint condEvaluated = (Constraint) condition.replaceVariables(execution.getValuation());
		
		//check whether condition is solvable
		boolean trueSolvable = solver.isSolvable(execution.getPC(), condEvaluated, true);
		boolean falseSolvable = solver.isSolvable(execution.getPC(), condEvaluated, false);
		
		if (trueSolvable){			
			if (falseSolvable){
				//case 1: both solvable
				
				//evaluate if the loop executes - true
				Execution execTrue = new Execution(execution);
				execTrue.getPC().add(condEvaluated);
				Set<Execution> execsInDo = new HashSet<Execution>();
				execsInDo.addAll(doStatements.executeSymbolic(execTrue));
				
				for (Execution exec : execsInDo){
					//execute the loop again
					results.addAll(executeSymbolic(exec));
				}
				
				//evaluate if the loop terminates - false
				Execution execFalse = new Execution(execution);
				execFalse.getPC().add(SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, condEvaluated));
				results.add(execFalse);
				
				return results;
			} else {
				//case 2: true only
				Execution execTrue = new Execution(execution);
				Set<Execution> execsInDo = new HashSet<Execution>();
				execsInDo.addAll(doStatements.executeSymbolic(execTrue));
				
				for (Execution exec : execsInDo){
					//execute the loop again
					results.addAll(executeSymbolic(exec));
				}
				
				return results;
			}
			
		} else {
			//case 3: loop never executes
			results.add(execution);
		}
			
		
		
		return null;
	}

	
}
