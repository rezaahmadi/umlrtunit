package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

//import choco.Choco;
//import choco.kernel.model.variables.integer.IntegerConstantVariable;
import symbolic.execution.code.engine.Execution;
//import symbolic.execution.constraints.choco.ConstantImpl;
//import symbolic.execution.constraints.manager.BinaryExpression;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.constraints.manager.operators.ArithmeticOperators;


//JDA:This class represents an abort statement
public class AbortStatement extends Statement {
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//System.out.println("AbortStatement!");
		Set<Execution> result = new HashSet<Execution>();
		//JDA: Here 'execution' represents the execution to follow.
		//JDA: It is the 'continuation' of the statement.
		//JDA: The abort statement has an empty continuation.
		return(result);
	}

}
