package symbolic.execution.code.model.cpp;

//import java.util.ArrayList;
import java.util.HashSet;
//import java.util.List;
import java.util.Set;

//import choco.kernel.model.constraints.Constraint;
//import choco.kernel.model.variables.Variable;


import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.utils.ConstraintPrinter;
//import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.PassiveObject;
//import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.SymbolicVariable;


public class Assignment extends Statement {
	
	/** variable to assign to */
	private SymbolicVariable var;
	/** expression that represents assigned value*/
	private Expression expr;
	
	/**
	 * Creates a new assignment based on the input expression.
	 * Note: expression must be of the form 'a=expr'
	 * @param expr
	 */
	public Assignment(Expression left, Expression right){		
		if (left instanceof SymbolicVariable){
				this.var = (SymbolicVariable) left;
		}
		if (right instanceof Constant){
			System.out.print("");
		}
		this.expr = right;
	}
	
	public Assignment(SymbolicVariable var, Expression expr){
		this.var = var;
		this.expr = expr;
	}
	
	@Override
	public String toString() {		
		return var + " <- " + (expr==null ? "null " : expr.print());
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
			
		//System.out.println(expr);
	
		//if a variable is a passive object
		if (var instanceof PassiveObject) {
			PassiveObject varObj = (PassiveObject)var;
			if (expr instanceof PassiveObject) {
				PassiveObject exprObj = (PassiveObject) expr;
				for (String fieldName : varObj.getFieldNames()){
					SymbolicVariable varField = varObj.getVarField(fieldName);					
					SymbolicVariable varTarget = exprObj.getVarField(fieldName);
					
					Expression exprTarget = execution.getValuation().getValue(varTarget);
					
					Expression exprEval = exprTarget.replaceVariables(execution.getValuation());
					//update object
					varObj.setValue(varField,exprEval);
					//upadte valuation
					execution.getValuation().setValue(varField, exprEval);
					
				}
			}
		} else {
			
			//replace variables in the assignment expression based on the received valuation
			Expression exprEval = expr.replaceVariables(execution.getValuation());		
			
		
			//replace the value of the variables
			execution.getValuation().setValue(var, exprEval);
			
			//check if field
			if (var.getOwner() != null){
				var.getOwner().setValue(var, exprEval);
			}
		}
		
		//return the only execution
		result.add(execution);
		
		return result;
		
	}
}
