package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;


//JDA:This class represents a null statement.
public class NullStatement extends Statement {
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//System.out.println("NullStatment!");
		Set<Execution> result = new HashSet<Execution>();
		//JDA: Here 'execution' represents the execution to follow.
		//JDA: It is the 'continuation' of the statement.
		//JDA: The null statement adds nothing to the continuation.
		result.add(execution);
		return result;
	}

}
