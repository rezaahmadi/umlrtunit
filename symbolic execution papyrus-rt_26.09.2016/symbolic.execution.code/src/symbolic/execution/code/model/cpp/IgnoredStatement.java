package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;


//JDA:This class log statements or other statements that can be 'safely' ignored.
public class IgnoredStatement extends Statement {
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//System.out.println("LogStatement!");
		Set<Execution> result = new HashSet<Execution>();
		//JDA: This treats log statements as comments.
		//JDA: We aren't worrying about details.
		result.add(execution);
		return result;
	}

}
