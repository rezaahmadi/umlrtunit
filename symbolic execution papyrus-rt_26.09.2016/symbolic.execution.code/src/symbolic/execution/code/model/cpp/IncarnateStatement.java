package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.engine.IncarnatePartAction;
//import symbolic.execution.code.engine.TimerAction;

public class IncarnateStatement extends Statement {

	private String partName;
	
	
	public IncarnateStatement(String partName) {
		super();
		this.partName = partName;
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		
		//add send action to a sequence
		IncarnatePartAction action = new IncarnatePartAction(partName);
		execution.addIncarnateAction(action);
		
		//return the only execution
		result.add(execution);		
		return result;
		
	}

}
