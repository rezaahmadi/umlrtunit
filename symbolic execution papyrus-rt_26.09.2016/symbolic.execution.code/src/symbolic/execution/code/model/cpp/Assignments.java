package symbolic.execution.code.model.cpp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import choco.kernel.model.constraints.Constraint;


import symbolic.execution.code.engine.Execution;

public class Assignments extends Statement {
	List<Assignment> assignments = new ArrayList<Assignment>();
	
	
	/**
	 * List of assignments in one statement.
	 * @param assigns
	 */
	public Assignments(List<Assignment> assigns){
		this.assignments = assigns;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Assignment a : assignments){
			sb.append(a.toString());
			sb.append(";\n");
		}
		return sb.toString();
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		//execute each statement
		for (Statement s : assignments){
			result.addAll(s.executeSymbolic(execution));
		}
		
		return result;
		
	}

}
