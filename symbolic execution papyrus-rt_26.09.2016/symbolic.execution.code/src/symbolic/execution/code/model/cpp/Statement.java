package symbolic.execution.code.model.cpp;

//import java.util.List;
import java.util.Set;

//import choco.kernel.model.constraints.Constraint;


import symbolic.execution.code.engine.Execution;


/**
 * The abstract class for all constructs in a language
 * @author zurowska
 *
 */
public abstract class Statement {
	
	
	/**
	 * A method that ensures that all constructs can be symbolically executed. 
	 * @param execution - a context of a symbolic execution
	 * @return one or more executions that results from  symbolic execution of this satetment 
	 */
	public abstract Set<Execution> executeSymbolic(Execution execution);
}
