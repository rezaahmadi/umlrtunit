package symbolic.execution.code.model.cpp;

//import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.model.cpp.LookupConstraints;
import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.utils.ConstraintPrinter;
//import symbolic.execution.constraints.manager.BinaryExpression;
//import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.PassiveObject;
//import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.SymbolicVariable;


//JDA: This class represents an assert statement.
//JDA: This initial version is unevaluated!
public class LookupAssert extends Statement {
	private int constraintIndex;
	
	public LookupAssert(int consRef){
		constraintIndex = consRef;
	}
	
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//System.out.println("AssertStatement! expr=[" + expr + "].");
		Set<Constraint> constraints = LookupConstraints.getConstraints(constraintIndex);
		CompoundStatement cs = new CompoundStatement();
		for (Constraint con: constraints) {
			Expression exp = con;
			AssertStatement tmpAssert = new AssertStatement(exp);
			cs.addStatement(tmpAssert);
		}
		
		Set<Execution> result = cs.executeSymbolic(execution);
		//System.out.println("JDA: evaluating lookup assert, " + constraintIndex + ".");
		//System.out.println("JDA: exectution = [" + execution + "].");
		if (result == null) {
			//System.out.println("JDA: abort evaluating lookup assert, " + constraintIndex + ".");
		}
		//System.out.println("JDA: success evaluating lookup assert, " + constraintIndex + ".");
		//System.out.println("JDA: constraints = [" + constraints + "].");
		//System.out.println("JDA: result = [" + result + "].");
		return result;
	}
}
