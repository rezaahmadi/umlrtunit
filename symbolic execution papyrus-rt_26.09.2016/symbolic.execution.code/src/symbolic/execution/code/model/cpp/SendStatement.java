package symbolic.execution.code.model.cpp;

import java.util.HashSet;
//import java.util.List;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.engine.SendAction;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class SendStatement extends Statement{
	/** name of port used to send */
	private String portName;
	
	/** name of signal */
	private String signalName;
		
	/** value of send variable */
	private Expression value;
	
	//JDA: added assignment option
	private Assignment assignment = null;
		
	public SendStatement(String portName, String signalName, Expression value) {
		super();
		this.portName = portName;
		this.signalName = signalName;
		this.value = value;
	}

	//JDA: New variant for the send assignment
	public SendStatement(String portName, String signalName, Expression value,
			SymbolicVariable id) {
		super();
		this.portName = portName;
		this.signalName = signalName;
		this.value = value;
		//JDA: Currently we assume that all send assignments assign the value 1 to the variable given by 'id'.
		//JDA: The current implementation does not allow for errors in a send.
		assignment = new Assignment(id, SymbolicFactory.createConstant(Integer.class, 1));
	}

	@Override
	public String toString() {		
		return portName +"."+signalName+"("+ value +")";
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		
		//evaluate expression
		Expression outputEvaluated = null;
		if (value !=null){
			outputEvaluated = value.replaceVariables(execution.getValuation());
		}
		//JDA: Added the following if statement to distinguish send assignments from ordinary send statements.
		if (assignment != null ) {
			//JDA: The send statement is a send assignment. Perform the send followed by the assignment
			//JDA: as a compound statement.
			SendStatement ordinarySendStatement = new SendStatement(portName, signalName, outputEvaluated);
			CompoundStatement cs = new CompoundStatement();
			cs.addStatement(ordinarySendStatement);
			cs.addStatement(assignment);
			result = cs.executeSymbolic(execution);
		}
		else {
			//JDA: The send statement is ordinary.
			//add send action to a sequence
			SendAction action = new SendAction(portName, signalName, outputEvaluated);
			execution.addSendAction(action);
			result.add(execution);
		}
		return result;
	}

}
