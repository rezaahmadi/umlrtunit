package symbolic.execution.code.model.cpp;

//import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;

//import symbolic.execution.code.engine.Execution;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.utils.ConstraintPrinter;
//import symbolic.execution.constraints.manager.BinaryExpression;
//import symbolic.execution.constraints.manager.Constant;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.PassiveObject;
//import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.Constraint;

public class LookupConstraints {
	private static int nextIndex = 0;
	private static ArrayList<Set<Constraint>> LookupArray = null;
	public static void reset() {
		nextIndex = 0;
		LookupArray = null;
	}
	public static int addConstraints(Set<Constraint> cons) {
		int result = nextIndex;
		if (LookupArray == null) {
			LookupArray = new ArrayList<Set<Constraint>>();
		}
		LookupArray.add(cons);
		nextIndex++;
		return result;
	}
	public static Set<Constraint> getConstraints(int index) {
		return(LookupArray.get(index));
	}

}
