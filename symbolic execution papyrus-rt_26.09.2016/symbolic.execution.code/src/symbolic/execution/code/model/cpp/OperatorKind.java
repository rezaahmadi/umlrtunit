package symbolic.execution.code.model.cpp;

public enum OperatorKind {
	
	plus, minus, mult, div, mod, eq

}
