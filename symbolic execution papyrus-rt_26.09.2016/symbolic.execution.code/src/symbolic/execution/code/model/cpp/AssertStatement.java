package symbolic.execution.code.model.cpp;

//import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.utils.ConstraintPrinter;
//import symbolic.execution.constraints.manager.BinaryExpression;
//import symbolic.execution.constraints.manager.Constant;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.PassiveObject;
//import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.SymbolicVariable;


//JDA: This class represents an assert statement.
//JDA: This initial version is unevaluated!
public class AssertStatement extends Statement {
	private Expression expr;
	
	public AssertStatement(Expression expr){		
		this.expr = expr;
	}
	
	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		//System.out.println("AssertStatement! expr=[" + expr + "].");
		NullStatement nullStatement = new NullStatement();
		AbortStatement abortStatement = new AbortStatement();
		IfStatement ifStatment = new IfStatement(expr, nullStatement, abortStatement);
		Set<Execution> result = ifStatment.executeSymbolic(execution);
		if (result == null) {
			//System.out.println("JDA: abort evaluating assert, " + expr+ ".");
		}
		return (result);
	}
}
