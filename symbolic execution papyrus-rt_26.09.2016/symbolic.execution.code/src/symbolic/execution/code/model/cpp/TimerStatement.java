package symbolic.execution.code.model.cpp;

import java.util.HashSet;
import java.util.Set;

import symbolic.execution.code.engine.Execution;
//import symbolic.execution.code.engine.SendAction;
import symbolic.execution.code.engine.TimerAction;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class TimerStatement extends Statement{
	/* ===========================================================================================
	 * JDA: In the "old version" of SE, this class not being treated as a 'SendStatement'
	        and, therefore, the 'execute' action of class 'ExecutionEngine' was not able to any set
	        timers. I modified the class so that to new version code is executed by default and
	        a private static flag and associated "set" method to turn on the required "old version"
	        functionality.
	============================================================================================== */
	//JDA: added 'oldSE' and 'setOldSE'
	private static boolean oldSE = false;
	
	public static void setOldSE(boolean b) {
		oldSE = b;
	}
	//JDA: end addition.
	
	private String name;
	private String portName;
	
	private SendStatement sendStatement;
	
	//JDA: added assignment option
	private Assignment assignment;
	
	public TimerStatement(String name, Expression expr){
		this.portName = name;
		//JDA: the following line has been changed.
		//sendStatement = new SendStatement(name, "inform", expr);
		sendStatement = new SendStatement(name, "informIn", expr);
		this.name = "informIn";
	}
	
	//JDA: New initializer for the assignment.
	public TimerStatement(String name, Expression id, Expression expr){
		this.portName = name;
		//JDA: the following line has been changed.
		//sendStatement = new SendStatement(name, "inform", expr);
		sendStatement = new SendStatement(name, "informIn", expr);
		assignment = new Assignment(id, SymbolicFactory.createConstant(Integer.class, 1));
		this.name = "informIn";
	}

	@Override
	public Set<Execution> executeSymbolic(Execution execution) {
		Set<Execution> result = new HashSet<Execution>();
		//JDA: if statement added.
		if (oldSE) {
			//JDA: "old version" functionality"
			//JDA: Is the following even relevant for the "old version"?
			//JDA: Changed the following
			//TimerAction action = new TimerAction(name);
			//TimerAction action = new TimerAction(portName);
			//JDA: I left this the same
			//execution.addTimerAction(action);
			if (assignment != null ) {
				CompoundStatement cs = new CompoundStatement();
				cs.addStatement(sendStatement);
				cs.addStatement(assignment);
				result = cs.executeSymbolic(execution);
			}
			else {
				result = sendStatement.executeSymbolic(execution);
			}
			return result;
		}
		else {
			//add send action to a sequence
			//JDA: New version functionality.
			//System.out.println("TimerStatement.executeSymbolic (new version).");
			TimerAction action = new TimerAction(name);
			execution.addTimerAction(action);
			
			//return the only execution
			result.add(execution);		
			return result;
		}
	}
}
