package symbolic.execution.code.model.cpp;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.code.parser.Token;
import symbolic.execution.code.utils.TypeGenerator;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;


public class Program {
	/* mapping of names to variables */
	private Map<String,SymbolicVariable> variables = new HashMap<String,SymbolicVariable>();
	
	/* statements of the programs */
	private List<Statement> statements = new ArrayList<Statement>();
	
	/*lookup for external passive classes*/
	private Map<String,Integer> passiveClassesLookup = new HashMap<String,Integer>();
	
	/*passive classes**/
	private Map<Integer, PassiveClass> passiveClasses= new HashMap<Integer,PassiveClass>();

	/*objects from passive classes*/
	private Map<String, PassiveObject> passiveObjects = new HashMap<String, PassiveObject>();
	
	public void addStatement(Statement s){
		statements.add(s);
	}
	
	public void addVariable(String name){
		SymbolicVariable sv = SymbolicFactory.createSymbolicVariable(name);
		if(! variables.containsKey(sv.getName())){
			variables.put(sv.getName(),sv);
		}
	}
	
	public void addVariable(SymbolicVariable sv){
		if(! variables.containsKey(sv.getName())){
			variables.put(sv.getName(),sv);
		}
	}
	
	public void addVariable(String name, int type){
		if (variables.containsKey(name)){
			return;
		}
		if (type > 0){
			SymbolicVariable sv = SymbolicFactory.createSymbolicVariable(name, TypeGenerator.generate(type));
			if(! variables.containsKey(sv.getName())){
				variables.put(sv.getName(),sv);
			}
		} else {
			addVariablePassive(name, type);
		}
		
	}
	
	
	public void setVarType(String name, int type){
		if (type < -5){
			variables.remove(name);
			addVariablePassive(name,type);
		} else {
			variables.get(name).setType(TypeGenerator.generate(type));
		}
	}
	
	private void addVariablePassive(String name, int type) {
		//get a factory for a passiveClass
		PassiveClass passiveClass = passiveClasses.get(type);
		
		PassiveObject passiveObject = passiveClass.createObject(name);
		
		for (SymbolicVariable var : passiveObject.getFieldVars()){
			if (! variables.containsKey(var.getName())){
				variables.put(var.getName(), var);
			}
		}			
		passiveObjects.put(name, passiveObject);
		
		
		
	}

	public String printVariables(){
		return variables.toString();
	}

	public String printStatements() {
		StringBuffer sb = new StringBuffer();
		for (Statement s: statements){
			sb.append(s.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

	
	public SymbolicVariable lookup(String name) {
		if (variables.containsKey(name)){
			return variables.get(name);
		}
//		else if (name.equals("p1")) {
//			addVariable("p1var0");
//			return variables.get("p1var0");
//		}
//		else if (name.equals("p2")) {
//			addVariable("p2var0");
//			return variables.get("p2var0");
//		}
		return passiveObjects.get(name);
	}
	
	public SymbolicVariable lookup(Token t) {
		if (variables.containsKey(t.getValue())){
			return variables.get(t.getValue());
		}
		return passiveObjects.get(t.getValue());
	}

	public Set<SymbolicVariable> getVariables() {
		return new HashSet<SymbolicVariable>(variables.values());
	}

	public List<Statement> getStatements() {
		return statements;
	}

	public void addVariables(Set<SymbolicVariable> vars) {
		for (SymbolicVariable var : vars){
			if (var instanceof PassiveObject)
				addPassiveObject((PassiveObject) var);
			else {
				if (variables.containsKey(var)){
					continue;
				}
				variables.put(var.getName(), var);
				if (var.getOwner() != null){
					variables.put(var.getOwner().getName(), var.getOwner());
					passiveObjects.put(var.getOwner().getName(), var.getOwner());
				}
			}
		}
		
		
	}

	public void addInputVariables(Set<SymbolicVariable> vars) {
		for (SymbolicVariable var : vars){
			if (var instanceof PassiveObject)
				addPassiveObjectInput((PassiveObject) var);
			else 
				variables.put("input", var);
		}
		
	}
	
	public int getExternalClass(String name){
		if (passiveClassesLookup.containsKey(name)){
			return passiveClassesLookup.get(name);
		} else {
			return -5;
		}
			
	}
	
	public void addPassiveClass(PassiveClass cl){
		int index = -1 * (passiveClassesLookup.size() + 10);
		
		passiveClassesLookup.put(cl.getName(), index);
		passiveClasses.put(index, cl);
	}

	public PassiveObject lookupPassiveObject(String name) {		
		return passiveObjects.get(name);
	}

	public void addPassiveObject(PassiveObject inputObj) {
		passiveObjects.put(inputObj.getName(), inputObj);
		for (SymbolicVariable sv : inputObj.getFieldVars()){
			variables.put(sv.getName(), sv);
		}
		
	}
	
	public void addPassiveObjectInput(PassiveObject inputObj) {
		passiveObjects.put("input", inputObj);
		for (SymbolicVariable sv : inputObj.getFieldVars()){
			variables.put(sv.getName(), sv);
		}
		
	}

	public void addPassiveClasses(Set<PassiveClass> pClasses) {
		for (PassiveClass pc : pClasses){
			addPassiveClass(pc);
		}
		
	}
	
}
