package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

//import choco.Choco;


import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.engine.SymbolicEngine;
import symbolic.execution.code.model.cpp.Program;
import symbolic.execution.code.model.cpp.Statement;
import symbolic.execution.code.parser.CPPParser;
import symbolic.execution.code.parser.ParseException;
import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.Engine;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class TestSymbolicExecution {
	public static void main(String[] args) {
		try {
			Program p = new Program();
			
			SymbolicFactory.setEngine(new EngineChoco());
			
			SymbolicVariable v = SymbolicFactory.createSymbolicVariable("lockCode1");
			v.setType(Integer.class);			
			p.addVariable(v);
			
			SymbolicVariable w = SymbolicFactory.createSymbolicVariable("lockCode2");
			w.setType(Integer.class);			
			p.addVariable(w);
			
			SymbolicVariable z = SymbolicFactory.createSymbolicVariable("lockCode3");
			z.setType(Integer.class);			
			p.addVariable(z);
			
			SymbolicVariable a = SymbolicFactory.createSymbolicVariable("matchCode1");
			a.setType(Integer.class);			
			p.addVariable(a);
			
			SymbolicVariable b = SymbolicFactory.createSymbolicVariable("matchCode2");
			b.setType(Integer.class);			
			p.addVariable(b);
			
			SymbolicVariable c = SymbolicFactory.createSymbolicVariable("matchCode3");
			c.setType(Integer.class);			
			p.addVariable(c);
			
			SymbolicVariable j = SymbolicFactory.createSymbolicVariable("carLights");
			j.setType(Integer.class);			
			p.addVariable(j);
			
			SymbolicVariable v1 = SymbolicFactory.createSymbolicVariable("input");
			v1.setType(Integer.class);			
			p.addVariable(v1);
			
			
			InputStream input = new FileInputStream(new File("cpp_examples/cpp4.txt"));			
			CPPParser parser = new CPPParser(input);
			parser.setProgram(p);
			parser.translation_unit();
			System.out.println("Program parsed successfully.");
			
			System.out.println("Variables -->" + p.printVariables());
			
			for(Statement s : p.getStatements()){
				System.out.println(s);
			}
			
			
			//System.out.println(p.printStatements());
			
			SymbolicEngine eng = new SymbolicEngine(p);
			eng.execute();
			
			for (Execution e : eng.getResults()) {
				System.out.println("EXECUTION");
				System.out.println(e.toString());				
				
			}
			
			System.out.println("DONE");
			
		} catch (ParseException e) {
			System.out.println("Encountered errors during parse.");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}
}
