package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.engine.SymbolicEngine;
import symbolic.execution.code.model.cpp.Program;
//import symbolic.execution.code.model.cpp.Statement;
import symbolic.execution.code.parser.CPPParser;
import symbolic.execution.code.parser.ParseException;
import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class TestSuiteBasic {
	
	public static Program testAssignments() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppAssignment.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testConditions() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppConditions.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testInput() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		SymbolicVariable varI = SymbolicFactory.createSymbolicVariable("input");
		varI.setType(Integer.class);		
		p.addVariable(varI);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppInput.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	
	public static Program testReturn() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		
		InputStream input = new FileInputStream(new File("cpp_examples/cppReturn.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testWhile() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		
		InputStream input = new FileInputStream(new File("cpp_examples/cppWhile.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	
	public static Program testSend() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		
		InputStream input = new FileInputStream(new File("cpp_examples/cppSend.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testIncarnate() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		
		InputStream input = new FileInputStream(new File("cpp_examples/cppIncarnate.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testIfStatement() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("phone1");
		var.setType(Boolean.class);		
		p.addVariable(var);

		var = SymbolicFactory.createSymbolicVariable("phone2");
		var.setType(Boolean.class);		
		p.addVariable(var);
		
		var = SymbolicFactory.createSymbolicVariable("phone3");
		var.setType(Boolean.class);		
		p.addVariable(var);
		
		var = SymbolicFactory.createSymbolicVariable("phone4");
		var.setType(Boolean.class);		
		p.addVariable(var);
		
		var = SymbolicFactory.createSymbolicVariable("numPhones");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		SymbolicVariable varI = SymbolicFactory.createSymbolicVariable("input");
		varI.setType(Integer.class);		
		p.addVariable(varI);	
		
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppIfStatement.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testInputJava() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		SymbolicVariable varI = SymbolicFactory.createSymbolicVariable("input");
		varI.setType(Integer.class);		
		p.addVariable(varI);		
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppInputJava.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testTimerJava() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);

		
		InputStream input = new FileInputStream(new File("cpp_examples/cppTimerJava.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testPassiveObjectDeclare() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		//create passive class
		PassiveClass pc = new PassiveClass("ExamplePassiveClass");
		pc.addFieldDesc("f1", Integer.class);
	
		p.addPassiveClass(pc);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppPassiveDeclare.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testPassiveObjectAssignToField() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		//create passive class
		PassiveClass pc = new PassiveClass("ExamplePassiveClass");
		pc.addFieldDesc("f1", Integer.class);
		pc.addFieldDesc("f2", Integer.class);
		pc.addFieldDesc("attribute1", Integer.class);
	
		p.addPassiveClass(pc);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppPassiveAssignToField.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testPassiveObjectAssignFromField() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		//create passive class
		PassiveClass pc = new PassiveClass("ExamplePassiveClass");
		pc.addFieldDesc("f1", Integer.class);
		pc.addFieldDesc("f2", Integer.class);
	
		p.addPassiveClass(pc);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppPassiveAssignFromField.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testPassiveObjectSend() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		//create passive class
		PassiveClass pc = new PassiveClass("ExamplePassiveClass");
		pc.addFieldDesc("f1", Integer.class);
		pc.addFieldDesc("f2", Integer.class);
	
		p.addPassiveClass(pc);
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppPassiveObjectSend.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	public static Program testPassiveObjectReceive() throws FileNotFoundException, ParseException {
		Program p = new Program();
		
		SymbolicVariable var = SymbolicFactory.createSymbolicVariable("k");
		var.setType(Integer.class);		
		p.addVariable(var);
		
		//create passive class
		PassiveClass pc = new PassiveClass("ExamplePassiveClass");
		pc.addFieldDesc("f1", Integer.class);
		pc.addFieldDesc("f2", Integer.class);
	
		p.addPassiveClass(pc);
		
		PassiveObject inputObj = pc.createObject("input");
		
		p.addPassiveObject(inputObj);
		
		
		InputStream input = new FileInputStream(new File("cpp_examples/cppPassiveObjectReceive.txt"));			
		CPPParser parser = new CPPParser(input);
		parser.setProgram(p);
		parser.translation_unit();
		
		return p;
	}
	
	
	public static void main(String[] args) {
		try {
			
			System.out.println("Program parsed successfully.");
		
			SymbolicFactory.setEngine(new EngineChoco());
		
		
			
			//parse program
			//Program p = testAssignments();
			//Program p = testConditions();
			//Program p = testInput();
			//Program p = testReturn();
			//Program p = testWhile();
			//Program p = testSend();
			//Program p = testInputJava();
			//Program p = testTimerJava();			
			//Program p = testPassiveObjectDeclare();		
			//Program p = testPassiveObjectAssignToField();
			//Program p = testPassiveObjectAssignFromField();
			//Program p = testPassiveObjectSend();
			
			//Program p = testPassiveObjectReceive();
			
			//Program p = testIncarnate();
			Program p = testIfStatement();
			
			//execute symbolically
			SymbolicEngine eng = new SymbolicEngine(p);
			eng.execute();
			
			for (Execution e : eng.getResults()) {
				System.out.println("EXECUTION");
				System.out.println(e.toString());				
				
			}
			
			System.out.println("DONE");
			
			
		} catch (FileNotFoundException e) {
			System.out.println("JDA: FileNotFoundException in TestSuiteBasic.main.");
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println("JDA: ParseException in TestSuiteBasic.main.");
			e.printStackTrace();
		} 
	}

}
