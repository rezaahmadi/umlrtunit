package tests;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.Transition;
//import symbolic.execution.umlrt.actions.SymbolicExecutionAction;

public class StateGeneration {
	
	public static SymbolicState createState(String locationName){
		Location l = new Location(locationName);
		
		ValuationSymbolic vs = new ValuationSymbolic();
		
		//variable attribute2 = 12.0
		LocationVariable<Double> lv1 = new LocationVariable<Double>("attribute2",Double.class);
		Expression expr1 = SymbolicFactory.createConstant(Double.class,new Double(12.0));		
		vs.setValue(lv1, expr1);
		
		//variable attribute1 = 100
		LocationVariable<Integer> lv2 = new LocationVariable<Integer>("attribute1", Integer.class);
		Expression expr2 = SymbolicFactory.createConstant(Integer.class, new Integer(100));
		vs.setValue(lv2, expr2);
		
		//pc = {(!(in1var0 > 0))}
		ActionVariableInput<Integer> ai = new ActionVariableInput<Integer>("in1var0S", Integer.class);
		Constraint cs1 = SymbolicFactory.createRelationalConstraint(ai.getSymbolicVar(), CompareOperators.ge, SymbolicFactory.createConstant(Integer.class, new Integer(0)));
		Constraint cs2 = SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, cs1);
		
		List<Constraint> pc = new ArrayList<Constraint>();
		pc.add(cs2);
		
		return new SymbolicState(l,vs,pc, null, null);
	}
	
	public static List<SymbolicState> generateTheSame(){
		List<SymbolicState> result = new ArrayList<SymbolicState>();
		result.add(createState("AfterIn2"));
		return result;
	}
	
	public static List<SymbolicState> generateEmpty(){
		return new ArrayList<SymbolicState>();
	}

	public static SymbolicState createStateBasic() {
		return createState("AfterIn1");
		
	}
	public static List<SymbolicState> generateInputDiff(){
		List<SymbolicState> result = new ArrayList<SymbolicState>();
		
		//create dummy variable that is used to diff
		SymbolicVariable sv = SymbolicFactory.createSymbolicVariable("special");
		
		//create a transition
		//source
		Location source = new Location("First");
		//target
		Location target = new Location("AfterIn1");
		//name
		Transition t = new Transition("in1", source, target, null, null, null, null);
		
		//create symbolic execution action
		Map<ModelVariable<?>,SymbolicVariable> mapping = new HashMap<ModelVariable<?>, SymbolicVariable>();
		mapping.put(new ActionVariableInput<Integer>("in1var", Integer.class), sv);
		
		ActionInputSymbolic inputExec = new ActionInputSymbolic(new ActionInput("protocol1_1","in1", new HashSet<ActionVariableInput<?>>()),false,null);
		inputExec.setMappingInputVariables(mapping);
		
		//create symbolic transition
		SymbolicTransition st = new SymbolicTransition(t, inputExec, null);
		
		//create Valuation
		ValuationSymbolic vs = new ValuationSymbolic();
		
		
		//variable attribute2 = 12.0
		LocationVariable<Double> lv1 = new LocationVariable<Double>("attribute2",Double.class);
		Expression expr1 = SymbolicFactory.createConstant(Double.class,new Double(12.0));		
		vs.setValue(lv1, expr1);
		
		//variable attribute1 = special
		LocationVariable<Integer> lv2 = new LocationVariable<Integer>("attribute1", Integer.class);		
		vs.setValue(lv2, sv);
		
		//pc = {(special>0)}		
		Constraint cs1 = SymbolicFactory.createRelationalConstraint(sv, CompareOperators.ge, SymbolicFactory.createConstant(Integer.class, new Integer(0)));
		Constraint cs2 = SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, cs1);
		
		List<Constraint> pc = new ArrayList<Constraint>();
		pc.add(cs2);
		
		
		SymbolicState state = new SymbolicState(target,vs,pc, null, null);
		
		
		//set the incoming transition
		state.setIngoingArc(new ArcSymbolic(st, null, null));
		
		result.add(state);
		
		return result;
	}

}
