package tests;



import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.CallEvent;
//import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.Operation;
//import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.UMLFactory;

import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;
//import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.UpdateFunction;
import symbolic.execution.umlrt.model.ActionOutputSM;
import symbolic.execution.umlrt.model.ActionsOutputMap;
import symbolic.execution.umlrt.model.FunctionGenerator;

public class TestFunctionGeneration {
	
	static String code = 
		"int k = 0; " +
		"a = k;" +
		"int c; " +
		"c = *rtdata;" +
		"if (c>=a)" +
			"port.event(c).send();";
	
	static String guardCode = 
		"int k;" +
		"k=a;" +
		"b=*rtdata;" +
		"return k==b;";
	
	static String smallCode="int k; k=*rtdata;";
	
	static Set<LocationVariable<?>> locationVariables = new HashSet<LocationVariable<?>>();
	
	static ActionInput action;
	
	static ActionsOutputMap outputs = new ActionsOutputMap();
	
	
	private static void testActionCode(){
		FunctionGenerator fg = new FunctionGenerator(locationVariables,action,outputs,new HashSet<PassiveClass>());
		
		fg.executeActionCode(smallCode);
		
		OutputFunction of = fg.getOutputFunction();
		UpdateFunction uf = fg.getUpdateFunction();
		
		System.out.println("OUTPUT " + of);
		System.out.println("UPDATE " + uf);
	}
	
	public static void main(String[] args) {
		//set engine
		SymbolicFactory.setEngine(new EngineChoco());
		
		//set action and outputs
		setActions();
		
		//generate function
		testActionCode();
		
		//generate guard
		testGuardCode();
	
	}
	
	private static void testGuardCode() {
		FunctionGenerator fg = new FunctionGenerator(locationVariables, action, outputs, new HashSet<PassiveClass>());
		
		fg.executeGuardCode(guardCode);
		GuardFunction gf = fg.getGuardFunction();
		
		System.out.println("GUARD" + gf);
	}

	private static void setActions() {
		action = new ActionInput("p","in", new ActionVariableInput<Integer>("input", Integer.class));
		
		locationVariables.add(new LocationVariable<Integer>("a", Integer.class));
		locationVariables.add(new LocationVariable<Integer>("b",Integer.class));
		outputs = new ActionsOutputMap();
		Port port = UMLFactory.eINSTANCE.createPort();
		port.setName("port");
		CallEvent event = UMLFactory.eINSTANCE.createCallEvent();
		
		Operation oper = UMLFactory.eINSTANCE.createOperation();
		event.setOperation(oper);
		oper.setName("event");
		PrimitiveType type = UMLFactory.eINSTANCE.createPrimitiveType();
		type.setName("int");
		oper.createOwnedParameter("varOut", type);
		outputs.add(port, event, new ActionOutputSM(port, event, null));
		
	}	
	
	
}
