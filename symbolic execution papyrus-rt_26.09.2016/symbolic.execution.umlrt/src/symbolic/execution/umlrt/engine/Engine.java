package symbolic.execution.umlrt.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.papyrusrt.umlrt.core.utils.MessageUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.ProtocolUtils;

//import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.Capsule;

//import org.eclipse.papyrusrt.xtumlrt.common.Capsule;

//import lpg.runtime.NullExportedSymbolsException;

import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.RedefinableElement;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.Vertex;

//import symbolic.execution.code.parser.ParseException;
//import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
//import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.ExecutionEngine;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.TimerActionInput;
import symbolic.execution.ffsm.model.TimerActionOutput;
import symbolic.execution.ffsm.model.Transition;
import symbolic.execution.ffsm.model.Valuation;
import symbolic.execution.umlrt.model.ActionInputSM;
import symbolic.execution.umlrt.model.ActionOutputSM;
import symbolic.execution.umlrt.model.ActionsInputMap;
import symbolic.execution.umlrt.model.ActionsOutputMap;
import symbolic.execution.umlrt.model.LocationsVarMap;
import symbolic.execution.umlrt.model.SMValidationException;
import symbolic.execution.umlrt.model.VertexExplorable;
import symbolic.execution.utils.UmlrtUtil;

//import com.ibm.xtools.uml.rt.core.Capsule;
//import com.ibm.xtools.uml.rt.core.RTFactory;
//import com.ibm.xtools.uml.rt.core.RTInheritableEvent;
//import com.ibm.xtools.uml.rt.core.RTPortRedefinition;
//import com.ibm.xtools.uml.rt.core.RTRegionRedefinition;

public class Engine {
	
	private Class capsule;
	
	private StateMachine sm;
	
	private ActionsInputMap actionsInput;
	
	private ActionsOutputMap actionsOutput;
	
	private LocationsVarMap locationVars;
	
	private SymbolicExecutionTree tree;
	
	private Map<String, Vertex> states;
	
	private Map<Vertex, Location> locationsMapping;
	
	private FunctionalFiniteStateMachine ffsm;
	
	private Set<PassiveClass> passiveClasses = new HashSet<PassiveClass>();
	
	
	public Engine(Class capsule) throws SMValidationException {
		this.capsule = capsule;
//		sm = capsule.getPrimaryStateMachine().getReferenceTarget();
		sm = UmlrtUtil.getStateMachine (capsule);
		
		states = new HashMap<String, Vertex>();
		
		locationsMapping = new HashMap<Vertex, Location>();
				
		ffsm = new FunctionalFiniteStateMachine(capsule.getName());
		
		//4.add passive classes
		addPassiveClasses();
		
		//1. extract location variables
		addLocationVariables(ffsm);
		
				
		//2.extract input and output actions with variables
		addActions(ffsm);
		
		//3.find initial state
		setInitialLocation(ffsm);
		//System.out.println("Initial location : " + ffsm.getInitialLocation());
		
		
		//4.prepare map of states
		prepareStateList();
	}	

	//JDA: added methods to obtain the number of error and unknown nodes.
	
	private int numberOfErrorNodes = 0;
	private int numberOfUnknownNodes = 0;
	private int numberOfSubsumedNodes = 0;
	public int getNumberOfErrorNodes() {
		return numberOfErrorNodes;
	}
	public int getNumberOfUnknownNodes() {
		return numberOfUnknownNodes;
	}
	//Bug 18
	public int getNumberOfSubsumedNodes() {
		return numberOfSubsumedNodes;
	}
	private void addPassiveClasses() {
		
		//find the top level package
		Package topLevel = findTopLevel(capsule);
		
		addPassiveClasses(topLevel);
		
		
		ffsm.addPassiveClasses(passiveClasses);
		
		
		
	}
	
	private void addPassiveClasses(Package pkg){

		for (Element member : pkg.getOwnedMembers()){
			if (isPassiveClass(member)){
				org.eclipse.uml2.uml.Class cl = (org.eclipse.uml2.uml.Class) member;
				PassiveClass pc = createPassiveClass(cl);
				passiveClasses.add(pc);
			} else {
				if (member instanceof Package){
					addPassiveClasses((Package) member);
				}
			}
		}
	}

	private PassiveClass createPassiveClass(Class cl) {
		PassiveClass passive = new PassiveClass(cl.getName());
		
		
		for (Property p : cl.getAllAttributes()){
			java.lang.Class<?> typeClass = getTypeClass(p.getType());
			if (typeClass !=null){
				passive.addFieldDesc(p.getName(), getTypeClass(p.getType()));
			}
		}
		return passive;
	}

	private java.lang.Class getTypeClass(Type type) {
		if (type == null || type.getName() == null){
			return null;
		}
		if (type.getName().equals("int")){
			return Integer.class;
		}
		if (type.getName().equals("double")){
			return Double.class;
		}
		return null;
	}

	private boolean isPassiveClass(Element member) {
		//check if is a class
		if (member instanceof org.eclipse.uml2.uml.Class){
			//check if a capsule
			
			if (! UmlrtUtil.isCapsule((Class) member)){
				return true;
			}
		}
		return false;
	}

	private Package findTopLevel(NamedElement element) {
		if (element.getOwner() == null){
			return (Package)element;
		}
		return findTopLevel((NamedElement) element.getOwner());
	}

	public Engine(Class capsule, String initialStateName) throws SMValidationException {
		this.capsule = capsule;
		sm = UmlrtUtil.getStateMachine(capsule); //capsule.getPrimaryStateMachine().getReferenceTarget();
				
		states = new HashMap<String, Vertex>();
		
		locationsMapping = new HashMap<Vertex, Location>();
		
		
		ffsm = new FunctionalFiniteStateMachine(capsule.getName());
		
		//1. extract location variables
		addLocationVariables(ffsm);
		
		//2.extract input and output actions with variables
		addActions(ffsm);
		
		//4.prepare map of states
		prepareStateList();
		
		//3.find initial state
		setInitialLocation(ffsm, initialStateName);
		//System.out.println("Initial location : " + ffsm.getInitialLocation());
		
		
		
	}	

	
	private boolean isStateComposite(State s) {		
		
		
		if (s.getRegions()==null || s.getRegions().isEmpty()){
			return false;
		}
		Region region = s.getRegions().get(0);
		
		if (region ==null){
			return false;
		}
		Iterator<Vertex> iter = region.getSubvertices().iterator();
		
		while (iter.hasNext()){
			if (iter.next() instanceof State){
				return true;
			}
		}		
		return false;
	}


	public SymbolicExecutionTree execute() throws Exception{
		
		//start exploration
		ExecutionEngine se = new ExecutionEngine(ffsm);		
		try {
			tree = se.execute();
			numberOfErrorNodes = se.getNumberOfErrorNodes();
			numberOfUnknownNodes = se.getNumberOfUnknownNodes();
			//Bug 18.
			numberOfSubsumedNodes = se.getNumberOfSubsumedNodes();
		}catch (Throwable t){
			//JDA: begin change
				if (t instanceof IllegalArgumentException) {
					throw (IllegalArgumentException) t;
				}
				t.printStackTrace();
			//JDA: end change
		}
		
		
		//print results		
		//System.out.println(tree.toString());
		
		return tree;
	}
	
	public SymbolicExecutionTree execute(SymbolicState root, List<SymbolicState> explored) throws Exception{
		
		//start exploration
		ExecutionEngine se = new ExecutionEngine(ffsm);		
		try {
			tree = se.execute(root,explored);

		}catch (Throwable t){
			t.printStackTrace();
		}
		
		
		//print results		
		//System.out.println(tree.toString());
		
		return tree;
	}

	private void setInitialLocation(FunctionalFiniteStateMachine ffsm) throws SMValidationException {
		Region region = sm.getRegions().get(0);
		
		Location initialLocation = null;
		for (Vertex v : region.getSubvertices()){
			if (v instanceof Pseudostate){
				if (((Pseudostate)v).getKind().equals(PseudostateKind.INITIAL_LITERAL)){
					initialLocation = new Location(new VertexExplorable(v,sm, actionsInput, actionsOutput, locationsMapping));
					ffsm.addLocation(initialLocation);
					ffsm.setInitial(initialLocation);
					//System.out.println("Found initial");
					break;
				}
			}
		}
		
		if (initialLocation != null){
			initialLocation.explore(ffsm);
		} else{
			throw new SMValidationException("No initial state found.");
		}
	}
	
	private void setInitialLocation(FunctionalFiniteStateMachine ffsm, String name) throws SMValidationException{
		Vertex s = states.get(name);
		
		Location initialLocation = new Location(new VertexExplorable(s, sm, actionsInput, actionsOutput, locationsMapping));
		
		if (s != null && initialLocation != null){
			initialLocation.explore(ffsm);
		} else {
			throw new SMValidationException("No initial state found");
		}
		
	}

	private void addActions(FunctionalFiniteStateMachine ffsm) {
		actionsInput = extractInputActions(ffsm);	
		actionsOutput = extractOutputActions(ffsm);		
		//System.out.println("Actions added");
	}

	//TODO: JDA: (was KZ) add more types. JDA: Check this out.
	//JDA: added long
	private void addLocationVariables(FunctionalFiniteStateMachine ffsm) {
		Valuation initVal = new Valuation();
		locationVars = new LocationsVarMap();
		for (Property prop : capsule.getAllAttributes()){
			if (prop.getAggregation().equals(AggregationKind.NONE_LITERAL)){
				if (prop.getType() instanceof PrimitiveType){
					String typeName = ((PrimitiveType)prop.getType()).getName();
					
					//JDA: added long here
//					if(typeName.equals("long") || typeName.equals("int") || typeName.equals("bool")){
					if(typeName.equals("long") || typeName.equals("Integer") || typeName.equals("Boolean")){
						LocationVariable<Integer> locVar = new LocationVariable<Integer>(prop.getName(), Integer.class);
						ffsm.addLocationVariable(locVar);
						locationVars.add(prop.getName(), locVar);
						
						String def = prop.getDefault();	
						if (def!=null && !def.isEmpty()){
							initVal.setValue(locVar, new Integer(Integer.parseInt(def)));
						} else {
							initVal.setValue(locVar,0);
						}
					} else if (typeName.equals("double")){
						LocationVariable<Double> locVar = new LocationVariable<Double>(prop.getName(), Double.class);												
						ffsm.addLocationVariable(locVar);
						locationVars.add(prop.getName(), locVar);
						
						String def = prop.getDefault();
						initVal.setValue(locVar, new Double(Double.parseDouble(def)));
					} 
				} else {
					PassiveClass pc;
					//JDA: broke the following into separate try/catch regions with
					//JDA: error checking.
					try {
						//JDA: this is essentially unchanged.
						pc = ffsm.getPassiveClass(prop.getType().getName());
					}
					catch (NullPointerException np_ex){
						//JDA: output 'belated' syntax error and rethrow the exception.
						String msg = np_ex.getMessage();
						String typeStr = "";
						Type t = prop.getType();
						if (t != null) {
							typeStr = ", of type " + t.getName();
						}
						NullPointerException ne = new NullPointerException(
								"Syntax error.\n" +
								"Could not create create 'location variable' for passive object, " + prop.getName() +
								typeStr + ".\nMsg: " +
								np_ex.getLocalizedMessage() + ".");
						ne.fillInStackTrace();
						throw ne;
					}
					PassiveObject obj;
					try {
						obj = pc.createObject(prop.getName());
					}
					catch (NullPointerException np_ex) {
						//JDA: output 'belated' syntax error and rethrow the exception.
						String msg = np_ex.getMessage();
						NullPointerException ne = new NullPointerException(
								"Syntax error.\n" +
								"Could not create create 'location variable' for passive object, " + prop.getName() + ", of type " +
								prop.getType().getName() + ".\nMsg: " +
								np_ex.getLocalizedMessage() + ".");
						ne.fillInStackTrace();
						throw ne;
					}				
										
					//locationVars.add(locVarObj.getName(), locVarObj);
					//ffsm.addLocationVariable(locVarObj);
					
					//PassiveObject object = (PassiveObject) locVar.getSymbolicVar();
					//set initial
					for (String fieldName: pc.getFieldNames()){
						java.lang.Class type = pc.getType(fieldName);
						if (type.equals(Integer.class)){
							LocationVariable<Integer> locVar = new LocationVariable<Integer>(prop.getName()+"."+fieldName, Integer.class);
							locVar.setSymbolicVariable(obj.getVarField(fieldName));
							ffsm.addLocationVariable(locVar);
							
							
							locationVars.add(prop.getName(), locVar);
							
							String def = prop.getDefault();	
							if (def!=null && !def.isEmpty()){
								initVal.setValue(locVar, new Integer(Integer.parseInt(def)));
							} else {
								initVal.setValue(locVar,0);
							}
							
							locVar.getSymbolicVar().setOwner(obj);
						} else if (type.equals(Double.class)){
							LocationVariable<Double> locVar = new LocationVariable<Double>(prop.getName()+"."+fieldName, Double.class);
							locVar.setSymbolicVariable(obj.getVarField(fieldName));
							
							ffsm.addLocationVariable(locVar);
							locationVars.add(prop.getName(), locVar);
							
							String def = prop.getDefault();
							initVal.setValue(locVar, new Double(Double.parseDouble(def)));
							
							locVar.getSymbolicVar().setOwner(obj);
						}
						
					}
					
				}
			} 
		}
		
		ffsm.setInitialValuation(initVal);
		
		//System.out.println("Initial valuation " + initVal);
		
	}

	
	//TODO: JDA: (was KZ) what if there are multiplied ports !!
	//TOO: JDA: Test multiple ports.
	private ActionsInputMap extractInputActions(FunctionalFiniteStateMachine ffsm) {
		ActionsInputMap map = new ActionsInputMap();
		for (Port port: UmlrtUtil.getPorts(capsule)){
			List<Operation> events;			
			if (port.isConjugated()){	
				events = UmlrtUtil.getOutEvents(port); // port.getProtocol().getAllOutEvents();
			} else {
				events = UmlrtUtil.getInEvents(port); //port.getProtocol().getAllInEvents();					
			}
			
			for (Operation op : events){
				Port portSM = port; // just port, as all ports in the loop are owned by the current capsule //port.getLocalRedefinition();
				
				CallEvent eventSM =  UmlrtUtil.getCallEvent(op); //event.getReferenceTarget();
				ActionInput action = null;
				if (eventSM.getOperation().getName().equals("timeout")){
					action = new TimerActionInput(portSM.getName());
				} else {
					//JDA: Using try catch to throw appropriate exception.
					try {action = new ActionInputSM(portSM, eventSM,ffsm);}
					catch (NullPointerException ex) {
						throw new NullPointerException("Cannot create action input state machine for port, " +
								port.getName() + ", event, " + eventSM.getOperation().getName() + ".");
					}
				}
				
				ffsm.addInputAction(action);
				map.add(portSM,eventSM,action);
			}
			
		}
		return map;
	}
	
	private ActionsOutputMap extractOutputActions(FunctionalFiniteStateMachine ffsm) {
		ActionsOutputMap map = new ActionsOutputMap();
		for (Port port: UmlrtUtil.getPorts(capsule)){			
			
			List<Operation> events;		
			if (!port.isConjugated()){	
				events = UmlrtUtil.getOutEvents(port); // port.getProtocol().getAllOutEvents();
			} else {
				events = UmlrtUtil.getInEvents(port); //port.getProtocol().getAllInEvents();					
			}
			if (((Collaboration)port.getType()).getName().equals("Timing")){ //(port.getProtocol().getCollaboration().getName().equals("Timing")){
				Port portSM = port;  // just port, as all ports in the loop are owned by the current capsule //port.getLocalRedefinition();
				
				TimerActionOutput timerAction = new TimerActionOutput(portSM.getName());
				ffsm.addOutputAction(timerAction);
				//JDA: changed to "informIn"
				//map.add(portSM, "inform", timerAction);
				map.add(portSM, "informIn", timerAction);
				
				/*CallEvent event = UMLFactory.eINSTANCE.createCallEvent();
				Operation oper = UMLFactory.eINSTANCE.createOperation();
				Parameter param = UMLFactory.eINSTANCE.createParameter();
				PrimitiveType pt = UMLFactory.eINSTANCE.createPrimitiveType();
				pt.setName("int");
				param.setType(pt);
				//JDA -- changed to "informIn" from "inform"
				oper.setName("informIn");
				oper.getOwnedParameters().add(param);
				event.setOperation(oper);
				ActionOutputSM act = new ActionOutputSM(portSM, event);
				ffsm.addOutputAction(act);
				map.add(portSM,event,act);*/
			}
			
			for (Operation op : events){				
				Port portSM = port; //just port as all ports in the loop are owned ports of the current capsule //port.getLocalRedefinition();
				CallEvent eventSM =  UmlrtUtil.getCallEvent(op); //event.getReferenceTarget();
				ActionOutputSM act = new ActionOutputSM(portSM, eventSM,ffsm);
				ffsm.addOutputAction(act);
				map.add(portSM,eventSM,act);
			}
			
		}
		return map;
	}

	private void prepareStateList(){
		List<String> path = new ArrayList<String>();
		StateMachine stm = UmlrtUtil.getStateMachine(capsule);
		for(Region r : stm.getRegions()){
			Region region = r; //MyUmlrtUtil.getLocallyRedefinedRegion(r, stm); //r.getLocalRedefinition();
			for(Vertex vertex : region.getSubvertices()){				
				findStates(vertex, path);
			}
		}
		
		//for (String key : states.keySet()){
		//	System.out.println("Mappring " + key + " -" + states.get(key).getName());
		//}
	}

	public Set<String> getStateList() {		
		return states.keySet();
	}

	private void findStates(Vertex vertex, List<String> path) {
		if (vertex instanceof State){
			State state = (State) vertex;			
			path.add(state.getName());			
			
			if (!isStateComposite(state)){
				states.put(buildName(path),state);
			} else {
				for(Region r : state.getRegions()){
					for(Vertex v : r.getSubvertices()){
						findStates(v, path);
					}
				}
			}			
			path.remove(path.size()-1);
		}
		
		if (vertex instanceof Pseudostate){
			if (((Pseudostate)vertex).getKind().equals(PseudostateKind.INITIAL_LITERAL)){
				path.add("Initial");
				states.put(buildName(path), vertex);
				path.remove(path.size()-1);
			}
		}
		
	}

	private String buildName(List<String> path) {
		StringBuffer sb = new StringBuffer();
		for(String s : path){
			sb.append(s);
			sb.append(":");
		}
		return sb.substring(0,sb.length()-1);
	}

	/*public ResultsReachability findReachable(String sourceName, String targetName,
			boolean isSymbolic) {
		
		ResultsReachability results = new ResultsReachability(isSymbolic, sourceName, targetName);
		//check all reachable
		for(String path : states.keySet()){
			Vertex v = states.get(path);
			
			boolean reachable = false;
			if (path.equals("Initial")){
				reachable = true;
			} else {
				if (locationsMapping.containsKey(v)){				
					Location l = locationsMapping.get(v);
					if (tree.hasLocation(l)){
						reachable = true;
					}
				}
			}
			if (!reachable){
				results.addUnreachable(v);
				results.addUnreachableString(path);
			}			
		}
				
		//check symbolic between states
		if (!results.isUnreachable(sourceName) && !results.isUnreachable(targetName)){
			Location lSource = locationsMapping.get(states.get(sourceName));
			Location lTarget = locationsMapping.get(states.get(targetName));
			
			System.out.println("looking for " + lSource +" "+ lTarget);
			List<SymbolicPath> paths = tree.findPaths(lSource, lTarget);
			
			System.out.println("Paths " + paths);
			results.setSymbolicPaths(paths);
		}
		
		//make concrete if necessary
		if (!isSymbolic){
			results.makeConcrete();
		}
		
		return results;
	}*/

	/*public List<String> getOutputSignalList() {		
		return actionsOutput.getSignalNames();
	}*/

	/*public ResultsOutput findPathsForOutput(String[] signals, boolean isSymbolic) {
		ResultsOutput results = new ResultsOutput(signals, isSymbolic);
		//prepare set of actions output
		Set<ActionOutput> outputSignals = findOutputActions(signals);
		
		//System.out.println("LOOKING for signals " + outputSignals);
		
		//search the tree
		List<SymbolicPath> paths = tree.findPathsForSignals(outputSignals);
		System.out.println(" Paths " + paths);
		
		results.setSymbolicPaths(paths);
		
		if (!isSymbolic){
			results.makeConcrete();
		}
		return results;
	}*/

	/*private Set<ActionOutput> findOutputActions(String[] signalNames) {
		Set<ActionOutput> result = new HashSet<ActionOutput>();
		
		
		System.out.println(actionsOutput.toString());
		
		for (String signalName : signalNames){
			String[] portAndSignal = signalName.split("[.]");
			
			System.out.println("PORT " + portAndSignal[0]);
			System.out.println("SIGNAL " + portAndSignal[1]);
			if (portAndSignal.length==2){
				result.add(actionsOutput.get(portAndSignal[0], portAndSignal[1]));
			}
		}
		return result;
	}*/

	/*public ResultsTestCases generateTestCases() {
		ResultsTestCases results = new ResultsTestCases();
		
		//ask the tree
		List<SymbolicPath> paths = tree.exploreAllPaths();
		results.setSymbolicPaths(paths);
		
		//solve
		results.makeConcrete();
		
		return results;
	}*/

	public SymbolicExecutionTree getTree() {		
		return tree;
	}
	
	@Override
	public String toString() {		
		return capsule.getName();
	}

	public Set<ActionOutput> getOutputActions(Port port) {
		return actionsOutput.getActionsForPort(port.getName());		
	}

	public ActionInput getActionInput(Port port, CallEvent signal) {
		return actionsInput.get(port, signal);
	}

	public Set<ActionInput> getInputActions(Port port) {		
		return actionsInput.get(port);
	}

	public ActionInput getActionInputFromString(String port, String signal) {
		return actionsInput.getActionFromString(port,signal);
		
	}

	public Set<ActionInput> getAllInputActions() {
		return actionsInput.getAllActions();
	}

	public Set<ActionOutput> getAllOutputActions() {
		return actionsOutput.getAllActions();
		
	}

	public Location getLocationFromName(String stateName) {
		Location l = ffsm.getLocationByNameExplorable(stateName);
		if (l==null){
			l = ffsm.getLocationByName(stateName);
		}
		return l;
		
	}

	public ActionOutput getActionOutputFromString(String port, String signal) {
		return actionsOutput.getAction(port,signal);		
	}

	public LocationVariable getAttribute(String name) {
		LocationVariable var = ffsm.getLocationVariable(name);
		return var;
	}

	public SymbolicState parse(SymbolicState state) {
		//find location
		Location loc = ffsm.getLocationByNameExplorable(state.getLocation().getName());
		
		//create valuations
		ValuationSymbolic val = parseValuation(state.getValuation());
		
		return new SymbolicState(loc,val, state.getPC(), null,null);
		
	}

	private ValuationSymbolic parseValuation(ValuationSymbolic valuation) {
		ValuationSymbolic parsed = new ValuationSymbolic();
		for (ModelVariable<?> var : valuation.getVariables()){
			ModelVariable<?> varParsed = ffsm.getLocationVariable(var.getName());
			parsed.setValue(varParsed, valuation.getValue(var));
		}
		return parsed;
	}

	public void parseTimers(SymbolicState rootParsed, List<String> timers) {
		for (String timer : timers){
			TimerActionOutput tao = getTimerAction(timer);
			if (tao != null)
				rootParsed.addSetTimer(tao);
		}
		
	}

	private TimerActionOutput getTimerAction(String timer) {
		//JDA: Changed to "informIn"
		ActionOutput ao = actionsOutput.get(timer, "informIn");
		if (ao != null && ao instanceof TimerActionOutput){
			return (TimerActionOutput) ao;
		}
		return null;
	}

	public List<SymbolicState> parse(List<SymbolicState> explored) {
		List<SymbolicState> result = new ArrayList<SymbolicState>(); 
		for (SymbolicState state : explored){
			result.add(parseFull(state));
		}
		return result;
	}

	private SymbolicState parseFull(SymbolicState state) {
		//find location
		Location loc = parseLocation(state.getLocation());
		
		//create valuations
		ValuationSymbolic val = parseValuation(state.getValuation());
		
		//parse transition
		SymbolicTransition st = parseTransition(state.getIngoingTransition());
		
		//parse existing timers
		List<TimerActionOutput> timers = parseTimers(state.getTimers());
		
		SymbolicState parsed = new SymbolicState(loc,val, state.getPC(), null,timers);
		
		if (st!=null)
			parsed.setIngoingArc(new ArcSymbolic(st, null, null));
		
		return parsed;
		
	}

	private List<TimerActionOutput> parseTimers(List<TimerActionOutput> timers) {
		if (timers==null){
			return null;
		}
		List<TimerActionOutput> result = new ArrayList<TimerActionOutput>();
		
		for(TimerActionOutput timer : timers){
			result.add(parseTimer(timer.getName()));
		}
		return result;
	}

	private TimerActionOutput parseTimer(String name) {
		return getTimerAction(name);
	}

	private SymbolicTransition parseTransition(SymbolicTransition st) {
		if (st==null){
			return null;
		}
		Transition transition = st.getTransition();
		
		Transition parsed = new Transition(transition.getName(), 
				parseLocation(transition.getSource()),
				parseLocation(transition.getTarget()),null,null,null,null);
		
		return new SymbolicTransition(parsed, st.getInputExecution(), null);
		
	}

	private Location parseLocation(Location loc) {
		Location locParsed = ffsm.getLocationByNameExplorable(loc.getName());
		
		if (locParsed==null){
			Vertex v = states.get(loc.getName());
			locParsed = new Location(new VertexExplorable(v, sm, actionsInput, actionsOutput, locationsMapping));
		}
		return locParsed;
	}

	public ActionOutput getActionOutput(Port port, CallEvent signal) {		
		return actionsOutput.getAction(port.getName(), signal.getName());
	}

}
