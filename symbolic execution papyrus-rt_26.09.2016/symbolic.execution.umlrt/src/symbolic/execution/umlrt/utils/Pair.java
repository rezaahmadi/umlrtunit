package symbolic.execution.umlrt.utils;

public class Pair<R,S> {
	private R first;
	private S second;
	
	public Pair(R first, S second){
		this.first = first;
		this.second = second;
	}
	
	public R getFirst(){
		return first;
	}
	
	public S getSecond(){
		return second;
	}
	
	@Override
	public String toString() {		
		return "(" + first + "," + second + ")";
	}
}
