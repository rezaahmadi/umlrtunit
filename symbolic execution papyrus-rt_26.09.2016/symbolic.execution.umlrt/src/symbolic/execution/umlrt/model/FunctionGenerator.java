package symbolic.execution.umlrt.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.lang.IllegalArgumentException;

import symbolic.execution.code.engine.Execution;
import symbolic.execution.code.engine.SendAction;
import symbolic.execution.code.engine.SymbolicEngine;
//import symbolic.execution.code.parser.ParseException;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.Assignments;
//import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.OutputSequence;
import symbolic.execution.ffsm.model.UpdateFunction;

public class FunctionGenerator {

	private Set<LocationVariable<?>> locationVariables;
	private ActionInput action;
	
	private String paramsString;
	
	private Set<Execution> actionCodeResults;
	private Set<Execution> guardCodeResults;
	
	private Map<LocationVariable<?>, SymbolicVariable> locationVars;
	
	private Map<ActionVariableInput<?>, SymbolicVariable> inputVars;
	
	private ActionsOutputMap actionsOutput;
	
	private Set<PassiveClass> passiveClasses;
	
	/* =================================================================================================
	 * JDA: Debug input and improvement in expressibility of exception handling.
	==================================================================================================== */
	public FunctionGenerator(Set<LocationVariable<?>> locationVariables, ActionInput action, 
			ActionsOutputMap actionsOutput, Set<PassiveClass> passiveClasses){
		this.locationVariables = locationVariables;
		this.action = action;
		this.actionsOutput = actionsOutput;
		
		this.paramsString = createParams();		
				
		this.locationVars = createProgramVars();				
		this.inputVars = createInputVars(action);		
		this.passiveClasses = passiveClasses;
		
	}
	
	private  Map<ActionVariableInput<?>, SymbolicVariable> createInputVars(ActionInput action) {	
		 Map<ActionVariableInput<?>,SymbolicVariable> result = 
			 new HashMap<ActionVariableInput<?>, SymbolicVariable>();
		
		if (action !=null){
			for (ActionVariableInput<?> var: action.getVars()){	
				result.put(var, var.getSymbolicVar());
				
			}
		}
		
		return result;
	}

	private Map<LocationVariable<?>, SymbolicVariable> createProgramVars() {
		Map<LocationVariable<?>, SymbolicVariable> result = 
			new HashMap<LocationVariable<?>, SymbolicVariable>();
		for (LocationVariable<?> locVar : locationVariables){										
			result.put(locVar,locVar.getSymbolicVar());
		}
		
		return result;
	}

	private String createParams() {
		if (locationVariables.isEmpty()){
			return "()";
		}
		StringBuffer sb = new StringBuffer();
		
		for (LocationVariable<?> var : locationVariables){
			String type = getTypeName(var.getType());
			if (type != null && !var.getName().contains(".")){
			sb.append(type);
			sb.append(" ");
			sb.append(var.getName());
			sb.append(",");
			}
		}
		if (sb.length() <= 1){
			return "()";
		}
		return "(" + sb.substring(0,sb.length()-1) + ")";
	}

	private String getTypeName(Class<?> type){
		if (type.equals(Integer.class)){
			return "int";
		}
		if (type.equals(Double.class)){
			return "float";
		}
		if (type.equals(Byte.class)){
			return "byte";
		}
		return null;
	}

	public void executeActionCode(String code){
		if (code.isEmpty()){
			return;
		}
		
		//create function signature
		String functionCode = createFunctionCode(code);
		
		//System.out.println("Executing symb : \n" + functionCode);
				
		//send code to symbolic execution
		Set<SymbolicVariable> locVars = new HashSet<SymbolicVariable>(locationVars.values());
		Set<SymbolicVariable> inVars = new HashSet<SymbolicVariable>(inputVars.values());
		
		
		SymbolicEngine engine = new SymbolicEngine(functionCode, locVars, inVars, passiveClasses);
		//System.out.println("engine created" + engine);
		try {
			engine.execute();
			actionCodeResults = engine.getResults();
			//System.out.println(actionCodeResults);
		}catch (Throwable t){
			//JDA: Made the following more expressive.
			System.out.println("JDA: Throwable in FunctionGenerator.executeActionCode:" + t);
			//JDA: added conditional rethrow.
			if (t instanceof IllegalArgumentException) {
				t.printStackTrace();
				throw (IllegalArgumentException) t;
			}
			if (t instanceof java.lang.ArithmeticException) {
				throw (java.lang.ArithmeticException) t;
			}
		}
		
				
		//System.out.println("!!!!!!!!!!!!!!" + actionCodeResults.toString());		
		
		
	}
	
	public UpdateFunction getUpdateFunction(){
		UpdateFunction uf = new UpdateFunction("",action.getVars(), locationVariables);
		for (Execution e : actionCodeResults){
			//prepare constraint
			Set<Constraint> caseConstraints = new HashSet<Constraint>(e.getPC());
			
			//prepare valuation
			Assignments caseExpressions = new Assignments(locationVariables);
			for (LocationVariable<?> locVar : locationVariables){
				//System.out.println("!!!" + e.getValuation());
				//System.out.println("!!!" + e.getValuation().getValue(programVars.get(locVar)));
				if (locVar.isObject()){
					
				} else {
				caseExpressions.assign(locVar, e.getValuation().getValue(locationVars.get(locVar)));
				}
			}
			
			uf.addCase(caseConstraints, caseExpressions);
		}
		
		return uf;
	}
	
	private Constraint createFromPC(List<Constraint> pc) {
		Constraint result = SymbolicFactory.createTrue();
		for(Constraint cst : pc){
			result = SymbolicFactory.createLogicalConstraint(result, LogicalOperators.and, cst);
		}
		return result;
	}

	public OutputFunction getOutputFunction(){
		OutputFunction of = new OutputFunction("", action.getVars(), locationVariables);
		//JDA: debug
		//System.out.println("getOutputFunction--actionCodeResults: " + actionCodeResults);
		for (Execution e : actionCodeResults){
			//JDA: debug
			//System.out.println("JDA: getOutputFunction--item, e, of actionCodeResults: " + e);
			//prepare constraint
			Set<Constraint> caseConstraints = new HashSet<Constraint>(e.getPC());
			
			//prepare output sequence
			OutputSequence outputSeq = new OutputSequence();
			//JDA: debug
			//System.out.println("getOutputFunction--e.getOutputSequence: " + e.getOutputSequence());
			for (SendAction send : e.getOutputSequence()){
				//JDA: debug
				//System.out.println("JDA: Item send of e: " + e.getOutputSequence());
				String port = send.getPort();
				String signal = send.getSignal();
				Expression output = send.getExpr();
				
				//System.out.println("LOOKING FOR OUTPUT " + port + "; " + signal);
				
				ActionOutput action = actionsOutput.get(port,signal);
				if (action == null){
					//JDA: This caused a null pointer exception. Modified to throw the exception explicitely.
					NullPointerException npe = new NullPointerException(
							"FunctionGenerator.getOutputFunction: port/signal Not found: " + "port=" + port +"," +
							"action=" + action +", signal:" +signal);
					throw npe;
				}
				ActionOutputInstance actionInstance = new ActionOutputInstance(action);
				
				for (ActionVariableOutput<?> var : action.getVars()){
					actionInstance.setVariableMapping(var, output);
					if (output instanceof PassiveObject){
						PassiveObject outputObj = (PassiveObject) output;
						PassiveObject varObj = (PassiveObject) var.getSymbolicVar();
						
						for (String fieldName : varObj.getFieldNames()){
							actionInstance.setSymbolicVariableMapping(varObj.getVarField(fieldName), 
									outputObj.getValue(fieldName));
						}
					} else {
						actionInstance.setSymbolicVariableMapping(var.getSymbolicVar(), output);
					}
				}
				
				outputSeq.addItem(actionInstance);
				
			}
			//System.out.println("JDA: FunctionGenerator.getOutputFunction: calling of.addcase.\n");
			//System.out.println("JDA:    caseConstraints =[" + caseConstraints + "].");
			//System.out.println("JDA:    outputSeq =[" + outputSeq + "].");
			of.addCase(caseConstraints, outputSeq);
		}
		//System.out.println("!!!!!!!" + of);
		//System.out.println("JDA: FunctionGenerator.getOutputFunction: calling of.addcase.\n");
		//System.out.println("JDA:    returning of = [" + of + "].");
		return of;
	}
	
	private String createFunctionCode(String code) {
		return "void eval" + paramsString + "{\n" + code + "\n}"; 		
	}

	public void executeGuardCode(String code){
		if (code == null || code.isEmpty()){
			return;
		}
		String functionCode = createFunctionCode(code);
				
		Set<SymbolicVariable> locVars = new HashSet<SymbolicVariable>(locationVars.values());
		Set<SymbolicVariable> inVars = new HashSet<SymbolicVariable>(inputVars.values());
		SymbolicEngine engine = new SymbolicEngine(functionCode, locVars, inVars, passiveClasses);
		//System.out.println("engine created");
		try {
			engine.execute();
			guardCodeResults = engine.getResults();
		}catch (Throwable t){
			//JDA: modified this one to be more expressive.
			System.out.println("JDA: Throwable in FunctionGenerator.executeGuardCode" + t);	
		}
	}
	
	public GuardFunction getGuardFunction(){
		GuardFunction gf = new GuardFunction("", action.getVars(), locationVariables);
		
		for (Execution e : guardCodeResults){
			Expression expr = e.getOutput();
			if (!(expr instanceof Constraint)){
				expr = SymbolicFactory.createRelationalConstraint(expr, CompareOperators.neq, SymbolicFactory.createConstant(Integer.class, 0));
			}
			if (e.getPC().isEmpty()){				
				 gf.addConstraint((Constraint)expr);
			} else {
				Constraint cst = createFromPC(e.getPC());
				gf.addConstraint(SymbolicFactory.createLogicalConstraint(cst, LogicalOperators.imply, expr));
			}
		}
		return gf;
	}
	
}
