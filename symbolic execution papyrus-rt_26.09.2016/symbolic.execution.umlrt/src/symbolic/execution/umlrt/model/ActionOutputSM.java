package symbolic.execution.umlrt.model;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Type;

import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;

public class ActionOutputSM extends ActionOutput {
	private Port port;
	private CallEvent signal;
	
	public ActionOutputSM(Port port, CallEvent signal, FunctionalFiniteStateMachine ffsm) {
		super(port.getName(),signal.getOperation().getName());
		this.port = port;
		this.signal = signal;
		
		vars = getOutputVars(signal.getOperation(), ffsm);
		
		for (ActionVariableOutput<?> var : vars){
			var.setAction(this);			
		}
	}

	private Set<ActionVariableOutput<?>> getOutputVars(Operation operation, FunctionalFiniteStateMachine ffsm) {
		Set<ActionVariableOutput<?>> result = new HashSet<ActionVariableOutput<?>>();		
		for (Parameter param : operation.getOwnedParameters()){
			Type type = param.getType();
			if (type == null){
				ActionVariableOutput<Object> var = new ActionVariableOutput<Object>(getSymVarName(param), Object.class);
				result.add(var);
				continue;
			}
			if (type instanceof PrimitiveType){
				PrimitiveType pType = (PrimitiveType) type;
				if (pType.getName().equals("int")){
					ActionVariableOutput<Integer> var = new ActionVariableOutput<Integer>(getSymVarName(param), Integer.class);
					result.add(var);
				} else if(pType.getName().equals("double")){
					ActionVariableOutput<Double> var = new ActionVariableOutput<Double>(getSymVarName(param), Double.class);
					result.add(var);
				} else {
					ActionVariableOutput<Object> var = new ActionVariableOutput<Object>(getSymVarName(param), Object.class);
					result.add(var);
				}
			} else {
			//get passive class
				PassiveClass pc = ffsm.getPassiveClass(type.getName());
				//JDA: added try/catch/throw
				ActionVariableOutput<PassiveClass> var;
				try {
					var =  new ActionVariableOutput<PassiveClass>(getSymVarName(param), PassiveClass.class, pc);
				}
				catch (NullPointerException ex) {
					throw new IllegalArgumentException("Could not create action variable output, " +
							getSymVarName(param)+", for passive class, " + pc + ".");
				}
				result.add(var);
			}
		}
		
		return result;
	}
	
	String getSymVarName(Parameter p) {
		return p.getName() + "var";
	}

	public CallEvent getSignal() {		
		return signal;
	}
	
	public String getSignalString(){
		return signal.getName();
	}
	
	public String getPortString(){
		return port.getName();
	}
}
