package symbolic.execution.umlrt.model;

import java.util.HashSet;
import java.util.Set;

//import lpg.runtime.NullExportedSymbolsException;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Type;

import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
//import symbolic.execution.ffsm.model.ModelVariable;

public class ActionInputSM extends ActionInput{	
	private Port port;
	private CallEvent signal;

	
	
	public ActionInputSM(Port portSM, CallEvent eventSM, FunctionalFiniteStateMachine ffsm) {
		super(portSM.getName(),eventSM.getOperation().getName());
		this.port = portSM;
		this.signal = eventSM;
		
		//create variables
		this.vars = getInputVars(eventSM.getOperation(), ffsm) ;
		for (ActionVariableInput<?> var : vars){
			var.setAction(this);
		}
		
	}


	//TODO: JDA: (was KZ) must add more types
	//TODO: JDA: Check whether more types are needed here.
	private Set<ActionVariableInput<?>> getInputVars(Operation operation, FunctionalFiniteStateMachine ffsm) {
		Set<ActionVariableInput<?>> result = new HashSet<ActionVariableInput<?>>();		
		for (Parameter param : operation.getOwnedParameters()){
			Type type = param.getType();
			if (type == null){
				ActionVariableInput<Object> var = new ActionVariableInput<Object>(getSymVarName(param), Object.class);
				result.add(var);
				continue;
			}
			if (type instanceof PrimitiveType){
				PrimitiveType pType = (PrimitiveType) type;
//				if (pType.getName().equals("int")){
				if (pType.getName().equals("Integer")){
					ActionVariableInput<Integer> var = new ActionVariableInput<Integer>(getSymVarName(param), Integer.class);
					result.add(var);
				} else if(pType.getName().equals("double")){
					ActionVariableInput<Double> var = new ActionVariableInput<Double>(getSymVarName(param), Double.class);
					result.add(var);
				}else if(pType.getName().equals("Boolean")){
					ActionVariableInput<Boolean> var = new ActionVariableInput<Boolean>(getSymVarName(param), Boolean.class);
					result.add(var);
				}
				else {
					ActionVariableInput<Object> var = new ActionVariableInput<Object>(getSymVarName(param), Object.class);
					result.add(var);
				}
			} else {
				//get passive class
				PassiveClass pc = ffsm.getPassiveClass(type.getName());	
				//JDA: Changed to try/catch
				ActionVariableInput<PassiveClass> var;
				try {
					var =  new ActionVariableInput<PassiveClass>(getSymVarName(param), PassiveClass.class, pc);
				}
				catch (NullPointerException ex) {
					throw new IllegalArgumentException("Could not create action input variable, " + getSymVarName(param) +
							"of type, " + type.getName() + ".");
				}
				result.add(var);
			}
		}
		
		return result;
	}
	
	String getSymVarName(Parameter p) {
		return p.getName() + "var";
	}



	public ActionInputSM(String name) {
		super("",name);
	}


	public CallEvent getSignal() {
		return signal;
	}
	
		
}
