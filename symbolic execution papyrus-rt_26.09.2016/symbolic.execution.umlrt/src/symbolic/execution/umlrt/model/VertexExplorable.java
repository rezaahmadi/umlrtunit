package symbolic.execution.umlrt.model;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;

//import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.TransitionKind;
import org.eclipse.uml2.uml.Vertex;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.Explorable;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.Location;
//import symbolic.execution.umlrt.engine.Engine;
import symbolic.execution.umlrt.utils.Pair;

public class VertexExplorable extends Explorable {
	private Vertex vertex;
	private StateMachine sm;	
	private ActionsInputMap actions;
	private ActionsOutputMap actionsOutput;
	private Map<Vertex, Location> locationsMapping;
	private boolean isHistory = false;
	
	private boolean isExplored = false;
	
	public VertexExplorable(Vertex vertex, StateMachine sm, ActionsInputMap actions,
			ActionsOutputMap actionsOutput, Map<Vertex, Location> locationsMapping){
		this.vertex = vertex;
		this.sm = sm;
		this.actions = actions;
		this.actionsOutput = actionsOutput;
		this.locationsMapping = locationsMapping;
		this.isHistory = isHistory();
	}
	
	
	private boolean isHistory() {
		if (vertex instanceof Pseudostate){
			Pseudostate ps = (Pseudostate) vertex;
			boolean isComposite = isContainerComposite(ps);
			if (isComposite && ps.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)){
				return ps.getOutgoings().isEmpty();
			}
		}
		return false;
	}


	private boolean isContainerComposite(Pseudostate ps) {
		State s = ps.getState();
		if (s == null){
			return false;
		}
		
		Region region = s.getRegions().get(0);
		
		if (region ==null){
			return false;
		}
		Iterator<Vertex> iter = region.getSubvertices().iterator();
		
		while (iter.hasNext()){
			if (iter.next() instanceof State){
				return true;
			}
		}		
		return false;
	}


	@Override
	public void explore(FunctionalFiniteStateMachine ffsm) {
		locationsMapping.put(vertex,location);
		ffsm.addLocation(location);
		if (isHistory){
			return;
		}
		Set<ActionInput> usedActions = new HashSet<ActionInput>();
		// find all outgoing transitions
		exploreVertex(ffsm, vertex, usedActions);
		
		// find transitions up in hierarchy
		Vertex parent = getParent(vertex);
		while (parent != null){
			exploreVertex(ffsm, parent, usedActions);
			parent = getParent(parent);
		}
		
		
		isExplored = true;
	}

	private Vertex getParent(Vertex v){
		Vertex parent = null;
		
		Region r = v.getContainer();
		if (r!=null){
			parent = r.getState();
		}
		return parent;
	}
	
	private void exploreVertex(FunctionalFiniteStateMachine ffsm, Vertex v, Set<ActionInput> used){
		Set<ActionInput> usedForThisVertex = new HashSet<ActionInput>();
		//System.out.println("!!! Exploring " + v.getName());
		//System.out.println("!!! used " + used);

		for (Transition t: findOutgoingTransitions(v)){	
			
			//System.out.println("!!!! Exploring transition " + t.getName());
			//for each transition chain			
			for (TransitionChain chain : findTransitionChains(t, new TransitionChain())){
				if (chain.getTriggeringEvents().isEmpty() && !chain.isInitial()){
					continue;
				}
				//System.out.println("CHAIN " + chain);				
				//create locations for targets
				Location targetLocation = null;
				boolean newLocation = false;
				
				if(chain.isInternal()){
					targetLocation = location; 
				} else {
					Vertex targetVertex = chain.getTargetState();
					if (locationsMapping.containsKey(targetVertex)){
						targetLocation = locationsMapping.get(targetVertex);
					} else {					
					 	targetLocation = new Location(new VertexExplorable(targetVertex, sm, actions,actionsOutput,locationsMapping));
					 	locationsMapping.put(targetVertex, targetLocation);
					 	newLocation = true;
					}
				}
				
				//create default transition for initial location
				boolean added = false;
				if (location.equals(ffsm.getInitialLocation())){
					TransitionSM transition = new TransitionSM(new DefaultActionInput(), 
							location, targetLocation, chain, ffsm, actionsOutput );
					ffsm.addTransition(transition);
					//System.out.println("JDA: VertexExplorable.exploreVertex: after ffsm.addTransition: transition: [" +
					//		transition + "].");
					added = true;
				}
				
				//for other locations
				for (Pair<Port,CallEvent> action : chain.getTriggeringEvents()){
					//get input action
					ActionInput actionInput = actions.get(action.getFirst(), action.getSecond());
					//if not already considered
					if (!used.contains(actionInput)){
						usedForThisVertex.add(actionInput);
						TransitionSM transition = new TransitionSM(actionInput,location, targetLocation, chain, ffsm, actionsOutput );
						//add transitions to ffsm
						ffsm.addTransition(transition);
						//System.out.println("JDA: VertexExplorable.exploreVertex: after ffsm.addTransition: transition: [" +
						//		transition + "].");
						added = true;
					}
					
				}
				if (added && newLocation){
					ffsm.addLocation(targetLocation);
					//System.out.println("JDA: VertexExplorable.exploreVertex: after ffsm.addLocation: targetLocation: [" +
					//		targetLocation + "].");
				}
			}
		}	
		//System.out.println("!!!!Used for this vertex " + usedForThisVertex);
		used.addAll(usedForThisVertex);
	}
	
	private List<Transition> findOutgoingTransitions(Vertex vertex){
		List<Transition> result = new ArrayList<Transition>();
		
		try {
			Object os = vertex.getOutgoings();
			result.addAll(vertex.getOutgoings());
			if (vertex instanceof State){
				State state = (State) vertex;
				
				for (Pseudostate point : state.getConnectionPoints()){
					if (point.getKind().equals(PseudostateKind.EXIT_POINT_LITERAL) || 
							point.getKind().equals(PseudostateKind.EXIT_POINT)){
						result.addAll(point.getOutgoings());
					}				
					if (point.getKind().equals(PseudostateKind.ENTRY_POINT) ||
							point.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)){
						if (!point.getIncomings().isEmpty()){
							continue;
						}
						for (Transition t : point.getOutgoings()){
							if (t.getKind().equals(TransitionKind.LOCAL) || 
									t.getKind().equals(TransitionKind.INTERNAL) ||
											t.getKind().equals(TransitionKind.LOCAL_LITERAL) ||
												t.getKind().equals(TransitionKind.INTERNAL_LITERAL)){
								result.add(t);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return result;
	}
	
	private List<TransitionChain> findTransitionChains(Transition first, TransitionChain chain){
		List<TransitionChain> result =  new ArrayList<TransitionChain>();
		chain.addTransition(first);		
		
		if (first.getTarget() instanceof State){
			result.add(chain);
			return result;
		} else if (first.getTarget() instanceof Pseudostate){
			Pseudostate pseudo = (Pseudostate) first.getTarget();
			
			switch(pseudo.getKind()){
			case ENTRY_POINT_LITERAL:
				if (pseudo.getOutgoings().size()==0){
					result.add(chain);
				}
				
			case EXIT_POINT_LITERAL:
				if (chain.isInternal()){
					result.add(chain);
				}
				
			case JUNCTION_LITERAL:				
				for (Transition t : pseudo.getOutgoings()){										
					result.addAll(findTransitionChains(t, new TransitionChain(chain)));
				}
				
				break;			
			case CHOICE_LITERAL:
				for (Transition t : pseudo.getOutgoings()){										
					result.addAll(findTransitionChains(t, new TransitionChain(chain)));
				}
				break;
			default: result.add(chain);
			}
		}
		return result;
	}
	
	@Override
	public boolean isExplored() {
		return isExplored;
	}

	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (isHistory){
			return "HISTORY";
		}
		if (vertex instanceof Pseudostate){
			sb.append(((Pseudostate)vertex).getKind().toString());
			sb.append(":");
		}
		if (vertex!=null && vertex.getName() != null){
			sb.append(vertex.getName());
		}
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof VertexExplorable){
			return vertex.equals(((VertexExplorable)o).vertex);
		}
		return false;
	}


	public Vertex getVertex() {		
		return vertex;
	}


	@Override
	public String getName() {
		StringBuffer name = new StringBuffer();
		Vertex vertex = this.vertex;
		name.append(vertex.getName());
		/*while (getParent(vertex) != null ){
			vertex = getParent(vertex);
			name.insert(0,"::");
			name.insert(0, vertex.getName());
		}*/
		return name.toString();
	}
	
}
