package symbolic.execution.umlrt.model;

import java.util.HashMap;
import java.util.Map;

import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.model.LocationVariable;

public class LocationsVarMap {
	
	private Map<String, LocationVariable<?>> map;
	
	public LocationsVarMap(){
		map = new HashMap<String, LocationVariable<?>>();
	}
	
	public void add(String name, LocationVariable<?> var){
		map.put(name, var);
	}

	public LocationVariable<?> getVariable(String name){
		return map.get(name);
	}

	public Map<String, SymbolicVariable> getIntegerMapping() {
		Map<String, SymbolicVariable> mapping = new HashMap<String, SymbolicVariable>();
		
		for (String var : map.keySet()){
			if (map.get(var).getType().equals(Integer.class)){
				mapping.put(var, map.get(var).getSymbolicVar());
			}
		}
		
		return mapping;
	}
	
}
