package symbolic.execution.umlrt.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Port;

import symbolic.execution.ffsm.model.ActionInput;

public class ActionsInputMap {
	
	private Map<Port, Map<CallEvent, ActionInput>> mapping;
	
	public ActionsInputMap(){
		mapping = new HashMap<Port, Map<CallEvent,ActionInput>>();
	}

	public void add(Port portSM, CallEvent eventSM, ActionInput actionInputSM) {
		Map<CallEvent, ActionInput> portMap = mapping.get(portSM); 
		if (portMap == null){
			portMap = new HashMap<CallEvent, ActionInput>();			
			mapping.put(portSM, portMap);
		}
		portMap.put(eventSM, actionInputSM);
	}

	public ActionInput get(Port port, CallEvent signal) {
		Map<CallEvent,ActionInput> map =  mapping.get(port);
		if (map!=null){
			for (CallEvent ce : map.keySet()){
				if (ce.getOperation().getName().equals(signal.getOperation().getName())){
					return map.get(ce);
				}
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Port port : mapping.keySet()){
			for (CallEvent event : mapping.get(port).keySet()){
				sb.append(mapping.get(port).get(event).toString());
				sb.append(";");
			}
		}
		return sb.toString();
	}

	public Set<ActionInput> get(Port port) {
		
		return new HashSet<ActionInput>(mapping.get(port).values());
	}

	public ActionInput getActionFromString(String port, String signal) {
		for (Port p : mapping.keySet()){
			if (p.getName().equals(port)){
				for (CallEvent s : mapping.get(p).keySet()){
					if (s.getOperation().getName().equals(signal)){
						return mapping.get(p).get(s);
					}
				}
			}
		}
		return null;
	}

	public Set<ActionInput> getAllActions() {
		Set<ActionInput> result = new HashSet<ActionInput>();
		for (Port p : mapping.keySet()){
			for (CallEvent ce : mapping.get(p).keySet()){
				result.add(mapping.get(p).get(ce));
			}
		}
		return result;
	}

}
