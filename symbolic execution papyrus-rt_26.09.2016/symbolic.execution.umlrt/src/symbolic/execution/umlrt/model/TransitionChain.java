package symbolic.execution.umlrt.model;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.TransitionKind;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Vertex;
import symbolic.execution.umlrt.utils.Pair;

/* ==============================================================================================
History:
    Commented out unnecessary code. April 22, 2016.

	Bugs 5 and 9: The methods 'gatherCode' and 'gatherCode' was moved into 'TransitionSM' and
	              collected into a single function. This was done to be able to deal with the
	              late evaluation of guards on or after the first condition point in the
	              chain. April 21, 2016.
	Documentation cleanup. April 17, 2016.
================================================================================================ */
public class TransitionChain {
	private List<Transition> chain;
	
	private boolean isInternal;
	
	private String name = null;
	
	public TransitionChain(){
		chain = new ArrayList<Transition>();
	}
	
	public TransitionChain(Transition t){
		chain = new ArrayList<Transition>();
		chain.add(t);
		isInternal = isInternal(t);
	}
	
	private boolean isInternal(Transition t) {
		return t.getKind().equals(TransitionKind.INTERNAL_LITERAL) || 
		t.getKind().equals(TransitionKind.LOCAL_LITERAL);
	}

	public TransitionChain(TransitionChain chain){
		this.chain = new ArrayList<Transition>(chain.chain);
		this.isInternal = chain.isInternal;
	}

	public List<Pair<Port,CallEvent>> getTriggeringEvents(){
		List<Pair<Port,CallEvent>> result = new ArrayList<Pair<Port,CallEvent>>();
		for (Transition t : chain){
			for (Trigger trigger : t.getTriggers()){								
				for (Port p : trigger.getPorts()){
					Event event = trigger.getEvent();
					if (event instanceof CallEvent){
						CallEvent call = (CallEvent) event;							
						result.add(new Pair<Port, CallEvent>(p,call));
					}
				}
			}
		}
				
		return result;
	}
	
	public Vertex getTargetState(){
		//if internal - target is the same as source
		if (isInternal){
			return null;
		}
		Vertex v = chain.get(chain.size()-1).getTarget();
		if (v instanceof State){
			return v;
		}
		if (v instanceof Pseudostate){
			Pseudostate pseud = (Pseudostate)v;
			if (pseud.getKind().equals(PseudostateKind.DEEP_HISTORY_LITERAL)){
				return v;
			}
			if (isContainerComposite(pseud) && pseud.getOutgoings().isEmpty()){
				return v;
			}
			return ((Pseudostate)v).getState();
		}
		return null;
	}

	private boolean isContainerComposite(Pseudostate ps) {
		State s = ps.getState();
		if (s == null){
			return false;
		}
		Region region = s.getRegions().get(0);
		
		if (region ==null){
			return false;
		}
		Iterator<Vertex> iter = region.getSubvertices().iterator();
		
		while (iter.hasNext()){
			if (iter.next() instanceof State){
				return true;
			}
		}		
		return false;
	}

	public void addTransition(Transition t) {		
		chain.add(t);
		
		if (t.getKind().equals(TransitionKind.INTERNAL_LITERAL) ||
				t.getKind().equals(TransitionKind.LOCAL_LITERAL)){
			if (t.getSource().getOwner() == t.getTarget().getOwner()){
				isInternal = true;
			}
			
		}
		
	}

	/* JDA remove
	private Vertex getParent(Vertex v){
		Vertex parent = null;
		
		Region r = v.getContainer();
		if (r!=null){
			parent = r.getState();
		}
		return parent;
	}
	JDA: end remove */
	

	//JDA: Bugs 5 and 9 this functionality was moved to TransitionSM
	/*
	public String gatherCode(Vertex source) {
		StringBuffer sb = new StringBuffer();
		
		Vertex chainSource = getSourceState();	
		if (chainSource instanceof Pseudostate){
			chainSource = ((Pseudostate)chainSource).getState();
		}
		while (source != chainSource){
			if (source instanceof State){
				if (!isInternal)
					sb.append(getStateExitCode((State)source));
				source = getParent(source);
			} else {
				source = ((Pseudostate)source).getState();
			}
		}
		
		for (Transition t : chain){
			//JDA: The "\n", below ensures that the first line of the next
			//JDA: transition will not be lost if the previous transition
			//JDA: ends in an in-line comment that is not terminated by an EOL.
			//JDA: When the transition, t, is from a 'condition' pseudo-state, prepend an assertion.
			//JDA: The assertion should be formed from the guard condition of the pseudo-state.
			String guard = getTransitionGuard(t);
			sb.append(getTransitionCode(t) + "\n");
		}
		
		return sb.toString();
	}
	*/

    //JDA: Bugs 5 and 9 this functionality was moved to TransitionSM
	private String getTransitionCode(Transition t) {
		StringBuffer sb = new StringBuffer();
		//check source state
		Vertex source = t.getSource();
		if (source instanceof State){
			State sourceState = (State) source;
			if (!isInternal)
				sb.append(getStateExitCode(sourceState));
		} else if (source instanceof Pseudostate){
			Pseudostate sourcePseudo = (Pseudostate) source;
			if (sourcePseudo.getKind().equals(PseudostateKind.EXIT_POINT_LITERAL)){
				if (!isInternal)
					sb.append(getStateExitCode(sourcePseudo.getState()));
			}
		}
		
		//transition effect
		sb.append(getBehaviorCode(t.getEffect()));
		
		//check target state
		Vertex target = t.getTarget();
		if (target instanceof State){
			State targetState = (State) target;
			if (!isInternal)
				sb.append(getStateEntryCode(targetState));
		} else if (source instanceof Pseudostate){
			Pseudostate targetPseudo = (Pseudostate)target;
			if (targetPseudo.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)){
				if (!isInternal)
					sb.append(getStateEntryCode(targetPseudo.getState()));
			}
		}
		
		return sb.toString();
	}

	private String getStateEntryCode(State targetState) {
		Behavior b = targetState.getEntry();
		return getBehaviorCode(b);
	}

	private String getStateExitCode(State sourceState) {
		Behavior b = sourceState.getExit();				
		return getBehaviorCode(b);
	}
	
	private String getBehaviorCode(Behavior b){
		StringBuffer sb = new StringBuffer();
		
		if (b instanceof OpaqueBehavior){
			OpaqueBehavior ob = (OpaqueBehavior) b;			
			for (String s :ob.getBodies()){
				sb.append(s);
			}
		}		
		return sb.toString();
	}

	//JDA: added the following
	private boolean isFromConditionNode(Transition t) {
		Vertex s = t.getSource();
		
		boolean result = false;
		if (s instanceof Pseudostate) {
			Pseudostate s2 = (Pseudostate) s;
			PseudostateKind kind = s2.getKind();
			if (kind ==PseudostateKind.CHOICE_LITERAL) {
				result = true;
			}
		}
		return result;
	}
	
	//JDA: begin new section
	public boolean sourceIsACondition(int index) {
		Transition t = chain.get(index);
		boolean result = isFromConditionNode(t);
		return result;
	}
	public int size() {
		int result = chain.size();
	    return result;
	}
	public String getCode(int index) {
		String result = null;
		Transition t = chain.get(index);
		result = getTransitionCode(t) + "\n";
		return result;
	}
	public String getConditionGuard(int index) {
		String result = new String("");
		Transition t = chain.get(index);
		if (isFromConditionNode(t)) {
			result = getTransitionGuard(t);
		}
		return result;
	}
	public String getNonConditionGuard(int index) {
		String result = new String("");
		Transition t = chain.get(index);
		if (!isFromConditionNode(t)) {
			result = getTransitionGuard(t);
		}
		return result;
	}
	
	public ArrayList<String> gatherGuardCode() {
		//JDA: the following is new.
		ArrayList<String> result = new ArrayList<String>();
		for (Transition t : chain){
			//StringBuffer sb = new StringBuffer();
			String guard = getTransitionGuard(t);
			//JDA: proposed replacement
			if (!isFromConditionNode(t)) 
			{
				result.add(guard);
			}
		}
		return result;
	}
	//JDA: end new section.
	
	//JDA: This code is no longer used.
	/*
	public String gatherGuardCode() {
		StringBuffer sb = new StringBuffer();
		//JDA: the following is new.
		StringBuffer newSb = new StringBuffer();
		for (Transition t : chain){
			
			String guard = getTransitionGuard(t);
			if (sb.length()>0 && !guard.isEmpty()){
				String condition = guard.replace("return", " && (").replace(";", ");");
				String old = sb.toString().replace("return", "return (").replace(";", ")"+condition);
				sb = new StringBuffer(old);
			} else {
				sb.append(guard);
			}
			//JDA: proposed replacement
			if (!isFromConditionNode(t)) 
			{
				//JDA: The guards from a condition pseudo-state are evaluated
				//JDA: when the pseudo state is reached. The guards from other
				//JDA: states and pseudo-states are evaluated when a message
				//JDA: arrives.
				if (newSb.length()>0 && !guard.isEmpty()){
					String condition = guard.replace("return", " && (").replace(";", ");");
					String old = newSb.toString().replace("return", "return (").replace(";", ")"+condition);
					newSb = new StringBuffer(old);
				} else {
					newSb.append(guard);
				}
			}
		}
		
		return sb.toString();
	}
*/
	private String getTransitionGuard(Transition t) {
		String s = "";
		if (t.getGuard()!=null){
			s= t.getGuard().getSpecification().stringValue();
		}
		return s;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Transition transition : chain){
			sb.append(transition.getName());
			sb.append("-");
		}
		
		return sb.toString();
	}

	public boolean isInternal() {			
		return isInternal;
	}

	public boolean isInitial() {		
		Vertex source = getSourceState();
		if(source instanceof Pseudostate){
			Pseudostate ps = (Pseudostate)source;
			if (ps.getKind().equals(PseudostateKind.INITIAL_LITERAL)){
				return true;
			}
		}
		return false;
	}
	
	public Vertex getSourceState(){
		if (chain.isEmpty()){
			return null;
		}
		return chain.get(0).getSource();
	}

	public void createName() {
		StringBuffer nameBuffer = new StringBuffer();
		for (Transition t : chain){
			nameBuffer.append(t.getName());
			nameBuffer.append(":");
		}
		name = nameBuffer.substring(0, nameBuffer.length()-1);
	}
	
	public String getName() {
		if (name == null)
			createName();
		return name;
	}
	
}
