package symbolic.execution.umlrt.model;

import symbolic.execution.ffsm.model.ActionInput;
//import symbolic.execution.ffsm.model.ActionVariableInput;

public class DefaultActionInput extends ActionInputSM {

	public DefaultActionInput() {
		super("default");
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ActionInput)
			return ((ActionInput)o).getName().equals("default");
		return false;
	}
}
