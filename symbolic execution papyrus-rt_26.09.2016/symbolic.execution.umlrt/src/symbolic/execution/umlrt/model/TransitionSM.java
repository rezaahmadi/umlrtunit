package symbolic.execution.umlrt.model;
/* ==============================================================================================
 History:
 	Bugs 5 and 9: Changes done, April 2016. Together bugs 5 and 9 cover changes needed to
 	              cope with choice points. The fixes for bug 5 only worked when there
 	              were no guards in the transition chain after the guard of the first
 	              choice point. The fixes for bug 9 removed this restriction.

 	              The original SE generation assumed treated choice points as junction points.
 	              This was wrong because the guards of choice points and subsequent guards
 	              of the associated segment are supposed to be evaluated when the choice
 	              point is reached.
 ================================================================================================ */
import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
import java.util.Set;

//import org.eclipse.jface.bindings.Trigger;

import symbolic.execution.code.model.cpp.LookupConstraints;
import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.functions.NoOutputFunction;
import symbolic.execution.ffsm.functions.NoUpdateFunction;
import symbolic.execution.ffsm.functions.TrueGuardFunction;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.Location;
//import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.Transition;
//import symbolic.execution.ffsm.model.UpdateFunction;

public class TransitionSM extends Transition {
	private TransitionChain chain;
	
	private String code = new String();
	
	private int segmentCount = 0;
	// The following counts the number of guards in the first segment.
	
	private int guardCount = 0;
	// The following is the the array of guard code function bodies for the first segment.
	// It is used determine whether the first segment has no guards.
	private ArrayList<String> guardCodeArray = new ArrayList<String>();
	// The following is the guard function for the first segment.
	private GuardFunction guardFunction;

	@Override
	    public String getCode() {
	    	return code;
	    }
		/* =================================================================================================
		finishSegment:
		
		This code collects common code needed to complete the processing of a segment of the transition
		chain. The chain is broken into segments at condition points.
		
		'codeBuffer' represents the code and 'workingGuardFunction' the guards collected for the segment.
		
		When a segment begins with a choice point, a lookup assert is used to collect the guards of
	    the segment. The lookup assert is injected into the generated code immediately before the
	    code of the segment.
			
	    A lookup assert is needed rather than an assert because the guards are parsed into Choco-style
		conditions that are not easily changed to C++ boolean expressions. Moreover, it seems pointless
		to transform to C++ and transform it back to a Choco-style expression (which might be more complex than
		the original). Instead, we save the Choco-style expression and look it up during code evaluation.
		==================================================================================================== */
		private void finishSegment(StringBuffer codeBuffer, GuardFunction workingGuardFunction, FunctionalFiniteStateMachine ffsm) {
			// Begin new segment. Note: a segment is a portion of transition chain beginning at
			// a state or condition pseudo-state. The first segment begins at a state. This
			// is characterized by 'segmentCount == 0'.
			if (segmentCount == 0) {
				// We are at the initial state.
				code = codeBuffer.toString();
				if (workingGuardFunction == null) {
					workingGuardFunction = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
				}
				guardFunction = workingGuardFunction;
			}
			else {
				// Here we need to create a "LookupAssert' from the working guard function,
				// append it to the code. Then append the code buffer.
				if (workingGuardFunction != null) {
					Set<Constraint> cons = workingGuardFunction.getGuardConstraints();
					int conIndex = LookupConstraints.addConstraints(cons);
					String lookupConstraintString = "\n$assert$placeholder(" + conIndex + ");\n";
					code = code + lookupConstraintString + codeBuffer.toString();
				}
				else {
					code = code + "\n" + codeBuffer.toString();
				}	
			}
			segmentCount++;
		}

		/* =================================================================================================
		gatherCodeAndGuards:
		
		This function collects and augments code, and guards. The functionality was moved here from
		TransitionChain.java and modified to take into account choice point guards as well as other guards. 
		==================================================================================================== */
		private void gatherCodeAndGuards(ActionsOutputMap actionsOutput, FunctionalFiniteStateMachine ffsm) {
			int sz = chain.size();
			
			//StringBuffer guardBuffer = new StringBuffer();
			StringBuffer codeBuffer = new StringBuffer();
			GuardFunction workingGuardFunction = null;
			for (int i = 0; i < sz; i++) {
				// The treatment of guards and code depends very much on whether the current item in 'chain'
				// is a condition pseudo-state.
				if (chain.sourceIsACondition(i)) {
					// Begin new segment.
					finishSegment(codeBuffer, workingGuardFunction, ffsm);
					// Reset 'workingGuardFunction' and 'codeBuffer'.
					workingGuardFunction = null;
					codeBuffer = new StringBuffer();
					String gstr = chain.getConditionGuard(i);
					String codeStr = chain.getCode(i).trim();
					if (! gstr.equals("")) {
						FunctionGenerator fg = new FunctionGenerator(ffsm.getLocationVariables(), inputAction, actionsOutput, ffsm.getPassiveClasses());
						fg.executeGuardCode(gstr);
						GuardFunction tmpGf = fg.getGuardFunction();
						tmpGf.getGuardConstraints();
						Set<Constraint> cons = tmpGf.getGuardConstraints();
						int conIndex = LookupConstraints.addConstraints(cons);
						String lookupConstraintString = "\n$assert$placeholder(" + conIndex + ");\n";
						codeBuffer.append(lookupConstraintString);
						codeBuffer.append("  //constraints: [" + cons + "]\n");
						codeBuffer.append(codeStr + "\n");
						//System.out.print("");
					}
					else {
					   throw new IllegalArgumentException("\nERROR: Tree generator cannot create update and output functions for code:\n" +
							code + "\nCause: Missing guard in branch from condition pseudo-node");
					}
				}
				else {
					String codeStr = chain.getCode(i).trim();
					codeBuffer.append("\n" + codeStr);
					String ncstr = chain.getNonConditionGuard(i);
					//System.out.println("JDA: gstr = [" + gstr + "].");
					if (! ncstr.equals("")) {
						if (segmentCount == 0)  {
							guardCount++;
						}
						FunctionGenerator fg = new FunctionGenerator(ffsm.getLocationVariables(), inputAction, actionsOutput, ffsm.getPassiveClasses());
						fg.executeGuardCode(ncstr);
						GuardFunction tmpGf = fg.getGuardFunction();
						if (segmentCount == 0) {
						   guardCodeArray.add(tmpGf.toString());
						}
						if (workingGuardFunction == null) {
							workingGuardFunction = tmpGf;
						}
						else {
							Set<Constraint> cons = tmpGf.getGuardConstraints();
							for (Constraint con: cons) {
								workingGuardFunction.addConstraint(con);
							}
						}
					}
				}
			}
			finishSegment(codeBuffer, workingGuardFunction, ffsm);
		}
			
	public TransitionSM(ActionInput action, Location source, Location target, TransitionChain chain, 
			FunctionalFiniteStateMachine ffsm, ActionsOutputMap actionsOutput) {		
		super(chain.getName());
		
		this.chain = chain;
		
		//source location
		this.source = source;
				
		//target location
		this.target = target;
		
		//input action 
		this.inputAction = action;		
		
		gatherCodeAndGuards(actionsOutput, ffsm);
		
		//System.out.println("TransitionSM--code: " + code);
		//System.out.println("TransitionSM--guard code:" + guardCode);
		
		if ((code !=null && !code.isEmpty()) || (guardCount > 0) ) {
			FunctionGenerator fg = new FunctionGenerator(ffsm.getLocationVariables(), inputAction, actionsOutput, ffsm.getPassiveClasses());
			if (code == null || code.isEmpty()){
				this.uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
				this.of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
			} else {			
				fg.executeActionCode(code);
				//JDA: Added abort.
				if (inputAction == null) {
					throw new IllegalArgumentException("\nERROR: Tree generator cannot create update and output functions for code:\n" +
							code + "\nProbable cause: Use of unsupported constructs in the code.");
				}
				this.uf = fg.getUpdateFunction();
				this.of = fg.getOutputFunction();
			}
		
			if (guardCount == 0){
				this.gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
			} else
			{
				this.gf = guardFunction;
			}
		} else {
			this.uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
			this.of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
			this.gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		}
	}

	@Override
	public String toString() {		
		return chain.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
}