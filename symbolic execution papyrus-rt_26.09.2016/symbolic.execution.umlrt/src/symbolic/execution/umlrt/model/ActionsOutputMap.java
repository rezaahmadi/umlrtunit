package symbolic.execution.umlrt.model;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Port;

import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.TimerActionOutput;

public class ActionsOutputMap {
	
	private Map<String, Map<String, ActionOutput>> map;

	public ActionsOutputMap(){
		map = new HashMap<String, Map<String,ActionOutput>>();
	}
	
	public void add(Port port, CallEvent event, ActionOutputSM act) {
		String portName = port.getName();
		
		Map<String, ActionOutput> portMap = map.get(portName);
		if (portMap == null){
			portMap = new HashMap<String, ActionOutput>();
			map.put(portName, portMap);
		}
		
		String signalName = event.getOperation().getName();
		portMap.put(signalName, act);
	}
	
	@Override
	public String toString() {		
		return map.toString();
	}

	public ActionOutput get(String port, String signal) {
		if (map.get(port) != null){
			return map.get(port).get(signal);
		}
		return null;
	}

	public List<String> getSignalNames() {	
		List<String> result = new ArrayList<String>(); 
		
		for (String port : map.keySet()){
			for(String signal : map.get(port).keySet()){
				result.add(port +"."+signal);
			}
		}		
		return result;
	}

	public Set<ActionOutput> getActionsForPort(String name) {
		if (map.containsKey(name)){
			return new HashSet<ActionOutput>(map.get(name).values());
		} else {
			return new HashSet<ActionOutput>();
		}
	}

	public void add(Port port, String signalName, TimerActionOutput timerAction) {
		String portName = port.getName();
		
		Map<String, ActionOutput> portMap = map.get(portName);
		if (portMap == null){
			portMap = new HashMap<String, ActionOutput>();
			map.put(portName, portMap);
		}
		
		portMap.put(signalName, timerAction);
		
	}

	public Set<ActionOutput> getAllActions() {
		Set<ActionOutput> result = new HashSet<ActionOutput>();
		for (String p : map.keySet()){
			for (String s : map.get(p).keySet()){
				result.add(map.get(p).get(s));
			}
		}
		return result;
	}

	public ActionOutput getAction(String port, String signal) {
		if (map.containsKey(port)){
			return map.get(port).get(signal);
		}
		return null;
		
	}

	public ActionOutput getAction(Port port, CallEvent signal) {
		return null;
	}

}
