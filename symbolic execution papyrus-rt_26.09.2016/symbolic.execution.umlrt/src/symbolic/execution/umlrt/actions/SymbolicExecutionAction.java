package symbolic.execution.umlrt.actions;

//import java.io.ByteArrayInputStream;
//import java.io.File;
import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.Map.Entry;

import javax.swing.JOptionPane;

//import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
//import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
//import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
//import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
//import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
//import org.eclipse.emf.ecore.resource.URIConverter;
//import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
//import org.eclipse.emf.transaction.TransactionalEditingDomain;
//import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
//import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsulePartUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
//import org.eclipse.jface.viewers.StructuredSelection;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.widgets.Display;
//import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.RedefinableElement;
import org.eclipse.uml2.uml.Stereotype;

//import org.eclipse.jface.dialogs.MessageDialog;

import symbolic.execution.code.model.cpp.TimerStatement;
import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.ffsm.model.Location;
import symbolic.execution.serialization.set.SET;
import symbolic.execution.set.serialization.transform.CSETTransform;
import symbolic.execution.tree.graph.SETEditor;
import symbolic.execution.tree.graph.SETEditorInput;
//import symbolic.execution.tree.synch.QueueManager;
//import symbolic.execution.tree.synch.SynchronizationAction;
import symbolic.execution.tree.synch.Synchronizer;
//import symbolic.execution.tree.synch.TreeConnector;
//import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.RelayActions;
import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;

import symbolic.execution.umlrt.engine.Engine;
import symbolic.execution.umlrt.model.ActionInputSM;
import symbolic.execution.umlrt.model.ActionOutputSM;
//import tests.StateGeneration;
import symbolic.execution.utils.UmlrtUtil;

//import com.ibm.xtools.uml.redefinition.PortRedefinition;
//import com.ibm.xtools.uml.redefinition.PropertyRedefinition;
//import com.ibm.xtools.uml.rt.core.Class;
//import com.ibm.xtools.uml.rt.core.RTCapsulePart;
//import com.ibm.xtools.uml.rt.core.RTConnectorRedefinition;
//import com.ibm.xtools.uml.rt.core.RTFactory;
//import com.ibm.xtools.uml.rt.core.RTPortRedefinition;
//import com.ibm.xtools.uml.rt.core.RTFactory.RTRedefFactory;
//import com.ibm.xtools.uml.rt.core.internal.uml.RTPortImpl;

/**
 * Our sample action implements workbench action delegate.
 * The action proxy will be created by the workbench and
 * shown in the UI. When the user tries to use the action,
 * this delegate will be created and execution will be 
 * delegated to it.
 * @see IWorkbenchWindowActionDelegate
 */
public class SymbolicExecutionAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	
	//private Diagram diagram;
	
	SymbolicExecutionTree containerTree;
	
	private Class capsuleContainer;
	
	//private Map<Property, String> partTrees;
	
	private Map<Property, SymbolicExecutionTree> partTrees;
	
	//JDA: changed spelling 'Tress' -> 'Trees'
	private Map<String, SymbolicExecutionTree> capsuleTrees;
	
	private Map<Object, Engine> engines;
	
	
	private boolean modular;
	/*===================================================================================
	 JDA: Added the following. This controls whether only the 'Open' symbolic execution
	      is generated. In this case the symbolic execution of capsules contained
	      directly or indirectly within the capsule are not generated and
	      the synchronised symbolic execution tree is not generated.
	 ==================================================================================== */
	private boolean generateOnlyOpen;
	
	private Resource capsuleResource;
	
	/**
	 * The constructor.
	 */
	public SymbolicExecutionAction() {
		SymbolicFactory.setEngine(new EngineChoco());
		initMaps();
		modular = true;
		//JDA: Added the following.
		generateOnlyOpen = false;
	}
	
	private void initMaps(){
		
		partTrees = new HashMap<Property, SymbolicExecutionTree>();
		capsuleTrees = new HashMap<String, SymbolicExecutionTree>();
		SymbolicFactory.setEngine(new EngineChoco());
		
		engines = new HashMap<Object, Engine>();
	}

	
	public SET executeSymbolicOpen(Class capsule, IFile file){
		this.capsuleContainer = capsule;
		
		SymbolicExecutionTree tree = executeSymbolic(capsule, null, true);
		
		SET result = serializeToFile(tree, file);
		
		return result;
	}
	
	public SymbolicExecutionTree run4TestGen(Class capsuleContainer) {
		System.out.println("********* Symbolic Execution and Tree Serialization begin ******************");
		SymbolicState.stopSynchronizing();
		SymbolicState.stopSerializing();
		if (capsuleContainer != null) {
			TimerStatement.setOldSE(true);
			initMaps();

			// execute symbolically container
			long startTime = System.currentTimeMillis();
			MemoryMeasureThread t = new MemoryMeasureThread();
			t.start();

			containerTree = executeSymbolic(capsuleContainer, null, false);
			/*******************************************/
			t.stopMeasure();
			long endTime = System.currentTimeMillis();
			System.out.println("*******************TIME " + (endTime - startTime));
			// JDA: begin modification to avoid NullPointerException
			if (containerTree == null) {
				System.out.println("JDA: SymbolicExecutionAction.run: the tree is empty.");
				return null;
			}
			if (containerTree.getFFSM() == null) {
				System.out.println("JDA: SymbolicExecutionAction.run: FFSM is empty.");
				return null;
			}

//			serializeTree(containerTree);

			// JDA: Added the following to assist debugging.
			System.out.println("!!!Symbolic Execution, Tree Displaying, and Tree Serialization end.");
			
		}
		return containerTree;
	}
	
	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		//JDA: Moved the following into the if-then.
		//initMaps();
		// JDA: Added the following.
		System.out.println("********* Symbolic Execution and Tree Serialization begin ******************");
		//JDA: added the following
		SymbolicState.stopSynchronizing();	
		SymbolicState.stopSerializing();
		if (capsuleContainer !=null){
			/* JDA ===========================================================================
			 * Note: The following empties 'capsuleTrees'. That makes the
			 * 'modular' flag useless because the symbolic execution tree for the given
			 * capsule is never in it, so it can't be gotten from it.
			 ===============================================================================*/
			//JDA: Added the following to protect changes to common code in 'TimerStatement'
			TimerStatement.setOldSE(true);
			//JDA: Moved here.
			initMaps();
			//JDA: End move.
			
			/* JDA: The following was 'originally' commented out ============================ */
			/*final MessageDialog dialog = new MessageDialog(
					window.getShell(),
					"Symbolic Exceution",
					null,
					"Executing capsule " + ((capsuleContainer != null) ? capsuleContainer.getUML2Class().getName() : "null") + "...",
					MessageDialog.INFORMATION,
					new String[]{}, 0
					);		
			*/
			// JDA: end of 'originally' commented out region ================================ */
			
			//execute symbolically container
			long startTime = System.currentTimeMillis();
			MemoryMeasureThread t = new MemoryMeasureThread();
			t.start();
			
			/* JDA: The following was 'originally' commented out ============================ */
			/*********TESTING FROM STATE******************/
			/*SymbolicState s = StateGeneration.createStateBasic();
			List<String> timers = new ArrayList<String>();
			//timers.add("timer");
			SymbolicExecutionTree containerTree = executeSymbolicFromState(capsuleContainer, s, timers, StateGeneration.generateInputDiff());
			*/	
			/*******************************************/
			// JDA: end of 'originally' commented out region ================================ */
			
			containerTree = executeSymbolic(capsuleContainer, null, true);
			/*******************************************/
			t.stopMeasure();
			long endTime = System.currentTimeMillis();
			System.out.println("*******************TIME " + (endTime - startTime));
			//JDA: begin modification to avoid NullPointerException
				if (containerTree == null) {
					System.out.println("JDA: SymbolicExecutionAction.run: the tree is empty.");
					return;
				}
				if (containerTree.getFFSM() == null) {
					System.out.println("JDA: SymbolicExecutionAction.run: FFSM is empty.");
					return;
			}
			//JDA: end modification
			/* JDA: The following was 'originally' commented out ============================ */
			/*long startTimeTC = System.currentTimeMillis();
			List<SymbolicPath> sps = containerTree.exploreAllPaths();
			System.out.println("Number of test cases " + sps.size());
			for (SymbolicPath sp : sps){
				//System.out.println("solving");
				sp.solve();
			}
			long endTimeTC = System.currentTimeMillis();
			
			
			System.out.println("*******************TIME  TC " + (endTimeTC - startTimeTC));*/
			
			//dialog.setBlockOnOpen(false);
			//dialog.open();
			// JDA: end of 'originally' commented out region ================================ */
			
			/* JDA: Commented out the dialog to assist debugging ================================= @/
			MessageDialog dialogDone = new MessageDialog(
					window.getShell(),
					"Symbolic Execution",
					null,
					"Executing capsule " + ((capsuleContainer != null) ? capsuleContainer.getUML2Class().getName() : "null") + "... Done \n"
					+ "Symbolic states : " + containerTree.getNumberOfStates(),
					MessageDialog.INFORMATION,
					new String[]{"Done"}, 0
					);
				dialogDone.open();
			/@ JDA: End of commented out dialog. ================================================ */
			
			//transform tree to file
				
			serializeTree(containerTree);
			
			//JDA: It appears that at some point trees were displayed here. This is no longer the case.
			//System.out.println("!!!Displaying tree");
			
			// JDA: Added the following to assist debugging.
			System.out.println("!!!Symbolic Execution, Tree Displaying, and Tree Serialization end.");
		}
	}

	public SymbolicExecutionTree getSET(){
		return containerTree;
	}
	
	private void serializeTree(SymbolicExecutionTree containerTree) {
		
		
		//create file
		URI uriModel = capsuleResource.getURI();	
		IResource resourceModel = ResourcesPlugin.getWorkspace().getRoot().findMember(uriModel.toPlatformString(true));		
		if (!(resourceModel instanceof IFile)){
			//JDA: When would this case happen?
			return;
		}
		IFile modelFile = (IFile) resourceModel;
		IProject project = modelFile.getProject();
		String suff = "";
		//JDA: added the following 'if' for filename suffix.
		if (containerTree instanceof SymbolicExecutionTreeSynch){
			suff = "_synch";
		}
		IFile newFile = project.getFile("sets/"+ capsuleContainer.getName()+ suff + ".set");
		
		serializeToFile(containerTree, newFile);
		
			/*String contents = "Whatever";
			InputStream source = new ByteArrayInputStream(contents.getBytes());
			try {
				newFile.create(source, false, null);
			} catch (CoreException e) {
				// XODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
		
		
		
			//File f = new File("set.xmi");
			//f.createNewFile();
			
			//File f = new File
		
	}
	
	/*********************
	 * Symblic execution from a given state.
	 * @param capsule
	 * @param root
	 * @return
	 */
	public SymbolicExecutionTree executeSymbolicFromState(Class capsule, SymbolicState root, 
			List<String> timers, List<SymbolicState> explored){
		//create engine
		SymbolicExecutionTree tree = null;
		try {
			//create engine
			Engine engine = new Engine(capsule, root.getLocation().getName());
			
			//parse symbolic state
			SymbolicState rootParsed = engine.parse(root);
			
			//parse explored states
			List<SymbolicState> exploredParsed = engine.parse(explored);
						
			//pars timers
			engine.parseTimers (rootParsed, timers);
			
			//create thread and execute			
			TreeGeneratorThread gen = new TreeGeneratorThread(engine, rootParsed, exploredParsed);
			gen.start();			
			gen.join();
		
			tree = gen.getTree();
		
			displayTree(tree, engine);
			//serializeTree(tree);
		
		} catch (Exception e){
			e.printStackTrace();
		}
		return tree;
		
	}
	
	private SET serializeToFile(SymbolicExecutionTree containerTree,
			IFile newFile) {
		CSETTransform transformer = new CSETTransform();
		
		SymbolicState.startSerializing();
		//transform
		SET setTransformed = transformer.transform(containerTree);
		
		//store as xmi
		ResourceFactoryImpl factory = new XMIResourceFactoryImpl();
				
		String uriS = newFile.getLocationURI().toString();
		URI uri = URI.createURI(uriS);
					
		Resource resorce = factory.createResource(uri);
		resorce.getContents().add(setTransformed);
		try {
			resorce.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return setTransformed;
	}

	public SymbolicExecutionTree executeSymbolic(Class capsule, Property capsulePart, boolean displayTree) {
		
		System.out.println("SYMBOLIC EXECUTION " + capsule.getName());
		//create symbolic execution tree for this capsule (open)
		SymbolicExecutionTree tree = null;
		
		//if modularity required check for already generated
		boolean isNew = true;
		//JDA: For debug
		//String nm = capsule.getName();
		/* ============================================================================
		 TODO: JDA: 'modular is ineffective because 'capsuleTrees' is emptied by ...
		            'initMaps' every time!
		=============================================================================== */
		if (modular){
			tree = capsuleTrees.get(capsule.getName());			
		}
		if (tree == null){
			//JDA: The tree is new in the sense that it is not in 'capsuleTrees' (was 'capsuleTress').
			//System.out.println("New tree !!!");
			/* ===============================================================================
			   JDA: The following calculates the "open" symbolic execution tree (SET) of the
			        given capsule. The open SET is the SET of the capsule in isolation: that
			        is, without any kind of synchronization with the parts of the capsule.
			        The capsule's parts are the subcapsules contained directly or indirectly
			        with it.
			 ================================================================================= */
			tree = executeSymbolicOpen(capsule, capsulePart, displayTree);			
			capsuleTrees.put(capsule.getName(), tree);			
		} else {
			isNew = false;
		}		
		
		//JDA: Added
		if (tree == null) {
			//JDA: No more to do.
			return tree;
		}
		//copy tree
		SymbolicExecutionTree set = null;
		if (tree instanceof SymbolicExecutionTreeSynch){
			set = new SymbolicExecutionTreeSynch((SymbolicExecutionTreeSynch)tree);				
		} else {
			set = new SymbolicExecutionTree(tree);
		}
		
		set.setName((capsulePart!=null ? (capsulePart.getName() + ":") : "" ) + set.getName() );
		set.setPartName(capsulePart!=null ? (capsulePart.getName()) : "");
		
		//find parts of this capsule

		//Set<RTCapsulePart> parts = null; //
//		Set<RTCapsulePart> parts = extractParts(capsule);
		List<Property> parts = UmlrtUtil.getCapsuleParts(capsule);  

		//if there are no parts then it's done
		//Changed the following line
		//if (parts == null || parts.isEmpty() || !isNew ){
		if (parts == null || parts.isEmpty() || !isNew || generateOnlyOpen){
			return set;
		}
		
		//create their execution trees //JDA: i.e., the execution trees of all the parts of the capsule
		Map<String, SymbolicExecutionTree> roleTrees = new HashMap<String, SymbolicExecutionTree>(); 
		for (Property part : parts){
			Class partCapsule = part.getType() instanceof Class ? (Class) part.getType() : null; //Class partCapsule = RTFactory.CapsuleFactory.getCapsule(partClass);
			if (partCapsule == null)
				continue;
//			Class partClass =  part.getClass_();  //Class partClass = (Class) part.getType().getValue();
			SymbolicExecutionTree partTree = executeSymbolic(partCapsule,part, displayTree); //executeSymbolic(partCapsule,part.getLocalRedefinition());			
			//partTree.setName(part.getName() + partTree.getName());
			
			roleTrees.put(part.getName(), partTree); //roleTrees.put(part.getLocalRedefinition().getName(), partTree);
			partTrees.put(part,partTree); //partTrees.put(part.getLocalRedefinition(),partTree);
		}
				
		
		//find connectors
		//System.out.println("Finding connectors");
		Map<ActionOutput, ActionInput> connectedActions = new HashMap<ActionOutput, ActionInput>();
		Map<ActionOutput, SymbolicExecutionTree> connectedTrees = new HashMap<ActionOutput, SymbolicExecutionTree>();		
		RelayActions relayActions = new RelayActions();
		createConnections(capsule,set,connectedActions, connectedTrees, roleTrees, relayActions);		
		
		//System.out.println("Connectors " + connectors);
		//synchronize
		//TODO: JDA: (was KZ): - must change this if hierarchy ...
		//      JDA: The above should be checked.
		
		Set<Set<SymbolicExecutionTree>> partition = createPartition(roleTrees.values(), set);
		Synchronizer synch = new Synchronizer(set, roleTrees, connectedActions, connectedTrees, partition, relayActions);
		SymbolicExecutionTree synchTree = synch.synchronizeTree();
				
		capsuleTrees.put(capsule.getName(), synchTree);
		
		//JDA: Next, display the synchronised tree for a composite capsule.
		//     TODO: JDA: analyse how display works.
		//     Roughly, it concurrently displays the tree in a tab in the main window. The
		//     'synchTree' parameter is used as the initial value of the contents of this
		//     tab. The display is actually controlled by a display editor so that the
		//     displayed nodes can be moved around to try to fix overlapping text.
		//TODO: JDA: The PC/Valuation data is not being restored ...
		//      JDA: The data is not being displayed correctly in some cases.
		displayTree(synchTree, null);
		System.out.println("******************* Statistics ********************");
		System.out.println("DONE EXECUCTION for " + capsule.getName());
		System.out.println("Number of states " + synchTree.getNumberOfStates());
		System.out.println("Number of transitions " + synchTree.getNumberOfTransitions());
		System.out.println("******************* Statistics ********************");
		System.out.println("***************************************************");
		
		return synchTree;
		
	}


	private Set<Set<SymbolicExecutionTree>> createPartition(
			Collection<SymbolicExecutionTree> trees,SymbolicExecutionTree tree) {
		Set<Set<SymbolicExecutionTree>> result = new HashSet<Set<SymbolicExecutionTree>>();
		
		Set<SymbolicExecutionTree> part = new HashSet<SymbolicExecutionTree>();
		part.add(tree);
		result.add(part);
		
		for(SymbolicExecutionTree t : trees){
			Set<SymbolicExecutionTree> p = new HashSet<SymbolicExecutionTree>();
			p.add(t);
			result.add(p);
		}
		return result;
	}

	private void createConnections(Class capsule, SymbolicExecutionTree set, Map<ActionOutput, ActionInput> connectedActions,
			Map<ActionOutput, SymbolicExecutionTree> connectedTrees, Map<String, SymbolicExecutionTree> roleTrees,
			RelayActions relay) {		
				
		for (Connector connector : UmlrtUtil.getConnectors(capsule)){
			ConnectorEnd connEnd1 = connector.getEnds().get(0);
			ConnectorEnd connEnd2 = connector.getEnds().get(1);			
			
			if (connEnd1 == null || connEnd2==null){
				continue;
			}
			
			Port portBase = null;
			Port portConj = null;
			
			Port portOuter = null;
			Port portInner = null;
			
			Property partBase = null;
			Property partConj = null;		
						
			Property partInner = null;
			
			Class capsuleBase = null;
			Class capsuleConj = null;
						
			Class capsuleInner = null;
			
			boolean isRelay1 = false;
			boolean isRelay2 = false;
			
			//check port on end 1 	
			
			
			if (connEnd1.getRole() instanceof Port){
				//find port
				Port portGeneral = (Port) connEnd1.getRole();				
				//RTPortRedefinition port = RTFactory.redefFactory.getPortRedefinition(portGeneral, capsule);
				Port port = UmlrtUtil.getLocallyRedefinedPort(portGeneral, capsule); //portGeneral.getRedefinedPort(portGeneral.getName(), portGeneral.getType());
				
				Class endCapsule  = null;

				Property prop = connEnd1.getPartWithPort();						
		 			
				if (prop != null){
					endCapsule = ((Class)prop.getType());
				} else {
					endCapsule = capsule;					
				}
				
				isRelay1 = !port.isBehavior() && capsule.getAllAttributes().contains(portGeneral);
				if (isRelay1){
					portOuter = port;//port.getLocalRedefinition();
				} else {
					partInner = prop;
					capsuleInner = endCapsule;
					portInner = port;//port.getLocalRedefinition();
				}
				
				
				if (!port.isConjugated()){
					portBase = port;//port.getLocalRedefinition();
					partBase = prop;
					capsuleBase = endCapsule;
										
				} else {
					portConj = port;//port.getLocalRedefinition();
					partConj = prop;
					capsuleConj = endCapsule;
				}
			}
			
			if (connEnd2.getRole() instanceof Port){
				//find port
				Port portGeneral = (Port) connEnd2.getRole();				
//				RTPortRedefinition port = RTFactory.redefFactory.getPortRedefinition(portGeneral, capsule);
//				Port port = portGeneral.getRedefinedPort(portGeneral.getName(), portGeneral.getType());
				Port port = UmlrtUtil.getLocallyRedefinedPort(portGeneral, capsule); //portGeneral.getRedefinedPort(portGeneral.getName(), portGeneral.getType());
				
				Class endCapsule  = null;
				Property prop = connEnd2.getPartWithPort();
				if (prop != null){					
					endCapsule = (Class)prop.getType();					
				} else {
					endCapsule = capsule;
				}
								
				isRelay2 = !port.isBehavior() && capsule.getAllAttributes().contains(portGeneral);
				if (isRelay2){
					portOuter = port;//port.getLocalRedefinition();
				} else {
					partInner = prop;
					capsuleInner = endCapsule;
					portInner = port; //port.getLocalRedefinition();
				}
				
				if (portBase == null){
					portBase = port;//port.getLocalRedefinition();
					partBase = prop;
					capsuleBase = endCapsule;
										
				} else {
					portConj = port;//port.getLocalRedefinition();
					partConj = prop;
					capsuleConj = endCapsule;
				}
			}
			
			/*System.out.println("PORT BASE " + portBase);
			System.out.println("PORT CONJ " + portConj);
			
			System.out.println("CAPSULE BASE" + capsuleBase);
			System.out.println("CAPSULE CONJ"  + capsuleConj);
			
			System.out.println(engines);
			System.out.println(engines.get(capsuleBase));*/
				
			Engine engineBase = modular || partBase==null? engines.get(capsuleBase) : engines.get(partBase);
			Engine engineConj = modular || partConj == null ? engines.get(capsuleConj) : engines.get(partConj);
			
			//check if relay
			
			if (isRelay1 || isRelay2){
				Engine engineInner = engines.get(capsuleInner);
				Engine engineOuter = engines.get(capsule);
					
				SymbolicExecutionTree innerTree = partTrees.get(partInner);
				
				//get Relay input
				for (ActionInput aiOuter : engineOuter.getInputActions(portOuter)){
					if (aiOuter==null || engineInner==null){
						System.out.println("Nukk");
					}
					ActionInput aiInner = engineInner.getActionInput(portInner, ((ActionInputSM)aiOuter).getSignal());
					ActionInput aiInnerTree = innerTree.mapInputAction(aiInner);
					ActionInput aiOuterTree = set.mapInputAction(aiOuter);					
					relay.putInput(aiInnerTree, aiOuterTree);
				}
				// get relay output
				for (ActionOutput aoOuter : engineOuter.getOutputActions(portOuter)){
					ActionOutput aoInner = engineInner.getActionOutput(portInner, ((ActionOutputSM)aoOuter).getSignal());
					ActionOutput aoInnerTree = innerTree.mapOutputAction(aoInner);
					ActionOutput aoOuterTree = set.mapOutputAction(aoOuter);
					relay.putOutput(aoInnerTree, aoOuterTree);
				}
				
				relay.addInnerTree(innerTree);
				
			} else {			
			//find connected actions for base port
				
				Set<ActionOutput> outputBase = engineBase.getOutputActions(portBase);
				for (ActionOutput action : outputBase){
					if (action instanceof ActionOutputSM){
						ActionInput input = engineConj.getActionInput(portConj,((ActionOutputSM)action).getSignal());
						SymbolicExecutionTree treeReceive = partConj==null ? set : roleTrees.get(partConj.getName());						
						ActionInput treesInput = treeReceive.mapInputAction(input);
						SymbolicExecutionTree treeSend = partBase==null ? set : roleTrees.get(partBase.getName());
						ActionOutput treesOutput = treeSend.mapOutputAction(action);
						connectedActions.put(treesOutput, treesInput);						
						connectedTrees.put(treesOutput, treeReceive);
					}				
				}
			
			//find connected actions for conj port
				Set<ActionOutput> outputConj = engineConj.getOutputActions(portConj);
				for(ActionOutput output : outputConj){
					if (output instanceof ActionOutputSM){
						ActionInput input = engineBase.getActionInput(portBase, ((ActionOutputSM)output).getSignal());
						SymbolicExecutionTree treeReceive = partBase==null ? set : roleTrees.get(partBase.getName());
						ActionInput treeInput = treeReceive.mapInputAction(input);
						SymbolicExecutionTree treeSend = partConj==null ? set : roleTrees.get(partConj.getName());
						ActionOutput treeOutput = treeSend.mapOutputAction(output);
						connectedActions.put(treeOutput, treeInput);						
						connectedTrees.put(treeOutput, treeReceive);
					}
				}
			}			
			//System.out.println(partTrees);
			/*TreeConnector connectorTree = new TreeConnector(portBase, portConj);

			//System.out.println("Part base " + partTrees.get(partBase));
			connectorTree.setBaseTree(partTrees.get(partBase));
			//System.out.println("Part conj " + partConj);
			connectorTree.setConjugatedTree(partTrees.get(partConj));
			
			result.add(connectorTree);*/
			
		}
		/*System.out.println("***************************");
		System.out.println(connectedActions);
		System.out.println(connectedTrees);*/		
	}

	/* ==================================================================================
    executeSymbolicOpen
    JDA: Compute and display the Symbolic Execution Tree of the given capsule isolated from
    its context. The notion of "open" is technical: an execution is called "open" if all
    of its input is "open". Input is called "open" with respect to an execution if
    no queue or network appears in the execution to mediate between the sender and the
    input.
	===================================================================================== */
	private SymbolicExecutionTree executeSymbolicOpen(Class capsule, Property part, boolean displayTree) {
		System.out.println("SYMBOLIC EXECUTION OPEN " + capsule.getName());
		SymbolicExecutionTree tree = null;
		//JDA: The bulk of the work for Open Symbolic Execution is done in this 'try' region.
		try {
			//execute capsule
			//JDA: The Functional Finite State Machine for the capsule, 'capsule' is generated
			//     in the following.
			Engine engine = new Engine(capsule);
			engines.put(capsule, engine);
			//JDA: Added braces to the following.
			if (!modular && part != null) {
				engines.put(part, engine);
			}
			//JDA: The following only squirrels away the 'engine' in preperation for
			//     'gen.execute', which is invoked indirectly by 'gen.start', below.
			TreeGeneratorThread gen = new TreeGeneratorThread(engine);
			//JDA: Stepping over start doesn't work! The problem seems to be that execution
			//     freezes at start. But this prevents 'gen.run' from progressing and also
			//     disables most of the debug features (except pause and stop). Pause
			//     followed by resume will kick start. But best to put a break point
			//     at 'gen.join' and resume to it..
			// JDA: 'gen.execute', invoked indirectly next, is the workhorse.
			gen.start();			
			gen.join();
			//JDA: now gen.execute has completed and the symbolic execution tree (SET)
			//     is ready to be picked up.
			tree = gen.getTree();
			
			System.out.println("*************** Open *********************");
			if (tree != null) {
				System.out.println("Size of FFSM " + tree.getFFSM().getLocationSize());
				int size = tree.getNumberOfStates();
				//JDA: modified the following to report the number of error and unknown states.
				int numberOfUnknownStates = gen.getNumberOfUnknownNodes();
				int numberOfErrorStates = gen.getNumberOfErrorNodes();
				int numberOfSubsumedStates = gen.getNumberOfSubsumedNodes();
				System.out.println("Number of states in SET " + size + " ("
					//JDA: Bug 18
					+ (size - (numberOfUnknownStates + numberOfErrorStates + numberOfSubsumedStates)) + " Regular states, "
					//JDA: Bug 18
					+ numberOfSubsumedStates + " Subsumed states, "
				    + numberOfErrorStates + " Error states, "
					+ numberOfUnknownStates + " Unknown states)." );
				System.out.println("Number of transitions in SET " + tree.getNumberOfTransitions());
				
				//printing test cases
//				System.out.println("printing test cases..");
//				List<SymbolicPath> allPaths = tree.exploreAllPaths();
//				int c = 1;
//				for (SymbolicPath path : allPaths) {
//					System.out.println(String.format("Test[%d]: %s", c, path.solve().toString()));
//					c++;
//				}
				System.out.println("-----------------------");
			}
			System.out.println("TIME " + gen.time);
			System.out.println("******************************************");
			
			//JDA: added the following:
				if (tree == null) {
					return tree;
				}
			
			//JDA: display the SE tree for the state machine of a capsule.
			if (displayTree)
				displayTree(tree, engine);
			
			 
			//check test cases
			//long timeForTC = genTC(tree);
			//System.out.println("TIME to generate TC " + timeForTC);
			//System.out.println("************************************");
			//System.out.println("************************************");
			
			
		} catch (Exception e) {
			//JOptionPane.showMessageDialog(null, e.getMessage(), "Exception returned:", JOptionPane.INFORMATION_MESSAGE);
			e.printStackTrace();
		}
		System.out.println("OPEN DONE");
		return tree;
	}

	/* JDA: remove
	private long genTC(SymbolicExecutionTree tree) {
		long startTime = System.currentTimeMillis();
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		
		paths = tree.exploreAllPaths();
		
		System.out.println("No pf TCs " + paths.size());
		
		int i=0;
		for(SymbolicPath sp : paths){
			//System.out.println("Solving " + i++);
			sp.solve();
		}
		
		long endTime = System.currentTimeMillis();
		
		return endTime - startTime;
	}
    JDA: end remove */
	
	private void displayTree(SymbolicExecutionTree tree, Engine engine) {
		//create input for editor
		SETEditorInput input = new SETEditorInput(tree);
		
		//open editor
		IWorkbenchPage page = window.getActivePage();		
		
		try {			
			SETEditor editor = (SETEditor) page.openEditor
				(input, SETEditor.ID);						
		} catch (PartInitException e) { 
			e.printStackTrace();
		}
		
		/*//open editor
		TreeEditorInput input = new TreeEditorInput(tree,engine);
		Graph g = openEditor(input);
		
		//create graph */
	}
	
	
	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {		
				
		getSelectedCapsule(selection);
	}

	private void getSelectedCapsule(ISelection selection) {		
		if (!(selection instanceof IStructuredSelection)){
			return;
		}		
		IStructuredSelection structured = (IStructuredSelection) selection;	
		
		Object selectedObject = structured.getFirstElement();
		
// REZA: how a variable defined in a scope could be used ?! 
//		if (selectedObject instanceof GraphicalEditPart){
//			GraphicalEditPart part = (GraphicalEditPart) selectedObject;
//			
//			EditPart diagramEditPart = part.getRoot().getContents();
//			//diagram = (Diagram) diagramEditPart.getModel();
//						
//		}
		
		
		if (selectedObject instanceof IAdaptable){
			IAdaptable adaptableObject = (IAdaptable) selectedObject;				
			
			selectIfCapsule((EObject) adaptableObject.getAdapter(EObject.class));
		}
	}

	private void selectIfCapsule(Object selected) {
		if (selected instanceof Property)
			selected = ((Property)selected).getClass_();
		if (selected instanceof Class) {
			if (selected instanceof Behavior) {
				capsuleResource = ((Class) selected).eContainer().eResource();
				selected = ((Class) selected).eContainer();
			} else {
				capsuleResource = ((Class) selected).eResource();
			}
			capsuleContainer = UmlrtUtil.getCapsule((Class) selected); // RTFactory.CapsuleFactory.getCapsule((Class)selected);
		}
	}

	/* ===============================================================================================================
    JDA: 'extractParts' is not currently needed for my work. However there seems to be an implicit assumption that
    each part is a capsule.
  =================================================================================================================*/
//	private Set<RTCapsulePart> extractParts(Class container) {
//		//display roles:
//		
//		Set<RTCapsulePart> result = new HashSet<RTCapsulePart>();
//		if (container.getAllRoles() == null){
//			return result;
//		}
//		//System.out.println("Capsules contained in " + container.getUML2Class().getName());
//		
//		//discover all parts
//		for (RTCapsulePart part : container.getAllRoles()){
//			if (part != null && part.getType() != null && part.getType().getValue() instanceof Class){
//				Class partClass = (Class) part.getType().getValue();
//				if (RTFactory.CapsuleFactory.isCapsule(partClass)){
//					Class partCapsule = RTFactory.CapsuleFactory.getCapsule(partClass);
//					result.add(part);
//				}
//				//System.out.println("Part name " + part.getName() + " : " + partCapsule.getUML2Class().getName() +
//				//	"["+part.getReferenceTarget().getLower() + ".." + part.getReferenceTarget().getUpper()+"]");									
//			}
//			//result.addAll(extractContained(partCapsule));			
//		}		
//		return result;
//	}


	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

	public void setModular(boolean b) {
		this.modular = b;
	}
	
	public void setGenerateOnlyOpen(boolean b) {
		this.generateOnlyOpen = b;
	}
	
	public void setCapsule(Class capsule){
		capsuleContainer = capsule;
	}
}