package symbolic.execution.umlrt.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
//import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

public class SymbolicExecutionNonModularAction implements IWorkbenchWindowActionDelegate{
	SymbolicExecutionAction sea;
	
	
	public SymbolicExecutionNonModularAction() {
		sea = new SymbolicExecutionAction();
		sea.setModular(false);
	}
	
	@Override
	public void dispose() {
		// TODO: JDA: Auto-generated method stub
		
	}

	@Override
	public void init(IWorkbenchWindow window) {
		sea.init(window);		
	}

	@Override
	public void run(IAction action) {		
		sea.run(action);
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		sea.selectionChanged(action, selection);
		
	}

}
