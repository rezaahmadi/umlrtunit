package symbolic.execution.umlrt.actions;

import java.util.List;

import javax.swing.JOptionPane;

//import com.ibm.xtools.uml.rt.core.Capsule;

import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.umlrt.engine.Engine;

public class TreeGeneratorThread extends Thread {
	
	SymbolicExecutionTree tree;
	Engine engine;	
	long time;
	private SymbolicState root = null;
	private List<SymbolicState> explored = null;
	
	public TreeGeneratorThread(Engine engine){
		this.engine = engine;
	}
	
	
	
	public TreeGeneratorThread(Engine engine, SymbolicState root, List<SymbolicState> explored) {
		this.engine = engine;
		this.root = root;
		this.explored = explored;
		
	}


	public int getNumberOfErrorNodes() {
		return engine.getNumberOfErrorNodes();
	}
	public int getNumberOfUnknownNodes() {
		return engine.getNumberOfUnknownNodes();
	}
	//JDA: Bug 18
	public int getNumberOfSubsumedNodes() {
		return engine.getNumberOfSubsumedNodes();
	}

	@Override
	public void run() throws IllegalArgumentException {
		try {
			
			long startTime = System.currentTimeMillis();
			if (root == null){
				tree = engine.execute();
			} else {
				tree = engine.execute(root,explored);
			}
			long endTime = System.currentTimeMillis();
			
			time = endTime-startTime;
			
		} catch (Exception e) {
			//JDA: begin change
			JOptionPane.showMessageDialog(null, e.getMessage(), "Exception returned:", JOptionPane.INFORMATION_MESSAGE);
			//JDA: end change.
			e.printStackTrace();
		}
		
		
	}

	public SymbolicExecutionTree getTree() {		
		return tree;
	}
	
	

}
