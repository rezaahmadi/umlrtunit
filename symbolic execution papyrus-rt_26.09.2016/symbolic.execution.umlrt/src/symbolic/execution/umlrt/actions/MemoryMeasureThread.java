package symbolic.execution.umlrt.actions;


public class MemoryMeasureThread extends Thread {
	private long maxMemory;
	private boolean stop = false;
	
	@Override
	public void run() {
		while (!stop){
			long current = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
			if (current > maxMemory){
				maxMemory = current;
			}
		}	
	}
	
	public void stopMeasure() {
		stop = true;
		System.out.println("MAXIMUM MEMORY USAGE " + maxMemory);
	}
	
	public void resetMeasure(){
		maxMemory = 0;
	}
		
}
