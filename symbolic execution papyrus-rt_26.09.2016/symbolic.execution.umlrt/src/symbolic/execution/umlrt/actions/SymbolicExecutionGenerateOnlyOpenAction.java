package symbolic.execution.umlrt.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
//import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

public class SymbolicExecutionGenerateOnlyOpenAction implements IWorkbenchWindowActionDelegate{
	SymbolicExecutionAction sea;
	
	
	public SymbolicExecutionGenerateOnlyOpenAction() {
		sea = new SymbolicExecutionAction();
		sea.setModular(true);
		sea.setGenerateOnlyOpen(true);
	}
	
	@Override
	public void dispose() {
		// TODO: JDA: Check out this auto-generated method stub.
	}

	@Override
	public void init(IWorkbenchWindow window) {
		sea.init(window);		
	}

	@Override
	public void run(IAction action) {		
		sea.run(action);
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		sea.selectionChanged(action, selection);
		
	}

}
