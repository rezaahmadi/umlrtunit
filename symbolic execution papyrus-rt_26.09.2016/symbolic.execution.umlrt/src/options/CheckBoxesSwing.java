/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package options;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
//import javax.swing.JOptionPane;

import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;

public class CheckBoxesSwing {   
	    
		public static class LocalCheckBoxes extends javax.swing.JPanel implements ItemListener{
			
			private static final long serialVersionUID = 1L;
			JCheckBox performSolvabilityCheckBox;
		    JCheckBox performDuplicateCheckBox;
		    JCheckBox useNewVersionCheckBox;
		    static int order = 0;
		    
		    public LocalCheckBoxes() {
    		   super(new BorderLayout());
    		   
    		   JOptionPane.showMessageDialog(null, "Hello");
    		   performSolvabilityCheckBox = new JCheckBox("Perform solubilityCheck");
		            
               performSolvabilityCheckBox.setSelected(ExecutionOptions.getCheckSolvability());
               //performSolvabilityCheckBox.setMnemonic(KeyEvent.  VK_C);
               
               performDuplicateCheckBox = new JCheckBox("Perform duplicate check");
               //performDuplicateCheckBox.setMnemonic(KeyEvent.VK_G);
               performDuplicateCheckBox.setSelected(ExecutionOptions.getCheckForDuplicatePCs());

               useNewVersionCheckBox = new JCheckBox("Use old version (i.e. no error events)");
               	//useNewVersionCheckBox.setMnemonic(KeyEvent.VK_H);
               useNewVersionCheckBox.setSelected(ExecutionOptions.getNewExecution());
               performSolvabilityCheckBox.addItemListener(this);
               performDuplicateCheckBox.addItemListener(this);
               useNewVersionCheckBox.addItemListener(this);
		        //Put the check boxes in a column in a panel
               	JPanel checkPanel = new JPanel(new GridLayout(0, 1));
	        	checkPanel.add(performSolvabilityCheckBox);
		        checkPanel.add(performDuplicateCheckBox);
		        checkPanel.add(useNewVersionCheckBox);
	        	add(checkPanel, BorderLayout.LINE_START);
		        setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		        
		   }

			@Override
			public void itemStateChanged(ItemEvent e) {
				Object source = e.getItemSelectable();

		        if (source == performSolvabilityCheckBox) {
		        	if (e.getStateChange() == ItemEvent.DESELECTED) {
		        		order++;
		        		JOptionPane.showMessageDialog(null, "DESELECTED: " + order);
		            }
		        	else if (e.getStateChange() == ItemEvent.SELECTED) {
		        		order++;
		        		JOptionPane.showMessageDialog(null, "SELECTED: " + order);
		            }
		        }
		        else if (source == performDuplicateCheckBox) {
		        	if (e.getStateChange() == ItemEvent.DESELECTED) {
		        		JOptionPane.showMessageDialog(null, "DESELECTED");
		            }
		        	else if (e.getStateChange() == ItemEvent.SELECTED) {
		        		JOptionPane.showMessageDialog(null, "SELECTED");
		            }
		        }
		        else if (source == useNewVersionCheckBox) {
		        	if (e.getStateChange() == ItemEvent.DESELECTED) {
		        		JOptionPane.showMessageDialog(null, "DESELECTED");
		            }
		        	else if (e.getStateChange() == ItemEvent.SELECTED) {
		        		JOptionPane.showMessageDialog(null, "SELECTED");
		            }
		        }
			}
		}
		
		private   void createAndShowGUI() {
	        //Create and set up the window.
	        JFrame frame = new JFrame("LocalCheckBox");
	        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	        //Create and set up the content pane.
	        JComponent newContentPane = new LocalCheckBoxes();
	        newContentPane.setOpaque(true); //content panes must be opaque
	        frame.setContentPane(newContentPane);

	        //Display the window.
	        frame.pack();
	        frame.setVisible(true);
	    }
		
		public void startGUI() {
			//LocalCheckBoxes cbs = new LocalCheckBoxes();
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });
		}
	}
