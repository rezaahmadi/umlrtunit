package symbolic.analysis.minimal.tree.editor.editors;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import symbolic.analysis.formula.checker.adaptation.ModelPart;
//import symbolic.analysis.formula.checker.exploration.CompositeTreeExplorer;
import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
import symbolic.analysis.formula.checker.model.MinimalCompositeSET;
import symbolic.analysis.formula.checker.process.RequestProcessor;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.tree.graph.model.CompositeNode;
import symbolic.execution.tree.graph.model.Connection;
import symbolic.execution.tree.graph.model.ConnectionTypes;
import symbolic.execution.tree.graph.model.Graph;
import symbolic.execution.tree.graph.model.SimpleNode;

public class MinimalTreeEditorInput implements IEditorInput{
	private Graph graph;
	
	private String name;
	
	private RequestProcessor processor;
	
	private Map<NodeSymbolic, SimpleNode> mapping;
	
	private MinimalCompositeSET set;
	
	private int numberOfShownStates = 0;
	
	private static int MAX_NUMBER_OF_SHOWN_STATES = 100;
	
	public MinimalTreeEditorInput(RequestProcessor processor) {
		//set the name for an editor
		this.name = "Minimal SET";
		
		this.processor = processor;
				
		this.graph = new Graph();
		
		this.mapping = new HashMap<NodeSymbolic, SimpleNode>();
		
		this.set = processor.getSET();
		
		createGraph(set);
		
	}

	private void createGraph(MinimalCompositeSET set) {
		//create graph		
		
		//get root
		CompositeSymbolicState root = set.getRootState();
		NodeSymbolic nodeRoot = set.getRoot();
		
		numberOfShownStates = 0;
		
		SimpleNode rootNodeNew = exploreState(nodeRoot, root);
		
		exploreNode(rootNodeNew, nodeRoot);
		
	}

	private SimpleNode exploreState(NodeSymbolic node, CompositeSymbolicState state){
		if (state.isComposite()){			
			CompositeNode topLevel = new CompositeNode(state.toString(), true);
			mapping.put(node, topLevel);
			exploreCompositeState(topLevel, state);
			graph.addNode(topLevel);
			
			return topLevel;
		} else {
			SimpleNode sn = new SimpleNode(state.toString());
			graph.addNode(sn);
			mapping.put(node, sn);
			
			return sn;
		}
	}
	
	private void exploreCompositeState(CompositeNode graphNode, CompositeSymbolicState treeState) {
		for (ModelPart mp : treeState.getParts()){
			CompositeSymbolicState statePart = treeState.getStateForPart(mp);
			
			if (statePart.isComposite()){				
				CompositeNode node = new CompositeNode(mp.getPartName() + ":" + statePart.toString(), false);
				graphNode.addChild(node);
				exploreCompositeState(node, statePart);
			} else {
				SimpleNode sn = new SimpleNode(mp.getPartName() + ":" + statePart.toString());
				graphNode.addChild(sn);
			}
		}
		
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public Graph getGraph() {
		return graph;
	}

	@Override
	public Object getAdapter(Class adapter) {
		// TODO: JDA: Empty auto generated method stub?
		return null;
	}

	@Override
	public boolean exists() {
		// TODO: JDA: Empty auto generated method stub?
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO: JDA: Empty auto generated method stub?
		return null;
	}

	@Override
	public String getName() {
		// TODO: JDA: Empty auto generated method stub
		return name;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO: JDA: Empty auto generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO: JDA: Empty auto generated method stub
		return name;
	}

	public boolean makeStep() {
		// make step with explorer
		boolean isDone = processor.makeStep();
		
		//NodeSymbolic lastExplored = explorer.getLastExplored();
		//SimpleNode graphNode = mapping.get(lastExplored);
		
		//get node from a graph
		//exploreNode(graphNode, lastExplored);
		
		graph.clearAll();
		
		createGraph(set);
		
		
		/*CompositeNode cn = new CompositeNode("TopLevel 1", true);
		cn.addChild(new SimpleNode("Inner 1"));
		graph.addNode(cn);*/
		
		return isDone;
	}

	private void exploreNode(SimpleNode graphNode, NodeSymbolic treeNode) {		
		// for each successor
		
		numberOfShownStates++;
		if (numberOfShownStates >= MAX_NUMBER_OF_SHOWN_STATES){
			return;
		}
		for (Arc<SymbolicTransition, SymbolicState> arc : treeNode.getChildren()){
			SymbolicTransition transition = arc.getContents();			
			
			NodeSymbolic targetTreeNode = (NodeSymbolic) arc.getTarget();
			if (targetTreeNode == null){
				int a = 0;
			}
			CompositeSymbolicState targetTreeState = (CompositeSymbolicState) targetTreeNode.getContents();
			
			SimpleNode targetGraphNode = exploreState(targetTreeNode,targetTreeState);
			
			Connection cn = new Connection(transition.printInLines(), ConnectionTypes.Synchronized);
			
			cn.setSource(graphNode);
			cn.setTarget(targetGraphNode);
						
			
			exploreNode(targetGraphNode,targetTreeNode);
			//}
		}
		
		
	}

	public void clear() {
		graph.clearAll();
		
	}

	public void run() {
		processor.run();
		
		createGraph(set);
		
	}

	public String getResult() {		
		return processor.getResult();
	}
	
	

}
