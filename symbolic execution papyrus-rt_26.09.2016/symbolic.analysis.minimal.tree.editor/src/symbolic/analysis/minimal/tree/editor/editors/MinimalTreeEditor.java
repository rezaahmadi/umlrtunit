package symbolic.analysis.minimal.tree.editor.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
//import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
//import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.part.EditorPart;

import symbolic.execution.tree.graph.editparts.GraphEditPart;
import symbolic.execution.tree.graph.editparts.SETEditPartFactory;

public class MinimalTreeEditor extends EditorPart {//implements ISelectionListener{
	public static String ID = MinimalTreeEditor.class.getName();

	private EditDomain editDomain;
	
	private GraphicalViewer viewer;
	
	private MinimalTreeEditorInput input;
	
	public MinimalTreeEditor() {}
	
	public void dispose() {		
		super.dispose();
	}
	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO: JDA: Empty auto generates stub!
		
	}
	@Override
	public void doSaveAs() {
		// TODO: JDA: Empty auto generated method stub
		
	}
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		setPartName(input.getName());
		//site.getPage().addSelectionListener(this);
		initEditDomain();
		
		if (input instanceof MinimalTreeEditorInput){
			this.input = (MinimalTreeEditorInput) input;
		}
		
	}
	private void initEditDomain() {
		this.editDomain = new DefaultEditDomain(this);
		
	}

	@Override
	public boolean isDirty() {
		// TODO: JDA: Empty auto generated method stub
		return false;
	}
	@Override
	public boolean isSaveAsAllowed() {
		// TODO: JDA: Empty auto generated method stub
		return false;
	}
	@Override
	public void createPartControl(Composite parent) {
		createGraphViewer(parent);
		
	}
	private void createGraphViewer(Composite parent) {
		ScrollingGraphicalViewer viewer = new ScrollingGraphicalViewer();
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		viewer.createControl(parent);
		viewer.getControl().setBackground(ColorConstants.white);
		viewer.setEditPartFactory(new SETEditPartFactory());
		viewer.setContents(((MinimalTreeEditorInput)getEditorInput()).getGraph());
		viewer.getContents().refresh();
		
		this.editDomain.addViewer(viewer);
		
		this.viewer = viewer;
		
	}

	@Override
	public void setFocus() {
		// TODO: JDA: Empty auto generated method stub
		
	}

	//@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		//((GraphEditPart)viewer.getContents()).toXYLayout();		
	}

	public boolean makeStep() {
		input.clear();
		if (viewer.getContents() instanceof GraphEditPart){
			GraphEditPart part = (GraphEditPart)viewer.getContents();
			part.toGraphLayout();
			part.getFigure().getLayoutManager().layout(part.getFigure());
			
		}
		viewer.getContents().refresh();
		
		
		boolean isDone = input.makeStep();		
		
		if (viewer.getContents() instanceof GraphEditPart){
			GraphEditPart part = (GraphEditPart)viewer.getContents();
			part.toGraphLayout();
			part.getFigure().getLayoutManager().layout(part.getFigure());
			
		}
		viewer.getContents().refresh();
		
		return isDone;
		
	}

	public void runAll() {
		input.clear();
		if (viewer.getContents() instanceof GraphEditPart){
			GraphEditPart part = (GraphEditPart)viewer.getContents();
			part.toGraphLayout();
			part.getFigure().getLayoutManager().layout(part.getFigure());
			
		}
		viewer.getContents().refresh();
		
		
		input.run();		
		
		if (viewer.getContents() instanceof GraphEditPart){
			GraphEditPart part = (GraphEditPart)viewer.getContents();
			part.toGraphLayout();
			part.getFigure().getLayoutManager().layout(part.getFigure());
			
		}
		viewer.getContents().refresh();
		
	}

	public String getResult() {		
		return input.getResult();
	}

}
