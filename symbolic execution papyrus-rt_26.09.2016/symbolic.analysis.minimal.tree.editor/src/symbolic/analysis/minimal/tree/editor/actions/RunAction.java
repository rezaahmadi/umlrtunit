package symbolic.analysis.minimal.tree.editor.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import symbolic.analysis.minimal.tree.editor.editors.MinimalTreeEditor;

public class RunAction implements IEditorActionDelegate{
	private MinimalTreeEditor editor;
	
	@Override
	public void run(IAction action) {
		if (editor != null){
			editor.runAll();
		}
		
		String msg = editor.getResult();
		
		MessageDialog.openInformation(editor.getSite().getShell(),"Result", msg);
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO: JDA: Empty auto generated method stub.
		
	}

	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof MinimalTreeEditor){
			this.editor = (MinimalTreeEditor) targetEditor;
		}
		
	}

}
