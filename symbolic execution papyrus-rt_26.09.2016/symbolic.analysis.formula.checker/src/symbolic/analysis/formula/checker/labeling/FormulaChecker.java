package symbolic.analysis.formula.checker.labeling;

//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
import java.util.Set;

import symbolic.analysis.formula.checker.model.AnyInputAction;
import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
import symbolic.analysis.formula.checker.model.NodeSymbolicCheck;
//import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.InOut;
import symbolic.analysis.language.InQueue;
import symbolic.analysis.language.InState;
import symbolic.analysis.language.Invariant;
import symbolic.analysis.language.PathQuantifiers;
import symbolic.analysis.language.CompositeFormula;
import symbolic.analysis.language.StateQuantifiers;
import symbolic.analysis.language.True;
import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;

public class FormulaChecker {
	
	private Labeler labeler;
	
	public FormulaChecker(Labeler labeler){
		this.labeler = labeler;
	}
	
	public boolean checkSatisfy(NodeSymbolicCheck node, InState inState){
		CompositeSymbolicState css = (CompositeSymbolicState) node.getContents();		
		return css.partInLocation(inState.getLocation(),inState.getAtPart().getPath(),0);
	}
	
	public boolean checkSatisfy(NodeSymbolicCheck node, InQueue inQueue){
		CompositeSymbolicState css = (CompositeSymbolicState) node.getContents();
		return css.partHasQueue(inQueue.getActions().createInputList(),inQueue.getAtPart().getPath(),0);
	}
		
	
	public boolean checkSatisfy(NodeSymbolicCheck node, InOut inOut){
		//check whether there is an arc with 
		for (Arc<SymbolicTransition, SymbolicState> arc : node.getChildren()){
			SymbolicTransition st = arc.getContents();
			
			//check trigger
			if (!(inOut.getActionInput().getActionInput() instanceof AnyInputAction)){
				if (st.getInputExecution()==null){
					continue;
				}
				if (st.getInputExecution().getAction().equals(inOut.getActionInput().getActionInput())){
					return true;
				} else {
					continue;
				}
			} 
			//check output sequence
			//if (inOut.getOutputList().checkOutputs(st.getOuputSequence().getSequence())){
			//	return true;
			//}
			
			
		}
		
		return false;
	}
	
	public boolean checkSatisfy(NodeSymbolicCheck node, Invariant invariant){
		
		
		CompositeSymbolicState css = (CompositeSymbolicState) node.getContents();
		CompositeSymbolicState cssPart = css.findPart(invariant.getAtPart().getPath(),1);
		SymbolicState statePart = cssPart.getTopState();
		
		Constraint cstReplaced = (Constraint) invariant.getCst().replaceVariables(statePart.getValuation().getSymbolicVariables());
		
		if (cstReplaced instanceof symbolic.execution.constraints.manager.True){
			return true;
		}
		//check whether false satisfiable
		boolean solved = SymbolicFactory.createSolver().isSolvable(statePart.getPC(), cstReplaced, false);
		
		return !solved;
		
	}

	public Boolean checkSatisfyAtomic(NodeSymbolicCheck node, Formula f){
		if (f instanceof InState){
			return checkSatisfy(node, (InState)f);
		} else if (f instanceof InQueue){
			return checkSatisfy(node, (InQueue)f);
		} else if (f instanceof InOut){
			return checkSatisfy(node, (InOut)f);
		} else if (f instanceof Invariant){
			return checkSatisfy(node, (Invariant)f);
		}
			return null;
	}
	
	public Boolean checkSatisfy(NodeSymbolicCheck state, Formula f) {
		if (f instanceof CompositeFormula){
			CompositeFormula sf = (CompositeFormula) f;
			if (sf instanceof True){
				return true;
			}
			Boolean left = labeler.getLabel(state,sf.getLeft());
			Boolean right = labeler.getLabel(state, sf.getRight());
			
			return evaluate(sf,left,right);
		} else if (f instanceof Formula){
			PathQuantifiers pq = f.getPathQ();
			StateQuantifiers sq = f.getStateQ();
			
			if (pq.equals(PathQuantifiers.E) && sq.equals(StateQuantifiers.X)){
				return checkEX(state, f.getLeft());
			} else if (pq.equals(PathQuantifiers.E) && sq.equals(StateQuantifiers.U)){
				return checkEU(state, f, f.getLeft(), f.getRight());
			} else if (pq.equals(PathQuantifiers.A) && sq.equals(StateQuantifiers.F)){
				return checkAF(state,f, f.getLeft());
			}
		}
		return false;
	}

	private Boolean checkAF(NodeSymbolicCheck node, Formula f, Formula left) {
		Boolean leftSat = labeler.getLabel(node,left); 
		if (leftSat!=null && leftSat){
			return true;
		}
		if (!node.isExplored()){
			return null;
		}
		
		//get Children
		Set<NodeSymbolicCheck> children = getChildren(node);
		
		if (children.isEmpty()){
			return false;
		}
		//if no children then false
		/*if (node.getChildren().isEmpty()){
			return false;
		}*/
		
		//check if any of my targets satisfies AF
		boolean childHasNull = false;
		for (NodeSymbolicCheck target : children){			
			Boolean childSat = labeler.getLabel(target, f);
			//Boolean thisSat = labeler
			if (childSat == null){
				childHasNull = true;
			} else {
				if (childSat == false){
					return false;
				}
			}
			
		}
		if (childHasNull){
			return null;
		}
		return true;
	}

	
	private Set<NodeSymbolicCheck> getChildren(NodeSymbolicCheck node) {
		Set<NodeSymbolicCheck> result = new HashSet<NodeSymbolicCheck>();
		NodeSymbolicCheck source = node;
		int i=0;
		while (source.getSubsumedBy() != null){
			source = (NodeSymbolicCheck) source.getSubsumedBy();
			
			if (source == null || source == node){
				break;
			}
			
			
			if ( i++ > 10000){
				System.out.println();
			}
		}
		for (Arc<SymbolicTransition,SymbolicState> arc : source.getChildren()){
			result.add((NodeSymbolicCheck) arc.getTarget());
		}
		return result;
	}

	private Boolean checkEU(NodeSymbolicCheck node, Formula f, Formula left, Formula right) {
		Boolean rightSat = labeler.getLabel(node, right);
		if (rightSat != null && rightSat){
			return true;
		}
		
		Boolean leftSat = labeler.getLabel(node, left);
		if (left instanceof True){
			leftSat = true;
		}
		if (leftSat == null){
			return null;
		}
		if (!leftSat){
			return false;
		}
		
		if (!node.isExplored()){
			return null;
		}
		
		//System.out.println("Before children");
		Set<NodeSymbolicCheck> children = getChildren(node);
		//System.out.println("After");
		if (children.isEmpty()){
			return false;
		}
		boolean hasNull=false;
		for (NodeSymbolicCheck target: getChildren(node)){			
			//Boolean targetRight = labeler.getLabel(target, right);
			Boolean targetFormulaSat = labeler.getLabel(target, f);
			if (targetFormulaSat == null){
				hasNull = true;
			} else {
				if (targetFormulaSat){
					return true;
				} else {
					continue;
				}
			
			}
			/*if (target.hasFormula(f)){
				return true;
			}*/
		}
		if (hasNull){
			return null;
		} else {
			return false;
		}
	}

	private Boolean checkEX(NodeSymbolicCheck node, Formula f) {
		//if  not explored - don't know
		if (!node.isExplored()){
			return null;
		}
		
		//get next nodes
		for (Arc<SymbolicTransition,SymbolicState> arc : node.getChildren()){
			NodeSymbolicCheck next = (NodeSymbolicCheck) arc.getTarget();
			if (next.hasFormula(f)){
				return true;
			}
		}
		return false;
	}

	private Boolean evaluate(CompositeFormula sf, Boolean left, Boolean right) {
		switch (sf.getOperator()){
		case and :
			if (left != null && right != null) 
				return left && right;
			if (left == null && right != null)
				return right==false ? false : null;
			if (right == null && left != null)
				return left == false ? false : null;			
			return null;
		case or :
			if (left !=null && right != null)
				return left || right;	
			if (left == null && right != null)
				return right==true ? true : null;
			if (right == null && left != null)
				return left == true ? true : null;
			return null;
		case not :
			if (left != null)
				return !left;
			return null;
		case imply :
			if (left !=null && right != null)
				return !left || right;	
			if (left == null && right != null)
				return right==true ? true : null;
			if (right == null && left != null)
				return left == false ? true : null;
			return null;
			
		}
		return false;
	}
}
