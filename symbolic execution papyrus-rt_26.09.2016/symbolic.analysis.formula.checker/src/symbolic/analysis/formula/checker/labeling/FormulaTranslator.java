package symbolic.analysis.formula.checker.labeling;

import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.CompositeFormula;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.LogicalOperator;
import symbolic.analysis.language.PathQuantifiers;
import symbolic.analysis.language.StateQuantifiers;
import symbolic.analysis.language.True;

public class FormulaTranslator {
	
	public FormulaTranslator(){}
	
	public Formula translate(Formula f){
		if (f instanceof AtomicStateFormula){
			return f;
		} else if (f instanceof CompositeFormula){
			CompositeFormula cf = (CompositeFormula) f;
			return new CompositeFormula(translate(cf.getLeft()), cf.getOperator(), translate(cf.getRight()));
		} else if (f instanceof Formula){
			PathQuantifiers pq = f.getPathQ();
			StateQuantifiers sq = f.getStateQ();
			
			
			
			if (pq.equals(PathQuantifiers.A)){
				switch (sq){
					case X : return translateAX(f.getLeft());
					case G : return translateAG(f.getLeft());
					case U : return translateAU(f.getLeft(),f.getRight());
					case F : return f;
				}
			} else if (pq.equals(PathQuantifiers.E)) {
				switch(sq){
					case F : return translateEF(f.getLeft());
					case G : return translateEG(f.getLeft());
					case X : return f;
					case U : return new Formula(PathQuantifiers.E, StateQuantifiers.U, translate(f.getLeft()), translate(f.getRight()));
				}
			}
		}
		return null;
	}

	private Formula translateEG(Formula f) {
		Formula AF = new Formula( PathQuantifiers.A, StateQuantifiers.F,
				 new CompositeFormula(null, LogicalOperator.not, translate(f)),null);
		return new CompositeFormula(null,LogicalOperator.not, AF);
	}

	private Formula translateAU(Formula left, Formula right) {
		Formula EGtranslated = translateEG(new CompositeFormula(null, LogicalOperator.not,translate(right)));
		Formula notEG = new CompositeFormula(null, LogicalOperator.not, EGtranslated);
		
		Formula EU = new Formula(PathQuantifiers.E, StateQuantifiers.U, 
						new CompositeFormula(null, LogicalOperator.not, translate(right)),
						new CompositeFormula(
									new CompositeFormula(null, LogicalOperator.not, translate(left)),
									LogicalOperator.and,
									new CompositeFormula(null, LogicalOperator.not,translate(right))));
		Formula notEU = new CompositeFormula(null, LogicalOperator.not, EU);
		return new CompositeFormula(notEG,LogicalOperator.and, notEU);
	}

	private Formula translateAG(Formula f) {
		Formula EF = new Formula( PathQuantifiers.E, StateQuantifiers.F,
				 new CompositeFormula(null, LogicalOperator.not, translate(f)),null);
		Formula translatedEF = translateEF(EF.getLeft()); 
		
		return new CompositeFormula(null,LogicalOperator.not, translatedEF);
	}

	private Formula translateEF(Formula f) {
		return new Formula(PathQuantifiers.E,StateQuantifiers.U, new True(), translate(f));
		
	}

	private Formula translateAX(Formula f) {
		Formula EX = new Formula( PathQuantifiers.E, StateQuantifiers.X,
				 new CompositeFormula(null, LogicalOperator.not, translate(f)),null);
		return new CompositeFormula(null,LogicalOperator.not, EX);
	}
	

}
