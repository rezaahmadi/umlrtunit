package symbolic.analysis.formula.checker.labeling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import symbolic.analysis.formula.checker.model.MinimalCompositeSET;
import symbolic.analysis.formula.checker.model.NodeSymbolicCheck;
import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.InOut;
import symbolic.analysis.language.StateQuantifiers;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;

public class Labeler {
	private Formula formula;
	private List<Formula> formulas;	
	private List<Formula> atomicPropositions;
	
	private FormulaChecker fc;
	
	private Map<NodeSymbolic,Set<Formula>> labels;	
	private Map<NodeSymbolic,Set<Formula>> negativeLabels;
	
	private boolean hasAtomicChanged = false;
	
	public Labeler(Formula formula) {
		//remember formula
		this.formula = formula;
		
		//generate subformulas
		SubformulaGenerator sf = new SubformulaGenerator();
		List<Formula> formulasAll = sf.getSubformula(formula);
		
		divideFormulas(formulasAll);
		
		this.fc = new FormulaChecker(this);
		
		//initialize labels
		this.labels = new HashMap<NodeSymbolic, Set<Formula>>();
		this.negativeLabels = new HashMap<NodeSymbolic, Set<Formula>>();
				
	}
	
	
	private void divideFormulas(List<Formula> formulasAll) {
		atomicPropositions = new ArrayList<Formula>();
		formulas = new ArrayList<Formula>();
		
		for (Formula f : formulasAll){
			if (f instanceof AtomicStateFormula){
				atomicPropositions.add(f);
			} else {
				formulas.add(f);
			}
		}
		
	}


	public void label(Set<NodeSymbolicCheck> newlyExplored, Set<NodeSymbolicCheck> lastExplored){
		//label atomic propositions		
		Set<NodeSymbolicCheck> changedLabels = labelAtomicPropositons(newlyExplored);
		
		System.out.println("Labeling atomic done");
		changedLabels.addAll(lastExplored);
		
		//if there is a change label formulas
		//Set<NodeSymbolicCheck> changedForLastFormula = new HashSet<NodeSymbolicCheck>();
		//changedForLastFormula = changedLabels;	
			
			
		while (!changedLabels.isEmpty()){				
			System.out.println("Changed " + changedLabels.size());
			changedLabels = labelFormulas(changedLabels);
				
		}
		
		System.out.println("Labeling done");
		
	}
	

	//can avoid going up if logical and 
	private Set<NodeSymbolicCheck> labelFormulas(Set<NodeSymbolicCheck> changedLabels) {
		Set<NodeSymbolicCheck> changed = new HashSet<NodeSymbolicCheck>();
		for (Formula f : formulas){
			if (f.getStateQ() != null && f.getStateQ().equals(StateQuantifiers.U)){
				//System.out.println("");
			}
			System.out.println("Formula " + f.toString());
			
			for (NodeSymbolicCheck state : changedLabels){
				
				if (changed.contains(state)){
					continue;
				}
				NodeSymbolicCheck current = state;
				//move up the tree to follow the change
				
				boolean added = false;
				while (current != null){	
					if (isLabeled(current,f)){
						if (f.getStateQ()!=null){
							//just for temporal !!
							current = current.getParent();
						} else {
							current = null;
						}						
						continue;
					}
					//System.out.println("Node " + state);
					//System.out.println("Current " + current);
					Boolean satis = fc.checkSatisfy(current, f);
					
					if (satis != null) {
						
						if (satis){
							addLabel(satis,current,f);
							//fc.checkSatisfy(current,f);
						}
						addLabel(satis, current, f);
						if (!added){
							changed.add(current);
							added = true;
						}
						if (f.getStateQ()!=null){
							//just for temporal !!
							current = current.getParent();
						} else {
							current = null;
						}
						
					} else {
						current = null;
					}
					
				}
			}
		}
		return changed;
	}


	private Set<NodeSymbolicCheck> labelAtomicPropositons(Set<NodeSymbolicCheck> newlyExplored) {
		Set<NodeSymbolicCheck> result = new HashSet<NodeSymbolicCheck>();
		for (Formula f : atomicPropositions){			
			for (NodeSymbolicCheck state : newlyExplored){
				if (f instanceof InOut){
					NodeSymbolicCheck parent = state.getParent(); 
					if (parent == null){                                                   
						continue;
					}
					if (!isLabeled(parent,f)){
						boolean satis = fc.checkSatisfy(parent, (InOut)f);						
						addLabel(satis,parent,f);
						result.add(parent);
					}
				} else {
					if (!isLabeled(state,f)){
						try {
						boolean satis = fc.checkSatisfyAtomic(state,f);
						addLabel(satis,state,f);
						result.add(state);
						}catch (NullPointerException e){
							System.out.println(e);
						}
					}
				}
			}
		}
		return result;
		
	}


	private void addLabel(boolean satis, NodeSymbolicCheck state, Formula f) {
		hasAtomicChanged = true;
		if (satis){
			Set<Formula> labelsState = labels.get(state);
			if (labelsState == null){
				labelsState = new HashSet<Formula>();
				labels.put(state, labelsState);
			}
			labelsState.add(f);
		} else {
			Set<Formula> labelsState = negativeLabels.get(state);
			if (labelsState == null){
				labelsState = new HashSet<Formula>();
				negativeLabels.put(state, labelsState);
			}
			labelsState.add(f);
		}
		
	}


	private boolean isLabeled(NodeSymbolicCheck state, Formula f) {
		boolean isPositive = false;
		boolean isNegative = false;
		if (labels.containsKey(state)){
			isPositive =  labels.get(state).contains(f);
		}
		if (negativeLabels.containsKey(state)){
			isNegative =  negativeLabels.get(state).contains(f);
		}
		return isPositive || isNegative;
	}


	/*public void label(NodeSymbolicCheck root){
		for (Formula f : formulas){			
			label(f,root);
		}
		if (hasAtomicChanged){
			hasAtomicChanged = false;
			label(root);
		}
	}*/

	/*private void label(Formula f, NodeSymbolicCheck node) {
		if (!node.hasFormulaChecked(f)){		
			if (fc.checkSatisfy(node,f)){
				if (!node.hasFormula(f)){
					node.addFinal(f);
					hasAtomicChanged=true;
				}
			}
		}
		for (Arc<SymbolicTransition, SymbolicState> arc :node.getChildren()){
			NodeSymbolicCheck target = (NodeSymbolicCheck) arc.getTarget();
			
			label(f,target);
		}
		
	}*/


	public Boolean getLabel(NodeSymbolicCheck node, Formula left) {
		if (labels.containsKey(node) && labels.get(node).contains(left)){
			return true;
		}
		if (negativeLabels.containsKey(node) && negativeLabels.get(node).contains(left)){
			return false;
		}
		return null;
	}


	public Set<NodeSymbolicCheck> labelMain(NodeSymbolicCheck node) {
		Set<NodeSymbolicCheck> result = new HashSet<NodeSymbolicCheck>();
		
		Boolean label = getLabel(node, formula);
		if (label==null){
			result.add(node);
		}
		
		for (Formula f : atomicPropositions){
			Boolean b =getLabel(node, f);
			if (f != formula){
				if (b == null){
					int a=0;
				}
			}
		
		}
		
		for (Formula f : formulas){
			Boolean b =getLabel(node, f);
			if (f != formula){
				if (b == null){
					int a=0;
				}
			}
		
		}
		
		for (Arc<SymbolicTransition,SymbolicState> arc : node.getChildren()){
			Set<NodeSymbolicCheck> arcResults = labelMain((NodeSymbolicCheck) arc.getTarget());
			result.addAll(arcResults);
		}
		return result;
	}


	public int getPositiveLabelsCount(NodeSymbolicCheck currentNode) {
		if (labels.containsKey(currentNode)){
			return labels.get(currentNode).size();
		}
		return 0;
	}

}
