package symbolic.analysis.formula.checker.labeling;

import java.util.ArrayList;
//import java.util.HashSet;
import java.util.List;
//import java.util.Set;

import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.CompositeFormula;

public class SubformulaGenerator {
	
	public List<Formula> getSubformula(Formula f){
		List<Formula> result= new ArrayList<Formula>();
		
		if (f instanceof AtomicStateFormula){
			result.add(f);
		} else if (f instanceof CompositeFormula){
			CompositeFormula sf = (CompositeFormula)f;
			result.addAll(getSubformula(sf.getLeft()));
			result.addAll(getSubformula(sf.getRight()));			
			result.add(f);
			
		} else if (f instanceof Formula){
			result.addAll(getSubformula(f.getRight()));
			result.addAll(getSubformula(f.getLeft()));			
			result.add(f);
			
		}
		
		return result;
		
	}

}
