package symbolic.analysis.formula.checker.adaptation;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

//import com.ibm.xtools.uml.redefinition.PropertyRedefinition;

public class ModelPart {
	private List<Property> path;
	
	private String capsuleName = "Controller";
	
	public ModelPart(String capsuleName){
		this.capsuleName = capsuleName;
		path = new ArrayList<Property>();
	}
	
	public ModelPart(ModelPart p, Property append, String capsuleName){
		this(capsuleName);
		path = new ArrayList<Property>(p.getPath());
		path.add(append);
	}
	
	public List<Property> getPath() {
		return path;
	}

	public ModelPart (List<Property> path){
		this.path = path;
	}
	
	@Override
	public String toString() {
		if (path.isEmpty())
			return capsuleName;
		StringBuffer b = new StringBuffer();
		for (Property p : path){
			b.append(p.getName());
			b.append('.');
		}
		return b.substring(0,b.length()-1);
	}

	public boolean hasPath(List<Property> p) {
		if (p.size() != path.size())
			return false;
		
		for (int i=0; i<p.size(); i++){
			if (!p.get(i).equals(path.get(i))){
				return false;
			}
		}
		return true;
	}

	public Class getPartClass() {
		
		return (Class)path.get(path.size()-1).getType();
	}

	public boolean directChild(List<Property> parentPath) {
		if (parentPath.size() + 1 != path.size()){
			return false;
		}
		for (int i=0; i<parentPath.size(); i++){
			if (!parentPath.get(i).equals(path.get(i))){
				return false;
			}
		}		
		return true;
	}

	public boolean isComposite(Set<ModelPart> parts) {
		for (ModelPart mp : parts){
			if (mp.directChild(path)){
				return true;
			}
		}
		return false;
	}

	public String getPartName() {
		if (path==null || path.isEmpty()){
			return "[]";
		}
		return path.get(path.size()-1).getName();
	}

	public boolean isTopLevel() {
		return path.isEmpty();
	}

	public boolean equalsString(List<String> propertiesString) {
		if (path.size() != propertiesString.size()){
			return false;
		}
		
		for (int i = 0; i<path.size(); i++){
			Property p = path.get(i);
			if (!p.getName().equals(propertiesString.get(i))){
				return false;
			}
		}
		
		return true;
		
	}

	public Property getLastPart() {
		if (path!=null && path.isEmpty()){
			return null;
		}
		return path.get(path.size()-1);
	}

	public boolean agreesToDepth(List<Property> otherPath, int depth) {
		for (int i=0; i<depth; i++){
			if (!otherPath.get(i).equals(path.get(i))){
				return false;
			}
		}
		return true;
	}

	public int length() {
		return path.size();		
	}

}
