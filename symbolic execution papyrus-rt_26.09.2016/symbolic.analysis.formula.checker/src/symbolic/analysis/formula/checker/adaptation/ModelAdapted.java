package symbolic.analysis.formula.checker.adaptation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.Class;
//import org.eclipse.papyrusrt.xtumlrt.common.Class;
import org.eclipse.uml2.uml.Class;
//import org.eclipse.uml2.uml.ConnectorEnd;
//import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

//import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
import symbolic.analysis.formula.checker.process.SymbolicTreesMap;
import symbolic.analysis.language.Variable;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.umlrt.engine.Engine;
//import symbolic.execution.umlrt.model.ActionOutputSM;
import symbolic.execution.umlrt.model.SMValidationException;
import symbolic.execution.utils.UmlrtUtil;

//import com.ibm.xtools.uml.rt.core.Class;
//import com.ibm.xtools.uml.rt.core.RTCapsulePart;
//import com.ibm.xtools.uml.rt.core.RTConnectorRedefinition;
//import com.ibm.xtools.uml.rt.core.RTFactory;
//import com.ibm.xtools.uml.rt.core.RTPortRedefinition;

public class ModelAdapted {
	private Class capsule;
	
	private Class capsuleClass;
	
	private ModelPart topLevelPart;
	
	private Set<ModelPart> parts;
	
	private Map<ModelPart,Class> partType;
	
	private Map<ModelPart, Set<ModelPart>> parentRelation;
	
	private Map<Class, Engine> engines;
	
	private ConnectionRelation connRelation;
	
	private Map<ModelPart, Class> partCapsule;
	
	private Map<Class, ConnectionRelationBasic> connectionRelationBasic;
	
	private Map<ModelPart,ActionMapping> actionsMap;
		
	
	private SymbolicTreesMap trees;
	
	public ModelAdapted(Class capsule) throws Exception{
		this.capsule = capsule;
		
		parts = new HashSet<ModelPart>();
		partType = new HashMap<ModelPart, Class>();		
		engines = new HashMap<Class, Engine>();		
		partCapsule = new HashMap<ModelPart, Class>();
		connectionRelationBasic = new HashMap<Class, ConnectionRelationBasic>();
		actionsMap = new HashMap<ModelPart, ActionMapping>();
		parentRelation = new HashMap<ModelPart, Set<ModelPart>>();
		
		topLevelPart = new ModelPart(new ArrayList<Property>());
		
		//process top level capsule
//		capsuleClass = capsule.getUML2Class();
		capsuleClass = capsule; //MyUmlrtUtil.getUML2Class(capsule);
		
		partType.put(topLevelPart,capsuleClass);
		
		//create engines
		Engine engine = new Engine(capsule);
		engines.put(capsuleClass, engine);
		createInnerEngines(capsule);
		
		//create map for trees
		trees = new SymbolicTreesMap(engines,partType,actionsMap);
		
		//adapt capsule and all parts
		adaptCapsule(capsule, topLevelPart);	
		
		//prepare trees for parts
		
		
		//displayInfo();
		
		//create generalized connection relation
		generalizeConnectionRelation();
	}
	
	

	private void createInnerEngines(Class capsule) throws SMValidationException {
		for (Property pp : UmlrtUtil.getCapsuleParts(capsule)) {
			if (pp.getType() != null && pp.getType() instanceof Class) {
				Class partClass = (Class) pp;
				if (UmlrtUtil.isCapsule(partClass)) {
					// Class pc =
					// RTFactory.CapsuleFactory.getCapsule(partClass);

					if (!engines.containsKey(partClass)) {
						Engine engine = new Engine(partClass);
						engines.put(partClass, engine);
					}
					createInnerEngines(partClass);
				}

			}
		}
	}



	private void generalizeConnectionRelation() {
		connRelation = new ConnectionRelation();
		
		//generalize connection relation for topLevel
		
		ConnectionRelationBasic basic = connectionRelationBasic.get(capsule);
					

		ConnectionRelation cr = new ConnectionRelation(basic, topLevelPart.getPath(), this);
		connRelation.union(cr);
		
		
		//create connection relation for all parts
		for (ModelPart mp : parts){
			
			//find basic connection relation
			Class capsule = partCapsule.get(mp);
			basic = connectionRelationBasic.get(capsule);
						

			cr = new ConnectionRelation(basic, mp.getPath(), this);
			connRelation.union(cr);
			
		}
		
	}



	/* JDA: begin removal
	private void displayInfo() {
		for (ModelPart prop : parts){
			System.out.println(prop);
		}
		
		System.out.println();
		
		//connRelation.displayInfo();
		
	}
   JDA: end removal */


	private void adaptCapsule(Class capsule, ModelPart currentPath) throws Exception {				
		//find parts
		Set<ModelPart> mps = extractParts(capsule, currentPath);
				
		//create parent-children relation
		parentRelation.put(currentPath, mps);
		
		//create connection		
		ConnectionRelationBasic connRelation = new ConnectionRelationBasic();
		connRelation.createConnectionRelation(capsule,engines, currentPath.isTopLevel());
		
		//create actions mapping
		ActionMapping actions = new ActionMapping();
		Set<ActionInput> inputs = engines.get(capsule).getAllInputActions();
		actions.createInputs(inputs);
		Set<ActionOutput> outputs = engines.get(capsule).getAllOutputActions();
		actions.createOutputs(outputs);
		
		actionsMap.put(currentPath,actions);
		
		//actionMapGeneral.union(actions);
		
		connectionRelationBasic.put(capsule, connRelation);
		
		for (ModelPart part : mps){
			adaptCapsule(partCapsule.get(part),part);
		}
	}	
	


//	private Set<ModelPart> extractParts(Class container, ModelPart mp) throws SMValidationException {
//		Set<ModelPart> newParts = new HashSet<ModelPart>();
//		
//		if (container.getAllRoles() == null){
//			return newParts;
//		}		
//		
//		//discover all parts
//		for (RTCapsulePart part : container.getAllRoles()){
//			if (part != null && part.getType() != null && part.getType().getValue() instanceof Class){
//			
//				
//				//class for a part
//				Class partClass = (Class) part.getType().getValue();				
//				
//				//if it is a capsule
//				if (!RTFactory.CapsuleFactory.isCapsule(partClass)){
//					continue;
//				}
//				//property 
//				Property prop = part.getLocalRedefinition();
//				
//				//create new part
//				ModelPart mpAppend = new ModelPart(mp,prop, container.getUML2Class().getName());
//				
//				//update maps
//				parts.add(mpAppend);				
//				partType.put(mpAppend,partClass);
//				
//				//capsuel
//				
//				Class partCap = RTFactory.CapsuleFactory.getCapsule(partClass);
//				
//				partCapsule.put(mpAppend, partCap);
//				newParts.add(mpAppend);
//				
//				//check whether caosules is new
//				/*if (!engines.containsKey(partClass)){					
//					
//					//generate engine
//					Engine engine = new Engine(partCap);				
//					engines.put(partClass, engine);
//				}*/								
//													
//			}			
//		}
//		
//		return newParts;
//				
//	}

	private Set<ModelPart> extractParts(Class container, ModelPart mp) throws SMValidationException {
		Set<ModelPart> newParts = new HashSet<ModelPart>();
		
		List<Property> capsuleParts = UmlrtUtil.getCapsuleParts(container); 
		
		if (capsuleParts == null){
			return newParts;
		}		
		
		//discover all parts
		for (Property part : capsuleParts){
			if (part != null && part.getType() != null && part.getType() instanceof Class){
			
				
				//class for a part
				Class partClass = (Class) part.getType();				
				
				//if it is a capsule
				if (UmlrtUtil.isCapsule(partClass)){
					continue;
				}
				//property 
				Property prop = part;
				
				//create new part
				ModelPart mpAppend = new ModelPart(mp,prop, container.getName());
				
				//update maps
				parts.add(mpAppend);				
				partType.put(mpAppend,partClass);
				
				//capsuel
				
				Class partCap = partClass;// RTFactory.CapsuleFactory.getCapsule(partClass);
				
				partCapsule.put(mpAppend, partCap);
				newParts.add(mpAppend);
				
				//check whether caosules is new
				/*if (!engines.containsKey(partClass)){					
					
					//generate engine
					Engine engine = new Engine(partCap);				
					engines.put(partClass, engine);
				}*/								
													
			}			
		}
		
		return newParts;
				
	}


	public ActionInput getInputFromString(String input, Property part) {		
		//find engine
		Class c;
		if (part!=null)
			c = (Class) part.getType();
		else 
			c = capsuleClass;
		
		Engine e = engines.get(c);
		
		//get input action
		return e.getActionInputFromString(input.substring(0,input.indexOf('.')), input.substring(input.indexOf('.')+1));
	}



	public ModelPart getConnectedPart(ActionInput a) {
		return connRelation.getConnectedActionPart(a);		
	}



	public ModelPart findModelPart(List<Property> path) {
		if (path == null || path.isEmpty()){
			return topLevelPart;
		}
		for (ModelPart mp : parts){
			if (mp.hasPath(path)){
				return mp;
			}
		}
		return null;
	}



	public ModelPart getConnectedPart(ActionInput a, List<Property> atPart) {
		ModelPart mp = findModelPart(atPart);
		ActionInput ai = actionsMap.get(mp).getInput(a);
		ModelPart part = connRelation.getConnectedActionPart(ai);
		
		return part;
	}



	public void initializeTrees(Set<ModelPart> initParts) {
		//tree for the main capsule
		trees.generateTree(topLevelPart);
						
		//tree for each part
		for (ModelPart mp : initParts){			
			trees.generateTree(mp);
		}
		
		//System.out.println(trees.display());
	}



	private Set<ModelPart> getParents(ModelPart mp) {
		Set<ModelPart> parents = new HashSet<ModelPart>();
		
		if (mp.isTopLevel()){
			return parents;
		}
		List<Property> path = new ArrayList<Property>(mp.getPath());
		path.remove(path.size()-1);		
		
		while (!path.isEmpty()){
			parents.add(findModelPart(path));
			path.remove(path.size()-1);
		}
		return parents;
	}



	public SymbolicTreesMap getTrees() {
		return trees;
	}



	public Set<ModelPart> getParents(Set<ModelPart> initParts) {
		Set<ModelPart> result = new HashSet<ModelPart>(initParts);
		for (ModelPart mp : initParts){
			result.addAll(getParents(mp));
		}
		return result;
	}



	public ActionInput getInputFromPart(ActionInput action,
			ModelPart part) {
		/*if (part==topLevelPart) {
			return action;
		}*/
		return actionsMap.get(part).getInput(action);
	}



	public ActionOutput getOutput(ActionOutput action, ModelPart part) {
		/*if (part == topLevelPart)
			return action;*/
		return actionsMap.get(part).getOutput(action);
	}



	public ModelPart findSender(ActionInputSymbolic input) {
		ActionInput action = input.getAction();
		
		return connRelation.getSender(action);
	}



	public ModelPart getTopLevelPart() {
		return topLevelPart;
	}



	public ActionInput getConnectedInputAction(ActionOutput ao) {
		return connRelation.getConnectedInputAction(ao);
	}



	public ModelPart getPart(ActionInput ai) {
		return connRelation.getPart(ai);
	}



	public SymbolicExecutionTree generateTree(ModelPart part) {
		return trees.generateTree(part);		
	}



	public ModelPart getModelPartFromString(List<String> propertiesString) {
		for (ModelPart mp : parts){
			if (mp.equalsString(propertiesString)){
				return mp;
			}
		}
		return null;
	}



	public Location findLocationFromName(String stateName, ModelPart mp) {
		Engine engine = engines.get(partCapsule.get(mp));		
		Location loc = engine.getLocationFromName(stateName);
		return loc;
	}



	public ActionInput getInputFromString(String port, String signal,
			ModelPart mp) {		
		//get engine
		Engine e = getEngineForPart(mp);
		if (e == null)
			return null;
		
		//get input action
		ActionInput ai = e.getActionInputFromString(port,signal);
		
		return actionsMap.get(mp).getInput(ai);
	
	}



	public ActionOutput getOutputFromString(String port, String signal,
			ModelPart mp) {
		//find engine
		//get engine
		Engine e = getEngineForPart(mp);
		if (e == null)
			return null;
				
		//get inp
		ActionOutput ao = e.getActionOutputFromString(port,signal); 
		return actionsMap.get(mp).getOutput(ao);
	}



	public LocationVariable<?> getAttribute(Variable var, ModelPart mp) {
		Engine e = getEngineForPart(mp);
		return e.getAttribute(var.getName());
		
	}

	public Engine getEngineForPart(ModelPart mp){
		Class c;
		if (mp!=null)
			c = (Class) mp.getLastPart().getType();
		else 
			c = capsuleClass;
		
		return engines.get(c);
		
	}



	public Set<ModelPart> getChildren(ModelPart mp){
		return parentRelation.get(mp);
	}



	public Set<ModelPart> getAllParts() {
		Set<ModelPart> allParts = new HashSet<ModelPart>(parts);
		allParts.add(topLevelPart);
		return allParts;
	}



	public boolean isRelayAction(ActionInputSymbolic input) {
		return connRelation.isRelayExternal(input.getAction());
		
	}

	
	
}
