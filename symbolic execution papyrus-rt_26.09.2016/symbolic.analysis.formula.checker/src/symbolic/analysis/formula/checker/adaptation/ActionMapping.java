package symbolic.analysis.formula.checker.adaptation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;

public class ActionMapping {
	private Map<ActionInput, ActionInput> inputActions;
	
	private Map<ActionOutput,ActionOutput> outputActions;
	
	public ActionMapping(){
		inputActions = new HashMap<ActionInput, ActionInput>();
		outputActions = new HashMap<ActionOutput, ActionOutput>();
	}
	
	public void createInputs(Set<ActionInput> actions){
		for (ActionInput action : actions){
			ActionInput newAction = new ActionInput(action);
			inputActions.put(action, newAction);
		}
	}
	
	public void createOutputs(Set<ActionOutput> actions){
		for (ActionOutput action : actions){
			ActionOutput newAction = new ActionOutput(action);
			outputActions.put(action, newAction);
		}
	}

	public ActionInput getInput(ActionInput action) {
		return inputActions.get(action);
	}

	public ActionOutput getOutput(ActionOutput action) {
		return outputActions.get(action);
	}

	public void union(ActionMapping actions) {
		inputActions.putAll(actions.inputActions);
		outputActions.putAll(actions.outputActions);
		
	}

	public Map<ActionInput, ActionInput> getInputMap() {
		return inputActions;
	}

	public Map<ActionOutput, ActionOutput> getOutputMap() {
		return outputActions;
	}

}
