package symbolic.analysis.formula.checker.adaptation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.rmi.CORBA.PortableRemoteObjectDelegate;

//import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.Capsule;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
//import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.RedefinableElement;
import org.eclipse.uml2.uml.internal.operations.RedefinableElementOperations;

//import com.ibm.xtools.uml.rt.core.Capsule;
//import com.ibm.xtools.uml.rt.core.RTConnectorRedefinition;
//import com.ibm.xtools.uml.rt.core.RTFactory;
//import com.ibm.xtools.uml.rt.core.RTPortRedefinition;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.tree.synch.model.RelayActions;
import symbolic.execution.umlrt.engine.Engine;
import symbolic.execution.umlrt.model.ActionOutputSM;
import symbolic.execution.umlrt.utils.Pair;
import symbolic.execution.utils.UmlrtUtil;

public class ConnectionRelationBasic {
	
	private Map<ActionOutput, ActionInput> connectedActions;
	
	private Map<ActionOutput, ActionInput> connectedActionsDeep;
	
	private Map<ActionOutput,Property> outputParts;
	
	private Map<ActionOutput,List<Property>> outputPartsDeep;
	
	private Map<ActionInput, Property> inputParts;
	
	private Map<ActionInput, List<Property>> inputPartsDeep;
	
	private Map<ActionInput,ActionOutput> reverseConnectedActions;
	
	private Map<Pair<ActionOutput, Property>, Pair<ActionInput, Property>> connections;
	
	private Map<Pair<ActionOutput, List<Property>>, Pair<ActionInput, List<Property>>> connectionsDeep;
	
	private Set<Pair<ActionInput, List<Property>>> relays;
	
	public ConnectionRelationBasic(){
		connectedActions = new HashMap<ActionOutput, ActionInput>();
		connectedActionsDeep = new HashMap<ActionOutput, ActionInput>();
		outputParts = new HashMap<ActionOutput, Property>();
		outputPartsDeep = new HashMap<ActionOutput, List<Property>>();
		inputParts = new HashMap<ActionInput, Property>();
		inputPartsDeep = new HashMap<ActionInput, List<Property>>();
		reverseConnectedActions = new HashMap<ActionInput, ActionOutput>();
		connections = new HashMap<Pair<ActionOutput,Property>, Pair<ActionInput,Property>>();
		connectionsDeep = new HashMap<Pair<ActionOutput,List<Property>>, Pair<ActionInput,List<Property>>>();
		relays = new HashSet<Pair<ActionInput,List<Property>>>();
	}
	
	
	public void createConnectionRelation(Class capsule, Map<org.eclipse.uml2.uml.Class,Engine> engines, boolean isTopCapsule){
		for (Connector connector : capsule.getOwnedConnectors()){
			ConnectorEnd connEnd1 = connector.getEnds().get(0);
			ConnectorEnd connEnd2 = connector.getEnds().get(1);			
			
			if (connEnd1 == null || connEnd2==null){
				continue;
			}
			
			Port portBase = null;
			Port portConj = null;
			
			Port portOuter = null;
			Port portInner = null;
			
			Property partBase = null;
			Property partConj = null;
			
			Property partInner = null;
			Property partOuter = null;
			
			org.eclipse.uml2.uml.Class capsuleBase = null;
			org.eclipse.uml2.uml.Class capsuleConj = null;
			
			
			ConnectorEnd endBase = null;
			ConnectorEnd endConj = null;
			
			ConnectorEnd endInner = null;
			ConnectorEnd endOuter = null;
			
			boolean isRelay1 = false;
			boolean isRelay2 = false;
			
			boolean isOuter1 = false;
			boolean isOuter2 = false;
			//check if it is a top relay connection
			
			
			//check port on end 1 				
			if (connEnd1.getRole() instanceof Port){
				
				
				//find port
				Port portGeneral = (Port) connEnd1.getRole();	
//				RTPortRedefinition port = RTFactory.redefFactory.getPortRedefinition(portGeneral, capsule);
				Port port = portGeneral.getRedefinedPort(portGeneral.getName(), portGeneral.getType());
				
				if (port.getName().equals("callControlO1")){
					System.out.println();;
				}
				org.eclipse.uml2.uml.Class endCapsule  = null;
				Property prop = connEnd1.getPartWithPort();
				if (prop != null){					
					 endCapsule = ((org.eclipse.uml2.uml.Class)prop.getType());
					
				} else {
					endCapsule = capsule; //MyUmlrtUtil.getUML2Class(capsule);
				}
				if (!port.isConjugated()){
					portBase = port; //port.getLocalRedefinition();
					partBase = prop;
					capsuleBase = endCapsule;
					endBase = connEnd1;
										
				} else {
					portConj = port;// port.getLocalRedefinition();
					partConj = prop;
					capsuleConj = endCapsule;
					endConj = connEnd1;
				}
				if (!port.isBehavior()){
					isRelay1 = true;
				}
					if (capsule.getAllAttributes().contains(portGeneral)){
						isOuter2 = true;
					
						portOuter = portGeneral;
						partOuter = prop;	
						endOuter = connEnd1;
					} else {
						portInner = portGeneral;
						partInner = prop;
						endInner = connEnd1;
					}
					if (isOuter1){
						portInner = portGeneral;
						partInner = prop;
						endInner = connEnd1;
					}
					
				
				
			}
			
			if (connEnd2.getRole() instanceof Port){
				//find port
				Port portGeneral = (Port) connEnd2.getRole();				
//				RTPortRedefinition port = RTFactory.redefFactory.getPortRedefinition(portGeneral, capsule);
				Port port = (Port) portGeneral.getRedefinedPort(portGeneral.getName(), portGeneral.getType());
				
				
				org.eclipse.uml2.uml.Class endCapsule  = null;
				Property prop = connEnd2.getPartWithPort();;
				if (prop != null){					
					endCapsule = (org.eclipse.uml2.uml.Class)prop.getType();
					
				} else {
					endCapsule = capsule;// MyUmlrtUtil.getUML2Class(capsule);
				}
				
				
				if (!port.isConjugated()){
					portBase = port;// port.getLocalRedefinition();
					partBase = prop;
					capsuleBase = endCapsule;
					endBase = connEnd2;
										
				} else {
					portConj = port;// port.getLocalRedefinition();
					partConj = prop;
					capsuleConj = endCapsule;
					endConj = connEnd2;
				}
				
				
					if (!port.isBehavior()){
						isRelay2 = true;
					}
					if (capsule.getAllAttributes().contains(portGeneral)){
						isOuter2 = true;
						portOuter = portGeneral;
						partOuter = prop;
						endOuter = connEnd2;
					} else {
						portInner = portGeneral;
						partInner = prop;
						endInner = connEnd2;
					}
				
					
			}
			
			//check for the relay in the top connection (and if 
			
			if ((isRelay1 || isRelay2) && (portBase != null && portConj != null)){
				//rplace base
					List<Property> basePath = new ArrayList<Property>();
					List<Property> conjPath = new ArrayList<Property>();
					if (portBase == null || portConj == null){
						System.out.println("Here");
					}
					if (!portBase.isBehavior()){
						Pair<ConnectorEnd,Port> base = findRelayEndForPort(endBase, portBase, null, basePath);
						endBase = base.getFirst();
						partBase = endBase.getPartWithPort();
						capsuleBase = (Class) partBase.getType();
						portBase = base.getSecond();
					}
					if (!portConj.isBehavior()){
					Pair<ConnectorEnd,Port> conj = findRelayEndForPort(endConj, portConj, null, conjPath); 
				
						 endConj = conj.getFirst();
						 partConj = endConj.getPartWithPort();
						 capsuleConj = (Class) partConj.getType();
						 portConj = conj.getSecond();					
					
					}
					isRelay1 = false;
					isRelay2 = false;
					
					Engine engineBase = engines.get(capsuleBase);
					Engine engineConj = engines.get(capsuleConj);
					
					Set<ActionOutput> outputBase = engineBase.getOutputActions(portBase);
					for (ActionOutput action : outputBase){
						if (action instanceof ActionOutputSM){
							ActionInput input = engineConj.getActionInput(portConj,((ActionOutputSM)action).getSignal());						
							connectedActionsDeep.put(action, input);
							reverseConnectedActions.put(input,action);
							
							outputPartsDeep.put(action, basePath);
							inputPartsDeep.put(input, conjPath);
							
							connectionsDeep.put(new Pair(action, basePath), new Pair(input, conjPath));
						}				
					}
				
				//find connected actions for conj port
					Set<ActionOutput> outputConj = engineConj.getOutputActions(portConj);
					for(ActionOutput output : outputConj){
						if (output instanceof ActionOutputSM){
							ActionInput input = engineBase.getActionInput(portBase, ((ActionOutputSM)output).getSignal());
							connectedActionsDeep.put(output,input);
							reverseConnectedActions.put(input,output);
							
							outputPartsDeep.put(output, conjPath);
							inputPartsDeep.put(input, basePath);
							
							connectionsDeep.put(new Pair(output, conjPath), new Pair(input, basePath));
						}
					}
					continue;
			} 
			if (isRelay1 || isRelay2){
				if (isTopCapsule){					
					
					//both are relay (outer action)
					List<Property> path = new ArrayList<Property>(); 
					if (portInner == null){
						System.out.println("Here");
					}
					Pair<ConnectorEnd,Port> inner = findRelayEndForPort(endInner, portInner, null,path);
					
					Port innerPort = inner.getSecond();
					ConnectorEnd innerEnd = inner.getFirst();
					Property innerPart = innerEnd.getPartWithPort();
					Class innerCapsule = (Class) innerPart.getType();
					
					Engine engine = engines.get(innerCapsule);
					
					
					Set<ActionInput> inputActions = engine.getInputActions(innerPort);
					for (ActionInput ai : inputActions){
						relays.add(new Pair<ActionInput, List<Property>>(ai,path));
					}
				}
				
				
				
				continue;
			}
			
			
			
			Engine engineBase = engines.get(capsuleBase);
			Engine engineConj = engines.get(capsuleConj);
			
			//check if relay
			if (isRelay1 || isRelay2){
				/*Set<ActionInput> inputsRelay;
				
				if (partBase!=null){
					inputsRelay = engineBase.getInputActions(portBase);										
				} else {
					inputsRelay = engineConj.getInputActions(portConj);					
				}
				*/
				
			} else {			
			//find connected actions for base port
				
				Set<ActionOutput> outputBase = engineBase.getOutputActions(portBase);
				for (ActionOutput action : outputBase){
					if (action instanceof ActionOutputSM){
						ActionInput input = engineConj.getActionInput(portConj,((ActionOutputSM)action).getSignal());						
						connectedActions.put(action, input);
						reverseConnectedActions.put(input,action);
						
						outputParts.put(action, partBase);
						inputParts.put(input, partConj);
						
						connections.put(new Pair(action, partBase), new Pair(input, partConj));
					}				
				}
			
			//find connected actions for conj port
				Set<ActionOutput> outputConj = engineConj.getOutputActions(portConj);
				for(ActionOutput output : outputConj){
					if (output instanceof ActionOutputSM){
						ActionInput input = engineBase.getActionInput(portBase, ((ActionOutputSM)output).getSignal());
						connectedActions.put(output,input);
						reverseConnectedActions.put(input,output);
						
						outputParts.put(output, partConj);
						inputParts.put(input, partBase);
						
						connections.put(new Pair(output, partConj), new Pair(input, partBase));
					}
				}
			}
		}

	}


	private Pair<ConnectorEnd, Port> findRelayEndForPort(ConnectorEnd end, Port port, ConnectorEnd previous,
			List<Property> path)  {
		if (port.isBehavior() ){
			if (port.isService()){
				path.add(end.getPartWithPort());
				return new Pair<ConnectorEnd,Port>(end,port);
			}
			else
				return new Pair<ConnectorEnd,Port>(previous,port);
		} 
		
		
		Property partWithPort = end.getPartWithPort();
		path.add(partWithPort);
		
		Class owner = (Class) partWithPort.getType();
		Class ownerCapsule = UmlrtUtil.getCapsule(owner); //RTFactory.CapsuleFactory.getCapsule(owner);
		for (Connector conn : ownerCapsule.getOwnedConnectors()){
			
			ConnectorEnd end1 = conn.getEnds().get(0);
			ConnectorEnd end2 = conn.getEnds().get(1);
			
			
			Port p1 = (Port) end1.getRole();
			Port p2 = (Port) end2.getRole();
			
			if (p1.equals(port)){
				return findRelayEndForPort(end2, p2, end, path);
			}
			if (p2.equals(port)){
				return findRelayEndForPort(end1, p1, end, path);
			}
			
		}
		return null;
	}


	/* JDA: remove
	private ConnectorEnd findConnectedEnd(ConnectorEnd end) {
		Connector connector = (Connector) end.getOwner();
		
		if (connector.getEnds().get(0).equals(end))
			return connector.getEnds().get(1);
		if (connector.getEnds().get(1).equals(end))
			return connector.getEnds().get(0);
		
		return null;
	}
    JDA: end remove */

	public void displayInfo() {
		System.out.println(connectedActions);
		
		System.out.println(outputParts);
		
		System.out.println(inputParts);
		
		/*
		for (ActionOutput act : connectedActions.keySet()){
			ActionInput in = connectedActions.get(act);
			System.out.println("Connection :" + act.getName() + ": " + outputParts.get(act).getName() + " toooo " + in.getName() + ": " + inputParts.get(in).getName());
		}*/
		
	}


	public Property getConnectedActionPart(ActionInput a) {
		ActionOutput out = reverseConnectedActions.get(a);
		
		if (out != null){
			return outputParts.get(out);
		}
		return null;
	}


	public Map<ActionOutput,ActionInput> getConn() {
		return connectedActions;
	}


	public Property getPartInput(ActionInput action) {
		return inputParts.get(action);
	}


	public Property getPartOutput(ActionOutput action) {
		return outputParts.get(action);
	}


	public Map<Pair<ActionOutput, Property>, Pair<ActionInput, Property>> getConnections() {
		return connections;
	}


	public Set<Pair<ActionInput,List<Property>>> getRelays() {
		return relays;
	}


	public Map<Pair<ActionOutput, List<Property>>, Pair<ActionInput, List<Property>>> getConnectionsDeep() {
		return connectionsDeep;
	}

}
