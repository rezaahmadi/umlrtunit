package symbolic.analysis.formula.checker.adaptation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.Property;

//import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.umlrt.utils.Pair;

public class ConnectionRelation {
	
	private Map<ActionOutput,ActionInput> conn;
	
	private Map<ActionInput, ActionOutput> reverseConn;
	
	private Map<ActionInput, ModelPart> inputPart;
	
	private Map<ActionOutput, ModelPart> outputPart;
	
	private Set<ActionInput> relayActions;
	
	public ConnectionRelation(ConnectionRelationBasic basic, List<Property> path, ModelAdapted model) {
		this();
		
		//prepare prefix
		List<Property> prefix = new ArrayList<Property>(path);
		//prefix.remove(path.size()-1);
		
		for (Map.Entry<Pair<ActionOutput, Property>,Pair<ActionInput,Property>> c : basic.getConnections().entrySet()){
			//find parts
			Property inputPartSuffix = c.getValue().getSecond();
			Property outputPartSuffix = c.getKey().getSecond();
			
			//find input model part	
			if (inputPartSuffix != null){
				prefix.add(inputPartSuffix);
			}
			ModelPart inputModelPart  = model.findModelPart(prefix);
			if (inputPartSuffix != null)
				prefix.remove(inputPartSuffix);
			
			//find output model part
			if (outputPartSuffix != null)
				prefix.add(outputPartSuffix);
			ModelPart outputModelPart = model.findModelPart(prefix);
			if (outputPartSuffix != null)
				prefix.remove(outputPartSuffix);
			
			//find actions
			ActionInput ai = model.getInputFromPart(c.getValue().getFirst(), inputModelPart);
			ActionOutput ao = model.getOutput(c.getKey().getFirst(), outputModelPart);
			
			
			
			//create entries
			conn.put(ao, ai);
			reverseConn.put(ai,ao);
			
			inputPart.put(ai,inputModelPart);
			outputPart.put(ao,outputModelPart);
			
		}
	
		//for deep actions
		for (Map.Entry<Pair<ActionOutput, List<Property>>,Pair<ActionInput,List<Property>>> c : basic.getConnectionsDeep().entrySet()){
			//find parts
			List<Property> inputPartSuffix = c.getValue().getSecond();
			List<Property> outputPartSuffix = c.getKey().getSecond();
			
			//find input model part	
			if (inputPartSuffix != null){
				prefix.addAll(inputPartSuffix);
			}
			ModelPart inputModelPart  = model.findModelPart(prefix);
			if (inputPartSuffix != null)
				prefix.removeAll(inputPartSuffix);
			
			//find output model part
			if (outputPartSuffix != null)
				prefix.addAll(outputPartSuffix);
			ModelPart outputModelPart = model.findModelPart(prefix);
			if (outputPartSuffix != null)
				prefix.removeAll(outputPartSuffix);
			
			//find actions
			ActionInput ai = model.getInputFromPart(c.getValue().getFirst(), inputModelPart);
			ActionOutput ao = model.getOutput(c.getKey().getFirst(), outputModelPart);
			
			
			
			//create entries
			conn.put(ao, ai);
			reverseConn.put(ai,ao);
			
			inputPart.put(ai,inputModelPart);
			outputPart.put(ao,outputModelPart);
			
		}
		

		
		for (Pair<ActionInput,List<Property>> act : basic.getRelays()){
			List<Property> inputPartSuffix = act.getSecond();
			if (inputPartSuffix != null){
				prefix.addAll(inputPartSuffix);
			}
			ModelPart inputModelPart  = model.findModelPart(prefix);
			if (inputPartSuffix != null)
				prefix.removeAll(inputPartSuffix);
			ActionInput ai = model.getInputFromPart(act.getFirst(), inputModelPart);
			
			relayActions.add(ai);
		}
		
	}

	public ConnectionRelation() {
		conn = new HashMap<ActionOutput, ActionInput>();
		reverseConn = new HashMap<ActionInput, ActionOutput>();
		inputPart = new HashMap<ActionInput, ModelPart>();
		outputPart = new HashMap<ActionOutput, ModelPart>();
		relayActions = new HashSet<ActionInput>();
	}

	public void union(ConnectionRelation cr) {
		conn.putAll(cr.conn);
		reverseConn.putAll(cr.reverseConn);
		inputPart.putAll(cr.inputPart);
		outputPart.putAll(cr.outputPart);
		relayActions.addAll(cr.relayActions);
		
	}

	public ModelPart getConnectedActionPart(ActionInput a) {
		return outputPart.get(reverseConn.get(a));
	}

	public ModelPart getSender(ActionInput action) {
		ActionOutput connected = reverseConn.get(action);
		if (connected != null)
			return outputPart.get(connected);
		return null;
	}

	public ActionInput getConnectedInputAction(ActionOutput ao) {
		return conn.get(ao);
	}

	public ModelPart getPart(ActionInput ai) {		
		return inputPart.get(ai);
	}

	public boolean isRelayExternal(ActionInput input) {
		return relayActions.contains(input);
	}
	

}
