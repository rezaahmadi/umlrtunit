package symbolic.analysis.formula.checker.initialization;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.uml2.uml.Property;

import symbolic.analysis.formula.checker.adaptation.ModelAdapted;
import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.InOut;
import symbolic.analysis.language.InQueue;
import symbolic.analysis.language.InState;
import symbolic.analysis.language.Invariant;
import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.CompositeFormula;
import symbolic.execution.ffsm.model.ActionInput;

public class InitialPartsExtractor {
	private ModelAdapted model;
	
	public InitialPartsExtractor(ModelAdapted model){
		this.model = model;
	}
	
	public Set<ModelPart> extract(Formula f){
		Set<ModelPart> result = new HashSet<ModelPart>();
		
		if (f instanceof AtomicStateFormula){
			result.addAll(extract((AtomicStateFormula)f));
		} else if (f instanceof CompositeFormula){
			CompositeFormula sf = (CompositeFormula) f;
			result.addAll(extract(sf.getLeft()));
			result.addAll(extract(sf.getRight()));
		} else if (f instanceof Formula){
			result.addAll(extract(f.getLeft()));
			result.addAll(extract(f.getRight()));
		} 
		//result = model.getParents(result);
		//result.add(model.getTopLevelPart());
		return result;
	}
	
	public Set<ModelPart> extract(InState f){
		Set<ModelPart> result = new HashSet<ModelPart>();
		List<Property> p = f.getAtPart().getPath();		
		if (p !=null) {
			ModelPart mp = model.findModelPart(p);
			result.add(mp);
		}
		return result;
	}
	
	
	public Set<ModelPart> extract(Invariant f){
		Set<ModelPart> result = new HashSet<ModelPart>();
		List<Property> p = f.getAtPart().getPath();		
		if (p !=null) {
			ModelPart mp = model.findModelPart(p);
			result.add(mp);
		}
		return result;
	}


	public Set<ModelPart> extract(AtomicStateFormula f) {		
		if (f instanceof InState){
			return extract((InState)f);
		} else if (f instanceof Invariant){
			return extract((Invariant)f);			
		} else if (f instanceof InQueue){
			return extract((InQueue)f);
		} else if (f instanceof InOut){
			return extract((InOut)f);
		}
		return null;
	}
	
	public Set<ModelPart> extract(InQueue f){
		Set<ModelPart> result = new HashSet<ModelPart>();
		List<Property> p = f.getAtPart().getPath();		
		if (p !=null) {
			ModelPart mp = model.findModelPart(p);
			result.add(mp);
		}		
		
		for (ActionInput a : f.getSequence()){
			//find output
			ModelPart partIn = model.getConnectedPart(a, f.getAtPart().getPath());
			if (partIn != null){
				result.add(partIn);
			}
			
		}
		
		return result;
	}
	
	public Set<ModelPart> extract(InOut f){
		Set<ModelPart> result = new HashSet<ModelPart>();
		List<Property> p = f.getAtPart().getPath();		
		if (p !=null) {
			ModelPart mp = model.findModelPart(p);
			result.add(mp);
		}	
		
		ModelPart partInput = model.getConnectedPart(f.getInputAction(), f.getAtPart().getPath());
		if (partInput != null){
			result.add(partInput);
		}
		
		return result;
	}
	

}
