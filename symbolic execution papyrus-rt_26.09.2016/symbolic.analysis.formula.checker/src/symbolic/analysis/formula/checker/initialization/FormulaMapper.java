package symbolic.analysis.formula.checker.initialization;

//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
import java.util.Set;

//import org.eclipse.uml2.uml.Property;

import symbolic.analysis.formula.checker.adaptation.ModelAdapted;
import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.analysis.formula.checker.model.AnyInputAction;
import symbolic.analysis.language.Action;
import symbolic.analysis.language.ActionList;
import symbolic.analysis.language.AtPart;
import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.Formula;
import symbolic.analysis.language.InOut;
import symbolic.analysis.language.InQueue;
import symbolic.analysis.language.InState;
import symbolic.analysis.language.Invariant;
import symbolic.analysis.language.CompositeFormula;
import symbolic.analysis.language.Variable;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;

public class FormulaMapper {
	
	private ModelAdapted model;
	
	
	public FormulaMapper(ModelAdapted model){
		this.model = model;
	}
	
	public void map(Formula formula){
		if (formula instanceof AtomicStateFormula){
			if (formula instanceof InState){
				map((InState)formula);
			} else if (formula instanceof InOut){
				map((InOut)formula);
			} else if (formula instanceof InQueue){
				map((InQueue)formula);
			} else if (formula instanceof Invariant){
				map((Invariant)formula);
			}
		} else if (formula instanceof CompositeFormula){
			CompositeFormula sf = (CompositeFormula)formula;
			map(sf.getRight());
			map(sf.getLeft());
		} else if (formula instanceof Formula){
			map(formula.getLeft());
			map(formula.getRight());
		}
	}
	
	private void map(InState formula){		
		//map at part
		ModelPart mp = map(formula.getAtPart());
		
		//find state
		Location l =model.findLocationFromName(formula.getStateName(), mp);
		
		formula.setLocation(l);
		
	}
	
	private void map(InOut formula){
		ModelPart mp = map(formula.getAtPart());
		
		Action inputAction = formula.getActionInput();		
		mapInput(inputAction,mp);
		
		ActionList outs = formula.getOutputList(); 
		mapActionListOutputs(outs, mp);
		
		
	}
	
	private void map(InQueue formula){
		ModelPart mp = map(formula.getAtPart());
		
		mapActionListInput(formula.getActions(),mp);
	}
	
	private void map(Invariant formula){
		Set<Variable> vars = formula.getVariables();
		
		ModelPart mp = map(formula.getAtPart());
		
		for (Variable var : vars){
			LocationVariable<?> locVar = model.getAttribute(var,mp);
			var.setVariable(locVar);
		}
		
	}
	
	private void mapActionListInput(ActionList actions, ModelPart mp) {
		for (Action act : actions.getActions()){
			mapInput(act, mp);
		}
		
	}

	private void mapActionListOutputs(ActionList outs, ModelPart mp) {
		for (Action act : outs.getActions()){
			mapOutput(act,mp);
		}
	}

	private void mapOutput(Action act, ModelPart mp) {
		if (act.getPort().equals("any")){
			act.setAction(new AnyOutputAction());
		}
		ActionOutput ao  = model.getOutputFromString(act.getPort(),act.getSignal(),mp);
		act.setAction(ao);
	}

	private void mapInput(Action inputAction, ModelPart mp) {
		if (inputAction.getPort().equals("any")){
			inputAction.setAction(new AnyInputAction());
		} else {
		
		ActionInput ai = model.getInputFromString(inputAction.getPort(),inputAction.getSignal(), mp);
		
		inputAction.setAction(ai);
		}
		
	}

	private ModelPart map(AtPart atPart){		
		ModelPart mp = model.getModelPartFromString(atPart.getPropertiesString());		
		atPart.setPath(mp.getPath());
		
		return mp;
		
	}
	

}
