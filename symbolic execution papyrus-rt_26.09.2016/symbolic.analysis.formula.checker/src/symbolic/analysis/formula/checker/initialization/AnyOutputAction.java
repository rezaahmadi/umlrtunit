package symbolic.analysis.formula.checker.initialization;

import java.util.HashSet;

//import symbolic.execution.ffsm.model.Action;
//import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;

public class AnyOutputAction extends ActionOutput {
	public AnyOutputAction() {		
		super(new ActionOutput("","any", new HashSet<ActionVariableOutput<?>>()));
	}
}
