package symbolic.analysis.formula.checker.exploration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.eclipse.core.internal.dtree.NodeComparison;
//import org.eclipse.uml2.uml.Property;

import symbolic.analysis.formula.checker.adaptation.ModelAdapted;
import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.analysis.formula.checker.labeling.Labeler;
import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
import symbolic.analysis.formula.checker.model.MinimalCompositeSET;
import symbolic.analysis.formula.checker.model.NodeSymbolicCheck;
import symbolic.analysis.formula.checker.model.PullTransition;
import symbolic.analysis.formula.checker.model.Queues;
import symbolic.analysis.formula.checker.process.SymbolicTreesMap;
//import symbolic.analysis.language.Formula;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.ActionInputTimeout;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.OutputSequenceSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.tree.synch.SynchronizationAction;
import symbolic.execution.tree.synch.UnreceivedAction;
import symbolic.execution.umlrt.utils.Pair;

/* ====================================================================================================================
History:
 	Bugs 2: Added null parameter to account for changes.
======================================================================================================================= */
public class CompositeTreeExplorer {
	public Set<ModelPart> initialParts;
	
	public SymbolicTreesMap trees;
	
	private MinimalCompositeSET tree;
	
	private List<ExplorationState> toExplore; 
	
	private Set<NodeSymbolicCheck> lastExplored;
	
	private ModelAdapted model;
	
	private Queues initialQueues;	
	
	private Set<ExplorationState> explored;
	
	private Labeler labeler;
				
	private Map<ActionInputSymbolic, List<Pair<ExplorationState,List<ModelPart>>>> failedPulls; 
	
	//private Labeler labeler;
	//private ExplorationState current;
	
		
	public CompositeTreeExplorer(Set<ModelPart> initial, ModelAdapted model, Labeler labeler){
		this.initialParts = initial;
		this.trees = model.getTrees();
		this.model = model;
			
		trees.generateInitialTrees(initial);
		
		//initial.add(model.getTopLevelPart());
		
		toExplore = new ArrayList<ExplorationState>();
		explored = new HashSet<ExplorationState>();
		
		this.labeler = labeler;
		
		//create queues manager			
		initialQueues = new Queues(model.getAllParts());
		
		//labeler = new Labeler(formulas, tree);
		failedPulls = new HashMap<ActionInputSymbolic, List<Pair<ExplorationState,List<ModelPart>>>>();
	}
	
	public void initialize(){
		//create root of the tree
		CompositeSymbolicState initialState = new CompositeSymbolicState(initialQueues.get(model.getTopLevelPart()));
		initialState.setTopState(new DummySymbolicState(tree));
		
		//build the hierarchy - and mark all states as 'dummy'
		buildInitialHierarchy(initialState, model.getTopLevelPart());
		
		ExplorationState initialExplorationState = new ExplorationState(true, initialQueues, model.getTopLevelPart());
		
		//update exploration state
		initialExplorationState.setCompositeState(initialState);
		
		tree = new MinimalCompositeSET(initialExplorationState.getCurrentNode());
				
		toExplore.add(initialExplorationState);
		
		//grab all default states from parts 
		for(ModelPart mp : initialParts){
			NodeSymbolic init = trees.getInitialTreeState(mp);
			initialState.updatePart(model.getTopLevelPart(), mp, init.getContents(), initialQueues, initialExplorationState.getValuation());
			
			initialExplorationState.setPartOriginalNode(mp,init);
		}
				
	}

	private void buildInitialHierarchy(CompositeSymbolicState css,
			ModelPart part) {
		Set<ModelPart> children = model.getChildren(part);
		
		if (children == null || children.isEmpty()){
			return;
		}
		
		for (ModelPart mp : children){
			CompositeSymbolicState cssChild = new CompositeSymbolicState(initialQueues.get(mp));
			cssChild.setTopState(new DummySymbolicState(tree));
			css.setPart(mp,cssChild);
			buildInitialHierarchy(cssChild, mp);
		}
		
	}

	/*private void buildHierarchy(Map<ModelPart, CompositeSymbolicState> partStates,
			Set<ModelPart> parts, CompositeSymbolicState state, List<Property> currentPath, Queues queues) {
		
		//find parts directly enclosed in current
		for (ModelPart mp : parts){
			if (mp.directChild(currentPath)){				
					CompositeSymbolicState compositeState = partStates.get(mp);					
					state.setPart(mp, compositeState);
					
					buildHierarchy(partStates, parts, compositeState, mp.getPath(), queues);
				} 
		}
	}*/
		

	public MinimalCompositeSET getSET() {
		return tree;
	}

	public Set<NodeSymbolicCheck> makeStep() {
		Set<NodeSymbolicCheck> newNodes = new HashSet<NodeSymbolicCheck>();
		if (toExplore == null || toExplore.isEmpty()){
			return newNodes;
		}
		
		System.out.println("Number to explore " + toExplore.size());
		
		ExplorationState current = getToExplore();
		current.getCurrentNode().setExplored();
		lastExplored = new HashSet<NodeSymbolicCheck>();
		lastExplored.add(current.getCurrentNode());
		explored.add(current);
		
		if (current.isInitial()){
			NodeSymbolicCheck newNode = performDefaultTransition(current);
			
			newNodes.add(newNode);
			newNodes.add(current.getCurrentNode());
			
		} else {	
			
			while (isSubsumed(current)){				
				if (!toExplore.isEmpty()) {
					current = getToExplore();
					if (current.getCurrentNode().hasSubsumes()){												
						break;
					} else {
						current.getCurrentNode().setExplored();
						lastExplored.add(current.getCurrentNode());	
						explored.add(current);
					}
				} else {
					current = null;
					break;
				}
			}
			
			if (current != null) {			
				//mark as explored
				current.getCurrentNode().setExplored();
				lastExplored.add(current.getCurrentNode());	
				explored.add(current);
				Set<ExplorationState> states = perfromTransition(current);
				for (ExplorationState s : states){
					newNodes.add(s.getCurrentNode());
					if (!toExplore.contains(s)){
						toExplore.add(s);
					}
				}
			}
		}
		
		
		
		return newNodes;
		
		
		
	}

	private Set<ExplorationState> perfromTransition(ExplorationState current) {
		Set<ExplorationState> result = new HashSet<ExplorationState>();
				
		
		
		//check transitions of all parts
		for (ModelPart mp : initialParts){
			NodeSymbolic currentPartNode = current.getOriginalNode(mp);
			
			Set<ExplorationState> states = exploreNode(mp, currentPartNode, current.getCurrentNode(), 
					current.getQueues(), true, current, new HashSet<ModelPart>(), new ArrayList<ModelPart>());
			
			result.addAll(states);
			
		}
		
		
		//toExplore.addAll(result);
		
		return result;
	}
	
	private boolean isSubsumed(ExplorationState checkedState){
		NodeSymbolicCheck checkedNode = checkedState.getCurrentNode();
		if (checkedNode.getSubsumedBy() != null){
			return true;
		}
		for (ExplorationState exploredState: explored){
			if (exploredState == checkedState){
				continue;
			}
			if(exploredState.getCurrentNode().getSubsumedBy()==checkedNode){
				return false;
			}
			if (checkedState.getQueues().equals(exploredState.getQueues())) {
				NodeSymbolicCheck exploredNode = exploredState.getCurrentNode();
				if (exploredNode == checkedNode){
					continue;
				}
				CompositeSymbolicState exploredCss = (CompositeSymbolicState) exploredNode.getContents();
				if (exploredCss.subsumes((CompositeSymbolicState)checkedNode.getContents())){
					exploredNode.addSubsumed(checkedNode);
					checkedNode.setSubsumedBy(exploredNode);
					System.out.println("Subsumed");
					return true;
				}
			}
		}
		for (ExplorationState exploredState : toExplore){			
			if (checkedState.getQueues().equals(exploredState.getQueues())) {
				NodeSymbolicCheck exploredNode = exploredState.getCurrentNode();
				if (exploredNode == checkedNode){
					continue;
				}
				if(exploredState.getCurrentNode().getSubsumedBy()==checkedNode){
					return false;
				}
				CompositeSymbolicState exploredCss = (CompositeSymbolicState) exploredNode.getContents();
				if (exploredCss.subsumes((CompositeSymbolicState)checkedNode.getContents())){
					exploredNode.addSubsumed(checkedNode);
					checkedNode.setSubsumedBy(exploredNode);
					System.out.println("Subsumed");
					return true;
				}
			}
		}
		return false;
		
		
	}
	
	/*private List<ExplorationState> exploreNodeStep(ModelPart mp, NodeSymbolic currentPartNode,
			NodeSymbolic currentNode, Queues queues, boolean active, ExplorationState current,
			Set<ModelPart> exploredParts)
	{
		current.getCurrentNode().setExplored();
		//lastExplored.add(current.getCurrentNode());
		List<ExplorationState> result = new ArrayList<ExplorationState>(); 
		
		if (currentPartNode.getSubsumedBy() != null){
			currentPartNode = currentPartNode.getSubsumedBy();
		}
		
		boolean synchPerformed = false;
		
		//check all outgoing transitions
		for (Arc<SymbolicTransition,SymbolicState> arc : currentPartNode.getChildren()){
			SymbolicTransition transition = arc.getContents();
			
			CompositeSymbolicState state = (CompositeSymbolicState) currentNode.getContents();
			ActionInputSymbolic input = transition.getInputExecution();			
			NodeSymbolic targetPartNode = (NodeSymbolic) arc.getTarget();
			
			if (input.getAction().getName().contains("imer")){
				ExplorationState stateAfterOpen = performOpen(mp,transition,targetPartNode, queues,current); 
				if (stateAfterOpen != null){
					result.add(stateAfterOpen);
					return result;
				}
			}
			
			
			
			ModelPart sender = model.findSender(input);
			//check open/close
			if (sender != null) {
				//close
				//check whether in queue already
				if (queues.get(mp).isFirst(input)){
					ExplorationState n = performSynch(queues, mp, transition, currentNode, targetPartNode, current);
					synchPerformed = true;
					if (n!=null){
						result.add(n);
						return result;
					}
				} else {
					// not in a queue -> if sender is outside must pull it 
					if (!(initialParts.contains(sender)) && !queues.get(mp).hasInput(input)){
						if (! exploredParts.contains(sender)){
							List<ExplorationState> statesInactive = exploreGeneration(sender, mp, input, current, exploredParts);							
							if (!statesInactive.isEmpty()){
								result.addAll(statesInactive);
								return result;
							}
								
						}
					}
				}
			} else {
				//action for timer - no 
				
				
				//open only for top level
				if (mp.isTopLevel()){
					ExplorationState stateAfterOpen = performOpen(mp,transition,targetPartNode, queues,current);
					if (stateAfterOpen != null){
						result.add(stateAfterOpen);
						return result;
					}
					
				}
			}
			
			/*if (!queues.get(mp).isEmpty() && !synchPerformed && active){
				ExplorationState n = performDrop(mp,queues,current);
				result.add(n);
				toExplore.add(n);
			}*/
		//}*/
		/*if (!queues.get(mp).isEmpty() && !synchPerformed ){
			ExplorationState n = performDrop(mp, queues,current);
			if (n != null){
				result.add(n);
				return result;
			}
			
		}
		
		return result;

		
	}*/

	private Set<ExplorationState> exploreNode(ModelPart mp, NodeSymbolic currentPartNode,
			NodeSymbolic currentNode, Queues queues, boolean active, ExplorationState current,
			Set<ModelPart> exploredParts, List<ModelPart> insideParts) {
		current.getCurrentNode().setExplored();
		//lastExplored.add(current.getCurrentNode());
		Set<ExplorationState> result = new HashSet<ExplorationState>(); 
		
		
		boolean replacedSubsumed = false;
		NodeSymbolic currentPartNodeS = currentPartNode;
		while (currentPartNodeS.getSubsumedBy() != null){
			currentPartNodeS = currentPartNodeS.getSubsumedBy();			
		}
		if (currentPartNode != currentPartNodeS){
			if (!queues.get(mp).isEmpty()){
				replacedSubsumed = true;
				currentPartNode = currentPartNodeS;
			} 
		}
			
		/*if(mp.getPartName().contains("Orig")){
			if (currentPartNode.getContents().getLocation().getName().contains("WaitingForTerm")){
				System.out.println();
			
			for (ModelPart mp2 : model.getAllParts()){
				if (mp2.getPartName().contains("Term")){
					NodeSymbolic ns = current.getOriginalNode(mp2);
					if (ns.getContents().getLocation().getName().contains("Connected")){
						System.out.println();
					}
				}
			}
			}
		}*/
		boolean synchPerformed = false;
		
		//check all outgoing transitiocurrentns
		for (Arc<SymbolicTransition,SymbolicState> arc : currentPartNode.getChildren()){
			SymbolicTransition transition = arc.getContents();
			
			
			CompositeSymbolicState state = (CompositeSymbolicState) currentNode.getContents();
			ActionInputSymbolic input = transition.getInputExecution();			
			NodeSymbolic targetPartNode = (NodeSymbolic) arc.getTarget();
			
			if (input.getAction().getSignalName().contains("answered")){
				System.out.println("Here");
				if (queues.get(mp).hasInput(input))
					System.out.println();
			}
			
			if (input.getAction().getName().contains("imer")){
				ExplorationState stateAfterOpen = performOpen(mp,transition,targetPartNode, queues,current); 
				result.add(stateAfterOpen);
				if (active)
					toExplore.add(stateAfterOpen);
				continue;
			}
			
			if (input instanceof ActionInputTimeout){
				ExplorationState stateAfterOpen = performOpen(mp,transition,targetPartNode, queues,current); 
				result.add(stateAfterOpen);
				if (active)
					toExplore.add(stateAfterOpen);
				continue;
			}
			
			
			ModelPart sender = model.findSender(input);
			//check open/close
			if (sender != null) {
				//close
				//check whether in queue already
				if (queues.get(mp).isFirst(input)){
					ExplorationState n = performSynch(queues, mp, transition, currentNode, targetPartNode, current);
					synchPerformed = true;
					if (n!=null){
						result.add(n);
						if (n==current){
							int a=0;
						}
						if (active)
							toExplore.add(n);
					}
				} else {
					if (!replacedSubsumed){
					// not in a queue -> if sender is outside must pull it 
					if (!(initialParts.contains(sender)) && !queues.get(mp).hasInput(input)){
						if (! exploredParts.contains(sender) && !current.removePulledArc(arc)){
							List<ExplorationState> statesInactive = exploreGeneration(sender, mp, input, current, exploredParts, insideParts);	
							for (ExplorationState s : statesInactive){
								s.addPulledArc(arc);
							}
							result.addAll(statesInactive);
								
						}
					}
					}
				}
			} else {
				//action for timer - no 
				if (!replacedSubsumed){
					//open only for top level
					if (mp.isTopLevel() || model.isRelayAction(input)){
						ExplorationState stateAfterOpen = performOpen(mp,transition,targetPartNode, queues,current); 
						result.add(stateAfterOpen);
						if (active)
							toExplore.add(stateAfterOpen);
					}	
				}
				
				
			}
			
			
		}
		
		if (!queues.get(mp).isEmpty() && !synchPerformed ){
			ExplorationState n = performDrop(mp, queues,current);
			result.add(n);
			if (active)
				toExplore.add(n);
		}
		
		return result;
	}
		
	
	private ExplorationState getToExplore(){
		ExplorationState s = toExplore.remove(0);
		return s;
		/*
		ExplorationState max = s;
		int i =0;
		/*int maxCount = labeler.getPositiveLabelsCount(s.getCurrentNode());
		while (i < toExplore.size()){			
			s = toExplore.get(i);
			int count = labeler.getPositiveLabelsCount(s.getCurrentNode()); 
			if (count > maxCount){
				maxCount = count;
				max = s;
			}
			i++;
		}*/
		/*toExplore.remove(max);
		return max;*/
	}

	private ExplorationState performDrop(ModelPart mp, Queues queues, ExplorationState current) {
		Queues newQueues = new Queues (queues);
		
		ActionInputSymbolic unrec = newQueues.get(mp).remove();
				
		//build composite
		CompositeSymbolicState oldCss = current.getCompositeState();
		CompositeSymbolicState newCss = new CompositeSymbolicState(oldCss, newQueues, model.getTopLevelPart());
		
		
		//add 
		ExplorationState es = new ExplorationState(current, newQueues);		
		es.setCompositeState(newCss);
		
		
		//create new node in a tree
		NodeSymbolic nodeNew = es.getCurrentNode();	
		
		UnreceivedAction unrecAction = new UnreceivedAction(unrec);
		
		//new symbolic transition
		SymbolicTransition newTransition = new SymbolicTransition(null,unrecAction, tree);
		
		//create new arc						
		ArcSymbolic arcNew = 
			new ArcSymbolic(newTransition, current.getCurrentNode(), nodeNew);
		
		//connect
		current.getCurrentNode().addChild(arcNew);
		es.getCompositeState().setIngoingArc(arcNew);
		
		return es;		
	}

	private ExplorationState performOpen(ModelPart mp, SymbolicTransition transition, NodeSymbolic targetPart, Queues queues,
			ExplorationState current) {
		Queues newQueues = new Queues (queues);
					
		//send actions from transition
		sendSequence(transition.getOuputSequence(), newQueues);
				
		//build composite
		CompositeSymbolicState oldCss = current.getCompositeState();
		CompositeSymbolicState newCss = new CompositeSymbolicState(oldCss, newQueues, model.getTopLevelPart());
		newCss.updatePart(model.getTopLevelPart(), mp, targetPart.getContents(), newQueues, current.getValuation());
		
		//add 
		ExplorationState es = new ExplorationState(current, newQueues);
		es.setPartOriginalNode(mp, targetPart);
		es.setCompositeState(newCss);
		
		
		//create new node in a tree
		NodeSymbolic nodeNew = es.getCurrentNode();	
		
		//new symbolic transition
		SymbolicTransition newTransition = new SymbolicTransition(transition, tree);
		
		//create new arc						
		ArcSymbolic arcNew = 
			new ArcSymbolic(newTransition, current.getCurrentNode(), nodeNew);
		
		//connect
		current.getCurrentNode().addChild(arcNew);
		es.getCompositeState().setIngoingArc(arcNew);
		
		return es;
	}

	
	private boolean isSubsumedInSet(ExplorationState checkedState,Collection<ExplorationState> states){
		for (ExplorationState otherState : states){
			if (isSubsumed(checkedState,otherState)){
				return true;
			}
		}
		return false;
	}
	
	//TODO: JDA: why is this not used?
	private boolean isSubsumedLimitedInactive(ExplorationState checkedState,
			ExplorationState exploredState){
		if (exploredState == checkedState){
			return true;
		}
		if (exploredState.getCurrentNode() == exploredState.getCurrentNode()){
			return true;
		}
		if (exploredState.getCurrentNode() == null && checkedState.getCurrentNode() == null){
			return false;
		}
		if (checkedState.getCurrentNode().getSubsumedBy() != null){
			return true;
		}
		NodeSymbolicCheck checkedNode = checkedState.getCurrentNode();
		if (checkedState.getQueues().equals(exploredState.getQueues())) {
			
			NodeSymbolicCheck exploredNode = exploredState.getCurrentNode();
			if (exploredNode == checkedNode){
				return true;
			}			
			CompositeSymbolicState exploredCss = (CompositeSymbolicState) exploredNode.getContents();
			if (exploredCss.subsumes((CompositeSymbolicState)checkedNode.getContents())){
				exploredNode.addSubsumed(checkedNode);
				checkedNode.setSubsumedBy(exploredNode);
				System.out.println("Subsumed inner");
				return true;
			}
		}
		return false;
		
	}
	private boolean isSubsumed(ExplorationState checkedState,
			ExplorationState exploredState) {
		if (exploredState == checkedState){
			return true;
		}
		/*if (exploredState.getCurrentNode() == exploredState.getCurrentNode()){
			return true;
		}
		if (exploredState.getCurrentNode() == null && checkedState.getCurrentNode() == null){
			return false;
		}*/
		if (checkedState.getCurrentNode().getSubsumedBy() != null){
			return true;
		}
		NodeSymbolicCheck checkedNode = checkedState.getCurrentNode();
		if (checkedState.getQueues().equals(exploredState.getQueues())) {
			
			NodeSymbolicCheck exploredNode = exploredState.getCurrentNode();
			if (exploredNode == checkedNode){
				return true;
			}			
			CompositeSymbolicState exploredCss = (CompositeSymbolicState) exploredNode.getContents();
			if (exploredCss.subsumes((CompositeSymbolicState)checkedNode.getContents())){
				exploredNode.addSubsumed(checkedNode);
				checkedNode.setSubsumedBy(exploredNode);
				System.out.println("Subsumed inner");
				return true;
			}
		}
		return false;
	}

	private List<ExplorationState> exploreGeneration(ModelPart inactive, ModelPart active, ActionInputSymbolic input, 
			ExplorationState current,Set<ModelPart> exploredParts, List<ModelPart> insideParts) {
		System.out.println("PULL "+ active +" from " + inactive.getPartName() + "  for " + input);
		
		Set<ModelPart> exploredPartsLocal = new HashSet<ModelPart>(exploredParts);
		exploredPartsLocal.add(inactive);
		
		insideParts.add(inactive);
		
		if (containsFailedPull(current,input)){
			System.out.println("PULL done");
			return new ArrayList<ExplorationState>();
		}
				
		List<ExplorationState> toExploreInactive = new ArrayList<ExplorationState>();
		Set<ExplorationState> exploredInactive = new HashSet<ExplorationState>();
		
		List<ExplorationState> leavesToReturn = new ArrayList<ExplorationState>();
		//ArcSymbolic pullTransition = null;
		
		
		ExplorationState initial = null;
		boolean inputGenerated = false;
		//ExplorationState stateWithGeneratedInput = null;
		if (! isActivated(current,inactive)){
			//generate SET for this part
			SymbolicExecutionTree set = model.generateTree(inactive);
		
		
			//perform default transition
			ExplorationState afterDefault = performDefaultTransitionInactive(set.getRoot(), inactive, current, input);
			afterDefault.addInactive(inactive);
					
			//pullTransition = afterDefault.getCompositeState().getIngoingArc();
			
			
			//check after default			
				if (afterDefault.getQueues().get(active).hasInput(input)){
					inputGenerated = true;
					
				}
			
			
			leavesToReturn.add(afterDefault);
			if (!inputGenerated){
				toExploreInactive.add(afterDefault);
			} else {
				exploredInactive.add(afterDefault);
				
			}
			
			initial = afterDefault;
		} else {
			
			ExplorationState afterCurrent = performSelfPullTransition(current, inactive, input);
			toExploreInactive.add(afterCurrent);
			//pullTransition = afterCurrent.getCompositeState().getIngoingArc();
			leavesToReturn.add(afterCurrent);
			initial = afterCurrent;
		}
				
		if (inputGenerated){
			System.out.println("Success pulled");
			return leavesToReturn;
		}
		
		while (!toExploreInactive.isEmpty() && !inputGenerated){
			ExplorationState stateToExplore = toExploreInactive.remove(0);
			stateToExplore.getCurrentNode().setExplored();
			markParentsExplored(stateToExplore.getCurrentNode());
			exploredInactive.add(stateToExplore);
			//explored.add(stateToExplore);
			
			Set<ExplorationState> states = exploreNode(inactive, stateToExplore.getOriginalNode(inactive), 
					stateToExplore.getCurrentNode(), stateToExplore.getQueues(), false, stateToExplore, exploredPartsLocal, insideParts);
			
			//check if is found
			if (!states.isEmpty()){
				leavesToReturn.remove(stateToExplore);
				leavesToReturn.addAll(states);
				for (ExplorationState checkedState : states){
					System.out.println("------" + checkedState.getCompositeState().printInLines());
					if (checkedState.getQueues().get(active).hasInput(input)){
						inputGenerated = true;					
						break;
					} else {
						boolean isSubsumed = false;
						
						isSubsumed = isSubsumedInSet(checkedState, exploredInactive);
						if (isSubsumed){
							leavesToReturn.remove(checkedState);
							checkedState.getCurrentNode().setExplored();
							continue;
						}
						isSubsumed = isSubsumedInSet(checkedState, toExploreInactive);
						if (isSubsumed){
							leavesToReturn.remove(checkedState);
							checkedState.getCurrentNode().setExplored();
							continue;
						}
						/*isSubsumed = isSubsumedInSet(checkedState, explored);
						if (isSubsumed){
							leavesToReturn.remove(checkedState);
							continue;
						}*/
						if (!isSubsumed) {					
							toExploreInactive.add(checkedState);					
						}
					}
				}
			}
			
		}
				
		if (!inputGenerated){
			leavesToReturn.clear();
			//insideParts = new ArrayList<ModelPart>();
			//insideParts.add(inactive);
			updateFailed(input,initial, insideParts);
			System.out.println("FAILED - PULL from " + inactive.getPartName() + "  for " + input);
		} else {
			System.out.println("SUCCESS - PULL from " + inactive.getPartName() + "  for " + input);
		}
		insideParts.remove(inactive);
			//create list
		//if this is for topLevel
		if(exploredParts.isEmpty()){
			toExplore.addAll(leavesToReturn);
		}
		
		
		return leavesToReturn;
		
	}

	private void updateFailed(ActionInputSymbolic input,
			ExplorationState initial, List<ModelPart> parts) {
		List<Pair<ExplorationState, List<ModelPart>>> failed = failedPulls.get(input);
		if (failed == null){
			failed = new ArrayList<Pair<ExplorationState,List<ModelPart>>>();
			failedPulls.put(input,failed);
		}
		failed.add(new Pair<ExplorationState,List<ModelPart>>(initial,parts));
		
	}

	private boolean containsFailedPull(ExplorationState current,
			ActionInputSymbolic input) {
		if (!failedPulls.containsKey(input)){
			return false;
		} else{
			for (Pair<ExplorationState,List<ModelPart>> failed : failedPulls.get(input)){
				if (isSubsumed(current, failed.getFirst())){
					return true;
					
				}
			}
		}
		
		return false;
	}

	//TODO: JDA: Why is this not used locally?
	private boolean isSubsumed(ExplorationState checkedState,
			ExplorationState exploredState, List<ModelPart> parts) {
		if (exploredState == checkedState){
			return true;
		}
		/*if (exploredState.getCurrentNode() == exploredState.getCurrentNode()){
			return true;
		}
		if (exploredState.getCurrentNode() == null && checkedState.getCurrentNode() == null){
			return false;
		}*/
		if (checkedState.getCurrentNode().getSubsumedBy() != null){
			return true;
		}
		NodeSymbolicCheck checkedNode = checkedState.getCurrentNode();
		if (checkedState.getQueues().equals(exploredState.getQueues())) {
			
			NodeSymbolicCheck exploredNode = exploredState.getCurrentNode();
			if (exploredNode == checkedNode){
				return true;
			}			
			CompositeSymbolicState exploredCss = (CompositeSymbolicState) exploredNode.getContents();
			if (exploredCss.subsumes((CompositeSymbolicState)checkedNode.getContents(), parts, model.getTopLevelPart())){
				exploredNode.addSubsumed(checkedNode);
				checkedNode.setSubsumedBy(exploredNode);
				System.out.println("Subsumed inner");
				return true;
			}
		}
		return false;

		
	}

	private void markParentsExplored(NodeSymbolicCheck state) {
		boolean hasParentUnexplored = true;
		
		NodeSymbolicCheck current = state;
		while(hasParentUnexplored){			
			ArcSymbolic arc = ((CompositeSymbolicState)state.getContents()).getIngoingArc();
			if (arc == null){
				break;
			}
			current = (NodeSymbolicCheck) arc.getSource();
			if (current == null){
				break;
			}
			if(current.isExplored()){
				hasParentUnexplored = false;				
			} else {
				current.setExplored();
			}
		}
	}

	private boolean isActivated(ExplorationState current, ModelPart part) {
		CompositeSymbolicState css = current.findStateForPart(part);
		if (css != null){
			boolean isDummy = css.getTopState() instanceof DummySymbolicState;
			return !isDummy;
		}
		return false;
	}

	private  ExplorationState performSelfPullTransition(ExplorationState current, ModelPart part, ActionInputSymbolic input) {
		ExplorationState afterCurrent = new ExplorationState(current, current.getQueues());
		
		PullTransition pt = new PullTransition(null, tree, input, part);
		
		
		
		CompositeSymbolicState css = new CompositeSymbolicState(current.getCompositeState(),current.getQueues(), model.getTopLevelPart());
		afterCurrent.setCompositeState(css);
		
		ArcSymbolic arc = new ArcSymbolic(pt, current.getCurrentNode(), afterCurrent.getCurrentNode());
		current.getCurrentNode().getChildren().add(arc);
		
		afterCurrent.getCompositeState().setIngoingArc(arc);
		
		
		return afterCurrent;
	}

	private ExplorationState performDefaultTransitionInactive(NodeSymbolic root, ModelPart part, ExplorationState current, 
			ActionInputSymbolic input) {
		Queues newQueues = new Queues(current.getQueues());
		newQueues.addQueue(part);
		
		//get the transition
		SymbolicTransition transition = root.getChildren().get(0).getContents();
		PullTransition transitionNew = new PullTransition(transition, tree, input, part);
		
		//send outputs
		sendSequence(transition.getOuputSequence(), newQueues);
		
		//add a state 
		NodeSymbolic initialState = (NodeSymbolic) root.getChildren().get(0).getTarget();
			
		//update current composite
		CompositeSymbolicState oldCss = current.getCompositeState();
		CompositeSymbolicState newCss = new CompositeSymbolicState(oldCss, newQueues, model.getTopLevelPart());
		newCss.updatePart(model.getTopLevelPart(), part, initialState.getContents(), newQueues, current.getValuation());
			
		//create exploration state
		ExplorationState newExplorationState  = new ExplorationState(current,newQueues);
		newExplorationState.setCompositeState(newCss);
		newExplorationState.setPartOriginalNode(part, initialState);
		
		ArcSymbolic arc = new ArcSymbolic(transitionNew, current.getCurrentNode(), newExplorationState.getCurrentNode());
		current.getCurrentNode().getChildren().add(arc);
		newExplorationState.getCompositeState().setIngoingArc(arc);
		
		
		return newExplorationState;
	}

	private ExplorationState performSynch(Queues queues, ModelPart mp, SymbolicTransition transition, NodeSymbolic currentNode,
			NodeSymbolic targetPartNode, ExplorationState current){
		
		Queues newQueues = new Queues(queues);
		
		//remove from a queue
		ActionInputSymbolic queueInput = newQueues.remove(mp);						
			
		//create synch action
		SynchronizationAction synchAction = new SynchronizationAction(queueInput, transition.getInputExecution());
		
		//update valuations
		Map<SymbolicVariable, Expression> val = synchAction.getValuationMapping();
		Map<SymbolicVariable, Expression> newCurrentVal = new HashMap<SymbolicVariable, Expression>(current.getValuation());
		newCurrentVal.putAll(val);
		
		//create new transition
		SymbolicTransition newTransition = new SymbolicTransition(transition.getTransition(), synchAction, transition.getTree());
		newTransition.setOutputSequence(transition.getOuputSequence());
		
		//send sequence
		sendSequence(transition.getOuputSequence(), newQueues);
		
		//get target							
		
		//create new composite state and a new exploration state (?)
		CompositeSymbolicState css = new CompositeSymbolicState((CompositeSymbolicState)currentNode.getContents(), newQueues, model.getTopLevelPart());
		css.updatePart(model.getTopLevelPart(),mp,targetPartNode.getContents(),newQueues, newCurrentVal);
		
		
		ExplorationState es = new ExplorationState(current, newQueues);
		es.setCompositeState(css);
		es.setPartOriginalNode(mp, targetPartNode);
		es.setValuation(newCurrentVal);
		
		boolean isSatis = es.checkSatsfiability(synchAction);
		if (!isSatis){
			
			return null;
		}
		
		//create new node in a tree
		NodeSymbolic nodeNew = es.getCurrentNode();	
		
		//create new arc						
		ArcSymbolic arcNew = 
			new ArcSymbolic(newTransition, currentNode, nodeNew);
		
		es.getCompositeState().setIngoingArc(arcNew);
		
		
		//connect
		currentNode.addChild(arcNew);
		
		return es;
		
	}

	private NodeSymbolicCheck performDefaultTransition(ExplorationState current) {
		Queues newQueues = new Queues(current.getQueues());
		ExplorationState newExploreState = new ExplorationState(false, newQueues, model.getTopLevelPart());
		toExplore.add(newExploreState);
		
		CompositeSymbolicState newState = new CompositeSymbolicState(current.getCompositeState(),newQueues, model.getTopLevelPart());
		newExploreState.setCompositeState(newState);
		
		
		/*
		//process top level & store
		NodeSymbolic currentTopLevel = current.getTopLevelNode();
		
		//find transition & execute
		SymbolicTransition transition = currentTopLevel.getChildren().get(0).getContents();
		sendSequence(transition.getOuputSequence(), newQueues);
		
		//get target
		NodeSymbolic nextTopLevel = (NodeSymbolic) currentTopLevel.getChildren().get(0).getTarget();
		
		//set top state
		newState.setTopState(nextTopLevel.getContents());
				
		newExploreState.setPartOriginalNode(model.getTopLevelPart(),nextTopLevel);		
				
		*/
		NodeSymbolic currentTopLevel = null;
		for (ModelPart mp : initialParts){			
			NodeSymbolic originalNode = current.getOriginalNode(mp);
			if (currentTopLevel == null){
				currentTopLevel = originalNode;
			}
			
			SymbolicTransition st = originalNode.getChildren().get(0).getContents();
			sendSequence(st.getOuputSequence(), newQueues);
			
			//find subsequent state
			NodeSymbolic nextOriginalNode = (NodeSymbolic) originalNode.getChildren().get(0).getTarget();
			
			//store
			newExploreState.setPartOriginalNode(mp,nextOriginalNode);
			
			
			//updated
			newState.updatePart(model.getTopLevelPart(), mp, nextOriginalNode.getContents(), newQueues, newExploreState.getValuation());
			
		}
		
		
				
		//create transition
		SymbolicTransition transitionNew = new SymbolicTransition((SymbolicTransition)currentTopLevel.getChildren().get(0).getContents(), tree);
		
		//connect
		ArcSymbolic arc = new ArcSymbolic(transitionNew, current.getCurrentNode(), newExploreState.getCurrentNode()); 
		current.getCurrentNode().getChildren().add(arc);
		newExploreState.getCompositeState().setIngoingArc(arc);
		
		return newExploreState.getCurrentNode();
	}

	private void sendSequence(OutputSequenceSymbolic outputSequence,
			Queues queues) {
		for (ActionOutputInstance aoi : outputSequence.getSequence()){
			ActionOutput ao = aoi.getActionOutput();
			
			//find connected action
			ActionInput ai = model.getConnectedInputAction(ao);
			ModelPart receiver = model.getPart(ai);
			
			if (ai==null){
				continue;
			}
			
			//create instance
			//Bug 2. Added null parameter to prevent syntax error.
			ActionInputSymbolic ais = new ActionInputSymbolic(ai, aoi.getSymbolicMapping(), aoi.getSymbolicVariableMapping(), null);
			
			//update queue for a part
			queues.addElement(receiver,ais);
			
		}
		
	}

	public Set<NodeSymbolicCheck> getLastExplored() {		
		return lastExplored;
	}

	public void run() {
		while (!toExplore.isEmpty()){
			makeStep();
		}	
	}
	
	public boolean isDone(){
		return toExplore.isEmpty();
	}

}
