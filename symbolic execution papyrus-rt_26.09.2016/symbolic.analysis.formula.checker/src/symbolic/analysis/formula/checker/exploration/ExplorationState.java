package symbolic.analysis.formula.checker.exploration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
import symbolic.analysis.formula.checker.model.NodeSymbolicCheck;
//import symbolic.analysis.formula.checker.model.Queue;
import symbolic.analysis.formula.checker.model.Queues;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.tree.synch.SynchronizationAction;

public class ExplorationState {
	
	private Map<ModelPart,NodeSymbolic> partOriginalStates;
	
	private CompositeSymbolicState currentState;
	
	private NodeSymbolicCheck currentNode;
	
	private Queues queues;
	
	private boolean isInitial;
	
	private Set<ModelPart> inactiveParts;
	
	private ModelPart topLevelPart;
	
	private Map<SymbolicVariable, Expression> valuation;
	
	private Arc<SymbolicTransition, SymbolicState> pulledArc;
	
	public ExplorationState(boolean isInitial, Queues queues, ModelPart topLevelPart){
		partOriginalStates = new HashMap<ModelPart, NodeSymbolic>();
		valuation = new HashMap<SymbolicVariable, Expression>();
		
		this.isInitial = isInitial;
		this.queues = queues;
		this.inactiveParts = new HashSet<ModelPart>();
				
		
		this.topLevelPart = topLevelPart;
	}

	public ExplorationState(ExplorationState other, Queues newQueues) {
		this(false,newQueues, other.topLevelPart);
		
		for (ModelPart mp : other.partOriginalStates.keySet()){
			partOriginalStates.put(mp, other.partOriginalStates.get(mp));
		}
		this.inactiveParts.addAll(other.inactiveParts);
		this.valuation.putAll(other.valuation);
	}

	public void setPartOriginalNode(ModelPart part, NodeSymbolic initRoot) {
		partOriginalStates.put(part, initRoot);
		
	}

	public void setCompositeState(CompositeSymbolicState state) {
		this.currentState = state;
		
		//build node
		currentNode = new NodeSymbolicCheck(state);
	}

	public boolean isInitial() {
		return isInitial;
	}

	public NodeSymbolic getTopLevelNode() {		
		return partOriginalStates.get(topLevelPart);
	}

	public Set<ModelPart> getCurrentParts() {
		return partOriginalStates.keySet();
	}

	public NodeSymbolic getOriginalNode(ModelPart mp) {
		return partOriginalStates.get(mp);
	}

	public NodeSymbolicCheck getCurrentNode() {
		return currentNode;
		
	}

	public Queues getQueues() {
		return queues;
	}

	public void addInactive(ModelPart inactive) {
		inactiveParts.add(inactive);
		
	}

	public boolean containsInactive(ModelPart inactive) {
		return inactiveParts.contains(inactive);
	}

	public Map<SymbolicVariable, Expression> getValuation() {
		return valuation;
	}

	public void setValuation(Map<SymbolicVariable, Expression> newCurrentVal) {
		this.valuation = newCurrentVal;
		
	}

	public boolean checkSatsfiability(SynchronizationAction action) {
		//gather constraints and valuations
		List<Constraint> constraints = new ArrayList<Constraint>();		
		constraints.addAll(action.getValuationConstraints());
		for(SymbolicState state : getStates()){
			constraints.addAll(state.getPC());
			//constraints.addAll(state.getValuation().createConstraints());
		}
		
		
		Solver solver = SymbolicFactory.createSolver();
		return solver.isSolvable(constraints);
	}

	private Set<SymbolicState> getStates() {
		return currentState.gatherStates();
	}

	public CompositeSymbolicState getCompositeState() {
		return currentState;
	}

	public CompositeSymbolicState findStateForPart(ModelPart part) {
		if (part == topLevelPart){
			return currentState;
		}
		return currentState.findStateForPart(part, 1);
	}

	public void addPulledArc(Arc<SymbolicTransition, SymbolicState> arc) {
		pulledArc = arc;
		
	}

	public boolean removePulledArc(Arc<SymbolicTransition, SymbolicState> arc) {
		if (arc == pulledArc){
			pulledArc = null;
			return true;
		}
		return false;
	}

}
