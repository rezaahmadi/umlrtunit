package symbolic.analysis.formula.checker.exploration;

//import symbolic.analysis.formula.checker.adaptation.ModelPart;
//import symbolic.analysis.formula.checker.model.CompositeSymbolicState;
//import symbolic.analysis.formula.checker.model.Queue;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;


public class DummySymbolicState extends SymbolicState {
	public DummySymbolicState(SymbolicExecutionTree tree){
		super(null,null,tree);
	}
	
	@Override
	public String toString() {		
		return "null";
	}
}
