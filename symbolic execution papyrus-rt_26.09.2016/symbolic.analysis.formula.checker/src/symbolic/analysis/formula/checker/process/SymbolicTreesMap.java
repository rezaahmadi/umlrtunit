package symbolic.analysis.formula.checker.process;

import java.util.HashMap;
//import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.Class;
//import org.eclipse.uml2.uml.Property;

//import com.ibm.xtools.uml.rt.core.Capsule;
//import com.ibm.xtools.uml.rt.core.RTCapsulePart;
//import com.ibm.xtools.uml.rt.core.RTFactory;

import symbolic.analysis.formula.checker.adaptation.ActionMapping;
//import symbolic.analysis.formula.checker.adaptation.ModelAdapted;
import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.umlrt.actions.TreeGeneratorThread;
import symbolic.execution.umlrt.engine.Engine;
//import symbolic.execution.umlrt.model.SMValidationException;

public class SymbolicTreesMap {
	
	private Map<Class, SymbolicExecutionTree> treeClass;
	
	private Map<ModelPart, SymbolicExecutionTree> treePart;
		
	private Map<Class, Engine> engines;

	private Map<ModelPart, ActionMapping> actionsMap;
	
	private Map<ModelPart, Class> partTypes;
	
	
	public SymbolicTreesMap(Map<Class, Engine> engines, Map<ModelPart, Class> partTypes, Map<ModelPart, ActionMapping> actionsMaps) {
		treeClass = new HashMap<Class, SymbolicExecutionTree>();
		treePart = new HashMap<ModelPart, SymbolicExecutionTree>();
		this.engines = engines;
		this.actionsMap = actionsMaps;
		this.partTypes = partTypes;
	}

	public SymbolicExecutionTree generateTree(ModelPart part){
		if (treePart.containsKey(part)){
			return treePart.get(part);
		}
		Class c = partTypes.get(part);
		if (treeClass.containsKey(partTypes.get(part))){			
			
			//just copy
			SymbolicExecutionTree set = new SymbolicExecutionTree(treeClass.get(c), actionsMap.get(part).getInputMap(), actionsMap.get(part).getOutputMap()); 
			treePart.put(part, set);
			return set;
		}
		
		Engine engine = engines.get(c);
		ActionMapping actions = actionsMap.get(part);
		//generate a tree
		SymbolicExecutionTree tree=null;
		try {			
			tree = engine.execute();
			if (actionsMap.get(part)==null){
				treeClass.put(c,tree);
			} else {
				tree= new SymbolicExecutionTree(tree, actions.getInputMap(), actions.getOutputMap());
				treeClass.put(c,tree);
			}
			
			treePart.put(part, tree);
			
		} catch (Exception e) {
			System.out.println("JDA: Exception in SymbolicExecutionTree.generateTree.");
			e.printStackTrace();
		}
		
		return tree;
								
		}

	/* JDA: begin removal
	private SymbolicExecutionTree executeSymbolic(Capsule capsule) {
		Engine engine = null;
		try {
			engine = new Engine(capsule);
		} catch (SMValidationException e1) {
			System.out.println("JDA: SMValidationException in SymbolicExecutionTree.executeSymbolic.");
			e1.printStackTrace();
		}		
		TreeGeneratorThread gen = new TreeGeneratorThread(engine);
		gen.start();			
		try {
			gen.join();
		} catch (InterruptedException e) {
			System.out.println("JDA: InterruptedException in SymbolicExecutionTree.executeSymbolic.");
			e.printStackTrace();
		}
		return gen.getTree();
	}
    JDA: end removal */
	
	/* JDA: begin removal
	private Set<RTCapsulePart> extractParts(Capsule container) {
		//display roles:
		
		Set<RTCapsulePart> result = new HashSet<RTCapsulePart>();
		if (container.getAllRoles() == null){
			return result;
		}
		System.out.println("Capsules contained in " + container.getUML2Class().getName());
		
		//discover all parts
		for (RTCapsulePart part : container.getAllRoles()){
			if (part != null && part.getType() != null && part.getType().getValue() instanceof Class){
				//Class partClass = (Class) part.getType().getValue();
				//Capsule partCapsule = RTFactory.CapsuleFactory.getCapsule(partClass);
				result.add(part);
				//System.out.println("Part name " + part.getName() + " : " + partCapsule.getUML2Class().getName() +
				//	"["+part.getReferenceTarget().getLower() + ".." + part.getReferenceTarget().getUpper()+"]");									
			}
			//result.addAll(extractContained(partCapsule));			
		}		
		
		return result;
	}
    JDA: end removal */
	
	public String display() {
		StringBuffer b = new StringBuffer();
		
		for (Class c : treeClass.keySet()){
			b.append (c.getName());
			b.append(",");
		}
		
		return b.toString();
	}

	public NodeSymbolic getInitialTreeState(ModelPart mp) {
		SymbolicExecutionTree tree = treePart.get(mp);
		if (tree != null){
				return tree.getRoot();
		}
		return null;
	}

	public void generateInitialTrees(Set<ModelPart> initial) {
		for (ModelPart mp : initial){
			generateTree(mp);
		}
		
		
	}
	
	
}
