package symbolic.analysis.formula.checker.process;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Properties;
import java.util.Set;

//import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.Class;

import org.eclipse.uml2.uml.Class;

import symbolic.analysis.formula.checker.adaptation.ModelAdapted;
import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.analysis.formula.checker.exploration.CompositeTreeExplorer;
import symbolic.analysis.formula.checker.initialization.FormulaMapper;
import symbolic.analysis.formula.checker.initialization.InitialPartsExtractor;
import symbolic.analysis.formula.checker.labeling.FormulaTranslator;
import symbolic.analysis.formula.checker.labeling.Labeler;
//import symbolic.analysis.formula.checker.labeling.SubformulaGenerator;
import symbolic.analysis.formula.checker.model.MinimalCompositeSET;
import symbolic.analysis.formula.checker.model.NodeSymbolicCheck;
import symbolic.analysis.language.Formula;
//import symbolic.analysis.language.InOut;
//import symbolic.analysis.language.InQueue;
//import symbolic.analysis.language.InState;
//import symbolic.analysis.language.Invariant;
//import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.parser.FormulaParser;
import symbolic.analysis.language.parser.ParseException;
//import symbolic.analysis.language.utils.IncorrectFormulaException;
//import symbolic.analysis.language.utils.IncorrectHierarchyException;
//import symbolic.analysis.language.utils.IncorrectInputActionException;
//import symbolic.analysis.language.utils.PropertyFinder;
//import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
//import symbolic.execution.ffsm.model.ActionInput;

//import com.ibm.xtools.uml.rt.core.Class;
//import org.eclipse.papyrusrt.xtumlrt.common.*;


public class RequestProcessor {
	
	private Class capsule;
	
	private Formula formulaOriginal;	
	
	private Formula formulaToCheck;
	
	private ModelAdapted model;
	
	private Labeler labeler;
	
	private CompositeTreeExplorer explorer;
	
	private boolean isDone;
	
	private Boolean isSatisfied;

	
	private boolean noNulls = true;
	
	public RequestProcessor(Class capsule) {
		this.capsule = capsule;		
		try {
			this.model = new ModelAdapted(capsule);
		} catch (Exception e) {
			System.out.println("JDA: Exception in RequestProcessor.");
			e.printStackTrace();
		}
	}

	public CompositeTreeExplorer process (String queryText) throws Exception{		
		//check formula
		if (queryText == null || queryText.isEmpty()){
			return null;
		}
		
		//parse formula
		formulaOriginal = parseFormula(queryText);
		
		if (formulaOriginal == null){
			return null;
		}                                                                                                                                                                                                                                                                                   
		
		//translate the formula
		
		FormulaTranslator translator = new FormulaTranslator();
		formulaToCheck = translator.translate(formulaOriginal);
		
		
		//extract initial parts
		InitialPartsExtractor extractor = new InitialPartsExtractor(model);
		Set<ModelPart> initParts = extractor.extract(formulaToCheck);
		
		//initialize parts
		model.initializeTrees(initParts);
		
		FormulaMapper fm = new FormulaMapper(model);
		fm.map(formulaToCheck);
		
		//build labeler 
		labeler = new Labeler(formulaToCheck);
		
		
		//build explorer
		explorer = new CompositeTreeExplorer(initParts, model, labeler);
		
		//initialize exploration
		explorer.initialize();
		
		
		return explorer;
		
		/*
		
		try {
			//parse formula
			InputStream input = createStream(queryText);			
			FormulaParser fp = new FormulaParser(input);			
			Formula sf = fp.LTLformula();
			
			System.out.println(sf);
			
			// map formula to a model
			FormulaMapper fm = new FormulaMapper(model);
			fm.map(sf);
			
			//translate
			FormulaTranslator translator = new FormulaTranslator();
			Formula translated = translator.translate(sf);
			
			
			//find initial pats/trees
			InitialPartsExtractor init = new InitialPartsExtractor(model);
			Set<ModelPart> initParts = init.extract(translated);
			Set<ModelPart> initPartsWithParents = model.getParents(initParts);
			for (ModelPart p : initParts){
				System.out.println(p.toString());
				
			}
		
		//generate trees for initial parts
		model.initializeTrees(initPartsWithParents);
		
		SubformulaGenerator sg = new SubformulaGenerator();
		
		List<Formula> formulas = sg.getSubformula(translated);
		
		
		
		} catch (ParseException e) {
			// XODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return explorer; */
		
	}

	private Formula parseFormula(String queryText) {
		try{
			InputStream input = createStream(queryText);			
			FormulaParser fp = new FormulaParser(input);			
			Formula origFormula = fp.LTLformula();		
			
			// map formula to a model
			FormulaMapper fm = new FormulaMapper(model);
			fm.map(origFormula);
			
			return origFormula;
			
		}catch (ParseException e){
			e.printStackTrace();
			return null;
		}
		
	}

	private static InputStream createStream(String code) {
		char[] charArray = code.toCharArray();
		byte[] byteArray = new byte[charArray.length];
		for (int i=0; i <byteArray.length; i++){
			byteArray[i] = (byte) charArray[i];
		}
		
		return new ByteArrayInputStream(byteArray);
	}


	public MinimalCompositeSET getSET() {
		return explorer.getSET();
	}

	public boolean makeStep() {
		//explore
		Set<NodeSymbolicCheck> newlyExplored = explorer.makeStep();
		
		//label
		//labeler.label((NodeSymbolicCheck) explorer.getSET().getRoot());
		//if (explorer.isDone()){
		//	newlyExplored = new HashSet<NodeSymbolicCheck>();
		//	newlyExplored.add(explorer.getLastExplored());
		//}
		labeler.label(newlyExplored, explorer.getLastExplored());
		
		
		
		//check
		isSatisfied = isSatisfied();
		
		if (isSatisfied!=null)
			isDone = true;
		
				
		boolean isNext = isDone || explorer.isDone();
		
		if (isNext){
			checkUnexplored((NodeSymbolicCheck) explorer.getSET().getRoot());
		}
		return isNext;
		
	}

	private void checkUnexplored(NodeSymbolicCheck root) {
		if (!root.isExplored()){			
			noNulls = false;
		}
		
		for (Arc<SymbolicTransition, SymbolicState> arc : root.getChildren()){
			checkUnexplored((NodeSymbolicCheck) arc.getTarget());
		}
		
	}

	public void run() {
		//explore until explorer done or formula done (!!)
		
		long start = System.currentTimeMillis();
		
		while (!explorer.isDone() && !isDone){
			Set<NodeSymbolicCheck> newlyExplored = explorer.makeStep();
			labeler.label(newlyExplored, explorer.getLastExplored());
			//labeler.label((NodeSymbolicCheck) explorer.getSET().getRoot());
						
			isSatisfied = isSatisfied();
			
			if (isSatisfied!=null)
				isDone = true;
			
		}		
		long end = System.currentTimeMillis();
		
		checkUnexplored((NodeSymbolicCheck) explorer.getSET().getRoot());
		
		System.out.println("************************************************");
		System.out.println("TIme " + (end-start)/1000 );
		System.out.println("************************************************");
	}
	
	

	public Boolean isSatisfied(){
		NodeSymbolicCheck root = (NodeSymbolicCheck) explorer.getSET().getRoot();
		Boolean label = labeler.getLabel(root, formulaToCheck);
		return label;
	}

	public String getResult() {
		
		//if (isDone){
			//check result 
			isSatisfied = isSatisfied();
			int noOfState = explorer.getSET().getNumberOfStates();
			if (isSatisfied == null){
				if (noNulls){
					isSatisfied = false;
				} else {
					return "Cannot check. Number of states " + noOfState;
				}
			}
			return "Formula " + formulaOriginal.toString() + (isSatisfied ? " is " : " is not ") + "satisfied. \n Tree has " + noOfState + " states";
		//}
		//return null;
	}
}
