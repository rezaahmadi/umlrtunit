package symbolic.analysis.formula.checker.model;

import java.util.ArrayList;
import java.util.List;

import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.model.ActionInput;

public class Queue {
	
	private List<ActionInputSymbolic> queue;
	private ModelPart part;
	
	public Queue(ModelPart part) {
		queue = new ArrayList<ActionInputSymbolic>();
		this.part = part;
	}

	public boolean isFirst(ActionInputSymbolic input) {
		if (queue.isEmpty())
			return false;
		ActionInput first = queue.get(0).getAction();
		return first.equals(input.getAction());
	}

	public void addAll(Queue other) {
		queue.addAll(other.queue);
		
	}

	public void add(ActionInputSymbolic ais) {
		queue.add(ais);		
	}
	@Override
	public String toString() {		
		return part + "->" +queue.toString();
	}

	public ActionInputSymbolic remove() {
		if (queue.isEmpty())
			return null;
		return queue.remove(0);
	}

	public boolean hasSignals(List<ActionInput> inputList) {
		if (queue.size() != inputList.size()){
			return false;
		}
		for (int i=0; i<queue.size(); i++){
			if (!queue.get(i).getAction().equals(inputList.get(i))){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Queue))
			return false;
		
			Queue otherQueue = (Queue)other;
			
			if (otherQueue.queue.size() != queue.size()){
				return false;
			}
			
			for (int i=0; i<queue.size(); i++){
				if (!(queue.get(i).getAction().getPortName().equals(otherQueue.queue.get(i).getAction().getPortName())
						&& queue.get(i).getAction().getSignalName().equals(otherQueue.queue.get(i).getAction().getSignalName()))){
					return false;
				}
			}
		
		return true;
	}

	public boolean hasInput(ActionInputSymbolic input) {
		if (input == null)
			return false;
		for (ActionInputSymbolic ais : queue){
			if (ais.getAction().equals(input.getAction())){
				return true;
			}
		}
		return false;
	}

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	public int size() {
		return queue.size();
	}

}
