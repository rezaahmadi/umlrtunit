package symbolic.analysis.formula.checker.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;

public class Queues {
	
	private Map<ModelPart, Queue> queues; 
	
	public Queues(Set<ModelPart> parts){
		queues = new HashMap<ModelPart, Queue>();
		
		for (ModelPart part : parts){
			queues.put(part, new Queue(part));
		}
	}

	public Queues(Queues other) {
		this(other.queues.keySet());
		
		for (ModelPart mp : queues.keySet()){
			queues.get(mp).addAll(other.queues.get(mp));
		}
	}

	public Queue get(ModelPart part) {
		return queues.get(part);
	}

	public void addElement(ModelPart receiver, ActionInputSymbolic ais) {
		Queue q = queues.get(receiver);
		if (q ==null){			
			q = new Queue(receiver);
			queues.put(receiver, q);
		}
		if (q.size() < 5){
			q.add(ais);
		}
		
	}

	public ActionInputSymbolic remove(ModelPart mp) {
		Queue q = queues.get(mp);
		if (q==null)
			return null;
		return q.remove();
	}

	public void addQueue(ModelPart part) {
		if (queues.containsKey(part)){
			return;
		}
		queues.put(part,new Queue(part));
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof Queues){
			Queues otherQ  = (Queues)other;
			if (!otherQ.queues.keySet().containsAll(queues.keySet()))
				return false;
			if (!queues.keySet().containsAll(otherQ.queues.keySet())){
				return false;
			}
			for (ModelPart mp : otherQ.queues.keySet()){
				if (! otherQ.queues.get(mp).equals(queues.get(mp))){
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
