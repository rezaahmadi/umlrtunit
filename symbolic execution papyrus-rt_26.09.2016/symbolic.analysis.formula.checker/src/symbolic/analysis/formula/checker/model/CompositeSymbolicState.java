package symbolic.analysis.formula.checker.model;

//import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.Property;


import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.TimerActionOutput;

public class CompositeSymbolicState extends SymbolicState{
	
	private SymbolicState topState;
	
	private Map<ModelPart, CompositeSymbolicState> partState;
	
	private Queue queue;
	
	public CompositeSymbolicState(Location location,
			ValuationSymbolic valuation, List<Constraint> pathCondition,
			SymbolicExecutionTree tree, List<TimerActionOutput> setTimers) {
		super(location, valuation, pathCondition, tree, setTimers);
		
		partState = new HashMap<ModelPart, CompositeSymbolicState>();
				
	}

	public CompositeSymbolicState(Queue queue) {
		this(null, null, null, null, null);
		this.queue = queue;
	}
	
	public CompositeSymbolicState(CompositeSymbolicState other, Queues queues, ModelPart part){
		this(null, null, null, null, null);
		
		topState = other.topState;
		queue = queues.get(part);
		
		for (ModelPart mp : other.partState.keySet()){
			CompositeSymbolicState cssChild = new CompositeSymbolicState(other.partState.get(mp), queues, mp);
			
			partState.put(mp, cssChild);			
		}
	}

	public void setTopState(SymbolicState topState) {
		this.topState = topState;
		
	}
	
	public SymbolicState getTopState(){
		return topState;
	}

	public void setPart(ModelPart mp, CompositeSymbolicState compositeState) {
		partState.put(mp, compositeState);
		
	}

	public Set<ModelPart> getParts() {
		return partState.keySet();
	}

	public CompositeSymbolicState getStateForPart(ModelPart mp) {
		return partState.get(mp);
	}

	@Override
	public String toString() {
		if (topState==null || queue==null){
			return "";
		}
		return topState.toString() +"\n"+ queue.toString();
	}


	public void updatePart(ModelPart currentPart, ModelPart partToChange,
			SymbolicState newState, Queues queues, Map<SymbolicVariable, Expression> vals) {			
		if (currentPart.equals(partToChange)){			
			newState.updateValuation(vals);
			setTopState(newState);
		} 
		
		for (ModelPart mp : partState.keySet()){
			CompositeSymbolicState state = partState.get(mp);
			
			state.updatePart(mp, partToChange, newState, queues, vals);			
		}
					
	}

	public boolean isComposite() {
		return !(partState.isEmpty());
	}

	public boolean partInLocation(Location location, List<Property> path, int depth) {
		if (path.size() == depth){
			if (topState.getLocation() == null){
				return false;
			}
			return topState.getLocation().equals(location);
		}		
		CompositeSymbolicState css = findPartIn(path,depth+1);
		
		return css.partInLocation(location, path, depth+1);
	}

	public CompositeSymbolicState findPartIn(List<Property> path, int depth) {
		
		for (ModelPart mp : partState.keySet()){
			if (mp.agreesToDepth(path,depth)){
				return partState.get(mp);
			}
		}		
		
		return null;
	}

	public CompositeSymbolicState findPart(List<Property> path, int depth) {
		if (path.size() == depth){
			return findPartIn(path,depth);
		}
		CompositeSymbolicState cssInner = findPartIn(path, depth);		
		
		return cssInner.findPart(path, depth+1);
					
	}
	public boolean partHasQueue(List<ActionInput> inputList,
			List<Property> path, int depth) {
		if(path.size() == depth){
			return queue.hasSignals(inputList);
		}		
		CompositeSymbolicState css = findPartIn(path, depth+1);
		
		return css.partHasQueue(inputList, path, depth+1);
	}
	
	public boolean subsumes(CompositeSymbolicState other){
		if (topState.subsumes(other.topState)){
			for (ModelPart mp : partState.keySet()){
				CompositeSymbolicState thisCss = partState.get(mp);
				CompositeSymbolicState otherCss = other.getStateForPart(mp);
				if (!thisCss.subsumes(otherCss)){
					return false;
				} 
			}
			return true;
		} else {
			return false;
		}
	}

	public Set<SymbolicState> gatherStates() {
		Set<SymbolicState> result = new HashSet<SymbolicState>();
		result.add(topState);
		for (ModelPart mp : partState.keySet()){
			result.addAll(partState.get(mp).gatherStates());
		}
		return result;
	}

	public ArcSymbolic getIngoingArc() {
		return ingoingArc;
	}

	public CompositeSymbolicState findStateForPart(ModelPart part, int i) {
		if (part.length() == i){
			return partState.get(part);
		}
		for (ModelPart mp : partState.keySet()){
			if (mp.agreesToDepth(part.getPath(), i)){
				CompositeSymbolicState css = partState.get(mp);
				return css.findStateForPart(part, i+1);
			}
		}
		return null;
	}

	public boolean subsumes(CompositeSymbolicState other,
			List<ModelPart> parts, ModelPart part) {
			if (parts.contains(part)){
				return topState.subsumes(other.topState);
			}
			if (topState.subsumes(other.topState) || !parts.contains(part)){
				
				for (ModelPart mp : partState.keySet()){
					CompositeSymbolicState thisCss = partState.get(mp);
					CompositeSymbolicState otherCss = other.getStateForPart(mp);
					if (!thisCss.subsumes(otherCss, parts, part)){
						return false;
					} 
				}
				return true;
			} else {
				return false;
			}
		
	}

	

}
