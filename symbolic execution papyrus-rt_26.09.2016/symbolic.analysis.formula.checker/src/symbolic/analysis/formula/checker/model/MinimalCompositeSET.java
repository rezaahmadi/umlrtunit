package symbolic.analysis.formula.checker.model;

import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;

public class MinimalCompositeSET extends SymbolicExecutionTree {

	//private NodeSymbolic root;
	
	public MinimalCompositeSET(NodeSymbolicCheck root){
		this.root = root;
	}
	
	public CompositeSymbolicState getRootState() {		
		return (CompositeSymbolicState) root.getContents();
	}

}
