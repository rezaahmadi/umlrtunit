package symbolic.analysis.formula.checker.model;

import java.util.HashSet;
//import java.util.Set;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;

public class AnyInputAction extends ActionInput {

	public AnyInputAction() {		
		super(new ActionInput("","any", new HashSet<ActionVariableInput<?>>()));
	}

}
