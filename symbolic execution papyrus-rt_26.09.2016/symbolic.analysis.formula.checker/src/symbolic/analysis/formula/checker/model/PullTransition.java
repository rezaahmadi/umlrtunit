package symbolic.analysis.formula.checker.model;

import symbolic.analysis.formula.checker.adaptation.ModelPart;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;

public class PullTransition extends SymbolicTransition{

	private ActionInputSymbolic input;
	
	private ModelPart part;
	
	public PullTransition(SymbolicTransition other, SymbolicExecutionTree tree) {
		super(other, tree);		
	}
	
	public PullTransition (SymbolicTransition other, SymbolicExecutionTree tree,
			ActionInputSymbolic input, ModelPart part){
		this(other,tree);
		this.input = input;
		this.part = part;
	}
	
	@Override
	public String toString() {
		return "PULL " + part.toString() + " for " + input.getAction().getName();
	}
	
	@Override
	public String printInLines() {		
		return toString();
	}

}
