package symbolic.analysis.formula.checker.model;

import java.util.HashSet;
import java.util.Set;

import symbolic.analysis.language.Formula;
//import symbolic.analysis.language.CompositeFormula;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
//import symbolic.execution.ffsm.engine.tree.TreeNode;

public class NodeSymbolicCheck extends NodeSymbolic{

	private Set<Formula> finalFormulas;
	private Set<Formula> tentativeFormulas;
	
	private boolean isExplored = false;
	
	public NodeSymbolicCheck(SymbolicState contents) {
		super(contents);
		finalFormulas = new HashSet<Formula>();
		tentativeFormulas = new HashSet<Formula>();
	}
	
	public void addFinal(Formula f){
		finalFormulas.add(f);
	}

	public boolean hasFormula(Formula formula) {		
		return finalFormulas.contains(formula);
	}

	public boolean hasFormulaChecked(Formula f) {
		return finalFormulas.contains(f);
	}

	public NodeSymbolicCheck getParent() {
		ArcSymbolic arc = ((CompositeSymbolicState)getContents()).getIngoingArc();
		if (arc != null){
			return (NodeSymbolicCheck) arc.getSource();
		}
		return null;
	}

	public boolean isExplored() {
		return isExplored;
	}	
	
	public void setExplored(){
		isExplored = true;
	}

	public boolean hasSubsumes() {
		return !subsumedNodes.isEmpty();
	}

}
