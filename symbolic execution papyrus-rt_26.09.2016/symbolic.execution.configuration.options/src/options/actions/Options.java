package options.actions;


//import javax.swing.JOptionPane;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
//import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
//import org.eclipse.jface.dialogs.IDialogConstants;
//import org.eclipse.jface.dialogs.MessageDialog;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import org.eclipse.swt.widgets.Text;

/**
 JDA: This class implements my options for Symbolic Execution.
 */
public class Options implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	/**
	 * The constructor.
	 */
	public Options() {
	}

	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		Display.getDefault().syncExec(new Runnable() {
		    public void run() {
		    	Display display = Display.getCurrent();
			    Shell shell = new Shell(display);
			    
			    GridLayout layout = new GridLayout(4, false);
			    layout.horizontalSpacing = 20;
			    layout.verticalSpacing = 20;
			    shell.setLayout(layout);
			    
			    // Maximum Depth Option

			    Label label = new Label(shell, SWT.NULL);
			    label.setText("Maximum Depth: ");
			    final Text maxDepth =  new Text(shell, SWT.WRAP | SWT.BORDER);
			    //maxDepth.setBounds(100, 50, 100, 20);
			    maxDepth.setTextLimit(10);
			    maxDepth.setText(Integer.toString(ExecutionOptions.getMaxDepth()));
			    

			    // Focus Listener for Maximum Depth.
			    // Note: This doesn't do very much because the focus can be lost when the
			    //       GUI is closed without a corresponding focus event.
			    FocusListener focusListener = new FocusListener() {
			      public void focusGained(FocusEvent e) {
			        Text t = (Text) e.widget;
			        t.selectAll();
			        //System.out.println("Focus gained: " + t.getText() + ".");
			      }

			      public void focusLost(FocusEvent e) {
			        Text t = (Text) e.widget;
			        if (t.getSelectionCount() > 0) {
			          t.clearSelection();
			        }
			        //System.out.println("Focus lost: " + t.getText() + ".");
			      }   

			    };
			    
			    // Check Box Options
			    
			    Group buttonGroup = new Group(shell, SWT.NONE);
			    GridLayout gridLayout = new GridLayout();
			    gridLayout.numColumns = 1;
			    buttonGroup.setLayout(gridLayout);
			    buttonGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			    //buttonGroup.setLocation(0, 100);
			    
			    
			    // Check Box Option - Use new execution model
			    
			    final Button cbNewExecution = new Button(buttonGroup, SWT.CHECK);
			    cbNewExecution.setText("New execution mode");
			    cbNewExecution.setSelection(ExecutionOptions.getNewExecution());
			    
			 // Check Box Option - Check depth mode
			    
			    final Button cbCheckDepth = new Button(buttonGroup, SWT.CHECK);
			    cbCheckDepth.setText("Check depth mode");
			    cbCheckDepth.setSelection(ExecutionOptions.getCheckDepth());
			    
			    // Check Box Option - Use new name generation model
			    
			    final Button cbNewNameGeneration = new Button(buttonGroup, SWT.CHECK);
			    cbNewNameGeneration.setText("New name generation mode");
			    cbNewNameGeneration.setSelection(ExecutionOptions.getNewNameGeneration());
			    
			    // Check Box Option - Check for duplicate PCs
			    
			    final Button cbCheckForDuplicatePCs = new Button(buttonGroup, SWT.CHECK);
			    cbCheckForDuplicatePCs.setText("Check for duplicate PCs");
			    cbCheckForDuplicatePCs.setSelection(ExecutionOptions.getCheckForDuplicatePCs());
			    
			    // Check Box Option - Check solvability
			    
			    final Button cbCheckSolvability = new Button(buttonGroup, SWT.CHECK);
			    cbCheckSolvability.setText("Check solvability");
			    cbCheckSolvability.setSelection(ExecutionOptions.getCheckSolvability());
			    
			    // Check Box Selection Listener
			    
			    SelectionAdapter selectionListener = new SelectionAdapter () {
			         public void widgetSelected(SelectionEvent event) {
			            Button button = ((Button) event.widget);
			            if (button == cbNewExecution) {
			            	ExecutionOptions.setNewExecution(cbNewExecution.getSelection());
			            	//System.out.println("cbNewExecution.getSelection(): " + 
			            	//		cbNewExecution.getSelection() + ".");
			            }
			            else if (button == cbCheckDepth) {
			            	ExecutionOptions.setCheckDepth(cbCheckDepth.getSelection());
			            	//System.out.println("cbCheckDepth.getSelection().getSelection(): " + 
			            	//		cbCheckDepth.getSelection() + ".");
			            }
			            else if (button == cbNewNameGeneration) {
			            	ExecutionOptions.setNewNameGeneration(cbNewNameGeneration.getSelection());
			            	//System.out.println("cbNewNameGeneration(): " + 
			            	//		cbNewNameGeneration.getSelection() + ".");
			            }
			            else if (button == cbCheckForDuplicatePCs) {
			            	ExecutionOptions.setCheckForDuplicatePCs(cbCheckForDuplicatePCs.getSelection());
			            	//System.out.println("cbCheckForDuplicatePCs.getSelection(): " + 
			            	//		cbCheckForDuplicatePCs.getSelection() + ".");
			            }
			            else if (button == cbCheckSolvability) {
			            	ExecutionOptions.setCheckSolvability(cbCheckSolvability.getSelection());
			            	//System.out.println("cbCheckSolvability.getSelection(): " + 
			            	//		cbCheckSolvability.getSelection() + ".");
			            }
			         };
			    };
			    
			    // Installing Check Box Selection Listeners
			    
			    cbNewExecution.addSelectionListener(selectionListener);
			    cbCheckDepth.addSelectionListener(selectionListener);
			    cbNewNameGeneration.addSelectionListener(selectionListener);
			    cbCheckForDuplicatePCs.addSelectionListener(selectionListener);
			    cbCheckSolvability.addSelectionListener(selectionListener);
			    maxDepth.addFocusListener(focusListener);
			    
			    // Set Maximum Depth Button
			    
			    Button setMaximumDepth = new Button(shell, SWT.PUSH);
			    setMaximumDepth.setText("Set Maximum Depth");
			    SelectionListener maxDepthListener = new SelectionListener() {
				
			    // Selection Listener for Set Maximum Depth Button
					@Override
					public void widgetSelected(SelectionEvent e) {
						System.out.println("Set Maximum Depth: selected.");
						int i = 0;
						try {
							i = Integer.parseInt(maxDepth.getText());
							if (i > 0) {
								ExecutionOptions.setMaxDepth(i);
							}
						}
						catch (Exception ex) {
							maxDepth.setText(Integer.toString(ExecutionOptions.getMaxDepth()));
						}
						maxDepth.setText(Integer.toString(ExecutionOptions.getMaxDepth()));
						System.out.println("maxDepth holds: " + maxDepth.getText());
					}
					
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						System.out.println("Set Maximum Depth: deselected.");	
					}
				};
				
				// Installing Maximum Set Maximum Depth Listener
				setMaximumDepth.addSelectionListener(maxDepthListener);
		        
				// Setting up the shell
			    shell.pack();
			    shell.open();
			    
			    // Event loop
			    while (!shell.isDisposed()) {
			      if (!display.readAndDispatch()) {
			        display.sleep();
			      }
			    }
			    // We do not dispose of the shell because that would kill the main program.
			    //display.dispose();
		    }
		});
		
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		System.out.println("action: " + action + ". selection: " + selection + ".");
		
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}

