package tests;

import symbolic.execution.ffsm.model.Explorable;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;

public class StringExplorable extends Explorable {
	private String value;
	
	public StringExplorable(String value){
		this.value = value;
	}
	
	
	@Override
	public void explore(FunctionalFiniteStateMachine ffsm) {
	}

	@Override
	public boolean isExplored() {
		return true;
	}

	@Override
	public String toString() {		
		return value;                                                                        
	}


	@Override
	public String getName() {
		// TODO: JDA: (was KZ) Auto-generated method stub
		return null;
	}
	
}
