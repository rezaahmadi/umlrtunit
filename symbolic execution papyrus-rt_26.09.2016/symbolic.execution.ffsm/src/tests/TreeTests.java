package tests;

//import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
//import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.engine.tree.TreeNode;
//import symbolic.execution.ffsm.model.Location;

public class TreeTests {
	
	public static void genericTreeTest(){
		
		TreeNode<String,Integer> s1 = new TreeNode<String, Integer>("s1");
		TreeNode<String,Integer> s2 = new TreeNode<String, Integer>("s2");
		TreeNode<String,Integer> s3 = new TreeNode<String, Integer>("s3");
		TreeNode<String,Integer> s4 = new TreeNode<String, Integer>("s4");
		TreeNode<String,Integer> s5 = new TreeNode<String, Integer>("s5");
		TreeNode<String,Integer> s6 = new TreeNode<String, Integer>("s6");
		TreeNode<String,Integer> s7 = new TreeNode<String, Integer>("s7");
		
		
		s1.addChild(s2,1);
		s1.addChild(s3,2);
		s2.addChild(s4,3);
		s2.addChild(s5,4);
		s3.addChild(s6,5);
		s3.addChild(s7,6);
		
		System.out.println(s1.print(0));
		
	}
	
	public static void main(String[] args) {
		genericTreeTest();
		
	}

}
