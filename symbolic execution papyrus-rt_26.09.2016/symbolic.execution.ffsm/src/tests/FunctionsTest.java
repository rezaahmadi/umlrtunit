package tests;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.choco.EngineChoco;
//import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
//import symbolic.execution.ffsm.engine.concrete.OutputSequenceExecution;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
//import symbolic.execution.ffsm.model.Action;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.Assignments;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.OutputSequence;
import symbolic.execution.ffsm.model.UpdateFunction;
//import symbolic.execution.ffsm.model.Valuation;
import symbolic.execution.utils.Pair;


public class FunctionsTest {
	
	public static void updateFunction(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		
		//define variables
		ActionVariableInput<Integer> var1 = new ActionVariableInput<Integer>("var1", Integer.class);
		ActionVariableInput<Integer> var2 = new ActionVariableInput<Integer>("var2", Integer.class);
		ffsm.addInputActionVariable(var1);
		ffsm.addInputActionVariable(var2);
		
		
		LocationVariable<Integer> varLoc = new LocationVariable<Integer>("varLoc", Integer.class);
		ffsm.addLocationVariable(varLoc);
		
		UpdateFunction func = new UpdateFunction("f1", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		Assignments outputs = new Assignments(ffsm.getLocationVariables());
		outputs.assign(varLoc, varLoc.getSymbolicVar());
		func.addCase(SymbolicFactory.createRelationalConstraint
				(var1.getSymbolicVar(), CompareOperators.geq, SymbolicFactory.createConstant(Integer.class, new Integer(0))),outputs);
		
		outputs = new Assignments(ffsm.getLocationVariables());
		outputs.assign(varLoc, SymbolicFactory.createBinaryExpression(varLoc.getSymbolicVar(), ArithmeticOperators.plus, createInteger(10)));
		func.addCase(SymbolicFactory.createLogicalConstraint(
				SymbolicFactory.createRelationalConstraint(var1.getSymbolicVar(), CompareOperators.geq, createInteger(0)), 
				LogicalOperators.and, 
				SymbolicFactory.createRelationalConstraint(varLoc.getSymbolicVar(), CompareOperators.geq, createInteger(10)))
				, outputs);
		
		
		System.out.println("FUNCTION: ");
		System.out.println(func);
		//create valuations
		ValuationSymbolic valuation = new ValuationSymbolic();
		valuation.setValue(varLoc, createInteger(15));
		
		System.out.println("VALUATION LOCATION");
		System.out.println(valuation.createConstraints());
		
		List<Constraint> valInputs = new ArrayList<Constraint>();
		//valInputs.add(createEqualInteger(var1, 1));
		//valInputs.add(createEqualInteger(var2, -10));
		
		System.out.println("CONSTRAINTS INPUT");
		System.out.println(valInputs);
		
		//create pc
		List<Constraint> pc = new ArrayList<Constraint>();
		pc.add(SymbolicFactory.createRelationalConstraint(var1.getSymbolicVar(), CompareOperators.leq, createInteger(0)));
		
		System.out.println("PC:");
		System.out.println(pc);
		
		List<Pair<Set<Constraint>, Assignments>> result = func.evaluateSymbolic(valuation, valInputs, pc);
		System.out.println("RESULT:");
		System.out.println(result);
	}
	
	private static Constraint createEqualInteger(
			ActionVariableInput<Integer> var, int i) {		
		return SymbolicFactory.createRelationalConstraint(var.getSymbolicVar(), CompareOperators.eq, createInteger(i));
	}

	private static Expression createInteger(int value) {		
		return SymbolicFactory.createConstant(Integer.class, new Integer(value));
	}

	public static void guardFunction(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		
		//define variables
		ActionVariableInput<Integer> var1 = new ActionVariableInput<Integer>("var1", Integer.class);
		ActionVariableInput<Integer> var2 = new ActionVariableInput<Integer>("var2", Integer.class);
		ffsm.addInputActionVariable(var1);
		ffsm.addInputActionVariable(var2);
		
		GuardFunction func = new GuardFunction("gf", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		func.addConstraint(createEqualInteger(var1, 0));
		
		System.out.println("FUNCTION: ");
		System.out.println(func);
		
		//create valuations
		ValuationSymbolic valuation = new ValuationSymbolic();
		
		System.out.println("VALUATION LOCATION");
		System.out.println(valuation.createConstraints());
		
		List<Constraint> valInputs = new ArrayList<Constraint>();
		//valInputs.add(createEqualInteger(var1, 1));
		//valInputs.add(createEqualInteger(var2, -10));
		
		System.out.println("CONSTRAINTS INPUT");
		System.out.println(valInputs);
		
		//create pc
		List<Constraint> pc = new ArrayList<Constraint>();
		pc.add(SymbolicFactory.createRelationalConstraint(var1.getSymbolicVar(), CompareOperators.leq, createInteger(0)));
		
		System.out.println("PC:");
		System.out.println(pc);
		
		List<Constraint> result = func.evaluateSymbolic(valuation, valInputs, pc);
		System.out.println("RESULT:");
		System.out.println(result);
	}
	
	public static void outputFunction(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		
		//define variables
		ActionVariableInput<Integer> var1 = new ActionVariableInput<Integer>("var1", Integer.class);
		ActionVariableInput<Integer> var2 = new ActionVariableInput<Integer>("var2", Integer.class);
		ffsm.addInputActionVariable(var1);
		ffsm.addInputActionVariable(var2);
		
		LocationVariable<Integer> varLoc = new LocationVariable<Integer>("varLoc", Integer.class);
		ffsm.addLocationVariable(varLoc);
		
		ActionVariableOutput<Integer> avo = new ActionVariableOutput<Integer>("outVar1", Integer.class); 
		ActionOutput ou = new ActionOutput("p","out1", avo);
		ffsm.addOutputAction(ou);
		
		OutputFunction func = new OutputFunction("of1", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		OutputSequence outSequence1 = new OutputSequence();
		ActionOutputInstance o1 = new ActionOutputInstance(ou);
		o1.setVariableMapping(avo, var1.getSymbolicVar());
		outSequence1.addItem(o1);		
		ActionOutputInstance o2 = new ActionOutputInstance(ou);
		o2.setVariableMapping(avo, var2.getSymbolicVar());
		outSequence1.addItem(o2);
		
		
		OutputSequence outSequence2 = new OutputSequence();
		
		ActionOutputInstance o3 = new ActionOutputInstance(ou);
		o3.setVariableMapping(avo, SymbolicFactory.createBinaryExpression(var1.getSymbolicVar(), ArithmeticOperators.minus, createInteger(10)));		
		outSequence2.addItem(o3);		
		ActionOutputInstance o4 = new ActionOutputInstance(ou);
		o4.setVariableMapping(avo, SymbolicFactory.createBinaryExpression(var2.getSymbolicVar(), ArithmeticOperators.minus, createInteger(10)));
		outSequence2.addItem(o4);
		
		func.addCase(SymbolicFactory.createRelationalConstraint(varLoc.getSymbolicVar(), CompareOperators.geq, createInteger(0)), outSequence1);
		func.addCase(SymbolicFactory.createRelationalConstraint(varLoc.getSymbolicVar(), CompareOperators.leq, createInteger(0)), outSequence2);
		
		
		System.out.println("FUNCTION: ");
		System.out.println(func);
		
		//create valuations
		ValuationSymbolic valuation = new ValuationSymbolic();
		valuation.setValue(varLoc, createInteger(0));
		
		System.out.println("VALUATION LOCATION");
		System.out.println(valuation.createConstraints());
		
		List<Constraint> valInputs = new ArrayList<Constraint>();
		//valInputs.add(createEqualInteger(var1, 1));
		//valInputs.add(createEqualInteger(var2, -10));
		
		System.out.println("CONSTRAINTS INPUT");
		System.out.println(valInputs);
		
		//create pc
		List<Constraint> pc = new ArrayList<Constraint>();
		pc.add(SymbolicFactory.createRelationalConstraint(var1.getSymbolicVar(), CompareOperators.leq, createInteger(0)));
		
		System.out.println("PC:");
		System.out.println(pc);
		
		List<Pair<Set<Constraint>,OutputSequence>> result = func.evaluateSymbolic(valuation, valInputs, pc);
		System.out.println("RESULT:");
		System.out.println(result);
	}
	
	public static void main(String[] args) {
		SymbolicFactory.setEngine(new EngineChoco());
		
		//updateFunction();
		//guardFunction();
		outputFunction();
	}

}
