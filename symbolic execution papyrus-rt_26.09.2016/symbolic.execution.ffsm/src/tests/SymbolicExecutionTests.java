package tests;

//import java.util.ArrayList;
import java.util.HashSet;
//import java.util.List;


import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.ffsm.engine.symbolic.ExecutionEngine;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
//import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.functions.NoOutputFunction;
import symbolic.execution.ffsm.functions.NoUpdateFunction;
import symbolic.execution.ffsm.functions.TrueGuardFunction;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.Assignments;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.OutputSequence;

import symbolic.execution.ffsm.model.Transition;
import symbolic.execution.ffsm.model.UpdateFunction;
import symbolic.execution.ffsm.model.Valuation;

//import symbolic.execution.utils.Pair;

public class SymbolicExecutionTests {
	private static Transition t1, t2, t3, t4, t5;
	private static LocationVariable<?> locVar1, locVar2;
	private static Valuation initialVal;
	
	public static FunctionalFiniteStateMachine createSimpleMachine(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		Location l1 = new Location(new StringExplorable("l1"));
		Location l2 = new Location(new StringExplorable("l2"));
		Location l3 = new Location(new StringExplorable("l3"));
		Location l4 = new Location(new StringExplorable("l4"));
		ffsm.addLocations(l1,l2,l3,l4);
		ffsm.setInitial(l1);
		
		ActionInput input = new ActionInput("p", "input", new HashSet<ActionVariableInput<?>>());
		
		GuardFunction gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		UpdateFunction uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		OutputFunction of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		Transition t1 = new Transition("t1",l1,l2,input,gf,uf,of);
		Transition t2 = new Transition("t2",l2,l3,input,gf,uf,of);
		Transition t3 = new Transition("t2", l2,l4,input,gf,uf,of);
		Transition t4 = new Transition("t4",l3,l1,input,gf,uf,of);
		Transition t5 = new Transition("t5",l4,l1,input,gf,uf,of);
		ffsm.addTransitions(t1,t2,t3,t4,t5);
		
		return ffsm;
		
	}
	
	public static FunctionalFiniteStateMachine createComplexMachine(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		Location l1 = new Location(new StringExplorable("l1"));
		Location l2 = new Location(new StringExplorable("l2"));
		Location l3 = new Location(new StringExplorable("l3"));
		Location l4 = new Location(new StringExplorable("l4"));
		Location l5 = new Location(new StringExplorable("l5"));
		Location l6 = new Location(new StringExplorable("l6"));
		ffsm.addLocations(l1,l2,l3,l4,l5,l6);
		ffsm.setInitial(l1);
		
		ActionInput input = new ActionInput("p","input", new HashSet<ActionVariableInput<?>>());
		
		GuardFunction gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		UpdateFunction uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		OutputFunction of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		Transition t1 = new Transition("t1",l1,l2,input,gf,uf,of);
		Transition t2 = new Transition("t2",l2,l3,input,gf,uf,of);
		Transition t3 = new Transition("t3",l2,l4,input,gf,uf,of);
		Transition t4 = new Transition("t4",l3,l1,input,gf,uf,of);
		Transition t5 = new Transition("t5",l4,l5,input,gf,uf,of);
		Transition t6 = new Transition("t6",l4,l6,input,gf,uf,of);
		Transition t7 = new Transition("t7",l5,l1,input,gf,uf,of);
		Transition t8 = new Transition("t8",l6,l1,input,gf,uf,of);
		ffsm.addTransitions(t1,t2,t3,t4,t5,t6,t7,t8);
		
		
		return ffsm;
	}
	
	public static FunctionalFiniteStateMachine createLocationVarsMachine(){		
		
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		Location l1 = new Location(new StringExplorable("l1"));
		Location l2 = new Location(new StringExplorable("l2"));
		Location l3 = new Location(new StringExplorable("l3"));
		Location l4 = new Location(new StringExplorable("l4"));
		ffsm.addLocations(l1,l2,l3,l4);
		ffsm.setInitial(l1);
		
		LocationVariable<Integer> loc1 = new LocationVariable<Integer>("loc1", Integer.class);
		ffsm.addLocationVariable(loc1);
		
		LocationVariable<Double> loc2 = new LocationVariable<Double>("loc2", Double.class);
		ffsm.addLocationVariable(loc2);
		
		ActionInput input = new ActionInput("p","input", new HashSet<ActionVariableInput<?>>());
		
		GuardFunction gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		UpdateFunction uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		OutputFunction of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		
		Transition t1 = new Transition("t1",l1,l2,input,gf,uf,of);
		Transition t2 = new Transition("t2",l2,l3,input,gf,uf,of);
		Transition t3 = new Transition("t3",l2,l4,input,gf,uf,of);
		Transition t4 = new Transition("t4",l3,l1,input,gf,uf,of);
		Transition t5 = new Transition("t5",l4,l1,input,gf,uf,of);
		ffsm.addTransitions(t1,t2,t3,t4,t5);
		
		Valuation v = new Valuation();
		v.setValue(loc1, 55);
		v.setValue(loc2, 1.67);
		
		ffsm.setInitialValuation(v);
		
		return ffsm;
	}
	
	private static FunctionalFiniteStateMachine createBasic(){
		FunctionalFiniteStateMachine ffsm = new FunctionalFiniteStateMachine("test");
		Location l1 = new Location(new StringExplorable("l1"));
		Location l2 = new Location(new StringExplorable("l2"));
		Location l3 = new Location(new StringExplorable("l3"));
		Location l4 = new Location(new StringExplorable("l4"));
		ffsm.addLocations(l1,l2,l3,l4);
		ffsm.setInitial(l1);
		
		locVar1 = new LocationVariable<Integer>("loc1", Integer.class);
		ffsm.addLocationVariable(locVar1);
		
		locVar2 = new LocationVariable<Double>("loc2", Double.class);
		ffsm.addLocationVariable(locVar2);
		
		initialVal = new Valuation();
		initialVal.setValue(locVar1, -55);
		initialVal.setValue(locVar2, 1.67);
		ffsm.setInitialValuation(initialVal);
		
		ActionInput input = new ActionInput("p","input", new HashSet<ActionVariableInput<?>>());
		
		GuardFunction gf = new TrueGuardFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		UpdateFunction uf = new NoUpdateFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		OutputFunction of = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		
		t1 = new Transition("t1",l1,l2,input,gf,uf,of);		
		t2 = new Transition("t1",l2,l3,input,gf,uf,of);
		t3 = new Transition("t1",l2,l4,input,gf,uf,of);
		t4 = new Transition("t1",l3,l1,input,gf,uf,of);
		t5 = new Transition("t1",l4,l1,input,gf,uf,of);
		
		ffsm.addTransitions(t1,t2,t3,t4,t5);
		
		return ffsm;
	}
	public static FunctionalFiniteStateMachine createGuardsLocVars(){				
		FunctionalFiniteStateMachine ffsm = createBasic();
		
		GuardFunction gfLoc1 = new GuardFunction("Loc1>0", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		gfLoc1.addConstraint(SymbolicFactory.createRelationalConstraint(
				locVar1.getSymbolicVar(), CompareOperators.ge, createIntegerConstant(0)));
		t2.setGf(gfLoc1);				
		
		initialVal.setValue(locVar1, 44);
		return ffsm;
		
	}

	private static Constant createIntegerConstant(int value) {
		return SymbolicFactory.createConstant(Integer.class, new Integer(value));
	}
	
	public static FunctionalFiniteStateMachine createGuardsInput(){
		FunctionalFiniteStateMachine ffsm = createBasic();
		
		ActionVariableInput<Integer> varInput = new ActionVariableInput<Integer>("inputVar", Integer.class);
		ffsm.addInputActionVariable(varInput);
		
		ActionInput inp = new ActionInput("p","in", varInput);
		ffsm.addInputAction(inp);
		
		GuardFunction gfInputVar = new GuardFunction("inputVar>0", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		gfInputVar.addConstraint(SymbolicFactory.createRelationalConstraint(
				varInput.getSymbolicVar(), CompareOperators.ge, createIntegerConstant(0)));
		
		t2.setInputAction(inp);
		t2.setGf(gfInputVar);
		return ffsm;
	}
	
	public static FunctionalFiniteStateMachine createSymbolicOutput(){
		FunctionalFiniteStateMachine ffsm = createBasic();
		
		ActionVariableInput<Integer> varInput = new ActionVariableInput<Integer>("inputVar", Integer.class);
		ffsm.addInputActionVariable(varInput);
		
		ActionInput inp = new ActionInput("p","in", varInput);
		ffsm.addInputAction(inp);
		
		ActionVariableOutput<Integer> varOutput = new ActionVariableOutput<Integer>("output", Integer.class);
		ActionOutput out = new ActionOutput("p","output", varOutput);		
		
		OutputFunction of = new OutputFunction("outLocVar",ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		OutputSequence os1 = new OutputSequence();
		ActionOutputInstance oi = new ActionOutputInstance(out);		
		oi.setVariableMapping(varOutput, varInput.getSymbolicVar());
		os1.addItem(oi);
		of.addCase(SymbolicFactory.createRelationalConstraint(varInput.getSymbolicVar(), CompareOperators.leq, createIntegerConstant(0)), os1);
		OutputSequence os2 = new OutputSequence();			
		ActionOutputInstance oi2 = new ActionOutputInstance(oi);
		oi2.setVariableMapping(varOutput, locVar1.getSymbolicVar());
		os2.addItem(oi2);
		of.addCase(SymbolicFactory.createRelationalConstraint(varInput.getSymbolicVar(), CompareOperators.geq, createIntegerConstant(0)), os2);		
		
		t2.setInputAction(inp);
		t2.setOf(of);
		
		return ffsm;

	}
	
	
	public static FunctionalFiniteStateMachine createSymbolicUpdate(){
		FunctionalFiniteStateMachine ffsm = createBasic();
		
		ActionVariableInput<Integer> varInput = new ActionVariableInput<Integer>("inputVar", Integer.class);
		ffsm.addInputActionVariable(varInput);
		
		ActionInput inp = new ActionInput("p","in", varInput);
		ffsm.addInputAction(inp);		
		
		
		OutputFunction noOutput = new NoOutputFunction(ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		
		UpdateFunction update = new UpdateFunction("toInputVar", ffsm.getActionVarsInput(), ffsm.getLocationVariables());
		Assignments assign = new Assignments(ffsm.getLocationVariables());
		assign.assign(locVar1, varInput.getSymbolicVar());
		update.addCase(SymbolicFactory.createRelationalConstraint(locVar1.getSymbolicVar(), CompareOperators.geq, createIntegerConstant(0)), assign);
		
		assign = new Assignments(ffsm.getLocationVariables());
		assign.assign(locVar2, SymbolicFactory.createConstant(Double.class, 77.5));
		update.addCase(SymbolicFactory.createRelationalConstraint(locVar1.getSymbolicVar(), CompareOperators.leq, createIntegerConstant(0)), assign);
		
		
		initialVal.setValue(locVar1, 10);
		
		t2.setInputAction(inp);
		t2.setUf(update);
		
		return ffsm;

	}
	
	public static void main(String[] args) {
		SymbolicFactory.setEngine(new EngineChoco());
		
		//FunctionalFiniteStateMachine ffsm = createSimpleMachine();
		FunctionalFiniteStateMachine ffsm = createSymbolicUpdate();
		
		ExecutionEngine engine = new ExecutionEngine(ffsm);
		SymbolicExecutionTree tree = engine.execute();
		System.out.println("Done");
		
		System.out.println(tree.toString());
	}

}
