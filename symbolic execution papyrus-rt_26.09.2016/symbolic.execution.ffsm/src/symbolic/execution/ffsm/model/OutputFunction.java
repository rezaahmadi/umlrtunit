package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.concrete.OutputSequenceExecution;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.utils.Pair;

public class OutputFunction extends Function{
	private Set<ActionVariableInput<?>> inputVariables;
	private Set<LocationVariable<?>> locationVariables;
	
	protected Map<Set<Constraint>, OutputSequence> cases;
	
	private Solver solver;
	
	protected OutputFunction(String name){
		super(name);
	}

	public OutputFunction(String name, Set<ActionVariableInput<?>> inputVaraibles, 
			Set<LocationVariable<?>> locationVariables){
		this(name);
		
		this.inputVariables = inputVaraibles;
		this.locationVariables = locationVariables;
		
		cases = new HashMap<Set<Constraint>, OutputSequence>();
		
		solver = SymbolicFactory.createSolver();
		
	}
	
	public void addCase(Set<Constraint> caseConstraints, OutputSequence outputSeq){
		cases.put(caseConstraints, outputSeq);
	}
	
	public void addCase(Constraint caseConstraint, OutputSequence outputSeq){
		Set<Constraint> caseConstraints = new HashSet<Constraint>();
		caseConstraints.add(caseConstraint);
		cases.put(caseConstraints, outputSeq);
	}

	public List<Pair<Set<Constraint>, OutputSequence>> evaluateSymbolic(
			ValuationSymbolic locVars, List<Constraint> inputVars, List<Constraint> pc) {
		
		List<Pair<Set<Constraint>, OutputSequence>> outputs = new ArrayList<Pair<Set<Constraint>,OutputSequence>>();
				
		//create constraints
		List<Constraint> constraints = new ArrayList<Constraint>();
		
		//add valuation constraints
		constraints.addAll(locVars.createConstraints());
		
		//add input constraints
		constraints.addAll(inputVars);
		
		//add pc
		constraints.addAll(pc);
		
		for (Set<Constraint> caseConstraints : cases.keySet()){								
			boolean trueSolvable = solver.isSolvable(constraints, caseConstraints, true);
			boolean falseSolvable = solver.isSolvable(constraints,caseConstraints, false);			
			
			//System.out.println("false solvable " + falseSolvable);
			if (trueSolvable){
				if(falseSolvable){
					outputs.add(new Pair<Set<Constraint>, OutputSequence>(caseConstraints, cases.get(caseConstraints)));
				}else{
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsTrue, cases.get(caseConstraints)));
				}
			} else {
				Set<Constraint> constraintsFalse = new HashSet<Constraint>();
				constraintsFalse.add(SymbolicFactory.createFalse());
				outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsFalse, cases.get(caseConstraints)));
			}
			
		}
		
		return outputs;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Set<Constraint> cs : cases.keySet()){
			sb.append(cs.toString());
			sb.append("->");
			sb.append(cases.get(cs).toString());
			sb.append(";");
		}
		return sb.toString();		
	}

	//TODO: JDA: (was KZ) must implement concrete evaluation for output func.
	//TODO: JDA: It's unclear to me what KZ wanted to do with this. The method returns null.
	public OutputSequenceExecution evaluate(Valuation inputVariables2,
			Valuation valuation) {
		return null;
	}

	public List<Pair<Set<Constraint>, OutputSequence>> evaluateSymbolic(
			List<Constraint> pcReplaced, Map<SymbolicVariable,Expression> replacement) {
		List<Pair<Set<Constraint>, OutputSequence>> outputs = new ArrayList<Pair<Set<Constraint>,OutputSequence>>();
		
		for (Set<Constraint> caseConstraints : cases.keySet()){	
			Set<Constraint> caseConstraintsReplaced = replace(caseConstraints, replacement);			
			
			if (caseConstraintsReplaced==null){
				Set<Constraint> constraintsFalse = new HashSet<Constraint>();
				constraintsFalse.add(SymbolicFactory.createFalse());
				outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsFalse, cases.get(caseConstraints)));
				continue;
			} else {
				if (caseConstraintsReplaced.isEmpty()){
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsTrue, cases.get(caseConstraints)));
					continue;
				}
				
			}
			
			boolean trueSolvable = solver.isSolvable(pcReplaced, caseConstraintsReplaced, true);
			boolean falseSolvable = solver.isSolvable(pcReplaced,caseConstraintsReplaced, false);			
			
			//System.out.println("false solvable " + falseSolvable);
			if (trueSolvable){
				if(falseSolvable){
					outputs.add(new Pair<Set<Constraint>, OutputSequence>(caseConstraintsReplaced, cases.get(caseConstraints)));
				}else{
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsTrue, cases.get(caseConstraints)));
				}
			} else {
				Set<Constraint> constraintsFalse = new HashSet<Constraint>();
				constraintsFalse.add(SymbolicFactory.createFalse());
				outputs.add(new Pair<Set<Constraint>, OutputSequence>(constraintsFalse, cases.get(caseConstraints)));
			}
			
		}
		
		return outputs;
	}

	private Set<Constraint> replace(Set<Constraint> caseConstraints,
			Map<SymbolicVariable, Expression> replacement) {
		Set<Constraint> replaced = new HashSet<Constraint>();
		for (Constraint cst : caseConstraints){
			Constraint replace = (Constraint)cst.replaceVariables(replacement);
			if (replace.equals(ff)){
				return null;
			}
			if (!replace.equals(tt)){
				replaced.add(replace);
			}			
		}
		return replaced;
	}
	
	private static final False ff = SymbolicFactory.createFalse();
	private static final True tt = SymbolicFactory.createTrue();
}
