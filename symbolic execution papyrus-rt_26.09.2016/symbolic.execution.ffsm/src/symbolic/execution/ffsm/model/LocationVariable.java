package symbolic.execution.ffsm.model;

import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;

public class LocationVariable<T> extends ModelVariable<T> {
	
	public LocationVariable(String name, Class<T> type) {
		super(name, type);		
	}

	public LocationVariable(String name, java.lang.Class<T> type,
			PassiveClass pc) {
		super(name, type, pc);
	}

}
