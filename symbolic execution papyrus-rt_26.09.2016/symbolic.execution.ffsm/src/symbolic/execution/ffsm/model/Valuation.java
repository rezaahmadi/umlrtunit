package symbolic.execution.ffsm.model;

import java.util.HashMap;
//import java.util.HashSet;
import java.util.Map;
//import java.util.Set;

public class Valuation {
	private Map<ModelVariable<?>, Value<?>> values;
	
	public Valuation(){
		values = new HashMap<ModelVariable<?>, Value<?>>();
	}
	
	public Valuation(Valuation valuation) {
		values = new HashMap<ModelVariable<?>, Value<?>>(valuation.values);
	}

	public <T> void setValue(ModelVariable<T> var, Value<T> value){
		values.put(var, value);		
	}
	
	public <T> T getValue(ModelVariable<T> var){
		if (values.containsKey(var)){
			return (T) values.get(var).getValue();
		}
		return null;
	}

	public void setValue(ModelVariable<?> var, Object value) {
		Value valueObj = new Value(var, value);
		values.put(var, valueObj);
		
	}

	@Override
	public String toString() { 
		return values.toString();
	}

}
