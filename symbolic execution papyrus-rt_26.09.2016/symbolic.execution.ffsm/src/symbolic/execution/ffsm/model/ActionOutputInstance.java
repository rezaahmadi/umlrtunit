package symbolic.execution.ffsm.model;

import java.util.HashMap;
import java.util.Map;

import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;

public class ActionOutputInstance extends ActionOutput{
	
	private ActionOutput action;
	
	private Map<ActionVariableOutput<?>, Expression> variableMapping;
	
	private Map<SymbolicVariable, Expression> symbolicVariableMapping;
	
	public ActionOutputInstance(ActionOutput action){
		super(action.portName, action.signalName, action.getVars());		
		this.action = action;
		
		variableMapping = new HashMap<ActionVariableOutput<?>, Expression>();
		symbolicVariableMapping = new HashMap<SymbolicVariable, Expression>();
	}
	
	public ActionOutputInstance(ActionOutputInstance other, ActionOutput actionOutput){
		super(other.portName, other.signalName, other.getVars());		
		this.action = actionOutput;
		
		variableMapping = new HashMap<ActionVariableOutput<?>, Expression>(other.variableMapping);
		symbolicVariableMapping = new HashMap<SymbolicVariable, Expression>(other.symbolicVariableMapping);
	}


	public void setVariableMapping(ActionVariableOutput<?> var, Expression expr){
		variableMapping.put(var, expr);
	}

	
	public void setSymbolicVariableMapping(SymbolicVariable var, Expression expr){
		symbolicVariableMapping.put(var, expr);
	}

	public String getActionOutputName() {		
		return action.getName();
	}


	public ActionOutput getActionOutput() {		
		return action;
	}


	public Expression getVariableMapping(ActionVariableOutput<?> var) {		
		return variableMapping.get(var);
	}
	
	public Expression getSymbolicVariableMapping (SymbolicVariable var){
		return symbolicVariableMapping.get(var);
	}

	@Override
	public String toString() {
		return name + variableMapping;
	}

	public Map<ActionVariableOutput<?>, Expression> getSymbolicMapping() {
		return variableMapping;
	}
	
	public Map<SymbolicVariable, Expression> getSymbolicVariableMapping(){
		return symbolicVariableMapping;
	}

	public void replaceAction(ActionOutput action) {		
		action.vars = this.action.vars;
		this.action = action;
		
	}

	
	
}
