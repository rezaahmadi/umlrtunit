package symbolic.execution.ffsm.model;

import java.text.NumberFormat;

public class Value<T> {
	private ModelVariable<T> variable;
	private T value;
	
	public Value(){}
	
	public Value(ModelVariable<T> variable){
		this.variable = variable;
	}
	
	public Value(ModelVariable<T> variable, T value){
		this.variable = variable;
		this.value = value;
	}
	
	public T getValue(){
		return value;
	}

	@Override
	public String toString() {	
		if (value instanceof Double){
			Double valD = (Double) value;
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(3);
			return nf.format(valD);
		}
		return value.toString();
	}
}
