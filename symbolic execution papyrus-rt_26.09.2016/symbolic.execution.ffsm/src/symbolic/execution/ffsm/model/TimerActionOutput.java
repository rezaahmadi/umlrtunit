package symbolic.execution.ffsm.model;

import java.util.HashSet;
//import java.util.Set;

public class TimerActionOutput extends ActionOutput{

	private ActionVariableOutput<Integer> timer; 
	
	public TimerActionOutput(String name) {
		super("",name);
		timer = new ActionVariableOutput<Integer>("timerVar",Integer.class);
		vars = new HashSet<ActionVariableOutput<?>>();
		vars.add(timer);		
	}
	
	@Override
	public String toString() {		
		return getName() + ".informIn";
	}

}
