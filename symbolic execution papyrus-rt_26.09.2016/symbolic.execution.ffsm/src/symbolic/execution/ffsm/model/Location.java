package symbolic.execution.ffsm.model;

public class Location {
	private Explorable contents;
	
	private String name;
	
	public Location(Explorable contents){
		this.contents = contents;
		contents.setLocation(this);
		this.name = contents.toString();
		if (name == null){
			name = "initial";
		}
	}

	public Location(String name){
		this.name = name;
	}
	
	public String getName(){		
		return name;
	}
	
	
	@Override
	public String toString() {		
		return name;
	}
	
	public void explore(FunctionalFiniteStateMachine ffsm){
		if (!contents.isExplored()){
			contents.explore(ffsm);
		}
	}
	
	//JDA: Added the following to take care of the new 'Error' location.
	private boolean isError(Object o) {
		if (o instanceof Location) {
			Location loc = (Location)o;
			return (loc.contents == null) && (loc.name.equals("Error"));
		}
		return false;
	}
	@Override
	public boolean equals(Object o) {
		//JDA: added the following for the new 'Error' location.
		if (o instanceof Location){
			if ((contents == null)) {
				
				//JDA: debug output
				//System.out.println("JDA: Location.equals: contents is null, "+
				//		"name = [[" + name + "]].");
				return isError(this) && isError(o);
			}
			return contents.equals(((Location)o).contents);
		}
		return false;
	}
	
	public Explorable getContents(){
		return contents;
	}

	public String getNameExplorable() {
		String n = contents.getName();
		if (n==null || n.equals("null")){
			n="initial";
		}
		return n;
	}
}
