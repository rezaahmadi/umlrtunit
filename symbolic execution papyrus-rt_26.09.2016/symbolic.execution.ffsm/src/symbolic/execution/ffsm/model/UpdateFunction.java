package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.constraints.manager.operators.LogicalOperators;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.utils.Pair;

public class UpdateFunction extends Function{
	
	//private Set<ActionVariableInput<?>> inputVariables;	
	//private Set<LocationVariable<?>> locationVariables;
	
	protected Map<Set<Constraint>, Assignments> cases;
	
	private Solver solver;
	
	protected UpdateFunction(String name){
		super(name);
		
		solver = SymbolicFactory.createSolver();
	}

	public UpdateFunction(String name, Set<ActionVariableInput<?>> inputVariables,
			Set<LocationVariable<?>> locationVariables){
		this(name);
		
		cases = new HashMap<Set<Constraint>, Assignments>();		
		
		//this.inputVariables = inputVariables;
		//this.locationVariables = locationVariables;		
	}
	
	public void addCase(Set<Constraint> caseConstraint, Assignments caseExpressions){
		cases.put(caseConstraint, caseExpressions);
	}
	
	public void addCase(Constraint caseConstraint, Assignments caseExpressions){
		Set<Constraint> caseConstraints = new HashSet<Constraint>();
		caseConstraints.add(caseConstraint);
		cases.put(caseConstraints, caseExpressions);
	}

	public List<Pair<Set<Constraint>, Assignments>> evaluateSymbolic(ValuationSymbolic locVars, 
			List<Constraint> inputVars, List<Constraint> pc) {
		List<Pair<Set<Constraint>, Assignments>> constrained = new ArrayList<Pair<Set<Constraint>,Assignments>>(); 
		
		//create list of constraints 
		List<Constraint> constraints = new ArrayList<Constraint>();
		
		//add constraints from a valuation 
		constraints.addAll(locVars.createConstraints());
		
		//add constraints from pc
		constraints.addAll(pc);
		
		//add constraints from input vars
		constraints.addAll(inputVars);
		
		for (Set<Constraint> caseConstraints : cases.keySet()){						
			//solve for true
			boolean trueSolvable = solver.isSolvable(constraints, caseConstraints, true);			
			
			//solve for false
			boolean falseSolvable = solver.isSolvable(constraints,caseConstraints, false);						
						
			if (trueSolvable){				
				if (falseSolvable){
					constrained.add(new Pair<Set<Constraint>, Assignments>(caseConstraints, cases.get(caseConstraints)));
				} else {
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsTrue, cases.get(caseConstraints)));
				}
					
			} else {
				Set<Constraint> constraintsFalse = new HashSet<Constraint>();
				constraintsFalse.add(SymbolicFactory.createFalse());
				constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsFalse, cases.get(caseConstraints)));
			}
		}
		
		return constrained;
	}

	/* JDA begin remove
	private Constraint createFromSet(Set<Constraint> constraints) {
		Constraint result = null;
		if (constraints.isEmpty()){
			return SymbolicFactory.createTrue();
		}
		Iterator<Constraint> iterator = constraints.iterator();		
		result = iterator.next();		
		while(iterator.hasNext()){
			result = SymbolicFactory.createLogicalConstraint(result, LogicalOperators.and, iterator.next());
		}
		return result;
	}
	JDA: end remove */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Set<Constraint> cs : cases.keySet()){
			sb.append(cs.toString());
			sb.append("->");
			sb.append(cases.get(cs).toString());
			sb.append(";");
		}
		return sb.toString();
	}
	
	//TODO: JDA: (was KZ)  implement concrete evaluation for update func. ...
	//TODO: JDA: It's unclear what KZ wanted to do with this. The method returns null.
	public Valuation evaluate(Valuation inputVariables2, Valuation valuation) {
		return null;
	}

	private static final False ff = SymbolicFactory.createFalse();
	private static final True tt = SymbolicFactory.createTrue();
	public List<Pair<Set<Constraint>, Assignments>> evaluateSymbolic(
			List<Constraint> pc, Map<SymbolicVariable, Expression> replacement) {
		
		List<Pair<Set<Constraint>, Assignments>> constrained = new ArrayList<Pair<Set<Constraint>,Assignments>>(); 
		
		//create list of constraints from pc 
		List<Constraint> constraintsPC = new ArrayList<Constraint>();
		for (Constraint cst : pc){
			Constraint replaced = (Constraint)cst.replaceVariables(replacement);			
				constraintsPC.add(replaced);
		}
		
		
		
		for (Set<Constraint> caseConstraints : cases.keySet()){
			
			//create list of repalced case constraints
			Set<Constraint> caseCsts = new HashSet<Constraint>();
			boolean isFalse = false;
			for (Constraint cst : caseConstraints){
				Constraint replaced = (Constraint)cst.replaceVariables(replacement);
				if (replaced.equals(ff)){
					Set<Constraint> constraintsFalse = new HashSet<Constraint>();
					constraintsFalse.add(SymbolicFactory.createFalse());
					constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsFalse, cases.get(caseConstraints)));
					isFalse = true;
					continue;
				}
				if (!replaced.equals(tt)){
					caseCsts.add(replaced);
				}
			}
			
			if (caseCsts.isEmpty()){
				if (!isFalse){
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsTrue, cases.get(caseConstraints)));
				}
				continue;
			}
			
						
			//solve for true
			boolean trueSolvable = solver.isSolvable(constraintsPC, caseCsts, true);			
			
			//solve for false
			boolean falseSolvable = solver.isSolvable(constraintsPC,caseCsts, false);						
						
			if (trueSolvable){				
				if (falseSolvable){
					constrained.add(new Pair<Set<Constraint>, Assignments>(caseCsts, cases.get(caseConstraints)));
				} else {
					Set<Constraint> constraintsTrue = new HashSet<Constraint>();
					constraintsTrue.add(SymbolicFactory.createTrue());
					constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsTrue, cases.get(caseConstraints)));
				}
					
			} else {
				Set<Constraint> constraintsFalse = new HashSet<Constraint>();
				constraintsFalse.add(SymbolicFactory.createFalse());
				constrained.add(new Pair<Set<Constraint>, Assignments>(constraintsFalse, cases.get(caseConstraints)));
			}
		}
		
		return constrained;

	}

}
