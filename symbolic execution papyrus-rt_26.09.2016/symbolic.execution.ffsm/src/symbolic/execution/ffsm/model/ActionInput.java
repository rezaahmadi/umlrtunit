package symbolic.execution.ffsm.model;

import java.util.HashSet;
import java.util.Set;
//import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;

/* ==============================================================================================
History:
    Commented out other used code. April 22, 2016.
	Bugs 2: Removed code no longer used after Bug 2 fix. April 17, 2016.
================================================================================================ */
public class ActionInput extends Action{		
	
	private int numberOfExecs = 0;
	
	//private boolean claimed = false;
	
	protected Set<ActionVariableInput<?>> vars;
	
	public ActionInput(String portName, String signalName, Set<ActionVariableInput<?>> vars) {
		super(portName, signalName);
		this.vars = vars;
	}
	
	public ActionInput(String portName, String signalName, ActionVariableInput<?>... variables){
		super(portName, signalName);
		vars = new HashSet<ActionVariableInput<?>>();
		for (ActionVariableInput<?> var : variables){
			vars.add(var);
		}
	}

	public ActionInput(ActionInput action) {
		super(action.portName, action.signalName);
		this.vars = action.vars;
	}

	public Set<ActionVariableInput<?>> getVars(){
		return new HashSet<ActionVariableInput<?>>(vars);
	}
	@Override
	public String toString() {
		return name;
	}


	
	public int useNumberOfExecs(){
		return numberOfExecs++;
	}
	
	//JDA: Bug 2. The following is no longer needed.
	/*
	public int getNumberOfExecs(){
		//System.out.println("JDA: ActionInput.getNumberOfExecs: name = " + name + ". claimed = " + claimed + 
		//		". numberOfExecs = " + numberOfExecs + ".");
		claimed = ActionInputSuffixGenerator.isClaimed(name);
		if (! claimed) {
			numberOfExecs = ActionInputSuffixGenerator.getSuffix(name);
			claimed = true;
		}
		//System.out.println("JDA: ActionInput.getNumberOfExecs: returning: " + numberOfExecs +
		//		". numberOfExecs = " + numberOfExecs +
		//		". claimed = " + claimed + "." );
		return numberOfExecs;
	}
	*/
	public String getName() {
		return name;
	}

	public void setVars(Set<ActionVariableInput<?>> vars) {
		this.vars = vars;
		
	}

	public void putNumberOfExecs(int i) {
		numberOfExecs = i;
		
	}

	
	
}
