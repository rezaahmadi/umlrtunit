package symbolic.execution.ffsm.model;

import java.util.Collection;
//import java.util.Set;

import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;


public abstract class ModelVariable<T> {
	private String name;
	private Class<T> type;
	private SymbolicVariable symbolicVar;
	
	
	
	public ModelVariable(String name, Class<T> type){
		this.name = name;
		this.type = type;
		this.symbolicVar = SymbolicFactory.createSymbolicVariable(name, type);
		
	}
	
	public ModelVariable(ModelVariable<T> other, String suffix){
		this.name = other.name;
		this.type = other.type;
	}
	
	public ModelVariable(String name, Class<T> type,
			PassiveClass pc) {
		this.name = name;
		this.type = type;
		this.symbolicVar = pc.createObject(name);
	}

	public SymbolicVariable generateConstraintVariable(String suffix) {
		if (isObject()){
			return ((PassiveObject)symbolicVar).getPassiveClass().createObject(name + suffix);
		}
		return SymbolicFactory.createSymbolicVariable(name + suffix, type);
	}
	
	public void setSymbolicVariable(SymbolicVariable var) {
		this.symbolicVar = var;		
	}
	


	public Class<T> getType(){
		return type;
	}
	
	public String getName(){
		return name;
	}
		
	@Override
	public String toString() {
		return name;
	}
	
	public SymbolicVariable getSymbolicVar(){
		return symbolicVar;
	}
		
	
	@Override
	public boolean equals(Object obj) {
		if (this==obj)
			return true;
		if (obj instanceof ModelVariable<?> ){
			ModelVariable<?> other = (ModelVariable<?>) obj;
			if (other.name==null){
				return name==null;
			}
			return other.name.equals(name);
		}
		return super.equals(obj);
	}
	
	public boolean isObject(){
		return type.equals(PassiveClass.class);
	}
	
	public Collection<SymbolicVariable> getSymbolicVarsPC(){
		PassiveObject obj = (PassiveObject) symbolicVar;
		return obj.getFieldVars();
	}
}
