package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.PassiveClass;

public class FunctionalFiniteStateMachine {
	private Set<Location> locations;
	
	private Set<LocationVariable<?>> locationVariables;
	
	private Set<ActionVariableInput<?>> actionVarsInput;
	
	private Set<ActionVariableOutput<?>> actionVarsOutput;
	
	private Set<ActionInput> actionsInput;
	
	private Set<ActionOutput> actionsOutput;
	
	private Set<GuardFunction> guardFunctions;
	
	private Set<UpdateFunction> updateFunctions;
	
	private Set<OutputFunction> outputFunctions;
	
	private List<Transition> transitions;
	
	private Location initialLocation;
	
	private Valuation initialValuation;
	
	private String name;
	
	private Map<String, Location> locationsMap;
	
	private Map<String, ActionInput> actionsInputMap;
	
	private Map<String, ActionOutput> actionsOutputMap;
	
	private Map<String, LocationVariable<?>> locationVarsMap;
		
	private Map<String,PassiveClass> passiveClasses;
	
	private FunctionalFiniteStateMachine(){
		locations = new HashSet<Location>();
		locationVariables = new HashSet<LocationVariable<?>>();
		actionVarsInput = new HashSet<ActionVariableInput<?>>();
		actionVarsOutput = new HashSet<ActionVariableOutput<?>>();
		actionsInput = new HashSet<ActionInput>();
		actionsOutput = new HashSet<ActionOutput>();
		guardFunctions = new HashSet<GuardFunction>();
		updateFunctions = new HashSet<UpdateFunction>();
		outputFunctions = new HashSet<OutputFunction>();
		transitions = new ArrayList<Transition>();
		
		locationsMap = new HashMap<String, Location>();
		actionsInputMap = new HashMap<String, ActionInput>();
		actionsOutputMap = new HashMap<String, ActionOutput>();
		locationVarsMap = new HashMap<String, LocationVariable<?>>();
		passiveClasses = new HashMap<String,PassiveClass>();
	}
	
	public FunctionalFiniteStateMachine(String name){
		this();
		this.name = name;
	}
	
	public void addLocation(Location location){
		locations.add(location);
		locationsMap.put(location.getNameExplorable(), location);
	}
	
	public void addLocations(Location... locs) {		
		locations.addAll(Arrays.asList(locs));	
		
		for (Location loc : locs){
			locationsMap.put(loc.getNameExplorable(), loc);
		}
	}
	
	public void setInitial(Location initial){
		locations.add(initial);
		this.initialLocation = initial;
	}
	
	public void addLocationVariable(LocationVariable<?> locationVar){
		locationVariables.add(locationVar);
		locationVarsMap.put(locationVar.getName(), locationVar);
	}
	
	public void addInputAction(ActionInput action){
		actionsInput.add(action);
		actionVarsInput.addAll(action.getVars());
		
		actionsInputMap.put(action.getName(), action);
	}

	public void addInputActionVariable(ActionVariableInput<?> variable){
		actionVarsInput.add(variable);
	}
	
	public void addOutputAction(ActionOutput action){
		actionsOutput.add(action);
		actionVarsOutput.addAll(action.getVars());
		
		actionsOutputMap.put(action.getName(),action);
	}
	
	
	public void addGuardFunction(GuardFunction gf){
		guardFunctions.add(gf);
	}
	
	public void addUpdateFunction(UpdateFunction uf){
		updateFunctions.add(uf);
	}
	
	public void addOutputFunction(OutputFunction of){
		outputFunctions.add(of);
	}
	
	public void addTransition(Transition t){
		if (t.hasTimerTrigger()){
			transitions.add(0,t);
		} else {
			transitions.add(t);
		}
	}	

	public void addTransitions(Transition... locs) {		
		transitions.addAll(Arrays.asList(locs));		
	}
	
	public void setInitialValuation(Valuation val){
		initialValuation = val;
	}
	
	/********************** GETTER METHODS ****************/
	//JDA - added (but commented out)
	//public List<Transition> getTransitions() {
	//	return transitions;
	//}
	
	
	public Location getInitialLocation() {
		return initialLocation;
	}
	
	public Valuation getInitialValuation() {
		return initialValuation;
	}

	public Set<LocationVariable<?>> getLocationVariables() {
		return locationVariables;
	}
	
	public Set<ActionInput> getActionsInput() {
		return actionsInput;
	}

	public Set<ActionOutput> getActionsOutput() {
		return actionsOutput;
	}

	public Set<ActionVariableInput<?>> getActionVarsInput() {
		return actionVarsInput;
	}

	public Set<ActionVariableOutput<?>> getActionVarsOutput() {
		return actionVarsOutput;
	}

	/*************** PRINTING METHODS ***************/
	
	public String toStringLocations(){
		return locations.toString();
	}
	
	public String toStringInitial(){
		return "inital:" + initialLocation.toString();
	}
	
	public String toStringLocationVariables(){
		return locationVariables.toString();
	}
	
	public String toStringActionsInput(){
		return actionsInput.toString();
	}
	
	public String toStringActionVarsInput(){
		return actionVarsInput.toString();
	}
	
	public String toStringActionsOutput(){
		return actionsOutput.toString();
	}
	
	public String toStringActionVarsOutput(){
		return actionVarsOutput.toString();
	}

	/******************* HELPERS METHOS ***********************************/
	
	public List<Transition> getOutgoingTransitions(Location location) {
		List<Transition> outgoings = new ArrayList<Transition>();
		
		//JDA: Debug
		//System.out.println("JDA: FunctionalFiniteStateMachine.getOutgoingTransitions: name: [[" + name + "]].");
		//System.out.println("JDA: FunctionalFiniteStateMachine.getOutgoingTransitions: name: [[" + location + "]].");
		//System.out.println("JDA: FunctionalFiniteStateMachine.getOutgoingTransitions: transitions: [[" + transitions + "]].");
		for (Transition t : transitions){
			if(t.getSource().equals(location)){
				if (t.hasTimerTrigger()){
					outgoings.add(0,t);
				} else {
					outgoings.add(t);
				}
			}
		}
		
		return outgoings;
	}

	public LocationVariable<?> getLocationVariable(String name) {		
		return locationVarsMap.get(name);
	}

	public String getName() {		
		return name;
	}

	public Map<String, LocationVariable<?>> getLocationVarsMap() {		
		return locationVarsMap;
	}

	public List<String> getActionOutputNames() {		
		return new ArrayList<String>(actionsOutputMap.keySet());
	}

	public Set<ActionOutput> getActionsOutputForNames(String[] actionNames) {
		Set<ActionOutput> result = new HashSet<ActionOutput>();
		for(String name : actionNames){
			result.add(actionsOutputMap.get(name));
		}
		return result;
	}

	public Set<String> getLocationNames() {
		Set<String> result = new HashSet<String>();
		for(Location location : locations){
			result.add(location.getNameExplorable());
		}
		return result;
	}

	public Location getLocationByNameExplorable(String name) {		
		return locationsMap.get(name);
	}

	public Map<String, ActionOutput> getActionsOutputMap() {
		return actionsOutputMap;
	}

	public Map<String, Location> getLocationsMap() {
		return locationsMap;
	}

	public boolean containsLocation(Location location) {		
		return locations.contains(location);
	}

	public int getLocationSize() {
		return locations.size();
	}

	public Set<PassiveClass> getPassiveClasses() {		
		return new HashSet<PassiveClass>(passiveClasses.values());
	}
	
	public void addPassiveClasses(Set<PassiveClass> passiveClasses){
		for (PassiveClass oc : passiveClasses){
			this.passiveClasses.put(oc.getName(), oc);
		}		
	}

	public PassiveClass getPassiveClass(String name) {
		
		return passiveClasses.get(name);
	}

	public Location getLocationByName(String stateName) {
		for (Location l : locations){
			if (l.getName().equals(stateName)){
				return l;
			}
		}
		return null;
	}

	

	

	
}
