package symbolic.execution.ffsm.model;

import java.util.HashSet;
//import java.util.Set;

public class TimerActionInput extends ActionInput{
	//private ActionVariableInput<Integer> timerVarlue;
	public TimerActionInput(String name) {
		super("", name);
		vars = new HashSet<ActionVariableInput<?>>();
	}

	
	@Override
	public String toString() {		
		return getName() + ".timeout()";
	}
}
