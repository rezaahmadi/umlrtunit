package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;


public class GuardFunction extends Function{		
	private Set<ActionVariableInput<?>> inputVariables;
	private Set<LocationVariable<?>> locationVariables;
	
	private Set<Constraint> guardConstraints;
	
	private Solver solver;
	
	protected GuardFunction(String name){
		super(name);
	}
	
	public GuardFunction(String name, Set<ActionVariableInput<?>> inputVariables,
			Set<LocationVariable<?>> locationVariables){
		this(name);
		
		this.inputVariables = new HashSet<ActionVariableInput<?>>(inputVariables);
		this.locationVariables = new HashSet<LocationVariable<?>>(locationVariables);		
		
		guardConstraints = new HashSet<Constraint>();
		solver = SymbolicFactory.createSolver();
	}
	
	public void addConstraint (Constraint guardConstraint){
		guardConstraints.add(guardConstraint); 		
	}
	
	
	public List<Constraint> evaluateSymbolic(ValuationSymbolic locVars, 
			List<Constraint> inputVars, List<Constraint> pc){
		List<Constraint> result = new ArrayList<Constraint>();		
		
		//prepare list of constraints
		List<Constraint> constraints = new ArrayList<Constraint>(); 
		
		//add valuation
		constraints.addAll(locVars.createConstraints());
	
		//add path conditions
		constraints.addAll(pc);
		
		//add 'valuation' of inputs
		constraints.addAll(inputVars);
				
		boolean trueSolvable = solver.isSolvable(constraints, guardConstraints, true);
		boolean falseSolvable = solver.isSolvable(constraints, guardConstraints, false);
		
		if (trueSolvable){
			if (falseSolvable){
				result.addAll(guardConstraints);
			} else {
				result.add(SymbolicFactory.createTrue());
			}
		}else {
			result.add(SymbolicFactory.createFalse());
		}
		return result;
	}

	public Set<Constraint> getGuardConstraints() {		
		return guardConstraints;
	}
	
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Constraint cs  : guardConstraints){
			sb.append(cs.print());
			sb.append(" AND ");
		}
		return sb.substring(0,sb.length()-4);
	}

	//TODO: JDA: (was KZ): must implement concrete evaluation of guard func. ...
	//TODO: JDA: Check what the above means. Is this really still to be done?
	public Boolean evaluate(Valuation inputVariables2, Valuation valuation) {
		return null;
	}

	public List<Constraint> evaluateSymbolic(List<Constraint> pc, Map<SymbolicVariable, Expression> replacement) {
		List<Constraint> result = new ArrayList<Constraint>();		
		
		if (guardConstraints.size()==1){
			if (guardConstraints.iterator().next().equals(tt)){
				result.add(SymbolicFactory.createTrue());
				return result;
				
			}
		}
		
		//prepare list of constraints
		List<Constraint> constraints = new ArrayList<Constraint>(); 
		
		//add constraints from PC
		for (Constraint cst : pc){
			Constraint replaced = (Constraint) cst.replaceVariables(replacement);
			
			if (replaced.equals(ff)){
				result.add(SymbolicFactory.createFalse());
				return result;
			}
			if(!replaced.equals(tt))
				constraints.add(replaced);
		}
		
		//prepare constraints from guards
		Set<Constraint> guardCst = new HashSet<Constraint>();
		for (Constraint cst : guardConstraints){
			Constraint replaced = (Constraint) cst.replaceVariables(replacement);
			
			if (replaced.equals(ff)){
				result.add(SymbolicFactory.createFalse());
				return result;
			}
			if(!replaced.equals(tt))
				guardCst.add(replaced);
		}
		
		if (guardCst.isEmpty()){
			result.add(SymbolicFactory.createTrue());
			return result;
		} 
				
		boolean trueSolvable = solver.isSolvable(constraints, guardCst, true);
		boolean falseSolvable = solver.isSolvable(constraints, guardCst, false);
		
		if (trueSolvable){
			if (falseSolvable){
				result.addAll(guardCst);
			} else {
				result.add(SymbolicFactory.createTrue());
			}
		}else {
			result.add(SymbolicFactory.createFalse());
		}
		
		return result;
	}
	
	private static final False ff = SymbolicFactory.createFalse();
	private static final True tt = SymbolicFactory.createTrue();
	
}
