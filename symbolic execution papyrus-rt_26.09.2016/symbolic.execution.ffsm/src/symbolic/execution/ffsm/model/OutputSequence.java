package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.List;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;

public class OutputSequence {
	protected List<ActionOutputInstance> sequence;
	
	public OutputSequence(){
		sequence = new ArrayList<ActionOutputInstance>();
	}

	public OutputSequence(List<ActionOutputInstance> sequence) {
		this.sequence = new ArrayList<ActionOutputInstance>(sequence);
	}

	public List<Constraint> createConstraints() {
		List<Constraint> constraints = new ArrayList<Constraint>();
		for (ActionOutputInstance item : sequence){
			for (ActionVariableOutput<?> outputVar: item.getVars()){
				constraints.add(SymbolicFactory.createRelationalConstraint(outputVar.getSymbolicVar(),
						CompareOperators.eq, item.getVariableMapping(outputVar)));
			}
		}
		return constraints;
	}

	public  List<ActionOutputInstance> getSequence() {
		return sequence;
	}
	
	public void addItem(ActionOutputInstance actionInstance){
		sequence.add(actionInstance);
	}
	
	@Override
	public String toString() {		
		return sequence.toString();
	}
}
