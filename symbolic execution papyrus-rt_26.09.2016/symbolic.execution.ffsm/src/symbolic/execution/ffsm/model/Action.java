package symbolic.execution.ffsm.model;

public abstract class Action {
	protected String name;	
	
	protected String portName;
	
	protected String signalName;
	
	public Action(String portName, String signalName){
		this.portName = portName;
		this.signalName = signalName;
		
		if (portName==null || portName.isEmpty())
			this.name=signalName;
		else if (signalName == null || signalName.isEmpty())
			this.name = portName;
		else 
			this.name = portName + "." + signalName; 
	
	}
	
	public String getPortName(){
		return portName;
	}
	
	public String getSignalName() {
		return signalName;
	}

}
