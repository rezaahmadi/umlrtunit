package symbolic.execution.ffsm.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;



public class Assignments {
	
	private Map<LocationVariable<?>, Expression> assign;
	
	public Assignments(Set<LocationVariable<?>> locationVariables){
		assign = new HashMap<LocationVariable<?>, Expression>();
		
		for (LocationVariable<?> locationVariable : locationVariables){
			assign.put(locationVariable,locationVariable.getSymbolicVar());
		}
		
	}
	
	public void assign(LocationVariable<?> locVariable, Expression expr){
		assign.put(locVariable, expr);
	}


	public List<Constraint> createConstraints(){
		List<Constraint> csts = new ArrayList<Constraint>();
				
		for (LocationVariable<?> var : assign.keySet()){			
			csts.add(SymbolicFactory.createRelationalConstraint
					(var.getSymbolicVar(), CompareOperators.eq, assign.get(var))); 			
		}
		
		return csts;
	}

	public Set<LocationVariable<?>> getLocationVariables() {
		return assign.keySet();
	}

	public Expression getValue(LocationVariable<?> locVar) {
		return assign.get(locVar);
	}
	
	
	@Override
	public String toString() {		
		StringBuffer sb = new StringBuffer();
		for (LocationVariable<?> locVar : assign.keySet()){
			sb.append(locVar.getName());
			sb.append(":=");
			sb.append(assign.get(locVar).print());
			sb.append(",");
		}
		return sb.toString();
	}
}
