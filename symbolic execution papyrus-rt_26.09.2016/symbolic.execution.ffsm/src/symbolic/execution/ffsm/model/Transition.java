package symbolic.execution.ffsm.model;


public class Transition {
	
	protected String name;
	
	protected Location source;
	
	protected Location target;
	
	protected ActionInput inputAction;
	
	//protected ActionInput inputActionForTransition;
	
	protected GuardFunction gf;
	
	protected UpdateFunction uf;
	
	protected OutputFunction of;
	
	//private Map<choco.kernel.model.variables.Variable, choco.kernel.model.variables.Variable> mappingInputVars;
	
	//private List<Constraint> inputVarsConstraints;

	public Transition(String name){
		//mappingInputVars = new HashMap<Variable, Variable>();
		//inputVarsConstraints = new ArrayList<Constraint>();
		this.name = name;
	}
	
	public Transition(String name, Location source, Location target,
			ActionInput inputAction, GuardFunction gf, UpdateFunction uf,
			OutputFunction of) {
		this(name);
		this.source = source;
		this.target = target;
		this.inputAction = inputAction;
		//this.inputActionForTransition = inputAction.createExecution();
		
		//mappingInputVars = FunctionApplication.createMapping(inputAction, inputActionForTransition);
		//inputVarsConstraints = createConstraints(mappingInputVars);
		
		this.gf = gf;
		this.uf = uf;
		this.of = of;
	}

	public String getCode() {
		return new String("");
	}
	
	public Location getSource() {
		return source;
	}

	public Location getTarget() {
		return target;
	}

	public ActionInput getInputAction() {
		return inputAction;
	}

	public GuardFunction getGf() {
		return gf;
	}

	public UpdateFunction getUf() {
		return uf;
	}

	public OutputFunction getOf() {
		return of;
	}
	
	@Override
	public String toString() {		
		return "(" + source + "," + inputAction + "," + gf + "," + uf + "," + of +"," + target + ")";
	}

	public void setGf(GuardFunction gf) {
		this.gf = gf;		
	}
	
	public void setUf(UpdateFunction uf) {
		this.uf =uf;		
	}
	
	public void setOf(OutputFunction of) {
		this.of = of;		
	}

	public void setInputAction(ActionInput inputAction) {
		this.inputAction = inputAction;
		
	}

	public boolean hasTimerTrigger() {
		if (inputAction instanceof TimerActionInput){
			return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this==obj){
			return true;
		}
		if (obj instanceof Transition){
			Transition other = (Transition) obj;
			return name.equals(other.name) &&
				    source.equals(other.source) &&
				    target.equals(other.target);
		}
		return false;
	}

	public String getName() {
		return name;
	}
	
}
