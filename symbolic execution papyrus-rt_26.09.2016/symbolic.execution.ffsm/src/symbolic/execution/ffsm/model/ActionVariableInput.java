package symbolic.execution.ffsm.model;

//import java.util.Collection;

import symbolic.execution.constraints.manager.PassiveClass;
//import symbolic.execution.constraints.manager.SymbolicVariable;

public class ActionVariableInput<T> extends ActionVariable<T> {

	public ActionVariableInput(String name, Class<T> type) {
		super(name, type);		
	}

	public ActionVariableInput(String name, Class<T> type,
			PassiveClass pc) {
		super(name,type,pc);
	}

	
	

}
