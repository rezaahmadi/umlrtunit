package symbolic.execution.ffsm.model;

import java.util.HashSet;
import java.util.Set;

public class ActionOutput extends Action{
	protected Set<ActionVariableOutput<?>> vars;

	public ActionOutput(String portName, String signalName, Set<ActionVariableOutput<?>> vars) {
		super(portName, signalName);
		this.vars = vars;
	}
	
	public ActionOutput(String portName, String signalName, ActionVariableOutput<?>... variables){
		super(portName, signalName);
		
		vars = new HashSet<ActionVariableOutput<?>>();
		for (ActionVariableOutput<?> var : variables){
			vars.add(var);
		}
	}
	
	public ActionOutput(ActionOutput action) {
		super(action.portName, action.signalName);		
		this.vars = action.vars;
	}

	public Set<ActionVariableOutput<?>> getVars(){
		return new HashSet<ActionVariableOutput<?>>(vars);
	}
	
	@Override
	public String toString() {		
		return name;
	}

	public String getName() {		
		return name;
	}
	
	//JDA: alternative equals
	public boolean eq(Object o) {
		if (o instanceof ActionOutput){
			return ((ActionOutput)o).getName().equals(name);
		}
		return false;
	}
	@Override
	public boolean equals(Object o) {
		//if (o instanceof ActionOutput){
		//	return ((ActionOutput)o).getName().equals(name);
		//}
		//return false;
		return super.equals(o);
	}
	@Override
	public int hashCode() {		
		return name.hashCode();
	}


	
}
