package symbolic.execution.ffsm.model;

public abstract class Function {
	private String name;
	
	public Function(String name){
		this.name = name;
	}

	@Override
	public String toString() {		
		return name;
	}
}
