package symbolic.execution.ffsm.model;

import symbolic.execution.constraints.manager.PassiveClass;

public abstract class ActionVariable<T> extends ModelVariable<T>{
	private Action action;
	
	public ActionVariable(String name, Class<T> type){
		super(name,type);		
	}
	
	public ActionVariable(ActionVariable<T> variable, String suffix){
		super(variable, suffix);
	}
	
	public ActionVariable(String name, Class<T> type, PassiveClass pc) {
		super(name,type,pc);
	}

	public void setAction(Action action){
		this.action = action;
	}

}
