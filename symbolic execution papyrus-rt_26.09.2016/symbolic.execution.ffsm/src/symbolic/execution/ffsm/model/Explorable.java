package symbolic.execution.ffsm.model;

public abstract class Explorable {
	protected Location location;
	
	public void setLocation(Location location){
		this.location = location;
	}
	
	public abstract void explore(FunctionalFiniteStateMachine ffsm);
	
	public abstract boolean isExplored();
	
	public abstract String getName();

}
