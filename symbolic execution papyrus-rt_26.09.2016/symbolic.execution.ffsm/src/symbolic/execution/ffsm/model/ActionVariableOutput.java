package symbolic.execution.ffsm.model;

import symbolic.execution.constraints.manager.PassiveClass;

public class ActionVariableOutput<T> extends ActionVariable<T> {

	public ActionVariableOutput(String name, Class<T> type) {
		super(name, type);
	}
	
	/*public ActionVariableOutput(ActionVariableOutput<T> variable, String suffix){
		super(variable, suffix);
	}*/

	public ActionVariableOutput(String name, Class<T> type,
			PassiveClass pc) {
		super(name,type,pc);
	}

}
