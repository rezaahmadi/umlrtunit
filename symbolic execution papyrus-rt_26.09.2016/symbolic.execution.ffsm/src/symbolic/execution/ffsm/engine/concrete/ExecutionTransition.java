package symbolic.execution.ffsm.engine.concrete;

import symbolic.execution.ffsm.model.Transition;
//import symbolic.execution.ffsm.model.Valuation;

public class ExecutionTransition {
	
	private Transition transition;
	private InputActionExecution inputAction;
	private OutputSequenceExecution outputSequence;
	private ExecutionState source, target;


	public ExecutionTransition(Transition t, InputActionExecution inputAction) {
		this.transition = t;
		this.inputAction = inputAction;
	}


	public ExecutionTransition(Transition t, InputActionExecution inputConcrete, 
			OutputSequenceExecution outputConcrete) {
		this(t,inputConcrete);
		outputSequence = outputConcrete;
	}


	public Transition getTransition() {		
		return transition;
	}


	public InputActionExecution getInputAction() {		
		return inputAction;
	}


	public void setOutputSequence(OutputSequenceExecution sequence) {
		this.outputSequence = sequence;		
	}
	
	public OutputSequenceExecution getOutputSequence() {
		return this.outputSequence ;		
	}
	
	@Override
	public String toString() {		
		return inputAction.getInputAction().getName() + "(" + inputAction.getValuation()+ ") " + outputSequence;
	}


	public void setSource(ExecutionState source) {
		this.source = source;
		
	}


	public void setTarget(ExecutionState target) {
		this.target = target;
		
	}


	public ExecutionState getSource() {		
		return source;
	}


	public ExecutionState getTarget() {
		return target;
	}
	

}
