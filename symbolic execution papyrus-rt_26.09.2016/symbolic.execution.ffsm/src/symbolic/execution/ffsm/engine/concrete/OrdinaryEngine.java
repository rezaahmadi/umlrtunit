package symbolic.execution.ffsm.engine.concrete;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

//import symbolic.execution.ffsm.model.Action;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.Transition;
import symbolic.execution.ffsm.model.Valuation;

public class OrdinaryEngine {
	FunctionalFiniteStateMachine ffsm;
	
	List<InputActionExecution> inputTrace = new ArrayList<InputActionExecution>();
	List<ExecutionState> states = new ArrayList<ExecutionState>();
	List<OutputSequenceExecution> outputTrace = new ArrayList<OutputSequenceExecution>();	
	
	
	
	public OrdinaryEngine(FunctionalFiniteStateMachine ffsm){
		this.ffsm = ffsm;
	}
	
	public void execute(int numberOfSteps){		
		//create initial execution state
		ExecutionState initialState = 
			new ExecutionState(ffsm.getInitialLocation(), ffsm.getInitialValuation());
						
		
		states.add(initialState);
		ExecutionState currentState = initialState;
		//execute 1 step
		for (int i = 0; i < numberOfSteps; i++){
						
			//generate random action
			InputActionExecution input = generateRandomInputAction();
			
			inputTrace.add(input);
			
			//find enabled transitions
			List<ExecutionTransition> enabledTransitions = new ArrayList<ExecutionTransition>();
			
			
			for (Transition t :ffsm.getOutgoingTransitions(currentState.getLocation())){
				
				
				//check action
				if (input.getInputAction().equals(t.getInputAction())){
					
					
					Boolean guard = t.getGf().evaluate(input.getInputVariables(), currentState.getValuation());					
					if (guard){
						enabledTransitions.add(new ExecutionTransition(t, input));
					}
				}
				
				//evaluate
				
			}
			
			
			
			if(enabledTransitions.isEmpty()){
				
				
			} else {
			
				//choose randomly
				int transitionNumber = (new Random()).nextInt(enabledTransitions.size()); 
			
				ExecutionTransition toFire = enabledTransitions.get(transitionNumber);
				
				//fire the transition - get update valuation and output sequence
				Valuation newLocationVars = toFire.getTransition().getUf().evaluate(toFire.getInputAction().getInputVariables(), currentState.getValuation());
				OutputSequenceExecution oSequence = toFire.getTransition().getOf().evaluate(toFire.getInputAction().getInputVariables(), currentState.getValuation());
				if (oSequence != null && !oSequence.isEmpty()){
					outputTrace.add(oSequence);
				}
					
				//store state
				toFire.setOutputSequence(oSequence);
			
				
				ExecutionState state = new ExecutionState(toFire.getTransition().getTarget(), newLocationVars);
				
				
				states.add(state);
				currentState = state;
			}
		}
		
		/*System.out.println("****************************************");
		System.out.println("EXECUTION report");
		
		System.out.println("INPUT trace :");
		System.out.println(inputTrace);
		
		System.out.println();
		
		System.out.println("OUTPUT trace :");
		System.out.println(outputTrace);
		
		System.out.println();
		
		System.out.println("STATES :");
		System.out.println(states);*/
		
	}

	private InputActionExecution generateRandomInputAction() {
		Set<ActionInput> inputs = ffsm.getActionsInput();		
		int number = new Random().nextInt(inputs.size());	
		Iterator<ActionInput> iter = inputs.iterator();
		for (int i =0; i < number; i++){
			iter.next();
		}
		
		ActionInput input = iter.next();
		Valuation inputVariables = createRandomValuation(input.getVars());
		
		return new InputActionExecution(input, inputVariables);
		
	}

	private Valuation createRandomValuation(Set<ActionVariableInput<?>> vars) {
		Valuation valuation = new Valuation();
		for (ActionVariableInput<?> var : vars){			
			if (var.getType().equals(Integer.class)){
				Integer n = new Random().nextInt(2000) - 1000;				
				ActionVariableInput<Integer> varInt = (ActionVariableInput<Integer>) var;
				valuation.setValue(varInt, n);
			}
			
		}
		return valuation;
	}


}
