package symbolic.execution.ffsm.engine.concrete;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.Valuation;

public class InputActionExecution {
	private ActionInput inputAction;
	private Valuation inputVariables;
	
	public InputActionExecution(ActionInput inputAction, Valuation val){
		this.inputAction = inputAction;
		this.inputVariables = val;
	}

	public ActionInput getInputAction() {
		return inputAction;
	}

	public Valuation getInputVariables() {
		return inputVariables;
	}
	
		
	@Override
	public String toString() {		
		return inputAction.toString() + inputVariables.toString() ;
	}

	public Valuation getValuation() {		
		return inputVariables;
	}
}
