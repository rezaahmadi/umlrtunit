package symbolic.execution.ffsm.engine.concrete;

import java.util.ArrayList;
import java.util.List;
//import java.util.Map;

//import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.Valuation;
import symbolic.execution.utils.Pair;

public class OutputSequenceExecution {
	private List<Pair<ActionOutputInstance, Valuation>> seq;
	
	public OutputSequenceExecution(){
		seq = new ArrayList<Pair<ActionOutputInstance,Valuation>>();
	}
	
	public void addOutput(Pair<ActionOutputInstance, Valuation> sequence){
		seq.add(sequence);
	}
	
	public boolean isEmpty(){
		return seq == null || seq.isEmpty();
	}
	
	@Override
	public String toString() {		
		if (seq.isEmpty()){
			return "[]";
		}
		StringBuffer sb = new StringBuffer();
		
		sb.append("[");
		for (Pair<ActionOutputInstance, Valuation> item :seq){
			sb.append(item.getFirst().toString());
			sb.append("(");
			sb.append(item.getSecond().toString());
			sb.append(")");
			sb.append(",");
		}
		
		return sb.substring(0, sb.length()-1) + "]";
	}

	
	
	
}
