package symbolic.execution.ffsm.engine.concrete;

import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.Valuation;

public class ExecutionState {
	private Location location;
	private Valuation valuation;
	
	public ExecutionState(Location location, Valuation valuation){
		this.location = location;
		this.valuation = valuation;
	}

	public Location getLocation() {
		return location;
	}

	public Valuation getValuation() {
		return valuation;
	}
	
	@Override
	public String toString() {
		return location.getName() + valuation.toString();
	}
	
	

}
