package symbolic.execution.ffsm.engine.concrete;

import java.util.ArrayList;
import java.util.List;
//import java.util.Map;

import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;

public class ConcretePath extends SymbolicPath{
	
	private List<ExecutionTransition> transitions;
	
	public List<ExecutionTransition> getTransitions(){
		return transitions;
	}
	
	ExecutionState singleState;
		
	
	public ConcretePath(SymbolicPath symbolicPath){
		arcs = symbolicPath.getArcs();
		singleNode = symbolicPath.getSingleNode();
		
		transitions = new ArrayList<ExecutionTransition>();
	}

	public void addTransition(ExecutionTransition trans) {
		transitions.add(trans);
		
	}

	public void setSingleState(ExecutionState singleState) {
		this.singleState = singleState;
		
	}
	
	
	
	
	@Override
	public String toString() {
		if (transitions.isEmpty()){
			if (singleState != null){
				return singleState.toString();
			} else {
				return "";
			}
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(transitions.get(0).getSource().toString());
		
		for (ExecutionTransition trans : transitions){
			sb.append("  <");
			sb.append(trans.toString());
			sb.append(">  ");
			sb.append(trans.getTarget().toString());
		}
		
		
		return sb.toString();
	}
	
	public String toString2() {
		if (transitions.isEmpty()){
			if (singleState != null){
				return singleState.toString();
			} else {
				return "";
			}
		}
		
		StringBuffer sb = new StringBuffer();
		//sb.append(transitions.get(0).getSource().toString());
		
		for (ExecutionTransition trans : transitions){
			sb.append("  <");
			sb.append(trans.toString());
			sb.append(">  ");
			sb.append(trans.getTarget().getLocation().getName());
		}
		
		
		return sb.toString();
	}

	public String getTraceString() {
		if (transitions.isEmpty()){
			if (singleState != null){
				return singleState.toString();
			} else {
				return "";
			}
		}
		
		StringBuffer sb = new StringBuffer();		
		
		for (ExecutionTransition trans : transitions){
			sb.append("  <");
			sb.append(trans.toString());
			sb.append(">  ");			
		}
		
		
		return sb.toString();
	}

}
