package symbolic.execution.ffsm.engine.tree;

//import java.util.List;

//import javax.xml.transform.Source;

//import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;

public class Arc<T,S> {
	private T contents;
	
	private TreeNode<S,T> source;
	private TreeNode<S,T> target;
	
	public Arc(T contents, TreeNode<S,T> source, TreeNode<S,T> target) {
		super();
		this.contents = contents;
		this.source = source;
		this.target = target;
	}

	public T getContents(){
		return contents;
	}
	
	public TreeNode<S,T> getSource(){
		return source;
	}
	
	public TreeNode<S,T> getTarget(){
		return target;
	}
	
	@Override
	public String toString() {	
		
		return contents==null ? "null " : contents.toString();
	}
	
	public String print(int indent) {
		StringBuffer sb = new StringBuffer();
		
		for (int i=0; i<indent; i++){
			sb.append("\t");
		}
		sb.append('-');
		sb.append(contents.toString());
		sb.append("->");		
		
		sb.append("\n");
		sb.append(target.print(indent + 1));
		
		
		return sb.toString();
	}
}
