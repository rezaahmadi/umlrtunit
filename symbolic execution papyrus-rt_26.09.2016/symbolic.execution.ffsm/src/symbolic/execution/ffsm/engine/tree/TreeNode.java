package symbolic.execution.ffsm.engine.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T,S> {
	private T contents;
	
	private List<Arc<S,T>> children;
	
	public TreeNode(){
		this.children = new ArrayList<Arc<S,T>>();		
	}
	
	public TreeNode(T contents, List<Arc<S,T>> children ) {
		this.contents = contents;
		this.children = children;
	}
	
	public TreeNode(T contents){
		this.children = new ArrayList<Arc<S,T>>();
		this.contents = contents;
	}
			
	
	public void addChild(Arc<S,T> child){
		children.add(child);
	}
	
	public T getContents(){
		return contents;
	}
	
	
	public List<Arc<S,T>> getChildren(){
		return children;
	}
	
	public String print(int indent){
		StringBuffer sb = new StringBuffer();
		
		for (int i=0; i<indent; i++){
			sb.append("\t");
		}
		
		sb.append(contents.toString());		
		sb.append("\n");
		for (Arc<S,T> child : children){
			sb.append(child.print(indent + 1));
			
		}
				
		
		return sb.toString();
	}
	
	@Override
	public String toString() {	
		//JDA: Changed.
		return "[contents:  " + java.lang.System.identityHashCode(contents) + " [" + contents.toString() + "], " +
				"children: "  + java.lang.System.identityHashCode(children) + " [" + children.toString() + "]]";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof TreeNode){
			TreeNode<T,S> t = (TreeNode<T,S>) o;			
			return contents.equals(t.contents);
		}
		return false;
	}
	
	@Override
	public int hashCode() {		
		return contents.hashCode();
	}

	public void addChild(TreeNode<T,S> target, S contents) {
		Arc<S,T> arc = new Arc<S,T>(contents, this, target);
		children.add(arc);
	}
	
	public void removeArc(Arc<S,T> arc){
		if (children.contains(arc)){
			children.remove(arc);
		}
	}
}
