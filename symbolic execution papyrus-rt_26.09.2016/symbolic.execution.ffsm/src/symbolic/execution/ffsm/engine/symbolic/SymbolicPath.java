package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import java.util.concurrent.SynchronousQueue;


import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.concrete.ConcretePath;
import symbolic.execution.ffsm.engine.concrete.ExecutionState;
import symbolic.execution.ffsm.engine.concrete.ExecutionTransition;
//import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.engine.tree.Arc;

public class SymbolicPath{
	protected List<ArcSymbolic> arcs;
	
	protected NodeSymbolic singleNode;
	
	private boolean isStarted;
	
	private Set<ActionInputSymbolic> inputs;
	
	private Map<SymbolicVariable, Constant> solution;
	
	private boolean isExtended;
	
	private static SolvedPCs solved = new SolvedPCs();
	
	public SymbolicPath(){
		arcs = new ArrayList<ArcSymbolic>();
		inputs = new HashSet<ActionInputSymbolic>();
	}
	
	public SymbolicPath(SymbolicPath other) {
		arcs = new ArrayList<ArcSymbolic>(other.arcs);
		singleNode = other.singleNode;
		isStarted = other.isStarted;
		inputs = new HashSet<ActionInputSymbolic>(other.inputs);
	}

	public void addArc(ArcSymbolic arc){
		arcs.add(arc);
	}

	public void removeLastArc() {
		if (!arcs.isEmpty()){
			arcs.remove(arcs.size()-1);
		}
		
	}

	public void setSingleNode(NodeSymbolic singleNode) {
		this.singleNode = singleNode;		
	}
	
	@Override
	public String toString() {	
		if (arcs.size() == 0){
			if (singleNode!=null){
				return singleNode.getContents().toString();
			} else {
				return "[]";
			}
		}
		StringBuffer sb = new StringBuffer();
		if (arcs.get(0).getSource() != null){
			sb.append(arcs.get(0).getSource());
		}
		for (ArcSymbolic arc : arcs){
			
			sb.append("  <" + arc.toString() + ">  ");
			if (arc.getTarget()!=null)
				sb.append(arc.getTarget());
		}
		return sb.toString();
	}

	public SymbolicState getLastState() {
		if (arcs.isEmpty()){
			return singleNode.getContents();
		}
		return arcs.get(arcs.size()-1).getTarget().getContents();
	}

	public String getPathString() {		
		return toString();
	}

	public void clear() {
		arcs.clear();		
	}

	public void setStarted(boolean b) {
		isStarted = b;		
	}

	public boolean isStarted() {		
		return isStarted;
	}

	public List<ArcSymbolic> getPath() {
		return arcs;
	}
	
	public void setPath(List<ArcSymbolic> arcs){
		this.arcs = arcs;
	}

	public boolean isEmptyPath() {		
		return arcs.isEmpty();
	}
	
	public ConcretePath solve() {
		ConcretePath result = new ConcretePath(this);

		// solve constraints from the last state
		NodeSymbolic lastNode = arcs.get(arcs.size() - 1).getTarget();

		List<Constraint> pcLast = new ArrayList<Constraint>();
		for (SymbolicState state : lastNode.getContentsAll()) {
			pcLast.addAll(state.getPC());
		}
		List<SymbolicVariable> inputVars = collectVars();

		Map<SymbolicVariable, Constant> solution = null;

		if (inputVars.isEmpty()) {
			solution = new HashMap<SymbolicVariable, Constant>();
		} else {
			solution = solved.findSolution(pcLast, inputVars);

			if (solution == null) {
				solution = SymbolicFactory.createSolver().solve(pcLast, inputVars);
				solved.addSolved(pcLast, inputVars, solution);
			}
		}

		result.setSolution(solution);
		// System.out.println("Solved " + solution);

		// concrete first state
		ExecutionState source = arcs.isEmpty() ? singleNode.getContents().createConcrete(solution)
				: arcs.get(0).getSource().getContents().createConcrete(solution);

		result.setSingleState(source);
		// build path
		try {
		for (ArcSymbolic arc : arcs) {
			
			ExecutionTransition trans = arc.getContents().createConcrete(solution);
			ExecutionState target = arc.getTarget().getContents().createConcrete(solution);

			trans.setSource(source);
			trans.setTarget(target);

			result.addTransition(trans);

			source = target;
		}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	private List<SymbolicVariable> collectVars() {
		List<SymbolicVariable> inputVars = new ArrayList<SymbolicVariable>();
		
		for (ActionInputSymbolic input : inputs){
			for (Expression expr : input.getMappingSymbolic().values()){
				if (expr instanceof SymbolicVariable){
					inputVars.add((SymbolicVariable)expr);
				}
			}
		}			
		return inputVars;
	}

	public void addInputAction(ActionInputSymbolic inputExecution) {
		inputs.add(inputExecution);
				
	}

	public Set<ActionInputSymbolic> getInputs() {
		return inputs;
	}

	public void setInputs(Set<ActionInputSymbolic> inputs) {
		this.inputs = inputs;
	}

	public String getTraceString() {		
		if (arcs.size() == 0){
			if (singleNode!=null){
				return singleNode.getContents().toString();
			} else {
				return "[]";
			}
		}
		StringBuffer sb = new StringBuffer();
		for (ArcSymbolic arc : arcs){
			sb.append("  <" + arc.getContents().toString() + ">  ");			
		}
		return sb.toString();
	}

	public List<ArcSymbolic> getArcs() {
		return arcs;
	}

	public NodeSymbolic getFirstState() {
		if(arcs.isEmpty()){
			return singleNode;
		}
		return (NodeSymbolic) arcs.get(0).getSource();
	}

	public NodeSymbolic getSingleNode() {
		return singleNode;
	}
	
	public String getSolutionString(){
		if (solution==null || solution.isEmpty()){
			return null;
		}
		StringBuffer sb = new StringBuffer();
		sb.append("Solution:\n");
		for (SymbolicVariable var : solution.keySet()){
			sb.append(var.print());
			sb.append("=");
			sb.append(solution.get(var).print());
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public void setSolution(Map<SymbolicVariable, Constant> solution) {
		this.solution = solution;
	}

	public void removeInputAction(ActionInputSymbolic inputExecution) {
		inputs.remove(inputExecution);
		
	}
	
	public boolean isExtended(){
		return isExtended;
	}
	
	public void setIsExtended(boolean isExtended){
		this.isExtended = isExtended;
	}

	public NodeSymbolic getLastNode() {
		if (arcs.isEmpty()){
			return singleNode;			
		} else {
			return (NodeSymbolic) arcs.get(arcs.size()-1).getTarget();
		}
		
	}
}
