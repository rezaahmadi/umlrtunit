package symbolic.execution.ffsm.engine.symbolic;

//import java.util.ArrayList;
//import java.util.List;

//import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.ffsm.model.Location;
//import symbolic.execution.ffsm.model.TimerActionOutput;

//JDA: Added to represent a state whose behaviour is unknown
//JDA: because of the cap on the depth of SE.
public class SymbolicStateUnknown extends SymbolicState {

	public SymbolicStateUnknown(SymbolicState treeState) {
		super(treeState);
		location = new Location("Unknown");
	}

}
