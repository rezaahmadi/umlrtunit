package symbolic.execution.ffsm.engine.symbolic;


import java.util.Map;
import java.util.Set;

//import javax.sound.midi.Sequence;

import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.engine.concrete.OutputSequenceExecution;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.OutputSequence;
import symbolic.execution.ffsm.model.Valuation;
import symbolic.execution.utils.Pair;


public class OutputSequenceSymbolic extends OutputSequence{
	
	public OutputSequenceSymbolic(){
		super();
	}
	
	public OutputSequenceSymbolic(OutputSequence outSequence, Map<SymbolicVariable,Expression> replacement, Map<ActionOutput,ActionOutput> actionsMap){
		super();
		
		for (ActionOutputInstance item : outSequence.getSequence()){			
			//create symbolic valuation
			ActionOutputInstance symbolicItem = new ActionOutputInstance(item, actionsMap.get(item.getActionOutput()));
			
			//System.out.println("SEQUENCS " + item.getSecond());
			for (ActionVariableOutput<?> variable : symbolicItem.getVars()){
				//get expression
				Expression original = symbolicItem.getVariableMapping(variable); 				
				
				if (original != null){
				//replace output variable expression
				Expression replaced = original.replaceVariables(replacement);
				
				//store replaced
				symbolicItem.setVariableMapping(variable, replaced);
				}
			}
			sequence.add(symbolicItem);
		}
	}
	
	public OutputSequenceSymbolic(OutputSequenceSymbolic other,
			Map<ActionOutput, ActionOutput> actionMap) {
		for (ActionOutputInstance item : other.sequence){
			ActionOutput mapped = actionMap.get(item.getActionOutput());
			if (mapped == null){
				sequence.add(item);
			} else{
				sequence.add(new ActionOutputInstance(item, mapped));
			}
				
			
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (ActionOutputInstance item : sequence){
			sb.append(item.toString());
		}		
		return sb.length()>2 ? (sb.substring(0,sb.length())+"]") : (sb.toString()+"]");
	}

	
	
	public OutputSequenceExecution createConcrete(Map<SymbolicVariable, Constant> solution) {
		OutputSequenceExecution result = new OutputSequenceExecution();
		for (ActionOutputInstance item: sequence){
			Valuation val = new Valuation();
			//find values of vars
			for(ActionVariableOutput<?> var : item.getVars()){
				//find value
				Expression expr = item.getVariableMapping(var).replaceVariables(solution);
				
				//create valuation
				if (expr instanceof Constant){
					val.setValue(var, ((Constant)expr).getValue());
				}
			}
			result.addOutput(new Pair<ActionOutputInstance, Valuation>(item,val));
		}
		
		return result;
	}

	public boolean hasSignal(Set<ActionOutput> outputSignals) {		
		for (ActionOutputInstance item : sequence){			
			for (ActionOutput ao : outputSignals){
				if (ao.getName().equals(item.getActionOutput().getName())){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasAllSignalsInOrder(Set<ActionOutput> outputSignals) {
		boolean res = false;
		for (ActionOutput ao : outputSignals){
			res = false;
			for (ActionOutputInstance item : sequence){
				if (ao.getName().equals(item.getActionOutput().getName())){
					res = true;
				}
			}
			if (!res)
				return false;
		}
		return res;
	}

	public boolean isEmpty() {
		return sequence==null || sequence.isEmpty();
	}

	public void addItems(OutputSequenceSymbolic other) {
		sequence.addAll(other.sequence);
		
	
	}

}
