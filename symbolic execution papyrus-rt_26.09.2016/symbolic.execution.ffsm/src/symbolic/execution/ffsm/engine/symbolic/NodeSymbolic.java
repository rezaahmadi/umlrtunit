package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.engine.tree.TreeNode;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.Location;


//JDA: modified this during "sync-fix" branch.
public class NodeSymbolic extends TreeNode<SymbolicState, SymbolicTransition>{
	
	protected NodeSymbolic subsumedBy;
	
	protected NodeSymbolic subsumedByCopy; 
	
	protected List<NodeSymbolic> subsumedNodes = new ArrayList<NodeSymbolic>();;
	
	protected List<NodeSymbolic> subsumedNodesCopy = new ArrayList<NodeSymbolic>();
	
	protected boolean isVisited = false;
	
	//JDA: Begin fast and dirty depth control.
	private int myDepth = 0;
	
	public void setDepth(int d) {
		myDepth = d;
	}
	public int getDepth() {
		return(myDepth);
	}
	//JDA: End fast and dirty depth control.
	
	//private SymbolicExecutionTree ownerTree;
	
	//JDA: added during Sync-fix branch 
	public NodeSymbolic(SymbolicState contents) {
		super(contents);
		System.out.print("");
		//this.ownerTree = ownerTree;
	}
	
	
	public NodeSymbolic(NodeSymbolic other, SymbolicExecutionTree tree, boolean deep, Map<ActionOutput, ActionOutput> outputsMap, Map<ActionInput, ActionInput> inputsMap) {	
		//System.out.println("Copying " + other.getFirstContents().getLocation().getName());
		//copy contents 
		
		super(new SymbolicState(other.getContents(), tree));
		
		//if subsumes inform old subsumed		
		for(NodeSymbolic sub : other.subsumedNodes){
			sub.subsumedByCopy = this;
		}
		
		//if subsumes inform new subsumes
		for (NodeSymbolic sub : other.subsumedNodesCopy){
			sub.subsumedBy = this;
		}
		
		subsumedNodes.addAll(other.subsumedNodesCopy);
		
		//if is subsumed
		if (other.subsumedBy != null){
			//subsuming already processed 
			if (other.subsumedByCopy != null){
				subsumedBy = other.subsumedByCopy;
				subsumedBy.addSubsumed(this);
			} else {
			//subsuming not processed yet
				other.subsumedBy.subsumedNodesCopy.add(this);
			}
		}
				
		
		//copy arcs
		if (deep){
			for(Arc<SymbolicTransition,SymbolicState> arc : other.getChildren()){
				SymbolicTransition t = new SymbolicTransition(arc.getContents(), tree, outputsMap, inputsMap);
				addChild( new NodeSymbolic((NodeSymbolic) arc.getTarget(), tree, deep,outputsMap, inputsMap), t);
			}
		}
		
		if(subsumedNodes.contains(subsumedBy)){
			System.out.println();
		}
	}
	
	public NodeSymbolic(NodeSymbolic other, SymbolicExecutionTree tree, boolean deep) {	
		//System.out.println("Copying " + other.getFirstContents().getLocation().getName());
		//copy contents 
		super(new SymbolicState(other.getContents(), tree));
		//System.out.println("JDA: NodeSymbolic(" +
		//		"\n   other =[" + other.getFullNodeSymbolicAsString() + "]" +
		//		"\n   deep = " + deep +
		//		"\n   tree = [" + tree.toString() + "]).");
		
		//inform subsumed		
		for(NodeSymbolic sub : other.subsumedNodes){
			sub.subsumedByCopy = this;
		}
		subsumedNodes = new ArrayList<NodeSymbolic>();
		
		
		//copy arcs
		if (deep){
			for(Arc<SymbolicTransition,SymbolicState> arc : other.getChildren()){
				SymbolicTransition t = new SymbolicTransition(arc.getContents(), tree);
				addChild( new NodeSymbolic((NodeSymbolic) arc.getTarget(), tree, deep), t);
			}
		}
		
		//set subsumedBy and subsumed
		subsumedBy = other.subsumedByCopy;
		if(subsumedBy != null)
			subsumedBy.addSubsumed(this);
		//System.out.println("JDA: NodeSymbolic: result =[" + this.getFullNodeSymbolicAsString() + "].");
	}
	
	//JDA: begin addition
	public String printChildren(List<Arc<SymbolicTransition, SymbolicState>> children) {
		String result = new String ("[size: " + children.size() + " ");
		for (Arc<SymbolicTransition, SymbolicState> child: children) {
			result += "["  + java.lang.System.identityHashCode(child)+ " " + child.toString() + "]";
		}
		result += "]";
		return result;
	}
	//JDA: end addition
	
	//JDA: Modified the following by adding the 'parent' parameter which is used
	//     to identify which path constraints and valuations are required for subtates of 'parent'.
	public String getFullNodeSymbolicAsString(Object master) {
		return(
			"[NodeSymbolic: " + java.lang.System.identityHashCode(this) + " class name is: " + this.getClass().getName() + "\n" +
			"   contents are " + java.lang.System.identityHashCode(getContents()) + " [" + this.getContents().toString(master) + "]\n" +
			"   children (outgoing arcs) are " + java.lang.System.identityHashCode(this.getChildren()) + " [" + printChildren(this.getChildren()) + "]\n" +
			"   isVisited is [" + isVisited + "]\n" +
			"   subsumedBy is [" + (subsumedBy == null ? "<null>" : subsumedBy.toString()) + "]\n" +
			"   subsumedByCopy is [" + (subsumedByCopy == null ? "<null>" : subsumedByCopy.toString()) + "]\n" +
			"   subsumedNodes are [" + subsumedNodes.toString() + "]\n" +
			"   subsumedNodesCopy is [" + subsumedNodesCopy.toString() + "]]\n");	
	}

	public SymbolicPath checkInvariant(SymbolicPath path, Constraint cst) {
		//System.out.println("Checking in state " + getContentsAll());
		
		//System.out.println("Current path " + path);
		//gather pc and valustions
		Map<SymbolicVariable, Expression> valuations = new HashMap<SymbolicVariable, Expression>();
		List<Constraint> overallPC = new ArrayList<Constraint>();
		for (SymbolicState state : getContentsAll()){
			valuations.putAll(state.getValuation().getSymbolicVariables());
			overallPC.addAll(state.getPC());
		}
		Constraint cstReplaced = (Constraint) cst.replaceVariables(valuations);
		
		//check whether false satisfiable
		boolean solved = SymbolicFactory.createSolver().isSolvable(overallPC, cstReplaced, false);
		
		//if cst satisfied moved to children
		if (solved){
			return path;
		} else {
			
			for (Arc<SymbolicTransition, SymbolicState> arc :  getChildren()){
				NodeSymbolic child = (NodeSymbolic) arc.getTarget();
				path.addArc((ArcSymbolic)arc);
				SymbolicPath childPath = child.checkInvariant(path, cst);
				if (childPath!=null){
					return childPath;
				}
				path.removeLastArc();
			}
			return null;
		}
		
	}

	public ArcSymbolic addChild(NodeSymbolic child, SymbolicTransition arcContents){
		ArcSymbolic arc = new ArcSymbolic(arcContents, this, child);
		addChild(arc);
		
		return arc;
	}



	public void exploreReachable(Map<SymbolicExecutionTree,Location> source, Map<SymbolicExecutionTree,Location> target,
			SymbolicPath current, List<SymbolicPath> paths){
		if (isVisited){
			return;
		}		
		
		boolean isSource = false;
		if (agree(source)){
			if (current.isExtended()){
				return;
			}
			
			current.clear();			
			current.setStarted(true);
			isSource = true;
		}
		if (current.isStarted() && agree(target)){
								
			if (!current.isEmptyPath()){
				paths.add(new SymbolicPath(current));
			}
			current.setStarted(false);
			current.clear();
			if(current.isExtended()){
				return;
			}
		}
				
		if (subsumedBy != null && current.isStarted() && !isSource){
			isVisited = true;
			boolean isExtended = current.isExtended();
			current.setIsExtended(true);
			current.addArc(new SubsumptionArc(null, this, subsumedBy));
			subsumedBy.exploreReachable(source, target, current, paths);	
			current.removeLastArc();
			current.setStarted(true);
			current.setIsExtended(isExtended);
			isVisited = false;
		} 
		
		for(Arc<SymbolicTransition, SymbolicState> arc : getChildren()){
			ArcSymbolic arcS = (ArcSymbolic) arc;
			current.addInputAction(arcS.getContents().getInputExecution());
			SymbolicPath currentLocal = new SymbolicPath(current);			
			if (current.isStarted()){
				current.addArc(arcS);
			}
			((NodeSymbolic)arcS.getTarget()).exploreReachable(source, target, current, paths);
			current.setStarted(currentLocal.isStarted());
			current.setPath(currentLocal.getPath());	
			current.removeInputAction(arcS.getContents().getInputExecution());
		}
		
		
	}

	private boolean agree(Map<SymbolicExecutionTree, Location> source) {		
		for(SymbolicState state : getContentsAll()){
			Location required = source.get(state.getTree());
			if(required==null){
				continue;
			}
			if (! required.equals(state.getLocation())){
				return false;
			}
		}
		return true;
	}

	public void exploreForSignals(Set<ActionOutput> outputSignals,
			SymbolicPath current, List<SymbolicPath> paths) {
		for (Arc<SymbolicTransition,SymbolicState> arc : getChildren()){
			ArcSymbolic arcS = (ArcSymbolic) arc;
			
			current.addArc(arcS);
			current.addInputAction(arcS.getContents().getInputExecution());
			if (arcS.getContents().getOuputSequence().hasSignal(outputSignals)){				
				paths.add(new SymbolicPath(current));
			}
			
			((NodeSymbolic)arc.getTarget()).exploreForSignals(outputSignals, current, paths);
			current.removeInputAction(arcS.getContents().getInputExecution());
			current.removeLastArc();
		}
		
	}
	
	public void exploreForSignalsInOrder(Set<ActionOutput> outputSignals,
			SymbolicPath current, List<SymbolicPath> paths) {
		for (Arc<SymbolicTransition,SymbolicState> arc : getChildren()){
			ArcSymbolic arcS = (ArcSymbolic) arc;
			
			current.addArc(arcS);
			current.addInputAction(arcS.getContents().getInputExecution());
			if (arcS.getContents().getOuputSequence().hasAllSignalsInOrder(outputSignals)){				
				paths.add(new SymbolicPath(current));
			}
			
			((NodeSymbolic)arc.getTarget()).exploreForSignalsInOrder(outputSignals, current, paths);
			current.removeInputAction(arcS.getContents().getInputExecution());
			current.removeLastArc();
		}
		
	}



	public void exploreAllPaths(SymbolicPath current, List<SymbolicPath> paths) {
		//if leaf than store
		if (getChildren().size() ==0){
			//if (!getContents().getLocation().getName().equals("HISTORY")){
				//System.out.println(current);
			//}
			paths.add(new SymbolicPath(current));
		}
		
		for (Arc<SymbolicTransition,SymbolicState> arc : getChildren()){
			ArcSymbolic arcS = (ArcSymbolic) arc;
			current.addInputAction(arcS.getContents().getInputExecution());
			
			current.addArc(arcS);			
			
			((NodeSymbolic)arc.getTarget()).exploreAllPaths(current,paths);
			current.removeInputAction(arcS.getContents().getInputExecution());
			current.removeLastArc();
		}
	}

	// ToDo-Reza: explore the path deeper for more interesting test cases
	// List<NodeSymbolic> snodes = lastNode.subsumedNodes;
	// while (lastNode.getDepth() < ExecutionOptions.getMaxDepth()) {
	// NodeSymbolic sb = lastNode.subsumedBy;
	// if (sb != null) {
	// List<Arc<SymbolicTransition, SymbolicState>> subsumingChildren =
	// sb.getChildren();
	// for (Arc<SymbolicTransition, SymbolicState> child : subsumingChildren) {
	// addArc((ArcSymbolic) child);
	// }
	// lastNode = arcs.get(arcs.size() - 1).getTarget();
	// }
	// }
	public void exploreAllPathsByDepth(SymbolicPath current, List<SymbolicPath> paths, int depth) {
		setDepth(depth);
		List<Arc<SymbolicTransition, SymbolicState>> children = getChildren();
		if (children.size() == 0) {
			if (getDepth() < ExecutionOptions.getMaxDepth()) {
				NodeSymbolic sb = getSubsumedBy();
				children = sb.getChildren();
			} else {
				paths.add(new SymbolicPath(current));
			}
		}
		for (Arc<SymbolicTransition, SymbolicState> arc : children) {
			ArcSymbolic arcS = (ArcSymbolic) arc;
			current.addInputAction(arcS.getContents().getInputExecution());
			current.addArc(arcS);

			((NodeSymbolic) arc.getTarget()).exploreAllPathsByDepth(current, paths, depth + 1);
			current.removeInputAction(arcS.getContents().getInputExecution());
			current.removeLastArc();
		}
	}

	public int getNumberOfStates() {
		int number =1;
		for (Arc<SymbolicTransition, SymbolicState> child : getChildren()){
			number += ((NodeSymbolic)child.getTarget()).getNumberOfStates();
		}
		return number;
	}

	public int getNumberOfArcs() {
		int number = getChildren().size();
		for (Arc<SymbolicTransition, SymbolicState> child : getChildren()){
			number += ((NodeSymbolic)child.getTarget()).getNumberOfArcs();
		}
		return number;		
	}

	public void setSubsumedBy(NodeSymbolic node) {
		if (subsumedNodes.contains(node)){
			//System.out.println();
		}
		this.subsumedBy = node;
		
	}

	public String printInLines(){
		StringBuffer sb = new StringBuffer();
		
		sb.append(getContents().printInLines());
				
		return sb.toString();
	}

	public NodeSymbolic getSubsumedBy() {
		return subsumedBy;
	}
	
	public void addSubsumed(NodeSymbolic node){
		if (node==subsumedBy){
			//System.out.println();
		}
		subsumedNodes.add(node);
	}

	public void projectNode(NodeSymbolic synchNode, SymbolicExecutionTree origTree, SymbolicExecutionTree projTree, 
			Set<NodeSymbolic> inTree, String partName) {
		//check all transitions in synchronized		
		
	}

	/* JDA begin remove
	private boolean isNew(NodeSymbolic projectedTarget, Set<NodeSymbolic> inTree) {		
		return false;
	}

	private void removeStates(NodeSymbolic synchNode,
			SymbolicExecutionTree tree, String partName) {
		List<SymbolicState> currents = new ArrayList<SymbolicState>(synchNode.getContentsAll());
		
		for(SymbolicState state : currents){
			if (!tree.getFFSM().containsLocation(state.getLocation())){
				synchNode.getContentsAll().remove(state);
			}
		}
		
	}
	JDA: end remove */

	public List<SymbolicState> getContentsAll() {
		List<SymbolicState> result = new ArrayList<SymbolicState>();
		result.add(getContents());
		return result;
	}


	public void setSubsumedByCopy(NodeSymbolic sub) {
		this.subsumedByCopy = sub;		
	}


	public void addSubsumedCopy(NodeSymbolic sub) {
		this.subsumedNodesCopy.add(sub);		
	}

	@Override
	public boolean equals(Object o) {
		return this==o;
	}


	public boolean same(NodeSymbolic treeNode) {
		return getContents().equals(treeNode.getContents());
		
	}


	public void setTree(SymbolicExecutionTree tree) {
		getContents().setTree(tree);
		
	}


	
}
