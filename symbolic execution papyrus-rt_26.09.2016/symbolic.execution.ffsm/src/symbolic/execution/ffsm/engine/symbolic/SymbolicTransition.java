package symbolic.execution.ffsm.engine.symbolic;

import java.util.Map;
//import java.util.concurrent.SynchronousQueue;

//import javax.swing.plaf.basic.BasicSliderUI.ActionScroller;

import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.engine.concrete.ExecutionTransition;
import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.engine.concrete.OutputSequenceExecution;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.ffsm.model.TimerActionInput;
import symbolic.execution.ffsm.model.Transition;


public class SymbolicTransition {
	
	//transition from original model
	private Transition transition;
	
	//input action execution
	private ActionInputSymbolic inputExecution;
	
	//output sequence
	private OutputSequenceSymbolic output;
	
	//owning tree
	private SymbolicExecutionTree tree;
	
	public SymbolicTransition(Transition transition, ActionInputSymbolic inputExec, SymbolicExecutionTree tree) {		
		this.transition = transition;	
		this.inputExecution = inputExec;
		output = new OutputSequenceSymbolic();
		this.tree = tree;
	}


	public SymbolicTransition(SymbolicTransition other, SymbolicExecutionTree tree, Map<ActionOutput, ActionOutput> outputsMap, Map<ActionInput, ActionInput> inputsMap) {
		this.transition = other.transition;
		ActionInput mapped = inputsMap.get(other.inputExecution.getAction());
		this.inputExecution = new ActionInputSymbolic(other.inputExecution, mapped==null?other.inputExecution.getAction():mapped);
		this.output = new OutputSequenceSymbolic(other.output, outputsMap);
		this.tree = tree;
	}


	public SymbolicTransition(SymbolicTransition other,SymbolicExecutionTree tree) {
		if (other != null){
			this.transition = other.transition;
		
		this.inputExecution = other.inputExecution;
		this.output =other.output;
		}
		this.tree = tree;
	}


	public SymbolicTransition(SymbolicTransition other,
			SymbolicExecutionTree tree,
			Map<ActionOutput, ActionOutput> outputsMap) {
		this.transition = other.transition;		
		this.inputExecution = other.inputExecution;
		this.output = new OutputSequenceSymbolic(other.output, outputsMap);
		this.tree = tree;
	}


	@Override
	public String toString() {		
		return inputExecution.getAction().toString() + "(" + inputExecution.printConstraints() + ") " + 
		(output==null ? "" : output.toString());
	}


	public void setOutputSequence(OutputSequenceSymbolic output) {
		this.output = output;
	}

	public void setInputExecution(ActionInputSymbolic inputExecution){
		this.inputExecution = inputExecution;
	}

	public String printInLines() {
		StringBuffer sb = new StringBuffer();
		//JDA: Patch to print timeouts more accurately.
		if (inputExecution instanceof ActionInputTimeout) {
			sb.append(inputExecution.getAction().getName() + ".timeout()");
		}
		else {
			sb.append(inputExecution.toString());
		}
		sb.append("\n");
		if (output != null){
			sb.append(output.toString());		
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public ActionInputSymbolic getInputExecution(){
		return inputExecution;
	}


	public Transition getTransition() {
		return transition;
	}


	public ExecutionTransition createConcrete(Map<SymbolicVariable, Constant> solution) {
		
		//prepare concrete input
		InputActionExecution inputConcrete = inputExecution.createConcrete(solution);
		
		//prepare concrete output sequence
		OutputSequenceExecution outputConcrete = output.createConcrete(solution);
		
		return new ExecutionTransition(transition, inputConcrete, outputConcrete);
	}


	public OutputSequenceSymbolic getOuputSequence() {		
		return output;
	}


	public SymbolicExecutionTree getTree() {
		return tree;
	}

}
