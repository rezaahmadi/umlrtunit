package symbolic.execution.ffsm.engine.symbolic;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
import symbolic.execution.ffsm.model.ModelVariable;

//TODO: JDA: document this
public class SymbolicStateAuxiliary {
	//JDA: commented out
	/* --------------
	public void copyValuationsAndPCs(SymbolicState target, SymbolicState source) {
		List<Constraint> pc = source.getPC();
		List<Constraint> pcNew = new ArrayList<Constraint>(pc);
		ValuationSymbolic val = source.getValuation();
		ValuationSymbolic valNew = new ValuationSymbolic(val);
		target.putValPC(valNew, pcNew);
	}
	---------------- */
	
	//JDA: This code deep copies symbolic valuations.
	public ValuationSymbolic copyValuationSymbolic(ValuationSymbolic vs) {
		ValuationSymbolic res = new ValuationSymbolic();
		if (vs != null) {
			Map<SymbolicVariable, Expression> var = vs.getSymbolicVariables();
			for (Entry<SymbolicVariable, Expression> en: var.entrySet()) {
				Expression ex = en.getValue();
				SymbolicVariable v = en.getKey();
				ModelVariable<?> mv = vs.getVariableByName(v.getName());
				res.setValue((ModelVariable<?>)mv, copyExpression(ex));
			}
		}
		else {
			//System.out.println("JDA: copyValuationsSymbolic: valuation is null.");
		}
		return res;
	}
	//JDA: This code deep copies path constraints.
	public List<Constraint> copyConstraintList(List<Constraint> lc)  {
		List<Constraint> nlc = new ArrayList<Constraint>();
		if (lc != null) {
			for (Constraint c: lc) {
				Constraint nc = (Constraint)copyExpression(c);
				nlc.add(nc);
			}
		}
		return nlc;
	}
	
	//JDA: This code deep copies expressions.
	private Expression copyExpression (Expression exp) {
		//TODO: JDA: This may not handle all possible expression implementations.
		//           E.g., for choco.
		Expression res = null;
		if (exp instanceof BinaryExpression) {
			if ((symbolic.execution.constraints.choco.BinaryExpressionImpl) exp 
					instanceof symbolic.execution.constraints.choco.BinaryExpressionImpl) {
				Expression l = ((BinaryExpression) exp).getLeft();
				Expression r = ((BinaryExpression) exp).getRight();
				ArithmeticOperators op = ((BinaryExpression) exp).getOp();
				res = new symbolic.execution.constraints.choco.BinaryExpressionImpl(
						copyExpression(l), op, copyExpression(r));
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof UnaryExpression ){
			ArithmeticOperators op = ((UnaryExpression) exp).getOp();
			Expression e = ((UnaryExpression) exp).getExpression();
			res = new UnaryExpression(op, copyExpression(e));
		}
		else if (exp instanceof Constant){
			Object v = ((Constant) exp).getValue();
			if ((symbolic.execution.constraints.choco.ConstantImpl) exp 
					instanceof symbolic.execution.constraints.choco.ConstantImpl) {
				res = new symbolic.execution.constraints.choco.ConstantImpl(v.getClass(), v);
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof Constraint){
			if (exp instanceof symbolic.execution.constraints.manager.False) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.False();
			}
			else if (exp instanceof symbolic.execution.constraints.manager.True) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.True();
			}
			else if (exp instanceof LogicalConstraint) {
				if (((symbolic.execution.constraints.choco.LogicalConstraintImpl)exp) instanceof  
						symbolic.execution.constraints.choco.LogicalConstraintImpl) {
					LogicalOperators op = ((LogicalConstraint) exp).getOp();
					Constraint l = ((LogicalConstraint) exp).getLeft();
					Constraint r = ((LogicalConstraint) exp).getRight();
				
					if (l == null) {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl(null, op, (Constraint) copyExpression(r));
					}
					else {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl((Constraint)copyExpression(l), op, (Constraint)copyExpression(r));
					}
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
			else if (exp instanceof RelationalConstraint) {
				if (exp instanceof symbolic.execution.constraints.choco.RelationalConstraintImpl) {
					CompareOperators op = ((RelationalConstraint) exp).getOp();
					Expression l = ((RelationalConstraint)exp).getLeft();
					Expression r = ((RelationalConstraint) exp).getRight();
					res = new symbolic.execution.constraints.choco.RelationalConstraintImpl(copyExpression(l), op, copyExpression(r));
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
		}
		else if (exp instanceof SymbolicVariable) {
			res = exp;
		}
		else if (exp == null) {
			System.out.println("JDA: Copying null expression" );
		}
		else {
			System.out.println("JDA: Ouch - no handler for expression, " + exp.toString() + ", of type, " 
					+ exp.getClass()+ "!" );
		}
		return res;
	}
}
