package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import javax.swing.plaf.basic.BasicSliderUI.ActionScroller;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.TimerActionOutput;

public class SymbolicExecutionTree {
	protected String treeName;
	
	protected NodeSymbolic root;	
	
	protected Set<Location> locations = new HashSet<Location>();
	
	protected FunctionalFiniteStateMachine ffsm;

	protected Map<ActionOutput, ActionOutput> outputActionsMap = new HashMap<ActionOutput, ActionOutput>();
	protected Map<ActionInput, ActionInput> inputActionsMap = new HashMap<ActionInput, ActionInput>();
	
	protected String partName = "";
	

	public SymbolicExecutionTree(){
		this.treeName = "SET";
	}
	
	public SymbolicExecutionTree(FunctionalFiniteStateMachine ffsm) {
		this.ffsm = ffsm;
		this.treeName = ffsm.getName();
		createActionsMappings();
	}
	
	private void createActionsMappings() {
		for (ActionOutput action : ffsm.getActionsOutput()){
			if (action instanceof TimerActionOutput){
				outputActionsMap.put(action, new TimerActionOutput(action.getName()));
			} else {
				outputActionsMap.put(action, new ActionOutput(action));
			}
		}
		for(ActionInput action : ffsm.getActionsInput()){
			inputActionsMap.put(action, new ActionInput(action));
		}		
		
	}

	public SymbolicExecutionTree(NodeSymbolic root, FunctionalFiniteStateMachine ffsm){
		this(ffsm);
		this.root = root;
		
	}
	
	public SymbolicExecutionTree(SymbolicState initialState, FunctionalFiniteStateMachine ffsm){
		this(ffsm);
		root = new NodeSymbolic(initialState);
		
	}
	
	
	public SymbolicExecutionTree(SymbolicExecutionTree other) {
	  //JDA: added try catch: this was throwing a null pointer
	  //JDA: exception.
	  try {
		this.locations = new HashSet<Location>(other.locations);
		this.ffsm = other.ffsm;
		createActionsMappings();
		Map<ActionOutput,ActionOutput> outputsMap = new HashMap<ActionOutput, ActionOutput>();
		Map<ActionInput,ActionInput> inputsMap = new HashMap<ActionInput, ActionInput>();
		createTreeMaps(this,other, outputsMap,inputsMap);
		
		this.root = new NodeSymbolic(other.root, this, true, outputsMap, inputsMap);
		this.treeName = other.treeName;
	  }
	  //JDA: added catch.
	  catch (NullPointerException ex) {
		    System.out.println("Null pointer exception in SymbolicExecutionTree.");
			ex.printStackTrace();
			//throw ex;
	  } 	
	}
		

	public SymbolicExecutionTree(SymbolicExecutionTree other,
			Map<ActionInput, ActionInput> inputsMap,
			Map<ActionOutput, ActionOutput> outputsMap) {
		this.locations = new HashSet<Location>(other.locations);
		this.ffsm = other.ffsm;
		createActionsMappings(inputsMap, outputsMap);
		
		createTreeMaps(this,other, outputsMap,inputsMap);
		
		this.root = new NodeSymbolic(other.root, this, true, outputsMap, inputsMap);
		this.treeName = other.treeName;
	}

	private void createActionsMappings(Map<ActionInput, ActionInput> inputsMap,
			Map<ActionOutput, ActionOutput> outputsMap) {
		this.inputActionsMap = new HashMap<ActionInput, ActionInput>(inputsMap);
		this.outputActionsMap = new HashMap<ActionOutput, ActionOutput>(outputsMap);
		
	}

	protected void createTreeMaps(SymbolicExecutionTree thisTree,
			SymbolicExecutionTree otherTree, Map<ActionOutput, ActionOutput> outputsMap,
			Map<ActionInput, ActionInput> inputsMap) {
		for (ActionInput input : thisTree.inputActionsMap.keySet()){
			inputsMap.put(otherTree.mapInputAction(input), thisTree.inputActionsMap.get(input));
		}
		for(ActionOutput output : thisTree.outputActionsMap.keySet()){
			outputsMap.put(otherTree.mapOutputAction(output), thisTree.outputActionsMap.get(output));
		}
		
	}

	@Override
	public String toString() {
		return treeName;		
	}
	
	public void setRoot(NodeSymbolic root){
		this.root = root;
	}
	
	public NodeSymbolic getRoot(){
		return root;
	}

	public SymbolicPath checkInvariant(Constraint cst) {
		SymbolicPath path = new SymbolicPath();
		
		path.setSingleNode(root);
		path = root.checkInvariant(path, cst);
		
		return path;
		
	}

	public void addLocation(Location l){
		locations.add(l);
	}
	
	public boolean hasLocation(Location location) {
		return locations.contains(location);
	}

	public List<SymbolicPath> findPaths(String sourceName, String targetName) {
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		Map<SymbolicExecutionTree, Location> source = new HashMap<SymbolicExecutionTree, Location>();
		source.put(this, ffsm.getLocationByNameExplorable(sourceName));
		
		Map<SymbolicExecutionTree,Location> target = new HashMap<SymbolicExecutionTree, Location>();		
		target.put(this, ffsm.getLocationByNameExplorable(targetName));
		
		root.exploreReachable(source, target, new SymbolicPath(), paths);
				
		return paths;
	}

	public List<SymbolicPath> findPathsForSignals(Set<ActionOutput> actionOutputs) {
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		
		root.exploreForSignals(actionOutputs, new SymbolicPath(), paths);
		
		return paths;
	}

	public List<SymbolicPath> exploreAllPaths() {
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		
		
		root.exploreAllPaths(new SymbolicPath(),paths);
		
		return paths;
	}
	
	public List<SymbolicPath> exploreAllPathsByDepth() {
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		
		root.exploreAllPathsByDepth(new SymbolicPath(), paths, 1);
		
		return paths;
	}

	public int getNumberOfStates() {
		return root.getNumberOfStates();
	}

	public String getName() {		
		return treeName;
	}

	public void setName(String treeName) {
		this.treeName = treeName;
	}

	public FunctionalFiniteStateMachine getFFSM() {		
		return ffsm;
	}

	public Map<String, LocationVariable<?>> getLocationsVarsMapping() {		
		return ffsm.getLocationVarsMap();
	}

	public Map<String,ActionOutput> getOutputActions() {
		return ffsm.getActionsOutputMap();	
	}

	public Set<String> getLocationNames() {
		Set<String> names = new HashSet<String>();
		for (Location l : locations){
			names.add(l.getNameExplorable());
		}
		return names;
	}

	public Set<String> getAllLocationNames() {
		return ffsm.getLocationNames();
	}

	public int getNumberOfTransitions() {		
		return root.getNumberOfArcs();
	}

	public ActionInput mapInputAction(ActionInput action) {
		//if (action==null){
		//	int k =0;
		//}
		if (action.getName().contains("default")){
			return action;
		}
		return inputActionsMap.get(action);
	}

	public Map<ActionOutput, ActionOutput> getActionsOutputMap() {		
		return outputActionsMap;
	}

	public ActionOutput mapOutputAction(ActionOutput action) {		
		return outputActionsMap.get(action);
	}
	
	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public boolean containsInputAction(ActionInput action) {
		return inputActionsMap.containsValue(action);
	}

}
