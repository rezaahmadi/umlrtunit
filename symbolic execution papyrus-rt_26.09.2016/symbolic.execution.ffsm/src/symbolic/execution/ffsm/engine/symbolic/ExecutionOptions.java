package symbolic.execution.ffsm.engine.symbolic;

/* -------------------------------------------------------------------------------------------------------
   Added by J. David Andrews.
   Options to control the symbolic execution.
----------------------------------------------------------------------------------------------------------*/
public class ExecutionOptions {
	private static boolean newExecution = true;
	private static boolean checkDepth = true;
	private static int maxDepth = 100;
	private static int maxDisplayNodes = 3000; //JDA:
	private static boolean checkSolvability = true;
	private static boolean checkForDuplicatePCs = true;
	private static boolean newNameGeneration = true;
	
	public static void setNewExecution (boolean val){
		newExecution = val;
	}
	public static boolean getNewExecution (){
		return newExecution;
	}
	
	public static void setCheckDepth (boolean b){
		checkDepth = b;
	}
	public static boolean getCheckDepth (){
		return checkDepth;
	}

	public static void setMaxDepth (int val){
		maxDepth= val;
	}
	public static int getMaxDepth (){
		return maxDepth;
	}
	public static void setMaxDisplayNodes (int val){
		maxDisplayNodes= val;
	}
	public static int getMaxDisplayNodes (){
		return maxDisplayNodes;
	}
	public static void setCheckForDuplicatePCs (boolean b){
		checkForDuplicatePCs = b;
	}
	public static void setCheckSolvability (boolean b){
		checkSolvability = b;
	}
	public static boolean getCheckForDuplicatePCs (){
		return checkForDuplicatePCs;
	}
	public static boolean getCheckSolvability (){
		return checkSolvability;
	}
	public static boolean getNewNameGeneration() {
		return newNameGeneration;
	}
	public static void setNewNameGeneration(boolean b) {
		newNameGeneration = b;
	}
}
