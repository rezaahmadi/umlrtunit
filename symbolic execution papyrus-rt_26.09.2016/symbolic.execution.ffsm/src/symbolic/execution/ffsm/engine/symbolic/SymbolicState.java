package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
import java.util.HashMap;
//import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.engine.concrete.ExecutionState;
//import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.engine.symbolic.SymbolicStateAuxiliary;
//import symbolic.execution.ffsm.model.ActionInput;
//import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.TimerActionInput;
import symbolic.execution.ffsm.model.TimerActionOutput;
import symbolic.execution.ffsm.model.Valuation;

//JDA: Lower-level symbolic states can appear in multiple places in the 'synchronized' symbolic
//     execution tree. The maps 'pcMap' and 'valMap' keep track of the mapping from
//     parent states to substates to ensure that there is no interference on dupicated states
public class SymbolicState {
	//location in FFSM
	protected Location location;
	
	//JDA: begin addition
	protected long nodeID = 0L;
	protected long subsumedByID = -1L; //April 12, 2016
	protected boolean valuationUpdated = false;
	protected String saveString;
	protected boolean mayBeDirty = false;
	//JDA: the next three items are added to ensure non-iterference caused by duplicated
	//     symbolic states in 'synchronized' trees.
	private static boolean synchronizing = false;
	private static boolean serializing = false;
	private Map<Object, List<Constraint>> pcMap = new HashMap<Object, List<Constraint>>();
	private  Map<Object, ValuationSymbolic> valMap = new HashMap<Object, ValuationSymbolic>();
	private SymbolicStateAuxiliary auxiliary = new SymbolicStateAuxiliary();
	//JDA: end addition
	
	//symbolic valuation of variables
	protected ValuationSymbolic valuation;
	
	//path conditions
	protected List<Constraint> pathCondition;
	
	protected ArcSymbolic ingoingArc;
		
	//private List<ActionInputSymbolic> queue;
	
	protected SymbolicExecutionTree tree;
	
	protected List<TimerActionOutput> setTimers;
	
	public SymbolicState(Location location, ValuationSymbolic valuation, SymbolicExecutionTree tree) {
		this.location = location;
		this.valuation = valuation;
		this.tree = tree;
		
		pathCondition = new ArrayList<Constraint>();	
		//queue = new ArrayList<ActionInputSymbolic>();
		setTimers = new ArrayList<TimerActionOutput>();
		;
	}	
	
	public SymbolicState(Location location, ValuationSymbolic valuation, List<Constraint> pathCondition, SymbolicExecutionTree tree,
			List<TimerActionOutput> setTimers) {
		this.location = location;
		this.valuation = new ValuationSymbolic(valuation);	
		if (pathCondition == null){
			this.pathCondition = new ArrayList<Constraint>();
		} else {
			this.pathCondition = new ArrayList<Constraint>(pathCondition);
		}
		//this.queue = new ArrayList<ActionInputSymbolic>();
		this.tree = tree;
		if (setTimers != null)
			this.setTimers = setTimers;
		else
			this.setTimers = new ArrayList<TimerActionOutput>();
	}
	
	public SymbolicState (SymbolicState other, SymbolicExecutionTree tree){
		this.location = other.location;
		this.valuation = new ValuationSymbolic(other.valuation);
		this.pathCondition = new ArrayList<Constraint>(other.pathCondition);
		this.ingoingArc = other.ingoingArc;
		//this.queue = new ArrayList<ActionInputSymbolic>(other.queue);
		this.tree = tree;
		this.setTimers = new ArrayList<TimerActionOutput>(other.setTimers);
	}
	
	public SymbolicState(SymbolicState treeState) {
		this(treeState, treeState.getTree());
	}
	
	//JDA: begin addition
	public void unexpected(String msg) {
		System.out.println(msg);
	}
	public void setID(long id) {
		nodeID = id;
	}
	//April 12, 2016
	public void setSubsumedByID(long id) {
		subsumedByID = id;
	}
	public static void startSynchronizing() {
		synchronizing = true;
	}
	public static void stopSynchronizing() {
		synchronizing = false;
	}
	public static void startSerializing() {
		serializing = true;
	}
	public static void stopSerializing() {
		serializing = false;
	}
	public long getID() {
		return(nodeID);
	}
	private void getMappedState(Object master) {
		if (!synchronizing && !serializing) {
			unexpected("Unexpected call to 'getMappedState(SymbolicState master)'.");
		}
		String tp = master.getClass().getSimpleName();
		if (! tp.equals("NodeSymbolicSynch")) {
			unexpected("Unexpected type: " + tp);
		}
		if (! valMap.keySet().contains(master)) {
			valMap.put(master, auxiliary.copyValuationSymbolic(valuation));
			pcMap.put(master, auxiliary.copyConstraintList(pathCondition));
		}
		else {
			//System.out.print("");
		}
	}
	//JDA: end addition

	public Location getLocation() {		
		return location;
	}

	//JDA: the master state is understood to contain the given state.
	//     We retrieve the valuation corresponding to the master.
	public ValuationSymbolic getValuation(Object master) {
		if ((!synchronizing) && (!serializing)) {
			unexpected("Unexpected call to 'getValuation(SymbolicState master)'.");
		}
		getMappedState(master);
		return valMap.get(master);
	}
	
	public ValuationSymbolic getValuation() {
		if (synchronizing && !serializing) {
			unexpected("Unexpected call to 'getValuation'.");
		}
		return valuation;
	}

	//JDA: added in branch "sync-fix" to support restoring data
	//     that was destroyed when a node was visited multiple
	//     times.
	public void putValPC(ValuationSymbolic val, List<Constraint> PC) {
		if (synchronizing) {
			unexpected("Unexpected call to 'putValPC'.");
		}
		pathCondition = PC;
		valuation = val;
	}
	public List<Constraint> getPC() {
		if (synchronizing && !serializing) {
			unexpected("Unexpected call to 'getPC'.");
		}
		return pathCondition;
	}
	
	//JDA: the master state is understood to contain the given state.
	//     We retrieve the path constraint corresponding to the master.
	public List<Constraint> getPC(Object master) {
		//return new ArrayList<Constraint>(pathCondition);
		if ((!synchronizing) && (!serializing)) {
			unexpected("Unexpected call to 'getPC(SymbolicState master)'.");
		}
		getMappedState(master);
		return pcMap.get(master);
	}
	
	public void addPCConstraint(Constraint constraint){
		if (synchronizing) {
			unexpected("Unexpected call to 'addPCConstraint'");
		}
		pathCondition.add(constraint);
	}
	
	public void setIngoingArc(ArcSymbolic arc){
		this.ingoingArc = arc;
	}
	
	//JDA: the master state is understood to contain the given state.
	//     We use the valuation and path constraint corresponding to the 
	//     master.
	public String toString(Object master) {
		if (!synchronizing) {
			unexpected("Unexpected call to 'toString(SymbolicState master)'.");
		}
		getMappedState(master);
		String result;
		result = toStringLocal(valMap.get(master), pcMap.get(master));
		return result;
	}

	@Override
	public String toString() {
		return toStringLocal(valuation, pathCondition);
	}
	
	public String toStringLocal(ValuationSymbolic val, List<Constraint> pc) {	
		StringBuffer sb = new StringBuffer();
		//TODO: JDA: Can the 'dirty' case occur any more?
		if (mayBeDirty) {
			sb.append("May be dirty. Saved value: [" + saveString + "]\n");
		}
		//if (ingoingArc != null) {
		//	SymbolicTransition contents = ingoingArc.getContents();
			//sb.append("input: [" + contents.getInputExecution().toString() + "]\n");
			//sb.append("output: " + contents.getOuputSequence().toString() + "\n");	
		//}
		//TODO:JDA:end addition
		//JDA: end debug
		//sb.append(java.lang.System.identityHashCode(this) + ": ");
		if (location != null){
			sb.append(location.getName());
		}
		if (val !=null){
			sb.append(",VAL=");
			sb.append(val.print());
		}
		if (pc !=null){
			sb.append(",PC=[");
			for (Constraint c : pc){
				if (c!=null){
					sb.append(c.print());
					sb.append(",");
				}
			}
			sb.append("]");
		}
		/*if (queue !=null && !queue.isEmpty()){
			sb.append(",QUEUE=");
			sb.append(queue);
		}*/
		//JDA: adding timers to the 'toString' method.
		return sb.toString() + ", setTimers: " + setTimers;
	}

	public String printInLines(Object master) {
		if (!synchronizing && !serializing) {
			unexpected("Unexpected call to 'printInLines'.");
		}
	    return printInLinesLocal(valMap.get(master), pcMap.get(master));
	}
	public String printInLines() {
	    return printInLinesLocal(valuation, pathCondition);
	}
	//JDA: Fixed the treatment of EOLs and added "setTimers"
	protected String printInLinesLocal(ValuationSymbolic val, List<Constraint> pc) {
	
		StringBuffer sb = new StringBuffer();
		boolean needsEol = false;
		if (ingoingArc != null) {
			SymbolicTransition contents = ingoingArc.getContents();
			sb.append("input:  [" + contents.getInputExecution().toString() + "]\n");
			sb.append("output: " + contents.getOuputSequence().toString() + "\n");
		}
		sb.append("ID: " + nodeID);
		if (subsumedByID != -1) {
			sb.append("(" + subsumedByID + ")");
		}
		
		if (location != null) {
			sb.append(" LOC: " + location.getName());
			needsEol = true;
		}
		if (val != null) {
			if (needsEol) {
				sb.append("\n");
			}
			sb.append("VAL=");
			sb.append(val.toString());
			needsEol = true;
		}
		
		if (pc !=null ){
			if (needsEol) {
				sb.append("\n");
			}
			sb.append("PC=[");		
			for (Constraint c : pc){
				if (c!=null){
					sb.append(c.print());
				}
			}
			sb.append("]");
			needsEol = true;
		}
		//JDA: Added the following. We need to see
		//     the set of timers to understand subsumption.
		if (setTimers !=null ){
			if (needsEol) {
				sb.append("\n");
			}
			sb.append("setTimers=" + setTimers.toString());		
			needsEol = true; //JDA: in case something might be needed after.
		}
		/*if (queue !=null && !queue.isEmpty()){
			sb.append("\nQUEUE=");
			sb.append(queue);
		}*/
		return sb.toString();
	}
	
	
	//returns true if this state subsumes other
	public boolean subsumes(SymbolicState s){
		//if both locations null - then subsumes
		if (synchronizing) {
			unexpected("Unexpected call to 'subsumes'.");
		}
		if (location== null){
			if (s.location==null)
				return true;
			else
				return false;
		} else {
			if (s.location == null){
				return false;
			}
		}
		
		//if locations different -> doesn't subsume
		if (!location.equals(s.location)){
			return false;
		}
		
		//check if subsumes without mapping	
		//JDA: Split this for debugging
		ValuationSymbolic v = s.valuation;
		List<Constraint> pc = s.pathCondition;
		List<TimerActionOutput> timers = s.getTimers();
		boolean valOK = valuation.equals(v);
		boolean pcOK = pc.containsAll(pathCondition);
		boolean timersOK = containsAllTimers(timers);
		boolean subsumes = valOK && pcOK && timersOK;
		//boolean subsumes = valuation.equals(s.valuation) && (s.pathCondition).containsAll(pathCondition) && containsAllTimers(s.getTimers());		
		if (subsumes){
			return subsumes;
		}
		//create mapping of input variables
		Map<SymbolicVariable, SymbolicVariable> compositeMapping = createMappingInput(this.getIngoingTransition(), s.getIngoingTransition());
		
		if (compositeMapping == null){
			return false;
		}
		
		Map<SymbolicVariable, SymbolicVariable> compositeMappingAll = new HashMap<SymbolicVariable, SymbolicVariable>();
		
		SymbolicState currentThis = this;
		SymbolicState currentOther = s;
		while (compositeMapping != null){
			compositeMappingAll.putAll(compositeMapping);
			
			currentThis = currentThis.getParent();
			currentOther = currentOther.getParent();
			
			if (currentOther==null || currentThis == null){
				break;
			}
			compositeMapping = createMappingInput(currentThis.getIngoingTransition(), currentOther.getIngoingTransition());			
		}
		
		//if mapping not possible -> doesn't subsume
		if (compositeMappingAll == null || compositeMappingAll.isEmpty()){
			return false;
		}
		
		//if mapping possible				
		List<Constraint> pcVarReplaced = replace(pathCondition, compositeMappingAll);
		ValuationSymbolic valVarReplaced = valuation.replace(compositeMappingAll);
		
		//System.out.println("!!!!Comparing " + (pcVarReplaced.toString()) + "   AND   " + s.printPC());
		//System.out.println("Comparing " + valVarReplaced + "------" + s.valuation);
		return valVarReplaced.equals(s.valuation) &&  s.pathCondition.containsAll(pcVarReplaced);
	}

	protected SymbolicState getParent() {
		if (ingoingArc == null || ingoingArc.getSource() == null){
			return null;
		}
		return ingoingArc.getSource().getContents();
	}

	public SymbolicTransition getIngoingTransition() {
		if (ingoingArc == null){
			return null;
		}
		return ingoingArc.getContents();
	}

	protected boolean containsAllTimers(List<TimerActionOutput> timers) {
		if (timers.size() != setTimers.size()){
			return false;
		}		
		for (int i=0; i < timers.size(); i++){
			if (!timers.get(i).eq(setTimers.get(i))){
				return false;
			}
		}
		return true;
		
	}

	protected List<Constraint> replace(List<Constraint> constraints,
			Map<SymbolicVariable, SymbolicVariable> mapping) {
		List<Constraint> replaced = new ArrayList<Constraint>();
		for (Constraint c : constraints){
			replaced.add((Constraint)c.replaceVariables(mapping));
		}
		return replaced;
	}

	protected Map<SymbolicVariable, SymbolicVariable> createMappingInput(SymbolicTransition t1, SymbolicTransition t2) {		
		Map<SymbolicVariable,SymbolicVariable> result = new HashMap<SymbolicVariable, SymbolicVariable>();
		if (t1==null || t2 ==null || !t1.getTransition().equals(t2.getTransition())){
			return null;
		}
		Map<SymbolicVariable, Expression> m1 = t1.getInputExecution().getMappingSymbolic();
		Map<SymbolicVariable, Expression> m2 = t2.getInputExecution().getMappingSymbolic();
		for (SymbolicVariable mVar : m1.keySet()){
			Expression e1 = m1.get(mVar);			
			if (! (e1 instanceof SymbolicVariable)){
				continue;
			}
			SymbolicVariable from = (SymbolicVariable) e1;
			
			Expression e2 = m2.get(mVar);
			if (e2 == null){
				e2 = findExpressionByName(mVar,m2);
			}
			if (! (e2 instanceof SymbolicVariable)){
				continue;
			}
			SymbolicVariable to = (SymbolicVariable)e2;
			result.put(from,to);
			
		}
		return result;
	}
	
	private Expression findExpressionByName(SymbolicVariable variable,
			Map<SymbolicVariable, Expression> mapping) {
		for (SymbolicVariable var : mapping.keySet()){
			if (var.equals(variable)){
				return mapping.get(var);
			}
		}
		return null;
	}

	//TODO: JDA: Will this needed? Why is it not called?
	/*public String printPC(){
		StringBuffer sb = new StringBuffer();
		sb.append("PC=[");		
		for (Constraint c : pathCondition){
			sb.append(c.print());
			sb.append(",");
		}
		sb.append("]");
		
		return sb.toString();
	}
	*/
	
	public ExecutionState createConcrete(Map<SymbolicVariable, Constant> solution) {
		Valuation val = new Valuation();
		for(ModelVariable<?> var : valuation.getVariables()){
			Expression value = valuation.getValue(var).replaceVariables(solution);			
			if (value instanceof Constant){
				val.setValue(var, ((Constant)value).getValue());
			}
			
		}
		
		return new ExecutionState(location, val);
	}

	/*public void setQueue(List<ActionInputSymbolic> queue) {
		this.queue = queue;
		
	}*/

	public SymbolicExecutionTree getTree() {
		return tree;
	}
	
	public void updateValuation(Map<SymbolicVariable, Expression> vals,
			Object master) {
		getMappedState(master);
		if (valuation == null || vals == null){
			return;
		}
		if (valuationUpdated) {
			//System.out.println("JDA: Vaulation has previously been updated for SymbolicState: <<" + this + ">>.");
			//System.out.println("JDA: Original value was: <<" + saveString + ">>.");
			//System.out.print("");
			if (! this.toString().equals(saveString)) { 
				mayBeDirty = true;
			}
		} else {
			saveString = this.toString();
		}		
		//update valuation
		
		ValuationSymbolic val = valMap.get(master);
		Set<ModelVariable<?>> vars = val.getVariables();
		if (vars.size() == 0) {
			System.out.print("");
		}
		for (ModelVariable<?> var: vars ){
			Expression variableValue = val.getValue(var);
			Expression expr = variableValue.replaceVariables(vals);
			val.setValue(var, expr);
		}
		valMap.put(master, val);
		//update PC
		List<Constraint> newPC = new ArrayList<Constraint>();
		List<Constraint> pc = pcMap.get(master);
		for(Constraint c : pc){
			Constraint newC = (Constraint) c.replaceVariables(vals);
			newPC.add(newC);
		}
		pcMap.put(master, newPC);
		valuationUpdated = true;	
	}
	
	public void updateValuation(Map<SymbolicVariable, Expression> valuationMapping) {
		//TODO: JDA: comment this out in production code.
		if (synchronizing) {
			unexpected("Unexpected call to updateValuation.");
		}
		if (valuation == null || valuationMapping == null){
			return;
		}
		if (valuationUpdated) {
			//System.out.println("JDA: Vaulation has previously been updated for SymbolicState: <<" + this + ">>.");
			//System.out.println("JDA: Original value was: <<" + saveString + ">>.");
			//System.out.print("");
			if (! this.toString().equals(saveString)) { 
				mayBeDirty = true;
			}
		} else {
			saveString = this.toString();
		}		
		//update valuation
		for (ModelVariable<?> var: valuation.getVariables() ){
			Expression expr = valuation.getValue(var).replaceVariables(valuationMapping);
			valuation.setValue(var, expr);
		}
		//update PC
		List<Constraint> newPC = new ArrayList<Constraint>();
		for(Constraint c : pathCondition){
			Constraint newC = (Constraint) c.replaceVariables(valuationMapping);
			newPC.add(newC);
		}
		pathCondition = newPC;
		valuationUpdated = true;
	}

	public TimerActionOutput containSetTimer(TimerActionInput action) {
		//System.out.println(setTimers);
				
		for (TimerActionOutput o : setTimers){			
			if (o.getName().equals(action.getName())){	
				return o;
			}
		}
		return null;
	}

	public void addSetTimers(OutputSequenceSymbolic ouputSequence) {
		for (ActionOutputInstance ao : ouputSequence.getSequence()){
			if (ao.getActionOutput() instanceof TimerActionOutput){
				//JDA: I protected this with an "if".
				//if (setTimers.indexOf(ao.getActionOutput()) < 0) {
					setTimers.add((TimerActionOutput) ao.getActionOutput());
				//}
			}
		}
		
	}

	public List<TimerActionOutput> getTimers() {		
		return new ArrayList<TimerActionOutput>(setTimers);
	}

	//JDA: Added for debugging.
	public int timerCount() {
		int count = setTimers.size();
		return count;
	}
	
	public void removeTimerSet(TimerActionOutput timer) {
		if (timer != null){
			//JDA: I changed this to log if a timer appears more than once.
			int inx = setTimers.indexOf(timer);
			if (inx >= 0) {
				setTimers.remove(timer);
				inx = setTimers.indexOf(timer);
				if (inx >= 0) {
					System.out.println("JDA: timer, " + timer + ", appears more than once!");
				}
			}
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof SymbolicState ){
			SymbolicState other = (SymbolicState) o;
			
			if (location==null || pathCondition == null || valuation==null || other.pathCondition==null){
				return false;
			}
			
			return location.equals(other.location) && pathCondition.containsAll(other.pathCondition) &&
				other.pathCondition.containsAll(pathCondition) && valuation.equals(other.valuation);
			
		}
		return false;
	}
	
	public void setTree(SymbolicExecutionTree tree) {
		this.tree = tree;
	}

	public void addSetTimer(TimerActionOutput tao) {
		setTimers.add(tao);
		
	}

	public ArcSymbolic getIngoingArc() {
		return ingoingArc;
	}

	public void fixMaps(Object oldMaster, Object newMaster) {
		if (!synchronizing) {
			unexpected("Unexpected call to 'fixMaps(SymbolicState oldMaster, SymbolicState newMaster)'.");
		}
		if (valMap.containsKey(oldMaster)) {
			ValuationSymbolic val = valMap.get(oldMaster);
			//valMap.remove(oldMaster);
			valMap.put(newMaster, auxiliary.copyValuationSymbolic(val));
			List<Constraint> pc = pcMap.get(oldMaster);
			//pcMap.remove(oldMaster);
			pcMap.put(newMaster, auxiliary.copyConstraintList(pc));
		}
	}
}
