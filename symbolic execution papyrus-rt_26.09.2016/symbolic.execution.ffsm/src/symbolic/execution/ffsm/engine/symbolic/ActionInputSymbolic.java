package symbolic.execution.ffsm.engine.symbolic;


//import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
//import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constant;
//import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.PassiveClass;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.Valuation;
//import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;

/* ====================================================================================================================
History:
    Commented out unused imports and one unused local variable. April 22, 2016.
    
 	Bugs 2. Changes were made to prevent unreachable error states. These ensure that symbolic inputs with the same
 	port and message signal use the same symbolic output variable. The bug was that original fix gave to different
 	Choco variables with the same name. This created unreachable error nodes in the SET. Completed April 17, 2016..
======================================================================================================================= */
public class ActionInputSymbolic {
	
	private ActionInput action;
	
	private Map<ModelVariable<?>, Expression> mappingInputVariables;
	
	private Map<SymbolicVariable, Expression> mappingSymbolicVariables;
	
	//private List<Constraint> mappingConstraints;
	
	private ActionInputSymbolic(){
		mappingInputVariables = new HashMap<ModelVariable<?>, Expression>();
		//mappingConstraints = new ArrayList<Constraint>();
		mappingSymbolicVariables = new HashMap<SymbolicVariable, Expression>();
	}
	
	public ActionInputSymbolic(ActionInput action, boolean isNewExec, HashMap<String, SymbolicVariable> variableMap){
		this();
		
		this.action = action;

		int number;
		//JDA: Replace the following
		/*
		if (ExecutionOptions.getNewNameGeneration()) {
			number = action.getNumberOfExecs();
		}
		else {
			number = action.useNumberOfExecs();
		}
		*/
		//JDA: with
		number = action.useNumberOfExecs();
		//JDA: end replace
		//create symbolic variables		
		for (ModelVariable<?> var : action.getVars()){
			// create symbolic variable
			//JDA: begin change for Bug 2
			SymbolicVariable constraintVar = null;
			if (variableMap == null) {
				//JDA: in this case we use the old method of generation. It had two issues.
				//     1. Different transitions from a node in the SET represented the
				//        same symbolic input, but were represented by different symbolic
				//        input variables.
				//    2. In that case, the negations of the new path constraints
				//       could not be collected into a path constraint for the new
				//       error transition for the input.    
				constraintVar = var.generateConstraintVariable(number+(isNewExec?"S":""));
			}
			else {
				//JDA: This is the new method of name generation that respects the identity
				//     of symbolic inputs that represent the same input and enables
				//     error processing. Two variables should have the same symbolic variable
				//     if they have the same port, signal, and data name.
				String key = action.getPortName() + "." + action.getSignalName() + "." + var.getName();
				SymbolicVariable tmp = variableMap.get(key);
				if (tmp == null) {
					constraintVar = var.generateConstraintVariable(number+(isNewExec?"S":""));
					variableMap.put(key, constraintVar);
				}
				else {
					constraintVar = tmp;
				}
			}
			//JDA: end change
			//System.out.println("JDA: ActionInputSymbolic.ActionInputSymbolic(action, isNewExec):");
			//System.out.println("JDA:     action = [" + action + "], isNewExec=" +isNewExec + ", var =[" + var +  "].");
			//System.out.println("JDA:     constraintVar = [" + constraintVar + "] constrainVar.hashCode = " + constraintVar.hashCode() + ".");
			
			//if I generated a passive object
			if (constraintVar instanceof PassiveObject){
				PassiveObject pObjectNew = (PassiveObject) constraintVar;
				
				PassiveObject pObjectExist = (PassiveObject) var.getSymbolicVar();
				
				for (String fieldName : pObjectNew.getFieldNames()){
					mappingSymbolicVariables.put(pObjectExist.getVarField(fieldName), pObjectNew.getVarField(fieldName));
				}
			}
			
			//put into mappings
			mappingInputVariables.put(var, constraintVar);
			mappingSymbolicVariables.put(var.getSymbolicVar(), constraintVar);
			
			//create constraint
			//mappingConstraints.add(createConstraint(var.getSymbolicVar(), constraintVar));
		}
	}
	

	//JDA: added the final parameter, 'variableMap' for Bug 2.
	public ActionInputSymbolic(ActionInput input,
			Map<ActionVariableOutput<?>, Expression> inputMapping, 
			Map<SymbolicVariable, Expression> symbolicVarMapping,
			HashMap<String, SymbolicVariable> variableMap) {
		this();
		action = input;
		for(ActionVariableInput<?> varInput : action.getVars()){
			//symbolic variable new
			SymbolicVariable varSymbNew;
			//DA: Replace the following:
			/*
			if (ExecutionOptions.getNewNameGeneration()) {
				varSymbNew = varInput.generateConstraintVariable(action.getNumberOfExecs() + "");
			}
			else {
				varSymbNew = varInput.generateConstraintVariable(action.useNumberOfExecs() + "");
			}
			*/
			//JDA: with:
			//JDA: begin replacement for Bug 2
			if (variableMap == null) {
				//JDA: in this case we use the old method of generation. It had two issues.
				//     1. Different transitions from a node in the SET represented the
				//        same symbolic input, but were represented by different symbolic
				//        input variables.
				//    2. In that case, the negations of the new path constraints
				//       could not be collected into a path constraint for the new
				//       error transition for the input.    
				varSymbNew = varInput.generateConstraintVariable(action.useNumberOfExecs() + "");
			}
			else {
				//JDA: This is the new method of name generation that respects the identity
				//     of symbolic inputs that represent the same input and enables
				//     error processing. Two variables should have the same symbolic variable
				//     if they have the same port, signal, and data name.
				String key = action.getPortName() + "." + action.getSignalName() + "." + varInput.getName();
				SymbolicVariable tmp = variableMap.get(key);
				if (tmp == null) {
					varSymbNew = varInput.generateConstraintVariable(action.useNumberOfExecs() + "");
					variableMap.put(key, varSymbNew);
				}
				else {
					varSymbNew = tmp;
				}
			}
			//JDA: end replacement
			//symbolic variable original
			SymbolicVariable varSymbOriginal = varInput.getSymbolicVar();
			
			
			//find variable output with the same name
			for(ActionVariableOutput<?> var : inputMapping.keySet()){
				if (varInput.getName().equals(var.getName())){
					mappingInputVariables.put(varInput, inputMapping.get(var));
					if (varSymbOriginal instanceof PassiveObject){
						PassiveObject poOriginal = (PassiveObject)varSymbOriginal;
						//PassiveObject poNew = (PassiveObject)varSymbNew;
						for (String fieldName : poOriginal.getFieldNames()){
							SymbolicVariable fieldVarOriginal = poOriginal.getVarField(fieldName);
							
							for (SymbolicVariable fieldsReceived : symbolicVarMapping.keySet()){
								if (fieldsReceived.getName().equals(fieldVarOriginal.getName())){
									mappingSymbolicVariables.put(fieldVarOriginal, symbolicVarMapping.get(fieldsReceived));
								}
							}
							
						}
						
					} else {
						mappingSymbolicVariables.put(varSymbOriginal, inputMapping.get(var));
					}
				}
			}			
		}
	}

	public ActionInputSymbolic(ActionInputSymbolic other) {
		this.action = other.action;
		this.mappingInputVariables = other.mappingInputVariables;
		this.mappingSymbolicVariables = other.mappingSymbolicVariables;
		//this.mappingConstraints = other.mappingConstraints;
	}

	public ActionInputSymbolic(ActionInputSymbolic other,
			ActionInput mappedAction) {
		this(other);
		this.action = mappedAction;
	}

	

	/*private Constraint createConstraint(SymbolicVariable original, SymbolicVariable execution){
		return SymbolicFactory.createRelationalConstraint(original, CompareOperators.eq, execution);
	}*/

	/*public List<Constraint> getConstraints() {		
		return new ArrayList<Constraint>(mappingConstraints);
	}*/

	public Map<SymbolicVariable,Expression> getMappingSymbolic() {		
		return mappingSymbolicVariables;
	}

	public Map<ModelVariable<?>, Expression> getMappingInputVariables() {		
		return mappingInputVariables;
	}

	public ActionInput getAction() {
		return action;
	}

	public Collection<SymbolicVariable> getConstraintVars() {
		Set<SymbolicVariable> vars = new HashSet<SymbolicVariable>();
		
		for (Expression expr: mappingInputVariables.values()){
			vars.addAll(expr.collectVariables());
		}
		return vars;//mappingInputVariables.values();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(action.getName());
		sb.append("(");
		for(ModelVariable<?> var : mappingInputVariables.keySet()){
			if (mappingInputVariables.get(var)==null){
				sb.append("null");
			} else {
			sb.append(mappingInputVariables.get(var).toString());
			}
		}
		if (mappingInputVariables.size() > 0){
			return sb.substring(0,sb.length()) +")";
		} else {
			return sb.toString() + ")";
		}	
		//return sb.toString();
	}
	
	public String printConstraints() {	
		StringBuffer sb = new StringBuffer();
		for (SymbolicVariable sv : mappingSymbolicVariables.keySet()){
			sb.append(sv.print());
			sb.append("=");
			sb.append(mappingSymbolicVariables.get(sv).print());
			sb.append(",");
		}
		return sb.toString();
	}
	


	public InputActionExecution createConcrete( Map<SymbolicVariable, Constant> solution) {
		//prepare valuation
		Valuation val = new Valuation();
		try {
		
		for (ModelVariable<?> var : mappingInputVariables.keySet()){
			Expression inputExpr = mappingInputVariables.get(var);
			Constant value = null;
			if (inputExpr instanceof SymbolicVariable){
				value = solution.get(mappingInputVariables.get(var));
			} else if (inputExpr instanceof Constant){
				value = (Constant) inputExpr;
			} else {
				throw new RuntimeException("cannot make input variable concrete (not variable or constant)");
			}
		
			//System.out.println("!!!!!!!!!! var " + var);
			val.setValue(var, value.getValue());
		}
		
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		return new InputActionExecution(action, val);
	}
	
	public void setValue(ModelVariable<?> var, Expression expr){
		mappingInputVariables.put(var, expr);
		mappingSymbolicVariables.put(var.getSymbolicVar(), expr);
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof ActionInputSymbolic){
			ActionInputSymbolic other = (ActionInputSymbolic) o;
			
			return action.equals(other.action) && equalValuation(other.mappingInputVariables);
		}
		return super.equals(o);
	}

	private boolean equalValuation(	Map<ModelVariable<?>, Expression> other) {
		for (ModelVariable<?> var : mappingInputVariables.keySet()){
			if ((mappingInputVariables.get(var) != null  && other.get(var) != null) &&
					!mappingInputVariables.get(var).equals(other.get(var))){
				return false;
			}
		}
		return true;
	}

	public Map<SymbolicVariable, Expression> getValuationMapping() {
		return mappingSymbolicVariables;
	}
	
	public void setMappingInputVariables(Map<ModelVariable<?>, SymbolicVariable> mapping){
		for (ModelVariable<?> var : mapping.keySet()){
			mappingInputVariables.put(var, mapping.get(var));
			mappingSymbolicVariables.put(var.getSymbolicVar(), mapping.get(var));
		}
	}

	public void setAction(ActionInput action) {
		this.action = action;
		
		
	}

	public void mapVariables(ActionInputSymbolic other) {
		action.setVars(other.getAction().getVars());
		
		mappingInputVariables = other.mappingInputVariables;
		mappingSymbolicVariables = other.mappingSymbolicVariables;
		
	}
}
