package symbolic.execution.ffsm.engine.symbolic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.utils.Pair;

public class SolvedPCs {
	
	private Map<Pair<List<Constraint>, List<SymbolicVariable>>, 
				Map<SymbolicVariable,Constant>> saved;
	
	public SolvedPCs(){
		saved = new HashMap<Pair<List<Constraint>,List<SymbolicVariable>>,
					Map<SymbolicVariable,Constant>>();
	}
	
	public void addSolved (List<Constraint> pc, List<SymbolicVariable> inputVars, 
			Map<SymbolicVariable, Constant> solution){
		//create pair
		Pair<List<Constraint>,List<SymbolicVariable>> pair = 
			new Pair<List<Constraint>, List<SymbolicVariable>>(pc, inputVars);
		//add to map
		saved.put(pair,solution);
	}
	
	public Map<SymbolicVariable,Constant> findSolution(List<Constraint> otherPC, List<SymbolicVariable> otherVars){
		Map<SymbolicVariable,Constant> solution = null;
		
		for (Pair<List<Constraint>, List<SymbolicVariable>> pair : saved.keySet() ){
			//check constraints
			if (sameConstraints(pair.getFirst(), otherPC)){
				//check vars
				if (sameVars(pair.getSecond(), otherVars)){
						
					return saved.get(pair);
					
				}				
			}		
		}
		
		return solution;
	}

	private boolean sameVars(List<SymbolicVariable> stored,
			List<SymbolicVariable> other) {
		return stored.containsAll(other);
	}

	private boolean sameConstraints(List<Constraint> stored, List<Constraint> other) {				
		return stored.containsAll(other);
	}

}
