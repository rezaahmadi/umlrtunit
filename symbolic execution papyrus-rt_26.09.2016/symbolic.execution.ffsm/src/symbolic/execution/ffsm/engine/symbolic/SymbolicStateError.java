package symbolic.execution.ffsm.engine.symbolic;

//import java.util.ArrayList;
//import java.util.List;

//import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.ffsm.model.Location;
//import symbolic.execution.ffsm.model.TimerActionOutput;

//JDA: Added to represent a state reached after an error
public class SymbolicStateError extends SymbolicState {

	public SymbolicStateError(SymbolicState treeState) {
		super(treeState);
		location = new Location("Error");
	}

}
