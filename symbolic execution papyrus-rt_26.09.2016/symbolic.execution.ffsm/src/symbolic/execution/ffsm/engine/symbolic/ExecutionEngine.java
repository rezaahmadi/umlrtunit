package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
//import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JOptionPane;

//import org.w3c.dom.traversal.NodeIterator;

//import symbolic.execution.constraints.choco.ConstraintChoco;
import symbolic.execution.constraints.choco.LogicalConstraintImpl;
//import symbolic.execution.constraints.choco.RelationalConstraintImpl;
import symbolic.execution.constraints.choco.SolverImpl;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
//import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
import symbolic.execution.ffsm.engine.tree.Arc;
//import symbolic.execution.ffsm.functions.NoOutputFunction;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.Assignments;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.GuardFunction;
//import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;
//import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.OutputSequence;
import symbolic.execution.ffsm.model.TimerActionInput;
import symbolic.execution.ffsm.model.TimerActionOutput;
import symbolic.execution.ffsm.model.Transition;
//import symbolic.execution.ffsm.engine.concrete.InputActionExecution;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import symbolic.execution.utils.Pair;
import symbolic.execution.code.model.cpp.LookupConstraints;

/* ==============================================================================================
History:
	Bugs 2: April 17, 1016. 'variableMap' now used to keep track of variables for symbolic
	outputs.
================================================================================================ */
public class ExecutionEngine {
	private boolean newVersion = ExecutionOptions.getNewExecution();
	private boolean checkDepth = ExecutionOptions.getCheckDepth();
	private boolean newNameGeneration = ExecutionOptions.getNewNameGeneration();
	private static final False ff = SymbolicFactory.createFalse();
	private static final True tt = SymbolicFactory.createTrue();
	//JDA: Bug 15. 15.Wrong ID of First State. Changed from 0L to 1L.
	long nextId = 1L;
	int numberOfErrorNodes = 0;
	int numberOfUnknownNodes = 0;
	//JDA: Bug 18. Record the number of subsumed nodes.
	int numberOfSubsumedNodes = 0;
	
	private FunctionalFiniteStateMachine ffsm;
	
	private Set<NodeSymbolic> explored;
	private List<NodeSymbolic> toExplore;
	
	public ExecutionEngine(FunctionalFiniteStateMachine ffsm){
		this.ffsm = ffsm;
		LookupConstraints.reset();
		explored = new HashSet<NodeSymbolic>();
		toExplore = new ArrayList<NodeSymbolic>();
	}
	
	public SymbolicExecutionTree execute(){
		newVersion = ExecutionOptions.getNewExecution();
		maxDepth = ExecutionOptions.getMaxDepth();
		newNameGeneration = ExecutionOptions.getNewNameGeneration();
		SymbolicExecutionTree tree = new SymbolicExecutionTree(ffsm);
		NodeSymbolic root = new NodeSymbolic(new SymbolicState(ffsm.getInitialLocation(), 
				new ValuationSymbolic(ffsm.getLocationVariables(), ffsm.getInitialValuation()), tree));
		SymbolicExecutionTree resultTree = execute(root, tree, false);
		return resultTree;
		
	}
	
	public SymbolicExecutionTree execute(SymbolicState root, List<SymbolicState> exploredStates){
		newVersion = ExecutionOptions.getNewExecution();
		maxDepth = ExecutionOptions.getMaxDepth();
		//add explored states
		for (SymbolicState state : exploredStates){
			explored.add(new NodeSymbolic(state));
		}
		
		//executed symbolic
		SymbolicExecutionTree tree = new SymbolicExecutionTree(ffsm);
		root.setTree(tree);
		
		NodeSymbolic rootNode = new NodeSymbolic(root);
		
		return execute(rootNode, tree, true);
	}
	
	//JDA: methods to obtain the number of error and unknown nodes.
	public int getNumberOfErrorNodes() {
		return numberOfErrorNodes;
	}
	public int getNumberOfUnknownNodes() {
		return numberOfUnknownNodes;
	}
	//Bug 18. Number of subsumed nodes.
	public int getNumberOfSubsumedNodes() {
		return numberOfSubsumedNodes;
	}
	
	/* =========================================================================
	 Dealing with missed actions
	 
	 An input action, ai, can be missed at a given state, s, for two reasons.
	 
	 1. Either there are no outgoing transitions from s corresponding to
	 ai. In the first case, we call the action 'missed' in the second, we call
	 it 'aborted'.
	 
	 2. Or there is such an outgoing transition but all the relevant guards are
	 false.
	 
	 We regard both cases as errors. The latter case is conditional. The specific
	 path condition for the latter is that all guards are false.
	========================================================================= */
	
	/* ==========================================================================
	  JDA: setupUnhandledActions
	  
	  This method initializes "unhandledActions" with respect to a node.
	       
	  Note that the action items that represent timers are handled
	  differently. A timer cannot fire if it has not been set.
	  
	  Note also the way the code identifies whether an action item represents a
	  timer.
	 ============================================================================*/
	private void setupUnhandledActions(
			HashMap<ActionInput, Boolean> unhandledActions, NodeSymbolic n)
	{
		//Set<ActionInput> ffsm_ais = ffsm.getActionsInput();
		Set<ActionInput> ais = ffsm.getActionsInput();
		boolean result = false;
		
		for (ActionInput ai: ais) {
			result = true;
			try {
				//JDA: The following will throw an exception if 'ai'
				//     is not a timer action input.
				TimerActionInput tai = (TimerActionInput)ai;
				//JDA: So ai is an timer action input.
				TimerActionOutput tao = isTimerSet(n, tai);
				result = false;
				if (tao != null) {
					if (n.getContents().setTimers.contains(tao)) {
						// A timer can only be missed if it is set.
						//System.out.println("JDA: the timer, [[" + tao + "]], for [[" + tai +"]] is set. It could be missed.");
						result = true;
					}
					else {
						//System.out.println("JDA: the timer, [[" + tao + "]], for [[" + tai +"]] is unexpectedly not set!");
						throw new InvalidPropertiesFormatException(
								"JDA: the timer, [[" + tao + "]], for [[" + tai +"]] was unexpectedly not set!");
					}
				}
				else {
					//System.out.println("JDA: the timer, [[" + tai +"]] is not set. It could not be missed.");
					result = false;
				}
			}
			catch (Exception e) {
				//System.out.println("JDA: type cast exception for [[" + ai +  "]] is not a problem");
				//JDA: 'ai' is not a timer action input.
				if (n instanceof NodeSymbolicError) {
					result = false;
				}
				if (n.getContents().ingoingArc == null){
					//JDA: The given node is the initial node. No true input can occur.
					result = false;
				}
				//System.out.print("");
			}
			unhandledActions.put(ai, result);
		}
		//JDA: end -- for (ActionInput ai: ais)
		//System.out.println("JDA: ExecutionEngine.addConstraints: unhandledActions: [["+ 
		//		unhandledActions + "]].");	
	}
	
	/* =========================================================================
	  JDA: processUnhandledActions
	  
	  This method creates a transition to an Error state for each action that
	  is completely unhandled.
	 ==========================================================================*/
	private void processUnhandledActions(
			HashMap<ActionInput, Boolean> unhandledActions,
			NodeSymbolic node,
			SymbolicExecutionTree tree,
			boolean isNewExec,
			boolean isError) {
		//System.out.print("");
		for (ActionInput ai: unhandledActions.keySet()) {
			if (unhandledActions.get(ai) == true) {
				Map<ActionVariableOutput<?>, Expression> inputMapping = new HashMap<ActionVariableOutput<?>, Expression>();
				Map<SymbolicVariable, Expression> symbolicVarMapping = new  HashMap<SymbolicVariable, Expression>();
				//JDA: We must distinguish timeouts from ordinary input.
				ActionInputSymbolic inputExec;
				if (ai instanceof TimerActionInput) {
					inputExec = new ActionInputTimeout(ai, true);
				}
				else {
					inputExec = new ActionInputSymbolic(ai, inputMapping, symbolicVarMapping, variableMap);
				}
				//JDA: End modification.
				Transition transition;
				ArcSymbolic arcS;
				//JDA: new: use of 'newVersion'.
				
				if (isError) {
					if (newVersion) {
						SymbolicStateError sError = new SymbolicStateError(node.getContents());
						NodeSymbolicError newNode = new NodeSymbolicError(sError);
						transition = new Transition("Error");
						SymbolicTransition st = new SymbolicTransition(transition, inputExec, tree);
						arcS = new ArcSymbolic(st, node, newNode);
						node.addChild(arcS);
						sError.setIngoingArc(arcS);
						//System.out.println("JDA: newNode.getContents:" + newNode.getContents());
						toExplore.add(newNode);
					}
				}
				else if (checkDepth) {
					SymbolicStateUnknown sUnknown = new SymbolicStateUnknown(node.getContents());
					NodeSymbolicUnknown newNode = new NodeSymbolicUnknown(sUnknown);
					transition = new Transition("Unknown");
					SymbolicTransition st = new SymbolicTransition(transition, inputExec, tree);
					arcS = new ArcSymbolic(st, node, newNode);
					node.addChild(arcS);
					sUnknown.setIngoingArc(arcS);
					//System.out.println("JDA: newNode.getContents:" + newNode.getContents());
					toExplore.add(newNode);
				}
			}
		}
	}
	
	/* =========================================================================
	 JDA: updateUnhandledActions
	 
	 This method changes the value of unhandledActions for the given action
	 input to the given boolean value.
	=============================================================================*/ 
	private void updateUnhandledActions(
			HashMap<ActionInput, Boolean> unhandledActions, ActionInput ai, boolean val) {
		unhandledActions.put(ai, val);
	}
	
	/* =========================================================================
	 The String in the domain of 'variableMap will be used by 'ActionInputSymbolic'
	 to identify those symbolic inputs that should be have the same symbolic variable.
	 This is enforced by 'ActionInputSymbolic'.
	=============================================================================*/
	private HashMap<String, SymbolicVariable> variableMap;
	
	/* =========================================================================
	 The variable map needs to reset each time processing of a new node in the
	 SET is to be processed.
	=============================================================================*/
	private void resetVariableMap() {
		if (!newNameGeneration) {
			variableMap = null;
			return;
		}
		variableMap = new HashMap<String, SymbolicVariable>();
	}
	
	/* =========================================================================
	 JDA: processAbortedActions
	 
	 This method creates error transitions for all possible aborted actions.
	 Recall that aborted actions are actions that are partially handled, but
	 do not result in a transition because all guards are false.
	=============================================================================*/ 
	private void processAbortedActions(
			NodeSymbolic exploringNode,
			//HashMap<String, ArrayList<Constraint>> abortedActionsPCs,
			SymbolicExecutionTree tree
			) {
		if (!newVersion) {
			return;
		}
		List<Arc<SymbolicTransition, SymbolicState>> arcs = exploringNode.getChildren();
		int initialPCSize = exploringNode.getContents().getPC().size();
		//JDA: When the following map is filled in, each mapped name will be a symbolic
		//     input from the initial node, 'exploringNode'. Each such name, 'input', will be
		//     mapped by the procedure the list of constraints whose conjunction
		//     is the constraint that 'input' will not be taken. In other words,
		//     that 'input' will be aborted because all guards will be false.
		HashMap<String, List<Constraint>> inputConstraints = new HashMap<String, List<Constraint>>();
		HashMap<String, SymbolicState> symbolicStateMap = new HashMap<String, SymbolicState>();
		//HashMap<String, String> portNameMap = new HashMap<String, String>();
		//HashMap<String, String> signalNameMap = new HashMap<String, String>();
		
		// JDA: Step 1: Go through outgoing arcs from the initial node, 'exploringNode', and
		//              create the condition for the arc not being taken. Each such arc will have
		//              a unique symbolic input name, 'name'. Add the generated condition
		//              to accumulating list of all such conditions for 'name'.
		for (Arc<SymbolicTransition, SymbolicState> arc: arcs) {
			SymbolicTransition transition = arc.getContents();
			List<Constraint> pcList = arc.getTarget().getContents().getPC();
			String actionName = transition.getInputExecution().getAction().getName();
			if (!symbolicStateMap.keySet().contains(actionName)) {
				//portNameMap.put(actionName, transition.getInputExecution().getAction().getPortName());
				//signalNameMap.put(actionName, transition.getInputExecution().getAction().getSignalName());
				symbolicStateMap.put(actionName, arc.getTarget().getContents());
			}
			//JDA: 'constraint' will be set to the conjunction of all *new*
			//     path conditions for 'arc' to be taken. This is logical
			//     negation for the condition for the arc to be taken.
			Constraint constraint = tt;
			for (int i=initialPCSize; i < pcList.size(); i++) {
				Constraint con = pcList.get(i);
				if (constraint == tt) {
					constraint = con;
					//System.out.println("ExecutionEngine.processAbortedActions: constraint: " + constraint + ".");
				}
				else {
					constraint = new LogicalConstraintImpl(constraint, LogicalOperators.and, con);
					//System.out.println("ExecutionEngine.processAbortedActions: constraint: " + constraint + ".");
				}
			}
			// Now we need to take the logical negation to create the condition for the
			// arc to be taken.
			if (constraint == tt) {
				constraint = ff;
			}
			else {
			    constraint = new LogicalConstraintImpl(null, LogicalOperators.not, constraint);
			}
			//System.out.print("");
			// JDA: Now we need to add 'constraint' to the list of conditions for the given
			//      action, 'actionName' not to be taken.
			if (inputConstraints.containsKey(actionName)) {
				List<Constraint> lc = inputConstraints.get(actionName);
				lc.add(constraint);
			}
			else {
				ArrayList<Constraint> lc = new ArrayList<Constraint>();
				lc.add(constraint);
				inputConstraints.put(actionName, lc); 
			}
		    //JDA: Now we have the condition for each possible input, that
		    //     the input will be aborted because all of its guards are
		    //     false. We need to produce and error transitions for each
			//     of these.
		}
		
		//JDA: Step 2: Now we need to process the abort condition for each
		//             possible input to produce the corresponding
		//             error transition if the abort condition is
		//             together with the original path conditions
		//             are satisfiable.
		for (String aName: inputConstraints.keySet()) {
				SymbolicStateError sError = new SymbolicStateError(exploringNode.getContents());
				NodeSymbolicError newNode = new NodeSymbolicError(sError);
				
				SymbolicState contents = newNode.getContents();
				//List<Constraint> nPC = contents.getPC();
				List<Constraint> constraints = contents.getPC();
				for (Constraint con: constraints) {
					if (con.equals(tt)) {
						constraints.remove(con);
					}
				}
				for (Constraint con: inputConstraints.get(aName)) {
					newNode.getContents().addPCConstraint(con);
				}
				SolverImpl solver = new SolverImpl();
				List<Constraint> theConstraints = newNode.getContents().getPC();
				if (solver.isSolvable(theConstraints)) {
					//System.out.println("JDA: theConstraints, [" + theConstraints + "] are solvable.");
					Transition transition = new Transition("Error");
					
					Set<ActionVariableInput<?>> variables;
					SymbolicState state = symbolicStateMap.get(aName);
					String portName = state.ingoingArc.getContents().getInputExecution().getAction().getPortName();
					String signalName = state.ingoingArc.getContents().getInputExecution().getAction().getSignalName();
					variables = state.ingoingArc.getContents().getInputExecution().getAction().getVars();
					ActionInput ai = new ActionInput(portName, signalName, variables);
					//JDA: added variableMap. Bug 2.
					ActionInputSymbolic inputExec = new ActionInputSymbolic(ai, false, variableMap);
					SymbolicTransition st = new SymbolicTransition(transition, inputExec, tree);
					ArcSymbolic arcS = new ArcSymbolic(st, exploringNode, newNode);
					exploringNode.addChild(arcS);
					sError.setIngoingArc(arcS);
					//System.out.println("JDA: ExecutionEngine.processAbortedActions: newNode.getContents:" + newNode.getContents());
					toExplore.add(newNode);
			}
			else {
				//System.out.println("JDA: theConstraints, [" + theConstraints + "] are not solvable.");
			}
		}
		//System.out.print("");
		//JDA: Now we are done
		
	}
	
	//JDA: Control of exploration depth.
	int maxDepth = 30;
	
	/* ====================================================================================================
	 * JDA: ExecutionEngine.execute
	 *      This performs the symbolic execution.
	 *      
	 ======================================================================================================= */
	private SymbolicExecutionTree execute(NodeSymbolic root, SymbolicExecutionTree tree, boolean isNewExec){
		//generate root of the tree from initial state
		
		//JDA: begin addition. 'newVersion' will be true if the execution options
		//     are configured to use the new execution model (i.e., to generate
		//     Error and Unknown Transitions when appropriate.
		newVersion = ExecutionOptions.getNewExecution();
		maxDepth = ExecutionOptions.getMaxDepth();
		newNameGeneration = ExecutionOptions.getNewNameGeneration();
		//JDA: Deleted the following. Bug 2.
		//ActionInputSuffixGenerator.reset();
		//JDA: end addition.
		tree.setRoot(root);
		
		numberOfErrorNodes = 0;
		numberOfUnknownNodes = 0;
		toExplore.add(root);
		root.setDepth(0);
		//int i=0; //JDA: i no longer serves a purpose.
		int currentDepth = 0;
		//JDA: end new code.
		while(!toExplore.isEmpty()){
			resetVariableMap();
			NodeSymbolic exploringNode = toExplore.remove(0);
			//System.out.println("JDA: ExecutionEngine.execute: Executing symbolic: " + exploringNode);			
			//if not already explored
			System.out.print("\n" + exploringNode.getContents().getID() + " [0]");
			if (!explored.contains(exploringNode)){
				System.out.print("[0a]");
				/* -------------- */
				if (!isNew(exploringNode, true)) {
					//TODO: JDA: Can this situation occur?
					System.out.println("The node to be explored is not new!");
				}
				System.out.print("[1]");
				/* -------------*/
				explored.add(exploringNode);
				//JDA: This 'if' is new.
				if (exploringNode instanceof NodeSymbolicError) {
					System.out.println("[1a Error Node]");
					//System.out.println("JDA: NodeSymbolicError");
					//JDA: the following is new.
					numberOfErrorNodes++;
					exploringNode.getContents().setID(-1L);
					//System.out.print("[1a]");
					continue;
				}
				if (exploringNode instanceof NodeSymbolicUnknown) {
					//System.out.println("JDA: NodeSymbolicUnknown");
					//JDA: the following is new
					numberOfUnknownNodes++;
					exploringNode.getContents().setID(-2L);
					//System.out.print("[1b]");
					continue;
				}
				System.out.print("[2]");
				//JDA: we no longer set the ID here
				//exploringNode.getContents().setID(nextId);
				List<NodeSymbolic> subsumed = exploringNode.subsumedNodes;
				//JDA: we need to remember the ID for when we process subsumption.
				long myID = exploringNode.getContents().getID();
				System.out.print("[3...");
				for (int j = 0; j<  exploringNode.subsumedNodes.size(); j++) {
					//subsumed.get(j).getContents().setID(nextId);
					subsumed.get(j).getContents().setSubsumedByID(myID);
				}
				System.out.print("...3]");
				//JDA: we no longer set the ID here.
				//nextId++;
				SymbolicState exploringState = exploringNode.getContents();
				//System.out.println("JDA: Exploring " + exploringState);
				boolean isTimerTriggered = false;	
				
				HashMap<ActionInput, Boolean> unhandledActions = new HashMap<ActionInput, Boolean>(); 
				setupUnhandledActions(unhandledActions,exploringNode);
				//System.out.println("JDA: SymbolicExecutionTree.execute: Ready to check out each outgoing transition.");
				
				//JDA: New code.
				System.out.print("[4]");
				if (checkDepth) {
					System.out.print("[4a]");
					currentDepth = exploringNode.getDepth();
					if (currentDepth >= maxDepth) {
						System.out.print("[4aa]");
						//System.out.println("JDA: Abandoning node at depth, " + maxDepth + ".");
						processUnhandledActions(unhandledActions, exploringNode, tree, true,false);
						continue; //JDA: With the next symbolic node (which may be at a lower depth).
					}
				}
				List<Transition> lt = ffsm.getOutgoingTransitions(exploringState.getLocation());
				System.out.print("[5...");
				for (Transition t : lt) {
					if (t.getInputAction().getClass().getName().equals("symbolic.execution.umlrt.model.DefaultActionInput")) {
						//JDA: Note that the initial action could be unhandled if it always terminates with a false
						//JDA: 'assert' statement.
						updateUnhandledActions(unhandledActions, t.getInputAction(), true);
					}
					if (isTimerTriggered) {
						//JDA: Note: Other transitions could come first even
						//     if they occur to the right in the transition list, 'lt'.
						isTimerTriggered = false;
						//JDA: end replacement
					}
					//create execution of input action
					ActionInputSymbolic inputExec = null;
					TimerActionOutput timer = null;
					System.out.print("[5.1]");
					if(t.getInputAction() instanceof TimerActionInput){
						System.out.print("[5.1a]");
						inputExec= new ActionInputTimeout(tree.mapInputAction(t.getInputAction()),isNewExec);
						timer = isTimerSet(exploringNode, (TimerActionInput)t.getInputAction()); 
						if (timer == null){
							//JDA: This timer transition will not occur. Continue with the next transition.
							System.out.print("[5.1aa]");
							continue;
						} else {
							System.out.print("[5.1ab]");
							isTimerTriggered = true;
						}
					} else {
						System.out.print("[5.1b]");
						inputExec = new ActionInputSymbolic(tree.mapInputAction(t.getInputAction()), isNewExec, variableMap);
					}
					System.out.print("[5.2]");
					//evaluate guard
					SymbolicState afterGuardState = evaluateGuardSymbolic(t, exploringState, inputExec);
					System.out.print("[5.3]");
					if(afterGuardState == null){
						//System.out.println("JDA: DB--> aborted. Guard false");
						continue;
					}
					Map<ActionOutput,ActionOutput> mao2ao = tree.getActionsOutputMap();
					//TODO: JDA: the following can take a long time.
					System.out.print("[5.4]");
					List<Pair<SymbolicState, OutputSequenceSymbolic>> afterOutputStates = evaluateOutputSymbolic(t, afterGuardState, inputExec, mao2ao);
					//JDA: The following (I hope) allows for unhandled actions introduced by 'assert' statements.
					if ((afterOutputStates != null) && (afterOutputStates.size() != 0)) {
						System.out.print("[5.4a]");
						updateUnhandledActions(unhandledActions, t.getInputAction(), false);
					}
					else {
						System.out.print("[5.4b]");
						//System.out.println("JDA: DB--> aborted. evaluateOutputSymbolic");
					}
					System.out.print("[5.5...");
					for (Pair<SymbolicState, OutputSequenceSymbolic> afterOutputState : afterOutputStates ) {
						//evaluate update
						SymbolicState sState = afterOutputState.getFirst();
						//System.out.print("[7a...");
						//TODO: JDA: The following can take a long time.
						System.out.print("[5.5.1]");
						List<SymbolicState> afterUpdateStates2 = evaluateUpdateSymbolic(t,sState, inputExec);
						if (afterUpdateStates2.isEmpty()) {
							System.out.print("[5.5.1a]");
							//System.out.println("JDA: DB--> aborted. evaluateUpdateSymbolic");
						}
						//System.out.print("7a]");
						//System.out.print("[8...");
						System.out.print("[5.5.2...");
						for (SymbolicState afterUpdateState2 : afterUpdateStates2){
							//System.out.print("[8a].");
							//System.out.println("JDA: afterUpdateSate2: [" + afterUpdateState2 + "].");
							//System.out.println("JDA: ExecutionEngine.execute: afterOutputState 2:" + afterOutputState);
							//if necessary remove set timer
							if (timer != null) {
								//JDA: The following only removes the first instance of 'timer' in the
								//     timer set associated with 'afterUpdateState'.
								//     The SE doesn't care what the times remaining in a set timer are. It
								//     assumes that a set timer may happen at any time.
								afterUpdateState2.removeTimerSet(timer);
								//System.out.println("JDA: afterUpdateSate2 after removeTimerSet(" + timer + "): [" + afterUpdateState2 + "].");
							}
							System.out.print("[5.5.2.1]");
							//System.out.print("[8b].");
							//create symbolic transition
							SymbolicTransition symbolicTransition = new SymbolicTransition(t,inputExec, afterUpdateState2.getTree());
							OutputSequenceSymbolic oss = afterOutputState.getSecond();
							symbolicTransition.setOutputSequence(oss);
							//add set timers
							OutputSequenceSymbolic oss2 = symbolicTransition.getOuputSequence();
							afterUpdateState2.addSetTimers(oss2);
							//System.out.println("JDA: afterUpdateSate after addSetTimers: [" + afterOutputState + "].");
							//create symbolic node in the tree
							NodeSymbolic targetNode = new NodeSymbolic(afterUpdateState2);
							//add this child to a node
							ArcSymbolic arc = exploringNode.addChild(targetNode, symbolicTransition);
							//set transition in state
							afterUpdateState2.setIngoingArc(arc);
							//System.out.print("[8c].");
							//JDA: Here's where we now set a nodes ID.
							targetNode.getContents().setID(nextId);
							nextId++;
							System.out.print("[5.5.2.2]");
							if (isNew(targetNode, true)){
								System.out.print("[5.5.2.2a]");
								//System.out.println("newNode (contents):" + newState);
								targetNode.setDepth(currentDepth + 1);
								//System.out.println("ExecutionEngine.execute depth of newNode:" + (currentDepth + 1));
								toExplore.add(targetNode);
								try {
								    afterUpdateState2.getLocation().explore(ffsm);
								}
								catch(java.lang.ArithmeticException ex) {
									System.out.print("ArithmeticException");
								}
								tree.addLocation(afterUpdateState2.getLocation());
							}
							else {//targetNode is not new.
								System.out.print("[5.5.2.2b]");
								//JDA The following ensures that subsumption is identified.
								targetNode.getContents().setSubsumedByID(targetNode.getSubsumedBy().getContents().getID());
								//JDA: Bug 18. Record the number of subsumed nodes.
								numberOfSubsumedNodes++;
								//System.out.println("JDA: ExecutionEngine.execute: target targetNode, [[" + targetNode + "]] is not new.");
							}
							System.out.print("[5.5.2.3]");
							//System.out.print("[8d].");
							//JDA: end - if (isNew(targetNode))
						}
						System.out.print("...5.5.2]");
						//JDA end -- for (SymbolicState afterUpdateState : afterUpdateStates)
					}
					System.out.print("...5.5]");
					//JDA: end -- for (Pair<SymbolicState, OutputSequenceSymbolic> afterOutputState : afterOutputStates )
				}
				System.out.print("...5]");
				//JDA: end -- for (Transition t : lt)
				if (newVersion) {
					System.out.print("[6a1]");
					processUnhandledActions(unhandledActions, exploringNode, tree, true,true);
					System.out.print("[6a2]");
					processAbortedActions(exploringNode, tree);
					System.out.print("[6a3]");
				}
			}
			else {
				System.out.print("[0b]");
				JOptionPane.showMessageDialog(null, "Node: The node to be explored, [[" + exploringNode +
						                               "]], has already been explored.", "Unexpected Node!"
						, JOptionPane.ERROR_MESSAGE);
				//System.out.println("JDA: ExecutionEngine.execute: The node, [[" + exploringNode + "]], has already been explored.");
			}
			System.out.println("[DONE]");
			//JDA: end -- if (!explored.contains(node))		
		}
		//JDA: end -- while(!toExplore.isEmpty())
		//JDA: Nothing more to explore, so we're done.
		return tree;				
	}

	private TimerActionOutput isTimerSet(NodeSymbolic node, TimerActionInput action) {		
		TimerActionOutput timer = node.getContents().containSetTimer(action);
			if (timer != null){				
				return timer;
			}
		
		return null;
	}

	private boolean isNew(NodeSymbolic newNode, boolean subsumingEnabled) {
		if (!subsumingEnabled)
			return true;
		for (NodeSymbolic node : explored){
            if (node instanceof NodeSymbolicError) {
            	continue;
            }
			if (node.getContents().subsumes(newNode.getContents())){
				newNode.setSubsumedBy(node);
				node.addSubsumed(newNode);
				//JDA: The following is to ensure that subsumption is correctly identified.
				newNode.getContents().setSubsumedByID(node.getContents().getID());
				//System.out.println(newNode.getFirstContents() + " NOT NEW");
				return false;
			}
		}
		for (NodeSymbolic node : toExplore){
			if (node instanceof NodeSymbolicError) {
            	continue;
            }
			if (node.getContents().subsumes(newNode.getContents())){
				//System.out.println(newNode.getFirstContents() + " NOT NEW");
				newNode.setSubsumedBy(node);
				//JDA: The following is to ensure that subsumption is correctly identified.
				newNode.getContents().setSubsumedByID(node.getContents().getID());
				node.addSubsumed(newNode);
				return false;
			}
		}
		return true;
	}

	private List<SymbolicState> evaluateUpdateSymbolic(Transition t,
			SymbolicState state, ActionInputSymbolic inputExec) {
		//System.out.println("update - START");
		
		//System.out.print("[eu0]");
		List<SymbolicState> result = new ArrayList<SymbolicState>();
		
		//System.out.println("Evaluate update " + state);
		
		//List<Pair<Set<Constraint>, Assignments>> constrainedValuations = 
		//	t.getUf().evaluateSymbolic(state.getValuation(), state.getPC(), inputExec.getConstraints());
		
		//create replacements
		//System.out.print("[eu1]");
		Map<SymbolicVariable,Expression> replacement = new HashMap<SymbolicVariable, Expression>();
		//System.out.print("[eu2]");
		replacement.putAll(state.getValuation().getSymbolicVariables());
		//System.out.print("[eu3]");
		replacement.putAll(inputExec.getMappingSymbolic());
		//System.out.print("[eu4]");
		List<Pair<Set<Constraint>, Assignments>> constrainedValuations = 
				t.getUf().evaluateSymbolic(state.getPC(), replacement);
		
		//for each results
		//System.out.print("[eu5...");
		for(Pair<Set<Constraint>, Assignments> item : constrainedValuations){
			//constraint
			//System.out.print("[eu5a]");
			Set<Constraint> caseConstraints = item.getFirst();
			//System.out.println("** case constraint : " + caseConstraints);
			
			Constraint first = caseConstraints.iterator().next();
			//if is possible
			//System.out.print("[eu5b]");
			if (!first.equals(ff)){
				//create copy of new state
				SymbolicState newState = new SymbolicState(state.getLocation(), state.getValuation(), state.getPC(), state.getTree(),state.getTimers());
						
				//if there are constrained must add them to the PC
				if(!first.equals(tt)){
					//System.out.print("[eu6...");
					for(Constraint caseConstraint : caseConstraints){
						if (ExecutionOptions.getCheckForDuplicatePCs()) {
							if (!caseConstraint.equals(tt) && !caseConstraint.equals(ff)) {
								if (!newState.pathCondition.contains(caseConstraint)) {
									newState.addPCConstraint(caseConstraint);
								}
								else {
									//System.out.println("JDA: ExecutionEngine.evaluateUpdateSymbolic: caseConstraint, [" +
									//		caseConstraint + "]," + " is already in newState.");
								}
							}
						}
						else {
							if (!caseConstraint.equals(tt) && !caseConstraint.equals(ff)) {
								newState.addPCConstraint(caseConstraint);
							}
						}
					}
					//System.out.print("eu6]");
				}
						
				//update valuation
				Assignments updates = item.getSecond();
				//System.out.print("[eu7...");
				for (LocationVariable<?> locVar : updates.getLocationVariables()){
					if (!locVar.isObject()) {
						Expression updatedLocalVar = updates.getValue(locVar); 
				
						Expression replaced = updatedLocalVar.replaceVariables(replacement);				
						newState.getValuation().setValue(locVar, replaced);
					}
				}
				//System.out.print("eu7]");
				SolverImpl solver = new SolverImpl();
				//System.out.print("[eu8...");
				if (ExecutionOptions.getCheckSolvability()) {
					//System.out.print("eu8]");
					//System.out.print("[eu9...");
					if (solver.isSolvable(newState.getPC())) {
						//System.out.print("eu9]");
						result.add(newState);
					}
					else {
						//System.out.print("eu9]");
						//System.out.println("JDA: ExecutionEngine.evaluateUpdateSymbolic: newState, [" +
						//		newState + "]," + " is inconsistent.");
					}
				}
				else {
					//System.out.print("eu8]");
					result.add(newState);
				}
			}
		}
		//System.out.print("eu5]");
		//System.out.println("update - END");
		
		return result;
	}

	private List<Pair<SymbolicState, OutputSequenceSymbolic>> evaluateOutputSymbolic(
			Transition t, SymbolicState state, ActionInputSymbolic inputExec, Map<ActionOutput,ActionOutput> actionsMap) {
		
		//System.out.println("Evaluating output - START");
		
		List<Pair<SymbolicState, OutputSequenceSymbolic>> sequence = new ArrayList<Pair<SymbolicState,OutputSequenceSymbolic>>();
		
		
		//create replacements
		Map<SymbolicVariable,Expression> replacement = new HashMap<SymbolicVariable, Expression>();
		replacement.putAll(state.getValuation().getSymbolicVariables());
		replacement.putAll(inputExec.getMappingSymbolic());
		
		List<Constraint> pcReplaced = replacePC(state.getPC(), replacement);
		
		//evaluate function
		//List<Pair<Set<Constraint>, OutputSequence>> constrainedSequences = 
		//	t.getOf().evaluateSymbolic(state.getValuation(), state.getPC(), inputExec.getConstraints());
		//JDA: changed the following for ease of debugging
		
		OutputFunction of = t.getOf();
		//System.out.println("JDA: ExecutionEngine.evaluateOutputSymbolic: of =[" + of + "].");
		List<Pair<Set<Constraint>, OutputSequence>> constrainedSequences = 
			of.evaluateSymbolic(pcReplaced, replacement);
		//System.out.println("JDA: ExecutionEngine.evaluateOutputSymbolic: constrainedSequences =[" + constrainedSequences + "].");
		for (Pair<Set<Constraint>,OutputSequence> constrainedSequence : constrainedSequences){
			//get case
			//System.out.println("JDA: ExecutionEngine.evaluateOutputSymbolic: constrainedSequence =[" + constrainedSequence + "].");
			Set<Constraint> caseConstraints = constrainedSequence.getFirst();
						
			Constraint first = caseConstraints.iterator().next();
			//System.out.println("JDA: ExecutionEngine.evaluateOutputSymbolic: first =[" + first + "].");
			//System.out.println("** case constraint : " + caseConstraints);
			
			//if solution possible
			if (!first.equals(ff)){			
				SymbolicState newState = new SymbolicState(state.getLocation(), state.getValuation(), state.getPC(), state.getTree(), state.getTimers());
						
				//if there are constrained must add them to the PC
				if(!first.equals(tt)){
					//create constraint
					for(Constraint cst : caseConstraints){						
						//System.out.println("** case constraint : " + replaced);
						//System.out.println("JDA: ExecutionEngine.evaluateOutputSymbolic: cst =[" + cst + "].");
						if (!cst.equals((tt)) && !cst.equals(ff));
								newState.addPCConstraint(cst);
					}
				}
			
				//must create an execution from output sequence - replace local vars and input vars
				OutputSequence os = constrainedSequence.getSecond();
			
				//replace location variables with expressions
				OutputSequenceSymbolic sos = new OutputSequenceSymbolic(os, replacement, actionsMap); 
			
				sequence.add(new Pair<SymbolicState, OutputSequenceSymbolic>(newState, sos));
			}
		}
		
		//System.out.println("Evaluating output - END");
		return sequence;
	}

	private List<Constraint> replacePC(List<Constraint> pc,
			Map<SymbolicVariable, Expression> replacement) {
		List<Constraint> result = new ArrayList<Constraint>();
		
		for (Constraint cst : pc){
			result.add((Constraint)cst.replaceVariables(replacement));
		}
		return result;
	}

	private SymbolicState evaluateGuardSymbolic(Transition t, SymbolicState state, ActionInputSymbolic inputExec) {
		SymbolicState newState = null;
		
		//System.out.println("Guard");
		
		//create replacements
		Map<SymbolicVariable,Expression> replacement = new HashMap<SymbolicVariable, Expression>();
		replacement.putAll(state.getValuation().getSymbolicVariables());
		replacement.putAll(inputExec.getMappingSymbolic());
		
		GuardFunction gf = t.getGf();
		List<Constraint> pc = state.getPC();
		List<Constraint> guards = gf.evaluateSymbolic(pc, replacement);
		//JDA: end modification
		
		//List<Constraint> guards = t.getGf().evaluateSymbolic(state.getValuation(), inputExec.getConstraints(), state.getPC());		
				
		Constraint first = guards.get(0);
		if (! first.equals(ff)){
			//create new symbolic state
			newState = new SymbolicState(t.getTarget(), state.getValuation(), state.getPC(), state.getTree(), state.getTimers());
			
			if (! first.equals(tt)){
												
				//replace constraints and add to pc
				for (Constraint guard : guards){
					//Constraint replaced = (Constraint) guard.replaceVariables(replacement);
					newState.addPCConstraint(guard);					
				}				
			} 
		} 
		return newState;
	}

}
