package symbolic.execution.ffsm.engine.symbolic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.ffsm.model.Valuation;
import symbolic.execution.ffsm.model.ModelVariable;

public class ValuationSymbolic {
	
	private Map<ModelVariable<?>, Expression> valuation;
	
	private Map<SymbolicVariable, Expression> symbolicVariableValuation;
	
	public ValuationSymbolic(){
		valuation = new HashMap<ModelVariable<?>, Expression>();
		symbolicVariableValuation = new HashMap<SymbolicVariable, Expression>();
	}
	
	public ValuationSymbolic(Set<? extends ModelVariable<?>> vars, Valuation valConcrete){
		this();
		for (ModelVariable<?> variable : vars){
			Object val = valConcrete.getValue(variable);			
			Constant cst = SymbolicFactory.createConstant(variable.getType(), val);
			valuation.put(variable, cst);
			symbolicVariableValuation.put(variable.getSymbolicVar(), cst);
		}
	}
	
	public ValuationSymbolic(ValuationSymbolic val){
		if (val==null){
			valuation = new HashMap<ModelVariable<?>, Expression>();
			
		} else {
			valuation = new HashMap<ModelVariable<?>, Expression>(val.valuation);
			symbolicVariableValuation = new HashMap<SymbolicVariable, Expression>(val.symbolicVariableValuation);
		}
	}
	

	public void setValue(ModelVariable<?> variable, Expression value){
		valuation.put(variable, value);		
		symbolicVariableValuation.put(variable.getSymbolicVar(), value);
	}

	public Expression getValue(ModelVariable<?> variable){
		return valuation.get(variable);
	}
	
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof ValuationSymbolic)){
			return false;
		}
		ValuationSymbolic otherValuation = (ValuationSymbolic) o;
		
		for (ModelVariable<?> var : valuation.keySet()){
			if (!valuation.get(var).equals(otherValuation.valuation.get(var))){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {	
		StringBuffer sb = new StringBuffer();
		int i=0;
		sb.append('{');
		int size = valuation.entrySet().size();
		for (Map.Entry<ModelVariable<?>,Expression> entry : valuation.entrySet()){
			sb.append(entry.toString());			
			i++;
			if (size>i){
				sb.append(",");
			}
			if (i%4==0){
				sb.append("\n");
			}
		}
		sb.append('}');
		return sb.toString();
	}

	public List<Constraint> createConstraints() {		
		List<Constraint> constraints = new ArrayList<Constraint>();		
		for (SymbolicVariable var : symbolicVariableValuation.keySet()){
			constraints.add(SymbolicFactory.createRelationalConstraint(var, 
					CompareOperators.eq, symbolicVariableValuation.get(var)));
		}
		return constraints;
	}

	public Set<ModelVariable<?>> getVariables() { 
		return valuation.keySet();
	}

	public Map<SymbolicVariable, Expression> getSymbolicVariables() {		
		return symbolicVariableValuation;
	}

	public ValuationSymbolic replace(Map<SymbolicVariable, SymbolicVariable> compositeMapping) {
		ValuationSymbolic newVal = new ValuationSymbolic();
		
		for (ModelVariable<?> var : valuation.keySet()){
			Expression replaced = valuation.get(var).replaceVariables(compositeMapping);
			newVal.setValue(var, replaced);
		}
		return newVal;
	}

	public String print() {
		StringBuffer sb = new StringBuffer();
		int i=0;
		sb.append('{');
		int size = valuation.entrySet().size();
		for (Map.Entry<ModelVariable<?>,Expression> entry : valuation.entrySet()){
			sb.append(entry.toString());			
			i++;
			if (size>i){
				sb.append(",");
			}
			
		}
		sb.append('}');
		return sb.toString();
	}

	public ModelVariable<?> getVariableByName(String name) {
		for (ModelVariable<?> var : valuation.keySet()){
			if (var.getName().equals(name)){
				return var;
			}
		}
		return null;
	}

}
