package symbolic.execution.ffsm.engine.symbolic;

public class SubsumptionArc extends ArcSymbolic{

	public SubsumptionArc(SymbolicTransition contents, NodeSymbolic source,
			NodeSymbolic target) {
		super(contents, source, target);
				
	}
	
	@Override
	public String toString() {		
		return "Subsumption";
	}

}
