package symbolic.execution.ffsm.engine.symbolic;

import symbolic.execution.ffsm.engine.tree.Arc;
//import symbolic.execution.ffsm.engine.tree.TreeNode;

public class ArcSymbolic extends Arc<SymbolicTransition, SymbolicState>{

	public ArcSymbolic(SymbolicTransition contents, NodeSymbolic source,
			NodeSymbolic target) {
		super(contents, source, target);
	}
	
	@Override
	public NodeSymbolic getSource() {		
		return (NodeSymbolic) super.getSource();
	}
	
	@Override
	public NodeSymbolic getTarget() {
		return (NodeSymbolic) super.getTarget();
	}

}
