package symbolic.execution.ffsm.functions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.Assignments;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.UpdateFunction;
import symbolic.execution.utils.Pair;

public class NoUpdateFunction extends UpdateFunction{
	
	public NoUpdateFunction(Set<ActionVariableInput<?>> inputs, Set<LocationVariable<?>> locations){
		super("noUpdate", inputs, locations);		
		addCase(SymbolicFactory.createTrue(), new Assignments(locations));
		
		
	}
	
	@Override
	public List<Pair<Set<Constraint>, Assignments>> evaluateSymbolic(
			ValuationSymbolic locVars, List<Constraint> inputVars,
			List<Constraint> pc) {
		
		List<Pair<Set<Constraint>, Assignments>> constrained = new ArrayList<Pair<Set<Constraint>,Assignments>>(); 
		
		
		for (Set<Constraint> caseConstraints : cases.keySet()){						
			Set<Constraint> constraintsTrue = new HashSet<Constraint>();
			constraintsTrue.add(SymbolicFactory.createTrue());
			constrained.add(new Pair<Set<Constraint>, Assignments>(caseConstraints, cases.get(caseConstraints)));
			
		}
		
		return constrained;
	}

}
