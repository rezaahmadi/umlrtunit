package symbolic.execution.ffsm.functions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.LocationVariable;
import symbolic.execution.ffsm.model.OutputFunction;
import symbolic.execution.ffsm.model.OutputSequence;
import symbolic.execution.utils.Pair;

public class NoOutputFunction extends OutputFunction{
	
	public NoOutputFunction(Set<ActionVariableInput<?>> inputs, Set<LocationVariable<?>> locations){
		super("noOutput", inputs, locations);
		OutputSequence seq = new OutputSequence();		
		addCase(SymbolicFactory.createTrue(), seq);
		
	}
	
	@Override
	public List<Pair<Set<Constraint>, OutputSequence>> evaluateSymbolic(
			List<Constraint> pcReplaced,
			Map<SymbolicVariable, Expression> replacement) {
		List<Pair<Set<Constraint>, OutputSequence>> outputs = new ArrayList<Pair<Set<Constraint>,OutputSequence>>();
		
		for (Set<Constraint> caseConstraint : cases.keySet()){
		
			Set<Constraint> constraintsTrue = new HashSet<Constraint>();
			constraintsTrue.add(SymbolicFactory.createTrue());
			outputs.add(new Pair<Set<Constraint>, OutputSequence>(caseConstraint, cases.get(caseConstraint)));
		}
		return outputs;
	}
}
