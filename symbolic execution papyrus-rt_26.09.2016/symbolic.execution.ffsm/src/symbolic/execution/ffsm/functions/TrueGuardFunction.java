package symbolic.execution.ffsm.functions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.model.ActionVariableInput;
import symbolic.execution.ffsm.model.GuardFunction;
import symbolic.execution.ffsm.model.LocationVariable;

public class TrueGuardFunction extends GuardFunction{

	public TrueGuardFunction(Set<ActionVariableInput<?>> inputs, Set<LocationVariable<?>> locations ){
		super("true", inputs, locations);
		
		addConstraint(SymbolicFactory.createTrue());
		
	}
	
	@Override
	public List<Constraint> evaluateSymbolic(List<Constraint> pc,
			Map<SymbolicVariable, Expression> replacement) {
		List<Constraint> result = new ArrayList<Constraint>();
		result.add(SymbolicFactory.createTrue());
		return result;
		
	}
}
