package symbolic.execution.utils;

public class Pair<R,S> {
	private R first;
	private S second;
	
	public Pair(R first, S second){
		this.first = first;
		this.second = second;
	}
	
	public R getFirst(){
		return first;
	}
	
	public S getSecond(){
		return second;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Pair){
			Pair p = (Pair) o;
			Object f = p.getFirst();
			Object s = p.getSecond();
			
			return f.equals(first) && s.equals(second);
		}
		return false;
	}
	
	@Override
	public String toString() {		
		return "(" + first + "," + second + ")";
	}
}
