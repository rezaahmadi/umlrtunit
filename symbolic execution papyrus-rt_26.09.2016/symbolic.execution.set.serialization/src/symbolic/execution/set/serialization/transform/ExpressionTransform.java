package symbolic.execution.set.serialization.transform;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.serialization.set.ArithmeticOperator;
import symbolic.execution.serialization.set.CompareOperators;
import symbolic.execution.serialization.set.EXPRESSION;
import symbolic.execution.serialization.set.LogicalOperators;
import symbolic.execution.serialization.set.Type;
import symbolic.execution.serialization.set.setFactory;
import symbolic.execution.serialization.set.impl.setFactoryImpl;

public class ExpressionTransform {
	private Expression expression;
	private setFactory factory;
	
	public ExpressionTransform(Expression expr){
		
		expression = expr;
		factory = new setFactoryImpl();
	}
	
	
	public EXPRESSION transform(Expression expression){
		if (expression instanceof BinaryExpression){
			return transformBinary((BinaryExpression)expression);
		} else if (expression instanceof UnaryExpression){
			return transformUnary((UnaryExpression)expression);
		} else if (expression instanceof Constraint){
			return transformConstraint((Constraint)expression);
		} else if (expression instanceof SymbolicVariable){
			return transformVariable((SymbolicVariable)expression);
		} else if (expression instanceof Constant) {
			return transformConstant((Constant) expression);
		}
		else {
			return null;
		}
	}


	private EXPRESSION transformConstant(Constant constant) {
		symbolic.execution.serialization.set.Constant result = factory.createConstant();
		
		result.setValue(constant.getValue());
		result.setStringRepresentation(constant.getValue().toString());
		
		if (constant.getType().equals(Integer.class)){
			result.setType(Type.INT);
		} else if (constant.getType().equals(Double.class)) {
			result.setType(Type.DOUBLE);
		} else {
			result.setType(Type.INT);
		}
		
		return result;
	}


	private EXPRESSION transformVariable(SymbolicVariable variable) {
		symbolic.execution.serialization.set.SymbolicVariable result = factory.createSymbolicVariable();
		result.setName(variable.getName());
		if (variable.getType().equals(Integer.class)){
			result.setType(Type.INT);
		} else if (variable.getType().equals(Double.class)) {
			result.setType(Type.DOUBLE);
		} else {
			result.setType(Type.INT);
		}
		
		
		return result;
	}


	private EXPRESSION transformConstraint(Constraint constraint) {
		if (constraint instanceof LogicalConstraint) {
			return transformLogical((LogicalConstraint)constraint);
		} else if (constraint instanceof RelationalConstraint) {
			return transformRelational((RelationalConstraint) constraint);
		} else {
			return null;
		}
	}


	private EXPRESSION transformRelational(RelationalConstraint constraint) {		
		symbolic.execution.serialization.set.RelationalConstraint result = factory.createRelationalConstraint();
		
		EXPRESSION cstLeft = transform (constraint.getLeft());
		EXPRESSION cstRight = transform (constraint.getRight());
		
		result.setLeft(cstLeft);
		result.setRight(cstRight);		
		
		switch (constraint.getOp()){
		case eq: result.setOp(CompareOperators.EQ); break;
		case geq: result.setOp(CompareOperators.GEQ); break;
		case leq: result.setOp(CompareOperators.LEQ); break;
		case neq: result.setOp(CompareOperators.NEQ); break;
		case le: result.setOp(CompareOperators.LE); break;
		case ge: result.setOp(CompareOperators.GE); break;
		}
		
		return result;
	}


	private EXPRESSION transformLogical(LogicalConstraint constraint) {
		symbolic.execution.serialization.set.LogicalConstraint result = factory.createLogicalConstraint();
		
		EXPRESSION cstLeft = transform(constraint.getLeft());
		EXPRESSION cstRight = transform(constraint.getRight());
		
		result.setLeft(cstLeft);
		result.setRight(cstRight);
		
		switch (constraint.getOp()){
		case and : result.setOp(LogicalOperators.AND); break;
		case or :  result.setOp(LogicalOperators.OR); break;
		case imply : result.setOp(LogicalOperators.IMPLY); break;
		case not :result.setOp(LogicalOperators.NOT); break;
		case xor: result.setOp(LogicalOperators.XOR); break;
		}
		return result;
	}


	private EXPRESSION transformUnary(UnaryExpression expr) {
		EXPRESSION exprLeft = transform(expr.getExpression());
		
		symbolic.execution.serialization.set.UnaryExpression result = factory.createUnaryExpression();
		result.setExpr(exprLeft);
		
		switch (expr.getOp()){
		case plus : result.setOp(ArithmeticOperator.PLUS);
		case minus : result.setOp(ArithmeticOperator.MINUS);
		}
		
		return result;
	}


	private EXPRESSION transformBinary(BinaryExpression expr) {
		EXPRESSION exprLeft = transform(expr.getLeft());
		EXPRESSION exprRight = transform(expr.getRight());
		
		symbolic.execution.serialization.set.BinaryExpression result = factory.createBinaryExpression();
		result.setLeft(exprLeft);
		result.setRight(exprRight);
		
		switch(expr.getOp()){
		case div: result.setOp(ArithmeticOperator.DIV); break;
		case minus : result.setOp(ArithmeticOperator.MINUS); break;
		case mod : result.setOp(ArithmeticOperator.MOD); break;
		case mult : result.setOp(ArithmeticOperator.MULT); break;
		case plus : result.setOp(ArithmeticOperator.PLUS); break;
		}
		
		return result;
	}

}
