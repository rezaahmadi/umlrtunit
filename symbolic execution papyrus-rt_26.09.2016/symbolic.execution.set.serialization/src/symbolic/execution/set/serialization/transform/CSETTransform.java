package symbolic.execution.set.serialization.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.OutputSequenceSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.engine.tree.Arc;
//import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.ffsm.model.TimerActionInput;
import symbolic.execution.serialization.set.CompositeState;
import symbolic.execution.serialization.set.ContainedNonCompositeState;
import symbolic.execution.serialization.set.EXPRESSION;
import symbolic.execution.serialization.set.Queue;
import symbolic.execution.serialization.set.SET;
import symbolic.execution.serialization.set.Signal;
import symbolic.execution.serialization.set.State;
//import symbolic.execution.serialization.set.TopLevelCompositeState;
import symbolic.execution.serialization.set.TopLevelNonCompositeState;
import symbolic.execution.serialization.set.Transition;
import symbolic.execution.serialization.set.VAL;
import symbolic.execution.serialization.set.input;
import symbolic.execution.serialization.set.output;
import symbolic.execution.serialization.set.setFactory;
import symbolic.execution.serialization.set.impl.setFactoryImpl;
import symbolic.execution.tree.synch.QueueManager;
import symbolic.execution.tree.synch.SynchronizationAction;
import symbolic.execution.tree.synch.UnreceivedAction;
import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.SentMessage;
//import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;
import symbolic.execution.tree.synch.model.SymbolicStateSynch;

/* ==================================================================================================
History:
    Cleanup of documentation. April 17, 2016.
 * JDA: This package was modified to display valuations and path conditions for composite states.
 * JDA: Also debugging for a crash problem that has been fixed.
===================================================================================================== */
public class CSETTransform {
	private setFactory factory;
	
	private Map<String,TopLevelNonCompositeState> statesId;
	
	private Map<NodeSymbolic, TopLevelNonCompositeState> statesNode;
	
	private static int stateCounter;
	
	private static int transCounter;
	
	public CSETTransform(){
		factory = new setFactoryImpl();
		statesId = new HashMap<String, TopLevelNonCompositeState>();
		statesNode = new HashMap<NodeSymbolic, TopLevelNonCompositeState>();
	}
	
	public SET transform(SymbolicExecutionTree containerTree){
		SET set = factory.createSET();
		
		//find root
		NodeSymbolic root = containerTree.getRoot();
		
		
		//transform root
		TopLevelNonCompositeState rootS = (TopLevelNonCompositeState) transformNode(root);
		
		//process root
		
		
		set.setRoot(rootS);
		
		return set;
		
		
	}

	private TopLevelNonCompositeState transformNode(NodeSymbolic root) {
		SymbolicState stateOriginal = root.getContents();
		//System.out.println("JDA: CSETTransform:transformNode:type of 'root': " + root.getClass().getSimpleName());
		
		//check whether stub has been created
		String id = "";
		if (statesNode.containsKey(root)){
			id = statesNode.get(root).getID();
		}
		State stateTransformed = transformState(stateOriginal, true, id, root);
		
		if (!(stateTransformed instanceof TopLevelNonCompositeState)){
			return null;
		}
		
		TopLevelNonCompositeState topLevelSource = (TopLevelNonCompositeState) stateTransformed;
		statesNode.put(root, topLevelSource);
		statesId.put(topLevelSource.getID(), topLevelSource);
		
		//transform subsumption
		transformSubsume(root, topLevelSource);
		
		for (Arc<SymbolicTransition, SymbolicState> arc : root.getChildren()){
			
			//create target node 
			//JDA DONE: The following was crashing for some reason on a "synched" tree. Added debug.
			//System.out.println("JDA: -- debug --> arc: <<" + arc + ">>. arc.getTarget: <<" + arc.getTarget() + ">>.");
			NodeSymbolic nextNode = (NodeSymbolic)arc.getTarget();
			TopLevelNonCompositeState topLevelTarget = transformNode(nextNode);
			//JDA: The following is crashing for some reason on a "synched" tree. Added debug
			//System.out.println("JDA: -- debug --> OK");
			
			//create transition
			Transition t = factory.createTransition();
			
			transformTransition(t,arc.getContents());
			
			//create trigger/output 
			transformSignals(t, arc.getContents());
			
			t.setTarget(topLevelTarget);
			topLevelSource.getSource().add(t);
		}
		
		return topLevelSource;
	}
	
	private void transformTransition(Transition t, SymbolicTransition transition) {
		//add ID
		t.setID(new Integer(transCounter++).toString());
				
		//add type
		ActionInputSymbolic trigger = transition.getInputExecution();
		if (trigger instanceof SynchronizationAction){
			t.setKind("closed");
		} else if (trigger instanceof UnreceivedAction){
			t.setKind("unrec");			
		} else {
			t.setKind("open");
		}
		
	}

	private void transformSubsume(NodeSymbolic root, TopLevelNonCompositeState topLevelSource) {
		NodeSymbolic subsumedBy = root.getSubsumedBy();
		if (subsumedBy != null){
			//find a state 
			TopLevelNonCompositeState state = statesNode.get(subsumedBy);
						
			//check if created
			if (state != null){
				topLevelSource.setSubsumedBy(state.getID());
			} else {
				//create a state
				SymbolicState contents = subsumedBy.getContents();
				if (contents instanceof SymbolicStateSynch){
					state = factory.createTopLevelCompositeState();
				} else {
					state = factory.createTopLevelNonCompositeState();
				}
				
				//set id of this state
				String id = createId();
				state.setID(id);
				topLevelSource.setSubsumedBy(id);
				
				//store in maps
				statesId.put(id, state);
				statesNode.put(subsumedBy, state);
			}
		}
		
	}

	private void transformSignals(Transition t, SymbolicTransition tOriginal) {
		//get input signal (name)
		input trigger = factory.createinput();
		//JDA: There is a problem because 'Unreceived' transitions are being created with a null transition.
		//JDA: Therefore, they raise a NullPointer exection in the next line.
		//TODO: JDA: Is the following code, added by me, needed?
		if ((tOriginal.getInputExecution() instanceof UnreceivedAction) && (tOriginal.getTransition() == null)){
			System.out.println("JDA: The symboloc transition, " + t + ", is an instance of 'UnreceivedAction' and its transition is null!");
			return;
		}
		if (tOriginal.getTransition().getInputAction() instanceof TimerActionInput) {
			trigger.setPort(tOriginal.getInputExecution().getAction().getSignalName());
			trigger.setSignal("timeout");
		}
		else {
			trigger.setPort(tOriginal.getInputExecution().getAction().getPortName());
			trigger.setSignal(tOriginal.getInputExecution().getAction().getSignalName());
		}
		
		trigger.setParameter(transformMappingSingle(tOriginal.getInputExecution().getValuationMapping()));
		t.setInput(trigger);
		
		
		//get sequence of output signals
		OutputSequenceSymbolic oss = tOriginal.getOuputSequence();
		
		
		for (ActionOutputInstance aoi : oss.getSequence()){
			output out = factory.createoutput();
			out.setSignal(aoi.getActionOutput().getSignalName());
			out.setPort(aoi.getActionOutput().getPortName());
			
			List<VAL> val = transformMapping(aoi.getSymbolicMapping());
			
			out.getValuation().addAll(val);
			
			t.getOutput().add(out);
		}
		
	}

	private List<VAL> transformMapping(
			Map<ActionVariableOutput<?>, Expression> mapping) {
		List<VAL> result = new ArrayList<VAL>();
		
		for (ActionVariableOutput<?> var : mapping.keySet()){
			VAL val = factory.createVAL();
			
			Expression value = mapping.get(var);
			EXPRESSION valueT = transformExpression(value);
			
			val.setName(var.getName());
			val.setValue(valueT);
			
			
			
			result.add(val);
		}
		return result;
	}

	private EXPRESSION transformMappingSingle(Map<SymbolicVariable, Expression> valuation) {
		if (valuation == null || valuation.isEmpty()){
			return null;
		}
		SymbolicVariable sv = valuation.keySet().iterator().next();
		Expression expr = valuation.get(sv);
				
		return transformExpression(expr);
	}

	private EXPRESSION transformExpression(Expression expr) {
		ExpressionTransform et = new ExpressionTransform(expr);
		EXPRESSION exprT = et.transform(expr);	
		return exprT;
	}

	private State transformState(SymbolicState stateOriginal, boolean isTopLevel, String idReq, NodeSymbolic root){
		State stateTransformed = null;
		//JDA: The valuations and path conditions for top level states
		//     did not appear in the '.set' file. This issue has been fixed.
		State stateTransformedForTopLevel = null;
		//JDA: End addition.
        //JDA: debug output
		//System.out.println("JDA - CCSTTransform.transformState. " +
		//		"stateOrigial: <<" + stateOriginal + ">>" +
		//		", isTopLevel: " + isTopLevel +
		//		", idReq: <<" + idReq + ">>."
		//		);
		if (stateOriginal instanceof SymbolicStateSynch){
			
			if (isTopLevel){
				//create top level composite
				if (!idReq.isEmpty()){
					stateTransformed = statesId.get(idReq);
				} else {
					//JDA: Added the following to display  PCs and Valuations for top-level composite states.
					stateTransformedForTopLevel = factory.createTopLevelCompositeState();
					transformSimple(stateTransformedForTopLevel, (SymbolicStateSynch)stateOriginal, root);
					//JDA: End addition.
					stateTransformed = factory.createTopLevelCompositeState();
				}
			} else {
				//create contained composite
				stateTransformed = factory.createContainedCompositeState();
			}
			transformComposite((CompositeState) stateTransformed, (SymbolicStateSynch)stateOriginal, root);
			//JDA: Added the following to display  PCs and Valuations for top-level composite states.
			if (stateTransformedForTopLevel != null) {
				stateTransformed.getVals().addAll(stateTransformedForTopLevel.getVals());
				stateTransformed.getPc().addAll(stateTransformedForTopLevel.getPc());
			}
			//JDA: End addition.
		} else {
			//create top level noncomposite
			if (isTopLevel){
				if (!idReq.isEmpty()){
					stateTransformed = statesId.get(idReq);
				} else {
					stateTransformed = factory.createTopLevelNonCompositeState();
				} 
			} else {
				stateTransformed = factory.createContainedNonCompositeState();
			}
			transformSimple(stateTransformed, stateOriginal, root);
			
		}
		String id = createId();
		stateTransformed.setID(id);
		stateTransformed.setName(stateOriginal.getLocation().getName());		
		
		return stateTransformed;
	}

	private String createId() {		
		return new Integer(stateCounter++).toString();
	}

	private void transformComposite(CompositeState state,
			SymbolicStateSynch stateOriginal, NodeSymbolic root) {
		//transform queues		
		List<Queue> queues = transformQueues(stateOriginal.getQueues());
		for (Queue q : queues){
			state.getQueue().add(q);
		}
		
		//transform insides
		for (String partName : stateOriginal.getParts().keySet()){
			SymbolicState partStateOriginal = stateOriginal.getParts().get(partName);
			ContainedNonCompositeState partState = (ContainedNonCompositeState) transformState(partStateOriginal, false, "", root);
			partState.setSourcePartName(partName);
			
			state.getSubstate().add(partState);
		}
		
	}
	
	private List<Queue> transformQueues(QueueManager queueManager) {
		List<Queue> queues = new ArrayList<Queue>();
		for (SymbolicExecutionTree tree : queueManager.getTrees()){
			symbolic.execution.tree.synch.model.Queue q = queueManager.getQueue(tree);
			
			Queue transformedQueue = factory.createQueue();
			transformedQueue.setRecPartName(tree.getPartName());
			queues.add(transformedQueue);
			
			for (SentMessage sm : q.getList()){
				transformedQueue.getSignal().add(transformInput(sm.getAction()));
			}
			
			
			
		}
		
		return queues;
	}

	private Signal transformInput(ActionInputSymbolic action) {
		Signal s = factory.createSignal();
		s.setPort(action.getAction().getPortName());
		s.setSignal(action.getAction().getSignalName());
		
		s.setInputParam(transformMappingQueue(action.getMappingInputVariables()));
		return s;
	}

	private EXPRESSION transformMappingQueue(
			Map<ModelVariable<?>, Expression> valuation) {
		if (valuation == null || valuation.isEmpty()){
			return null;
		}
		ModelVariable<?> sv = valuation.keySet().iterator().next();
		Expression expr = valuation.get(sv);
				
		return transformExpression(expr);		
	}

	private void transformSimple(State state, SymbolicState stateOriginal, NodeSymbolic root){
		//path constraints
		//System.out.println("JDA - CCSTTransform.transformSimple." +
		//		" state: <<" + state + ">>," +
		//		" stateOrigial: <<" + stateOriginal + ">>");
		List<Constraint> pc;
		if (root instanceof NodeSymbolicSynch) {
			pc = stateOriginal.getPC(root);
		}
		else {
			pc = stateOriginal.getPC();
		}
		for (Constraint cst : pc){
			//System.out.println("    JDA - CCSTTransform.transformSimple. cst: <<" + cst + ">>.");
			//JDA: Added the following "if-then" to protect the case that "cst" was "TRUE".
			//JDA: This case was crashing.
			if (!cst.toString().equals("TRUE")) {
				state.getPc().add(transformExpression(cst)); 
			}
			else {
				//System.out.println("JDA - Ouch. cst =<<" + cst + ">>");
			}
		}
		
		//valuations 
		ValuationSymbolic val;
		if (root instanceof NodeSymbolicSynch) {
			val = stateOriginal.getValuation(root);
		}
		else {
			val = stateOriginal.getValuation();
		}
		List<VAL> vals = transformValuation(val);
		
		
		state.getVals().addAll(vals);
	}

	private List<VAL> transformValuation(ValuationSymbolic valuation) {
		List<VAL> result = new ArrayList<VAL>();
		
		for (ModelVariable<?> var : valuation.getVariables()){
			VAL val = factory.createVAL();
			val.setName(var.getName());
			val.setValue(transformExpression(valuation.getValue(var)));
			
			result.add(val);
		}
		return result;
	}

}
