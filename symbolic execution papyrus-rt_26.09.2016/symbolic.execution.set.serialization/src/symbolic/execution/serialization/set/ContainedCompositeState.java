/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contained Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see symbolic.execution.serialization.set.setPackage#getContainedCompositeState()
 * @model
 * @generated
 */
public interface ContainedCompositeState extends CompositeState, ContainedNonCompositeState {
} // ContainedCompositeState
