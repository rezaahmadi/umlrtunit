/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Queue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.Queue#getRecPartName <em>Rec Part Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Queue#getRecPartType <em>Rec Part Type</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Queue#getSignal <em>Signal</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getQueue()
 * @model
 * @generated
 */
public interface Queue extends EObject {
	/**
	 * Returns the value of the '<em><b>Rec Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rec Part Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rec Part Name</em>' attribute.
	 * @see #setRecPartName(String)
	 * @see symbolic.execution.serialization.set.setPackage#getQueue_RecPartName()
	 * @model required="true"
	 * @generated
	 */
	String getRecPartName();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Queue#getRecPartName <em>Rec Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rec Part Name</em>' attribute.
	 * @see #getRecPartName()
	 * @generated
	 */
	void setRecPartName(String value);

	/**
	 * Returns the value of the '<em><b>Rec Part Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rec Part Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rec Part Type</em>' attribute.
	 * @see #setRecPartType(String)
	 * @see symbolic.execution.serialization.set.setPackage#getQueue_RecPartType()
	 * @model required="true"
	 * @generated
	 */
	String getRecPartType();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Queue#getRecPartType <em>Rec Part Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rec Part Type</em>' attribute.
	 * @see #getRecPartType()
	 * @generated
	 */
	void setRecPartType(String value);

	/**
	 * Returns the value of the '<em><b>Signal</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.Signal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getQueue_Signal()
	 * @model containment="true"
	 * @generated
	 */
	EList<Signal> getSignal();

} // Queue
