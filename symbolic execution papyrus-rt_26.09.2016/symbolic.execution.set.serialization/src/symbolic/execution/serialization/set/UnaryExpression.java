/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.UnaryExpression#getOp <em>Op</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.UnaryExpression#getExpr <em>Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getUnaryExpression()
 * @model
 * @generated
 */
public interface UnaryExpression extends EXPRESSION {
	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * The literals are from the enumeration {@link symbolic.execution.serialization.set.ArithmeticOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see symbolic.execution.serialization.set.ArithmeticOperator
	 * @see #setOp(ArithmeticOperator)
	 * @see symbolic.execution.serialization.set.setPackage#getUnaryExpression_Op()
	 * @model required="true"
	 * @generated
	 */
	ArithmeticOperator getOp();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.UnaryExpression#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see symbolic.execution.serialization.set.ArithmeticOperator
	 * @see #getOp()
	 * @generated
	 */
	void setOp(ArithmeticOperator value);

	/**
	 * Returns the value of the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expr</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expr</em>' containment reference.
	 * @see #setExpr(EXPRESSION)
	 * @see symbolic.execution.serialization.set.setPackage#getUnaryExpression_Expr()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EXPRESSION getExpr();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.UnaryExpression#getExpr <em>Expr</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expr</em>' containment reference.
	 * @see #getExpr()
	 * @generated
	 */
	void setExpr(EXPRESSION value);

} // UnaryExpression
