/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import symbolic.execution.serialization.set.CompositeState;
import symbolic.execution.serialization.set.ContainedNonCompositeState;
import symbolic.execution.serialization.set.Queue;
import symbolic.execution.serialization.set.setPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.impl.CompositeStateImpl#getSubstate <em>Substate</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.CompositeStateImpl#getQueue <em>Queue</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class CompositeStateImpl extends EObjectImpl implements CompositeState {
	/**
	 * The cached value of the '{@link #getSubstate() <em>Substate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubstate()
	 * @generated
	 * @ordered
	 */
	protected EList<ContainedNonCompositeState> substate;

	/**
	 * The cached value of the '{@link #getQueue() <em>Queue</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueue()
	 * @generated
	 * @ordered
	 */
	protected EList<Queue> queue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return setPackage.Literals.COMPOSITE_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContainedNonCompositeState> getSubstate() {
		if (substate == null) {
			substate = new EObjectContainmentEList<ContainedNonCompositeState>(ContainedNonCompositeState.class, this, setPackage.COMPOSITE_STATE__SUBSTATE);
		}
		return substate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Queue> getQueue() {
		if (queue == null) {
			queue = new EObjectContainmentEList<Queue>(Queue.class, this, setPackage.COMPOSITE_STATE__QUEUE);
		}
		return queue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case setPackage.COMPOSITE_STATE__SUBSTATE:
				return ((InternalEList<?>)getSubstate()).basicRemove(otherEnd, msgs);
			case setPackage.COMPOSITE_STATE__QUEUE:
				return ((InternalEList<?>)getQueue()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case setPackage.COMPOSITE_STATE__SUBSTATE:
				return getSubstate();
			case setPackage.COMPOSITE_STATE__QUEUE:
				return getQueue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case setPackage.COMPOSITE_STATE__SUBSTATE:
				getSubstate().clear();
				getSubstate().addAll((Collection<? extends ContainedNonCompositeState>)newValue);
				return;
			case setPackage.COMPOSITE_STATE__QUEUE:
				getQueue().clear();
				getQueue().addAll((Collection<? extends Queue>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case setPackage.COMPOSITE_STATE__SUBSTATE:
				getSubstate().clear();
				return;
			case setPackage.COMPOSITE_STATE__QUEUE:
				getQueue().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case setPackage.COMPOSITE_STATE__SUBSTATE:
				return substate != null && !substate.isEmpty();
			case setPackage.COMPOSITE_STATE__QUEUE:
				return queue != null && !queue.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CompositeStateImpl
