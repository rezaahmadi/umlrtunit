/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import symbolic.execution.serialization.set.EXPRESSION;
import symbolic.execution.serialization.set.State;
import symbolic.execution.serialization.set.VAL;
import symbolic.execution.serialization.set.setPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getName <em>Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getID <em>ID</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getSourcePartName <em>Source Part Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getSubsumedBy <em>Subsumed By</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getSubsumedStates <em>Subsumed States</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getVals <em>Vals</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.StateImpl#getPc <em>Pc</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class StateImpl extends EObjectImpl implements State {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourcePartName() <em>Source Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePartName()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_PART_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourcePartName() <em>Source Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePartName()
	 * @generated
	 * @ordered
	 */
	protected String sourcePartName = SOURCE_PART_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSubsumedBy() <em>Subsumed By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsumedBy()
	 * @generated
	 * @ordered
	 */
	protected static final String SUBSUMED_BY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSubsumedBy() <em>Subsumed By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsumedBy()
	 * @generated
	 * @ordered
	 */
	protected String subsumedBy = SUBSUMED_BY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubsumedStates() <em>Subsumed States</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubsumedStates()
	 * @generated
	 * @ordered
	 */
	protected EList<String> subsumedStates;

	/**
	 * The cached value of the '{@link #getVals() <em>Vals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVals()
	 * @generated
	 * @ordered
	 */
	protected EList<VAL> vals;

	/**
	 * The cached value of the '{@link #getPc() <em>Pc</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPc()
	 * @generated
	 * @ordered
	 */
	protected EList<EXPRESSION> pc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return setPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.STATE__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourcePartName() {
		return sourcePartName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourcePartName(String newSourcePartName) {
		String oldSourcePartName = sourcePartName;
		sourcePartName = newSourcePartName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.STATE__SOURCE_PART_NAME, oldSourcePartName, sourcePartName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSubsumedBy() {
		return subsumedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubsumedBy(String newSubsumedBy) {
		String oldSubsumedBy = subsumedBy;
		subsumedBy = newSubsumedBy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.STATE__SUBSUMED_BY, oldSubsumedBy, subsumedBy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSubsumedStates() {
		if (subsumedStates == null) {
			subsumedStates = new EDataTypeUniqueEList<String>(String.class, this, setPackage.STATE__SUBSUMED_STATES);
		}
		return subsumedStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VAL> getVals() {
		if (vals == null) {
			vals = new EObjectContainmentEList<VAL>(VAL.class, this, setPackage.STATE__VALS);
		}
		return vals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EXPRESSION> getPc() {
		if (pc == null) {
			pc = new EObjectContainmentEList<EXPRESSION>(EXPRESSION.class, this, setPackage.STATE__PC);
		}
		return pc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case setPackage.STATE__VALS:
				return ((InternalEList<?>)getVals()).basicRemove(otherEnd, msgs);
			case setPackage.STATE__PC:
				return ((InternalEList<?>)getPc()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case setPackage.STATE__NAME:
				return getName();
			case setPackage.STATE__ID:
				return getID();
			case setPackage.STATE__SOURCE_PART_NAME:
				return getSourcePartName();
			case setPackage.STATE__SUBSUMED_BY:
				return getSubsumedBy();
			case setPackage.STATE__SUBSUMED_STATES:
				return getSubsumedStates();
			case setPackage.STATE__VALS:
				return getVals();
			case setPackage.STATE__PC:
				return getPc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case setPackage.STATE__NAME:
				setName((String)newValue);
				return;
			case setPackage.STATE__ID:
				setID((String)newValue);
				return;
			case setPackage.STATE__SOURCE_PART_NAME:
				setSourcePartName((String)newValue);
				return;
			case setPackage.STATE__SUBSUMED_BY:
				setSubsumedBy((String)newValue);
				return;
			case setPackage.STATE__SUBSUMED_STATES:
				getSubsumedStates().clear();
				getSubsumedStates().addAll((Collection<? extends String>)newValue);
				return;
			case setPackage.STATE__VALS:
				getVals().clear();
				getVals().addAll((Collection<? extends VAL>)newValue);
				return;
			case setPackage.STATE__PC:
				getPc().clear();
				getPc().addAll((Collection<? extends EXPRESSION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case setPackage.STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case setPackage.STATE__ID:
				setID(ID_EDEFAULT);
				return;
			case setPackage.STATE__SOURCE_PART_NAME:
				setSourcePartName(SOURCE_PART_NAME_EDEFAULT);
				return;
			case setPackage.STATE__SUBSUMED_BY:
				setSubsumedBy(SUBSUMED_BY_EDEFAULT);
				return;
			case setPackage.STATE__SUBSUMED_STATES:
				getSubsumedStates().clear();
				return;
			case setPackage.STATE__VALS:
				getVals().clear();
				return;
			case setPackage.STATE__PC:
				getPc().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case setPackage.STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case setPackage.STATE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case setPackage.STATE__SOURCE_PART_NAME:
				return SOURCE_PART_NAME_EDEFAULT == null ? sourcePartName != null : !SOURCE_PART_NAME_EDEFAULT.equals(sourcePartName);
			case setPackage.STATE__SUBSUMED_BY:
				return SUBSUMED_BY_EDEFAULT == null ? subsumedBy != null : !SUBSUMED_BY_EDEFAULT.equals(subsumedBy);
			case setPackage.STATE__SUBSUMED_STATES:
				return subsumedStates != null && !subsumedStates.isEmpty();
			case setPackage.STATE__VALS:
				return vals != null && !vals.isEmpty();
			case setPackage.STATE__PC:
				return pc != null && !pc.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", ID: ");
		result.append(id);
		result.append(", sourcePartName: ");
		result.append(sourcePartName);
		result.append(", subsumedBy: ");
		result.append(subsumedBy);
		result.append(", subsumedStates: ");
		result.append(subsumedStates);
		result.append(')');
		return result.toString();
	}

} //StateImpl
