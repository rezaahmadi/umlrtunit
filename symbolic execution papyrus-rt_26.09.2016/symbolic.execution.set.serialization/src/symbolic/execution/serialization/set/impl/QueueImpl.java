/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import symbolic.execution.serialization.set.Queue;
import symbolic.execution.serialization.set.Signal;
import symbolic.execution.serialization.set.setPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Queue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.impl.QueueImpl#getRecPartName <em>Rec Part Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.QueueImpl#getRecPartType <em>Rec Part Type</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.impl.QueueImpl#getSignal <em>Signal</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class QueueImpl extends EObjectImpl implements Queue {
	/**
	 * The default value of the '{@link #getRecPartName() <em>Rec Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecPartName()
	 * @generated
	 * @ordered
	 */
	protected static final String REC_PART_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRecPartName() <em>Rec Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecPartName()
	 * @generated
	 * @ordered
	 */
	protected String recPartName = REC_PART_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getRecPartType() <em>Rec Part Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecPartType()
	 * @generated
	 * @ordered
	 */
	protected static final String REC_PART_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRecPartType() <em>Rec Part Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecPartType()
	 * @generated
	 * @ordered
	 */
	protected String recPartType = REC_PART_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSignal() <em>Signal</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignal()
	 * @generated
	 * @ordered
	 */
	protected EList<Signal> signal;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QueueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return setPackage.Literals.QUEUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRecPartName() {
		return recPartName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecPartName(String newRecPartName) {
		String oldRecPartName = recPartName;
		recPartName = newRecPartName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.QUEUE__REC_PART_NAME, oldRecPartName, recPartName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRecPartType() {
		return recPartType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecPartType(String newRecPartType) {
		String oldRecPartType = recPartType;
		recPartType = newRecPartType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, setPackage.QUEUE__REC_PART_TYPE, oldRecPartType, recPartType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Signal> getSignal() {
		if (signal == null) {
			signal = new EObjectContainmentEList<Signal>(Signal.class, this, setPackage.QUEUE__SIGNAL);
		}
		return signal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case setPackage.QUEUE__SIGNAL:
				return ((InternalEList<?>)getSignal()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case setPackage.QUEUE__REC_PART_NAME:
				return getRecPartName();
			case setPackage.QUEUE__REC_PART_TYPE:
				return getRecPartType();
			case setPackage.QUEUE__SIGNAL:
				return getSignal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case setPackage.QUEUE__REC_PART_NAME:
				setRecPartName((String)newValue);
				return;
			case setPackage.QUEUE__REC_PART_TYPE:
				setRecPartType((String)newValue);
				return;
			case setPackage.QUEUE__SIGNAL:
				getSignal().clear();
				getSignal().addAll((Collection<? extends Signal>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case setPackage.QUEUE__REC_PART_NAME:
				setRecPartName(REC_PART_NAME_EDEFAULT);
				return;
			case setPackage.QUEUE__REC_PART_TYPE:
				setRecPartType(REC_PART_TYPE_EDEFAULT);
				return;
			case setPackage.QUEUE__SIGNAL:
				getSignal().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case setPackage.QUEUE__REC_PART_NAME:
				return REC_PART_NAME_EDEFAULT == null ? recPartName != null : !REC_PART_NAME_EDEFAULT.equals(recPartName);
			case setPackage.QUEUE__REC_PART_TYPE:
				return REC_PART_TYPE_EDEFAULT == null ? recPartType != null : !REC_PART_TYPE_EDEFAULT.equals(recPartType);
			case setPackage.QUEUE__SIGNAL:
				return signal != null && !signal.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (recPartName: ");
		result.append(recPartName);
		result.append(", recPartType: ");
		result.append(recPartType);
		result.append(')');
		return result.toString();
	}

} //QueueImpl
