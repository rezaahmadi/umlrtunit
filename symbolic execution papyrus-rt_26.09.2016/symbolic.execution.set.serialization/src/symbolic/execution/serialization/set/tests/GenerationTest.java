package symbolic.execution.serialization.set.tests;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.serialization.set.SET;
import symbolic.execution.set.serialization.transform.CSETTransform;
import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;
import symbolic.execution.tree.synch.model.SymbolicStateSynch;

public class GenerationTest {
	
	public static void main(String[] args) {
		//create contents of a state
		SymbolicStateSynch rootState = createCompositeState();
		
		//create root
		NodeSymbolic root = new NodeSymbolicSynch(rootState);
		
		//create a tree
		SymbolicExecutionTreeSynch tree = new SymbolicExecutionTreeSynch(root, null, null);
		
		//transform it
		CSETTransform transformer = new CSETTransform();
		SET set = transformer.transform(tree);
		
		//store as xmi
		ResourceFactoryImpl factory = new XMIResourceFactoryImpl();
		
		Resource resorce = factory.createResource(URI.createFileURI("company.xmi"));
		resorce.getContents().add(set);
		try {
			resorce.save(null);
		} catch (IOException e) {
			System.out.println("JDA: IOException in GenerationTest.main.");
			e.printStackTrace();
		}
		
		
		
	}
	
	public static SymbolicStateSynch createCompositeState(){
		//create inner state
		//SymbolicState s = new SymbolicState(treeState) 
		
		return null;
	}

}
