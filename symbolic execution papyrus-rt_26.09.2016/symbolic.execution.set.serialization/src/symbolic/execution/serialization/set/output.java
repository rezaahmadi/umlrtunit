/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>output</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.output#getSignal <em>Signal</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.output#getPort <em>Port</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.output#getValuation <em>Valuation</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getoutput()
 * @model
 * @generated
 */
public interface output extends EObject {
	/**
	 * Returns the value of the '<em><b>Signal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal</em>' attribute.
	 * @see #setSignal(String)
	 * @see symbolic.execution.serialization.set.setPackage#getoutput_Signal()
	 * @model required="true"
	 * @generated
	 */
	String getSignal();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.output#getSignal <em>Signal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signal</em>' attribute.
	 * @see #getSignal()
	 * @generated
	 */
	void setSignal(String value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' attribute.
	 * @see #setPort(String)
	 * @see symbolic.execution.serialization.set.setPackage#getoutput_Port()
	 * @model required="true"
	 * @generated
	 */
	String getPort();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.output#getPort <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' attribute.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(String value);

	/**
	 * Returns the value of the '<em><b>Valuation</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.VAL}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valuation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valuation</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getoutput_Valuation()
	 * @model containment="true"
	 * @generated
	 */
	EList<VAL> getValuation();

} // output
