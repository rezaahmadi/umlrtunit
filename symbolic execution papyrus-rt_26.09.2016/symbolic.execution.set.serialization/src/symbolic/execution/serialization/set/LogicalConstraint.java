/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.LogicalConstraint#getOp <em>Op</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.LogicalConstraint#getLeft <em>Left</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.LogicalConstraint#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getLogicalConstraint()
 * @model
 * @generated
 */
public interface LogicalConstraint extends Constraint {
	/**
	 * Returns the value of the '<em><b>Op</b></em>' attribute.
	 * The literals are from the enumeration {@link symbolic.execution.serialization.set.LogicalOperators}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op</em>' attribute.
	 * @see symbolic.execution.serialization.set.LogicalOperators
	 * @see #setOp(LogicalOperators)
	 * @see symbolic.execution.serialization.set.setPackage#getLogicalConstraint_Op()
	 * @model required="true"
	 * @generated
	 */
	LogicalOperators getOp();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.LogicalConstraint#getOp <em>Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op</em>' attribute.
	 * @see symbolic.execution.serialization.set.LogicalOperators
	 * @see #getOp()
	 * @generated
	 */
	void setOp(LogicalOperators value);

	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(EXPRESSION)
	 * @see symbolic.execution.serialization.set.setPackage#getLogicalConstraint_Left()
	 * @model containment="true"
	 * @generated
	 */
	EXPRESSION getLeft();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.LogicalConstraint#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(EXPRESSION value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(EXPRESSION)
	 * @see symbolic.execution.serialization.set.setPackage#getLogicalConstraint_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EXPRESSION getRight();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.LogicalConstraint#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(EXPRESSION value);

} // LogicalConstraint
