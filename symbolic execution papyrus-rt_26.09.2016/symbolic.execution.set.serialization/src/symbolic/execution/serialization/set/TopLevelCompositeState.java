/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Top Level Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see symbolic.execution.serialization.set.setPackage#getTopLevelCompositeState()
 * @model
 * @generated
 */
public interface TopLevelCompositeState extends CompositeState, TopLevelNonCompositeState {
} // TopLevelCompositeState
