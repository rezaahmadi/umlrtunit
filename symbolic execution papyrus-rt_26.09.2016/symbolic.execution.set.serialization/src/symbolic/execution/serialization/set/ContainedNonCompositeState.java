/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contained Non Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see symbolic.execution.serialization.set.setPackage#getContainedNonCompositeState()
 * @model
 * @generated
 */
public interface ContainedNonCompositeState extends State {
} // ContainedNonCompositeState
