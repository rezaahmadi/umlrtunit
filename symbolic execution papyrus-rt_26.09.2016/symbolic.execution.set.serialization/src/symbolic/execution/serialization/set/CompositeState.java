/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.CompositeState#getSubstate <em>Substate</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.CompositeState#getQueue <em>Queue</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getCompositeState()
 * @model abstract="true"
 * @generated
 */
public interface CompositeState extends EObject {
	/**
	 * Returns the value of the '<em><b>Substate</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.ContainedNonCompositeState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Substate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Substate</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getCompositeState_Substate()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ContainedNonCompositeState> getSubstate();

	/**
	 * Returns the value of the '<em><b>Queue</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.Queue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queue</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queue</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getCompositeState_Queue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Queue> getQueue();

} // CompositeState
