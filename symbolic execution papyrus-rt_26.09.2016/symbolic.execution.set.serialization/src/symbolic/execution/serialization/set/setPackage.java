/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see symbolic.execution.serialization.set.setFactory
 * @model kind="package"
 * @generated
 */
public interface setPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "set";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://set/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "set";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	setPackage eINSTANCE = symbolic.execution.serialization.set.impl.setPackageImpl.init();

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.SETImpl <em>SET</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.SETImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSET()
	 * @generated
	 */
	int SET = 0;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET__ROOT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET__NAME = 1;

	/**
	 * The number of structural features of the '<em>SET</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.StateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getState()
	 * @generated
	 */
	int STATE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ID = 1;

	/**
	 * The feature id for the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SOURCE_PART_NAME = 2;

	/**
	 * The feature id for the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SUBSUMED_BY = 3;

	/**
	 * The feature id for the '<em><b>Subsumed States</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SUBSUMED_STATES = 4;

	/**
	 * The feature id for the '<em><b>Vals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__VALS = 5;

	/**
	 * The feature id for the '<em><b>Pc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PC = 6;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.TransitionImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Input</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__INPUT = 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__OUTPUT = 1;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__KIND = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ID = 4;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.inputImpl <em>input</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.inputImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getinput()
	 * @generated
	 */
	int INPUT = 3;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT__SIGNAL = 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT__PORT = 1;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT__PARAMETER = 2;

	/**
	 * The number of structural features of the '<em>input</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.outputImpl <em>output</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.outputImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getoutput()
	 * @generated
	 */
	int OUTPUT = 4;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT__SIGNAL = 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT__PORT = 1;

	/**
	 * The feature id for the '<em><b>Valuation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT__VALUATION = 2;

	/**
	 * The number of structural features of the '<em>output</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.TopLevelNonCompositeStateImpl <em>Top Level Non Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.TopLevelNonCompositeStateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTopLevelNonCompositeState()
	 * @generated
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__NAME = STATE__NAME;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__ID = STATE__ID;

	/**
	 * The feature id for the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__SOURCE_PART_NAME = STATE__SOURCE_PART_NAME;

	/**
	 * The feature id for the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__SUBSUMED_BY = STATE__SUBSUMED_BY;

	/**
	 * The feature id for the '<em><b>Subsumed States</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__SUBSUMED_STATES = STATE__SUBSUMED_STATES;

	/**
	 * The feature id for the '<em><b>Vals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__VALS = STATE__VALS;

	/**
	 * The feature id for the '<em><b>Pc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__PC = STATE__PC;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE__SOURCE = STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Top Level Non Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_NON_COMPOSITE_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.ContainedNonCompositeStateImpl <em>Contained Non Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.ContainedNonCompositeStateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getContainedNonCompositeState()
	 * @generated
	 */
	int CONTAINED_NON_COMPOSITE_STATE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__NAME = STATE__NAME;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__ID = STATE__ID;

	/**
	 * The feature id for the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__SOURCE_PART_NAME = STATE__SOURCE_PART_NAME;

	/**
	 * The feature id for the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__SUBSUMED_BY = STATE__SUBSUMED_BY;

	/**
	 * The feature id for the '<em><b>Subsumed States</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__SUBSUMED_STATES = STATE__SUBSUMED_STATES;

	/**
	 * The feature id for the '<em><b>Vals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__VALS = STATE__VALS;

	/**
	 * The feature id for the '<em><b>Pc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE__PC = STATE__PC;

	/**
	 * The number of structural features of the '<em>Contained Non Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_NON_COMPOSITE_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.CompositeStateImpl <em>Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.CompositeStateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getCompositeState()
	 * @generated
	 */
	int COMPOSITE_STATE = 9;

	/**
	 * The feature id for the '<em><b>Substate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__SUBSTATE = 0;

	/**
	 * The feature id for the '<em><b>Queue</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__QUEUE = 1;

	/**
	 * The number of structural features of the '<em>Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.TopLevelCompositeStateImpl <em>Top Level Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.TopLevelCompositeStateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTopLevelCompositeState()
	 * @generated
	 */
	int TOP_LEVEL_COMPOSITE_STATE = 7;

	/**
	 * The feature id for the '<em><b>Substate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__SUBSTATE = COMPOSITE_STATE__SUBSTATE;

	/**
	 * The feature id for the '<em><b>Queue</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__QUEUE = COMPOSITE_STATE__QUEUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__NAME = COMPOSITE_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__ID = COMPOSITE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__SOURCE_PART_NAME = COMPOSITE_STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__SUBSUMED_BY = COMPOSITE_STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Subsumed States</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__SUBSUMED_STATES = COMPOSITE_STATE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Vals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__VALS = COMPOSITE_STATE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Pc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__PC = COMPOSITE_STATE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE__SOURCE = COMPOSITE_STATE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Top Level Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOP_LEVEL_COMPOSITE_STATE_FEATURE_COUNT = COMPOSITE_STATE_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.ContainedCompositeStateImpl <em>Contained Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.ContainedCompositeStateImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getContainedCompositeState()
	 * @generated
	 */
	int CONTAINED_COMPOSITE_STATE = 8;

	/**
	 * The feature id for the '<em><b>Substate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__SUBSTATE = COMPOSITE_STATE__SUBSTATE;

	/**
	 * The feature id for the '<em><b>Queue</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__QUEUE = COMPOSITE_STATE__QUEUE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__NAME = COMPOSITE_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__ID = COMPOSITE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__SOURCE_PART_NAME = COMPOSITE_STATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__SUBSUMED_BY = COMPOSITE_STATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Subsumed States</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__SUBSUMED_STATES = COMPOSITE_STATE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Vals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__VALS = COMPOSITE_STATE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Pc</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE__PC = COMPOSITE_STATE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Contained Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINED_COMPOSITE_STATE_FEATURE_COUNT = COMPOSITE_STATE_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.QueueImpl <em>Queue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.QueueImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getQueue()
	 * @generated
	 */
	int QUEUE = 10;

	/**
	 * The feature id for the '<em><b>Rec Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE__REC_PART_NAME = 0;

	/**
	 * The feature id for the '<em><b>Rec Part Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE__REC_PART_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE__SIGNAL = 2;

	/**
	 * The number of structural features of the '<em>Queue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.VALImpl <em>VAL</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.VALImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getVAL()
	 * @generated
	 */
	int VAL = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAL__VALUE = 1;

	/**
	 * The number of structural features of the '<em>VAL</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.EXPRESSIONImpl <em>EXPRESSION</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.EXPRESSIONImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getEXPRESSION()
	 * @generated
	 */
	int EXPRESSION = 12;

	/**
	 * The number of structural features of the '<em>EXPRESSION</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.BinaryExpressionImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LEFT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RIGHT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.ConstantImpl <em>Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.ConstantImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getConstant()
	 * @generated
	 */
	int CONSTANT = 14;

	/**
	 * The feature id for the '<em><b>String Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__STRING_REPRESENTATION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__VALUE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__TYPE = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.ConstraintImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 15;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.SymbolicVariableImpl <em>Symbolic Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.SymbolicVariableImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSymbolicVariable()
	 * @generated
	 */
	int SYMBOLIC_VARIABLE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOLIC_VARIABLE__NAME = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOLIC_VARIABLE__TYPE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Symbolic Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOLIC_VARIABLE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.UnaryExpressionImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 17;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OP = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expr</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__EXPR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.LogicalConstraintImpl <em>Logical Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.LogicalConstraintImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getLogicalConstraint()
	 * @generated
	 */
	int LOGICAL_CONSTRAINT = 18;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__OP = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__LEFT = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__RIGHT = CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Logical Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.RelationalConstraintImpl <em>Relational Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.RelationalConstraintImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getRelationalConstraint()
	 * @generated
	 */
	int RELATIONAL_CONSTRAINT = 19;

	/**
	 * The feature id for the '<em><b>Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_CONSTRAINT__OP = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_CONSTRAINT__LEFT = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_CONSTRAINT__RIGHT = CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Relational Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONAL_CONSTRAINT_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.TrueImpl <em>True</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.TrueImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTrue()
	 * @generated
	 */
	int TRUE = 20;

	/**
	 * The feature id for the '<em><b>True</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE__TRUE = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>True</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.FalseImpl <em>False</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.FalseImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getFalse()
	 * @generated
	 */
	int FALSE = 21;

	/**
	 * The feature id for the '<em><b>False</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE__FALSE = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>False</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALSE_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.impl.SignalImpl <em>Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.impl.SignalImpl
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSignal()
	 * @generated
	 */
	int SIGNAL = 22;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__SIGNAL = 0;

	/**
	 * The feature id for the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__PORT = 1;

	/**
	 * The feature id for the '<em><b>Input Param</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL__INPUT_PARAM = 2;

	/**
	 * The number of structural features of the '<em>Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.ArithmeticOperator <em>Arithmetic Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.ArithmeticOperator
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getArithmeticOperator()
	 * @generated
	 */
	int ARITHMETIC_OPERATOR = 23;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.Type <em>Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.Type
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 24;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.LogicalOperators <em>Logical Operators</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.LogicalOperators
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getLogicalOperators()
	 * @generated
	 */
	int LOGICAL_OPERATORS = 25;

	/**
	 * The meta object id for the '{@link symbolic.execution.serialization.set.CompareOperators <em>Compare Operators</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see symbolic.execution.serialization.set.CompareOperators
	 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getCompareOperators()
	 * @generated
	 */
	int COMPARE_OPERATORS = 26;


	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.SET <em>SET</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SET</em>'.
	 * @see symbolic.execution.serialization.set.SET
	 * @generated
	 */
	EClass getSET();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.SET#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see symbolic.execution.serialization.set.SET#getRoot()
	 * @see #getSET()
	 * @generated
	 */
	EReference getSET_Root();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.SET#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see symbolic.execution.serialization.set.SET#getName()
	 * @see #getSET()
	 * @generated
	 */
	EAttribute getSET_Name();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see symbolic.execution.serialization.set.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.State#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see symbolic.execution.serialization.set.State#getName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Name();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.State#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see symbolic.execution.serialization.set.State#getID()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_ID();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.State#getSourcePartName <em>Source Part Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Part Name</em>'.
	 * @see symbolic.execution.serialization.set.State#getSourcePartName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_SourcePartName();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.State#getSubsumedBy <em>Subsumed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Subsumed By</em>'.
	 * @see symbolic.execution.serialization.set.State#getSubsumedBy()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_SubsumedBy();

	/**
	 * Returns the meta object for the attribute list '{@link symbolic.execution.serialization.set.State#getSubsumedStates <em>Subsumed States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Subsumed States</em>'.
	 * @see symbolic.execution.serialization.set.State#getSubsumedStates()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_SubsumedStates();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.State#getVals <em>Vals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vals</em>'.
	 * @see symbolic.execution.serialization.set.State#getVals()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Vals();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.State#getPc <em>Pc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pc</em>'.
	 * @see symbolic.execution.serialization.set.State#getPc()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Pc();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see symbolic.execution.serialization.set.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.Transition#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input</em>'.
	 * @see symbolic.execution.serialization.set.Transition#getInput()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Input();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.Transition#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output</em>'.
	 * @see symbolic.execution.serialization.set.Transition#getOutput()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Output();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Transition#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see symbolic.execution.serialization.set.Transition#getKind()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Kind();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target</em>'.
	 * @see symbolic.execution.serialization.set.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Transition#getID <em>ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ID</em>'.
	 * @see symbolic.execution.serialization.set.Transition#getID()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_ID();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.input <em>input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>input</em>'.
	 * @see symbolic.execution.serialization.set.input
	 * @generated
	 */
	EClass getinput();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.input#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signal</em>'.
	 * @see symbolic.execution.serialization.set.input#getSignal()
	 * @see #getinput()
	 * @generated
	 */
	EAttribute getinput_Signal();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.input#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see symbolic.execution.serialization.set.input#getPort()
	 * @see #getinput()
	 * @generated
	 */
	EAttribute getinput_Port();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.input#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter</em>'.
	 * @see symbolic.execution.serialization.set.input#getParameter()
	 * @see #getinput()
	 * @generated
	 */
	EReference getinput_Parameter();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.output <em>output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>output</em>'.
	 * @see symbolic.execution.serialization.set.output
	 * @generated
	 */
	EClass getoutput();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.output#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signal</em>'.
	 * @see symbolic.execution.serialization.set.output#getSignal()
	 * @see #getoutput()
	 * @generated
	 */
	EAttribute getoutput_Signal();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.output#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see symbolic.execution.serialization.set.output#getPort()
	 * @see #getoutput()
	 * @generated
	 */
	EAttribute getoutput_Port();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.output#getValuation <em>Valuation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Valuation</em>'.
	 * @see symbolic.execution.serialization.set.output#getValuation()
	 * @see #getoutput()
	 * @generated
	 */
	EReference getoutput_Valuation();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.TopLevelNonCompositeState <em>Top Level Non Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Top Level Non Composite State</em>'.
	 * @see symbolic.execution.serialization.set.TopLevelNonCompositeState
	 * @generated
	 */
	EClass getTopLevelNonCompositeState();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.TopLevelNonCompositeState#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Source</em>'.
	 * @see symbolic.execution.serialization.set.TopLevelNonCompositeState#getSource()
	 * @see #getTopLevelNonCompositeState()
	 * @generated
	 */
	EReference getTopLevelNonCompositeState_Source();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.ContainedNonCompositeState <em>Contained Non Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Non Composite State</em>'.
	 * @see symbolic.execution.serialization.set.ContainedNonCompositeState
	 * @generated
	 */
	EClass getContainedNonCompositeState();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.TopLevelCompositeState <em>Top Level Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Top Level Composite State</em>'.
	 * @see symbolic.execution.serialization.set.TopLevelCompositeState
	 * @generated
	 */
	EClass getTopLevelCompositeState();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.ContainedCompositeState <em>Contained Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contained Composite State</em>'.
	 * @see symbolic.execution.serialization.set.ContainedCompositeState
	 * @generated
	 */
	EClass getContainedCompositeState();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.CompositeState <em>Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite State</em>'.
	 * @see symbolic.execution.serialization.set.CompositeState
	 * @generated
	 */
	EClass getCompositeState();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.CompositeState#getSubstate <em>Substate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Substate</em>'.
	 * @see symbolic.execution.serialization.set.CompositeState#getSubstate()
	 * @see #getCompositeState()
	 * @generated
	 */
	EReference getCompositeState_Substate();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.CompositeState#getQueue <em>Queue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Queue</em>'.
	 * @see symbolic.execution.serialization.set.CompositeState#getQueue()
	 * @see #getCompositeState()
	 * @generated
	 */
	EReference getCompositeState_Queue();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.Queue <em>Queue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Queue</em>'.
	 * @see symbolic.execution.serialization.set.Queue
	 * @generated
	 */
	EClass getQueue();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Queue#getRecPartName <em>Rec Part Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rec Part Name</em>'.
	 * @see symbolic.execution.serialization.set.Queue#getRecPartName()
	 * @see #getQueue()
	 * @generated
	 */
	EAttribute getQueue_RecPartName();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Queue#getRecPartType <em>Rec Part Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rec Part Type</em>'.
	 * @see symbolic.execution.serialization.set.Queue#getRecPartType()
	 * @see #getQueue()
	 * @generated
	 */
	EAttribute getQueue_RecPartType();

	/**
	 * Returns the meta object for the containment reference list '{@link symbolic.execution.serialization.set.Queue#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signal</em>'.
	 * @see symbolic.execution.serialization.set.Queue#getSignal()
	 * @see #getQueue()
	 * @generated
	 */
	EReference getQueue_Signal();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.VAL <em>VAL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VAL</em>'.
	 * @see symbolic.execution.serialization.set.VAL
	 * @generated
	 */
	EClass getVAL();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.VAL#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see symbolic.execution.serialization.set.VAL#getName()
	 * @see #getVAL()
	 * @generated
	 */
	EAttribute getVAL_Name();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.VAL#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see symbolic.execution.serialization.set.VAL#getValue()
	 * @see #getVAL()
	 * @generated
	 */
	EReference getVAL_Value();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.EXPRESSION <em>EXPRESSION</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EXPRESSION</em>'.
	 * @see symbolic.execution.serialization.set.EXPRESSION
	 * @generated
	 */
	EClass getEXPRESSION();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see symbolic.execution.serialization.set.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.BinaryExpression#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see symbolic.execution.serialization.set.BinaryExpression#getOp()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EAttribute getBinaryExpression_Op();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.BinaryExpression#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see symbolic.execution.serialization.set.BinaryExpression#getLeft()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Left();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.BinaryExpression#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see symbolic.execution.serialization.set.BinaryExpression#getRight()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Right();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.Constant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see symbolic.execution.serialization.set.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Constant#getStringRepresentation <em>String Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Representation</em>'.
	 * @see symbolic.execution.serialization.set.Constant#getStringRepresentation()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_StringRepresentation();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Constant#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see symbolic.execution.serialization.set.Constant#getValue()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_Value();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Constant#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see symbolic.execution.serialization.set.Constant#getType()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_Type();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see symbolic.execution.serialization.set.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.SymbolicVariable <em>Symbolic Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbolic Variable</em>'.
	 * @see symbolic.execution.serialization.set.SymbolicVariable
	 * @generated
	 */
	EClass getSymbolicVariable();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.SymbolicVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see symbolic.execution.serialization.set.SymbolicVariable#getName()
	 * @see #getSymbolicVariable()
	 * @generated
	 */
	EAttribute getSymbolicVariable_Name();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.SymbolicVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see symbolic.execution.serialization.set.SymbolicVariable#getType()
	 * @see #getSymbolicVariable()
	 * @generated
	 */
	EAttribute getSymbolicVariable_Type();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see symbolic.execution.serialization.set.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.UnaryExpression#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see symbolic.execution.serialization.set.UnaryExpression#getOp()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EAttribute getUnaryExpression_Op();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.UnaryExpression#getExpr <em>Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expr</em>'.
	 * @see symbolic.execution.serialization.set.UnaryExpression#getExpr()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Expr();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.LogicalConstraint <em>Logical Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Constraint</em>'.
	 * @see symbolic.execution.serialization.set.LogicalConstraint
	 * @generated
	 */
	EClass getLogicalConstraint();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.LogicalConstraint#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see symbolic.execution.serialization.set.LogicalConstraint#getOp()
	 * @see #getLogicalConstraint()
	 * @generated
	 */
	EAttribute getLogicalConstraint_Op();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.LogicalConstraint#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see symbolic.execution.serialization.set.LogicalConstraint#getLeft()
	 * @see #getLogicalConstraint()
	 * @generated
	 */
	EReference getLogicalConstraint_Left();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.LogicalConstraint#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see symbolic.execution.serialization.set.LogicalConstraint#getRight()
	 * @see #getLogicalConstraint()
	 * @generated
	 */
	EReference getLogicalConstraint_Right();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.RelationalConstraint <em>Relational Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relational Constraint</em>'.
	 * @see symbolic.execution.serialization.set.RelationalConstraint
	 * @generated
	 */
	EClass getRelationalConstraint();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.RelationalConstraint#getOp <em>Op</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op</em>'.
	 * @see symbolic.execution.serialization.set.RelationalConstraint#getOp()
	 * @see #getRelationalConstraint()
	 * @generated
	 */
	EAttribute getRelationalConstraint_Op();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.RelationalConstraint#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see symbolic.execution.serialization.set.RelationalConstraint#getLeft()
	 * @see #getRelationalConstraint()
	 * @generated
	 */
	EReference getRelationalConstraint_Left();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.RelationalConstraint#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see symbolic.execution.serialization.set.RelationalConstraint#getRight()
	 * @see #getRelationalConstraint()
	 * @generated
	 */
	EReference getRelationalConstraint_Right();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.True <em>True</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>True</em>'.
	 * @see symbolic.execution.serialization.set.True
	 * @generated
	 */
	EClass getTrue();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.True#getTrue <em>True</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>True</em>'.
	 * @see symbolic.execution.serialization.set.True#getTrue()
	 * @see #getTrue()
	 * @generated
	 */
	EAttribute getTrue_True();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.False <em>False</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>False</em>'.
	 * @see symbolic.execution.serialization.set.False
	 * @generated
	 */
	EClass getFalse();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.False#getFalse <em>False</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>False</em>'.
	 * @see symbolic.execution.serialization.set.False#getFalse()
	 * @see #getFalse()
	 * @generated
	 */
	EAttribute getFalse_False();

	/**
	 * Returns the meta object for class '{@link symbolic.execution.serialization.set.Signal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal</em>'.
	 * @see symbolic.execution.serialization.set.Signal
	 * @generated
	 */
	EClass getSignal();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Signal#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signal</em>'.
	 * @see symbolic.execution.serialization.set.Signal#getSignal()
	 * @see #getSignal()
	 * @generated
	 */
	EAttribute getSignal_Signal();

	/**
	 * Returns the meta object for the attribute '{@link symbolic.execution.serialization.set.Signal#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port</em>'.
	 * @see symbolic.execution.serialization.set.Signal#getPort()
	 * @see #getSignal()
	 * @generated
	 */
	EAttribute getSignal_Port();

	/**
	 * Returns the meta object for the containment reference '{@link symbolic.execution.serialization.set.Signal#getInputParam <em>Input Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Input Param</em>'.
	 * @see symbolic.execution.serialization.set.Signal#getInputParam()
	 * @see #getSignal()
	 * @generated
	 */
	EReference getSignal_InputParam();

	/**
	 * Returns the meta object for enum '{@link symbolic.execution.serialization.set.ArithmeticOperator <em>Arithmetic Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Arithmetic Operator</em>'.
	 * @see symbolic.execution.serialization.set.ArithmeticOperator
	 * @generated
	 */
	EEnum getArithmeticOperator();

	/**
	 * Returns the meta object for enum '{@link symbolic.execution.serialization.set.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type</em>'.
	 * @see symbolic.execution.serialization.set.Type
	 * @generated
	 */
	EEnum getType();

	/**
	 * Returns the meta object for enum '{@link symbolic.execution.serialization.set.LogicalOperators <em>Logical Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operators</em>'.
	 * @see symbolic.execution.serialization.set.LogicalOperators
	 * @generated
	 */
	EEnum getLogicalOperators();

	/**
	 * Returns the meta object for enum '{@link symbolic.execution.serialization.set.CompareOperators <em>Compare Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Compare Operators</em>'.
	 * @see symbolic.execution.serialization.set.CompareOperators
	 * @generated
	 */
	EEnum getCompareOperators();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	setFactory getsetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.SETImpl <em>SET</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.SETImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSET()
		 * @generated
		 */
		EClass SET = eINSTANCE.getSET();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET__ROOT = eINSTANCE.getSET_Root();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SET__NAME = eINSTANCE.getSET_Name();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.StateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__NAME = eINSTANCE.getState_Name();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__ID = eINSTANCE.getState_ID();

		/**
		 * The meta object literal for the '<em><b>Source Part Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__SOURCE_PART_NAME = eINSTANCE.getState_SourcePartName();

		/**
		 * The meta object literal for the '<em><b>Subsumed By</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__SUBSUMED_BY = eINSTANCE.getState_SubsumedBy();

		/**
		 * The meta object literal for the '<em><b>Subsumed States</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__SUBSUMED_STATES = eINSTANCE.getState_SubsumedStates();

		/**
		 * The meta object literal for the '<em><b>Vals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__VALS = eINSTANCE.getState_Vals();

		/**
		 * The meta object literal for the '<em><b>Pc</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__PC = eINSTANCE.getState_Pc();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.TransitionImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__INPUT = eINSTANCE.getTransition_Input();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__OUTPUT = eINSTANCE.getTransition_Output();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__KIND = eINSTANCE.getTransition_Kind();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '<em><b>ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__ID = eINSTANCE.getTransition_ID();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.inputImpl <em>input</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.inputImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getinput()
		 * @generated
		 */
		EClass INPUT = eINSTANCE.getinput();

		/**
		 * The meta object literal for the '<em><b>Signal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT__SIGNAL = eINSTANCE.getinput_Signal();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT__PORT = eINSTANCE.getinput_Port();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INPUT__PARAMETER = eINSTANCE.getinput_Parameter();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.outputImpl <em>output</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.outputImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getoutput()
		 * @generated
		 */
		EClass OUTPUT = eINSTANCE.getoutput();

		/**
		 * The meta object literal for the '<em><b>Signal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT__SIGNAL = eINSTANCE.getoutput_Signal();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT__PORT = eINSTANCE.getoutput_Port();

		/**
		 * The meta object literal for the '<em><b>Valuation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUTPUT__VALUATION = eINSTANCE.getoutput_Valuation();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.TopLevelNonCompositeStateImpl <em>Top Level Non Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.TopLevelNonCompositeStateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTopLevelNonCompositeState()
		 * @generated
		 */
		EClass TOP_LEVEL_NON_COMPOSITE_STATE = eINSTANCE.getTopLevelNonCompositeState();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOP_LEVEL_NON_COMPOSITE_STATE__SOURCE = eINSTANCE.getTopLevelNonCompositeState_Source();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.ContainedNonCompositeStateImpl <em>Contained Non Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.ContainedNonCompositeStateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getContainedNonCompositeState()
		 * @generated
		 */
		EClass CONTAINED_NON_COMPOSITE_STATE = eINSTANCE.getContainedNonCompositeState();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.TopLevelCompositeStateImpl <em>Top Level Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.TopLevelCompositeStateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTopLevelCompositeState()
		 * @generated
		 */
		EClass TOP_LEVEL_COMPOSITE_STATE = eINSTANCE.getTopLevelCompositeState();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.ContainedCompositeStateImpl <em>Contained Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.ContainedCompositeStateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getContainedCompositeState()
		 * @generated
		 */
		EClass CONTAINED_COMPOSITE_STATE = eINSTANCE.getContainedCompositeState();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.CompositeStateImpl <em>Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.CompositeStateImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getCompositeState()
		 * @generated
		 */
		EClass COMPOSITE_STATE = eINSTANCE.getCompositeState();

		/**
		 * The meta object literal for the '<em><b>Substate</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STATE__SUBSTATE = eINSTANCE.getCompositeState_Substate();

		/**
		 * The meta object literal for the '<em><b>Queue</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STATE__QUEUE = eINSTANCE.getCompositeState_Queue();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.QueueImpl <em>Queue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.QueueImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getQueue()
		 * @generated
		 */
		EClass QUEUE = eINSTANCE.getQueue();

		/**
		 * The meta object literal for the '<em><b>Rec Part Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEUE__REC_PART_NAME = eINSTANCE.getQueue_RecPartName();

		/**
		 * The meta object literal for the '<em><b>Rec Part Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEUE__REC_PART_TYPE = eINSTANCE.getQueue_RecPartType();

		/**
		 * The meta object literal for the '<em><b>Signal</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUEUE__SIGNAL = eINSTANCE.getQueue_Signal();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.VALImpl <em>VAL</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.VALImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getVAL()
		 * @generated
		 */
		EClass VAL = eINSTANCE.getVAL();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VAL__NAME = eINSTANCE.getVAL_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VAL__VALUE = eINSTANCE.getVAL_Value();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.EXPRESSIONImpl <em>EXPRESSION</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.EXPRESSIONImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getEXPRESSION()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getEXPRESSION();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.BinaryExpressionImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EXPRESSION__OP = eINSTANCE.getBinaryExpression_Op();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LEFT = eINSTANCE.getBinaryExpression_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RIGHT = eINSTANCE.getBinaryExpression_Right();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.ConstantImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getConstant()
		 * @generated
		 */
		EClass CONSTANT = eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '<em><b>String Representation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__STRING_REPRESENTATION = eINSTANCE.getConstant_StringRepresentation();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__VALUE = eINSTANCE.getConstant_Value();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__TYPE = eINSTANCE.getConstant_Type();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.ConstraintImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.SymbolicVariableImpl <em>Symbolic Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.SymbolicVariableImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSymbolicVariable()
		 * @generated
		 */
		EClass SYMBOLIC_VARIABLE = eINSTANCE.getSymbolicVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYMBOLIC_VARIABLE__NAME = eINSTANCE.getSymbolicVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYMBOLIC_VARIABLE__TYPE = eINSTANCE.getSymbolicVariable_Type();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.UnaryExpressionImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EXPRESSION__OP = eINSTANCE.getUnaryExpression_Op();

		/**
		 * The meta object literal for the '<em><b>Expr</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__EXPR = eINSTANCE.getUnaryExpression_Expr();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.LogicalConstraintImpl <em>Logical Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.LogicalConstraintImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getLogicalConstraint()
		 * @generated
		 */
		EClass LOGICAL_CONSTRAINT = eINSTANCE.getLogicalConstraint();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_CONSTRAINT__OP = eINSTANCE.getLogicalConstraint_Op();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_CONSTRAINT__LEFT = eINSTANCE.getLogicalConstraint_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_CONSTRAINT__RIGHT = eINSTANCE.getLogicalConstraint_Right();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.RelationalConstraintImpl <em>Relational Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.RelationalConstraintImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getRelationalConstraint()
		 * @generated
		 */
		EClass RELATIONAL_CONSTRAINT = eINSTANCE.getRelationalConstraint();

		/**
		 * The meta object literal for the '<em><b>Op</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONAL_CONSTRAINT__OP = eINSTANCE.getRelationalConstraint_Op();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONAL_CONSTRAINT__LEFT = eINSTANCE.getRelationalConstraint_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONAL_CONSTRAINT__RIGHT = eINSTANCE.getRelationalConstraint_Right();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.TrueImpl <em>True</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.TrueImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getTrue()
		 * @generated
		 */
		EClass TRUE = eINSTANCE.getTrue();

		/**
		 * The meta object literal for the '<em><b>True</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRUE__TRUE = eINSTANCE.getTrue_True();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.FalseImpl <em>False</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.FalseImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getFalse()
		 * @generated
		 */
		EClass FALSE = eINSTANCE.getFalse();

		/**
		 * The meta object literal for the '<em><b>False</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FALSE__FALSE = eINSTANCE.getFalse_False();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.impl.SignalImpl <em>Signal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.impl.SignalImpl
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getSignal()
		 * @generated
		 */
		EClass SIGNAL = eINSTANCE.getSignal();

		/**
		 * The meta object literal for the '<em><b>Signal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL__SIGNAL = eINSTANCE.getSignal_Signal();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL__PORT = eINSTANCE.getSignal_Port();

		/**
		 * The meta object literal for the '<em><b>Input Param</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL__INPUT_PARAM = eINSTANCE.getSignal_InputParam();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.ArithmeticOperator <em>Arithmetic Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.ArithmeticOperator
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getArithmeticOperator()
		 * @generated
		 */
		EEnum ARITHMETIC_OPERATOR = eINSTANCE.getArithmeticOperator();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.Type <em>Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.Type
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getType()
		 * @generated
		 */
		EEnum TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.LogicalOperators <em>Logical Operators</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.LogicalOperators
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getLogicalOperators()
		 * @generated
		 */
		EEnum LOGICAL_OPERATORS = eINSTANCE.getLogicalOperators();

		/**
		 * The meta object literal for the '{@link symbolic.execution.serialization.set.CompareOperators <em>Compare Operators</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see symbolic.execution.serialization.set.CompareOperators
		 * @see symbolic.execution.serialization.set.impl.setPackageImpl#getCompareOperators()
		 * @generated
		 */
		EEnum COMPARE_OPERATORS = eINSTANCE.getCompareOperators();

	}

} //setPackage
