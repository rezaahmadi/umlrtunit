/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>False</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.False#getFalse <em>False</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getFalse()
 * @model
 * @generated
 */
public interface False extends Constraint {
	/**
	 * Returns the value of the '<em><b>False</b></em>' attribute.
	 * The default value is <code>"FALSE"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>False</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>False</em>' attribute.
	 * @see symbolic.execution.serialization.set.setPackage#getFalse_False()
	 * @model default="FALSE" required="true" changeable="false"
	 * @generated
	 */
	Boolean getFalse();

} // False
