/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see symbolic.execution.serialization.set.setPackage#getConstraint()
 * @model abstract="true"
 * @generated
 */
public interface Constraint extends EXPRESSION {
} // Constraint
