/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EXPRESSION</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see symbolic.execution.serialization.set.setPackage#getEXPRESSION()
 * @model abstract="true"
 * @generated
 */
public interface EXPRESSION extends EObject {
} // EXPRESSION
