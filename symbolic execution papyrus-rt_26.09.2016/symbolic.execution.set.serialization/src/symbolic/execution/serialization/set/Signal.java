/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.Signal#getSignal <em>Signal</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Signal#getPort <em>Port</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Signal#getInputParam <em>Input Param</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getSignal()
 * @model
 * @generated
 */
public interface Signal extends EObject {
	/**
	 * Returns the value of the '<em><b>Signal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal</em>' attribute.
	 * @see #setSignal(String)
	 * @see symbolic.execution.serialization.set.setPackage#getSignal_Signal()
	 * @model
	 * @generated
	 */
	String getSignal();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Signal#getSignal <em>Signal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signal</em>' attribute.
	 * @see #getSignal()
	 * @generated
	 */
	void setSignal(String value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' attribute.
	 * @see #setPort(String)
	 * @see symbolic.execution.serialization.set.setPackage#getSignal_Port()
	 * @model
	 * @generated
	 */
	String getPort();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Signal#getPort <em>Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' attribute.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(String value);

	/**
	 * Returns the value of the '<em><b>Input Param</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Param</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Param</em>' containment reference.
	 * @see #setInputParam(EXPRESSION)
	 * @see symbolic.execution.serialization.set.setPackage#getSignal_InputParam()
	 * @model containment="true"
	 * @generated
	 */
	EXPRESSION getInputParam();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Signal#getInputParam <em>Input Param</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Param</em>' containment reference.
	 * @see #getInputParam()
	 * @generated
	 */
	void setInputParam(EXPRESSION value);

} // Signal
