/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.Constant#getStringRepresentation <em>String Representation</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Constant#getValue <em>Value</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.Constant#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getConstant()
 * @model
 * @generated
 */
public interface Constant extends EXPRESSION {
	/**
	 * Returns the value of the '<em><b>String Representation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Representation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Representation</em>' attribute.
	 * @see #setStringRepresentation(String)
	 * @see symbolic.execution.serialization.set.setPackage#getConstant_StringRepresentation()
	 * @model required="true"
	 * @generated
	 */
	String getStringRepresentation();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Constant#getStringRepresentation <em>String Representation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Representation</em>' attribute.
	 * @see #getStringRepresentation()
	 * @generated
	 */
	void setStringRepresentation(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Object)
	 * @see symbolic.execution.serialization.set.setPackage#getConstant_Value()
	 * @model required="true"
	 * @generated
	 */
	Object getValue();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Constant#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Object value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link symbolic.execution.serialization.set.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see symbolic.execution.serialization.set.Type
	 * @see #setType(Type)
	 * @see symbolic.execution.serialization.set.setPackage#getConstant_Type()
	 * @model required="true"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.Constant#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see symbolic.execution.serialization.set.Type
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // Constant
