/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package symbolic.execution.serialization.set;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link symbolic.execution.serialization.set.State#getName <em>Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getID <em>ID</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getSourcePartName <em>Source Part Name</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getSubsumedBy <em>Subsumed By</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getSubsumedStates <em>Subsumed States</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getVals <em>Vals</em>}</li>
 *   <li>{@link symbolic.execution.serialization.set.State#getPc <em>Pc</em>}</li>
 * </ul>
 * </p>
 *
 * @see symbolic.execution.serialization.set.setPackage#getState()
 * @model abstract="true"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see symbolic.execution.serialization.set.setPackage#getState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see symbolic.execution.serialization.set.setPackage#getState_ID()
	 * @model required="true"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.State#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>Source Part Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Part Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Part Name</em>' attribute.
	 * @see #setSourcePartName(String)
	 * @see symbolic.execution.serialization.set.setPackage#getState_SourcePartName()
	 * @model
	 * @generated
	 */
	String getSourcePartName();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.State#getSourcePartName <em>Source Part Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Part Name</em>' attribute.
	 * @see #getSourcePartName()
	 * @generated
	 */
	void setSourcePartName(String value);

	/**
	 * Returns the value of the '<em><b>Subsumed By</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subsumed By</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subsumed By</em>' attribute.
	 * @see #setSubsumedBy(String)
	 * @see symbolic.execution.serialization.set.setPackage#getState_SubsumedBy()
	 * @model
	 * @generated
	 */
	String getSubsumedBy();

	/**
	 * Sets the value of the '{@link symbolic.execution.serialization.set.State#getSubsumedBy <em>Subsumed By</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subsumed By</em>' attribute.
	 * @see #getSubsumedBy()
	 * @generated
	 */
	void setSubsumedBy(String value);

	/**
	 * Returns the value of the '<em><b>Subsumed States</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subsumed States</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subsumed States</em>' attribute list.
	 * @see symbolic.execution.serialization.set.setPackage#getState_SubsumedStates()
	 * @model
	 * @generated
	 */
	EList<String> getSubsumedStates();

	/**
	 * Returns the value of the '<em><b>Vals</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.VAL}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vals</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getState_Vals()
	 * @model containment="true"
	 * @generated
	 */
	EList<VAL> getVals();

	/**
	 * Returns the value of the '<em><b>Pc</b></em>' containment reference list.
	 * The list contents are of type {@link symbolic.execution.serialization.set.EXPRESSION}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pc</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pc</em>' containment reference list.
	 * @see symbolic.execution.serialization.set.setPackage#getState_Pc()
	 * @model containment="true"
	 * @generated
	 */
	EList<EXPRESSION> getPc();

} // State
