/*language.jj*/

options{
	STATIC = false;
}

PARSER_BEGIN(FormulaParser)

package symbolic.analysis.language.parser;	
	import symbolic.analysis.language.*;
	import java.util.*;
	import java.io.*;

	public class FormulaParser{
		public static void main(String[] args) throws ParseException, TokenMgrError, FileNotFoundException{
			java.io.InputStream input = input = new java.io.FileInputStream(args[0]);			
			FormulaParser parser = new FormulaParser(input);
			Formula result = parser.LTLformula();
		}
		
		//public static Map<String,  LocationVariable<?>> variables;
	}
	
	
PARSER_END(FormulaParser)

SKIP:{" "}
TOKEN:{<NL:"\n"| "\r" | "\r\n" > }
TOKEN : {
	<EQ:"=="> |
	<NEQ: "!="> |
	<GT: ">"> |
	<GEQ: ">="> |
	<LT:"<"> |
	<LEQ:"<="> |
	<CONJ: "AND"> |
	<DISJ: "OR"> |
	<IMPL: "IMPLY"> |
	<NOT:"NOT"> |
	<NEXT: "X"> |	
	<FIN: "F"> |
	<GLOB: "G">|
	<UNTIL: "U"> |
	<ALL: "A"> |
	<EXST: "E"> |
	<AT: "@"> |
	<DOT: ".">
}


TOKEN : {<NUMBER:(["0"-"9"])+>}
TOKEN : {<ID: ((["a"-"z","A"-"Z"])+(["0"-"9"])*)+>}


Formula LTLformula():
{
	Formula left = null;
	Formula right = null;
	PathQuantifiers pq = null;
	StateQuantifiers sq = null;	
	CompositeFormula f = null;
}


{
	(
		LOOKAHEAD(2)
		(				
			<ALL> {pq = PathQuantifiers.A;}
			|			
			<EXST> {pq = PathQuantifiers.E;}
		)
		(
			<NEXT> {sq = StateQuantifiers.X;} "(" (left=LTLformula()) ")" 
			| 
			<FIN> {sq = StateQuantifiers.F;} "(" (left=LTLformula()) ")"	
			|
			<GLOB>{sq = StateQuantifiers.G;} "("(left=LTLformula()) ")"		
			|			
			"{"(left=LTLformula()) <UNTIL> (right=LTLformula())"}" {sq=StateQuantifiers.U;}
		)		
		|
		LOOKAHEAD(2)
		(f = formula()) {return f;}
	)
	
	{return new Formula(pq,sq,left,right);}
}

CompositeFormula formula():
{	
	CompositeFormula sf = null;
}
{
	(	
	sf = compositeFormula() 
	|
	sf = atomicFormula()
	)
	
	{return sf;}
}

CompositeFormula compositeFormula():
{
	Formula left = null;
	LogicalOperator op = null;
	Formula right = null;
	AtomicStateFormula atomic= null;
}
{	
	(
	LOOKAHEAD(5)
	("("(left = formula()) <CONJ> (right = formula())")" {op=LogicalOperator.and;})
	|
	LOOKAHEAD(5)	
	("("(left = formula()) <DISJ> (right = formula())")" {op=LogicalOperator.or;})
	|
	LOOKAHEAD(5)
	"("(left = formula()) <IMPL> (right = formula())")" {op=LogicalOperator.imply;}
	|
	LOOKAHEAD(5)
	"("<NOT> (right = formula())")" {op =LogicalOperator.not;}
	)
	
	{return new CompositeFormula(left,op,right);}
}




AtomicStateFormula atomicFormula():
{
	AtomicStateFormula f = null;	
}
{
	(
	LOOKAHEAD(action())
	f = inOut()
	|
	LOOKAHEAD("[")
	f = inQueue()
	|
	LOOKAHEAD(constraint())
	f = invariant()
	|	
	LOOKAHEAD(inState())
	f = inState()
	)
	
	{return f;}
}

InOut inOut():
{
	Action input = null;
	ActionList outs = null;
	AtPart atPart = null;
}
{
	(LOOKAHEAD(2)
	(input = action())"[" (outs = actionList()) "]" <AT> (atPart = atPart())
	|
	(input = action())"[" (outs = actionList()) "]" )
	
	{return new InOut(input, outs, atPart);}
}

InQueue inQueue():
{
	ActionList actionList = null;
	AtPart atPart = null;
}
{	
	(LOOKAHEAD(2)
	"["(actionList = actionList())"]"<AT>(atPart = atPart())
	|
	"["(actionList = actionList())"]")
	{return new InQueue(actionList,atPart);}
}

ActionList actionList():
{
	Action action = null;
	ActionList rest = null;
}
{
	( LOOKAHEAD(action()"]") 
	 (action = action())
	 |
	 LOOKAHEAD(action()",")
	 (action = action()) "," (rest = actionList()) )
	 
	 {return new ActionList(action,rest);} 
}

Action action():
{
	Token port = null;
	Token signal = null;
}
{
	((port = <ID>)"."(signal = <ID>)) "()"
	
	{return new Action(port.toString(), signal.toString());}
}



InState inState():
{
	InState is = null;
	String state = null;
	AtPart at = null;
	Token id = null;
}	 
{
	((id = <ID>) <AT> (at = atPart()))
	
	{return new InState(id.toString(),at);}
}

AtPart atPart():
{	
	Token atToken = null;
	AtPart atFurther = null;
}
{
	
	(
		LOOKAHEAD(3)
		(atToken=<ID>)<DOT>(atFurther = atPart())		
		|
		(atToken = <ID>) 
		
	)
	
		
	{return new AtPart(atToken.toString(),atFurther);}
		
}

Invariant invariant():
{
	Constraint cst = null;
	AtPart atPart = null;
}
{
	(cst = constraint())<AT>(atPart = atPart()) 
	
	{return new Invariant(cst,atPart);}
}

Constraint constraint():
{
	Variable varLeft=null;
	Variable varRight=null;
	CompareOperators op=null; 	
}
{
	(
	LOOKAHEAD(2)	
	(varLeft = id())(<EQ>)(varRight = id()) {op = CompareOperators.eq;}
	|
	LOOKAHEAD(2)
	(varLeft = id())(<NEQ>)(varRight = id()) {op = CompareOperators.neq;}
	|
	LOOKAHEAD(2)
	(varLeft = id())(<GEQ>)(varRight = id()) {op = CompareOperators.geq;}
	|
	LOOKAHEAD(2)
	(varLeft = id())(<GT>)(varRight = id()) {op = CompareOperators.ge;}
	|
	LOOKAHEAD(2)
	(varLeft = id())(<LEQ>)(varRight = id()) {op = CompareOperators.leq;}
	|
	LOOKAHEAD(2)
	(varLeft = id())(<LT>)(varRight = id()) {op = CompareOperators.le;}
	)
	
	{return new Constraint(varLeft, op, varRight);}
}



Variable id():
{	
	Token number;
	Token variable;
}
{
	(
		(number = <NUMBER>) 		
		{return new Constant(number.toString());}
	|
		(variable = <ID>) 
			{return new Variable(variable.toString());}
	
		
	)
	
}


