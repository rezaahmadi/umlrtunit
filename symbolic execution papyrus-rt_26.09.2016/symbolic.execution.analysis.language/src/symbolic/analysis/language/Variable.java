package symbolic.analysis.language;

import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.ffsm.model.LocationVariable;

public class Variable {
	private String name;
	private LocationVariable<?> variable;
	
	public Variable (String name){
		this.name = name;
	}

	@Override
	public String toString() {		
		return name;
	}

	public String getName() {
		return name;
	}

	public void setVariable(LocationVariable<?> locVar) {
		this.variable = locVar;
		
	}

	public Expression getSymbolicVariable() {
		return variable.getSymbolicVar();
	}
}
