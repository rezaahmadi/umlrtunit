package symbolic.analysis.language;

//import java.util.List;
//import java.util.Set;

//import org.eclipse.uml2.uml.Property;

import symbolic.execution.ffsm.model.ActionInput;

public class InOut extends AtomicStateFormula{
	private Action input;
	
	private ActionList outputList;
	
	public InOut(Action input, ActionList outputList, AtPart atPart){
		super(atPart);
		this.input = input;
		this.outputList = outputList;
		this.atPart = atPart;
	}

	public ActionInput getInputAction() {
		return input.getActionInput();
	}
	
	public Action getActionInput(){
		return input;
	}

	@Override
	public String toString() {		
		return input.toString() + outputList.toString() + "@" + atPart.toString();
	}

	public void setInputAction(ActionInput ai) {
		// TODO: JDA: Auto-generated method stub
		
	}

	public ActionList getOutputList() {		
		return outputList;
	}


}
