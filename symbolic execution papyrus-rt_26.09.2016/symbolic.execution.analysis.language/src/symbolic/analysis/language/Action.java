package symbolic.analysis.language;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;

public class Action {
	private String port;	
	private String signal;
	
	private symbolic.execution.ffsm.model.Action action;
	
	public Action(String port, String signal){
		this.port = port;
		this.signal = signal;
	}
	
	@Override
	public String toString() {		
		return port +"."+signal;
	}

	public ActionInput getActionInput() {
		if (action instanceof ActionInput){
			return (ActionInput)action;
		}
		return null;
	}
	
	public ActionOutput getActionOutput(){
		if (action instanceof ActionOutput){
			return (ActionOutput)action;
		}
		return null;
	}

	public String getPort() {
		return port;
	}
	
	public String getSignal(){
		return signal;
	}

	public void setAction(symbolic.execution.ffsm.model.Action action) {
		this.action = action;
		
	}
	
}
