package symbolic.analysis.language;

//import java.util.HashSet;
//import java.util.List;
import java.util.Set;

//import org.eclipse.uml2.uml.Property;


public class Invariant extends AtomicStateFormula{
	
	private Constraint cst;

	
	public Invariant (Constraint cst, AtPart atPart){
		super(atPart);
		
		this.cst = cst;
	}
	
	@Override
	public String toString() {		
		return "( " + cst.toString() + " )" + "@" + atPart.toString();
	}

	public Set<Variable> getVariables() {
		return cst.getVariables();
	}

	public Constraint getCst() {		
		return cst;
	}
	
}
