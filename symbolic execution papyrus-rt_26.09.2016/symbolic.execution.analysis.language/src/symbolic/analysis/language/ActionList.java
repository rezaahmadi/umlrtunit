package symbolic.analysis.language;

import java.util.ArrayList;
import java.util.List;

import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutputInstance;

public class ActionList {
	private List<Action> actionList;
	
	
	public ActionList(Action first, ActionList rest){
		this.actionList = new ArrayList<Action>();
		
		this.actionList.add(first);
		if (rest != null){
			this.actionList.addAll(rest.actionList);
		}
	}
	
	@Override
	public String toString() {		
		return actionList.toString();
	}

	public List<Action> getActions() {		
		return actionList;
	}

	public List<ActionInput> createInputList() {
		List<ActionInput> result = new ArrayList<ActionInput>();
		for (Action action : actionList){
			result.add(action.getActionInput());
		}
		
		return result;
	}

	public boolean checkOutputs(List<ActionOutputInstance> sequence) {
		if (actionList.size() != sequence.size()){
			return false;
		}
		
		for (int i=0; i<actionList.size(); i++){
			Action act = actionList.get(i);
			if (act.getActionOutput()==null){
				return false;
			}
			if (!act.getActionOutput().equals(sequence.get(i).getActionOutput())){
				return false;
			}
		}
		
		return true;
	}

}
