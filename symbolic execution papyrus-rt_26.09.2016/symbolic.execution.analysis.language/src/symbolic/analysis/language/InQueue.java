package symbolic.analysis.language;

//import java.util.HashSet;
import java.util.List;
//import java.util.Set;

//import org.eclipse.uml2.uml.Property;

import symbolic.execution.ffsm.model.ActionInput;

public class InQueue extends AtomicStateFormula{
	//List<ActionInput> sequence;
	
	ActionList sequence;

	public InQueue (ActionList actionList, AtPart atPart){
		super(atPart);
		
		this.sequence = actionList;
	}
	public List<ActionInput> getSequence() {		
		return sequence.createInputList();
	}

	
	@Override
	public String toString() {		
		return sequence.toString() +"@"+atPart.toString();
	}

	public ActionList getActions() {
		return sequence;
	}

}
