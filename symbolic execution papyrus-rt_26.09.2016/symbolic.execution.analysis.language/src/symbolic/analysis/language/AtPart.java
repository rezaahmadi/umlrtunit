package symbolic.analysis.language;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Property;

public class AtPart {
	
	private List<String> atPart;
	
	private List<Property> atPartProperty;
	
	public AtPart(String atPart){		
		this.atPart = new ArrayList<String>();
		this.atPart.add(atPart);
	}
	
	public AtPart(String atPart, AtPart rest){
		this.atPart = new ArrayList<String>();
		this.atPart.add(atPart);
		if (rest != null)
			this.atPart.addAll(rest.atPart);
	}
		
	
	@Override
	public String toString() {		
		return atPart.toString();
	}

	public List<String> getPropertiesString() {
		return atPart;
	}

	public void setPath(List<Property> path) {
		this.atPartProperty = path;
		
	}

	public List<Property> getPath() {
		return atPartProperty;
	}

}
