package symbolic.analysis.language;

public class Formula {
	private PathQuantifiers pq;
	private StateQuantifiers sq;
	private Formula left;
	private Formula right;
	
	public Formula(){}
	
	public Formula(PathQuantifiers pq, StateQuantifiers sq, Formula left, Formula right){
		this.sq = sq;
		this.pq = pq;
		this.left = left;
		this.right = right;
	}
	
	@Override
	public String toString() {		
		return pq.toString() + " " + sq.toString() + "(" + left.toString() + 
		(right==null?"":","+right.toString()) +")";
	}

	public Formula getRight() {
		return right;
	}
	
	public Formula getLeft(){
		return left;
	}

	public PathQuantifiers getPathQ() {		
		return pq;
	}
	
	public StateQuantifiers getStateQ() {
		return sq;
	}

}
