package symbolic.analysis.language;

public class True extends CompositeFormula{
	
	@Override
	public String toString() {		
		return "True";
	}

}
