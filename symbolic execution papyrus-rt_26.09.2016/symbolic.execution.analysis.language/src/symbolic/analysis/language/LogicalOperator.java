package symbolic.analysis.language;

public enum LogicalOperator {
	and, or, imply, not

}
