package symbolic.analysis.language.tests;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

//import symbolic.analysis.language.AtomicStateFormula;
import symbolic.analysis.language.Formula;
//import symbolic.analysis.language.CompositeFormula;
import symbolic.analysis.language.parser.FormulaParser;
import symbolic.analysis.language.parser.ParseException;

public class ParsingTests {
	
	public static void main(String[] args) {
		//(NOT (a1.t()[o1.t(),o1.t()]@bla.bla.boo AND a1>3@s))
		//String formula ="(E X a.b()[a.b()]@p)";
		String formula = "A X ( E F (P @ s.t.s)) AND E F (p @ s)";
		InputStream input = createStream(formula);
		
		FormulaParser fp = new FormulaParser(input);
		
		try {
			Formula sf = fp.LTLformula();
			System.out.println(sf.getClass().getName());
			System.out.println(sf);
		} catch (ParseException e) {
			System.out.println("JDA: ParseEception in ParsingTests.main.");
			e.printStackTrace();
		}
		
		
	}
	
	private static InputStream createStream(String code) {
		char[] charArray = code.toCharArray();
		byte[] byteArray = new byte[charArray.length];
		for (int i=0; i <byteArray.length; i++){
			byteArray[i] = (byte) charArray[i];
		}
		
		return new ByteArrayInputStream(byteArray);
	}

}
