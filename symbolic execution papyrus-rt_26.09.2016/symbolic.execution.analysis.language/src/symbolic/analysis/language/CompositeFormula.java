package symbolic.analysis.language;

public class CompositeFormula extends Formula{
	private Formula left;
	private LogicalOperator op;
	private Formula right;
	
	
	public CompositeFormula(){}
	
	public CompositeFormula(Formula left, LogicalOperator op, Formula right){
		this.left = left;
		this.op = op;
		this.right = right;
	}
	
	@Override
	public String toString() {		
		return (left==null ? "" : left.toString()) + " " + op.toString() + " " + right.toString();
	}
	
	public Formula getLeft(){
		return left;
	}
	
	public Formula getRight(){
		return right;
	}

	public LogicalOperator getOperator() {
		return op;
	}
	

}
