package symbolic.analysis.language;

//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;

//import org.eclipse.uml2.uml.Property;

import symbolic.execution.ffsm.model.Location;

public class InState extends AtomicStateFormula {
	
	private String stateName;
	private Location state;

	public InState(String state, AtPart at) {
		super(at);
		this.stateName = state;
	}
	
	public String getStateName(){
		return stateName;
	}

	@Override
	public String toString() {		
		return stateName +"@"+atPart.toString();
	}

	public void setLocation(Location l) {
		this.state = l;
		
	}

	public Location getLocation() {
		return state;
	}

}
