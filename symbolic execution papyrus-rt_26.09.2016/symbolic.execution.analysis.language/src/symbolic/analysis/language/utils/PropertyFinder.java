package symbolic.analysis.language.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
//import java.util.concurrent.ArrayBlockingQueue;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

import symbolic.execution.utils.UmlrtUtil;

//import com.ibm.xtools.uml.rt.core.Class;
//import com.ibm.xtools.uml.rt.core.RTCapsulePart;
//import com.ibm.xtools.uml.rt.core.RTFactory;

public class PropertyFinder {
	
	private Class capsule;
	
	public PropertyFinder(Class capsule){
		this.capsule = capsule;
	}
	
	public List<Property> findPropertyInCapsule(String property, Class capsule, List<Property> current) 
		throws IncorrectHierarchyException{
		
		int dotIndex = property.indexOf('.');
		if (dotIndex == -1){
			Property p = findPart(property, capsule);
			List<Property> currentNew = new ArrayList<Property>(current);
			currentNew.add(p);
			return  currentNew;
		}
		
		//first property to find 
		String headPart = property.substring(0, property.indexOf('.'));
		
		//property
		Property part = findPart(headPart, capsule);		
		if (part == null){
			throw new IncorrectHierarchyException(property);
		}
		
		//capsule for the property
		Class partClass = (Class) part.getType();
		Class partCapsule = UmlrtUtil.getCapsule(partClass);// RTFactory.CapsuleFactory.getCapsule(partClass);
		
		//generate path
		List<Property> currentNew = new ArrayList<Property>(current);
		currentNew.add(part);
		
		
		return findPropertyInCapsule(property.substring(dotIndex+1), partCapsule, currentNew);
		
	}

	

	private Property findPart(String property, Class capsule) {
		for (Property part : UmlrtUtil.getCapsuleParts(capsule)){			
			
			if  (part.getName().equals(property)){
				return part;
			}
		}
		return null;
	}
	
//	private Set<RTCapsulePart> extractParts(Class container) {
//		//display roles:
//		
//		Set<RTCapsulePart> result = new HashSet<RTCapsulePart>();
//		if (container.getAllRoles() == null){
//			return result;
//		}
//		System.out.println("Capsules contained in " + container.getUML2Class().getName());
//		
//		//discover all parts
//		for (RTCapsulePart part : container.getAllRoles()){
//			if (part != null && part.getType() != null && part.getType().getValue() instanceof Class){				
//				result.add(part);
//			}	
//		}		
//		
//		return result;
//	}

}
