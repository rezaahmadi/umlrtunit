package symbolic.analysis.language.utils;

//TODO: JDA: Why the warning?
public class IncorrectInputActionException extends Exception{

	public IncorrectInputActionException(String action){
		super("Action " + action + " has not been found");
	}
}
