package symbolic.analysis.language.utils;

//TODO: JDA: Why the warning?
public class IncorrectHierarchyException extends Exception {
	
	public IncorrectHierarchyException(String path) {
		super (path + " is incorrect path in hierarchy of capsules");
	}

}
