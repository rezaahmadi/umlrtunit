package symbolic.analysis.language;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;

public class Constraint {
	private Variable variableLeft;
	private CompareOperators op;
	private Variable variableRight;
	
	private symbolic.execution.constraints.manager.Constraint repr;
	
	public Constraint(Variable left, CompareOperators op, Variable right){
		variableLeft = left;
		this.op = op;
		variableRight = right;
		
	}
	
	private symbolic.execution.constraints.manager.operators.CompareOperators translate(
			CompareOperators op) {
		switch (op){
		case eq : return symbolic.execution.constraints.manager.operators.CompareOperators.eq;
		case neq : return symbolic.execution.constraints.manager.operators.CompareOperators.neq;
		case geq : return symbolic.execution.constraints.manager.operators.CompareOperators.geq;
		case leq : return symbolic.execution.constraints.manager.operators.CompareOperators.leq;
		case ge : return symbolic.execution.constraints.manager.operators.CompareOperators.ge;
		case le: return symbolic.execution.constraints.manager.operators.CompareOperators.le;
		}
		return null;
	}

	private Expression translate(Variable var) {
		if (var instanceof Constant){
			Constant c = (Constant) var;
			return SymbolicFactory.createConstant(Integer.class, c.getValue());
		}
		return var.getSymbolicVariable();
	}

	@Override
	public String toString() {		
		return variableLeft.toString() + op + variableRight.toString();
	}

	public Set<Variable> getVariables() {
		Set<Variable> vars = new HashSet<Variable>();
		vars.add(variableLeft);
		vars.add(variableRight);
		
		return vars;
	}

	public symbolic.execution.constraints.manager.Constraint replaceVariables(
			Map<SymbolicVariable, Expression> valuations) {
		if (repr == null){
			Expression leftC = translate(variableLeft);
			symbolic.execution.constraints.manager.operators.CompareOperators opC = translate(op);
			Expression rightC = translate(variableRight);
			
			repr = SymbolicFactory.createRelationalConstraint(leftC, opC, rightC);
		}
		return (symbolic.execution.constraints.manager.Constraint) repr.replaceVariables(valuations);
	}

}
