package symbolic.analysis.language;

public class Constant extends Variable {
	private String value; 
	
	public Constant(String value) {
		super(value);
		this.value = value;
	}

	@Override
	public String toString() {		
		return value;
	}

	public Object getValue() {
		return Integer.parseInt(value);
	}
}
