package symbolic.analysis.language;


//import java.util.List;
//import java.util.Set;

//import org.eclipse.uml2.uml.Property;

//import com.ibm.xtools.uml.rt.core.Capsule;

//import symbolic.analysis.language.utils.IncorrectFormulaException;
//import symbolic.analysis.language.utils.IncorrectHierarchyException;
//import symbolic.analysis.language.utils.PropertyFinder;

public abstract class AtomicStateFormula extends CompositeFormula{
	
	//protected List<Property> atPartProperty;
	
	protected AtPart atPart;
	
	public AtomicStateFormula(AtPart atPart){
		this.atPart = atPart;
	}
	
	/*public AtomicStateFormula(List<Property> atPart) {
		this.atPartProperty = atPart;
	}*/
	
	
	public AtPart getAtPart(){
		return atPart;
	}
	//public abstract Set<Property> getInitialParts();

	/*public static StateFormula create(String f, Capsule capsule) throws IncorrectFormulaException{
		PropertyFinder pf = new PropertyFinder(capsule);
		if (f.startsWith("a")){
			String part = f.substring(f.indexOf('_')+1);
			Property p;
			try {
				p = pf.findPropertyInCapsule(part, capsule);
			} catch (IncorrectHierarchyException e) {
				// XODO Auto-generated catch block
				e.printStackTrace();
				throw new IncorrectFormulaException(e.getMessage());
			}
			InState s= new InState(p);	
			return s;
		} else if (f.equals("b")) {
			String part = f.substring(f.indexOf('_')+1);
			Property p;
			try {
				p = pf.findPropertyInCapsule(part, capsule);
			} catch (IncorrectHierarchyException e) {
				// XODO Auto-generated catch block
				e.printStackTrace();
				throw new IncorrectFormulaException(e.getMessage());
			}
			InState s= new InState(p);	
			return s;						
			
		} else if (f.equals("c")) {
			
			
		} else if (f.equals("d")) {
			
		}
		return null;
	}*/

}
