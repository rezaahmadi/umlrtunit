package symbolic.execution.tree.synch.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.FunctionalFiniteStateMachine;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.LocationVariable;

public class SymbolicExecutionTreeSynch extends SymbolicExecutionTree {

	private Map<String, FunctionalFiniteStateMachine> roles;
	
	private Map<String, SymbolicExecutionTree> rolesTree;
	
	private SymbolicExecutionTree mainTree;
	
	private Set<String> directChildren;
		
	public SymbolicExecutionTreeSynch(NodeSymbolic root,
			FunctionalFiniteStateMachine ffsm, SymbolicExecutionTree mainTree) {
		super(root, ffsm);
		
		roles = new HashMap<String, FunctionalFiniteStateMachine>();
		rolesTree = new HashMap<String, SymbolicExecutionTree>();
		this.mainTree = mainTree;
	}
	
	public void addRole(String name, FunctionalFiniteStateMachine ffsm, SymbolicExecutionTree tree){
		roles.put(name, ffsm);
		rolesTree.put(name, tree);
	}
	
	public SymbolicExecutionTreeSynch(SymbolicExecutionTreeSynch other) {
		super(other.ffsm);
		this.locations = new HashSet<Location>(other.locations);
		this.roles = other.roles;
		this.rolesTree = other.rolesTree;
		
		
		//copy main tree
		this.mainTree = new SymbolicExecutionTree(other.mainTree);
		Map<ActionInput, ActionInput> inputsMap = new HashMap<ActionInput, ActionInput>();
		Map<ActionOutput, ActionOutput> outputsMap = new HashMap<ActionOutput, ActionOutput>();
		createTreeMaps(mainTree, other.mainTree, outputsMap, inputsMap);
		
		//copy included trees
		this.rolesTree = other.rolesTree;
		/*for (String part : rolesTree.keySet()){
			SymbolicExecutionTree tree = rolesTree.get(part);
			
			SymbolicExecutionTree copied = null;
			if (tree instanceof SymbolicExecutionTreeSynch){
				copied = new SymbolicExecutionTreeSynch((SymbolicExecutionTreeSynch) tree, inputsMap, outputsMap);
			} else {
				copied = new SymbolicExecutionTree(tree, inputsMap, outputsMap);
			}
			
			rolesTree.put(part,copied);
				
		}*/
			
		//copy this tree
		this.root = new NodeSymbolicSynch((NodeSymbolicSynch)other.root, this, true, inputsMap, outputsMap);
		this.treeName = other.treeName;
		
		
		
	}
	
	public SymbolicExecutionTreeSynch(SymbolicExecutionTreeSynch other,
			Map<ActionInput, ActionInput> inputsMap,
			Map<ActionOutput, ActionOutput> outputsMap) {
		super(other.ffsm);
		this.locations = new HashSet<Location>(other.locations);
		this.roles = other.roles;
		this.rolesTree = other.rolesTree;
				
		//copy main tree
		this.mainTree = new SymbolicExecutionTree(other.mainTree);
		createTreeMaps(mainTree, other.mainTree, outputsMap, inputsMap);
		
		//copy included trees
		for (String part : rolesTree.keySet()){
			SymbolicExecutionTree tree = rolesTree.get(part);
			
			SymbolicExecutionTree copied = null;
			if (tree instanceof SymbolicExecutionTreeSynch){
				copied = new SymbolicExecutionTreeSynch((SymbolicExecutionTreeSynch) tree, inputsMap, outputsMap);
			} else {
				copied = new SymbolicExecutionTree(tree, inputsMap, outputsMap);
			}
			rolesTree.put(part, copied);
				
		}
			
		//copy this tree
		this.root = new NodeSymbolicSynch((NodeSymbolicSynch)other.root, this, true, inputsMap, outputsMap);
		this.treeName = other.treeName;
		
		
		
	}

	@Override
	public String getName() {		
		return super.getName() + "Synchronized";
	}
	
	@Override
	public Map<String, LocationVariable<?>> getLocationsVarsMapping() {
		Map<String,LocationVariable<?>> result = super.getLocationsVarsMapping();
		
		for (String roleName : roles.keySet()){
			if (!roleName.isEmpty()){
				Map<String,LocationVariable<?>> vars = roles.get(roleName).getLocationVarsMap();
			
				for (String varName : vars.keySet()){
					result.put(roleName+"."+varName, vars.get(varName));
				}
			}
		}
		
		return result;
	}
	
	@Override
	public Map<String,ActionOutput> getOutputActions() {
		Map<String,ActionOutput> result = super.getOutputActions();
		
		//get from the main tree
		result.putAll(ffsm.getActionsOutputMap());
		
		//get from roles
		for (String roleName : roles.keySet()){
			if (!roleName.isEmpty()){
				Map<String,ActionOutput> roleMap = roles.get(roleName).getActionsOutputMap();
			
				for (String actionName : roleMap.keySet()){
					result.put(roleName+"."+actionName, roleMap.get(actionName));
				}
			}
		}
		return result;
	}
	
	public Map<String,Map<String,Location>> getLocationsMap(){
		Map<String,Map<String,Location>> result = new HashMap<String, Map<String,Location>>();
		for (String roleName : roles.keySet()){
			Map<String,Location> locations = roles.get(roleName).getLocationsMap();
			result.put(roleName,locations);
		}
		return result;
	}

	public Map<String, SymbolicExecutionTree> getTreesMap() {
		return new HashMap<String, SymbolicExecutionTree>(rolesTree);
	}

	public List<SymbolicPath> findPaths(
			Map<SymbolicExecutionTree, Location> source,
			Map<SymbolicExecutionTree, Location> target, boolean isSymbolic) {
		
		List<SymbolicPath> paths = new ArrayList<SymbolicPath>();
		
		SymbolicPath current = new SymbolicPath();
		getRoot().exploreReachable(source, target, current, paths);
		
		return paths;
	}

	public List<String> getTreeNames() {
		List<String> treeNames = new ArrayList<String>(rolesTree.keySet());
		treeNames.add(getName());
		return treeNames;
	}

	public SymbolicExecutionTree projectTree(String selected) {
		//find original tree
		SymbolicExecutionTree original = rolesTree.get(selected);
		
		if (original == null && selected.equals(getName())){
			original = mainTree;
		}
		
		SymbolicExecutionTree projected = new SymbolicExecutionTree(original.getFFSM());
		
		//create root
		NodeSymbolic rootNew = ((NodeSymbolicSynch)root).createProjectionNode(selected, projected);
		projected.setRoot(rootNew);
				
		((NodeSymbolicSynch)root).projectNode(rootNew, projected, original, selected);
		
		return projected;
	}
	
	@Override
	public ActionInput mapInputAction(ActionInput action) {		
		return mainTree.mapInputAction(action);
	}
	
	@Override
	public ActionOutput mapOutputAction(ActionOutput action) {		
		return mainTree.mapOutputAction(action);
	}
		
}
