package symbolic.execution.tree.synch.model;

import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;

public class SentMessage {
	private ActionInputSymbolic action;
	private SymbolicExecutionTree receiver;
	
	public SentMessage(ActionInputSymbolic action, SymbolicExecutionTree receiver){
		this.action = action;
		this.receiver = receiver;
	}

	public ActionInputSymbolic getAction() {
		return action;
	}

	public SymbolicExecutionTree getReceiver() {
		return receiver;
	}
	
	@Override
	public String toString() {		
		return action.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof SentMessage){
			SentMessage other = (SentMessage) o;
			
			return action.equals(other.action) && receiver.equals(other.receiver);
		}
		return super.equals(o);
	}

}
