package symbolic.execution.tree.synch.model;

import java.util.ArrayList;
import java.util.Collection;
//import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.ExtendedSymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicStateAuxiliary;
import symbolic.execution.tree.synch.QueueManager;
import symbolic.execution.tree.synch.SynchronizationState;

public class SymbolicStateSynch extends SymbolicState{
//public class SymbolicStateSynch extends ExtendedSymbolicState{
	
	private Map<String, SymbolicState> parts;
	//private Map<String, ExtendedSymbolicState> parts;
	
	private Map<String, SymbolicExecutionTree> roles;
	
	//JDA:
	//private SymbolicStateAuxiliary auxiliary = new SymbolicStateAuxiliary();
	
	//JDA: local copies was for debug and is commented out.
	//private Map<String, SymbolicExecutionTree> localCopies;
	
	private QueueManager queues;
	
	private SymbolicExecutionTree treeSynch;
	
	public SymbolicStateSynch(SymbolicState treeState,Map<String, SymbolicState> parts,
			QueueManager queues, Map<String, SymbolicExecutionTree> roles, SymbolicExecutionTreeSynch tree) {
		super(treeState);
		//Set<String> ks = parts.keySet();
		
		//JDA: begin replacement
		//JDA: replace:
		this.parts = parts;
		//JDA: by
		//this.parts = new HashMap<String, ExtendedSymbolicState>();
		//for (String str: ks) {
		//	SymbolicState s = parts.get(str);
		//	ExtendedSymbolicState es = new ExtendedSymbolicState(s);
		//	this.parts.put(str, es);
		//	es.setLocalPC(auxiliary.copyConstraintList(s.getPC()));
		//	es.setLocalValuation(auxiliary.copyValuationSymbolic(s.getValuation()));
		//}
		//JDA: end replacement
		this.queues = queues;
		this.roles = roles;
		this.treeSynch = tree;
		System.out.print("");
	}
	
	public SymbolicStateSynch(SymbolicStateSynch state, SymbolicExecutionTreeSynch tree){
		super(state);
		
		this.parts = state.parts;
		this.queues = state.queues;
		this.roles = state.roles;
		this.treeSynch = tree;
	}

	public SymbolicStateSynch(SynchronizationState initial, Map<String,SymbolicExecutionTree> roles, SymbolicExecutionTreeSynch tree) {
		this(initial.getTreeState(), initial.getParts(roles), initial.getQueues(), roles, tree);
	}

	public Map<String, SymbolicState> getParts() {
	//public Map<String, ExtendedSymbolicState> getParts() {
		return parts;
	}

	public void setParts(Map<String, SymbolicState> parts) {
	//public void setParts(Map<String, ExtendedSymbolicState> parts) {
		this.parts = parts;
	}

	public QueueManager getQueues() {
		return queues;
	}

	public void setQueues(QueueManager queues) {
		this.queues = queues;
	}

	public Collection<SymbolicState> getIncludedStates() {
	//public Collection<ExtendedSymbolicState> getIncludedStates() {
		return parts.values();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//JDA: and the master param, 'this'
		sb.append(super.toString(this));
		sb.append("\n");
		sb.append(queues.toString());		
		sb.append("\n");
		//sb.append(parts.toString());
		
		return sb.toString();
	}
	
	public String toString(Object master) {
		StringBuffer sb = new StringBuffer();
		//JDA: added the 'master' param.
		sb.append(super.toString(master));
		sb.append("\n");
		sb.append(queues.toString());		
		sb.append("\n");
		//sb.append(parts.toString());
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof SymbolicStateSynch){
			SymbolicStateSynch other = (SymbolicStateSynch) o;
			
			if (! super.equals(other)){
				return false;
			}
			
			//check queues for tree node
			Queue q = other.queues.getQueue(other.tree); 
			if (q==null || !q.equals(this.queues.getQueue(tree))){
				return false;
			}
			Set<String> allParts = new HashSet<String>(parts.keySet());
			allParts.addAll(other.parts.keySet());		
					
			for (String part : allParts){
				SymbolicState stateThis = parts.get(part);
				SymbolicState stateOther = other.parts.get(part);
				
				if (stateThis.equals(stateOther)){
					Queue qThis = queues.getQueue(roles.get(part)); 
					Queue qOther = other.queues.getQueue(other.roles.get(part));
					
					if (!qThis.equals(qOther)){
						return false;
					}
				}else {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public String printQueues() {
		return queues.toString(); 		
	}
	
	@Override
	public List<Constraint> getPC() {
		List<Constraint> result = new ArrayList<Constraint>();
		result.addAll(super.getPC());
		
		for (SymbolicState state : parts.values()){
			result.addAll(state.getPC());
		}
		return result;
	}

}
