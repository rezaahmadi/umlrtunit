package symbolic.execution.tree.synch.model;

import java.util.ArrayList;
//import java.util.HashSet;
import java.util.List;
import java.util.Map;
//import java.util.Set;

//import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.ExtendedNodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
//import symbolic.execution.tree.synch.QueueManager;
import symbolic.execution.tree.synch.SynchronizationAction;
import symbolic.execution.tree.synch.SynchronizationState;

public class NodeSymbolicSynch extends NodeSymbolic{
//public class NodeSymbolicSynch extends ExtendedNodeSymbolic{

	Arc<SymbolicTransition, SymbolicState> incomingArc = null;
	
	public NodeSymbolicSynch(SymbolicStateSynch contents) {
		super(contents);
		//TODO: JDA: Is this auto generated method stubs right?
	}

	public NodeSymbolicSynch(SynchronizationState initial, Map<String, SymbolicExecutionTree> roles, SymbolicExecutionTreeSynch tree) {
		super(new SymbolicStateSynch(initial, roles,tree));
		//System.out.print("");
	}
	
	
	public NodeSymbolicSynch(NodeSymbolicSynch other, SymbolicExecutionTreeSynch tree, boolean deep) {	
		//System.out.println("Copying " + other.getFirstContents().getLocation().getName());
		//copy contents 
		super(new SymbolicStateSynch(other.getContents(), tree));
		
		//if subsumes inform old subsumed		
		for(NodeSymbolic sub : other.subsumedNodes){
			sub.setSubsumedByCopy(this);
		}
		
		//if subsumes inform new subsumes
		for (NodeSymbolic sub : other.subsumedNodesCopy){
			sub.setSubsumedBy(this);
		}
		
		subsumedNodes.addAll(other.subsumedNodesCopy);
		
		//if is subsumed
		if (other.subsumedBy != null){
			//subsuming already processed 
			if (other.subsumedByCopy != null){
				subsumedBy = other.subsumedByCopy;
				subsumedBy.addSubsumed(this);
			} else {
			//subsuming not processed yet
				other.subsumedBy.addSubsumedCopy(this);
			}
		}
				
		
		//copy arcs
		if (deep){
			for(Arc<SymbolicTransition,SymbolicState> arc : other.getChildren()){
				SymbolicTransition t = new SymbolicTransition(arc.getContents(), tree);
				addChild( new NodeSymbolicSynch((NodeSymbolicSynch) arc.getTarget(), tree, deep), t);
			}
		}
		
		
	}

	public NodeSymbolicSynch(NodeSymbolicSynch other,
			SymbolicExecutionTreeSynch tree, boolean deep,
			Map<ActionInput, ActionInput> inputsMap,
			Map<ActionOutput, ActionOutput> outputsMap) {
		super(new SymbolicStateSynch(other.getContents(), tree));
		//inform subsumed		
		for(NodeSymbolic sub : other.subsumedNodes){
			sub.setSubsumedBy(this);
		}
		subsumedNodes = new ArrayList<NodeSymbolic>();
		//copy arcs
		if (deep){
			for(Arc<SymbolicTransition,SymbolicState> arc : other.getChildren()){
				SymbolicTransition oldTransition = arc.getContents();
				SymbolicTransition t = null;
				if (oldTransition.getInputExecution() instanceof SynchronizationAction){
					t = new SymbolicTransition(oldTransition, tree,outputsMap);
				} else {
					t = new SymbolicTransition(oldTransition, tree,outputsMap,inputsMap );
				}
				addChild( new NodeSymbolicSynch((NodeSymbolicSynch) arc.getTarget(), tree, deep, inputsMap, outputsMap), t);
			}
		}
		
		//set subsumedBy and subsumed
		subsumedBy = other.subsumedByCopy;
		if(subsumedBy != null)
			subsumedBy.addSubsumed(this);
	}
	
	/*
	@Override
	public String getFullNodeSymbolicAsString() {
		return(
			"[NodeSymbolicSync: " + java.lang.System.identityHashCode(this) + " class name is: " + this.getClass().getName() + "\n" +
			"   contents are " + java.lang.System.identityHashCode(getContents()) + " [" + this.getContents().toString() + "]\n" +
			"   children (outgoing arcs) are " + java.lang.System.identityHashCode(this.getChildren()) + " [" + this.getChildren() + "]\n" +
			"   isVisited is [" + isVisited + "]\n" +
			"   subsumedBy is [" + (subsumedBy == null ? "<null>" : subsumedBy.toString()) + "]\n" +
			"   subsumedByCopy is [" + (subsumedByCopy == null ? "<null>" : subsumedByCopy.toString()) + "]\n" +
			"   subsumedNodes are [" + subsumedNodes.toString() + "]\n" +
			"   subsumedNodesCopy is [" + subsumedNodesCopy.toString() + "]]\n");	
	}
	*/
	
	public void setIncomingArc(Arc<SymbolicTransition, SymbolicState> arc) {
		incomingArc = arc;
	}

	public Arc<SymbolicTransition, SymbolicState> getIncomingArc() {
		return(incomingArc);
	}
	
	@Override
	public String toString() {						
		return getContents().toString(this);
	}
	
	@Override
	public String printInLines() {	
		String str = "";
		Arc<SymbolicTransition, SymbolicState> arc = incomingArc;
		if (arc != null) {
			SymbolicTransition transition = arc.getContents();
			str += "input:  " + transition.getInputExecution().toString() + "\n";
			str += "output: " + transition.getOuputSequence().toString() + "\n";	
		}
		SymbolicStateSynch contents = getContents();
		str += contents.toString();
		return str;
		//return getContents().toString();
	}
	
	public String printInLines(Object master) {	
		String str = "";
		Arc<SymbolicTransition, SymbolicState> arc = incomingArc;
		if (arc != null) {
			SymbolicTransition transition = arc.getContents();
			str += "input:  " + transition.getInputExecution().toString() + "\n";
			str += "output: " + transition.getOuputSequence().toString() + "\n";	
		}
		SymbolicStateSynch contents = getContents();
		str += contents.toString(master);
		return str;
		//return getContents().toString();
	}
	
	@Override
	public SymbolicStateSynch getContents() {
		return (SymbolicStateSynch) super.getContents();
	}
	
	@Override
	public List<SymbolicState> getContentsAll() {
		List<SymbolicState> result = new ArrayList<SymbolicState>();
		result.add(getContents());
		result.addAll(getContents().getIncludedStates());
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof NodeSymbolicSynch){
			NodeSymbolicSynch other = (NodeSymbolicSynch) o;
			
			return other.getContents().equals(getContents());
		}
		return false;
	}

	public NodeSymbolic createProjectionNode(String selected, SymbolicExecutionTree tree) {
		SymbolicState state = getContents().getParts().get(selected);
		if (state == null){
			state = getContents();
		}
		state.setTree(tree);
		NodeSymbolic result = new NodeSymbolic(state); 
		return result;
	}

	public void projectNode(NodeSymbolic current, SymbolicExecutionTree projectedTree,
			SymbolicExecutionTree originalTree, String part) {
		for (Arc<SymbolicTransition, SymbolicState> arc : getChildren()){
			SymbolicTransition transition = arc.getContents();
			
			//synch target
			NodeSymbolicSynch target = (NodeSymbolicSynch) arc.getTarget();
			
			
			if (transition.getTree()==originalTree || transition.getInputExecution().getAction().getName() == "default"){
				//project target				
				NodeSymbolic projectedTarget = target.createProjectionNode(part, projectedTree);
				
				//add child
				
				current.addChild(projectedTarget, new SymbolicTransition(transition,projectedTree));
				
				target.projectNode(projectedTarget, projectedTree, originalTree, part);
			} else {
				target.projectNode( current, projectedTree, originalTree, part);
			}
		}

		
	}
}
