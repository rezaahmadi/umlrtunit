package symbolic.execution.tree.synch.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;

public class RelayActions {
	
	private Map<ActionInput,ActionInput> inputsMap;
	
	private Map<ActionOutput,ActionOutput> outputsMap;
	
	private Set<SymbolicExecutionTree> trees;
		
	public RelayActions(){
		inputsMap = new HashMap<ActionInput, ActionInput>();
		outputsMap = new HashMap<ActionOutput, ActionOutput>();
		trees = new HashSet<SymbolicExecutionTree>();
	}

	public void putInput(ActionInput inner, ActionInput outer){
		inputsMap.put(inner, outer);
	}
	
	public void putOutput(ActionOutput inner, ActionOutput outer){
		outputsMap.put(inner, outer);
	}
	
	public boolean containRelayInput(ActionInput inner){
		return inputsMap.containsKey(inner);
	}
	
	public ActionInput getOuterInput(ActionInput inner){
		return inputsMap.get(inner);
	}
	
	public boolean containsRelayOutput(ActionOutput inner){
		return outputsMap.containsKey(inner);
	}
	
	public ActionOutput getOuterOutput(ActionOutput inner){
		return outputsMap.get(inner);
	}

	public void addInnerTree(SymbolicExecutionTree innerTree) {
		trees.add(innerTree);
		
	}

	public Set<SymbolicExecutionTree> getTrees() {
		return trees;
	}

	public boolean containsTree(SymbolicExecutionTree tree) {		
		return trees.contains(tree);
	}
}
