package symbolic.execution.tree.synch.model;

import java.util.ArrayList;
import java.util.List;

import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;

public class Queue {
	
	private List<SentMessage> queue;
	
	public Queue(){
		queue = new ArrayList<SentMessage>();
	}
	
	public Queue(Queue other) {		
		queue = new ArrayList<SentMessage>(other.getList());
	}

	public void putInQueue(ActionInputSymbolic action, SymbolicExecutionTree receiver){
		if (queue.size()<=5){
			queue.add(new SentMessage(action,receiver));
		}
	}

	public SentMessage getFrom(){
		if (queue.isEmpty()){
			return null;
		}
		return queue.remove(0);
	}
	
	public SentMessage peek(){
		if (queue.isEmpty()){
			return null;
		}
		return queue.get(0);
	}

	public boolean isEmpty() {		
		return queue.isEmpty();
	}

	public List<SentMessage> getList() {		
		return new ArrayList<SentMessage>(queue);
	}
	
	@Override
	public String toString() {		
		return queue!=null ? queue.toString() : "null";
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Queue){
			Queue other = (Queue) o;
			if (queue.size()!=other.queue.size()){
				return false;
			}
			for (int i=0; i<queue.size(); i++){
				if (!queue.get(i).equals(other.queue.get(i)))
					return false;
			}
			return true;
		}
		return false;
	}
	
}
