package symbolic.execution.tree.synch;

import org.eclipse.uml2.uml.Port;

import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;

public class TreeConnector {
	private SymbolicExecutionTree base;
	
	private Port basePort;
	
	private SymbolicExecutionTree conjugated;
	
	private Port conjugatedPort;
	
	public TreeConnector(Port basePort, Port conjugatedPort){
		this.basePort = basePort;
		this.conjugatedPort = conjugatedPort;
	}
	
	public void setBaseTree(SymbolicExecutionTree base){
		this.base = base;
	}
	
	public void setConjugatedTree(SymbolicExecutionTree conjugated){
		this.conjugated = conjugated;
	}
	
	@Override
	public String toString() {
		
		return base +" ["+basePort.getName()+"] --- ["+conjugatedPort.getName()+"] "+conjugated;
	}

}
