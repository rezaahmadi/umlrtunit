package symbolic.execution.tree.synch;

import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
//import java.util.Set;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
//import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
//import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.ModelVariable;
//import symbolic.execution.tree.synch.Synchronizer;
//import symbolic.execution.tree.synch.SynchronizationState;

//JDA: begin addition for constants to support 'sync-fix'.
//TODO: JDA: Make more efficient. Improve documentation.
/*
 * JDA: 1) The path conditions and valuations of visited targets
 *         were being corrupted because of shared references.
 *      2) This corruption between the time a new state was
 *         queued and the time it was dequeued.
 *      3) This patches at least some cases of this problem.
 *      TODO: JDA: Test
 *      TODO: JDA: Make more efficient.
 *      TODO: JDA: Ensure the patch is applied when displaying and saving ...
 *      TODO: JDA: states. 
 */
public class SynchronizerAuxiliary {


	//JDA: This code deep copies symbolic valuations.
	ValuationSymbolic copyValuationSymbolic(ValuationSymbolic vs) {
		ValuationSymbolic res = new ValuationSymbolic();
		Map<SymbolicVariable, Expression> var = vs.getSymbolicVariables();
		for (Entry<SymbolicVariable, Expression> en: var.entrySet()) {
			Expression ex = en.getValue();
			SymbolicVariable v = en.getKey();
			ModelVariable<?> mv = vs.getVariableByName(v.getName());
			res.setValue((ModelVariable<?>)mv, copyExpression(ex));
		}
		return res;
	}
	//JDA: This code deep copies path constraints.
	List<Constraint> copyConstraintList(List<Constraint> lc)  {
		List<Constraint> nlc = new ArrayList<Constraint>();
		for (Constraint c: lc) {
			Constraint nc = (Constraint)copyExpression(c);
			nlc.add(nc);
		}
		return nlc;
	}
	
	//JDA: This code deep copies expressions.
	private Expression copyExpression (Expression exp) {
		//TODO: JDA: This may not handle all possible expression implementations.
		//           E.g., for choco.
		Expression res = null;
		if (exp instanceof BinaryExpression) {
			if ((symbolic.execution.constraints.choco.BinaryExpressionImpl) exp 
					instanceof symbolic.execution.constraints.choco.BinaryExpressionImpl) {
				Expression l = ((BinaryExpression) exp).getLeft();
				Expression r = ((BinaryExpression) exp).getRight();
				ArithmeticOperators op = ((BinaryExpression) exp).getOp();
				res = new symbolic.execution.constraints.choco.BinaryExpressionImpl(
						copyExpression(l), op, copyExpression(r));
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof UnaryExpression ){
			ArithmeticOperators op = ((UnaryExpression) exp).getOp();
			Expression e = ((UnaryExpression) exp).getExpression();
			res = new UnaryExpression(op, copyExpression(e));
		}
		else if (exp instanceof Constant){
			Object v = ((Constant) exp).getValue();
			if ((symbolic.execution.constraints.choco.ConstantImpl) exp 
					instanceof symbolic.execution.constraints.choco.ConstantImpl) {
				res = new symbolic.execution.constraints.choco.ConstantImpl(v.getClass(), v);
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof Constraint){
			if (exp instanceof symbolic.execution.constraints.manager.False) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.False();
			}
			else if (exp instanceof symbolic.execution.constraints.manager.True) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.True();
			}
			else if (exp instanceof LogicalConstraint) {
				if (((symbolic.execution.constraints.choco.LogicalConstraintImpl)exp) instanceof  
						symbolic.execution.constraints.choco.LogicalConstraintImpl) {
					LogicalOperators op = ((LogicalConstraint) exp).getOp();
					Constraint l = ((LogicalConstraint) exp).getLeft();
					Constraint r = ((LogicalConstraint) exp).getRight();
				
					if (l == null) {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl(null, op, (Constraint) copyExpression(r));
					}
					else {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl((Constraint)copyExpression(l), op, (Constraint)copyExpression(r));
					}
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
			else if (exp instanceof RelationalConstraint) {
				if (exp instanceof symbolic.execution.constraints.choco.RelationalConstraintImpl) {
					CompareOperators op = ((RelationalConstraint) exp).getOp();
					Expression l = ((RelationalConstraint)exp).getLeft();
					Expression r = ((RelationalConstraint) exp).getRight();
					res = new symbolic.execution.constraints.choco.RelationalConstraintImpl(copyExpression(l), op, copyExpression(r));
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
		}
		else if (exp instanceof SymbolicVariable) {
			res = exp;
		}
		else if (exp == null) {
			System.out.println("JDA: Copying null expression" );
		}
		else {
			System.out.println("JDA: Ouch - no handler for expression, " + exp.toString() + ", of type, " 
					+ exp.getClass()+ "!" );
		}
		return res;
	}

}
