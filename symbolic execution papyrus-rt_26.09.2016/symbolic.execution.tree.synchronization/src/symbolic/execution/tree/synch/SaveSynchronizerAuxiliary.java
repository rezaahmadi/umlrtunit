package symbolic.execution.tree.synch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
//import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
//import symbolic.execution.ffsm.model.Location;
import symbolic.execution.ffsm.model.ModelVariable;
//import symbolic.execution.tree.synch.Synchronizer;
import symbolic.execution.tree.synch.SynchronizationState;

//JDA: begin addition for constants to support 'sync-fix'.
//TODO: JDA: Make more efficient. Improve documentation.
/*
 * JDA: 1) The path conditions and valuations of visited targets
 *         were being corrupted because of shared references.
 *      2) This corruption between the time a new state was
 *         queued and the time it was dequeued.
 *      3) This patches at least some cases of this problem.
 *      TODO: JDA: Test
 *      TODO: JDA: Make more efficient.
 *      TODO: JDA: Ensure the patch is applied when displaying and saving ...
 *            JDA: states. 
 */
public class SaveSynchronizerAuxiliary {
	
	//JDA: visitedTargets: to hold the volatile data of visited targets.
	private HashSet<NodeSymbolic> visitedTargets = new HashSet<NodeSymbolic>();
	
	//JDA: savePC -- to save the Path Constraints of the top level component.
	private HashMap<SynchronizationState,List<Constraint>> savePC =
		new HashMap<SynchronizationState,List<Constraint>>();
	//JDA: saveVs -- to save the valuations of the top level component.
	private HashMap<SynchronizationState,ValuationSymbolic> saveVs =
		new HashMap<SynchronizationState,ValuationSymbolic>();
	//JDA: saveSubPC -- to save the PCs of the sub components.
	HashMap<SynchronizationState, Map<SymbolicExecutionTree, List<Constraint>>>  saveSubPC =
	  new HashMap<SynchronizationState, Map<SymbolicExecutionTree, List<Constraint>>>();
	//JDA: saveSubVal -- to save the valuations of the sub components.
	HashMap<SynchronizationState, Map<SymbolicExecutionTree, ValuationSymbolic>> saveSubVal =
	  new HashMap<SynchronizationState, Map<SymbolicExecutionTree, ValuationSymbolic>>();
	
	//JDA: saveSynchronizationStateData - to save the volatile data of the the given SynchronizationState.
	public void saveSynchronizationStateData(SynchronizationState synchState) {
		if(!savePC.containsKey(synchState)) {
			NodeSymbolic node = synchState.getNode();
			List<Constraint> pc = node.getContents().getPC();
			ValuationSymbolic v = node.getContents().getValuation();
			List<Constraint> copyPc = copyConstraintList(pc);
			ValuationSymbolic copyV = copyValuationSymbolic(v);
			savePC.put(synchState, copyPc);
			saveVs.put(synchState, copyV);
			Set<SymbolicExecutionTree> keys = synchState.getTrees();
			
			HashMap<SymbolicExecutionTree, List<Constraint>> tmpPcMap = 
				new HashMap<SymbolicExecutionTree, List<Constraint>>();
			HashMap<SymbolicExecutionTree, ValuationSymbolic> tmpValMap = 
				new HashMap<SymbolicExecutionTree, ValuationSymbolic>();
			for (SymbolicExecutionTree set: keys ) {
				NodeSymbolic cnode = synchState.getCurrentNode(set);
				List<Constraint> cpc = cnode.getContents().getPC();
				ValuationSymbolic cv = cnode.getContents().getValuation();
				List<Constraint> copyCpc = copyConstraintList(cpc);
				ValuationSymbolic copyCv = copyValuationSymbolic(cv);
				tmpPcMap.put(set, copyCpc);
				tmpValMap.put(set, copyCv);
			}
			saveSubPC.put(synchState, tmpPcMap);
			saveSubVal.put(synchState, tmpValMap);
		}
	}
	
	//JDA: saveSynchronizationStateData - to restore the volatile data of the the given SynchronizationState.
	public void restoreSynchronizationStateData(SynchronizationState synchState) {
		if(savePC.containsKey(synchState)) {
			NodeSymbolic node = synchState.getNode();
			List<Constraint> pc = savePC.get(synchState);
			ValuationSymbolic val = saveVs.get(synchState);
			node.getContents().putValPC(val, pc);
			
			Map<SymbolicExecutionTree, List<Constraint>> m = saveSubPC.get(synchState);
			Map<SymbolicExecutionTree, ValuationSymbolic> v = saveSubVal.get(synchState);
			Set<SymbolicExecutionTree> keys = synchState.getTrees();
			for (SymbolicExecutionTree set: keys) {
				NodeSymbolic cnode = synchState.getCurrentNode(set); 
				pc = m.get(set);
				val = v.get(set);
				cnode.getContents().putValPC(val, pc);
			}
		}
	}
	
	
	//JDA: added checkVisited. This, or something like it, is required to repair damage
	//JDA: caused by previous visits to 'target'. (Branch 'sync-fix'.)
	//JDA: to patch up the volitile data of the given target, if it has
	//JDA: been used or to save it, otherwise.
	public void checkVisited(NodeSymbolic target) {
		// TODO: JDA: Need to make 'checkVisited' more efficient, if possible.
		if (visitedTargets.contains(target)) {
			System.out.println("JDA: Fix, debug target, " + target.toString() + ", has been visited.");
			
			ValuationSymbolic valuationBefore = target.getContents().getValuation();
			List<Constraint> PCbefore = target.getContents().getPC();
			String currentValuesBefore = "valuation =[[" +
		    	((valuationBefore == null) ? "<null>" : valuationBefore.toString()) + "]], PC=[[" + 
		    	((PCbefore == null)? "<null>" : PCbefore.toString()) + "]].";
			System.out.println("JDA: Fix, Val. and PC before: " + currentValuesBefore);
			
			//JDA: begin alternative 1 (broken)
			//ValuationSymbolic valuation = valuationMap.get(target);
			//List<Constraint>  PC  = PCmap.get(target);
			//ValuationSymbolic restoreValuation = copyValuationSymbolic(valuation);
			//List<Constraint> restorePC = copyConstraintList(PC);
			//target.getContents().putValPC(restoreValuation, restorePC);
			//JDA: end alternative 1
			
			//JDA: begin alternative 2 (untested)
			NodeSymbolic tmpTarget = target.getContents().getIngoingArc().getTarget();
			SymbolicState tmpContents = tmpTarget.getContents();
			List<Constraint> tmpPC = tmpContents.getPC();
			ValuationSymbolic tmpValuation = tmpContents.getValuation();
			List<Constraint> tmpCopyPC = copyConstraintList(tmpPC);
			ValuationSymbolic tmpCopyValuation = copyValuationSymbolic(tmpValuation);
			target.getContents().putValPC(tmpCopyValuation, tmpCopyPC);
			//JDA: end alternative 2 (untested)
			
			ValuationSymbolic valuationAfter = target.getContents().getValuation();
			List<Constraint> PCafter = target.getContents().getPC();
			
			String currentValuesAfter = "valuation =[[" +
		    	((valuationAfter == null) ? "<null>" : valuationAfter.toString()) + "]], PC=[[" + 
		    	((PCafter == null)? "<null>" : PCafter.toString()) + "]].";
			System.out.println("JDA: Fix, Val. and PC after:   " + currentValuesAfter);
			if (!currentValuesAfter.equals(currentValuesBefore)) {
				System.out.println("JDA: Fix, after, changed.");
			}
			else {
				System.out.println("JDA: Fix, after, apparently unchanged.");
			}
		}
		else {
			System.out.println("JDA: Fix, debug target, " + target.toString() + ", has not been visited.");
			visitedTargets.add(target);
			List<Constraint> PC = target.getContents().getPC();
			ValuationSymbolic valuation = target.getContents().getValuation();
			String outS = "valuation =[[" + valuation.toString() + "]], PC=[[" + PC.toString() + "]].";
			//stringMap.put(target, outS);
			//PCmap.put(target, copyConstraintList(PC));
			//valuationMap.put(target, copyValuationSymbolic(valuation));
			System.out.println("JDA: Fix, Val. and PC:  " + outS);
			System.out.println("JDA: Fix, save only.");
		}
	}

	//JDA: This code deep copies symbolic valuations.
	ValuationSymbolic copyValuationSymbolic(ValuationSymbolic vs) {
		ValuationSymbolic res = new ValuationSymbolic();
		Map<SymbolicVariable, Expression> var = vs.getSymbolicVariables();
		for (Entry<SymbolicVariable, Expression> en: var.entrySet()) {
			Expression ex = en.getValue();
			SymbolicVariable v = en.getKey();
			ModelVariable<?> mv = vs.getVariableByName(v.getName());
			res.setValue((ModelVariable<?>)mv, copyExpression(ex));
		}
		return res;
	}
	//JDA: This code deep copies path constraints.
	List<Constraint> copyConstraintList(List<Constraint> lc)  {
		List<Constraint> nlc = new ArrayList<Constraint>();
		for (Constraint c: lc) {
			Constraint nc = (Constraint)copyExpression(c);
			nlc.add(nc);
		}
		return nlc;
	}
	
	//JDA: This code deep copies expressions.
	private Expression copyExpression (Expression exp) {
		//TODO: JDA: This may not handle all possible expression implementations.
		//           E.g., for choco.
		Expression res = null;
		if (exp instanceof BinaryExpression) {
			if ((symbolic.execution.constraints.choco.BinaryExpressionImpl) exp 
					instanceof symbolic.execution.constraints.choco.BinaryExpressionImpl) {
				Expression l = ((BinaryExpression) exp).getLeft();
				Expression r = ((BinaryExpression) exp).getRight();
				ArithmeticOperators op = ((BinaryExpression) exp).getOp();
				res = new symbolic.execution.constraints.choco.BinaryExpressionImpl(
						copyExpression(l), op, copyExpression(r));
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof UnaryExpression ){
			ArithmeticOperators op = ((UnaryExpression) exp).getOp();
			Expression e = ((UnaryExpression) exp).getExpression();
			res = new UnaryExpression(op, copyExpression(e));
		}
		else if (exp instanceof Constant){
			Object v = ((Constant) exp).getValue();
			if ((symbolic.execution.constraints.choco.ConstantImpl) exp 
					instanceof symbolic.execution.constraints.choco.ConstantImpl) {
				res = new symbolic.execution.constraints.choco.ConstantImpl(v.getClass(), v);
			}
			else {
				System.out.println("JDA: error - type of exp, " + exp.toString() +
						"is " + exp.getClass() + ". It is not handled.");
			}
		}
		else if (exp instanceof Constraint){
			if (exp instanceof symbolic.execution.constraints.manager.False) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.False();
			}
			else if (exp instanceof symbolic.execution.constraints.manager.True) {
				//TODO: JDA: this probably should be more concrete.
				res = new symbolic.execution.constraints.manager.True();
			}
			else if (exp instanceof LogicalConstraint) {
				if (((symbolic.execution.constraints.choco.LogicalConstraintImpl)exp) instanceof  
						symbolic.execution.constraints.choco.LogicalConstraintImpl) {
					LogicalOperators op = ((LogicalConstraint) exp).getOp();
					Constraint l = ((LogicalConstraint) exp).getLeft();
					Constraint r = ((LogicalConstraint) exp).getRight();
				
					if (l == null) {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl(null, op, (Constraint) copyExpression(r));
					}
					else {
						res = new symbolic.execution.constraints.choco.LogicalConstraintImpl((Constraint)copyExpression(l), op, (Constraint)copyExpression(r));
					}
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
			else if (exp instanceof RelationalConstraint) {
				if (exp instanceof symbolic.execution.constraints.choco.RelationalConstraintImpl) {
					CompareOperators op = ((RelationalConstraint) exp).getOp();
					Expression l = ((RelationalConstraint)exp).getLeft();
					Expression r = ((RelationalConstraint) exp).getRight();
					res = new symbolic.execution.constraints.choco.RelationalConstraintImpl(copyExpression(l), op, copyExpression(r));
				}
				else {
					System.out.println("JDA: error - type of exp, " + exp.toString() +
							"is " + exp.getClass() + ". It is not handled.");
				}
			}
		}
		else if (exp instanceof SymbolicVariable) {
			res = exp;
		}
		else if (exp == null) {
			System.out.println("JDA: Copying null expression" );
		}
		else {
			System.out.println("JDA: Ouch - no handler for expression, " + exp.toString() + ", of type, " 
					+ exp.getClass()+ "!" );
		}
		return res;
	}

}
