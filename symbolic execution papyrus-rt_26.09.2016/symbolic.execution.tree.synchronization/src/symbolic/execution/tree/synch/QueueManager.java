package symbolic.execution.tree.synch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.tree.synch.model.Queue;

public class QueueManager {
	private Map<SymbolicExecutionTree, Queue> treeQueue;
	
	private Map<Queue,Set<SymbolicExecutionTree>> queueTrees; 
	
	private List<Queue> order;
	
	public QueueManager(){
		treeQueue = new HashMap<SymbolicExecutionTree, Queue>();
		queueTrees = new HashMap<Queue, Set<SymbolicExecutionTree>>();
		
		order = new ArrayList<Queue>();
	}
	
	public QueueManager(QueueManager other){
		this();
				
		for(Queue q : other.order){
			Queue newQ = new Queue(q);
			order.add(newQ);
			
			queueTrees.put(newQ, other.queueTrees.get(q));
			
			for(SymbolicExecutionTree tree : other.queueTrees.get(q)){
				treeQueue.put(tree, newQ);
						}
		}
		
		
	}

	public QueueManager(Set<Set<SymbolicExecutionTree>> partition) {
		this();
		for (Set<SymbolicExecutionTree> part : partition){						
			Queue q = new Queue();
			order.add(q);
			queueTrees.put(q,part);
			
			for (SymbolicExecutionTree tree : part){
				treeQueue.put(tree, q);
			}
		}
	}

	public void putInQueue(SymbolicExecutionTree receiver,
			ActionInputSymbolic action) {
		if (treeQueue.containsKey(receiver)){
			treeQueue.get(receiver).putInQueue(action, receiver);
		} else {
			Queue q = new Queue();
			q.putInQueue(action, receiver);
			treeQueue.put(receiver,q);
		}
		
	}

	public Set<Queue> getAllQueues() {		
		return new HashSet<Queue>(treeQueue.values());
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		int size = 0;
		for (Queue q : order){
			sb.append(queueTrees.get(q));
			sb.append("<-");
			sb.append(q.toString());
			size++;
			if (size < queueTrees.size()){
				sb.append("\n");
			}
		}		
		return sb.toString();
	}

	public Queue getQueue(SymbolicExecutionTree tree) {
		return treeQueue.get(tree);		
	}

	public Set<SymbolicExecutionTree> getTrees() {		
		return treeQueue.keySet();
	}

	public void replaceQueue(Queue qOld, Queue qNew) {
		for (SymbolicExecutionTree t : treeQueue.keySet()){
			if (treeQueue.get(t) == qOld){
				treeQueue.put(t,qNew);
			}
		}
		
		Set<SymbolicExecutionTree> trees = queueTrees.get(qOld);
		queueTrees.remove(qOld);
		queueTrees.put(qNew,trees);
		
		int i=0;
		for (i=0; i < order.size(); i++){
			if (order.get(i)==qOld){
				break;
			}
		}
		
		order.remove(i);
		order.add(qNew);
		
	}
}
