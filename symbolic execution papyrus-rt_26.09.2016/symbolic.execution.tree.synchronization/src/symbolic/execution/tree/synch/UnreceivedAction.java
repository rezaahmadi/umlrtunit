package symbolic.execution.tree.synch;

import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;

public class UnreceivedAction extends ActionInputSymbolic {

	public UnreceivedAction(ActionInputSymbolic other) {
		super(other);	
		
	}
	
	@Override
	public String toString() {		
		return "UNRECEIVED->" + super.toString();
	}

}
