package symbolic.execution.tree.synch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
//import symbolic.execution.ffsm.engine.tree.TreeNode;
import symbolic.execution.ffsm.engine.symbolic.SymbolicStateAuxiliary;

//import javax.swing.tree.TreeNode;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.ExtendedNodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
//import symbolic.execution.tree.synch.model.SymbolicStateSynch;
//import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
//import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
//import symbolic.execution.ffsm.engine.tree.Arc;
//import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.Queue;
//import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;
//import symbolic.execution.tree.synch.model.SymbolicStateSynch;

//JDA: Significant changes were made to in branch "sync-fix".
//     It turned out that the changes needed were only
//     to support better debugging.
public class SynchronizationState  {
	
	//JDA: Begin new
	private SymbolicStateAuxiliary auxiliary = new SymbolicStateAuxiliary();
	private String addedBy;
	private SynchronizationState exploringState;
	//JDA: End new
	
	private QueueManager queueManager;
	
	//private NodeSymbolic treeNode;
	private NodeSymbolic treeNode;
	
	private Map<SymbolicExecutionTree, NodeSymbolic> currentNodes;
	//private Map<SymbolicExecutionTree, ExtendedNodeSymbolic> currentNodes;
		
	private NodeSymbolicSynch node;
	
	//private Map<NodeSymbolic, Map<SymbolicVariable,Expression>> valuationUpdates;
	private Map<SymbolicExecutionTree, Map<SymbolicVariable,Expression>> valuationUpdates;
	
	private SymbolicExecutionTree tree;
	
	//JDA: parent is new for debugging.
	private SynchronizationState parent = null;
	
	public SynchronizationState(QueueManager queueManager, SymbolicExecutionTree tree){
		this.queueManager = queueManager;
		this.tree = tree;
		
		currentNodes = new HashMap<SymbolicExecutionTree, NodeSymbolic>();
		//currentNodes = new HashMap<SymbolicExecutionTree, ExtendedNodeSymbolic>();
		//valuationUpdates = new HashMap<NodeSymbolic, Map<SymbolicVariable,Expression>>();
		valuationUpdates = new HashMap<SymbolicExecutionTree, Map<SymbolicVariable,Expression>>();
	}
	
	public SynchronizationState(SynchronizationState other, String adder) {
		//JDA: new
		addedBy = adder;
		queueManager = new QueueManager(other.queueManager);
		currentNodes = new HashMap<SymbolicExecutionTree, NodeSymbolic>(other.currentNodes);
		//currentNodes = new HashMap<SymbolicExecutionTree, ExtendedNodeSymbolic>(other.currentNodes);
		//valuationUpdates = new HashMap<NodeSymbolic, Map<SymbolicVariable,Expression>>(other.valuationUpdates);
		valuationUpdates = new HashMap<SymbolicExecutionTree, Map<SymbolicVariable,Expression>>(other.valuationUpdates);
		node = other.node;
		treeNode = other.treeNode;
		tree = other.tree;
		parent = other;
		//System.out.println("JDA: SynchronizationState.SynchronizationState: other: !!!" + other.getFullStateAsString() + "!!!");
		//System.out.println("JDA: SynchronizationState.SynchronizationState: this:  !!!" + this.getFullStateAsString()  + "!!!");
	}
	
	//public void setCurrentNode(SymbolicExecutionTree tree, NodeSymbolic node) {	
	public void setCurrentNode(SymbolicExecutionTree tree, NodeSymbolic node) {	
		
		//NodeSymbolic current = currentNodes.get(tree);
		NodeSymbolic current = currentNodes.get(tree);
		if (tree == this.tree){
			current = treeNode;
			treeNode = node;
		} else {
			currentNodes.put(tree, node);
		}
		if (treeNode != this.node) {
			//System.out.println("JDA: SynchronizationState.setCurrentNode: treeNode != this.node.");
		}
		else {
			//System.out.println("JDA: SynchronizationState.setCurrentNode: treeNode == this.node.");
		}
		
		//switch previous valuations
		//Map<SymbolicVariable,Expression> previousVals = valuationUpdates.remove(current);
		//if (previousVals != null){
			//valuationUpdates.put(node, previousVals);
		//	valuationUpdates.put(tree, previousVals);
		//}		
		
	}
	
	//JDA: added the following
	public void putExloringState(SynchronizationState eState) {
		exploringState = eState;
	}
	//JDA: added the following
	public SynchronizationState getExloringState(SynchronizationState eState) {
		return exploringState;
	}
	
	public void setTreeNode(NodeSymbolic node){
		NodeSymbolic current = treeNode;
		
		//switch previous valuations
		Map<SymbolicVariable,Expression> previousVals = valuationUpdates.remove(current);
		if (previousVals != null){
			//valuationUpdates.put(node, previousVals);
			valuationUpdates.put(tree, previousVals);
		}		
		
		treeNode = node;
		//System.out.println("JDA: SychronizationState.setTreeNode. treeNode is now <<" + treeNode + ">>.");
	}

	//JDA: added the following
	private String printListOfStates(List<SymbolicState> list) {
		String result = "";
		if (list == null) {
			result = "<null>";
		}
		else {
			result = "[";
			int sz = list.size();
			SymbolicState myState = this.getNode().getContents();
			if (sz >= 1) {
				//DA: added the master param, 'myState'.
				result += list.get(0).toString(myState);
				for (int i = 1; i < sz; i++)  {
					result += "," + list.get(i).toString(myState);
				}
			}
			result += "]";
		}
		return result;
	}
	
	private String printListOfStates(List<SymbolicState> list, Object master) {
		String result = "";
		if (list == null) {
			result = "<null>";
		}
		else {
			result = "[";
			int sz = list.size();
			SymbolicState myState = this.getNode().getContents();
			if (sz >= 1) {
				//JDA: added the master param.
				result += list.get(0).toString(master);
				for (int i = 1; i < sz; i++)  {
					result += "," + list.get(i).toString(master);
				}
			}
			result += "]";
		}
		return result;
	}
	
	public List<SymbolicState> getStates() {
		List<SymbolicState> states = new ArrayList<SymbolicState>();
		//System.out.println("JDA: getStates. treeNode is now <<" + treeNode + ">>.");
		states.add(treeNode.getContents());
		for(NodeSymbolic n : currentNodes.values()){
			states.add(n.getContents());
		}
		return states;
	}

	//JDA: added the following for debugging.
	public String getCurrentNodesAsString() {
		//JDA: Replace the following:
		//return currentNodes.toString();
		String result = new String();
		for (SymbolicExecutionTree set: currentNodes.keySet()) {
			NodeSymbolic n = currentNodes.get(set);
			result += n.getFullNodeSymbolicAsString(this.node);
		}
		return result;
	}
	//JDA: added the following for debugging.
	public String getFullStateAsString() {
		String ns;
		if (node == null) {
			ns = "<null>";
		}
		else {
			ns = node.toString();
		}
		
		return( "[------------------------\n" +
				
			"Hashcode: " + this.hashCode() +
			   " Parent: " + (parent == null? "<null>" : parent.hashCode()) +
			   " Creator: " + (exploringState == null? "<null>" : exploringState.hashCode()) +
			   " Added by: " + addedBy +
			   "\nQueue:" + this.queueManager.toString() +
			   //JDA: added the master param, 'node'
			   "\nnode: ["+ node.getFullNodeSymbolicAsString(node) + "]\n" + 
			   "\ncurrentNodes: [" + getCurrentNodesAsString() + "]\n" +
			   //JDA: added the master param, 'node'
			   "treeNode: [" + treeNode.getFullNodeSymbolicAsString(node) + "]\n" +
			   //Fixed to add master param, 'node'
			   "getStates(): [" + printListOfStates(getStates(),node) + "]\n" +
			   "valuationUpdates: [" + valuationUpdates + "]\n" +
			   "------------------------]");
	}
	public List<NodeSymbolic> getNodes() {
		return new ArrayList<NodeSymbolic>(currentNodes.values());
	}


	public void addSignal(SymbolicExecutionTree receiver, ActionInputSymbolic action) {
		queueManager.putInQueue(receiver, action);
		
	}

	@Override
	public String toString() {
		//JDA: Minor fix to cover the case that "node" is null.
		if (node == null) {
			return "<null>";
		}
		return node.toString();
	}

	public void updateValuation(SymbolicExecutionTree set, NodeSymbolic target, Object master) {
		//Map<SymbolicVariable, Expression> vals = valuationUpdates.get(target);
		Map<SymbolicVariable, Expression> vals = valuationUpdates.get(set);
		//System.out.println("JDA: SynchronizationState:updateValuation 1: target: [" + target.getFullNodeSymbolicAsString() + "].");
		if (vals==null){
			//System.out.println("JDA: SynchronizationState: updateValuation: vals: [null]. Returning.");
			return;
		}
		//System.out.println("JDA: SynchronizationState:updateValuation 1: vals: [" + vals.toString() + "].");
		for(SymbolicState s: target.getContentsAll()){
			//System.out.println("JDA: SynchronizationState:updateValuation 1: s (raw, before)" +
			//		s.printInLines());
			//System.out.println("JDA: SynchronizationState:updateValuation 1: s (cooked, before)" +
			//		s.printInLines(master));
			s.updateValuation(vals, master);
			//System.out.println("JDA: SynchronizationState:updateValuation 1: s (raw, after)" +
			//		s.printInLines());
			//System.out.println("JDA: SynchronizationState:updateValuation 1: s (cooked, after)" +
			//		s.printInLines(master));
			//System.out.print("");
		}
	}

	//public void updateValuation(NodeSymbolic target, SynchronizationAction synch) {
	public void updateValuation(SymbolicExecutionTree set, NodeSymbolic target, SynchronizationAction synch, Object master) {
		Map<SymbolicVariable, Expression> vals = valuationUpdates.get(set);
		if (vals==null){
			vals = synch.getValuationMapping();
		} else {
			vals.putAll(synch.getValuationMapping());			
		}
		//valuationUpdates.put(target,vals);
		//valuationUpdates.put(target,vals);
		valuationUpdates.put(set,vals);
		//System.out.prinftln("JDA: SynchronizationState.updateValuation: target = <<" +
		//		target + ">>");
		//System.out.println("JDA: SynchronizationState.updateValuation: target.getClass().getName() = <<" +
		//		target.getClass().getName() + ">>");
		//System.out.println("JDA: SynchronizationState.updateValuation 2: synch = <<" +
		//		synch + ">>");
		//JDA: Minor change for debugging
		List<SymbolicState> allContents = target.getContentsAll();
		//System.out.println("JDA: SynchronizationState.updateValuation: allContents = <<" +
		//		allContents + ">>");
		//System.out.println("JDA: SynchronizationState.updateValuation: this (before) = [" +
		//		getFullStateAsString() + "]");
		for(SymbolicState s: allContents){
			//System.out.println("JDA: SynchronizationState.updateValuation: s before = <<" +
			//		s.toString() + ">>");
			//System.out.println("JDA: SynchronizationState.updateValuation: vals = <<" +
			//		vals + ">>");
			//System.out.println("JDA: updateValuation 2: s (raw, before)" +
			//		s.printInLines());
			//System.out.println("JDA: SynchronizationState.updateValuation 2: s (cooked, before)" +
			//		s.printInLines(master));
			s.updateValuation(vals, master);
			//System.out.println("JDA: SynchronizationState.updateValuation 2: s (raw, after)" +
			//		s.printInLines());
			//System.out.println("JDA: SynchronizationState.updateValuation 2: s (cooked, after)" +
			//		s.printInLines(master));
			//System.out.print("");
		}
		//System.out.println("JDA: SynchronizationState.updateValuation: this (after) = [" +
		//		getFullStateAsString() + "]");
		
	}


	public boolean checkSatsfiability(SynchronizationAction action, Object master) {
		//gather constraints and valuations
		List<Constraint> constraints = new ArrayList<Constraint>();	
		constraints.addAll(action.getValuationConstraints());
		//System.out.println("JDA: checkSatsfiability action, <<" + action + ">>.");
		//System.out.println("JDA - checkSatsfiability: initial constraints, <<" + constraints + ">>.");
		//System.out.println("JDA - checkSatsfiability: getStates(), <<" + getStates() + ">>.");
		for(SymbolicState state : getStates()){
			//System.out.println("JDA - checkSatsfiability: state <<" + state + ">>.");
			//System.out.println("JDA - checkSatsfiability: state.getPC() <<" + state.getPC() + ">>.");
			constraints.addAll(state.getPC(master));
			//System.out.println("JDA - checkSatsfiability: constraints, <<" + constraints + ">>.");
			//System.out.println("JDA - checkSatsfiability: state.getValuation <<" + state.getValuation() + ">>.");
			constraints.addAll(state.getValuation(master).createConstraints());
			//System.out.println("JDA - checkSatsfiability: constraints, <<" + constraints + ">>.");
		}
		//System.out.println("JDA - checkSatsfiability: constraints before check, <<" + constraints + ">>.");
		Solver solver = SymbolicFactory.createSolver();
		boolean result = solver.isSolvable(constraints);
		//System.out.println("JDA - checkSatsfiability: result, " + result + ".");
		return result;
	}

	public void setNode(NodeSymbolicSynch node) {
		List<NodeSymbolic> nodeList = this.getNodes();
		this.node = node;
	}

	public Set<SymbolicExecutionTree> getTrees() {		
		return currentNodes.keySet();
	}

	public NodeSymbolic getCurrentNode(SymbolicExecutionTree t) {	
		if (t== tree){
			return treeNode;
		}
		return currentNodes.get(t);
	}

	public Set<Queue> getAllQueues() {		
		return queueManager.getAllQueues();
	}

	public NodeSymbolic getNode() {		
		return node;
	}

	public QueueManager getQueueManager() {		
		return queueManager;
	}

	//FIXME: JDA: Bug 1 (SSE-Subsumption): 'SyncrhonizationState.subsumes' is probably broken because of changes ...
	//       JDA:  to ensure that valuations and PCs of contained states are kept separate.
	public boolean subsumes(SynchronizationState state) {
		//check treeNode
		if (!state.treeNode.same(treeNode)){
			return false;
		}
		
		//check queues for tree node
		if (!state.getQueueManager().getQueue(tree).equals(this.queueManager.getQueue(tree))){
			return false;
		}
		Set<SymbolicExecutionTree> allTrees = new HashSet<SymbolicExecutionTree>(currentNodes.keySet());
		allTrees.addAll(state.currentNodes.keySet());		
				
		for (SymbolicExecutionTree tree : allTrees){
			NodeSymbolic stateThis = getCurrentNode(tree);
			NodeSymbolic stateOther = state.getCurrentNode(tree);
			
			if (stateThis.same(stateOther)){
				
				Queue qThis = queueManager.getQueue(tree); 
				Queue qOther = state.queueManager.getQueue(tree);
				
				if (!qThis.equals(qOther)){
					return false;
				}
			}else {
				return false;
			}
		}
		return true;
	}

	public NodeSymbolic getMainTreeNode() {		
		return treeNode;
	}

	public SymbolicState getTreeState() {
		return treeNode.getContents();
	}

	public Map<String, SymbolicState> getParts(Map<String, SymbolicExecutionTree> roles) {
		Map<String,SymbolicState> result = new HashMap<String, SymbolicState>();
		
		for (String roleName : roles.keySet()){
			result.put(roleName, currentNodes.get(roles.get(roleName)).getContents());
		}
		
		return result;
	}

	public QueueManager getQueues() {		
		return queueManager;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof SynchronizationState){
			SynchronizationState state = (SynchronizationState)o;
			return this.subsumes(state);
		}
		return false;
	}

	public void replaceQueue(Queue q, Queue qCopy) {
		queueManager.replaceQueue(q,qCopy);
		
	}
}
