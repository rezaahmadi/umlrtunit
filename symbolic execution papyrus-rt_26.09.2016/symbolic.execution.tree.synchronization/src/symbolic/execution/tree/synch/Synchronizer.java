package symbolic.execution.tree.synch;

//import java.awt.RenderingHints.Key;
//import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.Collection;
//import java.util.HashMap;
//import java.util.HashSet;
import java.util.List;
import java.util.Map;
//import java.util.Map.Entry;
import java.util.Set;

//import org.eclipse.uml2.uml.Transition;

//import symbolic.execution.constraints.manager.BinaryExpression;
//import symbolic.execution.constraints.manager.Constant;
//import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.constraints.manager.False;
//import symbolic.execution.constraints.manager.LogicalConstraint;
//import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolicError;
//import symbolic.execution.ffsm.engine.symbolic.NodeSymbolicUnknown;
import symbolic.execution.ffsm.engine.symbolic.OutputSequenceSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
//import symbolic.execution.ffsm.engine.symbolic.ValuationSymbolic;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.engine.tree.TreeNode;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.Location;
//import symbolic.execution.ffsm.model.ModelVariable;
import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.Queue;
import symbolic.execution.tree.synch.model.RelayActions;
import symbolic.execution.tree.synch.model.SentMessage;
import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;
import symbolic.execution.tree.synch.utils.PermutationGenerator;

//import symbolic.execution.constraints.manager.Expression;
//import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
//import symbolic.execution.constraints.manager.operators.LogicalOperators;
//import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.operators.CompareOperators;

import symbolic.execution.tree.synch.SynchronizationAction;


/* ======================================================================================================
 JDA: Class Synchronizer is designed to produced a synchronized symbolic execution of the selected
 capsule as part of the process of the symbolic execution for the model. This Class will not be
 used if the user chooses to produce the so-called "open" symbolic execution only.

 Done: JDA: Changes done in branch 'sync-fix' to synchronization node bug.
 The problem was due to the fact that symbolic path constraints and
 valuations were be overwritten when a node representing the symbolic
 execution of a component were revisited. These components represent
 either the state machine of the given capsule, or one
 of the state machines of a capsule contained within it.
 
 When a node of a component was revisited, its data had been overwritten
 during the previous visitation. I have written code to deep copy
 path constraints and valuations (and their expressions) and have used this
 code to  save and restore them. 
 
 Some of the data that the Synchronizer uses comes from instances of
 the component (capsule/subcapsule) symbolic execution trees. This
 data is shared among different SynchronizationState nodes. The
 instances read and write the valuations and path constraints
 of the component SETs. Unfortunately, this means that the valuations
 and path constraints are shared. Therefore, the data can be
 overwritten between the time a new state is created and put on
 the set of nodes to be explored, and the time the state
 is retrieved from this set.
 
 The new class, 'SynchronizationAuxilary.java' is responsible
 for saving and restoring this data.
 
 TODO: JDA: Test
 TODO: JDA: Display and output to '.set' broken. Fix. ...
       JDA: They do not use save and restore
 
 History:
     April 17, 2016. Bug 2 added null 'variableMapping' parameter.
 ====================================================================================================== */

public class Synchronizer {
	
	private SymbolicExecutionTree tree;
	private Map<String, SymbolicExecutionTree> roleTrees;

	private Map<ActionOutput, ActionInput> connectedActions;
	private Map<ActionOutput, SymbolicExecutionTree> connectedTrees;

	private RelayActions relayAction;

	private List<SynchronizationState> toExplore = new ArrayList<SynchronizationState>();
	private List<SynchronizationState> explored = new ArrayList<SynchronizationState>();

	private SymbolicExecutionTreeSynch compositeTree;

	private QueueManager qm;

	public Synchronizer(SymbolicExecutionTree tree,
			Map<String, SymbolicExecutionTree> roleTrees,
			Map<ActionOutput, ActionInput> connectedActions,
			Map<ActionOutput, SymbolicExecutionTree> connectedTrees,
			Set<Set<SymbolicExecutionTree>> partition, RelayActions relayActions) {
		//System.out.println("JDA: Synchronizer.Synchronizer: tree !!!" + tree
		//		+ "!!!");
		//System.out.println("JDA:                            roleTrees !!!"
		//		+ roleTrees + "!!!");
		//System.out.println("JDA:                            connectedActions !!!"
		//				+ connectedActions + "!!!");
		//System.out.println("JDA:                            connectedTrees !!!"
		//		+ connectedTrees + "!!!");
		//System.out.println("JDA:                            partition !!!"
		//		+ partition + "!!!");
		//System.out.println("JDA:                            relayActions !!!"
		//		+ relayActions + "!!!");
		//Map<SymbolicExecutionTree, SymbolicExecutionTree> copied = new HashMap<SymbolicExecutionTree, SymbolicExecutionTree>();

		this.tree = tree;
		this.connectedActions = connectedActions;
		this.connectedTrees = connectedTrees;
		this.roleTrees = roleTrees;
		this.relayAction = relayActions;
		qm = new QueueManager(partition);

		//JDA: begin original commented-out region.
		/*
		copied.put(tree, this.tree);		
		//copy tress		
		this.roleTrees = new HashMap<String,SymbolicExecutionTree>();
		for (String roleName : roleTrees.keySet()){
			SymbolicExecutionTree t = roleTrees.get(roleName);
			
			SymbolicExecutionTree set = null;
			if (t instanceof SymbolicExecutionTreeSynch){
				set = new SymbolicExecutionTreeSynch((SymbolicExecutionTreeSynch)t);				
			} else {
				set = new SymbolicExecutionTree(t);
			}
			
			set.setName(roleName + "_" + set.getName());
			
			this.roleTrees.put(roleName,set);			
			copied.put(roleTrees.get(roleName),set);
		}
					
		//map connected trees
		this.connectedTrees = new HashMap<ActionOutput, SymbolicExecutionTree>();
		for (ActionOutput output : connectedTrees.keySet()){			
			this.connectedTrees.put(output, copied.get(connectedTrees.get(output)));
		}
		
		this.relayAction = new HashMap<ActionInput, SymbolicExecutionTree>();
		for (ActionInput in : relayActions.keySet()){
			this.relayAction.put(in, copied.get(relayActions.get(in)));
		}
		
		//create queue manager
		Set<Set<SymbolicExecutionTree>> partitionCopy = new HashSet<Set<SymbolicExecutionTree>>();
		for(Set<SymbolicExecutionTree> part : partition){
			Set<SymbolicExecutionTree> partCopy = new HashSet<SymbolicExecutionTree>();
			for(SymbolicExecutionTree t : part){
				partCopy.add(copied.get(t));
			}
			partitionCopy.add(partCopy);
		}*/
		//JDA: end original commented-out region.
	}
	
	private void fixStates(SynchronizationState oldState,  SynchronizationState newState) {
		NodeSymbolic oldN = oldState.getNode();
		NodeSymbolic newN = newState.getNode();
		newState.getNode().getContents().fixMaps(oldN, newN);
		List<NodeSymbolic> currentNodes = newState.getNodes();
		for (NodeSymbolic n: currentNodes) {
			n.getContents().fixMaps(oldN, newN);
		}
	}

	public SymbolicExecutionTree synchronizeTree() {
		// TODO: JDA: (was KZ) check whether [to implement the] default action in [the] queue for a capsule.
		// JDA modified output for clarity.
		SymbolicState.startSynchronizing();
		//System.out.println("JDA: ************* Synchronizer.synchronizeTree SYNCHRONIZING **************** ");
		//System.out.println("JDA:   tree: !!!" + tree + "!!!");
		//System.out.println("JDA:   roleTrees: !!!" + roleTrees + "!!!");
		//System.out.println("JDA:   qm: (Queues) !!!" + qm + "!!!");
		//System.out.println("JDA: ************************************************************************* ");

		// gather all roots
		// TODO: JDA: ASSUME : only single symbolic states for roots in SETs
		SynchronizationState initial = new SynchronizationState(qm, tree);
		initial.setTreeNode(tree.getRoot());
		// initial.setCurrentNode(tree,tree.getRoot());
		for (SymbolicExecutionTree t : roleTrees.values()) {
			//initial.setCurrentNode(t, t.getRoot());
			initial.setCurrentNode(t, t.getRoot());
		}

		// create new root and tree
		NodeSymbolicSynch root = new NodeSymbolicSynch(initial, roleTrees,
				compositeTree);
		initial.setNode(root);
		compositeTree = new SymbolicExecutionTreeSynch(root, tree.getFFSM(),
				tree);
		for (String roleName : roleTrees.keySet()) {
			compositeTree.addRole(roleName, roleTrees.get(roleName).getFFSM(),
					roleTrees.get(roleName));
		}
		// fire default actions
		// TODO: JDA: (was KZ) ASSUME: there is only 1 default !! JDA: The assumption means ...
		//       JDA: ... that currently the code treats the initialization of all
		//       JDA: ... components as occurring in a single transition. In practice
		//       JDA: ... this is not true.

		List<SynchronizationState> firstStates = exploreCombinationInitial(initial);

		//JDA: begin originally commented out region.
		/*
		SynchronizationState firstState = new SynchronizationState(new QueueManager(qm));
		SymbolicTransition def=null;
		for (SymbolicExecutionTree t : initial.getTrees()){
			
			NodeSymbolic initNode = initial.getCurrentNode(t);
			
			//get symbolic transition
			SymbolicTransition transition = initNode.getChildren().get(0).getContents();
			def=transition;
			if (!transition.getOuputSequence().isEmpty()){
				System.out.println("FOUND TO SEND -- " + transition.getOuputSequence());
				for (ActionOutputInstance ai : transition.getOuputSequence().getSequence()){
					sendSignal(ai, firstState);
				}
			}
			
			//get target node
			NodeSymbolic firstNode = (NodeSymbolic) initNode.getChildren().get(0).getTarget();
			firstState.setCurrentNode(t, firstNode);
		}
		
		//create node

		NodeSymbolicSynch firstNode = new NodeSymbolicSynch(firstState.getStates(), firstState.getQueueManager());
		root.addChild(firstNode, new SymbolicTransition(def));
		
		firstState.setNode(firstNode);
		*/
		//JDA: end originally commented out region.
		
		// add state for exploration
		toExplore.addAll(firstStates);

		// explore the composite state and build a tree
		while (!toExplore.isEmpty()) {
			//System.out.println("TO explore " + toExplore);

			SynchronizationState exploring = toExplore.remove(0);
			//System.out.println("JDA - synchronizer.synchronizeTree: exploring EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
			//		+ exploring.getFullStateAsString() + "\nEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE.");
			//JDA: added.			
			
			explored.add(exploring);

			exploreState(exploring);
			//System.out.println("JDA - synchronizer.synchronizeTree: Exloring Finished FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
			//		+ exploring.getFullStateAsString() + "\nFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF.");
		}
		//SymbolicState.stopSynchronizing();
		return compositeTree;
	}

	private List<SynchronizationState> exploreCombinationInitial(SynchronizationState initial) {
		//System.out.println("JDA - exploreCombinationInitial: initial, !!!" + initial.getFullStateAsString() + "!!!");
		List<SynchronizationState> results = new ArrayList<SynchronizationState>();

		for (Arc<SymbolicTransition, SymbolicState> arc : initial.getMainTreeNode().getChildren()) {
			NodeSymbolic node = (NodeSymbolic) arc.getTarget();

			SynchronizationState newState = new SynchronizationState(
					new QueueManager(qm), tree);

			newState.setTreeNode(node);

			results.add(newState);

		}

		// find

		for (SymbolicExecutionTree symbTree : roleTrees.values()) {
			List<SynchronizationState> additional = new ArrayList<SynchronizationState>();

			// get current node for the tree
			NodeSymbolic initNode = initial.getCurrentNode(symbTree);

			// get the first target node
			NodeSymbolic firstNode = (NodeSymbolic) initNode.getChildren().get(0).getTarget();
			for (SynchronizationState state : results) {
				state.setCurrentNode(symbTree, firstNode);
			}

			// if there are more children - have to create a new synch. state
			for (int i = 1; i < initNode.getChildren().size(); i++) {
				NodeSymbolic node = (NodeSymbolic) initNode.getChildren().get(i).getTarget();
				for (SynchronizationState state : results) {
					SynchronizationState copied = new SynchronizationState(state, "exploreCombinationsInitial");
					copied.setCurrentNode(symbTree, node);
					additional.add(copied);
				}
			}

			results.addAll(additional);
		}

		return exploreCombinations(results, initial);
	}

	private List<SynchronizationState> exploreCombinations(
			List<SynchronizationState> targets, SynchronizationState initial) {
		//System.out.println("JDA: Synchronizer.exploreCombinations: targets: !!!" + targets + "!!!.");
		//System.out.println("JDA:                                   initial: !!!" + initial.getFullStateAsString() + "!!!.");
		List<SynchronizationState> results = new ArrayList<SynchronizationState>();

		for (SynchronizationState state : targets) {
			// Output sequences
			List<OutputSequenceSymbolic> sequences = new ArrayList<OutputSequenceSymbolic>();

			// get sequence form the tree
			OutputSequenceSymbolic treeSequence = getSequenceBetween(initial.getMainTreeNode(), state.getMainTreeNode());

			// get sequence from each role
			for (SymbolicExecutionTree tree : roleTrees.values()) {
				OutputSequenceSymbolic seq = getSequenceBetween(initial.getCurrentNode(tree),
						state.getCurrentNode(tree));
				if (!seq.isEmpty()) {
					sequences.add(seq);
				}
			}

			// get example transition
			SymbolicTransition tr = initial.getMainTreeNode().getChildren().get(0).getContents();

			if (sequences.size() <= 1) {
				SymbolicTransition transition = new SymbolicTransition(tr, compositeTree);

				if (!sequences.isEmpty()) {
					OutputSequenceSymbolic seq = sequences.get(0);
					transition.setOutputSequence(seq);
					seq.addItems(treeSequence);
					sendSignals(seq, state);
				} else {
					transition.setOutputSequence(treeSequence);
					sendSignals(treeSequence, state);
				}

				// if state is new
				if (!results.contains(state)) {
					// create node
					NodeSymbolicSynch newNode = new NodeSymbolicSynch(state, roleTrees, compositeTree);

					// set composite node
					state.setNode(newNode);

					// connect
					initial.getNode().addChild(newNode, transition);

					results.add(state);
				}
			} else {
				// find all possible combinations
				PermutationGenerator gen = new PermutationGenerator(
						sequences.size());

				while (gen.hasMore()) {
					SynchronizationState combinationState = new SynchronizationState(state, "exploreCombinations");

					int[] order = gen.getNext();

					// create transition
					SymbolicTransition transition = new SymbolicTransition(tr, compositeTree);
					OutputSequenceSymbolic seq = new OutputSequenceSymbolic();
					for (int i = 0; i < order.length; i++) {
						seq.addItems(sequences.get(order[i]));
					}
					if (treeSequence != null) {
						seq.addItems(treeSequence);
					}
					sendSignals(seq, combinationState);
					transition.setOutputSequence(seq);

					try {
						if (!results.contains(combinationState)) {
							// create node
							NodeSymbolicSynch newNode = new NodeSymbolicSynch(combinationState, roleTrees, compositeTree);

							// connect
							initial.getNode().addChild(newNode, transition);

							// set node
							combinationState.setNode(newNode);

							results.add(combinationState);

						} else {
							//System.out.println("D");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}
		return results;
	}

	private OutputSequenceSymbolic getSequenceBetween(NodeSymbolic source,
			NodeSymbolic target) {
		//System.out.println("JDA: Synchronizer.getSequenceBetween: source: !!!" + source + "!!!.");
		//System.out.println("JDA:                                  target: !!!" + target + "!!!.");
		for (Arc<SymbolicTransition, SymbolicState> arc : source.getChildren()) {
			if (arc.getTarget() == target) {
				return (arc.getContents()).getOuputSequence();
			}
		}

		return null;
	}

	private void sendSignals(OutputSequenceSymbolic seq,
			SynchronizationState state) {
		//System.out.println("JDA: Synchronizer.sendSignals: seq: !!!" + seq + "!!!.");
		//System.out.println("JDA:                           state: !!!" + state.getFullStateAsString() + "!!!.");
		for (ActionOutputInstance act : seq.getSequence()) {
			if (relayAction.containsRelayOutput(act.getActionOutput())) {
				act.replaceAction(act.getActionOutput());
			} else {
				sendSignal(act, state);
			}
		}
	}

	private void addStateToExplore(SynchronizationState state, SynchronizationState currentState) {
		//System.out.println("JDA: Synchronizer.addStateToExplore: state: !!!" +
		//state.getFullStateAsString() + "!!!.");
		boolean isSubsumed = false;
		for (SynchronizationState s : toExplore) {
			if (s.subsumes(state)) {
				state.getNode().setSubsumedBy(s.getNode());
				s.getNode().addSubsumed(state.getNode());
				isSubsumed = true;
				//System.out.println("Subsuming state: [[" +
				//		state.getFullStateAsString() + "]] ...................");
				//System.out.println(".... to unexplored state: [[" +
				//		s.getFullStateAsString() + "]]");
				break;
			}
		}

		if (!isSubsumed) {
			for (SynchronizationState s : explored) {
				if (s.subsumes(state)) {
					state.getNode().setSubsumedBy(s.getNode());
					s.getNode().addSubsumed(state.getNode());
					//System.out.println("Subsuming state: [[" +
					//		state.getFullStateAsString() + "]] ...................");
					//System.out.println(".... to explored state: [[" +
					//		s.getFullStateAsString() + "]]");
					isSubsumed = true;
					break;
				}
			}
		}

		if (!isSubsumed) {
			state.putExloringState(currentState);
			//System.out.println("JDA: Synchronizer.addStateToExplore: actually adding state: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
			//				+ state.getFullStateAsString() + "\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			toExplore.add(state);
		} else {
			//System.out.println("JDA: Synchronizer.addStateToExplore: subsuming state: SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
			//		+ state.getFullStateAsString() + "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
		}
	}

	//JDA: Begin modification 'actionIsConnected','isError' and 'isUnknown'
	//JDA: actionIsConnected is needed because an action can be logically equivalent to an object in
	//JDA:    'connectedActions', while being a different object. I don't want to write 
	//JDA:    a equals function and Hash, if I can avoid it.
	private boolean actionIsConnected(ActionInput action) {
		Collection<ActionInput> cv = connectedActions.values();
		for (ActionInput ai: cv) {
				boolean namesEqual = ai.getName().equals(action.getName());
				boolean signalsEqual = ai.getSignalName().equals(action.getSignalName());
				boolean portsEqual = ai.getPortName().equals(action.getPortName());
				if (namesEqual && signalsEqual && portsEqual) {
					return true;
				}
		}
		return false;
	}
	
	//JDA: Begin Addition
	private boolean actionIsTimer (ActionInput action) {
		if (action.getPortName() == "") {
			return true;
		}
		return false;
	}
	//JDA: End Addition.
	
	private boolean isError(NodeSymbolic ns) {
		Location loc = ns.getContents().getLocation();
		return (loc.getName() == "Error") && (loc.getContents() == null);
	}
	
	private boolean isUnknown(NodeSymbolic ns) {
		Location loc = ns.getContents().getLocation();
		return (loc.getName() == "Unknown") && (loc.getContents() == null);
	}
	//JDA: End modification 'isError' and 'isUnknown'
	
	private void exploreState(SynchronizationState currentState) {
		//System.out.println("JDA: Synchronizer.exploreState.");

		//System.out.println(tree.getName()+ " -- To explore " +
		//toExplore.size() + "   Explored " + explored.size());

		if ((toExplore.size() + explored.size()) % 1000 == 0) {
			//System.out.println(tree.getName() + " -- To explore "
			//		+ toExplore.size() + "   Explored " + explored.size());
			System.gc();
		}
		//System.out.println(("To explore"));

		// copy state that will be explored
		SynchronizationState currentCopy = new SynchronizationState(currentState, "exploreState");

		// JDA: debug string save.
		// String save = currentCopy.getFullStateAsString();

		// check open
		checkOpenTransitions(currentCopy);

		// String newstr = currentCopy.getFullStateAsString();
		// if (!save.equals(newstr)) {
		//System.out.println("JDA: Synchronizer.exploreState currentState changed in checkOpenTransitions.");
		//System.out.println("    old: !!!" + save + "!!!");
		//System.out.println("    new: !!!" + newstr + "!!!");
		// save = newstr;
		// }
		// check relayed actions (inside open)
		checkRelayActions(currentCopy);

		// newstr = currentCopy.getFullStateAsString();
		// if (!save.equals(newstr)) {
		//    System.out.println("JDA: Synchronizer.exploreState currentState changed in checkRelayActions.");
		//    System.out.println("    old: !!!" + save + "!!!");
		//    System.out.println("    new: !!!" + newstr + "!!!");
		//    save = newstr;
		// }
		// check already synchronized
		checkSynchronized(currentCopy);

		// newstr = currentCopy.getFullStateAsString();
		// if (!save.equals(newstr)) {
		//    System.out.println("JDA: Synchronizer.exploreState currentState changed in checkSynchronized.");
		//    System.out.println("    old: !!!" + save + "!!!");
		//    System.out.println("    new: !!!" + newstr + "!!!");
		//    save = newstr;
		// }

		// check each sent signal in a queue
		//System.out.println("JDA: Synchronizer.exploreState (after checkSynchronized).");
		//System.out.println("JDA: Synchronizer.exploreState: all queues: !!!" + currentCopy.getAllQueues() + "!!!.");
		for (Queue q : currentCopy.getAllQueues()) {
			Queue qCopy = new Queue(q);
			if (qCopy.isEmpty()) {
				continue;
			}
			//System.out.println("JDA: Synchronizer.exploreState: q: !!!" + q + "!!!.");
			// get message
			SentMessage signal = q.getFrom();
			//System.out.println("JDA: Synchronizer.exploreState: signal: !!!" + signal + "!!!.");
			boolean received = false;

			SymbolicExecutionTree receiver = signal.getReceiver();
			//System.out.println("JDA: Synchronizer.exploreState: receiver: !!!" + receiver + "!!! .");
			NodeSymbolic nodeReceiver = currentCopy.getCurrentNode(receiver);
			
			//System.out.println("JDA: Synchronizer.exploreState: nodeReceiver: !!!" + 
			//		nodeReceiver.getFullNodeSymbolicAsString()+ "!!! .");
			// try to unwind
			
			boolean isSubsumed;
			if (nodeReceiver.getSubsumedBy() != null) {
				isSubsumed = true;
				//System.out.println("[JDA: Synchronizer.exploreState: nodeReceiver: !!!"
				//				+ nodeReceiver.toString()
				//				+ "!!! will be subsumed by:");
				nodeReceiver = nodeReceiver.getSubsumedBy();
				//System.out.println("JDA: Synchronizer.exploreState: nodeReceiver: !!!"
				//				+ nodeReceiver.toString() + "]!!!.");
			}
			//System.out.println("JDA: Synchronizer.exploreState: isSubsumed = " + isSubsumed + ".");
			// find arc with action for synch
			// JDA: change to aid debug
			List<Arc<SymbolicTransition, SymbolicState>> listOfArcs = nodeReceiver.getChildren();
			//System.out.println("JDA: Synchronizer.exploreState: listOfArcs: !!!"
			//		+ listOfArcs + "!!!.");
			//JDA: debug only! Commented out
			/* ------------
			if ((listOfArcs.size() == 0) && (nodeReceiver.getContents().getLocation().getName() != "Error")) {
				//JDA: This action will not be received!
				//System.out.println("JDA: Synchronizer.exploreState: listOfArcs (aka children) of the nodeReceiver is empty!");
			}
			else if (listOfArcs.size() > 1) {
				//System.out.println("JDA: Synchronizer.exploreState: listOfArcs (aka children) of the nodeReceiver has more than one element.");
			}
			else if (listOfArcs.size() > 1) {
				//System.out.println("JDA: Synchronizer.exploreState: listOfArcs (aka children) of the nodeReceiver has one element.");
			}
			---------- */
			int arcNo = -1;
			for (Arc<SymbolicTransition, SymbolicState> arc : listOfArcs) {
				SymbolicTransition transition = arc.getContents();
				nodeReceiver.getContents().getIngoingArc().getTarget();
				arcNo++;
				// check if free for synchronization
				if (transition.getInputExecution() instanceof SynchronizationAction) {
					//System.out.println("JDA: Synchronizer.exploreState: transition: !!!"
					//						+ transition.toString() + "!!!.");
					//System.out.println("JDA: Synchronizer.exploreState: SynchroniationAction. Continuing.");
					continue;
				}

				// check if actions agree
				if (!transition.getInputExecution().getAction().equals(signal.getAction().getAction())) {
					continue;
				}
				//System.out.println("JDA: Synchronizer.exploreState: arc: !!!" + arc.toString() + "!!!.");
				//System.out.println("JDA: Synchronizer.exploreState: arc.getSource(): !!!" +
				//		arc.getSource().toString() + "!!!.");
				//System.out.println("JDA: Synchronizer.exploreState: arc.getTarget(): !!!" +
				//		arc.getTarget().toString() + "!!!.");
				//System.out.println("JDA: Synchronizer.exploreState: transition: !!!"
				//				+ transition.toString() + "!!!.");

				// create synchronization
				SynchronizationAction synchAction = new SynchronizationAction(
						signal.getAction(), transition.getInputExecution());
				List<NodeSymbolic> nds = currentState.getNodes();
				for (NodeSymbolic nd: nds) {
					if (nd.getSubsumedBy() != null) {
						//System.out.println("JDA: Synchronizer.exploreState: nd is subsumed.");
					}
				}

				// Create new state
				SynchronizationState newState = new SynchronizationState(currentCopy, "exploreState");
				//boolean deep = true;
				//SynchronizationState newState = new SynchronizationState(currentCopy, "exploreState (" + transition + ")", deep);
				//System.out.println(
				//		"JDA: Synchronizer.exploreState: newState after \'newState = new SynchronizationState(currentCopy);\': !!!"
				//		+ newState.getFullStateAsString() + "!!!.");

				// find target
				NodeSymbolic target = (NodeSymbolic) arc.getTarget();
				newState.setCurrentNode(receiver, target);
				//create new node
				NodeSymbolicSynch node = new NodeSymbolicSynch(newState,
					roleTrees, compositeTree);
				newState.setNode(node);

				boolean isSatisfiable = newState.checkSatsfiability(synchAction, node);
				//System.out.println("JDA: Synchronizer.exploreState: satisfiability of synchronization action !!!"
				//				+ synchAction + "!!! is " + isSatisfiable + ".");
				if (!isSatisfiable) {
					continue;
				}
				fixStates(currentState, newState);
				received = true;

				// fire action - update values, send actions
				//newState.updateValuation(target, synchAction);
				newState.updateValuation(receiver, target, synchAction, newState.getNode());
				//System.out.println("JDA:  Synchronizer.exploreState: target: !!!"
				//				+ target.toString() + "!!!.");
				//System.out.println("JDA:  Synchronizer.exploreState: synchAction: !!!"
				//				+ synchAction.toString() + "!!!.");
				//System.out.println("JDA: Synchronizer.exploreState: newState after \'newState.updateValuation(target, synchAction);\': !!!"
				//				+ newState.getFullStateAsString() + "!!!.");
				for (ActionOutputInstance output : transition.getOuputSequence().getSequence()) {
					sendSignal(output, newState);
				}
				//System.out.println("JDA: Synchronizer.exploreState: newState after sending signals: !!!"
				//				+ newState.getFullStateAsString() + "!!!.");
				// create transition
				SymbolicTransition trans = new SymbolicTransition(
						transition.getTransition(), synchAction,
						transition.getTree());
				trans.setOutputSequence(transition.getOuputSequence());

				//JDA: Try moving the creation of the new node further back
				// create new node
				//NodeSymbolicSynch node = new NodeSymbolicSynch(newState,
				//		roleTrees, compositeTree);
				//newState.setNode(node);
				currentState.getNode().addChild(node, trans);
                node.setIncomingArc(arc);
				addStateToExplore(newState, currentState);
				//System.out.println("JDA: Synchronizer.exploreState: addStateToExplore has returned.");		
			}

			if (!received) {
				if (! isError(nodeReceiver)) {
					//System.out.println("Unreceived transition: currentState: [" + currentState.getFullStateAsString() + "] ...");
					//System.out.println("Unreceived transition: receiver: [" + receiver + "] ...");
					//System.out.println("Unreceived transition: nodeReceiver: [" + nodeReceiver + "]");
					createUnreceivedAction(currentCopy, signal.getAction(),
						receiver);
				}
			}
			currentCopy.replaceQueue(q, qCopy);
		}
	}

	
	private void checkSynchronized(SynchronizationState currentState) {
		//System.out.println("JDA: Synchronizer.checkSynchronized: currentState: !!!"
		//				+ currentState.getFullStateAsString() + "!!!.");
		for (SymbolicExecutionTree tree : roleTrees.values()) {
			NodeSymbolic inner = currentState.getCurrentNode(tree);
			for (Arc<SymbolicTransition, SymbolicState> arc : inner.getChildren()) {

				// find trigger
				SymbolicTransition transition = arc.getContents();
				ActionInputSymbolic trigger = transition.getInputExecution();

				// JDA: modified the following
				// if (!(trigger instanceof SynchronizationAction) &&
				// !trigger.getAction().getName().contains("Timer")){
				// continue;
				// }
				if (!(trigger instanceof SynchronizationAction)) {
					// JDA: if the port name is empty assume that the port is a
					// timing port.
					if (trigger.getAction().getPortName().equals("")) {
						//System.out.println("JDA - Synchronizer.checkSynchronized (portname = !!!), name = !!!"
						//				+ trigger.getAction().getName() + "!!!.");
					} else {
						//System.out.println("JDA - Synchronizer.checkSynchronized (portname = !!!), name = !!!"
						//		+ trigger.getAction().getName() + "!!!.");
						continue;
					}
				}

				// check whether not connected
				//JDA: Modified the following:
				ActionInput action = trigger.getAction();
				if (actionIsConnected(action)) {
					continue;
				}
				//JDA: End modification
				//System.out.println("JDA - Synchronizer.checkSynchronized treating: !!!"
				//				+ trigger.getAction().getName()
				//				+ "!!! as a synchronization action.");
				// create new state
				SynchronizationState newState = new SynchronizationState(currentState, "checkSynchronized");
				//boolean deep = true;
				//SynchronizationState newState = new SynchronizationState(
				//		currentState, "checkSynchronized", deep);
				//JDA: End change.

				// find target
				NodeSymbolic target = (NodeSymbolic) arc.getTarget();
				if (isError(target)) {
					//System.out.println("JDA: receiver, " + target + ", is in 'Error'.");
					continue;
				}
				if (isUnknown(target)) {
					//System.out.println("JDA: receiver, " + target + ", is in 'Unknown'.");
					continue;
				}
				//TODO: JDA: Why was there no "subsumed by" check here on "checkSynchronized"?
				if (target.getSubsumedBy() != null) {
					//System.out.println("[JDA: Synchronizer.checkSynchronizer: target: !!!"
					//				+ target.toString()
					//				+ "!!! will be subsumed by:");
					target = target.getSubsumedBy();
					//System.out.println("JDA: Synchronizer.exploreState: target: !!!"
					//				+ target.toString() + "]!!!.");
				}
				
				for (ActionOutputInstance output : transition.getOuputSequence().getSequence()) {
					if (relayAction.containsTree(tree)
							&& relayAction.containsRelayOutput(output.getActionOutput())) {
						output.replaceAction(relayAction.getOuterOutput(output.getActionOutput()));
					} else {
						sendSignal(output, newState);
					}
				}

				// switch nodes
				// newState.setTreeNode(target); //JDA: added for test. This is
				// bad here.
				newState.setCurrentNode(tree, target);
				//newState.updateValuation(target);
				newState.updateValuation(tree, target, newState.getNode());

				// create new node and connect
				NodeSymbolicSynch newNode = new NodeSymbolicSynch(newState,
						roleTrees, compositeTree);
				newState.setNode(newNode);
				fixStates(currentState, newState);
				newNode.setIncomingArc(arc);
				//JDA: moved this down
				//newState.updateValuation(tree, target, newState.getNode());
				currentState.getNode().addChild(newNode, transition);
				//System.out.println("JDA: Synchronizer.checkSynchronized: transition: " + transition + ".");
				//System.out.println("JDA: Synchronizer.checkSynchronized: before addStateToExplore.");
				addStateToExplore(newState, currentState);
				//System.out.println("JDA: Synchronizer.checkSynchronized: addStateToExplore has returned.");
			}
		}
	}

	private void checkOpenTransitions(SynchronizationState currentState) {
		//System.out.println("JDA: Synchronizer.checkOpenTransitions: currentState: !!!"
		//				+ currentState.getFullStateAsString() + "!!!.");
		/*
		 /*=== JDA commented out=====================
		 * This code introduced duplicate transitions.
		 * Change done on March 11, 2013. Rev. 445.
		for (SymbolicExecutionTree tree : roleTrees.values()){
			NodeSymbolic openNode = currentState.getCurrentNode(tree);
			//if (openNode.getSubsumedBy()!=null){
			//	openNode = openNode.getSubsumedBy();
			//}
			for (Arc<SymbolicTransition,SymbolicState> arc : openNode.getChildren()){
				
				//find trigger
				SymbolicTransition transition = arc.getContents();
				ActionInputSymbolic trigger = transition.getInputExecution();
				
				//check whether not connected
				if(tree.containsInputAction(trigger.getAction()) &&
						connectedActions.values().contains(trigger.getAction())){
					continue;
				}
				//create new state
				SynchronizationState newState = new SynchronizationState(currentState);
				
				//find target
				NodeSymbolic target = (NodeSymbolic) arc.getTarget();
				
				//send signals
				sendSignals(transition.getOuputSequence(), newState);
				
				//switch nodes
				newState.setCurrentNode(tree, target);
				
				//create new node and connect
				NodeSymbolicSynch newNode = new NodeSymbolicSynch(newState,roleTrees,compositeTree);
				newState.setNode(newNode);
				currentState.getNode().addChild(newNode, transition);
				
				addStateToExplore(newState);
					
			}
				
		}
		JDA: end comment out ============== */
		NodeSymbolic openNode = currentState.getMainTreeNode();
		// JDA: Uncommenting the following to the version prior to the version
		// JDA: immediately prior to Revision 445.
		if (openNode.getSubsumedBy() != null) {
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: nodeReceiver: !!!"
			//				+ openNode.toString() + "!!! will be subsumed by:");
			openNode = openNode.getSubsumedBy();
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: nodeReceiver: !!!"
			//				+ openNode.toString() + "!!!.");
		}
		// JDA: end uncommented region.
		for (Arc<SymbolicTransition, SymbolicState> arc : openNode.getChildren()) {
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: arc: !!!"
			//		+ arc.toString() + "!!!.");
			// find trigger
			SymbolicTransition transition = arc.getContents();
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: transition: !!!"
			//		+ transition.toString() + "!!!.");
			ActionInputSymbolic trigger = transition.getInputExecution();
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: trigger: !!!"
			//		+ trigger.toString() + "!!!.");
			// check whether not connected   
			
			ActionInput action = trigger.getAction();
			if (actionIsConnected(action)) {
				continue;
			}
			if (actionIsTimer(action)) {
				//System.out.print("JDA: Synchronizer.checkOpenTransitions: Action, " + action + " is treated as open.");
			}
			//JDA: Note that timer actions may be treated as equivalent to open ones.
			SynchronizationState newState = new SynchronizationState(currentState, "checkOpenTransitions");
			//boolean deep = true;
			//SynchronizationState newState = new SynchronizationState(
			//		currentState, "checkOpenTransitions (" + transition + ")", deep);
			
			//find target
			NodeSymbolic target = (NodeSymbolic) arc.getTarget();
			
			// send signals
			sendSignals(transition.getOuputSequence(), newState);

			// switch nodes
			newState.setCurrentNode(tree, target);
			newState.updateValuation(tree, target, newState.getNode());

			// create new node and connect
			NodeSymbolicSynch newNode = new NodeSymbolicSynch(newState,
					roleTrees, compositeTree);
			newState.setNode(newNode);
			fixStates(currentState, newState);
			NodeSymbolic nd = currentState.getNode();
			nd.addChild(newNode, transition);
			newNode.setIncomingArc(arc);
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: transition: " + transition + ".");
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: before addStateToExplore.");
			addStateToExplore(newState, currentState);
			//System.out.println("JDA: Synchronizer.checkOpenTransitions: addStateToExplore has returned.");

		}

	}

	private void checkRelayActions(SynchronizationState currentState) {
		//System.out.println("JDA: Synchronizer.checkRelayActions: currentState: !!!"
		//				+ currentState.getFullStateAsString() + "!!!.");
		//System.out.println("JDA -                                relayAction.getTrees() is !!!"
		//				+ relayAction.getTrees() + "!!!");
		for (SymbolicExecutionTree relayedTree : relayAction.getTrees()) {

			NodeSymbolic openNode = currentState.getCurrentNode(relayedTree);

			if (openNode == null) {
				return;
			}

			for (Arc<SymbolicTransition, SymbolicState> arc : openNode
					.getChildren()) {
				// find trigger
				SymbolicTransition transition = arc.getContents();

				ActionInputSymbolic trigger = transition.getInputExecution();

				// check whether not connected
				if (!relayAction.containRelayInput(trigger.getAction())) {
					continue;
				}
				// create new state
				//TODO: JDA: Why is there no "subsumed by" check here?
				SynchronizationState newState = new SynchronizationState(currentState, "checkRelayActions");
				//boolean deep = true;
				//SynchronizationState newState = new SynchronizationState(
				//		currentState, "checkRelayActions (" + transition + ")", deep);

				// find target
				NodeSymbolic target = (NodeSymbolic) arc.getTarget();

				// send signals
				sendSignals(transition.getOuputSequence(), newState);

				// switch nodes
				newState.setCurrentNode(relayedTree, target);

				// create new transition
				ActionInput ai = relayAction.getOuterInput(trigger.getAction());
				ActionInputSymbolic triggerNew = new ActionInputSymbolic(ai,
						false, null);
				triggerNew.mapVariables(trigger);
				triggerNew.setAction(relayAction.getOuterInput(trigger
						.getAction()));
				SymbolicTransition newTransition = new SymbolicTransition(
						transition.getTransition(), triggerNew, relayedTree);

				// create new node and connect
				NodeSymbolicSynch newNode = new NodeSymbolicSynch(newState,
						roleTrees, compositeTree);
				newState.setNode(newNode);
				fixStates(currentState, newState);
				newNode.setIncomingArc(arc);
				currentState.getNode().addChild(newNode, newTransition);
				//System.out.println("JDA: Synchronizer.checkRelayActions: transition: " + transition + ".");
				addStateToExplore(newState, currentState);
				//System.out.println("JDA: Synchronizer.checkRelayActions: addStateToExplore has returned.");
			}

		}

	}

	private void createUnreceivedAction(SynchronizationState currentState,
			ActionInputSymbolic queueAction, SymbolicExecutionTree receiver) {
		//System.out.println("JDA: Synchronizer.createUnreceivedAction: currentState: !!!"
		//				+ currentState.getFullStateAsString() + "!!!.");
		//System.out.println("JDA:                                      queueAction: !!!"
		//				+ queueAction + "!!!.");
		//System.out.println("JDA:                                      receiver: !!!"
		//				+ receiver + "!!!.");
		// new state and node
		SynchronizationState newState = new SynchronizationState(currentState, "createUnreceivedAction");
		NodeSymbolicSynch newNode = new NodeSymbolicSynch(newState, roleTrees,
				compositeTree);
		newState.setNode(newNode);
		fixStates(currentState, newState);

		// connection
		UnreceivedAction action = new UnreceivedAction(queueAction);

		// transition
		SymbolicTransition transition = new SymbolicTransition(null, action,
				receiver);

		currentState.getNode().addChild(newNode, transition);
		TreeNode<SymbolicState, SymbolicTransition> source = currentState.getMainTreeNode();
		TreeNode<SymbolicState, SymbolicTransition> target = newState.getMainTreeNode();
		Arc<SymbolicTransition, SymbolicState> arc = new Arc<SymbolicTransition, SymbolicState>(transition, source, target);
		newNode.setIncomingArc(arc);
		//System.out.println("JDA: Synchronizer.createUnreceivedAction: transition: " + transition + ".");
		addStateToExplore(newState, currentState);
		//System.out.println("JDA: Synchronizer.createUnreceivedAction: addStateToExplore has returned.");
	}

	private void sendSignal(ActionOutputInstance ai, SynchronizationState state) {
		//System.out.println("JDA; Synchronizer.sendSignal: ai (action output instance): !!!"
		//				+ ai + "!!!.");
		//System.out.println("JDA:                          state (synchronization state): !!!"
		//				+ state.getFullStateAsString() + "!!!.");
		// find tree
		SymbolicExecutionTree receiver = connectedTrees.get(ai
				.getActionOutput());

		if (receiver == null) {
			//System.out.println("JDA: Synchronizer.sendSignal receiver == !!!<null!!!>.");
			return;
		}
		//System.out.println("JDA: Synchronizer.sendSignal receiver == !!!"
		//		+ receiver + "!!!.");
		// find input signal
		ActionInput input = connectedActions.get(ai.getActionOutput());
		//System.out.println("JDA; Synchronizer.sendSignal input == !!!" + input
		//		+ "!!!.");
		// create Symbolic
		ActionInputSymbolic as = new ActionInputSymbolic(input,
				ai.getSymbolicMapping(), ai.getSymbolicVariableMapping(), null);
		//System.out.println("JDA: Synchronizer.sendSignal ActionInputSymbolic as == !!!"
		//				+ as + "!!!.");
		state.addSignal(receiver, as);

	}
}
