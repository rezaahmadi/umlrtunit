package symbolic.execution.tree.synch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.PassiveObject;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.CompareOperators;
//import symbolic.execution.constraints.manager.operators.LogicalOperators;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
//import symbolic.execution.ffsm.model.ActionInput;
//import symbolic.execution.ffsm.model.ActionVariableOutput;
//import symbolic.execution.ffsm.model.ModelVariable;

public class SynchronizationAction extends ActionInputSymbolic {
	
	private Map<SymbolicVariable, Expression> valuationMapping;

	private List<Constraint> valuationConstraints;
	
	public SynchronizationAction(ActionInputSymbolic queueAction,
			ActionInputSymbolic inputAction) {
		super(inputAction);
			
		valuationMapping = new HashMap<SymbolicVariable, Expression>();
		valuationConstraints = new ArrayList<Constraint>();
		//create mapping for new valuation
		for(Expression symbVarOrig : inputAction.getMappingSymbolic().keySet()){				
			Expression expr = queueAction.getMappingSymbolic().get(symbVarOrig);
			SymbolicVariable symbVar = (SymbolicVariable) inputAction.getMappingSymbolic().get(symbVarOrig);
			if (expr != null){
				valuationMapping.put((SymbolicVariable)symbVar, expr);
				if (! (symbVar instanceof PassiveObject)){
					valuationConstraints.add(SymbolicFactory.createRelationalConstraint(symbVar, CompareOperators.eq, expr));
				}
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(getAction().getName());
		sb.append("(");
		for(SymbolicVariable var : valuationMapping.keySet()){
			sb.append(valuationMapping.get(var).toString());
			sb.append(",");
		}
		if (valuationMapping.size() > 0){
			return sb.substring(0,sb.length()-1) +")";
		} else {
			return sb.toString() + ")";
		}	
		//return sb.toString();
	}

	public Map<SymbolicVariable,Expression> getValuationMapping() {
		return valuationMapping;
	}
	
	public List<Constraint> getValuationConstraints(){
		return valuationConstraints;
	}

}
