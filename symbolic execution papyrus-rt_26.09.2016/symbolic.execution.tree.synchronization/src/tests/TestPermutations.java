package tests;

import symbolic.execution.tree.synch.utils.PermutationGenerator;

public class TestPermutations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] letters = new String[]{"a","c","d"};
		PermutationGenerator gen = new PermutationGenerator(3);
		
		while(gen.hasMore()){
			StringBuffer permutation = new StringBuffer();
			int[] perm = gen.getNext();
			for (int i=0; i<perm.length; i++){
				permutation.append(letters[perm[i]]);
			}
			System.out.println(permutation);
		}

	}

}
