/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package symbolic.execution.utils;

import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.RedefinableElement;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.internal.operations.StateMachineOperations;

import java.util.ArrayList;
import java.util.List;
//import java.awt.event.ItemListener;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsulePartUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.MessageUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.ProtocolUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.StateMachineUtils;
import org.eclipse.papyrusrt.umlrt.core.utils.UMLRTProfileUtils;
//import org.eclipse.emf.ecore.EObject;
//import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
//import org.eclipse.papyrusrt.xtumlrt.common.Capsule;
//import org.eclipse.papyrusrt.xtumlrt.util.XTUMLRTUtil;
//import org.eclipse.papyrusrt.xtumlrt.common.*;
import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.Capsule;
import org.eclipse.papyrusrt.umlrt.profile.UMLRealTime.CapsulePart;
import org.eclipse.papyrusrt.xtumlrt.util.XTUMLRTUtil;


/**
 * @author reza
 *
 */
public class UmlrtUtil {
	
	public static Class getCapsule(Class uml2Obj){
		// As Papyrus-RT model elemets are UML2 classes with specific
		// stereotypes, we already have associated UML2 classes 
		// ToDo: make sure we do not need another way 
		// to retrieve the actual Capsule object
		if (isCapsule(uml2Obj))
			return uml2Obj;
		return null;
	}
	
	public static boolean isCapsule (Classifier obj){
		return CapsuleUtils.isCapsule(obj);
//		for (Stereotype s : obj.getAppliedStereotypes()){
//		if (s instanceof Capsule)
//			return true;
//	}
//	return false;
	}
	
	//ToDo: this must be recursive
	public static List<Property> getCapsuleParts(Class capsule){
		List<Property> res = new ArrayList<Property>();
		for (Property part: capsule.getParts()){
			for (Stereotype s : part.getAppliedStereotypes())
				if (s.getName().equals("CapsulePart"))
					res.add(part);
		}
		
		return res;
//		return XTUMLRTUtil.getAllCapsuleParts(capsule);
	}
	
//	static List<Property> parts = new ArrayList<Property>();
	public static void getCapsulePartsRecursive(Class capsule, List<Property> parts) {
		if (capsule == null)
			return;
//		List<Property> tmpParts = new ArrayList<Property>();
		for (Property part : capsule.getParts())
			for (Stereotype s : part.getAppliedStereotypes())
				if (s.getName().equals("CapsulePart")){
					parts.add(part);
					getCapsulePartsRecursive((Class) part.getType(), parts);
				}
//		parts.addAll(tmpParts);
//		for (Property part : tmpParts)
//			getCapsulePartsRecursive((Class) part.getType(), parts);

//		return parts;
	}
	
	public static StateMachine getStateMachine(Class capsule) {
		// in ibm: capsule.getPrimaryStateMachine().getReferenceTarget();
		if (capsule.getOwnedBehaviors() != null && capsule.getOwnedBehaviors().size() > 0) {
			Behavior b = capsule.getOwnedBehaviors().get(0);
			if (b instanceof StateMachine)
				return (StateMachine) b;
//			throw new Exception("no state machine for capsule");
		}
		return null;
//		throw new Exception("no state machine for capsule");
	}
	
	public void getAllRedifinedPorts(Capsule capsule){
			
	}
	
	public static Property getPartWithPort(ConnectorEnd end){
		return end.getPartWithPort();
		//return end.getPartWithPort();
	}
 
	public static EList<Port> getOwnedPorts(Class capsule) {
		return capsule.getOwnedPorts();
	}
	
	public static EList<Port> getPorts(Class capsule) {
		return capsule.getOwnedPorts();
	}
	
	public static Port getPort(Class capsule, String portName) {
		for (Port p : getPorts(capsule)){
			if (p.getName().equals(portName))
				return p;
		}
		return null;
	}

	
	public static List<Operation> getInEvents(Port port){
		return ProtocolUtils.getMessageSetIn((Collaboration)port.getType()).getAllOperations();
		
	}

	public static List<Operation> getOutEvents(Port port) {
		return ProtocolUtils.getMessageSetOut((Collaboration)port.getType()).getAllOperations();
	}
	
	public static CallEvent getCallEvent(Operation op){
		return MessageUtils.getCallEvent(op);
	}

	public static EList<Connector> getConnectors(Class capsule) {
		return capsule.getOwnedConnectors();
	}

	public static Port getLocallyRedefinedPort(Port port, Class capsule) {
		Port portRedefined = null;
		RedefinableElement elemRedefined = capsule.getRedefinedElement(port.getName());
		if (elemRedefined != null && elemRedefined instanceof Port)
			portRedefined = (Port) elemRedefined;
		
		//otherwise we cannot run the compound symbolic execution 
		if (portRedefined == null)
			portRedefined = port;
		
		return portRedefined;
	}
	
	public static Region getLocallyRedefinedRegion(Region reg, Class stm) {
		Region regRedefined = null;
		RedefinableElement elemRedefined = stm.getRedefinedElement(reg.getName());
		if (elemRedefined != null && elemRedefined instanceof Region)
			regRedefined = (Region) elemRedefined;
		return regRedefined;
	}
	
	public static Class selectIfCapsule(Object selected) {
		Resource capsuleResource = null;
		if (selected instanceof Property)
			selected = ((Property)selected).getClass_();
		if (selected instanceof Class) {
			if (selected instanceof Behavior) {
				capsuleResource = ((Class) selected).eContainer().eResource();
				selected = ((Class) selected).eContainer();
			} else {
				capsuleResource = ((Class) selected).eResource();
			}
			return getCapsule((Class) selected); // RTFactory.CapsuleFactory.getCapsule((Class)selected);
		}
		return null;
	}
	
//	public static Class getCapsuleUnderTest(Object selected) {
//		Class capsule = selectIfCapsule(selected);
//		for (Property part: capsule.getParts()){
//			for (Stereotype s : part.getAppliedStereotypes())
//				if (s.getName().equals("CapsuleUnderTest"))
//					return (Class) part.getType();
//		}
//		return capsule;
//	}
	
	public static Class getCapsuleByStereotypeName(Object selected, String stereotype) {
		Class capsule = selectIfCapsule(selected);
		List<Property> parts = new ArrayList<Property>();
		getCapsulePartsRecursive(capsule, parts);
		for (Property part : parts) {
			for (Stereotype s : part.getAppliedStereotypes())
				if (s.getName().equals(stereotype))
					return (Class) part.getType();
		}
		return null;
		// throw new Exception(String.format("Capsule with Stereotype: %s does
		// not exist.", stereotype));
	}
	
//	public static Class getCapsuleByStereotype(Object selected) {
//		Class capsule = selectIfCapsule(selected);
//		for (Property part: capsule.getParts()){
//			for (Stereotype s : part.getAppliedStereotypes())
//				if (s.getName().equals("TestSuits"))
//					return (Class) part.getType();
//		}
//		return null;
//	}
//	
//	public static List<Class> getCapsuleByStereotype(Class capsule) {
//		List<Class> testProps = new ArrayList<Class>();
//		for (Property part: capsule.getParts()){
//			for (Stereotype s : part.getAppliedStereotypes())
//				if (s.getName().equals("CapsuleTestProperty"))
//					testProps.add((Class) part.getType());
//		}
//		return testProps;
//	}
//
//	public static List<String> getAllMessages(Class testProp) {
//		// TODO Auto-generated method stub
//		List<String> triggers = new ArrayList<String>();
//		for (Interface op:  getStateMachine(testProp).getAllImplementedInterfaces()){
//			String o = op.getName();
//			triggers.add(o);
//		}
//		
//		for (Operation op:  getStateMachine(testProp).getAllOperations()){
//			String o = op.getName();
//			triggers.add(o);
//		}
//		return triggers;
//	}

	public static List<Transition> getAllTransitions(Class propertyCapsule) {
		// TODO Auto-generated method stub
		List<Transition> transitions = getStateMachine(propertyCapsule).getRegions().get(0).getTransitions();
		return transitions;
	}

	public static List<Vertex> getAllVertexes(Class propertyCapsule) {
		// TODO Auto-generated method stub
		return UmlrtUtil.getStateMachine(propertyCapsule).getRegions().get(0).getSubvertices();
	}

	public static List<String> getTriggers(Class testProp) {
		// TODO Auto-generated method stub
//		for (Transition t : UmlrtUtil.getAllTransitions(testProp)) {
//			List<Trigger> triggers = t.getTriggers();
//			if (triggers != null && triggers.size() > 0){
//				Object e2 = triggers.get(0).getPorts().get(0);
//				Object e3 = triggers.get(0).getEvent();
////				List<Element> elems = triggers.get(0).getEvent().eGet(Operation.class);
//				propTriggers.add(triggers.get(0).getEvent().getName());
//			}
//		}
		
		//ToDo: fix this BS
		List<String> list = null;
		if (testProp.getName().equals("prop1CruiseControl"))
			list = new ArrayList<String>() {
				{
					add("on");
					add("engineOff");
					add("disableControl");
					add("engineOn");
				}
			};
		else if (testProp.getName().equals("prop2CruiseControl"))
			list = new ArrayList<String>() {
				{
					add("engineOff");
					add("engineOn");
					add("on");
				}
			};

		return list;
	}

	public static Pseudostate getInitialState(StateMachine stateMachine) {
		// TODO Auto-generated method stub
		return getInitialState(stateMachine.getRegions().get(0).getSubvertices());
	}
	
	public static Pseudostate getInitialState(List<Vertex> vertexes) {
		// TODO Auto-generated method stub
		for (Vertex v: vertexes){
			if (v instanceof Pseudostate)
				if ( ((Pseudostate)v).getKind() == PseudostateKind.INITIAL_LITERAL)
					return (Pseudostate)v;
		}
		return null;
	}
	
	//ToDo: remove the above method as the below one support it.
//	public static Pseudostate getConnectionPoint(StateMachine stateMachine, String exitpoint) {
//		// TODO Auto-generated method stub
//		for (Pseudostate conn: stateMachine.getConnectionPoints()){
//			if ( ((Pseudostate)conn).getKind() == PseudostateKind.EXIT_POINT_LITERAL && conn.getName().equals(exitpoint))
//					return (Pseudostate)conn;
//		}
//		return null;
//	}
	
	//ToDo: remove the above method as the below one support it.
	public static Pseudostate getConnectionPoint(StateMachine stateMachine, String point, PseudostateKind kind) {
		// TODO Auto-generated method stub
		for (Vertex v : stateMachine.getRegions().get(0).getSubvertices()) {
			if (v instanceof State) {
				State state = (State) v;
				if (state.getRegions() != null && state.getRegions().size() > 0) {
					for (Pseudostate conn : state.getConnectionPoints()) {
						if (PseudostateKind.EXIT_POINT_LITERAL == kind) {
							if (((Pseudostate) conn).getKind() == PseudostateKind.EXIT_POINT_LITERAL
									&& conn.getName().equals(point))
								return (Pseudostate) conn;
						} else if (PseudostateKind.ENTRY_POINT_LITERAL == kind) {
							if (((Pseudostate) conn).getKind() == PseudostateKind.ENTRY_POINT_LITERAL
									&& conn.getName().equals(point))
								return (Pseudostate) conn;
						}
					}
				}
			}
		}
		return null;
	}

	// e.g. on "TestData" port and "nextState" message
	public static Trigger createTrigger(Class testDriverCapsule, String portName, String msgName) {
		// TODO Auto-generated method stub
		Trigger trig = UMLFactory.eINSTANCE.createTrigger();
		Port testDataPort = UmlrtUtil.getPort(testDriverCapsule, portName);
		trig.getPorts().add(testDataPort);
		Operation nextStateOp = null;
		Collaboration testingProtocol = (Collaboration) testDataPort.getType();
		for (Interface intr : testingProtocol.allRealizedInterfaces()) {
			for (Operation opr : intr.getAllOperations()) {
				if (opr.getName().equals(msgName)) {
					nextStateOp = opr; // (Operation)
										// opr.getOperation("nextState",
										// null, null);
					break;
				}
			}
		}

		CallEvent ev = UMLFactory.eINSTANCE.createCallEvent();
		ev.setOperation(nextStateOp);
		trig.setEvent(ev);

		return trig;
	}

//	public static void addOpaqueBehaviourToState(Vertex state, String actionCode, OpaqueBehaviourPoint point) {
//		OpaqueBehavior ob = UMLFactory.eINSTANCE.createOpaqueBehavior();
//		ob.getLanguages().add("C++");
//		if (point == OpaqueBehaviourPoint.OnEntry) {
//			if (((State) state).getEntry() != null
//					&& ((OpaqueBehavior) ((State) state).getEntry()).getBodies() != null) {
//				actionCode = ((OpaqueBehavior) ((State) state).getEntry()).getBodies().get(0) + actionCode;
//			}
//
//			ob.getBodies().add(actionCode);
//			((State) state).setEntry(ob);
//
//		} else { //OpaqueBehaviourPoint.OnExit
//			if (((State) state).getExit() != null && ((OpaqueBehavior) ((State) state).getExit()).getBodies() != null) {
//				actionCode = ((OpaqueBehavior) ((State) state).getExit()).getBodies().get(0) + actionCode; 
//			}
//
//			ob.getBodies().add(actionCode);
//			((State) state).setExit(ob);
//		}
//	}
	
	public static void addEffectToTransition(Transition trans, String actionCode) {
		OpaqueBehavior ob = UMLFactory.eINSTANCE.createOpaqueBehavior();
		ob.getLanguages().add("C++");
		if (trans.getEffect() != null && ((OpaqueBehavior) trans.getEffect()).getBodies() != null) {
			actionCode = ((OpaqueBehavior) trans.getEffect()).getBodies().get(0) + actionCode;
		}

		ob.getBodies().add(actionCode);
		trans.setEffect(ob);
	}
	
	//This function parses the transition's effect body to extract all output messages
	//e.g., messages in this form: port1.msg1().send().
	//ToDo: finish this method
	public static List<String> getTransitionOutMessages(Transition trans){
		List<String> outMessages = new ArrayList<String>();
		if (trans.getEffect() != null) {
			
			OpaqueBehavior effect = (OpaqueBehavior) trans.getEffect();
			for (String body: effect.getBodies()) {
				
			}
		}
		return outMessages;
	}

	public static Vertex getTestableState(StateMachine stateMachine) {
		// TODO Auto-generated method stub
		for (Vertex v: stateMachine.getRegions().get(0).getSubvertices()) {
			if (v.getName()!=null && v.getName().equals("TESTABLE"))
			{
				State s = (State)v;
				Pseudostate entry = s.getConnectionPoint("init_entry");
				if ( entry!=null)
					return entry.getOutgoings().get(0).getTarget();
			}
		}
		return null;
	}

	
	public static Vertex getState(Class cut, String sName) {
		for (Vertex v:getAllVertexes(cut)) {
			if (v.getName().equals(sName))
				return v;
		}
		return null;
	}

//	private void printTest(TestData test){
//		//printing tests
//		System.out.println(String.format("%s: %s", test.getName(), test.toString()));
//	}
}
