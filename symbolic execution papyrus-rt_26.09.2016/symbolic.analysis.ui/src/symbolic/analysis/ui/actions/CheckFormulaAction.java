package symbolic.analysis.ui.actions;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
//import org.eclipse.uml2.uml.Class;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;

//import symbolic.analysis.formula.checker.exploration.CompositeTreeExplorer;
import symbolic.analysis.formula.checker.process.RequestProcessor;
//import symbolic.analysis.language.AtomicStateFormula;
//import symbolic.analysis.language.utils.IncorrectFormulaException;
import symbolic.analysis.minimal.tree.editor.editors.MinimalTreeEditor;
import symbolic.analysis.minimal.tree.editor.editors.MinimalTreeEditorInput;
import symbolic.execution.constraints.choco.EngineChoco;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.utils.UmlrtUtil;

//import com.ibm.xtools.uml.rt.core.Capsule;
//import com.ibm.xtools.uml.rt.core.RTFactory;
//import org.eclipse.papyrusrt.xtumlrt.common.*;
//import org.eclipse.papyrusrt.umlrt.core.*;
//import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
import org.eclipse.uml2.uml.Class;

/**
 * Our sample action implements workbench action delegate.
 * The action proxy will be created by the workbench and
 * shown in the UI. When the user tries to use the action,
 * this delegate will be created and execution will be 
 * delegated to it.
 * @see IWorkbenchWindowActionDelegate
 */
public class CheckFormulaAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	private Object diagram;
	private Class capsuleContainer;
	/**
	 * The constructor.
	 */
	public CheckFormulaAction() {
		SymbolicFactory.setEngine(new EngineChoco());
	}

	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		if (capsuleContainer == null){
			
			return;
		}
		
		InputDialog inputDialog = new InputDialog(
				window.getShell(),
				"Formula checking",
				"Formula",
				"",
				null
				);
		inputDialog.open();
		
		String formula = inputDialog.getValue();
		
		//parse formula
		try {			
			
			RequestProcessor rp = new RequestProcessor(capsuleContainer);
			rp.process(formula);
			display(rp);
		}catch (Exception e) {
			e.printStackTrace();
			MessageDialog.openWarning(window.getShell(), "Wrong formula", e.getMessage());
		}
		
		
		
	}

	private void display(RequestProcessor rp) {
		MinimalTreeEditorInput input = new MinimalTreeEditorInput(rp);
		
		IWorkbenchPage page = window.getActivePage();
		
		try {
			MinimalTreeEditor editor = (MinimalTreeEditor) page.openEditor(input, MinimalTreeEditor.ID);
		} catch (PartInitException e) {
			System.out.println("PartInitException exception in CheckFormulaAction.dispay.");
			e.printStackTrace();
		}
		
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		getSelectedCapsule(selection);
		
		
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
	
	
	private void getSelectedCapsule(ISelection selection) {		
		if (!(selection instanceof IStructuredSelection)){
			return;
		}		
		IStructuredSelection structured = (IStructuredSelection) selection;	
		
		Object selectedObject = structured.getFirstElement();
		if (diagram == null && selectedObject instanceof GraphicalEditPart){
			GraphicalEditPart part = (GraphicalEditPart) selectedObject;
			
			EditPart diagramEditPart = part.getRoot().getContents();
			diagram = (Diagram) diagramEditPart.getModel();
		}
		
		
		if (selectedObject instanceof IAdaptable){
			IAdaptable adaptableObject = (IAdaptable) selectedObject;			
			selectIfCapsule(adaptableObject.getAdapter(EObject.class));
		}
		
		
	}

	private void selectIfCapsule(Object selected) {				
		if (selected instanceof Class){
			if (UmlrtUtil.isCapsule((Class)selected)){
				capsuleContainer = (Class)selected;				
			}
		}
	}

}