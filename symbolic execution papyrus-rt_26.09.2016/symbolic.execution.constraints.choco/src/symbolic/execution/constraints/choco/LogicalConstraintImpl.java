package symbolic.execution.constraints.choco;

//import java.util.Map;

import choco.Choco;

import symbolic.execution.constraints.manager.Constraint;
//import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.LogicalConstraint;
//import symbolic.execution.constraints.manager.SymbolicValuation;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

public class LogicalConstraintImpl extends LogicalConstraint implements ConstraintChoco{
	private choco.kernel.model.constraints.Constraint chocoConstraint;
	

	public LogicalConstraintImpl(Constraint left, LogicalOperators op, Constraint right){
		super(left,op,right);
		
		if (left == null && right instanceof ConstraintChoco){
			chocoConstraint = createUnaryConstraint(op, (ConstraintChoco) right);
		} else if (left instanceof ConstraintChoco && right instanceof ConstraintChoco){
			chocoConstraint = createConstraint((ConstraintChoco)left, op, (ConstraintChoco)right);
		}		
	}
	
	private choco.kernel.model.constraints.Constraint createUnaryConstraint(
			LogicalOperators op, ConstraintChoco right) {
		switch(op){
		case not : return Choco.not(right.getChocoConstraint());
		};
		return null;
	}

	private choco.kernel.model.constraints.Constraint createConstraint(
			ConstraintChoco left, LogicalOperators op, ConstraintChoco right) {
		
		switch (op){
		case and : return Choco.and(left.getChocoConstraint(), right.getChocoConstraint());
		case or : return Choco.or(left.getChocoConstraint(), right.getChocoConstraint()); 
		case imply : return Choco.implies(left.getChocoConstraint(), right.getChocoConstraint());
		case xor : return Choco.or(Choco.and(Choco.not(left.getChocoConstraint()), right.getChocoConstraint()),
				Choco.and(left.getChocoConstraint(), Choco.not(right.getChocoConstraint())));
		}
		return null;
	}


	@Override
	public choco.kernel.model.constraints.Constraint getChocoConstraint() {		
		return chocoConstraint;
	}
	
	/*@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression replaced = super.replaceVariables(valuation);
		
		if (replaced instanceof LogicalConstraint){
			LogicalConstraint cst = (LogicalConstraint) replaced ;
			
			return new LogicalConstraintImpl(cst.getLeft(), cst.getOp(), cst.getRight());			
		}
		return null;
	}*/

}
