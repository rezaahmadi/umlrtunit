package symbolic.execution.constraints.choco;

//import java.util.Map;

import choco.Choco;
import choco.kernel.model.constraints.Constraint;
//import choco.kernel.model.variables.ComponentVariable;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.real.RealExpressionVariable;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.RelationalConstraint;
//import symbolic.execution.constraints.manager.SymbolicValuation;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.operators.CompareOperators;

public class RelationalConstraintImpl extends RelationalConstraint implements ConstraintChoco{

	private Constraint chocoConstraint;
	
	public RelationalConstraintImpl(Expression left, CompareOperators op, Expression right){
		super(left,op,right);
		
		createChocoConstraint(left, op, right);
	}

	private void createChocoConstraint(Expression left, CompareOperators op,
			Expression right) {
		Variable varLeft = ((ExpressionChoco)left).getChocoVariable();
		Variable varRight = ((ExpressionChoco)right).getChocoVariable();
				
		if (varLeft instanceof IntegerExpressionVariable && varRight instanceof IntegerExpressionVariable){
			chocoConstraint = createIntegerConstraint((IntegerExpressionVariable)varLeft, op, (IntegerExpressionVariable)varRight);
		} else if (varLeft instanceof RealExpressionVariable && varRight instanceof RealExpressionVariable){
			chocoConstraint = createRealConstraint((RealExpressionVariable)varLeft, op, (RealExpressionVariable)varRight);
		}		
	}

	private Constraint createRealConstraint(RealExpressionVariable varLeft,
			CompareOperators op, RealExpressionVariable varRight) {
		Constraint result = null;
		switch(op){
		case eq : result = Choco.eq(varLeft, varRight); break;		
		case geq : result = Choco.geq(varLeft, varRight); break;	
		case leq : result = Choco.leq(varLeft, varRight); break;		
		}
		return result;		
	}

	private Constraint createIntegerConstraint( IntegerExpressionVariable varLeft, CompareOperators op,
			IntegerExpressionVariable varRight) {
		
		Constraint result = null;
		switch(op){
		case eq : result = Choco.eq(varLeft, varRight); break;
		case neq : result = Choco.neq(varLeft, varRight); break;		
		case geq : result = Choco.geq(varLeft, varRight); break;
		case ge : result = Choco.gt(varLeft, varRight); break;
		case leq : result = Choco.leq(varLeft, varRight); break;
		case le : result = Choco.lt(varLeft, varRight); break;
		}
		return result;
	}

	/*@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression replaced = super.replaceVariables(valuation);
		if (replaced instanceof RelationalConstraint){
			RelationalConstraint cst = (RelationalConstraint) replaced;
			return new RelationalConstraintImpl(cst.getLeft(), cst.getOp(), cst.getRight());
		}
		return null;
	}*/

	@Override
	public Constraint getChocoConstraint() {		
		return chocoConstraint;
	}
	
	
}
