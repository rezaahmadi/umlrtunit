package symbolic.execution.constraints.choco;

import choco.Choco;
import choco.kernel.model.variables.Variable;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicValuation;

public class ConstantImpl extends Constant implements ExpressionChoco{
	private Variable chocoConstant;

	public ConstantImpl(Class type, Object value){
		super(type, value);		
		
		chocoConstant = createChocoConstant(type, value);
	}
	
	private Variable createChocoConstant(Class type, Object value) {		
		if (type.equals(Integer.class)){
			return Choco.constant((Integer)value);
		} 
		if (type.equals(Long.class)){
			return Choco.constant((Long)value);
		} 
		if (type.equals(Float.class)){
			return Choco.constant((Float) value);
		} 
		if (type.equals(Double.class)){
			return Choco.constant((Double)value);
		}
		if (type.equals(Boolean.class)){
			if ((Boolean)value == true)
				return Choco.constant((Integer)1);
			else
				return Choco.constant((Integer)0);
		}
		return null;
		
	}

	@Override
	public Variable getChocoVariable() {		
		return chocoConstant;
	}
	
	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		return this;
	}

}
