package symbolic.execution.constraints.choco;

import choco.Choco;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.real.RealExpressionVariable;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicValuation;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;

public class UnaryExpressionImpl extends UnaryExpression implements ExpressionChoco{
	private Variable chocoExpression;

	public UnaryExpressionImpl(ArithmeticOperators op, Expression expr){
		super(op,expr);
		
		createChocoExpression(expr,op);
	}
	
	private void createChocoExpression(Expression expr, ArithmeticOperators op) {
		if (expr instanceof ExpressionChoco){
			Variable var = ((ExpressionChoco)expr).getChocoVariable();
			
			if (var instanceof IntegerExpressionVariable){
				chocoExpression = createIntegerVariable(op,(IntegerExpressionVariable)var);
			}
			if (var instanceof RealExpressionVariable){
				chocoExpression = createRealVariable(op,(RealExpressionVariable)var);
			}
		}
		
	}

	private Variable createRealVariable(ArithmeticOperators op,
			RealExpressionVariable var) {
		switch(op){
		case plus : return var;
		case minus : return Choco.minus(0, var);
		}
		return null;
	}

	private Variable createIntegerVariable(ArithmeticOperators op, IntegerExpressionVariable var) {
		switch(op){
		case plus : return var;
		case minus : return Choco.minus(0,var);
		}
		return null;
	}

	@Override
	public Variable getChocoVariable() {		
		return chocoExpression;
	}
	
	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression replaced = super.replaceVariables(valuation);
		
		if (replaced instanceof UnaryExpression){
			UnaryExpression uExpr = (UnaryExpression) replaced;
			
			return new UnaryExpressionImpl(uExpr.getOp(), uExpr.getExpression());
		}
		
		if (replaced instanceof Constant){
			Constant constant = (Constant)replaced;
			return new ConstantImpl(constant.getType(), constant.getValue());
		}	
		
		return null;
	}

}
