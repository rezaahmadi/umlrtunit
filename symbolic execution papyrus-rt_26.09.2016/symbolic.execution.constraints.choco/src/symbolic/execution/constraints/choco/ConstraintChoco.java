package symbolic.execution.constraints.choco;

import choco.kernel.model.constraints.Constraint;

public interface ConstraintChoco {
	
	public Constraint getChocoConstraint();
}
