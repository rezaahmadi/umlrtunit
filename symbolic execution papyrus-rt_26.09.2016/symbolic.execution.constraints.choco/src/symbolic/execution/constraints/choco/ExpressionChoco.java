package symbolic.execution.constraints.choco;

import choco.kernel.model.variables.Variable;

public interface ExpressionChoco {
	
	public Variable getChocoVariable();

}
