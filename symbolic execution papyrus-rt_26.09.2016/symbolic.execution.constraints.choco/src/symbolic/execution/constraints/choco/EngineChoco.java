package symbolic.execution.constraints.choco;

import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.False;
import symbolic.execution.constraints.manager.LogicalConstraint;
import symbolic.execution.constraints.manager.RelationalConstraint;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.True;
import symbolic.execution.constraints.manager.UnaryExpression;
import symbolic.execution.constraints.manager.factories.Engine;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;
import symbolic.execution.constraints.manager.operators.CompareOperators;
import symbolic.execution.constraints.manager.operators.LogicalOperators;

public class EngineChoco extends Engine{

	@Override
	public BinaryExpression createBinaryExpression(Expression left, ArithmeticOperators op, Expression right) {	
		return new BinaryExpressionImpl(left,op,right);
	}

	@Override
	public Constant createConstant(Class type, Object value) {			
		return new ConstantImpl(type, value);
	}

	@Override
	public LogicalConstraint createLogicalConstraint(Expression left, LogicalOperators op, Expression right) {
		if ((left == null || left instanceof Constraint) && right instanceof Constraint){
			return new LogicalConstraintImpl((Constraint)left,op,(Constraint)right);
		}
		return null;
	}

	@Override
	public SymbolicVariable createVariable(String name) {
		return new SymbolicVariableImpl(name);
	}

	@Override
	public RelationalConstraint createRelationalConstraint(Expression left,
			CompareOperators op, Expression right) {
		return new RelationalConstraintImpl(left,op,right);
	}

	@Override
	public SymbolicVariable createVariable(String name, Class type) {		
		return new SymbolicVariableImpl(name, type);
	}

	@Override
	public Solver createSolver() {		
		return new SolverImpl();
	}

	@Override
	public UnaryExpression createUnaryExpression(ArithmeticOperators op,
			Expression expr) {		
		return new UnaryExpressionImpl(op,expr);
	}

	@Override
	public False createFalse() {
		return new FalseImpl();
	}

	@Override
	public True createTrue() {
		return new TrueImpl();
	}

}
