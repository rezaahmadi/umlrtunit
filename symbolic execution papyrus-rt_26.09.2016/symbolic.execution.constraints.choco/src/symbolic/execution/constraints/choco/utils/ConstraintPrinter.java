package symbolic.execution.constraints.choco.utils;

//import choco.Choco;
import choco.kernel.model.constraints.ComponentConstraint;
import choco.kernel.model.constraints.Constraint;
import choco.kernel.model.constraints.MetaConstraint;
import choco.kernel.model.variables.ComponentVariable;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerConstantVariable;
//import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.model.variables.real.RealConstantVariable;
import choco.kernel.model.variables.real.RealVariable;


public class ConstraintPrinter {
	
	public static String print(Constraint c){
		MetaConstraint<Constraint> m = null;
		if (c instanceof MetaConstraint){
			m = (MetaConstraint<Constraint>) c;
		}
		ComponentConstraint cc = null;
		if (c instanceof ComponentConstraint){
			cc = (ComponentConstraint) c;
		}
		switch (c.getConstraintType()){
		case TRUE : return "true"; 
		case FALSE : return "false";
		case EQ : return print(cc.getVariables()[0]) + " == " + print(cc.getVariables()[1]);
		case NEQ : return print(cc.getVariables()[0]) + " != " + print(cc.getVariables()[1]);
		case GEQ : return print(cc.getVariables()[0]) + " >= " + print(cc.getVariables()[1]);
		case LEQ : return print(cc.getVariables()[0]) + " <= " + print(cc.getVariables()[1]);
		case GT : return print(cc.getVariables()[0]) + " > " + print(cc.getVariables()[1]);
		case LT : return print(cc.getVariables()[0]) + " < " + print(cc.getVariables()[1]);
		case AND :
			StringBuffer sb = new StringBuffer();
			for (Constraint inner : m.getConstraints()){
				sb.append("(");
				sb.append(print(inner));
				sb.append(")");
				sb.append(" AND ");
			}
			return sb.substring(0, sb.length() - 5);
		case NOT :
			return "NOT ("+print(m.getConstraint(0)) + ")";
		case OR :
			StringBuffer sbOr = new StringBuffer();
			for (Constraint inner : m.getConstraints()){
				sbOr.append("(");
				sbOr.append(print(inner));
				sbOr.append(")");
				sbOr.append(" OR ");
			}
			return sbOr.substring(0, sbOr.length() - 4);
		case IMPLIES :
			return "(" + print(m.getConstraint(0)) + ") => (" + print(m.getConstraint(1)) +")";
		default: return c.pretty();
		}
	}
	
	public static String print(Variable var){
		if (var == null){
			return "nil";
		}
		if (var instanceof IntegerConstantVariable){
			return ((IntegerConstantVariable)var).getValue() +"";
		}
		if (var instanceof IntegerVariable){
			return ((IntegerVariable) var).getName();
		}
		if (var instanceof RealConstantVariable){
			return ((RealConstantVariable)var).getValue() +"";
		}
		if (var instanceof RealVariable){
			return ((RealVariable) var).getName();
		}
		if (var instanceof ComponentVariable){			
			ComponentVariable expr = (ComponentVariable) var;
			switch (expr.getOperator()){
			case PLUS : 
				return "(" + print(expr.getVariables()[0]) + " + " + print(expr.getVariables()[1]) + ")"; 
			case MINUS : 
				return "(" + print(expr.getVariables()[0]) + " - " + print(expr.getVariables()[1]) + ")";
			case MULT :
				return "(" + print(expr.getVariables()[0]) + " * " + print(expr.getVariables()[1]) + ")"; 
			case DIV :
				return "(" + print(expr.getVariables()[0]) + " / " + print(expr.getVariables()[1]) + ")";
			case MOD :
				return "(" + print(expr.getVariables()[0]) + " mod " + print(expr.getVariables()[1]) + ")";
			case POWER :
				return "(" + print(expr.getVariables()[0]) + " ^ " + print(expr.getVariables()[1]) + ")";
			default:
				return var.pretty();
			}
		}
		
		return var.pretty();
			
	}
	
}
