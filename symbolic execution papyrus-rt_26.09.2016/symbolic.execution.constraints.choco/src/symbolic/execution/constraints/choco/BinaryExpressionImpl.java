package symbolic.execution.constraints.choco;

import choco.Choco;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
import choco.kernel.model.variables.real.RealExpressionVariable;
import symbolic.execution.constraints.manager.BinaryExpression;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicValuation;
//import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.operators.ArithmeticOperators;

public class BinaryExpressionImpl extends BinaryExpression implements ExpressionChoco{
	private Variable chocoExpression;
	
	
	private Variable chocoVarLeft;
	private Variable chocoVarRight;
	
	public BinaryExpressionImpl(Expression left, ArithmeticOperators op, Expression right){
		super(left,op,right);
		
		if (left instanceof ExpressionChoco && right instanceof ExpressionChoco){
			chocoVarLeft = ((ExpressionChoco)left).getChocoVariable();
			chocoVarRight = ((ExpressionChoco)right).getChocoVariable();
			
			createChocoVariable(op);
		}
				
	}


	private void createChocoVariable(ArithmeticOperators op) {		
						
		if (chocoVarLeft instanceof RealExpressionVariable && 
				chocoVarRight instanceof RealExpressionVariable){
			chocoExpression = createRealExpression((RealExpressionVariable)chocoVarLeft,op,
				(RealExpressionVariable)chocoVarRight); 
		}			
		if (chocoVarLeft instanceof IntegerExpressionVariable && 
				chocoVarRight instanceof IntegerExpressionVariable){
			chocoExpression = createIntegerExpression((IntegerExpressionVariable)chocoVarLeft,op,
				(IntegerExpressionVariable)chocoVarRight); 
		}		
	}

	private Variable createIntegerExpression(IntegerExpressionVariable varLeft,
			ArithmeticOperators op, IntegerExpressionVariable varRight) {
		switch (op){
		case plus: return Choco.plus(varLeft, varRight);
		case minus: return Choco.minus(varLeft, varRight);
		case div: return Choco.div(varLeft, varRight);
		case mod: return Choco.mod(varLeft, varRight);
		case mult: return Choco.mult(varLeft, varRight);
		}
		return null;
	}


	private Variable createRealExpression(RealExpressionVariable varLeft,
			ArithmeticOperators op, RealExpressionVariable varRight) {

		switch (op){
		case plus: return Choco.plus(varLeft, varRight);
		case minus: return Choco.minus(varLeft, varRight);
		case mult: return Choco.mult(varLeft, varRight);		
		}
		
		return null;
	}


	@Override
	public Variable getChocoVariable() {		
		return chocoExpression;
	}
	
	@Override
	public Expression replaceVariables(SymbolicValuation valuation) {
		Expression replaced = super.replaceVariables(valuation);
		
		if (replaced instanceof BinaryExpression){
			Expression left = ((BinaryExpression)replaced).getLeft();
			Expression right = ((BinaryExpression)replaced).getRight();
			ArithmeticOperators op = ((BinaryExpression)replaced).getOp();
			
			return new BinaryExpressionImpl(left, op, right);
			
		}
		
		if (replaced instanceof Constant){
			Constant constant = (Constant)replaced;
			return new ConstantImpl(constant.getType(), constant.getValue());
		}	
		
		return null;
	}

}
