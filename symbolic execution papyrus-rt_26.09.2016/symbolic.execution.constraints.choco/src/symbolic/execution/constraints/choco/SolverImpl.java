package symbolic.execution.constraints.choco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import choco.Choco;
//import choco.ChocoContrib;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.variables.Variable;
import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.model.variables.real.RealVariable;
import choco.kernel.solver.ContradictionException;

//import symbolic.execution.constraints.choco.utils.ConstraintPrinter;
import symbolic.execution.constraints.manager.Constant;
import symbolic.execution.constraints.manager.Constraint;
import symbolic.execution.constraints.manager.Solver;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import symbolic.execution.constraints.manager.operators.LogicalOperators;
//import test.TestSolvable;

public class SolverImpl extends Solver{

	@Override
	public boolean isSolvable(List<Constraint> constraints) {
		
		//System.out.println("JDA: SolverImpl.solve: Constraints in model " + constraints);
	
		Model model = new CPModel();		
		
		for (Constraint cst : constraints){
			if (cst instanceof ConstraintChoco){
				choco.kernel.model.constraints.Constraint c = ((ConstraintChoco)cst).getChocoConstraint();
				if (c != null)
					try {
						model.addConstraint(c);
					} catch (Exception e) {
						System.out.println(e.getStackTrace());	
					}
			}
		}
		choco.kernel.solver.Solver s = new CPSolver();
		
		s.read(model);
		boolean solvable = true;
		
		try {
			s.propagate();
			//JDA: Added the following.
			//TODO: JDA: Sometimes the ContradictionException does not happen when expected. Why?
			solvable = s.solve();
		} catch (ContradictionException e) { 
			//e.printStackTrace();
			solvable = false;
		}
		
		//System.out.println("JDA: SolverImpl.solve: solvable = " + solvable);
		return solvable;
	}
	
	@Override
	public boolean isSolvable(List<Constraint> constraints, Constraint condition, boolean isTrue) {
		
		List<Constraint> constraintsLocal = new ArrayList<Constraint>(constraints);
		if (isTrue){
			try {
				constraintsLocal.add(condition);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
				e.printStackTrace();
				throw e;
			}
		} else {
			Constraint notCondition = SymbolicFactory.createLogicalConstraint(null, LogicalOperators.not, condition);			
			constraintsLocal.add(notCondition);
						
			
		}
				
		return isSolvable(constraintsLocal);
	}

	@Override
	public boolean isSolvable(List<Constraint> constraints,
			Set<Constraint> constraintsCheck, boolean checkTrue) {
		
		List<choco.kernel.model.constraints.Constraint> chocoCsts = new ArrayList<choco.kernel.model.constraints.Constraint>();
		for (Constraint cst : constraints) {
			if (cst instanceof Constraint)
				chocoCsts.add(((ConstraintChoco) cst).getChocoConstraint());
		}

		Set<choco.kernel.model.constraints.Constraint> chocoCheck = new HashSet<choco.kernel.model.constraints.Constraint>();
		Iterator<Constraint> iter = constraintsCheck.iterator();
		while (iter.hasNext()) {
			Constraint cst = iter.next();
			if (cst instanceof ConstraintChoco) {
				chocoCheck.add(((ConstraintChoco) cst).getChocoConstraint());
			}
		}

		Model m = new CPModel();
		
		//add all constraints
		for (choco.kernel.model.constraints.Constraint cst : chocoCsts){
			if (cst != null)
				m.addConstraint(cst);
		}
		
		//create check constraint		                                                                                                 	
		if (checkTrue){
			for (choco.kernel.model.constraints.Constraint cst : chocoCheck){
				if (cst !=null)
					try {
						m.addConstraint(cst);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println(e.getMessage());
						e.printStackTrace();
						throw e;
					}
			}
		} else {
			if (!chocoCheck.isEmpty()){
				if (chocoCheck.size() == 1) {
					choco.kernel.model.constraints.Constraint checkCst = chocoCheck.iterator().next();
					if (checkCst != null) {
						choco.kernel.model.constraints.Constraint check = checkCst;
						m.addConstraint(Choco.not(check));
					}				
				} else {
					choco.kernel.model.constraints.Constraint[] csts = new choco.kernel.model.constraints.Constraint[chocoCheck.size()];
					int i = 0;
					for (choco.kernel.model.constraints.Constraint cst : chocoCheck){						
							csts[i] = cst;
							i++;						
					}
					choco.kernel.model.constraints.Constraint check = Choco.and(csts);
					m.addConstraint(Choco.not(check));
				}
			}
			else {
				return false;
			}
		}
		
		//solve
		choco.kernel.solver.Solver s = new CPSolver();
		s.read(m);
		
		/*System.out.println("Constraints !!!");		
		Iterator<choco.kernel.model.constraints.Constraint> iter = m.getConstraintIterator();
		while (iter.hasNext()){
			choco.kernel.model.constraints.Constraint cst = iter.next();
			System.out.println(cst.pretty());
		}
		
		System.out.println("Variables " );
		Iterator<IntegerVariable> ite = m.getIntVarIterator();
		while (ite.hasNext()){
			System.out.println(ite.next().pretty());
		}
		 */
		boolean solvable = true;
		;
		try {
			s.propagate();
		} catch (ContradictionException e) {
			solvable = false;
		}

		return solvable;
		
	}

	@Override
	public Map<SymbolicVariable, Constant> solve(List<Constraint> constraints, List<SymbolicVariable> variables) {
		Model m = new CPModel();
		for(SymbolicVariable sv : variables){
			if (sv instanceof ExpressionChoco){
				Variable var = ((ExpressionChoco)sv).getChocoVariable();
				m.addVariable(var);
			}
		}
		for(Constraint cst : constraints){
			if (cst instanceof ConstraintChoco){
				choco.kernel.model.constraints.Constraint c = ((ConstraintChoco)cst).getChocoConstraint();
				m.addConstraint(c);
			}
		}
		
		choco.kernel.solver.Solver s = new CPSolver();
		s.read(m);
		s.solve();
		
		
		Map<SymbolicVariable, Constant> solution = new HashMap<SymbolicVariable, Constant>();

		for (SymbolicVariable sv : variables) {
			if (sv instanceof ExpressionChoco) {
				Variable var = ((ExpressionChoco) sv).getChocoVariable();

				if (var instanceof IntegerVariable) {
					int val = s.getVar(((IntegerVariable) var)).getVal();

					Constant cst = new ConstantImpl(sv.getType(), new Integer(val));
					solution.put(sv, cst);
				} else if (var instanceof RealVariable) {
					double val = s.getVar(((RealVariable) var)).getSup()
							+ (s.getVar(((RealVariable) var)).getSup() - s.getVar(((RealVariable) var)).getInf()) / 2;

					Constant cst = new ConstantImpl(sv.getType(), new Double(val));
					solution.put(sv, cst);
				}
			}
		}
		
		return solution;
	}

}
