package symbolic.execution.constraints.choco;

import symbolic.execution.constraints.manager.Expression;
import symbolic.execution.constraints.manager.SymbolicValuation;
import symbolic.execution.constraints.manager.SymbolicVariable;
import choco.Choco;
import choco.kernel.model.variables.Variable;

public class SymbolicVariableImpl extends SymbolicVariable implements ExpressionChoco{

	private Variable chocoVariable;
	
	public SymbolicVariableImpl(String name) {
		super(name);
	}

	public SymbolicVariableImpl(String name, Class type){
		super(name,type);
		setType(type);
	}
	
	@Override
	public void setType(Class<?> type) {	
		super.setType(type);
		if (type.equals(Integer.class)){
			chocoVariable = Choco.makeIntVar(name, -1000, 1000);
		} else if (type.equals(Long.class)){
			chocoVariable = Choco.makeIntVar(name, -1000, 1000);
		} else if (type.equals(Short.class)){
			chocoVariable = Choco.makeIntVar(name, -1000, 1000);
		} else if (type.equals(Float.class)){
			chocoVariable = Choco.makeRealVar(name, -1000, 1000);
		} else if (type.equals(Double.class)){
			chocoVariable = Choco.makeRealVar(name, -1000, 1000);
		} else if (type.equals(Boolean.class)){
			chocoVariable = Choco.makeIntVar(name,0,1);
		}
	}

	@Override
	public Variable getChocoVariable() {		
		return chocoVariable;
	}

}
