package symbolic.execution.constraints.choco;

import choco.Choco;
import choco.kernel.model.constraints.Constraint;
import symbolic.execution.constraints.manager.True;

public class TrueImpl extends True implements ConstraintChoco{
	private Constraint chocoConstraint = Choco.TRUE;

	@Override
	public Constraint getChocoConstraint() {
		return chocoConstraint;
	}

}
