package symbolic.execution.constraints.choco;

import choco.Choco;
import choco.kernel.model.constraints.Constraint;
import symbolic.execution.constraints.manager.False;

public class FalseImpl extends False implements ConstraintChoco{
	private Constraint chcocConstraint = Choco.FALSE;

	@Override
	public Constraint getChocoConstraint() {
		return chcocConstraint;
	}

}
