package test;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
//import java.util.Map;
import java.util.Set;

//import symbolic.execution.constraints.choco.ConstantImpl;
//import symbolic.execution.constraints.choco.ConstraintChoco;
//import symbolic.execution.constraints.choco.ExpressionChoco;
//import symbolic.execution.constraints.manager.Constant;
//import symbolic.execution.constraints.manager.SymbolicVariable;
//import symbolic.execution.constraints.manager.factories.SymbolicFactory;
import choco.Choco;
//import choco.IPretty;
import choco.cp.model.CPModel;
import choco.cp.solver.CPSolver;
import choco.kernel.model.Model;
import choco.kernel.model.constraints.Constraint;
//import choco.kernel.model.variables.Variable;
//import choco.kernel.model.variables.VariableType;
import choco.kernel.model.variables.integer.IntegerExpressionVariable;
//import choco.kernel.model.variables.integer.IntegerVariable;
import choco.kernel.model.variables.integer.IntegerVariable;
//import choco.kernel.model.variables.real.RealVariable;
import choco.kernel.solver.ContradictionException;

public class TestSolvable {


	static IntegerVariable v1,v2,v3; 
	
public static void main(String[] args) {
	Set<Constraint> csts = new HashSet<Constraint>();
	
	//Constraint t = Choco.TRUE;
	v1 = new IntegerVariable("keypressvar3", -1000, 1000);
	v2 = new IntegerVariable("keypressvar6", -1000, 1000);
	v3 = new IntegerVariable("keypressvar6", -1000, 1000);
	
	/*Constraint c1 = Choco.eq(7, Choco.makeIntVar("keypressvar3", -1000, 1000));
	Constraint c2 = Choco.eq(7, Choco.makeIntVar("keypressvar6", -1000, 1000));
	Constraint c3 = Choco.eq(7, Choco.makeIntVar("keypressvar9", -1000, 1000));
	Constraint a1 = Choco.and(c2,c3);
	Constraint a2 = Choco.and(c1,a1);
	Constraint a3 = Choco.and(t,a2);*/
	
	IntegerExpressionVariable expr = Choco.div(v1, 5);
	
	Constraint c1 = Choco.gt(v1,0);
	Constraint c2 = Choco.gt(expr,0);
	Constraint c3 = Choco.not(c2);
	
	
	
	csts.add(c1);
	csts.add(c3);
	
	
	//System.out.println(a3.pretty());
	
	boolean is = isSolvable(new ArrayList<Constraint>(),csts,true);
	
	System.out.println(is);
	
	/*is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,true);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,true);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,true);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,true);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,false);
	System.out.println(is);
	
	is = isSolvable(new ArrayList<Constraint>(),csts,true);
	System.out.println(is); */
}

public static boolean isSolvable(List<Constraint> constraints) {
	
	//System.out.println("Constraints in model " + constraints);
	
	Model model = new CPModel();		
	
	Constraint[] arr = new Constraint[1];
	arr[0] = constraints.get(0);
	
	Constraint cst = Choco.and(arr);
	
	model.addConstraint(cst);
	/*
	for (Constraint cst : constraints){
		if (cst instanceof ConstraintChoco){
			choco.kernel.model.constraints.Constraint c = ((ConstraintChoco)cst).getChocoConstraint();				
			model.addConstraint(c);
		}
	}*/
	choco.kernel.solver.Solver s = new CPSolver();
	
	s.read(model);
	boolean solvable = true;
	
	//try {
		s.solve();
	//} //catch (ContradictionException e) { 
		//e.printStackTrace();
	//	solvable = false;
//	}
	
	
	//s.getSetVar(0).getVarIndex(arg0)
	return solvable;
}

public static boolean isSolvable(List<Constraint> constraints,
		Set<Constraint> constraintsCheck, boolean checkTrue) {
	Model m = new CPModel();
	
	//add all constraints
	for (Constraint cst : constraints){
		if (cst != null)
			m.addConstraint(cst);
	}
	
	//create check constraint
	                                                                                                 		
	if (checkTrue){
		for (Constraint cst : constraintsCheck){
			if (cst !=null)
				m.addConstraint(cst);
			
		}
	} else {
		if (!constraintsCheck.isEmpty()){
			
		
		if (constraintsCheck.size() == 1) {
			Constraint checkCst = constraintsCheck.iterator().next();
			if (checkCst != null) {
				choco.kernel.model.constraints.Constraint check = checkCst;
				m.addConstraint(Choco.not(check));
			}
			
		} else {
			choco.kernel.model.constraints.Constraint[] csts = new choco.kernel.model.constraints.Constraint[constraintsCheck.size()];
			int i = 0;
			for (Constraint cst : constraintsCheck){
				if (cst instanceof Constraint){
					csts[i] = cst;
					i++;
				}
			}
			choco.kernel.model.constraints.Constraint check = Choco.and(csts);
			m.addConstraint(Choco.not(check));
		}
		}
		else {
			return false;
		}
	}
	
	//solve
	choco.kernel.solver.Solver s = new CPSolver();
	s.read(m);
	
	System.out.println("Constraints !!!");
	/* */
	Iterator<choco.kernel.model.constraints.Constraint> iter = m.getConstraintIterator();
	while (iter.hasNext()){
		choco.kernel.model.constraints.Constraint cst = iter.next();
		System.out.println(cst.pretty());
	}
	
	System.out.println("Variables " );
	Iterator<IntegerVariable> ite = m.getIntVarIterator();
	while (ite.hasNext()){
		System.out.println(ite.next().pretty());
	}
	
	/* */
	boolean solvable = true;;
	try {
		s.propagate();
		s.solve();
	} catch (ContradictionException e) {
		solvable = false;
	}
	
	System.out.println(s.solutionToString());
	
	return solvable;
}

}