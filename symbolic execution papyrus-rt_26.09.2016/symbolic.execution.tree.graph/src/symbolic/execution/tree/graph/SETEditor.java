package symbolic.execution.tree.graph;

//TODO: JDA: There are a number of auto generated method stubs here!
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditDomain;
//import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import symbolic.execution.tree.graph.editparts.GraphEditPart;
import symbolic.execution.tree.graph.editparts.SETEditPartFactory;
//import symbolic.execution.tree.graph.model.ContentsModel;
import symbolic.execution.tree.graph.path.selection.SelectedPath;

public class SETEditor extends EditorPart implements ISelectionListener {
	
	public static final String ID = SETEditor.class.getName();
	
	private EditDomain editDomain;
	
	private GraphicalViewer viewer;
	
	public SETEditor() {
		
	}
	
	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO: JDA: Auto-generated method stub
		
	}
	@Override
	public void doSaveAs() {
		// TODO: JDA: Auto-generated method stub
		
	}
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);	
		setPartName(input.getName());
		site.getPage().addSelectionListener(this);
		initEditDomain();
	}
	
	private void initEditDomain() {
		this.editDomain = new DefaultEditDomain(this);
		
	}

	@Override
	public boolean isDirty() {
		// TODO: JDA: Auto-generated method stub
		return false;
	}
	@Override
	public boolean isSaveAsAllowed() {
		// TODO: JDA: Auto-generated method stub
		return false;
	}
	@Override
	public void createPartControl(Composite parent) {
		createGraphViewer(parent);		
	}
	private void createGraphViewer(Composite parent) {
		ScrollingGraphicalViewer viewer = new ScrollingGraphicalViewer();
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		viewer.createControl(parent);
		viewer.getControl().setBackground(ColorConstants.white);
		viewer.setEditPartFactory(new SETEditPartFactory());
		viewer.setContents(((SETEditorInput)getEditorInput()).getGraph());		
		viewer.getContents().refresh();
		
		editDomain.addViewer(viewer);
		
		this.viewer = viewer;
	}
	@Override
	public void setFocus() {
		// TODO: JDA: Auto-generated method stub
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {		
		if (selection instanceof SelectedPath){
			if (!selection.isEmpty()){
				SelectedPath path = (SelectedPath) selection;
				if (path.getEditor() == this){
					((GraphEditPart)viewer.getContents()).toXYLayout();
					((SETEditorInput)getEditorInput()).highlight(path);
				}
			} else {
				((SETEditorInput)getEditorInput()).unhighlight();
			}
		}
		
	}

	public GraphicalViewer getGraphicalViewer() {
		return viewer;
	}

}
