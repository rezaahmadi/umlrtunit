package symbolic.execution.tree.graph.figures;

//import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
//import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
//import org.eclipse.draw2d.RectangleFigure;
//import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

public class SimpleNodeFigure extends Figure {
	private final MarginBorder border = new MarginBorder(7,7,7,7);
	private Label label;
	
	protected boolean isHighlight = false;
	protected Color highlightColor = ColorConstants.black;
	
	//private RectangleFigure rectangle;
	
	protected SimpleNodeFigure(){}
	
	public SimpleNodeFigure(String text) {
		FlowLayout layout = new FlowLayout();
		layout.setHorizontal(false);
		setLayoutManager(layout);
		//rectangle = new RectangleFigure();
		//add(rectangle);
		label = new Label(text);
		add(label);
		setBorder(border);
		setOpaque(true);
	}
	
	public Label getLabel() {
		return label;
	}
	
	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		Rectangle r = getBounds().getCopy();		
		
		if (isHighlight){
			graphics.setLineWidth(2);
			//graphics.setBackgroundColor(ColorConstants.lightGray);			
			graphics.setForegroundColor(highlightColor);
			graphics.drawRoundRectangle(new Rectangle(r.x+1,r.y+1, r.width-2,r.height-2),10,10);
		} else {
			graphics.setLineWidth(1);			
			graphics.drawRoundRectangle(new Rectangle(r.x,r.y, r.width-1,r.height-1),10,10);
		}
		
		//graphics.drawRoundRectangle(new Rectangle(r.x,r.y, r.width-1,r.height-1),10,10);
		//setConstraint(rectangle, new Rectangle(0,0,r.width,r.height));
		//setConstraint(label, new Rectangle(0,0,r.width,r.height));
		
		
		//rectangle.invalidate();
		//label.invalidate();
	}

	public void setHighlight(boolean highlight) {
		this.isHighlight = highlight;		
		
	}

	public void setHighlightColor(Color highlightColor) {
		this.highlightColor = highlightColor;
	}
	
}
