package symbolic.execution.tree.graph.figures;

//import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
//import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;



public class CompositeNodeFigure extends SimpleNodeFigure{
	
	private final MarginBorder border = new MarginBorder(3,3,3,3);
	 
	private Label label;
	private Figure bottom;
	private NodesElementsFigure contents;
	private boolean external;
	
	public CompositeNodeFigure(String text, boolean external){			
		this.external = external;
				
		label = new Label(text);
		label.setTextAlignment(PositionConstants.LEFT);
		label.setLabelAlignment(PositionConstants.LEFT);
		
		label.validate();
		//label.setLabelAlignment(PositionConstants.ALWAYS_LEFT);
		contents = new NodesElementsFigure();
		
		//bottom things
		bottom = new Figure();
		bottom.setLayoutManager(new FlowLayout());
		
		Label dummy = new Label("     ");
		bottom.add(dummy);
		bottom.add(contents);
		
		
		//layout
		ToolbarLayout layout = new ToolbarLayout();
		layout.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);
		//layout.setHorizontal(false);
		//FlowLayout layout = new FlowLayout(false);
		setLayoutManager(layout);
		
		
		add(label);
		add(bottom);
		setBorder(border);
		setOpaque(true);
		
	}
	
	public IFigure getContents() {		
		return contents;
	}
	
	
	
	
	public Dimension getPreferredSize(int wHint, int hHint) {
		this.validate();
		
		
		
		Dimension labelSize = label.getTextBounds().getSize();
		Dimension contentsSize = bottom.getSize();
		
		int width = Math.max(labelSize.width, contentsSize.width);
		int height = labelSize.height + contentsSize.height;
		
		Rectangle result = new Rectangle(0,0,width,height);
				
		result.resize(getInsets().getWidth(), getInsets().getHeight());		
		return result.getSize();
	}

	/*public void setBounds(Rectangle rect) {
		
		//label bounds
		//Dimension size = label.getPreferredSize();
		//label.setSize(size);
		//label.setLocation(rect.getLocation());
		
		//contents bounds
		//rect.performTranslate(20, 0);
		//contents.setBounds(rect);
		super.setBounds(rect);	
	}*/

	protected void paintFigure(Graphics graphics) {
		label.invalidate();
		graphics.setBackgroundColor(ColorConstants.gray);
		Rectangle r = getBounds().getCopy();
		//draws rectangle for contents
		Rectangle rDrawn = new Rectangle(r.x+getInsets().getWidth(), 
				r.y + label.getSize().height + getInsets().getHeight(), 10, contents.getSize().height-getInsets().getHeight()); 
		graphics.fillRoundRectangle(rDrawn, 5,5);
		graphics.setBackgroundColor(ColorConstants.white);
		if (external){
			if (isHighlight){
				graphics.setLineWidth(2);
				//graphics.setBackgroundColor(ColorConstants.lightGray);			
				graphics.setForegroundColor(highlightColor);
				graphics.drawRoundRectangle(new Rectangle(r.x+1,r.y+1, r.width-2,r.height-2),10,10);
			} else {
				graphics.setLineWidth(1);			
				graphics.drawRoundRectangle(new Rectangle(r.x,r.y, r.width-1,r.height-1),10,10);
			}
						
		}
	}

	public Label getLabel() {
		return label;
	}

	/*public void setContents(IFigure figure) {
		this.contents = figure;
		
	}*/


}


