package symbolic.execution.tree.graph.figures;


//import javax.swing.text.StyleConstants.ColorConstants;

import org.eclipse.draw2d.ConnectionEndpointLocator;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolygonDecoration;
//import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.PolylineConnection;
//import org.eclipse.draw2d.RelativeLocator;
//import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import symbolic.execution.tree.graph.model.ConnectionTypes;

public class ConnectionFigure extends PolylineConnection{
	
	private ConnectionEndpointLocator locator;
	private Label label;
	
	public ConnectionFigure(String text, ConnectionTypes type){
		
		locator = new ConnectionEndpointLocator(this, false);			
		label = new Label(text);
		add(label, locator);
		
		locator.setVDistance(0-label.getSize().width);
		
		switch (type) {
		case Open : break;
		case Synchronized : setForegroundColor(org.eclipse.draw2d.ColorConstants.black);setLineStyle(3); break;
		case Unreceived : setForegroundColor(org.eclipse.draw2d.ColorConstants.orange); break;
		case Subsume : setForegroundColor(org.eclipse.draw2d.ColorConstants.red); break;
		}
		
		setTargetDecoration(new PolygonDecoration());
	}

	public void setHighlited(boolean isHighlighed) {
		if (isHighlighed){
			setLineWidth(3);
		} else {
			setLineWidth(1);
		}
		
	}

	public void setHighlightColor(Color highlightColor) {
		/*if (highlightColor != null){
			setForegroundColor(highlightColor);
		} else {
			setForegroundColor(org.eclipse.draw2d.ColorConstants.black);
		}*/
		
	}
	
	@Override
	public void setBounds(Rectangle rect) {		
		super.setBounds(rect);		
		
	}
	
	@Override
	public void paintFigure(Graphics graphics) {		
		//locator.setUDistance(Math.max(2,getBounds().height/4));
		super.paintFigure(graphics);
		
	}
	
	//TODO: JDA: Why is this not used locally?
	private int length(){
		double height2 = Math.pow(getBounds().height, 2);
		double width2 = Math.pow(getBounds().width, 2);
		
		return (int) Math.sqrt(height2 + width2);
	}
	
}
