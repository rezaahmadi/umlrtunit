package symbolic.execution.tree.graph.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;

public class NodeElementFigure extends Label{

	public NodeElementFigure(String text){
		super(text);
		setLabelAlignment(LEFT);
		setBorder(new MarginBorder(2, 0, 2, 0));
	}
	
	@Override
	public void paint(Graphics graphics) {		
		super.paint(graphics);		
		//invalidate();
	}

	/*private Label label;
	
	
	public NodeElementFigure(String text){
		setLayoutManager(new FlowLayout());
		label = new Label(text);
		add(label);
	}
	
	public Label getLabel() {
		return label;
	}
	
	@Override
	public void paint(Graphics graphics) {		
		super.paint(graphics);
		setConstraint(label, getBounds());
		label.invalidate();
	}*/
	
}
