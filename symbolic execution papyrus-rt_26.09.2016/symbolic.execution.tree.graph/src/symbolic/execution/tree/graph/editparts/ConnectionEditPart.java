package symbolic.execution.tree.graph.editparts;

import java.util.Observable;
import java.util.Observer;

import org.eclipse.draw2d.IFigure;
//import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
//import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;

import symbolic.execution.tree.graph.figures.ConnectionFigure;
import symbolic.execution.tree.graph.model.Connection;
//import symbolic.execution.tree.graph.policies.EditLayoutPolicy;

public class ConnectionEditPart extends AbstractConnectionEditPart 
	implements Observer{

	public ConnectionEditPart(Connection connection) {
		setModel(connection);
	}
	
	@Override
	public void activate() {
		if(!isActive()){
			((Connection)getModel()).addObserver(this);
		}
		super.activate();
	}
	
	@Override
	public void deactivate() {
		if (isActive()){
			((Connection)getModel()).addObserver(this);
		}
		super.deactivate();
	}
	
	@Override
	protected IFigure createFigure() {	
		Connection model = (Connection) getModel();
		return new ConnectionFigure(model.getText(), model.getType());
	}
	
	@Override
	protected void createEditPolicies() {
	}

	@Override
	public void update(Observable observable, Object data) {
		refreshVisuals();	
		
	}
	
	@Override
	protected void refreshVisuals() {		
		super.refreshVisuals();
		
		Connection conn = (Connection) getModel();
		ConnectionFigure figure = (ConnectionFigure) getFigure();
		figure.setHighlited(conn.getHighlight());
		figure.setHighlightColor(conn.getHighlightColor());		
		
		Point location = figure.getLocation();
		GraphicalEditPart parent = (GraphicalEditPart)getParent();		
		Rectangle r = new Rectangle(location.x, location.y,-1,-1);
		if (parent != null){
			parent.setLayoutConstraint(this, figure, r);
		}
	}

}
