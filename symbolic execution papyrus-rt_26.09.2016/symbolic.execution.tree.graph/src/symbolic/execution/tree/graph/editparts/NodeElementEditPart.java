package symbolic.execution.tree.graph.editparts;

//import java.util.List;
//import java.util.Observable;
//import java.util.Observer;

import org.eclipse.draw2d.IFigure;
//import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
//import org.eclipse.gef.editparts.AbstractTreeEditPart;

//import symbolic.execution.tree.graph.figures.CompositeNodeFigure;
import symbolic.execution.tree.graph.figures.NodeElementFigure;
//import symbolic.execution.tree.graph.figures.SimpleNodeFigure;
import symbolic.execution.tree.graph.model.SimpleNode;

public class NodeElementEditPart extends AbstractGraphicalEditPart {
	
	public NodeElementEditPart(SimpleNode model) {		
		setModel(model);
	}

	@Override
	protected IFigure createFigure() {
		SimpleNode node = (SimpleNode) getModel();
		return new NodeElementFigure(node.getName());
	}

	@Override
	protected void createEditPolicies() {
		// TODO: JDA: Auto-generated method stub
		
	}
	
	@Override
	protected void refreshVisuals() {		
		NodeElementFigure figure = (NodeElementFigure)getFigure();
		Point location = figure.getLocation();
		GraphicalEditPart parent = (GraphicalEditPart)getParent();
		Rectangle r = new Rectangle(location.x, location.y,-1,-1);
		
		parent.setLayoutConstraint(this, figure, r);
	}

}
