package symbolic.execution.tree.graph.editparts;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import symbolic.execution.tree.graph.figures.SimpleNodeFigure;
import symbolic.execution.tree.graph.model.Connection;
import symbolic.execution.tree.graph.model.SimpleNode;
import symbolic.execution.tree.graph.policies.EditLayoutPolicy;

public class SimpleNodeEditPart extends AbstractGraphicalEditPart 
	implements Observer{

	public SimpleNodeEditPart(SimpleNode node){
		setModel(node);
	}
	
	@Override
	protected IFigure createFigure() {
		return new SimpleNodeFigure(((SimpleNode)getModel()).getName());
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, new EditLayoutPolicy((GraphEditPart) getParent()));
		
	}

	@Override
	public void activate() {
		if (!isActive()){
			((SimpleNode)getModel()).addObserver(this);
		}
		super.activate();
	}
	
	@Override
	public void deactivate() {
		if (isActive()){
			((SimpleNode)getModel()).addObserver(this);
		}
		super.deactivate();
	}
	
	
	
	@Override
	protected void refreshVisuals() {		
		SimpleNodeFigure figure = (SimpleNodeFigure)getFigure();		
		
		Point location = figure.getLocation();
		GraphicalEditPart parent = (GraphicalEditPart)getParent();
		Rectangle r = new Rectangle(location.x, location.y,-1,-1);
		
		SimpleNode model = (SimpleNode)getModel();
		figure.setHighlight(model.getHighlight());
		figure.setHighlightColor(model.getHighlightColor());
		
		parent.setLayoutConstraint(this, figure, r);
	}

	@Override
	public List<Connection> getModelSourceConnections() {		
		return ((SimpleNode)getModel()).getSourceConnections();
	}
	
	@Override
	public List<Connection> getModelTargetConnections() {		
		return ((SimpleNode)getModel()).getTargetConnections();
	}

	@Override
	public void update(Observable observable, Object data) {
		refreshVisuals();
		refreshSourceConnections();
		refreshTargetConnections();
		
	}
}
