package symbolic.execution.tree.graph.editparts;

import java.util.List;

//import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.FreeformLayer;
//import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import symbolic.execution.tree.graph.layout.DelegatingLayoutManager;
//import symbolic.execution.tree.graph.layout.GraphLayoutManager;
import symbolic.execution.tree.graph.model.Graph;
import symbolic.execution.tree.graph.policies.XYLayoutPolicy;

public class GraphEditPart extends AbstractGraphicalEditPart{

	private boolean isManualLayout;
	
	public GraphEditPart(Graph model) {
		setModel(model);
	}
	
	@Override
	protected IFigure createFigure() {
		FreeformLayer layer = new FreeformLayer();
		layer.setLayoutManager(new DelegatingLayoutManager(this));
		//layer.setLayoutManager(new GraphLayoutManager(this));
		//layer.setLayoutManager(new FlowLayout());
		layer.setBorder(new LineBorder(1));
		return layer;		
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.LAYOUT_ROLE, new XYLayoutPolicy(this));
		
	}
	
	@Override
	protected List getModelChildren() {		
		
		return ((Graph)getModel()).getNodes();
	}
	
	public void toXYLayout() {
		if (!isManualLayout){
			isManualLayout = true;
			((DelegatingLayoutManager)getFigure().getLayoutManager()).switchLayout();
		}
	}

	public void toGraphLayout() {
		if (isManualLayout){
			isManualLayout = false;
			((DelegatingLayoutManager)getFigure().getLayoutManager()).switchLayout();
		}
		
	}

}
