package symbolic.execution.tree.graph.editparts;

import java.util.List;
import java.util.Observable;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
//import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
//import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import symbolic.execution.tree.graph.figures.CompositeNodeFigure;
//import symbolic.execution.tree.graph.figures.SimpleNodeFigure;
import symbolic.execution.tree.graph.model.CompositeNode;
//import symbolic.execution.tree.graph.model.SimpleNode;
import symbolic.execution.tree.graph.policies.EditLayoutPolicy;

public class CompositeNodeEditPart extends SimpleNodeEditPart{

	public CompositeNodeEditPart(CompositeNode node) {
		super(node);
		setModel(node);
	}	
	
	@Override
	public void activate() {
		if (!isActive()){
			((CompositeNode)getModel()).addObserver(this);
			super.activate();
		}
		
	}
	
	@Override
	public void deactivate() {
		if (isActive()){
			((CompositeNode)getModel()).addObserver(this);
		}
		super.deactivate();
	}
	
	@Override
	protected IFigure createFigure() {		
		CompositeNode node = (CompositeNode)getModel();
		return new CompositeNodeFigure(node.getName(), node.isExternal());
	}

	@Override
	protected void createEditPolicies() {
		if (getParent() instanceof GraphEditPart){
			installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, new EditLayoutPolicy((GraphEditPart) getParent()));
		}
		
	}
	
	@Override
	protected List getModelChildren() {		
		return ((CompositeNode)getModel()).getNodeChildren();
	}

	@Override
	protected void refreshVisuals() {		
		CompositeNodeFigure figure = (CompositeNodeFigure)getFigure();
		CompositeNode model = (CompositeNode) getModel();
		
		figure.setHighlight(model.getHighlight());
		figure.setHighlightColor(model.getHighlightColor());
		
		Point location = figure.getLocation();
		GraphicalEditPart parent = (GraphicalEditPart)getParent();
		Rectangle r = new Rectangle(location.x, location.y,-1,-1);
				
		
		
		parent.setLayoutConstraint(this, figure, r);		
	}
	
	@Override
	public IFigure getContentPane() {		
		CompositeNodeFigure figure = (CompositeNodeFigure)getFigure();
		return figure.getContents();
	}

	@Override
	public void update(Observable observable, Object data) {		
		refreshVisuals();
	}
	
}
