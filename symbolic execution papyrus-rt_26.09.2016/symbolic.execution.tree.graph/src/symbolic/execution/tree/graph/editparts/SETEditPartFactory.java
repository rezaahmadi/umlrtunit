package symbolic.execution.tree.graph.editparts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import symbolic.execution.tree.graph.model.CompositeNode;
import symbolic.execution.tree.graph.model.Connection;
import symbolic.execution.tree.graph.model.Graph;
import symbolic.execution.tree.graph.model.SimpleNode;

public class SETEditPartFactory implements EditPartFactory{

	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof Graph){
			return new GraphEditPart((Graph)model);			
		}
		if (model instanceof CompositeNode){
			return new CompositeNodeEditPart((CompositeNode)model);
		}		
		if (model instanceof SimpleNode){
			if (context instanceof CompositeNodeEditPart)
				return new NodeElementEditPart((SimpleNode)model);
			else
				return new SimpleNodeEditPart((SimpleNode)model);
				
		}
		if (model instanceof Connection){
			return new ConnectionEditPart((Connection)model);
		}
		return null;
	}

	
}
