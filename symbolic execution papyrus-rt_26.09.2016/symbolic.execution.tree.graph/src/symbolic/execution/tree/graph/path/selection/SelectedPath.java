package symbolic.execution.tree.graph.path.selection;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;

import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;

public class SelectedPath implements ISelection {
	private SymbolicPath selected;
	
	private boolean isEmpty;
	
	private IEditorPart editor;
	
	@Override
	public boolean isEmpty() {
		return isEmpty;
	}
	
	public void setSelected(SymbolicPath selection){
		if (selection !=null){
			this.selected = selection;
			isEmpty = (false);
		}
	}
		
	public void setEditor(IEditorPart editor){
		this.editor = editor;
	}
	
	@Override
	public String toString() {		
		return selected.toString();
	}

	public SymbolicPath getPath() {
		return selected;
	}
	
	public IEditorPart getEditor() {
		return editor;
	}
}
