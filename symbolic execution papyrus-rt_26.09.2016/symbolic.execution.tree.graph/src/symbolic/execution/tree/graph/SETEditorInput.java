package symbolic.execution.tree.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SubsumptionArc;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.tree.graph.model.CompositeNode;
import symbolic.execution.tree.graph.model.Connection;
import symbolic.execution.tree.graph.model.ConnectionTypes;
import symbolic.execution.tree.graph.model.Element;
import symbolic.execution.tree.graph.model.Graph;
import symbolic.execution.tree.graph.model.SimpleNode;
import symbolic.execution.tree.graph.path.selection.SelectedPath;
import symbolic.execution.tree.synch.SynchronizationAction;
import symbolic.execution.tree.synch.UnreceivedAction;
import symbolic.execution.tree.synch.model.NodeSymbolicSynch;
import symbolic.execution.tree.synch.model.SymbolicExecutionTreeSynch;
import symbolic.execution.tree.synch.model.SymbolicStateSynch;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;

//TODO: JDA: There are a number of auto generated method stubs here!
public class SETEditorInput implements IEditorInput{

	private SymbolicExecutionTree tree;
	
	//JDA: begin change
	private int maxDisplayedNodes = ExecutionOptions.getMaxDisplayNodes();
	private boolean messageDisplayed = false;
	
	private Graph graph;
	
	private Map<NodeSymbolic,SimpleNode> nodesToModelNodes = new HashMap<NodeSymbolic, SimpleNode>();
	private Map<SymbolicTransition, Connection> transToConn = new HashMap<SymbolicTransition, Connection>();
	
	private List<Element> highlighted = new ArrayList<Element>();
	private List<Connection> subsumptionConnections = new ArrayList<Connection>();
	
	private SelectedPath selectedPath;
	
	public SETEditorInput(SymbolicExecutionTree tree){
		this.tree = tree;
		this.graph = createGraph(tree);
	}
	
	public Graph getGraph() {
		return graph;
	}	
	
	private Graph createGraph(SymbolicExecutionTree tree) {	
		//JDA: Debug
		//if (tree instanceof SymbolicExecutionTreeSynch) {
		//	System.out.println("JDA: createGraph: tree: [" + tree.toString() + "].");
		//}
		Graph graph = new Graph();		
		
		//create root
		NodeSymbolic rootNode = tree.getRoot();
		addNode(graph,rootNode);
		
		//process root		
		exploreNode(graph,tree.getRoot());		
		return graph;
	}

	private void addNode(Graph graph, NodeSymbolic node){
		if (node instanceof NodeSymbolicSynch){
			NodeSymbolicSynch nodeSynch = (NodeSymbolicSynch) node;
			//JDA: This is the one that populates the data presented in a so-called 'synch' node.
			CompositeNode compNode = new CompositeNode(nodeSynch.printInLines(node), true);
			nodesToModelNodes.put(nodeSynch, compNode);
			graph.addNode(compNode);
			
			SymbolicStateSynch stateSynch = nodeSynch.getContents();
			for (String part : stateSynch.getParts().keySet()){
				exploreState(graph,compNode, stateSynch.getParts().get(part), part, node);
			}	
			
		} else {
			//JDA: This is the one that populates the data presented in a simple node.
			SimpleNode modelNode = new SimpleNode(node.printInLines());
			graph.addNode(modelNode);
			nodesToModelNodes.put(node, modelNode);
		}
	}
	
	//TODO: JDA: Document the following new function
	private void exploreState(Graph graph, CompositeNode compNode,
			SymbolicState state, String partName, NodeSymbolic node) {
		if (state instanceof SymbolicStateSynch){
			SymbolicStateSynch stateSynch = (SymbolicStateSynch) state;			
			CompositeNode stateNode = new CompositeNode(partName + ":" + stateSynch.toString(), false);
			compNode.addChild(stateNode);
			for (String part : stateSynch.getParts().keySet()){
				SymbolicState childState = stateSynch.getParts().get(part);				
				exploreState(graph,stateNode,childState, part);
			}
		} else {
			//JDA: added the master param, 'node'.
			SimpleNode stateNode = new SimpleNode(partName+ ":" + state.toString(node));
			compNode.addChild(stateNode);
		}
		
	}

	private void exploreState(Graph graph, CompositeNode compNode, SymbolicState state, String partName) {		
		if (state instanceof SymbolicStateSynch){
			SymbolicStateSynch stateSynch = (SymbolicStateSynch) state;			
			CompositeNode stateNode = new CompositeNode(partName + ":" + stateSynch.toString(), false);
			compNode.addChild(stateNode);
			for (String part : stateSynch.getParts().keySet()){
				SymbolicState childState = stateSynch.getParts().get(part);				
				exploreState(graph,stateNode,childState, part);
			}
		} else {
			SimpleNode stateNode = new SimpleNode(partName+ ":" + state.toString());
			compNode.addChild(stateNode);
		}
		
	}

	private void exploreNode(Graph graph, NodeSymbolic source) {
		//JDA: changed the following line:
		//if (graph.getNodes().size()> 300)
		if (graph.getNodes().size()> maxDisplayedNodes){
			//JDA: added the following 'if'
			if (! messageDisplayed) {
				JOptionPane.showMessageDialog(null, "Maximum number of displayed options (" + maxDisplayedNodes +
						") exceeded.", "Node Limit Exceeded"
						, JOptionPane.INFORMATION_MESSAGE);
				messageDisplayed = true;
			}
			return;
		}
		for (Arc<SymbolicTransition, SymbolicState> arc : source.getChildren()){
						
			//add target
			NodeSymbolic target = (NodeSymbolic) arc.getTarget();
			addNode(graph, target);
			
			//add transition
			addTransition(graph,source,target,arc.getContents());
			
			//process target
			exploreNode(graph, target);
		}
		
		
	}

	private void addTransition(Graph graph, NodeSymbolic source, NodeSymbolic target,
			SymbolicTransition transition) {
		SimpleNode modelTarget = nodesToModelNodes.get(target);
		SimpleNode modelSource = nodesToModelNodes.get(source);
	
		Connection conn = null;
		if (transition.getInputExecution() instanceof SynchronizationAction ){
			conn = new Connection(transition.printInLines(), ConnectionTypes.Synchronized);
		} else {
			if (transition.getInputExecution() instanceof UnreceivedAction){
				conn = new Connection(transition.printInLines(), ConnectionTypes.Unreceived);
			} else {
				conn = new Connection(transition.printInLines(), ConnectionTypes.Open);
			}
		}
		 
		conn.setSource(modelSource);
		conn.setTarget(modelTarget);
		
		transToConn.put(transition, conn);
	}

	@Override
	public boolean exists() {
		// TODO: JDA: Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO: JDA: Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {		
		return tree.getName();
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO: JDA: Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {		
		return tree.getName();
	}

	@Override
	public Object getAdapter(Class adapter) {
		// TODO: JDA: Auto-generated method stub
		return null;
	}

	public SymbolicExecutionTree getTree() {
		return tree;
	}

	public void highlight(SelectedPath path) {
		if (selectedPath !=null && selectedPath==path){
			return;
		}
		unhighlight();
		
		for (ArcSymbolic arc : path.getPath().getArcs()){
			
			if (arc instanceof SubsumptionArc){
				//find source
				SimpleNode source = nodesToModelNodes.get(arc.getSource());
				source.highlight(ColorConstants.lightGreen);
				highlighted.add(source);
			
				//find target
				SimpleNode target = nodesToModelNodes.get(arc.getTarget());
				target.highlight(ColorConstants.lightGreen);
				highlighted.add(target);
				
				//create connection - dummy
				Connection conn = new Connection("subsume", ConnectionTypes.Subsume);
				conn.setSource(source);
				conn.setTarget(target);
				conn.highlight(ColorConstants.red);
				subsumptionConnections.add(conn);
				
			} else {
							
				//find source
				SimpleNode source = nodesToModelNodes.get(arc.getSource());
				source.highlight(ColorConstants.black);
				highlighted.add(source);
			
				//find target
				SimpleNode target = nodesToModelNodes.get(arc.getTarget());
				target.highlight(ColorConstants.black);
				highlighted.add(target);
			
				//find connection
				Connection conn = transToConn.get(arc.getContents());
				conn.highlight(ColorConstants.black);
				highlighted.add(conn);
			}
		}
		
	}

	public void unhighlight() {
		try {
		for (Element e : highlighted){
			e.unhighlight();
		}
		highlighted.clear();
		
		for (Connection conn : subsumptionConnections){
			conn.unhighlight();
			conn.setSource(null);
			conn.setTarget(null);
		}
		subsumptionConnections.clear();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
