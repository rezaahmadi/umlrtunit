package symbolic.execution.tree.graph.policies;

import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;

import symbolic.execution.tree.graph.editparts.GraphEditPart;
import symbolic.execution.tree.graph.editparts.SimpleNodeEditPart;
import symbolic.execution.tree.graph.layout.DelegatingLayoutManager;

public class XYLayoutPolicy extends XYLayoutEditPolicy {

	private GraphEditPart graph;
	
	public XYLayoutPolicy(GraphEditPart graph){
		this.graph = graph;
	}
	
	
	@Override
	protected Command createChangeConstraintCommand(EditPart child,
			Object constraint) {
		NodeChangeConstraintCommand command = new NodeChangeConstraintCommand();
		command.setNewConstraint((Rectangle)constraint);		
		command.setNode((SimpleNodeEditPart)child);
		return command;
	}

	@Override
	protected Command getCreateCommand(CreateRequest request) {
		// TODO: JDA: Auto-generated method stub
		return null;
	}

	@Override
	protected XYLayout getXYLayout() {		
		return (XYLayout) ((DelegatingLayoutManager)graph.getFigure().getLayoutManager()).getLayoutManager();
	}
	
}
