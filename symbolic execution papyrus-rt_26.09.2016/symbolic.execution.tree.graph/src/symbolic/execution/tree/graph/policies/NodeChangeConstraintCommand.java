package symbolic.execution.tree.graph.policies;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.commands.Command;

import symbolic.execution.tree.graph.editparts.SimpleNodeEditPart;

public class NodeChangeConstraintCommand extends Command {
	private Rectangle oldConstraint;
	private Rectangle newConstraint;
	
	private SimpleNodeEditPart node;
	
	@Override
	public void execute() {
		
		if (oldConstraint == null){
			oldConstraint = node.getFigure().getBounds();
		}
		
		if (newConstraint.height == -1 || newConstraint.width==-1){
			node.getFigure().setBounds(new Rectangle(newConstraint.getLocation(), oldConstraint.getSize()));
		} else {
			node.getFigure().setBounds(newConstraint);
		}
	}

	public void setNewConstraint(Rectangle newConstraint) {
		this.newConstraint = newConstraint;
	}
	
	public void setNode(SimpleNodeEditPart node) {
		this.node = node;
	}
}
