package symbolic.execution.tree.graph.policies;

import org.eclipse.gef.editpolicies.SelectionEditPolicy;

import symbolic.execution.tree.graph.editparts.GraphEditPart;

public class EditLayoutPolicy extends SelectionEditPolicy{

	private GraphEditPart graph;
	
	public EditLayoutPolicy(GraphEditPart graph) {
		this.graph = graph;
	}
	
	@Override
	protected void hideSelection() {
		// TODO: JDA: Is this auto-generated method stub used?
		
	}

	@Override
	protected void showSelection() {
		graph.toXYLayout();
		
	}

}
