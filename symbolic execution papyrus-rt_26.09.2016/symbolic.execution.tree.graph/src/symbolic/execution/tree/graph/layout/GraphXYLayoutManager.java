package symbolic.execution.tree.graph.layout;

import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

public class GraphXYLayoutManager extends XYLayout{

	@Override
	public void layout(IFigure parent) {		
		super.layout(parent);
	}
	
	/*@Override
	public Object getConstraint(IFigure child) {		
		Rectangle currentBounds = child.getBounds();
		return new Rectangle(currentBounds.x, currentBounds.y, -1,-1);
	}*/

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		container.validate();
		List children = container.getChildren();
		Rectangle result = new Rectangle().setLocation(container.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++)
			result.union(((IFigure) children.get(i)).getBounds());
		result.resize(container.getInsets().getWidth(), container.getInsets().getHeight());
		return result.getSize();		
	}

	
}
