package symbolic.execution.tree.graph.layout;

import java.util.List;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;


import symbolic.execution.tree.graph.editparts.GraphEditPart;

public class GraphLayoutManager extends AbstractLayout{

	private GraphEditPart graph;
	
	public GraphLayoutManager(GraphEditPart graph) {
		this.graph = graph;
	}
	
	@Override
	public void layout(IFigure container) {		
		new DirectedGraphLayoutVisitor().layoutGraph(graph);
		
	}

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		container.validate();
		List children = container.getChildren();
		Rectangle result = new Rectangle().setLocation(container.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++)
			result.union(((IFigure) children.get(i)).getBounds());
		result.resize(container.getInsets().getWidth(), container.getInsets().getHeight());
		return result.getSize();		
	}

}
