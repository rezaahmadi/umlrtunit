package symbolic.execution.tree.graph.layout;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutManager;
//import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import symbolic.execution.tree.graph.editparts.GraphEditPart;

public class DelegatingLayoutManager extends AbstractLayout {

	private LayoutManager currentLayout;
	private GraphLayoutManager graphLayout;
	private GraphXYLayoutManager xyLayout;
	
	private GraphEditPart graph;
	
	public DelegatingLayoutManager(GraphEditPart graph) {
		this.graphLayout = new GraphLayoutManager(graph);
		this.xyLayout = new GraphXYLayoutManager();
		
		this.currentLayout = graphLayout;
		this.graph = graph;
	}
	
	@Override
	public void layout(IFigure container) {
		currentLayout.layout(container);		
	}

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint,
			int hHint) {
		// TODO: JDA: Auto-generated method stub
		return null;
	}

	@Override
	public Object getConstraint(IFigure child) {
		return currentLayout.getConstraint(child);
	}
	
	@Override
	public Dimension getMinimumSize(IFigure container, int wHint, int hHint) {
		return currentLayout.getMinimumSize(container, wHint, hHint);
	}
	
	@Override
	public Dimension getPreferredSize(IFigure container, int wHint, int hHint) {		
		return currentLayout.getPreferredSize(container, wHint, hHint);
	}
	
	@Override
	public void invalidate() {		
		currentLayout.invalidate();
	}
	
	@Override
	public void setConstraint(IFigure child, Object constraint) {		
		currentLayout.setConstraint(child, constraint);
	}
	
	@Override
	public void remove(IFigure child) {		
		currentLayout.remove(child);
	}
	
	public void setXYLayoutConstraint(IFigure child, Rectangle constraint)
	{
		xyLayout.setConstraint(child, constraint);
	}

	public void setLayoutManager(IFigure container, LayoutManager layoutManager){
		container.setLayoutManager(layoutManager);
	}
	
	public LayoutManager getLayoutManager(){
		return currentLayout;
	}
	
	public void switchLayout() {
		if (currentLayout == graphLayout){
			currentLayout = xyLayout;
		} else {
			currentLayout = graphLayout;
		}
				
		
		//layout(graph.getFigure());		
	}

}
