package symbolic.execution.tree.graph.layout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.AbsoluteBendpoint;
//import org.eclipse.draw2d.ArrowButton;
//import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.draw2d.graph.NodeList;
//import org.eclipse.gef.EditPart;
//import org.eclipse.gef.editparts.AbstractEditPart;

import symbolic.execution.tree.graph.editparts.ConnectionEditPart;
import symbolic.execution.tree.graph.editparts.GraphEditPart;
import symbolic.execution.tree.graph.editparts.SimpleNodeEditPart;
import symbolic.execution.tree.graph.figures.SimpleNodeFigure;

public class DirectedGraphLayoutVisitor {
	
	private DirectedGraph digraph;
	private Map partToNode;
	
	public void layoutGraph(GraphEditPart graph){
		partToNode = new HashMap();
		digraph = new DirectedGraph();
		
		addNodes(graph);
		if (digraph.nodes.size() > 0){
			addEdges(graph);
			new DirectedGraphLayout().visit(digraph);
			applyResults(graph);
		}
	}

	/*****************Graph contribution methods**********************/
	private void addNodes(GraphEditPart graph) {
		for (Object child : graph.getChildren()){
			if (child instanceof SimpleNodeEditPart){
				addNodes((SimpleNodeEditPart) child);
			}
		}
		
	}
	
	private void addEdges(GraphEditPart graph){
		for (Object child : graph.getChildren()){
			if (child instanceof SimpleNodeEditPart){
				addEdges((SimpleNodeEditPart) child);
			}
		}
	}

	/*********************** Nodes contribution methods *****************/
	private void addNodes(SimpleNodeEditPart nodeEditPart) {
		Node node = new Node(nodeEditPart);
		node.width = nodeEditPart.getFigure().getPreferredSize(3000,4000).width;
		node.height = nodeEditPart.getFigure().getPreferredSize(3000,4000).height;
		
		node.setPadding(new Insets(30,30,30,30));
		partToNode.put(nodeEditPart, node);
		digraph.nodes.add(node);
	}
	
	private void addEdges(SimpleNodeEditPart nodeEditPart){
		List outgoings = nodeEditPart.getSourceConnections();
		for (Object conn : outgoings){
			ConnectionEditPart connectionPart = (ConnectionEditPart) conn;
			addEdges(connectionPart);
		}		
	}
	
	/*********************** Connection contribution mehods *************/
	private void addEdges(ConnectionEditPart connectionPart){		
		Node source = (Node) partToNode.get(connectionPart.getSource());
		Node target = (Node) partToNode.get(connectionPart.getTarget());
		Edge edge = new Edge(connectionPart,source,target);
		edge.weight=2;
		digraph.edges.add(edge);
		partToNode.put(connectionPart,edge);
	}
	
	/*************************** Graph apply methods *********************/
	private void applyResults(GraphEditPart graph){
		applyChildrenResult(graph);
	}

	private void applyChildrenResult(GraphEditPart graph) {
		for (Object child : graph.getChildren()){
			SimpleNodeEditPart nodePart = (SimpleNodeEditPart)child;
			applyResults(nodePart);
		}
		
	}

	/************************** Node apply methods ********************/
	private void applyResults(SimpleNodeEditPart nodePart) {
		Node node = (Node)partToNode.get(nodePart);
		SimpleNodeFigure figure = (SimpleNodeFigure) nodePart.getFigure();
		
		Rectangle bounds = new Rectangle(node.x,node.y,
				figure.getPreferredSize(300, 400).width, figure.getPreferredSize(300,400).height);
		figure.setBounds(bounds);
		
		for(Object conn : nodePart.getSourceConnections()){
			ConnectionEditPart connectionPart = (ConnectionEditPart) conn;
			applyResult(connectionPart);
		}
		
	}

	/*********************** Connection apply method *******************/
	private void applyResult(ConnectionEditPart connectionPart) {
		Edge edge = (Edge) partToNode.get(connectionPart);
		NodeList nodes = edge.vNodes;
		
		PolylineConnection conn = (PolylineConnection) connectionPart.getConnectionFigure();
		
		
		if (nodes != null){
			List bends = new ArrayList();
			for (int i=0; i<nodes.size(); i++){
				Node vn = nodes.getNode(i);
				int x=vn.x;
				int y=vn.y;
				
				if (edge.isFeedback()){
					bends.add(new AbsoluteBendpoint(x, y + vn.height));
					bends.add(new AbsoluteBendpoint(x,y));
				} else {
					bends.add(new AbsoluteBendpoint(x,y));
					bends.add(new AbsoluteBendpoint(x, y + vn.height));
				}
			}
			conn.setRoutingConstraint(bends);
		} else {
			conn.setRoutingConstraint(Collections.EMPTY_LIST);
		}
	}

}
