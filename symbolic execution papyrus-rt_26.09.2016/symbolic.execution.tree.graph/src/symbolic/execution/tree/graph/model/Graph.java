package symbolic.execution.tree.graph.model;

import java.util.ArrayList;
import java.util.List;

public class Graph {
	private List<SimpleNode> nodes;

	public List<SimpleNode> getNodes(){
		if (nodes == null){
			nodes = new ArrayList<SimpleNode>();
		}
		return nodes;
	}
	
	public void addNode(SimpleNode node){
		getNodes().add(node);
	}

	public void clearAll() {
		nodes = new ArrayList<SimpleNode>();
		
	}
	
}
