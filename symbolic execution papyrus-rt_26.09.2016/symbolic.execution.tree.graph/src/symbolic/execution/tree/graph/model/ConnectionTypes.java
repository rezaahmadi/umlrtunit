package symbolic.execution.tree.graph.model;

public enum ConnectionTypes {
	
	Synchronized, Unreceived, Open, Subsume;

}
