package symbolic.execution.tree.graph.model;

//import org.eclipse.draw2d.geometry.Dimension;
//import org.eclipse.draw2d.geometry.Point;
//import org.eclipse.draw2d.geometry.Rectangle;


public class Connection extends Element{
	private String text;
	//TODO:JDA:Decide whether to go with the 'saveText' version.
	private String saveText;
	
	private SimpleNode source,target;
	
	private ConnectionTypes type;
	
	public Connection(String text, ConnectionTypes type){
		//TODO:JDA:Decide whether to go with the 'saveText' version.
		//     JDA:was: this.text = text;
		this.text = "";
		this.saveText = text;
		this.type = type;
	}
	
	public SimpleNode getSource() {
		return source;
	}
	
	public SimpleNode getTarget() {
		return target;
	}
	
	public void setSource(SimpleNode source){
		if (this.source != null){
			this.source.removeSourceConnection(this);
		}
		this.source = source;
		if (source != null)
			source.addSourceConnection(this);
		setChanged();
		notifyObservers();
	}
		
	public void setTarget(SimpleNode target){
		if (this.target != null){
			this.target.removeTargetConnection(this);
		}
		this.target = target;
		if (target != null)
			target.addTargetConnection(this);
		setChanged();
		notifyObservers();
	}

	public String getText() {
		return text;
	}

	public ConnectionTypes getType() {		
		return type;
	}
}
