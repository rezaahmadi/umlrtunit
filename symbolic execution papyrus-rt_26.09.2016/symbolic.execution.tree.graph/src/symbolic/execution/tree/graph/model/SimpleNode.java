package symbolic.execution.tree.graph.model;


import java.util.ArrayList;
import java.util.List;
//import java.util.Observable;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;


public class SimpleNode extends Element{
	private String name;
	
	private Rectangle constraints;
	
	private List<Connection> sourceConnections;
	private List<Connection> targetConnections;
	

	public SimpleNode(String name) {
		super();
		this.name = name;
		this.constraints = new Rectangle(new Point(0,0), new Dimension(50,30));
	}

	public SimpleNode(String name, Rectangle constraints) {
		super();
		this.name = name;
		this.constraints = constraints;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rectangle getConstraints() {
		return constraints;
	}

	public void setConstraints(Rectangle constraints) {
		this.constraints = constraints;
	}
	
	public void changeLocation(Point p){
		this.constraints.setLocation(p);
	}

	public List<Connection> getSourceConnections() {
		if (sourceConnections == null){
			sourceConnections = new ArrayList<Connection>();
		}
		return sourceConnections;
	}
	
	public List<Connection> getTargetConnections() {
		if (targetConnections == null){
			targetConnections = new ArrayList<Connection>();
		}
		return targetConnections;
	}
	
	public void removeSourceConnection(Connection connection) {
		getSourceConnections().remove(connection);	
		setChanged();
		notifyObservers();
	}

	public void addSourceConnection(Connection connection) {
		getSourceConnections().add(connection);
		setChanged();
		notifyObservers();
	}

	public void removeTargetConnection(Connection connection) {
		getTargetConnections().remove(connection);
		setChanged();
		notifyObservers();		
	}

	public void addTargetConnection(Connection connection) {
		getTargetConnections().add(connection);
		setChanged();
		notifyObservers();
		
	}
	
	
}
