package symbolic.execution.tree.graph.model;

import org.eclipse.draw2d.geometry.Point;

public class ContentsModel {
	
	public static Graph getSampleModel(){
		Graph result = new Graph();
		
		SimpleNode n1 = createNode("node 1", new Point(50, 100));
		SimpleNode n2 = createNode("node 2", new Point(250, 150));
		SimpleNode n3 = createNode("node 3", new Point(50, 300));
		result.addNode(n1);
		result.addNode(n2);
		result.addNode(n3);
		
		CompositeNode cn1 = createCompositeNode("comp", new Point(250,150)); 
		result.addNode(cn1);
		
		createConnection(n2,n3);
		createConnection(n2,cn1);
		createConnection(n1,n2);		
		return result;
	}

	private static void createConnection(SimpleNode source, SimpleNode target) {
		Connection c = new Connection("text", ConnectionTypes.Open);
		c.setSource(source);
		c.setTarget(target);
		
	}

	private static CompositeNode createCompositeNode(String name, Point point) {
		CompositeNode comp = new CompositeNode(name, true);
		comp.changeLocation(point);
		comp.addChild(createNode("inside1_1",point));
		comp.addChild(createCompositeNode2("comp2", point));
		comp.addChild(createNode("inside1_2",point));
		return comp;
	}
	private static CompositeNode createCompositeNode2(String name, Point point) {
		CompositeNode comp = new CompositeNode(name, false);
		comp.changeLocation(point);
		comp.addChild(createNode("inside2_1",point));
		comp.addChild(createNode("inside2_2",point));
		return comp;
	}

	public static SimpleNode createNode(String name, Point location){
		SimpleNode result = new SimpleNode(name);
		result.changeLocation(location);
		
		return result;
	}
	
}
