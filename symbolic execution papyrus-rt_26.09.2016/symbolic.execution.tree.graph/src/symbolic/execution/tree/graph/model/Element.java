package symbolic.execution.tree.graph.model;

import java.util.Observable;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;

public class Element extends Observable {
	private boolean isHighlight;
	
	private Color highlightColor;
	
	public void highlight(Color color){
		if (!isHighlight){
			isHighlight = true;
			highlightColor = color;
			setChanged();
			notifyObservers();
		}
	}
	
	public void unhighlight(){
		if (isHighlight){
			isHighlight = false;	
			highlightColor = ColorConstants.black;
			setChanged();
			notifyObservers();
		}
	}
	
	public boolean getHighlight() {
		return isHighlight;
	}
	
	public Color getHighlightColor(){
		return highlightColor;
	}

}
