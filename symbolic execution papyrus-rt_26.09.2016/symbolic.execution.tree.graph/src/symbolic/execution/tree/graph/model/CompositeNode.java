package symbolic.execution.tree.graph.model;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Rectangle;

public class CompositeNode extends SimpleNode {
	private List<SimpleNode> children; 

	private boolean isExternal;
	
	public CompositeNode(String name, boolean isExternal) {
		super(name, new Rectangle(0,0,70,50));		
		this.isExternal = isExternal;
	}
	
	public List<SimpleNode> getNodeChildren(){
		if (children == null){
			children = new ArrayList<SimpleNode>();
		}
		return children;
	}
	
	public void addChild(SimpleNode node){
		getNodeChildren().add(node);
	}

	public boolean isExternal() {
		return isExternal;
	}

}
