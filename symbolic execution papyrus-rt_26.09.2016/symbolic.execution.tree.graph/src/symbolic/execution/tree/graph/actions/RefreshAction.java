package symbolic.execution.tree.graph.actions;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import symbolic.execution.tree.graph.SETEditor;
import symbolic.execution.tree.graph.SETEditorInput;
import symbolic.execution.tree.graph.editparts.GraphEditPart;

public class RefreshAction implements IEditorActionDelegate {
	SETEditor editor;
	
	
	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		if (targetEditor instanceof SETEditor)
			this.editor = (SETEditor) targetEditor;
		
	}

	@Override
	public void run(IAction action) {
		if (editor != null){
			((SETEditorInput)editor.getEditorInput()).unhighlight();
		
			GraphicalViewer viewer = editor.getGraphicalViewer();
			if (viewer.getContents() instanceof GraphEditPart){
				GraphEditPart part = (GraphEditPart)viewer.getContents();
				part.toGraphLayout();
				part.getFigure().getLayoutManager().layout(part.getFigure());
				
			}
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO: JDA: Is this Auto-generated method stub used? It's body is empty!
		
	}

}
