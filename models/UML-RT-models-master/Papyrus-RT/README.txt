This folder contains plugins developped with Papyrus-RT.

Some hints to make the life easier to everyone who would like to use your
models: 

- Please provide clean and compilable models;

- Please add a folder with all diagrams in graphical format (so we do not have
  to import the model to understand it);

- Please provide a description of the model (for example an attached text
  file), and if possible, the code generator used;

- Make sure that your model does not import useless profile defintions nor
  packages;

- Make sure that diagrams are create with the Papyrus-RT viewpoint
  configuration (diagram icons are yellow);

- As the current code generator of Papyrus-RT recently changed, please be sure
  that your models are compatible with the last version. (Specially, the use
  "umlrtparam_" prefixing all protocols' parameters has been removed);

- Your model should not depend on any user-defined package or profile
  definition. However, if it is the case, please publish both.
