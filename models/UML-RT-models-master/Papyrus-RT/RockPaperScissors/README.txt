Name: Rock Paper Scissors
Author: Nicolas Hili <hili@cs.queensu.ca>
Version: 0.1
Last edited: February, 6, 2017
Papyrus-RT C++ Code Generator: 0.8.0.201612210512
Eclipse SDK: Neon.2 (4.6.2)
Operating System: Linux / Ubuntu 16.04.1 LTS 

Description:

    This models is a simulation of the popular "Rock Paper Scissors" game. It
    consists of one referee asking two players to pick a choice between Rock,
    Paper, and Scissors. Each player provides its choice to the referee that
    designates the winner (or draw). The game ends after three rounds. In case
    of draw, the round is started again.


Change log:

0.1: initial version. 


Trace output:

Controller "DefaultController" running.

-- Starting round 1 -- Player 1 played: Rock Player 2 played: Rock No winner.
Let's try again

-- Starting round 1 -- Player 1 played: Paper Player 2 played: Rock First
player won the round Score is currently (1 - 0)

-- Starting round 2 -- Player 1 played: Rock Player 2 played: Scissors First
player won the round Score is currently (2 - 0)

-- Starting round 3 -- Player 1 played: Rock Player 2 played: Rock No winner.
Let's try again

-- Starting round 3 -- Player 1 played: Scissors Player 2 played: Rock Second
player won the round

First player won the game !!!  Final score is (2 - 1) Game is done.
