This folder contains plugins developped with RSARTE.

Some hints to make the life easier to everyone who would like to use your
models: 

- Please provide clean and compilable models;

- Please add a folder with all diagrams in graphical format (so we do not have
  to import the model to understand it);

- Please provide a description of the model (for example an attached text
  file), and if possible, the code generator used;

- Make sure that your model does not import useless profile defintions nor
  packages;

- Your model should not depend on any user-defined package or profile
  definition. However, if it is the case, please publish both.
