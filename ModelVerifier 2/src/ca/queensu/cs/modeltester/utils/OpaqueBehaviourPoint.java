package ca.queensu.cs.modeltester.utils;

/**
 * @author reza
 *
 */
public enum OpaqueBehaviourPoint {
	OnExit,
	OnEntry
}
