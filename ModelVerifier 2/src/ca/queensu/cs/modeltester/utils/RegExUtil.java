/**
 * 
 */
package ca.queensu.cs.modeltester.utils;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author reza
 *
 */
public class RegExUtil {
	
//	public static String getVarDecl(String actionCode) {
//		Pattern p = Pattern.compile(".+\\s(.+?)(;|=)");
//		Matcher m = p.matcher(actionCode);
//
//		if (m.find()) {
//			return m.group(1);
//		}
//		return "";
//	}
	
	public static List<String> getVarsDecl(String actionCode) {
		Pattern p = Pattern.compile("\\s*([a-zA-Z_$][a-zA-Z_$0-9]*)\\s+([a-zA-Z_$][a-zA-Z_$0-9]*)\\s*(;|=)");
		Matcher m = p.matcher(actionCode);
		
		List<String> vars = new ArrayList<String>();

		while (m.find()) {
			vars.add(m.group(2));
		}
		return vars;
	}
	
	
	public static boolean isVarAssigned(String actionCode, String var) {
//		Pattern p = Pattern.compile(".*[+-=]\\s*(" + var + ")\\s*[+-;].*");
//		Pattern p = Pattern.compile("\\s*([a-zA-Z_$][a-zA-Z_$0-9]*)\\s*=");
//		Matcher m = p.matcher(actionCode);
//		if (m.find()) {
//			if (m.group(1).equals(var))
//				return true;
//		}
////		if (m.find()) return true;
//		return false;
		return getVarAssigns(actionCode).contains(var);
	}
	
	public static String getVarAssign(String actionCode) {
		List<String> res = getVarAssigns(actionCode);
		if (res.size()>0)
			return res.get(0);
		return "";
	}

	// we support, for now,only var++, var--, --var, ++var and var = ..
	public static List<String> getVarAssigns(String actionCode) {
		Pattern p1 = Pattern.compile("\\s*(\\w+)\\s*=\\s*");
		Matcher m1 = p1.matcher(actionCode);

		// Pattern p2 = Pattern.compile("\\s*\\w+\\+\\+");
		Pattern p2 = Pattern.compile("\\s*(\\w+)[+][+]");
		Matcher m2 = p2.matcher(actionCode);

		Pattern p3 = Pattern.compile("\\s*[+][+](\\w+)");
		Matcher m3 = p3.matcher(actionCode);

		Pattern p4 = Pattern.compile("\\s*[-][-](\\w+)");
		Matcher m4 = p4.matcher(actionCode);

		Pattern p5 = Pattern.compile("\\s*(\\w+)[-][-]");
		Matcher m5 = p5.matcher(actionCode);

		List<String> assigns = new ArrayList<String>();

		while (m1.find()) {
			assigns.add(m1.group(1));
		}
		while (m2.find()) {
			assigns.add(m2.group(1));
		}
		while (m3.find()) {
			assigns.add(m3.group(1));
		}
		while (m4.find()) {
			assigns.add(m4.group(1));
		}
		while (m5.find()) {
			assigns.add(m5.group(1));
		}

		return assigns;
	}

//	public static String getAssignmentLeftSide(String actionCode) {
//		Pattern p = Pattern.compile("\\s*(\\w+)\\s*=");
//		Matcher m = p.matcher(actionCode);
//
//		if (m.find()) {
//			return m.group(1);
//		}
//		return "";
//	}
	
	private static List<String> getDeclareLeft(String actionCode) {
		List<String> res = new ArrayList<String>();
		if (getVarsDecl(actionCode).size() > 0) {
			String left = actionCode.split("[=;]")[0];
			for (String ss : left.split(" ")) {
				if (!ss.equals("") && !ss.equals(" "))
					res.add(ss);
			}
		}
		return res;
	}
	
	public static boolean isVarUsed(String actionCode, String var) {
		return getAllVarsUsed(actionCode).contains(var);
	}
	
	public static String getIfPredicate(String actionCode) {
		Pattern p = Pattern.compile("if\\s*[(](.+)[)]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
		Matcher m = p.matcher(actionCode);

		if (m.find()) {
			return m.group(1);
		}
		return "";
	}

	
	public static List<String> getIfBodies(String actionCode) {
		// int i1= actionCode.indexOf(actionCode);
		// int i2= actionCode.last(actionCode);

		Pattern p = Pattern.compile("if(.*)[{](.*)[}]", Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
		Matcher m = p.matcher(actionCode);

		List<String> ifs = new ArrayList<String>();

		while (m.find()) {
			ifs.add((m.group(2)));
		}
		return ifs;
	}
	
	// currently only supports right hand side in an assignment
	// needs a fix to disclude vars declared
	public static List<String> getAllVarsUsed(String actionCode) {
		// Pattern p = Pattern.compile(".*[+-=]\\s*(" + var + ")\\s*[+-;].*");
		//Pattern p = Pattern.compile("\\s*(return|.*[<>=+-/%,(])\\s*([a-zA-Z_$][a-zA-Z_$0-9]*)*");
		Pattern p = Pattern.compile("\\s*([a-zA-Z_$][a-zA-Z_$0-9]*)\\s*");
		Matcher m = p.matcher(actionCode);
		List<String> vars = new ArrayList<String>();
		while (m.find()) {
			if (!vars.contains(m.group(1)) && !getDeclareLeft(actionCode).contains(m.group(1)))
				vars.add(m.group(1));
		}
		// if (m.find()) return true;
		return vars;
	}
	
//	private List<String> types(){
//		return new ArrayList<String>(){{add("int");add("float");add("double");}};
//	}
	
	public static List<String> getMessageParameters(String actionCode) {
		Pattern p = Pattern.compile(".+[(](.*)[)][.]send.*");
		Matcher m = p.matcher(actionCode);

		if (m.find()) {
			return Arrays.asList(m.group(1).replace(" ", "").split(","));
		}
		return null;
	}

	public static boolean isMessageSendingCommand(String actionCode) {
		Pattern p = Pattern.compile(".+[.]send[(].*[)].*");
		Matcher m = p.matcher(actionCode);

		return m.matches();
	}
	
	public static boolean isSendToPortCommand(String actionCode, String portName) {
		Pattern p = Pattern.compile(portName + "[.].+[.]send[(].*[)].*");
		Matcher m = p.matcher(actionCode);

		return m.matches();
	}
	
	public static boolean isTimerSetCommand(String actionCode) {
		Pattern p = Pattern.compile("\\s*.+[.]informIn[(].+[)].*");
		Matcher m = p.matcher(actionCode);

		return m.matches();	
	}

//	public static String getIfPredicate(String actionCode) {
//		Pattern p = Pattern.compile("\\s*if[(](.+)[)].*");
//		Matcher m = p.matcher(actionCode);
//
//		if (m.find()) {
//			return m.group(1);
//		}
//		return "";
//	}


	public static boolean isTriggerGenerated(String actionCode, String trig) {
		//if (!isMessageSendingCommand(actionCode))
		//	return false;
		String pt = ".*" + trig + ".*[.]send[(].*[)].*";
//		Pattern p = Pattern.compile(pt);
//		Matcher m = p.matcher("\n  port1.msg1().send();");
//		return m.matches();
		
		Pattern regex = Pattern.compile("^.*"+trig+".*", Pattern.DOTALL);
		Matcher regexMatcher = regex.matcher(actionCode);
		boolean f = regexMatcher.find();
		return f;
	}


	public static List<String> getIfBlocks(String actionCodesStr) {
		// TODO Auto-generated method stub
		return null;
	}

	public static boolean isVarDeclared(String statement, String varName) {
		return getVarsDecl(statement).stream().anyMatch(decl->decl.equals(varName));
	}
	
}
