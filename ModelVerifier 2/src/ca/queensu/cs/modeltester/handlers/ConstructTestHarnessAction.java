package ca.queensu.cs.modeltester.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolConformance;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StringExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.internal.impl.CollaborationImpl;
import ca.queensu.cs.modeltester.smarttesting.ITestGenerator;
import ca.queensu.cs.modeltester.smarttesting.InputMessage;
import ca.queensu.cs.modeltester.smarttesting.RandomTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SequentialTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SmartTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.TestData;
import ca.queensu.cs.modeltester.smarttesting.TestGenConfig;
import parser.absconparseur.components.PSoftRelation;
import symbolic.execution.ffsm.engine.concrete.ConcretePath;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import ca.queensu.cs.modeltester.utils.OpaqueBehaviourPoint;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import org.eclipse.papyrusrt.umlrt.uml.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.DateTime;

/**
 * @author reza implements and extends both interface and base cases to allow
 *         user generating test cases using popup menu or using toolbar. ToDo:
 *         AbstractHandler.execute is not implemented yet.
 */
public class ConstructTestHarnessAction extends AbstractHandler implements IWorkbenchWindowActionDelegate {

	private static Class selectedCapsule;
	private static Class cut;
	
	private static IWorkbenchWindow window;

	public static class TestGeneratorRunner implements Runnable {
		
		//test inputs to be generated
		List<TestData> testSuit;
		TestSuitPanel testSuitPanel; 

		
		private TestGeneratorRunner() {
			
		}

		// private static String SmartTestGeneratorQualifiedName =
		// "ca.queensu.cs.modelverifier.testharness.SmartTestGenerator";
		private static TestGeneratorRunner instance;

		public static TestGeneratorRunner getInstance() {
			if (instance == null)
				instance = new TestGeneratorRunner();
			return instance;
		}

		@Override
		public void run() {
			try {
				// enableSmartTestGenerator(true);
				// java.lang.Class[] params = new java.lang.Class[1];
				// params[0] = Class.class;
				// getSmartTestGeneratorObj().getClass().getMethod("generate",
				// params).invoke(getSmartTestGeneratorObj(),
				// SmartTestcaseGenerationAction.capsule);
				if (ExecutionOptions.getMaxDepth() > 10)
					ExecutionOptions.setMaxDepth(6);

				// generateTestCasesStateMachine();
				// ToDo: show test generation panel
				TestGenerationOptionsPanel tgo = new TestGenerationOptionsPanel();
				tgo.startGUI();

				// ToDo: inject input signals as Entry action code into
				// RunningTest state

				// generate JUnit test cases to be executed on the property
				// Todo: extract property names automatically and generate test
				// cases for each one of them

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void generateTestSuit() {

			// TestGenConfig conf = new TestGenConfig(5, 10, true);
			// ITestGenerator smartTestCaseGenerator = new
			// RandomTestGenerator(window, selectedCapsule, conf);
			// ITestGenerator smartTestCaseGenerator =
			// SymbolicTestGenerator.getInstance(window, selectedCapsule, conf);
			ITestGenerator smartTestCaseGenerator;
			if (TestGenConfig.isSequentialTesting())
				smartTestCaseGenerator = new SequentialTestGenerator(window, selectedCapsule);
			else if (TestGenConfig.isRandomTesting())
				smartTestCaseGenerator = new RandomTestGenerator(window, selectedCapsule);
			else
				smartTestCaseGenerator = new SmartTestGenerator(window, selectedCapsule);

			// generate test inputs
			Class cut = UmlrtUtil.getCapsuleByStereotypeName(selectedCapsule, "CapsuleUnderTest");
			
			//calculate time
			//DateTime dateTime = new DateTime(parent, SWT.CALENDAR | SWT.SHORT);
			//dateTime.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			long t1 = System.currentTimeMillis();
//			long diffInMillies = date2.getTime() - date1.getTime();
//		    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
			testSuit = smartTestCaseGenerator.generateTestInputs(cut);
			long t2 = System.currentTimeMillis();
			System.out.println(String.format("**time to generate tests: %s **", t2 - t1));

			//show the generated tests
			testSuitPanel = new TestSuitPanel();
			testSuitPanel.showTestSuitTree(testSuit);
			
			// System.out.println("printing generated tests..");
			// for (TestData test: testInputs){
			// System.out.println(test.getName() + " : " +
			// test.getInputs().toString());
			// }

			//ToDo: add this to a button click
			//inject input signals as a string of states and transitions to the TestHarness state machine
			//injectTestSuitInTestHarness();
		}

		public void injectTestSuitInTestHarness() {
			
			//ToDo: remove this: only for test purposes
			List<TestData> newTestSuit = new ArrayList<TestData>();
			if (testSuitPanel.getTree().getSelectionCount() > 0) {
				for (TreePath node : testSuitPanel.getTree().getSelectionPaths()) {
					DefaultMutableTreeNode selectedTest = (DefaultMutableTreeNode) node.getPath()[1];
					for (TestData test : testSuit) {
						if (test.getCode() == Integer.parseInt(selectedTest.getUserObject().toString())) {
							newTestSuit.add(test);
						}
					}
				}
				
				if (newTestSuit.size() > 0)
					testSuit = newTestSuit;
			}

			Class testHarnessCapsule = UmlrtUtil.getCapsuleByStereotypeName(selectedCapsule, "TestHarness");
			StateMachine testHarnessSm = UmlrtUtil.getStateMachine(testHarnessCapsule);
			
			Pseudostate initialState = UmlrtUtil.getInitialState(testHarnessSm);
			State mainState = ((State) (testHarnessSm.getRegions().get(0).getSubvertex("Main")));
			State testSuitRunningState = (State) mainState.getRegions().get(0).getSubvertex("TestSuiteRunning");
			Region testRegion = testSuitRunningState.getRegions().get(0);

			//Init state
			final Vertex initialVertex = testSuitRunningState.getConnectionPoint("entry1").getOutgoings().get(0).getTarget();
			final Vertex exitVertex = testSuitRunningState.getConnectionPoint("exit1");// UmlrtUtil.getConnectionPoint2(testHarnessSm, "Exit1");

			// first remove existing test cases
			removeExistingTestSuit(testSuitRunningState);

			// for every test input
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(testHarnessCapsule);
			domain.getCommandStack().execute(new RecordingCommand(domain) {
				@Override
				protected void doExecute() {
					// Implement your write operations here,
					// for example: set a new name
					
					//creating required ports on the test harness
					for (String portStr: UmlrtUtil.getPortsInvoved(testSuit)) {
						Port port = UmlrtUtil.getPort(cut, portStr);
						if (!UmlrtUtil.capsuleHasPort(testHarnessCapsule, portStr)) {
							Port newPort = testHarnessCapsule.createOwnedPort(port.getName(), port.getType());
							newPort.applyStereotype(port.getAppliedStereotypes().get(0));
							newPort.setIsConjugated(!port.isConjugated());
							newPort.setIsBehavior(true);
						}
//						if (!UmlrtUtil.capsuleHasPort(testHarnessCapsule,newPort.getName())) {
						//testHarnessCapsule.getOwnedPorts().add(newPort);
						//						}
					}

//					Vertex sourceState = initialVertex.getOutgoings().get(0).getTarget();
					Vertex sourceState = initialVertex;
					String testInputsStr = "";
					// int lim = 2;
					int nodeCounter = 0;
					int testCounter = 1;
					Transition trans = null;
					for (TestData testCase : testSuit) {
						// if (lim <= 0)
						// break;
						// testInputsStr += String.format("std::cout<<
						// \"UMLrtUnit: running test [%s]..\" <<
						// std::endl;\n", testInputEntry.getValue());

						// for every input signal in a test input
						State targetState = null;
						boolean setupTestFlg = false;
						boolean nextTestFlg = false;
						for (InputMessage testInput : testCase.getInputMessages()) {

							// target state
							Vertex newState = testRegion.createSubvertex("__test__node__" + nodeCounter,
									UMLPackage.eINSTANCE.getState()); // UMLFactory.eINSTANCE.createState();
							targetState = (State) newState;
							
							//ToDo: apply RTState stereotype
							EObject rtState = (EObject) testSuitRunningState.getAppliedStereotypes().get(0);
							if (rtState instanceof Stereotype) 
								targetState.applyStereotype((Stereotype)rtState);
							// targetState.setName(testInput +
							// "_n_testingnode");
							testRegion.getSubvertices().add(targetState);

							// if event is not timeout, then an input
							// signal is added to the test state
							String port = testInput.getPort();
							String msg = testInput.getName();
							String stateEntryStr = "";
							
							//adding an entry code to report about the upcoming test & tell about the upcoming test
							//only for the first state of the test
							
							if (!setupTestFlg) {
//							addSetupTestAction(testCase, targetState);
							}
							
							String params = testInput.getParamtersString();
							//State Entry code
							if (!msg.equals("timeout") && !msg.equals("timeout()")) { 
								stateEntryStr = String.format("std::cout<<\"*** Test Harness: %s.%s(%s) ***\" << std::endl;\n "
										+ "%s.%s(%s).send();\n", port, msg, params, port, msg, params);
								//adding an action code to record the event
								stateEntryStr += String.format("TE.setEvent(\"%s.%s(%s)\");", port, msg, params);
							}
							else {
								stateEntryStr = String.format("std::cout<<\"*** Test Harness: waiting for timer[%s] to time out ***\" << std::endl;\n", port);
								//adding an action code to record the event
								stateEntryStr += String.format("TE.setEvent(\"WAITING_FOR_%s.%s\");", port, msg);
							}
							//adding the opaque behavior
							UmlrtUtil.addOpaqueBehaviourToState(targetState, stateEntryStr, OpaqueBehaviourPoint.OnEntry);
/*							
							OpaqueBehavior stateEntry = UMLFactory.eINSTANCE.createOpaqueBehavior();
							stateEntry.getLanguages().add("C++");
							stateEntry.getBodies().add(stateEntryStr);
							// setStateEntryInTransaction(targetState,
							// stateEntry);
							targetState.setEntry(stateEntry); */

							// creating transition
							trans = testRegion.createTransition("");// UMLFactory.eINSTANCE.createTransition();
//							trans = testRegion.createTransition("_t_" + nodeCounter);// UMLFactory.eINSTANCE.createTransition();

							// creating a trigger
							//if this is the first transition of the test we add nextTest else nextState
							Trigger trig = null;
							if (!nextTestFlg) {
								trig = UmlrtUtil.createTrigger(testHarnessCapsule, "TestData", "nextTest");

								//Also an action code for adding the current TestEvent to the TestReportObj
								//if the first transition of the test state machine
								if (sourceState.getName().equals("INIT")) {
										UmlrtUtil.addEffectToTransition(trans,String.format(
										  "TE.Event = \"\";\n" + 
										  "TE.TestCode = %d;\n", testCase.getCode()));
								}
								else {
										UmlrtUtil.addEffectToTransition(trans,String.format(
										   "TestReportObj.add (TE);\n" + 
										   "TE.Event = \"\";\n" + 
										   "TE.TestCode = %d;\n", testCase.getCode()));
								}
								nextTestFlg = true;
							} else {
								trig = UmlrtUtil.createTrigger(testHarnessCapsule, "TestData", "nextState");
								Event e2 = trig.getEvent();
								
							}

							//ToDo: use UML-RT Facade objects
							//UMLRTFactory.createTransition(trans);
//							UMLRTTransition tt = UMLRTFactory.createTransition(transition);
							
							nodeCounter++;
							// trans.setName(testInput + "_t_testingnode");
							testRegion.getTransitions().add(trans);
							trans.getTriggers().add(trig);
							System.out.println(trig.getEvent().eResource());
								System.out.println(trig.eResource());
								System.out.println(trig.getPorts().get(0).getType().eResource());
							// join source and target states
							trans.setSource(sourceState);
							trans.setTarget(targetState);

							// adding new trigger and state
							// testHarnessSm.getRegions().get(0).getSubvertices().add(targetState);
							// testHarnessSm.getRegions().get(0).getTransitions().add(trans);

							// set the current target state as source
							// state
							sourceState = targetState;
							
						}//for (String testInput : testCase.getInputs())
						
						// testInputsStr +=
						// String.format("TestReport.nextTest().send();\n\n",
						// testInputEntry.getValue());
						// lim--;

						
						// adding an effect on last node to move
						// CUT and property to next test
						// This should not be added on the last node of the testSuit state machine
						if (testCounter <= testSuit.size() && targetState instanceof State) {
//							addTearDownTestAction(testCase, targetState);
							addSetupTestAction(testCase, targetState);
						}
						
						
						
						testCounter++;
					}//for (TestData testCase : testSuit)

					//create a link between last node and exitpoint1
					Transition transLast = testRegion.createTransition("");// UMLFactory.eINSTANCE.createTransition();
//					Transition transLast = testRegion.createTransition("_t_" + nodeCounter);// UMLFactory.eINSTANCE.createTransition();
					Trigger trig = UmlrtUtil.createTrigger(testHarnessCapsule, "timerLast", "timeout");
					transLast.getTriggers().add(trig);
					nodeCounter++;
					// trans.setName(testInput + "_t_testingnode");
					testRegion.getTransitions().add(transLast);

					// join source and target states
					transLast.setSource(sourceState);
					transLast.setTarget(exitVertex);
					
					//set lastTimer timer on last node
					//ToDo: this should be more robust
					String setLastTimerStr = "timerLast.informIn(UMLRTTimespec(0,1));";
					UmlrtUtil.addOpaqueBehaviourToState(sourceState, setLastTimerStr, OpaqueBehaviourPoint.OnEntry);
					
					//add the last TestEvent to TestReportObj as the last transition effect
					UmlrtUtil.addEffectToTransition(transLast, "TestReportObj.add (TE);\n");

				}//doExecute

				private void addTearDownTestAction(TestData testCase, State targetState) {
					String body = String.format("TestData.tearDownTest(%d).send();\n ",testCase.getCode());
					UmlrtUtil.addOpaqueBehaviourToState(targetState, body, OpaqueBehaviourPoint.OnEntry);
				}

				private void addSetupTestAction(TestData testCase, State targetState) {

					String nextTestMsg = String.format(
							"std::cout << \"*** Test Harness: running test [Test%d] with inputs %s ***\" << std::endl;\n"
									+ "TestData.setUpTest(%d).send();\n",
							testCase.getCode(), testCase.toString(), testCase.getCode());
					UmlrtUtil.addOpaqueBehaviourToState(targetState, nextTestMsg, OpaqueBehaviourPoint.OnEntry);
					/*
					 * OpaqueBehavior nextTestEntryCode =
					 * UMLFactory.eINSTANCE.createOpaqueBehavior();
					 * nextTestEntryCode.getLanguages().add("C++"); String nextTestMsg =
					 * String.format( "TestData.initTest(\"%s\").send();\n " +
					 * "std::cout << \"*** Test Harness: running test [%s] with inputs %s ***\" << std::endl;\n"
					 * , testInputEntry.getName(), testInputEntry.getName(),
					 * testInputEntry.getInputs().toString()); if (((State) targetState).getEntry()
					 * != null && ((OpaqueBehavior) ((State) targetState).getEntry()).getBodies() !=
					 * null) { nextTestMsg = nextTestMsg + ((OpaqueBehavior)((State)
					 * targetState).getEntry()).getBodies().get(0); }
					 * nextTestEntryCode.getBodies().add(nextTestMsg); ((State)
					 * targetState).setEntry(nextTestEntryCode);
					 */

				}
				
			});
		}
		
		private void removeExistingTestSuit(State testRunningState) {
			TransactionalEditingDomain domain0 = TransactionUtil.getEditingDomain(testRunningState);
			domain0.getCommandStack().execute(new RecordingCommand(domain0) {
				@Override
				protected void doExecute() {
					List<Vertex> rem = new ArrayList<Vertex>(); 
					for (Vertex v: testRunningState.getRegions().get(0).getSubvertices())
						if (!v.getName().equals("INIT"))
							rem.add(v);
					
					List<Transition> rem2 = new ArrayList<Transition>(); 
					for (Transition t: testRunningState.getRegions().get(0).getTransitions())
						if (!t.getName().equals("tINIT"))
							rem2.add(t);
					
					if (rem != null && rem.size() > 0)
						testRunningState.getRegions().get(0).getSubvertices().removeAll(rem);
					if (rem2 != null && rem2.size() > 0)
						testRunningState.getRegions().get(0).getTransitions().removeAll(rem2);
				}
			});
		}
	
	}

	private static void testStateCreate() {

		Class testSuitsCapsule = UmlrtUtil.getCapsuleByStereotypeName(selectedCapsule, "TestSuit");
		StateMachine testSuitSm = UmlrtUtil.getStateMachine(testSuitsCapsule);

		State runningTestState = UMLFactory.eINSTANCE.createState();
		runningTestState.setName("tttt");
		//
		// OpaqueBehavior inputTestsActionCode =
		// UMLFactory.eINSTANCE.createOpaqueBehavior();
		// //(OpaqueBehavior)runningTestS.getEntry();
		// inputTestsActionCode.getLanguages().add("C++");
		// inputTestsActionCode.getBodies().add(testInputsStr);
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(testSuitSm);
		domain.getCommandStack().execute(new RecordingCommand(domain) {
			@Override
			protected void doExecute() {
				// Implement your write operations here,
				// for example: set a new name
				testSuitSm.getRegions().get(0).getSubvertices().add(runningTestState);
			}
		});
	}

	@Override
	public void run(IAction arg0) {
		if (selectedCapsule != null) {
			Thread verifierThread = new Thread(TestGeneratorRunner.getInstance());
			verifierThread.setName("modelTestingThread");
			verifierThread.start();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}
		IStructuredSelection structured = (IStructuredSelection) selection;

		Object selectedObject = structured.getFirstElement();

		if (selectedObject instanceof IAdaptable) {
			IAdaptable adaptableObject = (IAdaptable) selectedObject;
			selectedCapsule = UmlrtUtil.selectIfCapsule((EObject) adaptableObject.getAdapter(EObject.class));
			cut = UmlrtUtil.getCapsuleByStereotypeName(selectedCapsule, "CapsuleUnderTest");
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void init(IWorkbenchWindow w) {
		this.window = w;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

//	private static void setStateEntryInTransaction(State obj, OpaqueBehavior actionCode) {
//		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(obj);
//		domain.getCommandStack().execute(new RecordingCommand(domain) {
//			@Override
//			protected void doExecute() {
//				// Implement your write operations here,
//				// for example: set a new name
//				obj.setEntry(actionCode);
//			}
//		});
//	}
	
	
}
