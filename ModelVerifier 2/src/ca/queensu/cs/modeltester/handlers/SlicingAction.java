package ca.queensu.cs.modeltester.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolConformance;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StringExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.internal.impl.CollaborationImpl;

import ca.queensu.cs.modeltester.slicing.Slicer;
import ca.queensu.cs.modeltester.smarttesting.ITestGenerator;
import ca.queensu.cs.modeltester.smarttesting.RandomTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SequentialTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SmartTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.TestData;
import ca.queensu.cs.modeltester.smarttesting.TestGenConfig;
import parser.absconparseur.components.PSoftRelation;
import symbolic.execution.ffsm.engine.concrete.ConcretePath;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import ca.queensu.cs.modeltester.utils.OpaqueBehaviourPoint;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import org.eclipse.papyrusrt.umlrt.uml.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.DateTime;

/**
 * @author reza implements and extends both interface and base cases to allow
 *         user generating test cases using popup menu or using toolbar. ToDo:
 *         AbstractHandler.execute is not implemented yet.
 */
public class SlicingAction extends AbstractHandler implements IWorkbenchWindowActionDelegate {

	private static Class selectedCapsule;
	private static Class cut;
	
	private static IWorkbenchWindow window;

	 
	@Override
	public void run(IAction arg0) {
		if (getSelectedCapsule() != null) {
			//StateMachineSlicer.testDD(getSelectedCapsule());
			new SlicingOptionsPanel().startGUI();
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}
		IStructuredSelection structured = (IStructuredSelection) selection;

		Object selectedObject = structured.getFirstElement();

		if (selectedObject instanceof IAdaptable) {
			IAdaptable adaptableObject = (IAdaptable) selectedObject;
			setSelectedCapsule(UmlrtUtil.selectIfCapsule((EObject) adaptableObject.getAdapter(EObject.class)));
			cut = UmlrtUtil.getCapsuleByStereotypeName(getSelectedCapsule(), "CapsuleUnderTest");
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void init(IWorkbenchWindow w) {
		this.window = w;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	public static Class getSelectedCapsule() {
		return selectedCapsule;
	}

	public static void setSelectedCapsule(Class selectedCapsule) {
		SlicingAction.selectedCapsule = selectedCapsule;
	}

}
