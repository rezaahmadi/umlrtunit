/**
 * 
 */
package ca.queensu.cs.modeltester.handlers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Map;

import java.util.*;

import javax.swing.JComponent;

/**
 * @author rezaahmadi
 *
 */

// This library uses mxgraph.JGraph to visualize 
// dependencies in a UML-RT Capsule

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxEdgeStyle;
import com.mxgraph.view.mxGraph;

import ca.queensu.cs.modeltester.handlers.SlicingOptionsPanel.OptionsPanel;
import ca.queensu.cs.modelverifier.slicing.data.Dependency;
import ca.queensu.cs.modelverifier.slicing.data.DependencyGraph;
import ca.queensu.cs.modelverifier.slicing.data.DependencyNode;

public class DependencyViewer extends javax.swing.JPanel {

	private static final long serialVersionUID = 12L;

	private GraphControl graphControl;

	private DependencyViewer(DependencyGraph dg) {
		super(new BorderLayout());

		// super("mxGraph");

		// Creates graph with model
		mxGraph graph = new mxGraph();
		Object parent = graph.getDefaultParent();

		// Sets the default vertex style
		Map<String, Object> style = graph.getStylesheet().getDefaultVertexStyle();
		style.put(mxConstants.STYLE_GRADIENTCOLOR, "#BBCCEE");
		style.put(mxConstants.STYLE_ROUNDED, true);
		style.put(mxConstants.STYLE_SHADOW, true);
		style.put(mxConstants.STYLE_NOEDGESTYLE, true);
		style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
		style.put(mxConstants.STYLE_EDGE, mxEdgeStyle.ElbowConnector);

		//style.put(mxConstants.STYLE_LABEL_BORDERCOLOR, "#000000");
		//style.put(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, "#000000");


		Map<Object, Object> Dependency_Vertex_Map = new HashMap<Object, Object>();

		graph.getModel().beginUpdate();

		try {

			//adding new vertex per node
			for (DependencyNode node : dg) {
				Object vertex = graph.insertVertex(parent, null, node.getDependantName(), 0, 0, 150, 30);
//				if (!Dependency_Vertex_Map.containsKey(node.getDependant()))
				Dependency_Vertex_Map.put(node, vertex);
			}
			
//			if (Dependency_Vertex_Map.size()<2) { //dum
				
				
			// add nodes
			for (DependencyNode node : dg) {

				List<Dependency> dependencies = node.getDependencies();

				// only add nodes with dependencies
				// if (dependencies != null && dependencies.size() > 0) {
				// source node
				Object nodeFrom = Dependency_Vertex_Map.get(node); 
				for (Dependency dep : dependencies) {

					// target node
					Object nodeTo = Dependency_Vertex_Map.get(dep.getDependentNode()); 

					// create edge
					Object newEdge = graph.insertEdge(parent, null, dep.getKind(), nodeFrom, nodeTo);
					
					//triangle
//					mxCell v3 = (mxCell) graph.insertVertex(newEdge, null, dep.getKind(), -0.5, 0, 40, 40,
//							"shape=triangle");
//					v3.getGeometry().setRelative(true);
//					v3.getGeometry().setOffset(new mxPoint(-20, -20));

				}
				// }
			}
			
//			} //dum

            //mxIGraphLayout layout = new ExtendedCompactTreeLayout(graph);

			mxIGraphLayout layout = new mxHierarchicalLayout(graph);
			layout.execute(parent);
		} finally {
			graph.getModel().endUpdate();
		}

		// Creates a control in a scrollpane
		graphControl = new GraphControl(graph);
		JScrollPane scrollPane = new JScrollPane(graphControl);
		scrollPane.setAutoscrolls(true);

		// Puts the control into the frame
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		setPreferredSize(new Dimension(720, 720));
	}

	public static void showUI(DependencyGraph dg) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				JFrame frame = new JFrame("<< Dependency Viewer >>");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				// Create and set up the content pane.
				JComponent newContentPane = new DependencyViewer(dg);
				newContentPane.setOpaque(true); // content panes must be opaque
				frame.setContentPane(newContentPane);

				// Display the window.
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);

			}
		});
	}

}
