package ca.queensu.cs.modeltester.handlers;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.eclipse.uml2.uml.Component;

import ca.queensu.cs.modeltester.smarttesting.TestGenConfig;
import header.java;;

/**
 * @author reza
 *
 */
public class TestGenerationOptionsPanel {

	public static class OptionsPanel extends javax.swing.JPanel implements ItemListener {

		private static final long serialVersionUID = 1L;
		JRadioButton radioBtnPerformSymbolicTestGeneration;
		JRadioButton radioBtnPerformRandomTestGeneration;
		JRadioButton radioBtnPerformSequentialTestGeneration;
		JCheckBox chkStopOnFailureFound;
		JCheckBox chkReduceTestsBasedOnProp;
		
		ButtonGroup grpOptions = new ButtonGroup(); 
		JTextField txtMaxTestLength;
		JTextField txtOutMsgFilter;
		JTextField txtTestSuitSize;
		JTextField txtParametersRanges;
		JButton btnGenerateTests;
		JLabel lblMaxTestLength;
		JLabel lblTestSuitSize;
		JLabel lblOutMsgFilter;
		JLabel lblParametersRanges;


		static int order = 0;

		public OptionsPanel() {
			super(new BorderLayout());

//			JOptionPane.showMessageDialog(null, "Hello");
			radioBtnPerformSymbolicTestGeneration = new JRadioButton ("Perform Test Gen using Symbolic Execution");
			radioBtnPerformSymbolicTestGeneration.setSelected(TestGenConfig.isSmartTesting());

			radioBtnPerformRandomTestGeneration = new JRadioButton ("Perform Random Test Gen Based on CUT's Ports Protocols");
			radioBtnPerformRandomTestGeneration.setSelected(TestGenConfig.isRandomTesting());

			radioBtnPerformSequentialTestGeneration = new JRadioButton ("Perform Test Gen by Sequentially Exploring CUT");
			radioBtnPerformSequentialTestGeneration.setSelected(TestGenConfig.isSequentialTesting());
			
			chkStopOnFailureFound =  new JCheckBox ("Test Generation Stops Once First Failure-producing Test Found");
			chkStopOnFailureFound.setSelected(TestGenConfig.isStopOnFailureFound());
			
			chkReduceTestsBasedOnProp =  new JCheckBox ("Reduce Test Cases based on Properties");
			chkReduceTestsBasedOnProp.setEnabled(radioBtnPerformSymbolicTestGeneration.isSelected());
			chkReduceTestsBasedOnProp.setSelected(radioBtnPerformSymbolicTestGeneration.isSelected());
			TestGenConfig.setPropertyAwareTestGeneration(chkReduceTestsBasedOnProp.isSelected());
			
			//adding all buttons to the same group, so one is selected at a time
			grpOptions.add(radioBtnPerformRandomTestGeneration);
			grpOptions.add(radioBtnPerformSequentialTestGeneration);
			grpOptions.add(radioBtnPerformSymbolicTestGeneration);

			txtMaxTestLength = new JTextField();
			txtMaxTestLength.setText(Integer.toString(TestGenConfig.getTestMaxLength()));
			
			txtOutMsgFilter = new JTextField();
			txtOutMsgFilter.setText(TestGenConfig.getOutMsgFilter());

			txtTestSuitSize = new JTextField();
			txtTestSuitSize.setText(Integer.toString(TestGenConfig.getTestSuitLength()));
			
			txtParametersRanges = new JTextField();
			txtParametersRanges.setText(TestGenConfig.getParametersRanges());

			radioBtnPerformSymbolicTestGeneration.addItemListener(this);
			radioBtnPerformRandomTestGeneration.addItemListener(this);
			radioBtnPerformSequentialTestGeneration.addItemListener(this);

			// max test length listener
			txtMaxTestLength.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtMaxTestLength.getText() != "")
						TestGenConfig.setTestMaxLength(Integer.parseInt(txtMaxTestLength.getText()));
				}
			});
			lblMaxTestLength = new JLabel("Max Test Length: ");
			lblMaxTestLength.setLabelFor(txtMaxTestLength);

			// Out messages filter listener
			txtOutMsgFilter.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtOutMsgFilter.getText() != "")
						TestGenConfig.setOutMsgFilter(txtOutMsgFilter.getText());
				}
			});
			lblOutMsgFilter = new JLabel("Filter tests based on generated output message: ");
			lblOutMsgFilter.setLabelFor(txtOutMsgFilter);
			
			// test suit size listener
			txtTestSuitSize.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtTestSuitSize.getText() != "")
						TestGenConfig.setTestSuitLength(Integer.parseInt(txtTestSuitSize.getText()));
				}
			});
			lblTestSuitSize = new JLabel("Test Suit Size: ");
			lblTestSuitSize.setLabelFor(txtTestSuitSize);
			
			// test suit size listener
			txtParametersRanges.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (txtParametersRanges.getText() != "")
						TestGenConfig.setParameterRanges(txtParametersRanges.getText());
				}
			});
			lblParametersRanges = new JLabel("Parameter Value Ranges(param:range1,range2;): ");
			lblParametersRanges.setLabelFor(txtParametersRanges);

			// test generation button listener
			btnGenerateTests = new JButton("Generate tests");
			btnGenerateTests.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// display/center the jdialog when the button is pressed
					ConstructTestHarnessAction.TestGeneratorRunner.getInstance().generateTestSuit();
				}
			});
			
			//enable reduce test cases checkbox 
			radioBtnPerformSymbolicTestGeneration.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (radioBtnPerformSymbolicTestGeneration.isSelected()) {
						TestGenConfig.setPropertyAwareTestGeneration(true);
						chkReduceTestsBasedOnProp.setEnabled(true);
					}else
					{
						TestGenConfig.setPropertyAwareTestGeneration(false);
						chkReduceTestsBasedOnProp.setEnabled(false);
					}
				}
			});
			
			//disable reduce test cases checkbox 
			radioBtnPerformSequentialTestGeneration.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TestGenConfig.setPropertyAwareTestGeneration(false);
					chkReduceTestsBasedOnProp.setEnabled(false);
				}
			});

			//disable reduce test cases checkbox
			radioBtnPerformRandomTestGeneration.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TestGenConfig.setPropertyAwareTestGeneration(false);
					chkReduceTestsBasedOnProp.setEnabled(false);
				}
			});
			
			// disable reduce test cases checkbox
			chkReduceTestsBasedOnProp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TestGenConfig.setPropertyAwareTestGeneration(chkReduceTestsBasedOnProp.isSelected());
				}
			});

			// Put the check boxes in a column in a panel
			JPanel checkPanel = new JPanel(new GridLayout(0, 2));
			checkPanel.add(radioBtnPerformRandomTestGeneration);
			checkPanel.add(radioBtnPerformSequentialTestGeneration);
			checkPanel.add(radioBtnPerformSymbolicTestGeneration);
			//checkPanel.add(chkReduceTestsBasedOnProp);
			checkPanel.add(chkStopOnFailureFound);
			checkPanel.add(lblMaxTestLength);
			checkPanel.add(txtMaxTestLength);
			checkPanel.add(lblTestSuitSize);
			checkPanel.add(txtTestSuitSize);
			checkPanel.add(lblOutMsgFilter);
			checkPanel.add(txtOutMsgFilter);
			checkPanel.add(lblParametersRanges);
			checkPanel.add(txtParametersRanges);
			checkPanel.add(btnGenerateTests);
			
			//JPanel parametersPanel = new JPanel(new GridLayout(0, 1));
			
			
			add(checkPanel, BorderLayout.LINE_START);
			setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		}

		@SuppressWarnings("deprecation")
		@Override
		public void itemStateChanged(ItemEvent e) {
			Object source = e.getItemSelectable();

			if (source == radioBtnPerformSymbolicTestGeneration) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					order++;
					TestGenConfig.setSmartTesting(false);
					//JOptionPane.showMessageDialog(null, "DESELECTED: " + order);
				} else if (e.getStateChange() == ItemEvent.SELECTED) {
					order++;
					TestGenConfig.setSmartTesting(true);
					//JOptionPane.showMessageDialog(null, "SELECTED: " + order);
				}
			} else if (source == radioBtnPerformRandomTestGeneration) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					//JOptionPane.showMessageDialog(null, "DESELECTED");
					TestGenConfig.setRandomTesting(false);
				} else if (e.getStateChange() == ItemEvent.SELECTED) {
					//JOptionPane.showMessageDialog(null, "SELECTED");
					TestGenConfig.setRandomTesting(true);
				}
			} else if (source == radioBtnPerformSequentialTestGeneration) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					//JOptionPane.showMessageDialog(null, "DESELECTED");
					TestGenConfig.setSequentialTesting(false);
				} else if (e.getStateChange() == ItemEvent.SELECTED) {
					//JOptionPane.showMessageDialog(null, "SELECTED");
					TestGenConfig.setSequentialTesting(true);
				}
			}else if (source == chkStopOnFailureFound) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					//JOptionPane.showMessageDialog(null, "DESELECTED");
					TestGenConfig.setStopOnFailureFound(false);
				} else if (e.getStateChange() == ItemEvent.SELECTED) {
					//JOptionPane.showMessageDialog(null, "SELECTED");
					TestGenConfig.setStopOnFailureFound(true);
				}
			}
		}
	}

	private void createAndShowGUI() {
		// Create and set up the window.
//		JDialog frame = new JDialog("Configuring test case generation");
		JFrame frame = new JFrame("Configuring test case generation");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new OptionsPanel();
		newContentPane.setOpaque(true); // content panes must be opaque
		frame.setContentPane(newContentPane);
		
		//trigger textbox actionPerformed
//		JButton b;
//		for (Component c: newContentPane.getComponents()){
//			if (c instanceof JButton){
//				b = (JButton)c;break;
//			}
//		}
//		frame.getRootPane().setDefaultButton(btnGenerateTests);
//		//adjust location
//		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
//		frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);

		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void startGUI() {
		// LocalCheckBoxes cbs = new LocalCheckBoxes();
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
