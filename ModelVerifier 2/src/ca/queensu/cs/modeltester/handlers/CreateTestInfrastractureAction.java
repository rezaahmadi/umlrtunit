package ca.queensu.cs.modeltester.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolConformance;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StringExpression;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.internal.impl.CollaborationImpl;

import ca.queensu.cs.modeltester.slicing.Slicer;
import ca.queensu.cs.modeltester.smarttesting.ITestGenerator;
import ca.queensu.cs.modeltester.smarttesting.RandomTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SequentialTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.SmartTestGenerator;
import ca.queensu.cs.modeltester.smarttesting.TestData;
import ca.queensu.cs.modeltester.smarttesting.TestGenConfig;
import parser.absconparseur.components.PSoftRelation;
import symbolic.execution.ffsm.engine.concrete.ConcretePath;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import ca.queensu.cs.modeltester.utils.OpaqueBehaviourPoint;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import org.eclipse.papyrusrt.umlrt.uml.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.DateTime;

/**
 * @author reza implements and extends both interface and base cases to allow
 *         user generating test cases using popup menu or using toolbar. ToDo:
 *         AbstractHandler.execute is not implemented yet.
 */
public class CreateTestInfrastractureAction extends AbstractHandler implements IWorkbenchWindowActionDelegate {

	private static Class selectedCapsule;
	private static Class cut;
	private static Class th;
	private static Class tp;
	
	private static IWorkbenchWindow window;

	 
	@Override
	public void run(IAction arg0) {
		if (getSelectedCapsule() != null) {
			// StateMachineSlicer.testDD(getSelectedCapsule());
			System.out.println("making the selected capsule testable");

			StateMachine smCUT = UmlrtUtil.getStateMachine(cut);
			 Object[] allVertexes = smCUT.getRegions().get(0).getSubvertices().toArray();
			 Object[] allTransitions = smCUT.getRegions().get(0).getTransitions().toArray();

			// for every test input
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(smCUT);
			domain.getCommandStack().execute(new RecordingCommand(domain) {
				@Override
				protected void doExecute() {
					
					//adding TestData port on the CUT
					Port port1 = UmlrtUtil.getPort(th, "TestData");
					Port newPort1 = cut.createOwnedPort(port1.getName(), port1.getType());
					newPort1.applyStereotype(port1.getAppliedStereotypes().get(0));
					newPort1.setIsConjugated(!port1.isConjugated());
					newPort1.setIsConjugated(!port1.isConjugated());
					newPort1.setIsBehavior(true); //this is not a relay port
					
					//adding PropertyControl port on the TP
					Port port2 = UmlrtUtil.getPort(th, "PropertyControl");
					Port newPort2 = tp.createOwnedPort(port2.getName(), port2.getType());
					newPort2.applyStereotype(port2.getAppliedStereotypes().get(0));
					newPort2.setIsConjugated(!port2.isConjugated());
					newPort2.setIsBehavior(true); //this is not a relay port
					

					Region stateMachineRegion = smCUT.getRegions().get(0);
					
					Vertex initialState = UmlrtUtil.getInitialState(smCUT);
					
					// testable state
					Vertex newState = UmlrtUtil.createNewVertex("TESTABLE", stateMachineRegion, UmlrtUtil.getOneStableState(smCUT));
					State testableState = (State) newState;


					// adding the main state machine into the TESTABLE state
				    Region testableRegion = testableState.createRegion("regionTestable");
					// Apply RTRegion stereotype
					EObject rtRegion = (EObject) stateMachineRegion.getAppliedStereotypes().get(0);
					if (rtRegion instanceof Stereotype)
						testableRegion.applyStereotype((Stereotype) rtRegion);
					testableState.getRegions().add(testableRegion);

					// adding the vertex/transitions of the main state machine to TESTABLE
					for (Object v:allVertexes) {
						testableRegion.getSubvertices().add((Vertex)v);
					}
					
					for (Object t:allTransitions) {
						testableRegion.getTransitions().add((Transition)t);
					}
					
					//adding transition from the new initial to Testable
					Vertex initialStateNew = UmlrtUtil.createNewVertex("initMAIN", stateMachineRegion, initialState);
					Transition initToTestableTrans = stateMachineRegion.createTransition("toTestable");
					initToTestableTrans.setSource(initialStateNew);
					initToTestableTrans.setTarget(testableState);
					stateMachineRegion.getTransitions().add(initToTestableTrans);
					
					
					//adding self transition on Testable
					Transition setUpTestTrans = stateMachineRegion.createTransition("setupTest");
					setUpTestTrans.setSource(testableState);
					setUpTestTrans.setTarget(testableState);
					Trigger trigNew = UmlrtUtil.createTrigger(cut, "TestData", "setUpTest");
					setUpTestTrans.getTriggers().add(trigNew);
					stateMachineRegion.getTransitions().add(setUpTestTrans);
					
					
					//adding nextTest action code
					UmlrtUtil.addOpaqueBehaviourToState(testableState, "TestData.nextTest().send();\n",
							OpaqueBehaviourPoint.OnEntry);
					
					//adding nextState action code to each stable state
					//the stable state must need a trigger
					List<Vertex> res = new ArrayList<Vertex>();
					UmlrtUtil.getAllVertexes(res, testableRegion);
					for (Vertex v : res) {
						if (v instanceof State && UmlrtUtil.outGoingsNeedTrigger(v)) {
							UmlrtUtil.addOpaqueBehaviourToState(v, "TestData.nextState().send();\n",
									OpaqueBehaviourPoint.OnEntry);
						}
					}
					

					
					// creating the connections between TH and other parts
					for (Port portTH : th.getOwnedPorts()) {
						Port portCUT = cut.getOwnedPort(portTH.getName(), portTH.getType());
						if (portCUT!=null)
							UmlrtUtil.createConnector(selectedCapsule, UmlrtUtil.getPartByStereotypeName(selectedCapsule, "TestHarness"), UmlrtUtil.getPartByStereotypeName(selectedCapsule, "CapsuleUnderTest"), portTH, portCUT);
						
						List<Port> trrr = tp.getOwnedPorts();
						Port portTP = tp.getOwnedPort(portTH.getName(), portTH.getType());
						if (portTP!=null)
							UmlrtUtil.createConnector(selectedCapsule, UmlrtUtil.getPartByStereotypeName(selectedCapsule, "TestHarness"), UmlrtUtil.getPartByStereotypeName(selectedCapsule, "TestProperty"), portTH, portTP);
					}
					// creating the connections between CUT and TP
					for (Port portCUT : cut.getOwnedPorts()) {
						Port portTP = tp.getOwnedPort(portCUT.getName(), portCUT.getType());
						if (portTP!=null)
							UmlrtUtil.createConnector(selectedCapsule, UmlrtUtil.getPartByStereotypeName(selectedCapsule, "CapsuleUnderTest"), UmlrtUtil.getPartByStereotypeName(selectedCapsule, "TestProperty"), portCUT, portTP);
					}

				}// doExecute

			});
			
			System.out.println("capsule made testable!!!");
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}
		IStructuredSelection structured = (IStructuredSelection) selection;

		Object selectedObject = structured.getFirstElement();

		if (selectedObject instanceof IAdaptable) {
			IAdaptable adaptableObject = (IAdaptable) selectedObject;
			setSelectedCapsule(UmlrtUtil.selectIfCapsule((EObject) adaptableObject.getAdapter(EObject.class)));
			cut = UmlrtUtil.getCapsuleByStereotypeName(getSelectedCapsule(), "CapsuleUnderTest");
			th = UmlrtUtil.getCapsuleByStereotypeName(getSelectedCapsule(), "TestHarness");
			tp = UmlrtUtil.getCapsuleByStereotypeName(getSelectedCapsule(), "TestProperty");
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void init(IWorkbenchWindow w) {
		this.window = w;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	public static Class getSelectedCapsule() {
		return selectedCapsule;
	}

	public static void setSelectedCapsule(Class selectedCapsule) {
		CreateTestInfrastractureAction.selectedCapsule = selectedCapsule;
	}

}
