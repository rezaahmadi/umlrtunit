package ca.queensu.cs.modeltester.handlers;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import javax.swing.tree.DefaultMutableTreeNode;

import ca.queensu.cs.modeltester.smarttesting.*;

import java.util.*;

/**
 * @author reza
 *
 */

public class TestSuitPanel extends JFrame {
	private JTree treeTestSuit;
	
	public JTree getTree() {
		return treeTestSuit;
	}
	
//	private CheckboxTree treeTestSuit;
	private void generateTests(List<TestData> testSuit){
		DefaultMutableTreeNode nodeRoot = new DefaultMutableTreeNode("Test Suit");
		for (TestData test: testSuit){
			DefaultMutableTreeNode nodeTestName = new DefaultMutableTreeNode(test.getCode());
			DefaultMutableTreeNode nodeTestInputs = new DefaultMutableTreeNode(test.toString());
			
			//adding the test name to the root
			nodeTestName.add(nodeTestInputs);
			
			//adding test inputs under test name
			nodeRoot.add(nodeTestName);
		}
		//JTree that includes test suit
		treeTestSuit = new JTree(nodeRoot);
		
		//popup
		treeTestSuit.addMouseListener(new TestSuitPopupClickLister());

		//adding tree to the upper panel
		JPanel pnlTreeContainer = new JPanel();
		pnlTreeContainer.setLayout(new GridLayout(1,2));
		pnlTreeContainer.add(treeTestSuit);
		
		//add a button for adding generated tests into Test driver
//		JButton btnAddTestsToDriver = new JButton("Inject Test Suit in Test Driver");
//		btnAddTestsToDriver.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				//inject input signals as a string of states and transitions to the TestDriver state machine
//				TestGenerationAction.TestGeneratorRunner.getInstance().injectTestSuitInTestDriver();
//			}
//		});
//		JPanel pnlAddTestsBtn = new JPanel();
//		pnlAddTestsBtn.add(btnAddTestsToDriver);
		

		//adding a scrollbar panel
		JScrollPane pnlScroll = new JScrollPane(pnlTreeContainer);
		pnlScroll.setPreferredSize(new Dimension (800,600));
//		JScrollPane scrPane = new JScrollPane(pnlTreeContainer);
//		scrPane.setPreferredSize(new Dimension(450, 110));

//		JPanel pnlMain = new JPanel();
//		pnlMain.setLayout(new GridLayout(1, 5));
		
		//insert both panels on left and right side of the main panel
//		pnlMain.add(scrPane);
//		pnlMain.add(pnlAddTestsBtn);

		//add the scrollbar panel to the frame
//		add(pnlMain); 
		add(pnlScroll);
			
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		String testTechnique = "";
		if (TestGenConfig.isSequentialTesting())
			testTechnique = "Sequential";
		if (TestGenConfig.isRandomTesting())
			testTechnique = "Random";
		if (TestGenConfig.isPropertyAwareTestGeneration())
			testTechnique = "Symbolic Execution";
		this.setTitle(String.format("Generated test suite ( %s Technique)", testTechnique));

		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void showTestSuitTree(List<TestData> testSuit){
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                generateTests(testSuit);
            }
        });
	}

}

class PopupActionListener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		JMenuItem source = (JMenuItem) e.getSource();
		switch (source.getActionCommand()) {
			case "inject": {
				// inject input signals as a string of states and transitions to the TestDriver
				// state machine
				long t1 = System.currentTimeMillis();
				ConstructTestHarnessAction.TestGeneratorRunner.getInstance().injectTestSuitInTestHarness();
				long t2 = System.currentTimeMillis();
				System.out.println(String.format("time to inject test suite state machine: %sms", t2-t1));

				//message box
				JOptionPane.showMessageDialog(null, "Generated Test Suit Injected into Test Harness!", "Generating Test Suit State-machine " , JOptionPane.INFORMATION_MESSAGE);
			
			};break;
			
			case "select": {
				// inject input signals as a string of states and transitions to the TestDriver
				// state machine
//				TestGenerationAction.TestGeneratorRunner.getInstance().injectTestSuitInTestDriver();
			};break;
			
			case "unselect": {
				// inject input signals as a string of states and transitions to the TestDriver
				// state machine
//				TestGenerationAction.TestGeneratorRunner.getInstance().injectTestSuitInTestDriver();
			};break;
		}
	}
}


class TestSuitPopup extends JPopupMenu {
	
//	public static Map<JMenuItem, Integer> mpItems = new HashMap<JMenuItem, Integer>();
    JMenuItem itmInjectTestSuit;
    JMenuItem itmSelectAll;
    JMenuItem itmUnselectAll;
    public TestSuitPopup(){
    	
    	PopupActionListener actionListener = new PopupActionListener();
    	
        itmInjectTestSuit = new JMenuItem("Inject Test Suit in Test Harness");
        itmInjectTestSuit.setActionCommand("inject");
        itmInjectTestSuit.addActionListener(actionListener);
        
        itmSelectAll = new JMenuItem("Select All");
        itmSelectAll.setActionCommand("select");
        itmSelectAll.addActionListener(actionListener);
        
        itmUnselectAll = new JMenuItem("Unselect All");
        itmUnselectAll.setActionCommand("unselect");
        itmUnselectAll.addActionListener(actionListener);
        
        //add as menu items
        add(itmInjectTestSuit);
        add(itmSelectAll);
        add(itmUnselectAll);
        
        //add into map as well
//        mpItems.put(itmInjectTestSuit,1);
//        mpItems.put(itmSelectAll,2);
//        mpItems.put(itmUnselectAll,3);
    }
    
	
}

class TestSuitPopupClickLister extends MouseAdapter {
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
        TestSuitPopup menu = new TestSuitPopup();
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}

