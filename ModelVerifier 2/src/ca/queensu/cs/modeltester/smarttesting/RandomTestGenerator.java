package ca.queensu.cs.modeltester.smarttesting;

import java.util.*;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Parameter;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import ca.queensu.cs.modeltester.utils.UmlrtUtil;

/**
 * @author reza
 *
 */
public class RandomTestGenerator implements ITestGenerator {

 	private static List<TestData> testSuit;
	private static ITestGenerator instance;
	private static Class selectedCapsule;
	private static IWorkbenchWindow window;
	private static List<Port> capsulePorts;
	private TestGenConfig conf;
	private Map.Entry<Integer, Integer> intRang;
	private Map.Entry<Byte, Byte> charRange;
	private String charList = "*#0123456789";

	private int testNum = 0;
	
//	public static ITestGenerator getInstance(IWorkbenchWindow w, Class capsule, TestConfig conf) {
//		if (instance == null){
//			instance = new RandomTestGenerator(conf);
//		}
//		instance.setConf(conf);
//		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
//		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
//		window = w;
//		return instance;
//	}
	
	public RandomTestGenerator(IWorkbenchWindow w, Class capsule, TestGenConfig conf) {
		// TODO Auto-generated constructor stub
		setConf(conf);
		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
		window = w;
	}
	
	public RandomTestGenerator(IWorkbenchWindow w, Class capsule) {
		// TODO Auto-generated constructor stub
		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
		window = w;
	}
	
	@Override
	public void setConf(TestGenConfig conf) {
		// TODO Auto-generated method stub
		this.conf = conf;
	}

	@Override
	public TestGenConfig getConf() {
		// TODO Auto-generated method stub
		return conf;
	}

	@Override
	public List<TestData> generateTestInputs(Class topTestingCapsule) {
		int testNum = 0;
		ThreadLocalRandom rand = ThreadLocalRandom.current();
		//Random randomGen = new Random();
		List<InputMessage> allInputMessages = new ArrayList<InputMessage>();
		//TestData newTest = new TestData();
		//List<String> triggers = new ArrayList<String>();
		// extracting port triggers
		for (Port p : capsulePorts) {
			//we disclude TestData port signals in input signals
			if (p.getName().equals("TestData"))
				continue;
			Collaboration coll = (Collaboration) p.getType();
			if (!(p.isConjugated())) {
				for (Interface intr : coll.allRealizedInterfaces())
					for (Operation op: intr.getOperations()){
						InputMessage inputMessage = new InputMessage();
						//List<Object> params = new ArrayList<Object>();
						for (Parameter param: op.getOwnedParameters()) {
							if (param.getType().getName().equals("char"))
							{
								inputMessage.getParamters().add(new InputParameter(param.getName(),"char"));
							} else if (param.getType().getName().equals("Integer")) {
								inputMessage.getParamters().add(new InputParameter(param.getName(),"Integer"));
							}else
							{
								for (Element elem: param.getType().getOwnedElements()) {
									if (elem instanceof Property) 
										inputMessage.getParamters().add(new InputParameter(( (Property) elem).getName(),
												( (Property) elem).getType().getName(),param.getType()));
									
								}
							}
						}
						inputMessage.setName(op.getName());
						inputMessage.setPort(p.getName());
						allInputMessages.add(inputMessage);
						//triggers.add(String.format("%s.%s", p.getName(), op.getName()));
					}
			} else {
				for (Interface intr : coll.allUsedInterfaces())
					for (Operation op: intr.getOperations()){
						InputMessage inputMessage = new InputMessage();
						//List<Object> params = new ArrayList<Object>();
						for (Parameter param: op.getOwnedParameters()) {
							if (param.getType().getName().equals("char"))
							{
								//int charAt = rand.nextInt(0, charList.length()-1);
								//charList.charAt(charAt)
								inputMessage.getParamters().add(new InputParameter(param.getName(),"char"));
							}else if (param.getType().getName().equals("Integer")) {
								// int charAt = rand.nextInt(0, charList.length()-1);
								// inputMessage.getParamters().add(charList.charAt(charAt));
								// int charAt = rand.nextInt(0, charList.length()-1);
								// charList.charAt(charAt)
								inputMessage.getParamters().add(new InputParameter(param.getName(),"Integer"));
							}else
							{
								for (Element elem: param.getType().getOwnedElements()) {
									if (elem instanceof Property) 
										inputMessage.getParamters().add(new InputParameter(( (Property) elem).getName(),
												( (Property) elem).getType().getName(),param.getType()));
									
								}
							}
						}
						inputMessage.setName(op.getName());
						inputMessage.setPort(p.getName());
						allInputMessages.add(inputMessage);
						//triggers.add(String.format("%s.%s", p.getName(), op.getName()));
					}
			}
		}

		// generating tests
		testSuit = new ArrayList<TestData>();
		while (TestGenConfig.getTestSuitLength() > testSuit.size()) {
			TestData newTest = new TestData();
			newTest.setCode(testNum);
			while (newTest.getInputMessages().size() < TestGenConfig.getTestMaxLength()) {
				// randomely pick a trigger
				int msgIndex = rand.nextInt(0, allInputMessages.size() - 1);
				// int msgIndex = rand.nextInt((allInputMessages.size() - 1) - 0) + 0;
				// deep copy
				InputMessage msg = new InputMessage();
				msg.setName(allInputMessages.get(msgIndex).getName());
				msg.setPort(allInputMessages.get(msgIndex).getPort());
				for (InputParameter ip : allInputMessages.get(msgIndex).getParamters()) {
					InputParameter newIp = new InputParameter(ip.getName(), ip.getType(),ip.getComplexType());

					// ToDo: fix this BS, replace it with ParameterRanges from TestGenConfig
					if (ip.getType().equals("char")) {
						int randIndex = rand.nextInt(0, charList.length() - 1);
						newIp.setValue(charList.charAt(randIndex));
					} else if (ip.getType().equals("Integer")) {

						List<InputParameterValueRanges> foundRanges = TestGenConfig.parameterRangesMap.stream()
								.filter(p -> p.getInputParameterName().equals(newIp.getName()))
								.collect(Collectors.toList());
						if (foundRanges != null && foundRanges.size() > 0) {
							int randValue = rand.nextInt((int) foundRanges.get(0).getMin(),
									(int) foundRanges.get(0).getMax());
							newIp.setValue(randValue);
						}
					}
					msg.getParamters().add(newIp);
				}
				newTest.getInputMessages().add(msg);
			}
			testSuit.add(newTest);
			testNum++;
		}
		return testSuit;
	}
	

	@Override
	public void generateTestProperties(Class topTestingCapsule, Class propertyCapsule) {
		// TODO Auto-generated method stub
		
	}

}
