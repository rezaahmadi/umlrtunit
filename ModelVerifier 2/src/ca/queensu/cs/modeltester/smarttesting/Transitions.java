package ca.queensu.cs.modeltester.smarttesting;
import java.util.*;

import org.eclipse.uml2.uml.Transition;

/**
 * @author reza
 *
 */
public class Transitions extends ArrayList<Transition> {
	public List<Transition> getBySourceState(String stateName) {
		List<Transition> res = new ArrayList<Transition>();
		while (this.iterator().hasNext()) {
			Transition t = this.iterator().next();
			if (t.getSource().getName().equals(stateName))
				res.add(t);
		}
		return res;
	}
}
