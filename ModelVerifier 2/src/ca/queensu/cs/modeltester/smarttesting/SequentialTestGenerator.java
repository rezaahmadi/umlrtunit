/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;

import java.util.*;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Vertex;
import choco.kernel.solver.goals.solver.ChoicePoint;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;

/**
 * @author reza
 *
 */
public class SequentialTestGenerator implements ITestGenerator {
 	private static List<TestData> testSuit;
	private static ITestGenerator instance;
	private static Class selectedCapsule;
	private static IWorkbenchWindow window;
	private TestGenConfig conf;
	public TestGenConfig getConf() {
		return conf;
	}

	@Override
	public void setConf(TestGenConfig conf) {
		this.conf = conf;
	}

	private int testNum = 0;
	
//	public static ITestGenerator getInstance(IWorkbenchWindow w, Class capsule, TestConfig conf) {
//		if (instance == null){
//			instance = new SmartTestGenerator(conf);
//		}
//		instance.setConf(conf);
//		selectedCapsule = capsule;
//		window = w;
//		return instance;
//	}
	
	public SequentialTestGenerator(IWorkbenchWindow w, Class capsule, TestGenConfig conf) {
		// TODO Auto-generated constructor stub
		setConf(conf);
		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
//		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
		window = w;
	}
	
	public SequentialTestGenerator(IWorkbenchWindow w, Class capsule) {
		// TODO Auto-generated constructor stub
		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
//		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
		window = w;
	}
	
	@Override
	public List<TestData> generateTestInputs(Class cut) {
		StateMachine stateMachine = UmlrtUtil.getStateMachine(cut);
		testSuit = new ArrayList<TestData>();
		testNum = 0;
		//generating state machine input signals starting from first vertex (not initial)
	    //getStatemachineInputSignals(new TestData(), UmlrtUtil.getInitialState (stateMachine).getOutgoings().get(0).getTarget());
	    getStatemachineInputSignals(new TestData(), UmlrtUtil.getTestableState(stateMachine));
	    return testSuit;
	}

	private void printTest(TestData test){
		//printing tests
		System.out.println(String.format("%d: %s", test.getCode(), test.toString()));
	}
	
	private void getStatemachineInputSignals(TestData test, Vertex initialState) {

//		if (testSuit.size() >= TestGenConfig.getTestSuitLength())
//			return;
		if (test.getInputs().size() > TestGenConfig.getTestMaxLength()) 
			return;
		
		//skip all Pseudostates including Entry_Points and Exist_Points and Choice_Points
		while((initialState instanceof Pseudostate)){
			Pseudostate ss = (Pseudostate) initialState;
			if (ss.getKind() == PseudostateKind.CHOICE_LITERAL)
				break;
			initialState = initialState.getOutgoings().get(0).getTarget();
		}
		
		 
		 //if choice
		if (initialState instanceof Pseudostate) {
			Pseudostate ss = (Pseudostate) initialState;
			if (ss.getKind() == PseudostateKind.CHOICE_LITERAL) {
				for (Transition choice : ss.getOutgoings()) {
					getStatemachineInputSignals(test, choice.getTarget());
				}
			}
		}
		
		for (Transition trans : initialState.getOutgoings()) {
			for (Trigger trig : trans.getTriggers()) {
				// if test is still small enough in size
				// if (test.getInputs().size() <
				// TestGenConfig.getTestMaxLength()){

				TestData newTest = new TestData(-1, test.getInputs());
				// if (test.getInputs().size() <
				// TestGenConfig.getTestMaxLength()) {
				String portName = trig.getPorts().get(0).getName();
				String operation = ((CallEvent) (trig.getEvent())).getOperation().getName();

				// we disclude TestData port signals in input signals
				if (portName.equals("TestData"))
					continue;

				// do not add transitions with no trigger in the test
				// input
				if (operation.equals(""))
					continue;

				// test.getInputs().add(String.format("%s.%s", portName,
				// operation));

				// if not enough tests have been generated
				newTest.getInputs().add(String.format("%s.%s", portName, operation));
				// newTest.setName(test.getName());
				// for (String input: test.getInputs()){
				// newTest.getInputs().add(input);
				// }

				getStatemachineInputSignals(new TestData(newTest.getCode(), newTest.getInputs()), trans.getTarget());
				// getStatemachineInputSignals(newTest,
				// trans.getTarget());
				// }
				
				if (testSuit.size() < TestGenConfig.getTestSuitLength()) {
					boolean exists = false;
					test.setCode(testNum);
					printTest(test);
					if (test.getInputs().size() > 0) {
						for (TestData t : testSuit) {
							if (t.getInputs().toString().equals(test.getInputs().toString()))
								exists = true;
						}
						if (!exists) {
							testSuit.add(new TestData(test.getCode(), test.getInputs()));
							testNum++;
						}
					}
				}
				
//				getStatemachineInputSignals(new TestData(newTest.getName(), newTest.getInputs()), trans.getTarget());
			}
			// }
		}
	}
	
	private void getStatemachineInputSignals__old(TestData test, Vertex initialState) {

//		if (testSuit.size() >= TestGenConfig.getTestSuitLength())
//			return;
		if (test.getInputs().size() > TestGenConfig.getTestMaxLength()) 
			return;
		
		//skip all Pseudostates including Entry_Points and Exist_Points and Choice_Points
		while((initialState instanceof Pseudostate)){
			Pseudostate ss = (Pseudostate) initialState;
			if (ss.getKind() == PseudostateKind.CHOICE_LITERAL)
				break;
			initialState = initialState.getOutgoings().get(0).getTarget();
		}
		
		 
		 //if choice
		if (initialState instanceof Pseudostate) {
			Pseudostate ss = (Pseudostate) initialState;
			if (ss.getKind() == PseudostateKind.CHOICE_LITERAL) {
				for (Transition choice : ss.getOutgoings()) {
					getStatemachineInputSignals(test, choice.getTarget());
				}
			}
		}
		
		for (Transition trans : initialState.getOutgoings()) {
			for (Trigger trig : trans.getTriggers()) {
				// if test is still small enough in size
				// if (test.getInputs().size() <
				// TestGenConfig.getTestMaxLength()){

				TestData newTest = new TestData(-1, test.getInputs());
				// if (test.getInputs().size() <
				// TestGenConfig.getTestMaxLength()) {
				String portName = trig.getPorts().get(0).getName();
				String operation = ((CallEvent) (trig.getEvent())).getOperation().getName();

				// we disclude TestData port signals in input signals
				if (portName.equals("TestData"))
					continue;

				// do not add transitions with no trigger in the test
				// input
				if (operation.equals(""))
					continue;

				// test.getInputs().add(String.format("%s.%s", portName,
				// operation));

				// if not enough tests have been generated
				newTest.getInputs().add(String.format("%s.%s", portName, operation));
				// newTest.setName(test.getName());
				// for (String input: test.getInputs()){
				// newTest.getInputs().add(input);
				// }

				getStatemachineInputSignals(new TestData(newTest.getCode(), newTest.getInputs()), trans.getTarget());
				// getStatemachineInputSignals(newTest,
				// trans.getTarget());
				// }
				
				if (testSuit.size() < TestGenConfig.getTestSuitLength()) {
					boolean exists = false;
					test.setCode(testNum);
					printTest(test);
					if (test.getInputs().size() > 0) {
						for (TestData t : testSuit) {
							if (t.getInputs().toString().equals(test.getInputs().toString()))
								exists = true;
						}
						if (!exists) {
							testSuit.add(new TestData(test.getCode(), test.getInputs()));
							testNum++;
						}
					}
				}
				
//				getStatemachineInputSignals(new TestData(newTest.getName(), newTest.getInputs()), trans.getTarget());
			}
			// }
		}
	}

	@Override
	public void generateTestProperties(Class topTestingCapsule, Class propertyCapsule) {
		// TODO Auto-generated method stub
		
	}
}