package ca.queensu.cs.modeltester.smarttesting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Vertex;

import ca.queensu.cs.modeltester.slicing.Slicer;
import ca.queensu.cs.modeltester.utils.FileUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import symbolic.execution.ffsm.engine.concrete.ConcretePath;
import symbolic.execution.ffsm.engine.concrete.ExecutionTransition;
import symbolic.execution.ffsm.engine.symbolic.ActionInputSymbolic;
import symbolic.execution.ffsm.engine.symbolic.ArcSymbolic;
import symbolic.execution.ffsm.engine.symbolic.ExecutionOptions;
import symbolic.execution.ffsm.engine.symbolic.NodeSymbolic;
import symbolic.execution.ffsm.engine.symbolic.OutputSequenceSymbolic;
import symbolic.execution.ffsm.engine.symbolic.SymbolicExecutionTree;
import symbolic.execution.ffsm.engine.symbolic.SymbolicPath;
import symbolic.execution.ffsm.engine.symbolic.SymbolicState;
import symbolic.execution.ffsm.engine.symbolic.SymbolicTransition;
import symbolic.execution.ffsm.engine.tree.Arc;
import symbolic.execution.ffsm.model.ActionInput;
import symbolic.execution.ffsm.model.ActionOutput;
import symbolic.execution.ffsm.model.ActionOutputInstance;
import symbolic.execution.ffsm.model.ActionVariableOutput;
import symbolic.execution.ffsm.model.Location;
import symbolic.execution.umlrt.actions.SymbolicExecutionAction;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import ca.queensu.cs.modelverifier.slicing.data.Criteria;

/**
 * @author reza
 *
 */
public class SmartTestGenerator implements ITestGenerator {
	private SymbolicExecutionAction sea;
//	private SymbolicExecutionTree reducedSet;
	private static IWorkbenchWindow window;
	private static ITestGenerator instance;
	private Map<String, Object> tmplInputs = null;
	private static Class selectedCapsule;
	private static Class propertyCapsule;
	private static StateMachine propertyCapsuleSm;
	
	//ToDo: make this more robust by attaching a stereotype to the states
	private static final String FAILURE_STATE = "PROP_VIOLATION";
	private static final String INITIAL_STATE = "PROP_INIT";
	
//	private Vertex propertyCurrState;
	private Configuration cfg = null;
	
	private TestGenConfig conf;
	public TestGenConfig getConf() {
		return conf;
	}
	@Override
	public void setConf(TestGenConfig conf) {
		this.conf = conf;
	}

	private int genC = 0;
	
	public SmartTestGenerator() {
		sea = new SymbolicExecutionAction();

		//configuring the code generator		
		cfg = new Configuration(Configuration.VERSION_2_3_25);
//		cfg.setClassForTemplateLoading(Generator.class, "testtemplates");
		cfg.setIncompatibleImprovements(new Version(2, 3, 20));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setLocale(Locale.US);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

	}

//	public static ITestGenerator getInstance(IWorkbenchWindow w, Class capsule) {
//		if (instance == null){
//			instance = new SymbolicTestGenerator();
//		}
//		selectedCapsule = capsule;
//		window = w;
//		return instance;
//	}
	
	public SmartTestGenerator(IWorkbenchWindow w, Class capsule, TestGenConfig conf) {
		// TODO Auto-generated constructor stub
		this (w, capsule);
		setConf(conf);
	}
	
	public SmartTestGenerator(IWorkbenchWindow w, Class capsule) {
		// TODO Auto-generated constructor stub
		//setConf(conf);
		sea = new SymbolicExecutionAction();
		selectedCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleUnderTest");
		propertyCapsule = UmlrtUtil.getCapsuleByStereotypeName(capsule, "CapsuleTestProperty");
		propertyCapsuleSm = (StateMachine)propertyCapsule.getOwnedBehaviors().get(0);
//		resetPropertyState();
//		capsulePorts = UmlrtUtil.getPorts(selectedCapsule);
		window = w;
	}
	
	private SymbolicExecutionTree generateSet(Class capsule) {
		System.out.println(String.format("Creating SET for %s capsule..", capsule.getName()));
		//to remove Error transitions 
		ExecutionOptions.setNewExecution(false);
		ExecutionOptions.setMaxDepth(TestGenConfig.getTestMaxLength());
		sea.init(window);
		sea.setModular(true);
		sea.setGenerateOnlyOpen(true);
//		sea.setCapsule(capsule);
		return sea.run4TestGen(capsule);
	}
	
	public List<ConcretePath> generateConcretePaths(Class topTestingCapsule) {
		System.out.println(String.format("generating tests for %s capsule..", topTestingCapsule.getName()));
		List<ConcretePath> tests = new ArrayList<ConcretePath>();
		//printing test inputs
		
		//getting the original capsule from the model
		//the original capsule includes no Testing-related features
		//String fix = TestGenConfig.isPropertyAwareTestGeneration()?"_REDUCED" :  "_TESTING";
		for (PackageableElement elem: topTestingCapsule.getModel().getPackagedElements())
		{
			if (elem instanceof Class && elem.getName().equals(topTestingCapsule.getName().replace("_TESTING", "")))
			{
				System.out.println("elemt::" + elem.getName());
				topTestingCapsule = (Class)elem;
				List<SymbolicPath> allPaths = null;
				
				//if reducing the test cases based on a property
				if (TestGenConfig.isPropertyAwareTestGeneration()) {
		
					Criteria criteria= new Criteria();
					//ToDo fill criteria, use below
					//extracting criteria
//					for (Transition t: criteriaSm.getRegions().get(0).getTransitions()) {
//						//triggers
//						if (!t.getTriggers().get(0).getName().equals("timeout"))
//							criteria.add(t.getTriggers().get(0).getName());
//					}
					
					SymbolicExecutionTree set = generateSet(Slicer.slice(topTestingCapsule, criteria));
//					SymbolicExecutionTree set = generateSet(topTestingCapsule);
					allPaths = set.exploreAllPaths();
					//allPaths = reduceSetBasedOnProp(generateSet(topTestingCapsule).getRoot());
					System.out.println("printing test cases..");
				}
				else //no path reduction
				{
					SymbolicExecutionTree set = generateSet(topTestingCapsule);
					System.out.println("printing test cases..");
					allPaths = set.exploreAllPaths();
				}

//				List<SymbolicPath> allPaths = set.exploreAllPathsByDepth();
//				List<SymbolicPath> allPaths = set.exploreAllPaths();
//				List<SymbolicPath> allPaths = reducedSet.exploreAllPaths();
				int c = 1;
				for (SymbolicPath path : allPaths) {
					try {
						ConcretePath test = path.solve();
						tests.add(test);
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
				break;
			}
		}
	 
		return tests;
	}
 
//	public List<SymbolicPath> generateSymbolicPaths(Class topTestingCapsule) {
//		System.out.println(String.format("generating tests for %s capsule..", topTestingCapsule.getName()));
//		List<ConcretePath> tests = new ArrayList<ConcretePath>();
//		//printing test cases
//		SymbolicExecutionTree set = generateSET(topTestingCapsule);
//
//		List<SymbolicPath> allPaths = set.exploreAllPathsByDepth();
//		return allPaths;
//	}
	
	
	// we reduce the generated Symbolic Execution Tree to those path that are likely
	// to fail the Property
	private List<SymbolicPath> reduceSetBasedOnProp(NodeSymbolic root) {
		
		List<SymbolicPath> reducedSet = new ArrayList<SymbolicPath>();
		
		//the required series of CUT output actions that will put the property into Failure state
		Set<ActionOutput> requiredActionOutputs = new HashSet<ActionOutput>();
		List<Set<ActionOutput>> res = new ArrayList<Set<ActionOutput>>();

		getPropertyReachableToFailActionInputs(UmlrtUtil.getConnectionPoint(propertyCapsuleSm, "init_entry", PseudostateKind.ENTRY_POINT_LITERAL)
						.getOutgoings().get(0).getTarget(), requiredActionOutputs, res);
		
		//explore all path in the CUT that produces outputs that can put the property into Failure state
		//ToDo: fix this: we explore for signals in the CUT based on one reachable path in the property
		root.exploreForSignals(res.get(0), new SymbolicPath(), reducedSet);
		return reducedSet;
		
//		for (Arc<SymbolicTransition, SymbolicState> arc : node.getChildren()) {
//			for (ActionOutputInstance outputAction : arc.getContents().getOuputSequence().getSequence()) {
//
//				// if none of the output actions can put the Property into Success state
//				if (!putsPropertyIntoSuccessState(outputAction.getActionOutputName())) {
//					//ToDo: fix start from here
//					reduceSetBasedOnProp(arc);
//					
//				}else
//				{
//					resetPropertyState();
//				}
//			}
//		}
	}
	
//	private void resetPropertyState() {
//		propertyCurrState = propertyCapsuleSm.getRegions().get(0).getSubvertices().get(0);
//	}
	
	//ToDo: we assume that property is currently simple and each node does not have more than 2 outgoing transitions
//	private boolean putsPropertyIntoSuccessState(String actionOutputName) {
//		boolean res = false;
//		for (Transition trans: propertyCurrState.getOutgoings()) {
//
//			//if this action output can trigger a transition
//			if (trans.getTriggers().get(0).getEvent().getName().equals(actionOutputName)) {
//				
//				//if this action output can put property into SUCCESS state
//				if (trans.getTarget().getName().equals("SUCCESS")) {
//					res = true;
//				}
//				else {
//					propertyCurrState = trans.getTarget();
//					res = false;
//					break;
//				}
//			}
//		}//for
//		if (res) {
//			resetPropertyState();
//		}
//		return res;
//	}
	
	
	// we obtain the property's required input signals that transits the property
	// from Initial state to the Failure state
	private void getPropertyReachableToFailActionInputs(Vertex currentState, Set<ActionOutput> requiredActionOutputs, List<Set<ActionOutput>> res) {
		if (currentState.getName().equals(FAILURE_STATE)) {
			//to copy the set otherwise it would get updated in next executions
			Set<ActionOutput> actions = new HashSet<ActionOutput>();
			for (ActionOutput ao: requiredActionOutputs)
				actions.add(ao);
			res.add(actions);
			return;
		}
		for (Transition trans : currentState.getOutgoings()) {
			for (Trigger trig : trans.getTriggers()) {
				String portName = trig.getPorts().get(0).getName();
				String trigger = ((CallEvent) (trig.getEvent())).getOperation().getName();
				requiredActionOutputs.add(new ActionOutput(portName, trigger, new HashSet<ActionVariableOutput<?>>()));
				getPropertyReachableToFailActionInputs(trans.getTarget(), requiredActionOutputs, res);
			}
		}

	}
	
	//we obtain the property's required input signals that transits the property from
	//Initial state to the Failure state
	private Set<ActionInput> getPropertyReachableToFailActionInputsUsingSE() {

		SymbolicExecutionTree propertySet = generateSet(propertyCapsule);

		//property source and destination states
		Map<SymbolicExecutionTree,Location> source = new HashMap<SymbolicExecutionTree, Location>();
		Map<SymbolicExecutionTree,Location> target = new HashMap<SymbolicExecutionTree, Location>();
		Location locInit = new Location("INIT");
		Location locFail = new Location("FAILURE");
		source.put(propertySet, locInit);
		target.put(propertySet, locFail);

		//reachable path from source and target in the property
		List<SymbolicPath> reachablePaths = new ArrayList<SymbolicPath>();
		propertySet.getRoot().exploreReachable(source, target, new SymbolicPath(), reachablePaths);

		Set<ActionInput> res = new HashSet<ActionInput>(); 
		//ToDo: fix this. we assume that in a property there is only one reachable path from Initial to Failure states
		for (ArcSymbolic arc: reachablePaths.get(0).getPath()) {
			ActionInput ai = arc.getContents().getTransition().getInputAction();
			res.add(ai);
		}
		
		return res;
	}

		
	/* (non-Javadoc)
	 * @see ca.queensu.cs.modelverifier.smarttesting.ITestGenerator#generateTestInputs(org.eclipse.uml2.uml.Class)
	 */
	@Override
	public List<TestData> generateTestInputs (Class topTestingCapsule) {
        //template's inputs
		List<TestData> testInputs = new ArrayList<TestData>();
		
		//generate test cases
		List<ConcretePath> concretePaths = generateConcretePaths(topTestingCapsule);
		
		int c = 0;
		for (ConcretePath path: concretePaths){
			/*boolean test = true;
			//extract messages from conrete path
			List<String> messages = new ArrayList<String>();
			for (ExecutionTransition transition: path.getTransitions()){
				boolean action = false;
				String inputAction = transition.getInputAction().getInputAction().toString();
				inputAction+= String.format("(%s)", transition.getInputAction().getInputVariables().toString());
				inputAction+= String.format("->", transition.getTarget().toString());
				if (!inputAction.equals("default")) {
					if (!inputAction.contains(".")) //in case .timeout() was missing
						inputAction+=".timeout()";
					messages.add(inputAction);
				}
			}
			testInputs.add(new TestData(c, messages));
			*/
			List<String> messages = new ArrayList<String>();
			boolean isShown = true;
			String test = path.toString2();
			if (TestGenConfig.getOutMsgFilter()!=null && !TestGenConfig.getOutMsgFilter().isEmpty()) {
				if (!path.getTransitions().stream().anyMatch(testStep -> TestGenConfig.getOutMsgFilterList().stream()
						.allMatch(msg -> testStep.toString().contains(msg)))) {
					isShown = false;
				}
			}
			if (isShown == true){
				messages.add(test);
				testInputs.add(new TestData(c, messages));
				c++;
			}
		}
		
		System.out.println(String.format("%d test cases were generated.", c));
		
		return testInputs;
	}
	
	/* (non-Javadoc)
	 * @see ca.queensu.cs.modelverifier.smarttesting.ITestGenerator#generateTestProperties(org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class)
	 */
	//ToDo: this is an old impl. Should be REMOVED
	@Override
	public void generateTestProperties (Class topTestingCapsule, Class propertyCapsule) {

	}
	
	
//	public void generateJUnitTestCases (Class topTestingCapsule, Class testProp) {
//	
//		//loading templates
//		try {
//			cfg.setDirectoryForTemplateLoading(new File(String.format("%stesttemplates/", FileUtil.getTempDir())));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    
//        //template's inputs
//        tmplInputs = new HashMap<String, Object>();
//		
////		tmplInputs.clear();
//		//filling template inputs
//		tmplInputs.put("packageName", "ca.queensu.cs.modelverifier.smarttesting.testcases");
//		tmplInputs.put("propertyName", testProp.getName());
//		tmplInputs.put("capsule", topTestingCapsule.getName());
//
//
//		//generate test cases
//		List<ConcretePath> concretePaths = generateConcretePaths(topTestingCapsule);
//		//ToDo move this code to symbolic execution to prevent generating unrelated symbolic paths
//		List<String> propTriggers = UmlrtUtil.getTriggers(testProp);
//		
//		int c = 0;
//		for (ConcretePath path: concretePaths){
//			boolean test = true;
//			//extract messages from conrete path
//			List<String> messages = new ArrayList<String>();
//			for (ExecutionTransition transition: path.getTransitions()){
//				boolean action = false;
//				String inputAction = transition.getInputAction().getInputAction().toString();
//				//ToDo: use port names rather than removing them
//				if (inputAction!=null && inputAction.contains(".")){
//					inputAction = inputAction.split("\\.")[1];
//					if (!inputAction.equals("default"))
//					{
//						for (String msg: propTriggers)
//							if (inputAction.equals(msg)) {action = true;break;};
//						if (!action){
//							test = false;
//							break;
//						}
//						messages.add(inputAction);
//					}
//				}
//			}
//			if (!test)
//				continue;
//			tmplInputs.put("messages", messages);
//			c++;
//			tmplInputs.put("testNum", c);
//			String propNameFirstCapital = testProp.getName().substring(0, 1).toUpperCase() + testProp.getName().substring(1); 
//			generateEntity("TestCase.ftl", String.format("%sTest%s", propNameFirstCapital, c),
//					tmplInputs);
//		}
//		
//		System.out.println(String.format("%d test cases were generated.", c));
//	}
//	
	
//	private void generateEntity(String tmplName, Object entity, Map<String, Object> tmplInputs){
//        Writer fileWriter = null;
//        try {
//        	
//            //getting the template
//        	//URL url = GenerateCodeSample.class.getResource("helloworld.ftl");
//			Template template = cfg.getTemplate(tmplName);
//			
//			//writing the output to the console
////			Writer consoleWriter = new OutputStreamWriter(System.out);
////          template.process(tmplInputs, consoleWriter);
//            
//            //writing the output to a file
//			fileWriter = new FileWriter(new File(String.format("%s%s.java", FileUtil.getGenDir(), entity.toString())));
//            template.process(tmplInputs, fileWriter);
//            
//			if (fileWriter != null)
//				fileWriter.close();
//			
//			System.out.println(String.format("%s.java was generated.", entity.toString()));
//			genC++;
//			
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (TemplateException e) {
//			e.printStackTrace();
//		}
//
//	}

}
