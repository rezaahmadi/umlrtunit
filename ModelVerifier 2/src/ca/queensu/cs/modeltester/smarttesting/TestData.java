/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;

import java.util.List;
import java.util.ArrayList;

/**
 * @author reza
 *
 */
public class TestData {
	List<InputMessage> inputMessages;
	
	/**
	 * @return the inputMessages
	 */
	public List<InputMessage> getInputMessages() {
		return inputMessages;
	}

	/**
	 * @param inputMessages the inputMessages to set
	 */
	public void setInputMessages(List<InputMessage> inputMessages) {
		this.inputMessages = inputMessages;
	}

	//ToDo: This must be removed
	List<String> inputs = new ArrayList<String>();
	int code;

	public List<String> getInputs() {
		return inputs;
	}

	public void setInputs(List<String> inputs) {
		this.inputs = inputs;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public TestData() {
		inputMessages  = new ArrayList<InputMessage>();
	}
	
	public TestData(int code, List<String> inputs) {
		for (String input: inputs)
			this.inputs.add(input);
		this.code = code;
	}
	
	public String toString() {
		// return inputs.toString() ;
		StringBuilder sb = new StringBuilder();
		int c1 = 0;
		for (InputMessage im : inputMessages) {
			sb.append(String.format("%s.%s", im.getPort(), im.getName()));
			sb.append("(");
			int c2 = 0;
			//for primitive types
			for (InputParameter ip : im.getParamters()) {
				if (ip.getComplexType() == null) {
					if (ip.getValue() != null)
						sb.append(ip.getValueStr());
					c2++;
					if (c2 < im.getParamters().size())
						sb.append(", ");
				}
			}
			//for complex types
			//ToDo: implementation is BS fix this BS 
			String complexIp="{ "; 
			for (InputParameter ip : im.getParamters()) {
				if (ip.getComplexType() != null) {
					if (ip.getValue() != null)
						complexIp+=ip.getName() + "=" + ip.getValueStr() + ", ";
				}
			}
			 if (!complexIp.equals("{ "))
				 sb.append(complexIp.substring(0, complexIp.length()-2)+" }");

			sb.append(")");
			c1++;
			if (c1 < inputMessages.size())
				sb.append(", ");
		}
		return sb.toString();
	}
}
