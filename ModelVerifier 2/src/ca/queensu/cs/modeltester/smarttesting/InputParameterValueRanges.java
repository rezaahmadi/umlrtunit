/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;
import java.util.*;
/**
 * @author rezaahmadi
 *
 */
public class InputParameterValueRanges {
	/**
	 * 
	 */
	public InputParameterValueRanges() {
	}
	/**
	 * @param inputParameterName
	 * @param possibleValues
	 * @param min
	 * @param max
	 */
	public InputParameterValueRanges(String inputParameterName, List<Object> possibleValues, Object min, Object max) {
		this.inputParameterName = inputParameterName;
		this.possibleValues = possibleValues;
		this.min = min;
		this.max = max;
	}
	private String inputParameterName;
	/**
	 * @return the inputParameterName
	 */
	public String getInputParameterName() {
		return inputParameterName;
	}
	/**
	 * @param inputParameterName the inputParameterName to set
	 */
	public void setInputParameterName(String inputParameterName) {
		this.inputParameterName = inputParameterName;
	}
	/**
	 * @return the possibleValues
	 */
	public List<Object> getPossibleValues() {
		return possibleValues;
	}
	/**
	 * @param possibleValues the possibleValues to set
	 */
	public void setPossibleValues(List<Object> possibleValues) {
		this.possibleValues = possibleValues;
	}
	/**
	 * @return the min
	 */
	public Object getMin() {
		return min;
	}
	/**
	 * @param min the min to set
	 */
	public void setMin(Object min) {
		this.min = min;
	}
	/**
	 * @return the max
	 */
	public Object getMax() {
		return max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(Object max) {
		this.max = max;
	}
	private List<Object> possibleValues;
	private Object min;
	private Object max;
}
