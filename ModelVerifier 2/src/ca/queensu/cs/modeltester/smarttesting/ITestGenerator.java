package ca.queensu.cs.modeltester.smarttesting;

import java.util.HashMap;
import java.util.List;

import org.eclipse.uml2.uml.Class;

/**
 * @author reza
 *
 */
public interface ITestGenerator {
	public void setConf(TestGenConfig conf);
	public TestGenConfig getConf();
	List<TestData> generateTestInputs(Class topTestingCapsule);
	void generateTestProperties(Class topTestingCapsule, Class propertyCapsule);
}