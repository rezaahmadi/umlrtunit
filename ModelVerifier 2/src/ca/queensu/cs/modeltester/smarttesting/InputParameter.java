/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;

/**
 * @author rezaahmadi
 *
 */
public class InputParameter{
	private Object complexType;
	/**
	 * @return the complexType
	 */
	public Object getComplexType() {
		return complexType;
	}
	/**
	 * @param complexType the complexType to set
	 */
	public void setComplexType(Object complexType) {
		this.complexType = complexType;
	}
	public InputParameter(String name, String type) {
		this.type = type;
		this.name=name;
	}
	public InputParameter(String name, String type, Object ct) {
		this.type = type;
		this.name=name;
		setComplexType(ct);
	}
	private String type;
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	private String name;
	private Object value;
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValueStr() {
		if (value.getClass().equals(char.class) || value.getClass().getName().equals("java.lang.Character"))
			return String.format("'%s'", value);
		return value.toString();		
	}
}
