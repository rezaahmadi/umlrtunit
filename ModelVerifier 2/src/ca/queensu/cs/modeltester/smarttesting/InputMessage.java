/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;

import java.util.*;
import org.apache.commons.lang3.*;

/**
 * @author rezaahmadi
 *
 */
public class InputMessage {
	
	private String port;
	
	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}

	public InputMessage() {
		this.paramters  = new ArrayList<InputParameter>();
	}
	
	/**
	 * @param paramters
	 * @param name
	 */
	public InputMessage(List<InputParameter> paramters, String name) {
		this.paramters = paramters;
		this.name = name;
	}
	private List<InputParameter> paramters;
	/**
	 * @return the paramters
	 */
	public List<InputParameter> getParamters() {
		return paramters;
	}
	/**
	 * @param paramters the paramters to set
	 */
	public void setParamters(List<InputParameter> paramters) {
		this.paramters = paramters;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	private String name;
	
	private List<String> getParameterValues(){
		List<String> valueStrs = new ArrayList<String>();
		for (InputParameter p:paramters)
			valueStrs.add(p.getValueStr());
		return valueStrs;
	}
	
	public String getParamtersString() {
		return StringUtils.join(getParameterValues(), ',');
	}
}
