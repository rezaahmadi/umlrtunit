/**
 * 
 */
package ca.queensu.cs.modeltester.smarttesting;

import java.util.*;

/**
 * @author reza
 *
 */
public class TestGenConfig {
	private static int testMaxLength;
	private static int testSuitLength;
	private static boolean stochastic;
	private static boolean sequentialTesting;
	private static boolean randomTesting;
	private static boolean propertyAwareTestGeneration;
	private static String parameterRanges;
	public static List<InputParameterValueRanges> parameterRangesMap;

	
	public static boolean isPropertyAwareTestGeneration() {
		return propertyAwareTestGeneration;
	}
	public static void setPropertyAwareTestGeneration(boolean propertyAwareTestGeneration) {
		TestGenConfig.propertyAwareTestGeneration = propertyAwareTestGeneration;
	}
	public static boolean isSequentialTesting() {
		return sequentialTesting;
	}
	public static void setSequentialTesting(boolean smartTesting) {
		TestGenConfig.sequentialTesting = smartTesting;
	}
	public static boolean isRandomTesting() {
		return randomTesting;
	}
	public static void setRandomTesting(boolean randomTesting) {
		TestGenConfig.randomTesting = randomTesting;
	}
	public static boolean isSmartTesting() {
		return smartTesting;
	}
	public static void setSmartTesting(boolean smartTesting) {
		TestGenConfig.smartTesting = smartTesting;
	}
	private static boolean smartTesting;
	private static boolean stopOnFailureFound;
	private static String outMsgFilter;
	
//	public TestGenConfig(int testMaxLength, int testSuitLength, boolean stochastic) {
//		super();
//		TestGenConfig.testMaxLength = testMaxLength;
//		TestGenConfig.testSuitLength = testSuitLength;
//		TestGenConfig.stochastic = stochastic;
//	}
	public static int getTestMaxLength() {
		return testMaxLength;
	}
	public static void setTestMaxLength(int testMaxLength) {
		TestGenConfig.testMaxLength = testMaxLength;
	}
	public static int getTestSuitLength() {
		return testSuitLength;
	}
	public static void setTestSuitLength(int testSuitLength) {
		TestGenConfig.testSuitLength = testSuitLength;
	}
	public static boolean isStochastic() {
		return stochastic;
	}
	public static void setStochastic(boolean stochastic) {
		TestGenConfig.stochastic = stochastic;
	}
	
	public static void setStopOnFailureFound(boolean stopOnFailureFound) {
		TestGenConfig.stopOnFailureFound = stopOnFailureFound;
	}
	
	public static boolean isStopOnFailureFound() {
		return stopOnFailureFound;

	}
	public static String getOutMsgFilter() {
		// TODO Auto-generated method stub
		return outMsgFilter;
	}
	
	public static List<String> getOutMsgFilterList() {
		// TODO Auto-generated method stub
		List<String> res = new ArrayList<String>();
		if (!getOutMsgFilter().isEmpty()) {
			for (String msg:getOutMsgFilter().split(",")) {
				res.add(msg);
			}
		}
		return res;
	}
	
	public static void setOutMsgFilter(String osm) {
		// TODO Auto-generated method stub
		 outMsgFilter = osm;
	}
	public static void setParameterRanges(String text) {
		// TODO Auto-generated method stub
		parameterRanges=text;
		
		// set in the map, for easier access
		parameterRangesMap = new ArrayList<InputParameterValueRanges>();
		String[] ranges = text.split(";");
		for (String pairs : ranges) {
			String paramName = pairs.split(":")[0];
			String range = pairs.split(":")[1];
			InputParameterValueRanges valueRange = new InputParameterValueRanges();
			valueRange.setMin(Integer.parseInt(range.split(",")[0]));
			valueRange.setMax(Integer.parseInt(range.split(",")[1]));
			valueRange.setInputParameterName(paramName);
			parameterRangesMap.add(valueRange);
		}
	}
	public static String getParametersRanges() {
		// TODO Auto-generated method stub
		return parameterRanges;
	}
}
