/**
 * 
 */
package ca.queensu.cs.modeltester.slicing;

//import java.util.HashMap;
import java.util.*;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;
//import org.hsqldb.lib.HashMap;
import ca.queensu.cs.modeltester.utils.RegExUtil;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import ca.queensu.cs.modelverifier.slicing.data.Statement;
import ca.queensu.cs.modelverifier.slicing.data.ActionCodes;
import ca.queensu.cs.modelverifier.slicing.data.Dependency;
import ca.queensu.cs.modelverifier.slicing.data.DependencyGraph;
import ca.queensu.cs.modelverifier.slicing.data.DependencyKind;
import ca.queensu.cs.modelverifier.slicing.data.DependencyNode;
import ca.queensu.cs.modelverifier.slicing.data.ObjectType;
import ca.queensu.cs.modelverifier.slicing.data.SlicingConfig;
import ca.queensu.cs.modelverifier.slicing.data.TransitionPath;

/**
 * @author reza
 * 
 *         Our slicing technique is based on Korel et.al algorithm
 */
public class DependencyCreator {

	public List<Object> transtates;
	private Class capsule;
	private Map<Class, List<Property>> partsWithDependencies;
	private DependencyGraph dependencyGraph;
	public DependencyGraph getDependencyGraph() {
		return dependencyGraph;
	}

	public DependencyCreator(Class capsule) {

		this.capsule = capsule;

		//init the graph
		this.dependencyGraph = new DependencyGraph();
		// filling the graph with states and transitions of the capsule's state machine
		initDependencyGraph();
		
		//ToDo: name unnamed connectors
		
		// transtates.addAll(UmlrtUtil.getAllVertexes(capsule));
	}

	public DependencyCreator() {}
	
	//dependencies between ports and parts
	//ToDo: needs tests
	public void createStructuralDependencies(boolean isTop, Object capsule, List<Property> parts) {
		// capsule top to its parts dependencies
		Class clsCapsule = capsule instanceof Class ? (Class) capsule : (Class) ((Property) (capsule)).getType();
		boolean isComposite = UmlrtUtil.isCompositeCapsule(clsCapsule);
		// top to its ports and connectors
		if (isTop)
			addPortAndPartDependencies(true, isComposite, capsule);
		
		//is composite
		for (Property part : UmlrtUtil.getCapsuleParts(clsCapsule)) {
			//addDependency(dependencyGraph, part, capsule, DependencyKind.hasPart, null);
			addDependency(dependencyGraph, capsule, part, DependencyKind.PartOf, null);

			//adding part's dependencies to ports and connectors
			addPortAndPartDependencies(false, isComposite, part);
			
			//call the function with parts of the current capsule
			createStructuralDependencies(false, part, UmlrtUtil.getCapsuleParts((Class)part.getType()));
		}
	}

	/**
	 * @param part
	 */
	private void addPortAndPartDependencies(boolean isTop, boolean isComposite, Object capsule) {
		Class clsCapsule = capsule instanceof Class ? (Class) capsule : (Class) ((Property) (capsule)).getType();
		// parts-ports and ports-connectorEnds
		if (isTop) { //Top capsule to its ports
			for (Property port : UmlrtUtil.getPorts(clsCapsule)) {
				//addDependency(dependencyGraph, port, capsule, DependencyKind.hasPort, null);
				addDependency(dependencyGraph, capsule, port, DependencyKind.PortOf, null);
			}
		} 
		if (isComposite) { // //capsule parts to its connectorEnds
			Class compositeCapsule = null;
			if (isTop)
				compositeCapsule = (Class) capsule;
			else
				compositeCapsule=(Class)(((Property)capsule).getType());
			for (Property part : UmlrtUtil.getCapsuleParts(compositeCapsule)) {
				for (Port port : UmlrtUtil.getPorts((Class)part.getType())) {
					//addDependency(dependencyGraph, port, part, DependencyKind.hasPort, null);
					addDependency(dependencyGraph, part, port, DependencyKind.PortOf, null);
				}
			}
		}

		// ports-connectors
		// here I connect port to connectors, 
		// but it is simple to change it such that connect connectors to connectorEnds 
		for (Connector connector : UmlrtUtil.getConnectors(clsCapsule)) {
			ConnectorEnd connEnd1 = connector.getEnds().get(0);
			ConnectorEnd connEnd2 = connector.getEnds().get(1);

			if (connEnd1 != null && connEnd1.getRole() instanceof Port) {
				// port-connector
				addDependency(dependencyGraph, connEnd1.getRole(), connector, DependencyKind.con, null);
				// connector-port
				addDependency(dependencyGraph, connector, connEnd1.getRole(), DependencyKind.ConnEnd, null);
			}

			if (connEnd2 != null && connEnd2.getRole() instanceof Port) {
				// port-connector
				addDependency(dependencyGraph, connEnd2.getRole(), connector, DependencyKind.con, null);
				// connector-port
				addDependency(dependencyGraph, connector, connEnd2.getRole(), DependencyKind.ConnEnd, null);
			}
		}
	}

	
	 
	public void createBehavioralDependencies() {

		// creating the data dependencies
		createDataDependencies();

		// create control dependencies
		createControlDependencies();
		
		//create transitions to ports dependencies
		//createTopCapsulePortsToTransitionsDependencies();
		createPortTransitionDependencies();
		
		
		//communication
		createCommDependency();

		// ToDo: time & timer dependencies
		
		//discover dependencies between action codes in each transition
		//and add related information in the dep. graph
		createActionCodeDependencies();

		//return dependencyGraph;
	}


	//create capsule part connector ends to transitions dependencies
//	private void createPortToTransitionsDependencies() {
//		for (Property part : UmlrtUtil.getCapsuleParts(capsule)) {
//			List<ConnectorEnd> connectorEnds = UmlrtUtil.getConnectorEnds(capsule, part);
//			if (connectorEnds != null && connectorEnds.size() > 0) {
//				for (Transition t : UmlrtUtil.getAllTransitions((Class) part.getType())) {
//
//					// finding dependencies where a transition sends a message to a port
//					String actionCode = UmlrtUtil.getActionCodeStr(t);
//					List<ConnectorEnd> dependingPorts = connectorEnds.stream()
//							.filter(ce -> RegExUtil.isSendToPortCommand(actionCode, ce.getRole().getName()))
//							.collect(Collectors.toList());
//					for (ConnectorEnd port : dependingPorts) {
//						// transition is sending message to the port
//						DependencyNode trNode = dependencyGraph.getNodeOf(t);
//						DependencyNode portNode = dependencyGraph.getNodeOf(port);
//						trNode.getDependencies().add(new Dependency(portNode, DependencyKind.SendsMessageTo));
//					}
//
//					// finding dependencies where a message from a port triggers a transition
//					List<String> triggers = UmlrtUtil.getTriggers(t);
//					for (String trigger : triggers) {
//						String triggerName = trigger.split("\\.")[0];
//						Optional<ConnectorEnd> p = connectorEnds.stream()
//								.filter(port -> port.getRole().getName().equals(triggerName)).findFirst();
//						if (p != null) {
//							// transition is triggered by a message from port
//							DependencyNode trNode = dependencyGraph.getNodeOf(t);
//							DependencyNode portNode = dependencyGraph.getNodeOf(p.get());
//							trNode.getDependencies().add(new Dependency(portNode, DependencyKind.ReceivesMessageFrom));
//						}
//					}
//				}
//			}
//		}
//	}
	
	//create capsule transitions to its ports dependencies
	private void createPortTransitionDependencies() {
		
		//create dependencies for the container capsule
		createPortTransitionDependencies(capsule);
		
		//create dependencies for the parts
		for (Property part : UmlrtUtil.getCapsuleParts(capsule)) {
			createPortTransitionDependencies((Class)part.getType());
		}
	}
	
	//create capsule transitions to its ports dependencies
	private void createPortTransitionDependencies(Class capsule) {
		List<Port> ports = UmlrtUtil.getPorts(capsule);
		if (ports != null && ports.size() > 0) {
			for (Transition t : UmlrtUtil.getAllTransitions(capsule)) {

				// finding dependencies where a transition sends a message to a port
				String actionCode = UmlrtUtil.getActionCodeStr(t);
				List<Port> dependingPorts = ports.stream()
						.filter(port -> RegExUtil.isSendToPortCommand(actionCode, port.getName()))
						.collect(Collectors.toList());
				for (Port port : dependingPorts) {
					// transition is sending message to the port
					DependencyNode trNode = dependencyGraph.getNodeOf(t);
					DependencyNode portNode = dependencyGraph.getNodeOf(port);
					trNode.getDependencies().add(new Dependency(portNode, DependencyKind.Triggers));
				}

				// finding dependencies where a message from a port triggers a transition
				List<String> triggers = UmlrtUtil.getTriggers(t);
				for (String trigger : triggers) {
					String triggerName = trigger.split("\\.")[0];
					Optional<Port> p = ports.stream().filter(port -> port.getName().equals(triggerName)).findFirst();
					if (p != null) {
						// transition is triggered by a message from port
						DependencyNode trNode = dependencyGraph.getNodeOf(t);
						DependencyNode portNode = dependencyGraph.getNodeOf(p.get());
						trNode.getDependencies().add(new Dependency(portNode, DependencyKind.TrigBy));
					}
				}
			}
		}

	}

	// there exist a control dependency between transitions T1, T2 iff:
	// rule1. source(T2) exists in all maximalPaths( target(T1) )
	// rule2. T1 has a sibling T3 where, there exist at least one path
	// in maximalPaths(source(T3)) that source(T2) does not exist in it
	// same might be defined for data dependency between states
	public void createControlDependencies() {
		for (Object obj1 : transtates) {
			for (Object obj2 : transtates) {
				//transition t is not control dependent on itself
				if (obj2 instanceof Transition && obj1 instanceof Transition && !obj1.equals(obj2)) {
					boolean rule1 = false, rule2 = false;
					Transition t1 = (Transition) obj1;
					Transition t2 = (Transition) obj2;
					//ToDo: check this
//					if (!t1.getContainer().getStateMachine().equals(t2.getContainer().getStateMachine()))
//						continue;
					TransitionPath currentPath = new TransitionPath();
					List<TransitionPath> allPaths = new ArrayList<TransitionPath>();

					// rule1
//					if (t1.getTarget().equals(t2.getSource())){
//						//rule1 = true;
//					} else {
						UmlrtUtil.findAllMaximalPaths(t1.getTarget(), t1.getTarget(), currentPath, allPaths);
						int c = 0;
						for (TransitionPath tp : allPaths) {
							for (Transition tt : tp.getItems()) {
								if (tt.equals(t2)) {
									c++;
									break;
								}
							}
						}
						if (allPaths.size() > 0 && c == allPaths.size()) {
							rule1 = true;
						} else
							rule1 = false;
//					}
					
					// rule2
					if (rule1) {
						currentPath = new TransitionPath();
						allPaths = new ArrayList<TransitionPath>();
						Vertex parantNode = t1.getSource();
						for (Transition sibling : parantNode.getOutgoings()) {
							if (!sibling.equals(t1)) {
								currentPath = new TransitionPath();
								allPaths = new ArrayList<TransitionPath>();
								if (sibling.getTarget().getOutgoings() == null//in case sibling does not have outgoings
										|| sibling.getTarget().getOutgoings().size() == 0) {
									TransitionPath path = new TransitionPath();
									path.add(sibling);
									allPaths.add(path);
								} else {
									UmlrtUtil.findAllMaximalPaths(sibling.getTarget(), sibling.getTarget(), currentPath,
											allPaths);
								}
								for (TransitionPath tp : allPaths) {
									rule2 = true;
									for (Transition tt : tp.getItems()) {
										if (tt.equals(t2)) {
											rule2 = false;
										}
									}
									if (rule2)
										break;
								} // for
							} // if
							if (rule2)
								break; // for
						} // for
					} // if

					if (rule1 && rule2) { // t2 is control dependent on t1
						DependencyNode t1Node = dependencyGraph.getNodeOf(t1);
						DependencyNode t2Node = dependencyGraph.getNodeOf(t2);
						t2Node.getDependencies().add(new Dependency(t1Node, DependencyKind.ctrl));
					}
				}
			} // for
		} // for
	}

	// there exist a data dependency between T1, T2 iff:
	// 1. T1 modifies v and T2 uses v
	// 2. there is at least one path from T1 -> T2 where v is not modified
	// same might be defined for data dependency between states
	// ToDo: we currently support data dependency between transitions
	public void createDataDependencies() {
		// get all capsule attributes
		List<Property> atts = UmlrtUtil.getCapsuleProperties(capsule);

		for (Object obj1 : transtates) {
			for (Object obj2 : transtates) {
				// ToDo:currently assume that only transitions have action code
				// t2 action code depends on t1 action code
				if (obj2 instanceof Transition && obj1 instanceof Transition) {
					Transition t1 = (Transition) obj1;
					Transition t2 = (Transition) obj2;
					//ToDo: check this
//					if (!t1.getContainer().getStateMachine().equals(t2.getContainer().getStateMachine()))
//						continue;
					for (Property att : atts) {
						// ToDo:for now we assume transitions have 1 line of action code
						boolean t1Assigned = RegExUtil.isVarAssigned(UmlrtUtil.getActionCodeStr(t1), att.getName());
						boolean t2UsedActionCode = RegExUtil.isVarUsed(UmlrtUtil.getActionCodeStr(t2), att.getName());
						boolean t2UsedGuard = RegExUtil.isVarUsed(UmlrtUtil.getGuardStr(t2), att.getName());
						if (t1Assigned && (t2UsedActionCode || t2UsedGuard)) {
							// is there one path that the variable is not updated in
							if (t1.getTarget().equals(t2.getSource())) {
								addDependency(dependencyGraph, t1, t2, DependencyKind.dd, att.getName());
							} else if (varNotUpdated((Transition) t1, (Transition) t2, att.getName())) {
								addDependency(dependencyGraph, t1, t2, DependencyKind.dd, att.getName());
							}
						}
					} // for
				} // if

			}

		}

	}
	

	public void createActionCodeDependencies() {
		for (DependencyNode node : dependencyGraph) {
			if (node.getActionCodes() != null && !node.getActionCodes().isEmpty()) {
				// data dependencies
				for (Statement stmt1 : node.getActionCodes()) {
					//String varAssigned = RegExUtil.getVarAssign(stmt1.getStatement());
					List<String> varsUsed = RegExUtil.getAllVarsUsed(stmt1.getStatement());
					if (varsUsed.size() > 0) {
						for (Statement stmt2 : node.getActionCodes()) {
							// each statement is only dependent on previous statements
							if (stmt1.getLine() > stmt2.getLine()) {
								if (varsUsed.stream().anyMatch(v -> RegExUtil.isVarDeclared(stmt2.getStatement(), v) || RegExUtil.isVarAssigned(stmt2.getStatement(), v))) {
									stmt1.getDependencies().add(stmt2);
								}
							}
						}
					}
				} // for

				// control dependencies
				for (Statement stmt : node.getActionCodes()) {
					Statement ifStatement = node.getActionCodes().stmtIsInIfBlock(stmt);
					if (ifStatement != null)
						stmt.getDependencies().add(ifStatement);
				}
			} // for
		}
	}

	public void createCommDependency() {
		for (Object obj1 : transtates) {
			for (Object obj2 : transtates) {
				//a transition is not communication dependent on itself
				//also two comm-dependent transitions must come from two distinct state machine
				//for the moment transitions can be comm-dependent only
				if ((obj1 != obj2) && (UmlrtUtil.getStateMachine(obj1) != UmlrtUtil.getStateMachine(obj2)) && obj2 instanceof Transition
						&& obj1 instanceof Transition) {
//				if ((obj1 != obj2) && obj2 instanceof Transition
//						&& obj1 instanceof Transition) {
					Transition t1 = (Transition) obj1;
					Transition t2 = (Transition) obj2;
					List<String> triggers = UmlrtUtil.getTriggers(t2);
					String actionStr = UmlrtUtil.getActionCodeStr(t1);
					for (String trig:triggers) {
						if (RegExUtil.isTriggerGenerated(actionStr, trig)) {//if t1 generates any of the required triggers
							addDependency(dependencyGraph, t1, t2, DependencyKind.com, trig);
						}
					}//for
				}//if
			}//for
		}//for
	}
	
	//obj2 is dependent on obj1
	private void addDependency(DependencyGraph dg, Object dependent, Object dependOn, DependencyKind kind, Object obj) {
		DependencyNode node1 = dg.getNodeOf(dependent);
		DependencyNode node2 = dg.getNodeOf(dependOn);
		if (node1==null || node2==null) {
			System.out.println(String.format( "node1:%s, node2:%s", node1.getDependantName(),node2.getDependantName()));
		}
		node2.getDependencies().add(new Dependency(node1, kind, obj));
	}
	
//	private void addCommDependency(DependencyGraph dg, Transition t1, Transition t2, Object trigger) {
//		DependencyNode t1Node = dg.getNodeOf(t1);
//		DependencyNode t2Node = dg.getNodeOf(t2);
//		t2Node.getDependencies().add(new Dependency(t1Node, DependencyKind.Communication, trigger));
//	}

	public void createTimerDependency(DependencyGraph dg) {

	}

	public void createTimeoutDependency(DependencyGraph dg) {

	}

	public boolean areControlDependent(Transition t1, Transition t2) {
		return false;

	}

	// checking if there exists one path from node1 to node2,
	// where a variable is not updated
	// ToDo: we currently only support transitions
	private boolean varNotUpdated(Transition node1, Transition node2, String var) {
		// if the two transitions share a state
		if (node1.getTarget().equals(node2.getSource()))
			return true;

		List<TransitionPath> allPath = new ArrayList<TransitionPath>();
		UmlrtUtil.findAllPaths(node1.getTarget(), node2.getSource(), new TransitionPath(), allPath);
		if (allPath.size() > 0) {
			for (TransitionPath path : allPath) {
				for (Transition t : path.getItems()) {
					if (!RegExUtil.isVarAssigned(UmlrtUtil.getActionCodeStr(t), var)) {
						return true;
					} // if
				} // for
			} // for
		}
		return false;
	}

	
	// fill the graph will all objects inside the UML-RT capsule & state machine
	public void initDependencyGraph() {
		
		partsWithDependencies = new HashMap<Class, List<Property>>();
		//extract parts
//		if (SlicingConfig.getCompsiteCapsuleSlicing() == true) {
		UmlrtUtil.getCapsulePartsWithDependencies(capsule, partsWithDependencies);
//		}
		
		// list of states and transitions
		transtates = new ArrayList<Object>();
		transtates.addAll(UmlrtUtil.getAllTransitions(capsule));
//		if (SlicingConfig.getCompsiteCapsuleSlicing() == true) {
		for (Class part : partsWithDependencies.keySet()) {
			transtates.addAll(UmlrtUtil.getAllTransitions(part));
		}
//		} else {
//			transtates.addAll(UmlrtUtil.getAllTransitions(capsule));
//		}

		UmlrtUtil.NameUnnamedTransitions(capsule, transtates);
		
		//StateMachine sm = UmlrtUtil.getStateMachine(capsule);
		int i = 0;

		// adding the top capsule to the dg
		DependencyNode depCapsuleTop = new DependencyNode(i, ObjectType.Part, capsule, false, capsule.getName());
		// adding to the graph
		dependencyGraph.add(depCapsuleTop);
		i++;
		
		// adding all capsule attributes (port, part, connectorEnds, and others like int,floar,..) to the dependency graph
//		for (Property att : capsule.getAllAttributes()) {
//		    if (UmlrtUtil.isCapsulePart(att)) {
//				// adding all parts to the dependency graph
//				DependencyNode depPart = new DependencyNode(i, ObjectType.Part, att, false, att.getName());
//				// adding to the graph
//				dependencyGraph.add(depPart);
//			} else if (att instanceof Port) {
//				DependencyNode depPort = new DependencyNode(i, ObjectType.Port, att, false, att.getName());
//				// adding to the graph
//				dependencyGraph.add(depPort);
//			} else { // other type of attribute
//				DependencyNode dep = new DependencyNode(i, ObjectType.CapsuleAttribute, att, capsule, false);
//				// adding to the graph
//				dependencyGraph.add(dep);
//			}
//			i++;
//		}

		/*
		 * // adding all vertexes to the dependency graph List<Vertex> allStates =
		 * UmlrtUtil.getAllVertexes(sm); for (Vertex vertex : allStates) {
		 * DependencyNode dep = new DependencyNode(i, ObjectType.State, vertex, false);
		 * 
		 * // adding to the graph dependencyGraph.add(dep); i++; }
		 * 
		 * // adding all transitions to the dependency graph List<Transition>
		 * allTransitions = UmlrtUtil.getAllTransitions(sm); for (Transition tran :
		 * allTransitions) { DependencyNode dep = new DependencyNode(i,
		 * ObjectType.Transition, tran, false);
		 * 
		 * // adding transition's action codes to its related node String actionCodeStr
		 * = UmlrtUtil.getActionCodeStr(tran); dep.setActionCodes(new
		 * ActionCodes(actionCodeStr));
		 * 
		 * // adding to the graph dependencyGraph.add(dep); i++; }
		 */


		// adding ports, connectors and other attributes of each part to the DG
		for (Map.Entry<Class, List<Property>> partDependencies : partsWithDependencies.entrySet()) {
			// for (Property part : partDependencies.getValue()) {

			// adding all connectorEnds to the graph
//			for (ConnectorEnd ce : UmlrtUtil.getConnectorEnds(partDependencies.getKey())) {
//				// adding all ConnectorEnd to the dependency graph
//				DependencyNode depConnectorEnd = new DependencyNode(i, ObjectType.ConnectorEnd, ce, capsule, false);
//				// adding to the graph
//				dependencyGraph.add(depConnectorEnd);
//			}

//			// adding ports and other attributes of each part to the DG
//			for (Property att : partDependencies.getKey().getAllAttributes()) {
//				if (att instanceof Port) {
//					DependencyNode depPort = new DependencyNode(i, ObjectType.Port, att, false, att.getName());
//					// adding to the graph
//					dependencyGraph.add(depPort);
//				} else { // other type of attribute
//					DependencyNode dep = new DependencyNode(i, ObjectType.CapsuleAttribute, att, capsule, false);
//					// adding to the graph
//					dependencyGraph.add(dep);
//				}
//				i++;
//			}
			
			for (Property att : partDependencies.getKey().getAllAttributes()) {
			    if (UmlrtUtil.isCapsulePart(att)) {
					// adding all parts to the dependency graph
					DependencyNode depPart = new DependencyNode(i, ObjectType.Part, att, false, att.getName());
					// adding to the graph
					dependencyGraph.add(depPart);
				} else if (att instanceof Port) {
					DependencyNode depPort = new DependencyNode(i, ObjectType.Port, att, false, att.getName());
					// adding to the graph
					dependencyGraph.add(depPort);
				} else { // other type of attribute
					DependencyNode dep = new DependencyNode(i, ObjectType.CapsuleAttribute, att, capsule, false);
					// adding to the graph
					dependencyGraph.add(dep);
				}
				i++;
			}

			// adding all connectors of the part to the dependency graph
			for (Connector connector : UmlrtUtil.getConnectors(partDependencies.getKey())) {
				if (connector.getName().isEmpty())
					connector.setName("connector_" + i);
				DependencyNode depConnector = new DependencyNode(i, ObjectType.Connector, connector, false,
						connector.getName());
				// adding to the graph
				dependencyGraph.add(depConnector);
				i++;
			}
			// }
		}

		i = addTransitionsToDG(i);

	}

	/**
	 * @param i
	 * @return
	 */
	private int addTransitionsToDG(int i) {
		// adding all transitions to the dependency graph
		// and adding all vertexes to the dependency graph
		for (Object o : transtates) {
			if (o instanceof Transition) {
				Transition tran = (Transition) o;
				DependencyNode dep = new DependencyNode(i, ObjectType.Transition, tran, false);

				// adding transition's action codes to its related node
				String actionCodeStr = UmlrtUtil.getActionCodeStr(tran);
				dep.setActionCodes(new ActionCodes(actionCodeStr));

				// adding to the graph
				dependencyGraph.add(dep);
				i++;

			} else if (o instanceof State) {
				State s = (State) o;
				DependencyNode dep = new DependencyNode(i, ObjectType.State, s, false);

				// adding to the graph
				dependencyGraph.add(dep);
				i++;
			}
		}
		return i;
	}

}
