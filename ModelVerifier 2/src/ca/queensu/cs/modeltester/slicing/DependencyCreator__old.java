/**
 * 
 */
package ca.queensu.cs.modeltester.slicing;

import java.util.List;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.Vertex;

import ca.queensu.cs.modeltester.utils.RegExUtil;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;
import ca.queensu.cs.modelverifier.slicing.data.DependencyGraph;
import ca.queensu.cs.modelverifier.slicing.data.DependencyNode;
import ca.queensu.cs.modelverifier.slicing.data.ObjectType;
import symbolic.execution.code.model.cpp.Program;
import symbolic.execution.code.parser.CPPParser;
import symbolic.execution.constraints.manager.SymbolicVariable;
import symbolic.execution.umlrt.utils.Pair;

/**
 * @author reza
 * 
 * Our slicing technique is partly based on an algorithm explained in "And-Or Dependence Graphs for Slicing
	Statecharts" by Chris Fox and Arthorn Luangsodsai 
 */
public class DependencyCreator__old {
	
	public static DependencyGraph create(Class capsule) {
		
		DependencyGraph dependencyGraph = new DependencyGraph();

		// fill the graph will all objects inside the state machine
		initDependencyGraph(capsule, dependencyGraph);

		// connecting nodes inside the graph based on dependencies
//		createDependancyGraph(dependencyGraph, capsule);

		return dependencyGraph;
	}

	//ToDo: double check this
//	private static void createDependancyGraph(DependencyGraph dependencyGraph, Class capsule) {
//		for (DependencyObj node: dependencyGraph) {
//			
//			//finding dependencies on the states
//			if (node.getObjectType() == ObjectType.State) {
//				State state = (State) node.getDependantObj();
//				for (Transition t: state.getIncomings()) {
//					//a state is dependent on all triggers of its incoming transitions
//					for (Trigger trig: t.getTriggers()) {
//						node.dependsOn.add(dependencyGraph.getNodeOf(trig));
//					}
//					
//					//a state is dependent on all guards of its incoming transitions
//					node.dependsOn.add(dependencyGraph.getNodeOf(t.getGuard()));
//					
//					//a state is dependent on the source state of its incoming transitions
//					node.dependsOn.add(dependencyGraph.getNodeOf(t.getSource()));
//				}
//			}
//			
//			//ToDo: finding time dependencies between states
//			
//			// finding action codes dependencies to the capsule attributes
//			if (node.getObjectType() == ObjectType.ActionCode) {
//				// check capsule attribute updates
//				String varName = RegExUtil.getVarAssigns((String) node.dependantObj);
//				if (varName!="") {
//					for (Property att : capsule.getAllAttributes()) {
//						if (varName.equals(att)) {
//							// adding a dependency from a varible in the action code to the capsule
//							// attribute
//							node.dependsOn.add(dependencyGraph.getNodeOf(att));
//
//							// adding a dependency from the capsule attribute to a variable in the action
//							// code
//							dependencyGraph.getNodeOf(att).dependsOn.add(node);
//						}
//					}
//				} // end if
//				
//				// check capsule attribute in message sends
//				if (RegExUtil.isMessageSendingCommand((String) node.dependantObj)) {
//					List<String> params = RegExUtil.getMessageParameters((String) node.dependantObj);
//					for (Property att : capsule.getAllAttributes()) {
//						if (params.contains(att)) {
//							// adding a dependency from a varible in the action code to the capsule
//							// attribute
//							node.dependsOn.add(dependencyGraph.getNodeOf(att));
//						}
//					}
//				}// end if
//				
//				//ToDo check data dependencies between action codes
//				//ToDo check control dependencies between action codes
//				//ToDo: finding clock dependencies
//				
//			}//end if
//			
//			
//		}
//	}
	
	// fill the graph will all objects inside the UML-RT capsule & state machine
	private static void initDependencyGraph(Class capsule, DependencyGraph dependencyGraph) {
		
		StateMachine sm = UmlrtUtil.getStateMachine(capsule);
		int i = 0;
		
		//adding all capsule attributes to the dependency graph
		for (Property att: capsule.getAllAttributes()) {
			DependencyNode dep = new DependencyNode(i, ObjectType.CapsuleAttribute, att, capsule, false);
			
			//adding to the graph
			dependencyGraph.add(dep);
			i++;
		}

		//adding all vertexes to the dependency graph
		List<Vertex> allStates = UmlrtUtil.getAllVertexes(sm);
		for (Vertex vertex: allStates) {
			DependencyNode dep = new DependencyNode(i, ObjectType.State, vertex, false);
			
			//adding to the graph
			dependencyGraph.add(dep);
			i++;
		}

		// adding all action codes on transtions to the dependency graph
		List<Transition> allTransitions = UmlrtUtil.getAllTransitions(sm); 
		for (Transition tran : allTransitions) {

			//adding all transitions to the dependency graph
			//DependencyObj depTrans = new DependencyObj(i, DependentType.Transition, tran, false);
			// adding to the graph
			//dependencyGraph.add(depTrans);
			//i++;
			
			//adding guards to the graph
//			DependencyObj depGuard = new DependencyObj(i, DependentType.Guard, tran.getGuard().getSpecification().stringValue(), false);
			DependencyNode depGuard = new DependencyNode(i, ObjectType.Guard, tran.getGuard(), tran, false);
			dependencyGraph.add(depGuard);
			i++;
			
			//adding triggers to the graph
			for (Trigger trig: tran.getTriggers()) {
				DependencyNode depTrigger = new DependencyNode(i, ObjectType.Trigger, trig, tran, false);
				dependencyGraph.add(depTrigger);
				i++;
//				Event event = trig.getEvent();
//				if (event instanceof CallEvent){
//					CallEvent call = (CallEvent) event;
//					Pair<Port, CallEvent> trigInfo = new Pair<Port, CallEvent>(trig.getPorts().get(0), call);
//					DependencyObj depTrigger = new DependencyObj(i, DependentType.Trigger, trigInfo, false);
//					dependencyGraph.add(depTrigger);
//					i++;
//				}
			}
			
			//adding action codes
			//ToDo: parse the action code using Karolina's parser
			if (tran.getEffect() != null && ((OpaqueBehavior) tran.getEffect()).getBodies() != null) {
				for (String actionCode : ((OpaqueBehavior) tran.getEffect()).getBodies()) {
					DependencyNode depAC = new DependencyNode(i, ObjectType.ActionCode, actionCode, tran, false);

					// adding to the graph
					dependencyGraph.add(depAC);
					i++;
				}
			}
		}
		
		// adding all action codes in states to the dependency graph
		for (Vertex vertex : allStates) {
			if (vertex instanceof State) {
				State state = (State)vertex;
				
				//State Entry action code
				if (state.getEntry()!=null && ((OpaqueBehavior)state.getEntry()).getBodies()!=null) {
					for (String actionCode : ((OpaqueBehavior) state.getEntry()).getBodies()) {
						DependencyNode dep = new DependencyNode(i, ObjectType.ActionCode, actionCode, state, false);
						
						//adding to the graph
						dependencyGraph.add(dep);
						i++;
					}
				}
				
				//State Exit action code
				if (state.getExit()!=null && ((OpaqueBehavior)state.getExit()).getBodies()!=null) {
					for (String actionCode : ((OpaqueBehavior) state.getExit()).getBodies()) {
						DependencyNode dep = new DependencyNode(i, ObjectType.ActionCode, actionCode, state, false);
						
						//adding to the graph
						dependencyGraph.add(dep);
						i++;
					}
				}
			}
		}//for
	}

	private static InputStream createStream(String code) {
		char[] charArray = code.toCharArray();
		byte[] byteArray = new byte[charArray.length];
		for (int i=0; i <byteArray.length; i++){
			byteArray[i] = (byte) charArray[i];
		}
		
		return new ByteArrayInputStream(byteArray);
	}

}
