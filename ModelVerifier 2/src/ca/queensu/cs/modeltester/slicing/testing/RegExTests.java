/**
 * 
 */
package ca.queensu.cs.modeltester.slicing.testing;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.queensu.cs.modeltester.utils.RegExUtil;

/**
 * @author rezaahmadi
 *
 */
public class RegExTests {
	String test1 = "int x = 0";
	String test2 = "intx;";
	String test3 = " x++";
	String test4 = "ex67   =    2* 100";
	String test5 = "++x";
	String test6 = "-3-x";
	String test7 = "x-2-";
	String test8 = " p1.m(rt, x).send();";
	String test9 = " p1 - m(rt, x).send();";
	String test10 = "port1.msg1(XYZ).send();";
	String test11 = "z=z+1;\n" + "y=z-1;\n" + "";
	String test12 = "return Y - XYZ;";
	
	@Test
	public void testVarDecl() {
		
		List<String> ss1 = RegExUtil.getVarsDecl("float XYZ; int X; Object X; return X90;");
		assert(ss1.contains("XYZ"));
		assert(ss1.contains("X"));
		
		ss1 = RegExUtil.getVarsDecl("++x;port1.msg1(XYZ).send();");
		assert(!ss1.contains("XYZ"));
		assert(!ss1.contains("x"));
		assert(!ss1.contains("X90"));
		
		List<String> ss12 = RegExUtil.getVarsDecl("\n" + 
				"float x = 10;\n" + 
				"PersonX p2;\n" + 
				"int x90;");
		assert(ss12.contains("x"));
		assert(ss12.contains("p2"));
		assert(ss12.contains("x90"));
		assert(!ss12.contains("x902"));
		
		assert RegExUtil.isVarDeclared("int X;", "X");
		assert RegExUtil.isVarDeclared("int X22=10;", "X22");
		assert RegExUtil.isVarDeclared("int X22 =10;", "X22");
	}
	
	@Test
	public void testVarUsed() {
		assert RegExUtil.isVarUsed("port1.msg1(XYZ).send();", "XYZ");
		assert RegExUtil.isVarUsed("return Y - XYZ;", "Y");
		assert RegExUtil.isVarUsed("X = 90 - Y - XYZ - B2B;", "B2B");
		assert !RegExUtil.isVarUsed("float XYZ;", "XYZ");
		assert RegExUtil.isVarUsed("X+= XYZ;", "XYZ");
		assert RegExUtil.isVarUsed("X+= XYZ;", "X");
		assert !RegExUtil.isVarUsed("Object X = 12;", "Object");
		assert !RegExUtil.isVarUsed("int X = 12;", "int");
	}
	

	@Test
	public void testMessageSending() {
		
		assert RegExUtil.isMessageSendingCommand("  port1.msg1().send(2);");
		assert RegExUtil.isMessageSendingCommand("port1.msg1().send();");
		assert RegExUtil.isMessageSendingCommand("port1.msg1(XYZ).send();");
		assert RegExUtil.isMessageSendingCommand("   port1.msg123(XYZ, 2).send(); ");
		assert RegExUtil.isMessageSendingCommand(" port211.m33sg123(XYZ, 2, iop).send();");

		//assert RegExUtil.getMessageParameters("port211.m33sg123(XYZ, 2, iop).send();").stream().anyMatch(s->s.equals("XYZ"));
		assert RegExUtil.getMessageParameters("port211.m33sg123(XYZ, 2, iop).send();").stream().anyMatch(s->s.equals("XYZ"));
		assert RegExUtil.getMessageParameters("port211.m33sg123(XYZ, 2, iop).send();").stream().anyMatch(s->s.equals("iop"));
		assert RegExUtil.getMessageParameters("port211.m33sg123(XYZ, 2, iop).send();").stream().anyMatch(s->s.equals("2"));
		assert RegExUtil.getMessageParameters("port1.msg1(XYZ).send();").stream().anyMatch(s->s.equals("XYZ"));
		assert !RegExUtil.getMessageParameters("port1.msg1(XYZ).send();").stream().anyMatch(s->s.equals("XZ"));
		assert !RegExUtil.getMessageParameters("port1.msg1(XYZ).send();").stream().anyMatch(s->s.equals("2"));
	}
	
	//stupid code! fix this.
	public int getCount(List<String> list, String s) {
		int c=0;
		for (String item:list)
			if (s.equals(item))
				c++;
		return c;
	}
	
	@Test
	public void testGetIfPredicate() {
		assert RegExUtil.getIfPredicate("if (X>Y*10){X=100;}").equals("X>Y*10");
		assert RegExUtil.getIfPredicate("if (UCU){").equals("UCU");
		assert RegExUtil.getIfPredicate("if   (X<19 )").equals("X<19 ");

		assert (RegExUtil.getIfPredicate(
				"    if (Y>10 && Y<1901)\n" + "    {\n" + "        X++;\n" + "        Y--;\n" + "    }"))
						.equals("Y>10 && Y<1901");

		assert (RegExUtil.getIfPredicate("if (X>100) {X++}")).equals("X>100");
	}

	@Test
	public void testGetIfBody() {
		assert RegExUtil.getIfBodies("if (X>Y*10){X=100;}").stream().anyMatch(s->s.equals("X=100;"));
		assert RegExUtil.getIfBodies("    if (Y>10 && Y<1901)\n" + 
				"    {\n" + 
				"        X++;\n" + 
				"        Y--;\n" + 
				"    }\n" + 
				"").stream().anyMatch(s->s.equals("\n" + 
						"        X++;\n" + 
						"        Y--;\n" + 
						"    "));

	}

}
