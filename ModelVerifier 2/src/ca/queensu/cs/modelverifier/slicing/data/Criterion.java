/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

import java.util.*;

/**
 * @author rezaahmadi
 *
 */
public class Criterion {
 
	public Criterion() {}
	
	/**
	 * @param value
	 * @param kind
	 */
	public Criterion(Object value, CriterionKind kind) {
		this.value = value;
		this.kind = kind;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * @return the kind
	 */
	public CriterionKind getKind() {
		return kind;
	}

	/**
	 * @param kind the kind to set
	 */
	public void setKind(CriterionKind kind) {
		this.kind = kind;
	}

	private Object value;
  
	private CriterionKind kind;

}
