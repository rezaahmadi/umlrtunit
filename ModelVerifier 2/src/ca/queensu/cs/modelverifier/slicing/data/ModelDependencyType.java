/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

/**
 * @author rezaahmadi
 *
 */
public enum ModelDependencyType {
	Behaviroal, Structural, ActionCodes
}
