/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

import java.util.List;
import java.util.stream.Collectors;

import ca.queensu.cs.modeltester.utils.RegExUtil;
import ca.queensu.cs.modeltester.utils.UmlrtUtil;

import java.util.ArrayList;

/**
 * @author rezaahmadi
 *
 */
public class ActionCodes extends ArrayList<Statement> {
//	private String actionCodesStr;
//	public String[] getActionCodesLines(){
//		return actionCodesStr.split("[\n]");
//	}

	//ToDo:we currently do not support nested if blocks
	public Statement stmtIsInIfBlock(Statement stmt) {
		
		int i=0;
		while (i < this.size()) {
			int j = 0;
			if (RegExUtil.getIfPredicate(this.get(i).getStatement()) != "") {
				j = i;
				i++;
				while (i < this.size() && !this.get(i).getStatement().equals(stmt.getStatement())
						&& !this.get(i).getStatement().contains("}")) {
					i++;
				}
				if (this.get(i).getStatement().equals(stmt.getStatement()))
					return this.get(j);
				else
					i++;
			}
			else {
				i++;
			}
		}
		
//		List<String> ifBlocksStr = RegExUtil.getIfBlocks(actionCodesStr);
//		for (String ifBlockStr:ifBlocksStr) {
//			ActionCodes ifBlock = new ActionCodes(ifBlockStr);
//			for (Statement s:ifBlock) {
//				if (s.getStatement().equals(stmt.getStatement()))
//					
//			}
//		}
//		while (index < actionCodesStr.length()) {
//			int i = actionCodesStr.indexOf("if", index);
//			int i = actionCodesStr.indexOf("if", index);
//			if (i!=-1 && stmt.getLine() < j && stmt.getLine() > i)
//				return true;
//			index = j;
//		}
		// for (String ifBody : RegExUtil.getIfBodies(actionCodesStr)) {
		// int i = actionCodesStr.indexOf("{");
		// int j = actionCodesStr.lastIndexOf("}");
		// if (stmt.getLine() < j && stmt.getLine() > i)
		// locs.add(i);
		// }
		return null;
	}
	
	public ActionCodes(String actionCodesStr) {
		if (!actionCodesStr.equals("")) {
			String[] splitted = actionCodesStr.split("[\n]");
			int i = 0;
			for (String s : splitted) {
				if (!s.isEmpty()) {
					this.add(new Statement(s, i, false, RegExUtil.getAllVarsUsed(s), RegExUtil.getVarAssign(s)));
					i++;
				}
			}
		}
	}
	
	public ActionCodes() {}

	/**
	 * @return the actionCodesStr
	 */
	public String getActionCodesStr() {

		String actionCodesStr="";
		for (Statement s:this)
			actionCodesStr+=s.getStatement().trim() + "\n";
		return actionCodesStr;
	}

//	public void setActionCodesStr(String str) {
//
//		this.actionCodesStr = str;
//	}

	public void setMarked(int location) {
		for (Statement s:this)
			if (s.getLine() == location)
				s.setMarked(true);
	}

	public Statement getByVarAssignedName(String varName) {
		if (varName.isEmpty())
			return null;
		List<Statement> res = this.stream().filter(stmt -> stmt.getVarAssigned().equals(varName))
				.collect(Collectors.toList());
		if (!res.isEmpty())
			return res.get(0);
		return null;
	}

	public void printMarked() {
		String markedStr = "";
		for (Statement stmt:this) {
			if (stmt.isMarked()) {
				markedStr+=stmt.getStatement() + " \n ";
			}
		}
		System.out.println(String.format("Statements Marked: %s", markedStr));
	}

	public void printAll() {
		String str = "";
		for (Statement stmt : this) {
			str += stmt.getStatement() + " \n ";
		}
		System.out.println(String.format("node's actioncode after slice: %s", str));
	}
	
	@Override
	public boolean remove(Object obj) {
		if (obj instanceof Statement) {
		 Statement stmt =	(Statement)obj;
		 if (stmt.getStatement().contains("}"))
			 return false;
		}
		return super.remove(obj);
		// TODO Auto-generated method stub
	}
	
	@Override
	public ActionCodes clone() {
		ActionCodes newList = new ActionCodes();
		for (Statement stmt:this) {
			newList.add(stmt);
		}
		return newList;
	}
}
