package ca.queensu.cs.modelverifier.slicing.data;

public enum ObjectType {
	State, Transition, Guard, ActionCode, Trigger, CapsuleAttribute, Part, Port, Connector, ConnectorEnd
}
