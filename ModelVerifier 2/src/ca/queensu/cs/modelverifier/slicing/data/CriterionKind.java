/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

/**
 * @author rezaahmadi
 *
 */
public enum CriterionKind {
	Transition, Variable, Trigger, PropertySM, Port, Part
}
