/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

import java.util.*;
/**
 * @author rezaahmadi
 *
 */
public class Statement {
	private ActionCodes dependencies;
	private List<String> varsUsed;
	private String varAssigned;
	
	/**
	 * @return the verUsed
	 */
	public List<String> getVarUsed() {
		return varsUsed;
	}
	/**
	 * @param verUsed the verUsed to set
	 */
	public void setVerUsed(List<String> varsUsed) {
		this.varsUsed = varsUsed;
	}
	/**
	 * @return the varAssigned
	 */
	public String getVarAssigned() {
		return varAssigned;
	}
	/**
	 * @param varAssigned the varAssigned to set
	 */
	public void setVarAssigned(String varAssigned) {
		this.varAssigned = varAssigned;
	}
	/**
	 * @return the dependencies
	 */
	public ActionCodes getDependencies() {
		if (dependencies == null)
			dependencies = new ActionCodes();
		return dependencies;
	}
	/**
	 * @param dependencies the dependencies to set
	 */
	public void setDependencies(ActionCodes dependencies) {
		this.dependencies = dependencies;
	}
	private boolean marked;
	/**
	 * @return the marked
	 */
	public boolean isMarked() {
		return marked;
	}
	/**
	 * @param marked the marked to set
	 */
	public void setMarked(boolean marked) {
		this.marked = marked;
	}
	private int line;
	private String statement;
	/**
	 * @param order
	 * @param statement
	 * @param used 
	 * @param assigned 
	 */
	public Statement(String statement, int line, boolean marked, List<String> used, String assigned) {
		this.line = line;
		this.statement = statement;
		this.marked = marked;
		 this.varsUsed = used;
		 this.varAssigned = assigned;
	}
	/**
	 * @return the order
	 */
	public int getLine() {
		return line;
	}
	/**
	 * @param order the order to set
	 */
	public void setLine(int line) {
		this.line = line;
	}
	/**
	 * @return the statement
	 */
	public String getStatement() {
		return statement;
	}
	/**
	 * @param statement the statement to set
	 */
	public void setStatement(String statement) {
		this.statement = statement;
	}

}
