/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

import java.util.*;

import org.eclipse.uml2.uml.Transition;

/**
 * @author rezaahmadi
 *
 */
public class Criteria extends ArrayList<Criterion> {
	public List<Transition> getTransitions() {
		List<Transition> res = new ArrayList<Transition>();
		for (Criterion c : this)
			if (c.getKind() == CriterionKind.Transition)
				res.add((Transition)c.getValue());

		return res;
	}
	
	public List<String> getVariables() {
		List<String> res = new ArrayList<String>();
		for (Criterion c : this)
			if (c.getKind() == CriterionKind.Variable)
				res.add((String)c.getValue());

		return res;
	}

	public boolean hasCriterionOfKind(CriterionKind kind) {
		for (Criterion c : this) {
			if (c.getKind() == kind)
				return true;
		}
		return false;
	}

//	public void returnNonEmpties() {
//		for (Criterion c:this)
//			if (c.getValue().equals(null) || c.getValue().equals(""))
//				this.remove(c);
//		
//	}
}
