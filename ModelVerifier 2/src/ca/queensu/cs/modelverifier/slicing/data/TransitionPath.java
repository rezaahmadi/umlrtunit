/**
 * 
 */
package ca.queensu.cs.modelverifier.slicing.data;

import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import java.util.List;
import java.util.ArrayList;


/**
 * @author rezaahmadi
 *
 */
public class TransitionPath {
	
	private State sourceState;
	private State targetState;

	
	/**
	 * @return the sourceState
	 */
	public State getSourceState() {
		return sourceState;
	}

	/**
	 * @param sourceState the sourceState to set
	 */
	public void setSourceState(State sourceState) {
		this.sourceState = sourceState;
	}

	/**
	 * @return the targetState
	 */
	public State getTargetState() {
		return targetState;
	}

	/**
	 * @param targetState the targetState to set
	 */
	public void setTargetState(State targetState) {
		this.targetState = targetState;
	}

	public void print() {
		String pathStr = "";
		for (Transition t : items) {
			pathStr += t.getName() + "-";
		}
		pathStr = pathStr.substring(0, pathStr.length() - 1);
		System.out.println(pathStr);
	}
	
	public void add(Transition t) {
		items.add(t);
	}
	
	public void remove(Transition t) {
		items.remove(t);
	}
	
	List<Transition> items = new ArrayList<Transition>();

	/**
	 * @return the items
	 */
	public List<Transition> getItems() {
		return items;
	}

	public TransitionPath(TransitionPath newObj) {
		items = new ArrayList<Transition>();
		for (Transition t:newObj.getItems()) {
			items.add(t);
		}
	}
	
	public TransitionPath(List<Transition> trans) {
		for (Transition t:trans) {
			items.add(t);
		}
	}
	
	public TransitionPath() {
	}

	public int count(Transition t) {
		int c = 0;
		for (Transition member : items) {
			if (member.equals(t))
				c++;
		}
		return c;
	}

	public  String getPathStr() {
		String path = "";
		for (Transition t: items) {
			path+= t.getName()+ ",";
		}
		return path;
	}
}
