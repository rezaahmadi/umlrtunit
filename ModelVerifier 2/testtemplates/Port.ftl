package ${packageName};

import static org.junit.Assert.*;

import org.junit.Test;

public enum #{portName}Port {
	<#list messages as message>
	${message},
	</#list>
}

