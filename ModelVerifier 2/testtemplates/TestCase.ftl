package ${packageName};

import static org.junit.Assert.*;

import org.junit.Test;

import ca.queensu.cs.modelverifier.smarttesting.testproperties.${propertyName?cap_first};

/**
 * @author reza
 *
 */
public class ${propertyName?cap_first}Test${testNum} {
	@Test
	public void test() {
		${propertyName?cap_first} obj = new ${propertyName?cap_first}();
		<#list messages as m>
		obj.${m}();		
		</#list>
		assert(obj.isSucceed());
	}
}
