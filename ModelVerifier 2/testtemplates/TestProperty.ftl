package ${packageName};

import java.util.*;

/**
 * @author reza
 *
 */
public class ${propertyName} {

	enum State {
		<#list states as state>
		${state},		
		</#list>
	};
	
	State currentState;
	boolean succeed;

	public boolean isSucceed() {
		return succeed;
	}

	private void setSucceed(boolean succeed) {
		this.succeed = succeed;
	}

	public State getCurrentState() {
		return currentState;
	}

	public void setCurrentState(State currentState) {
		this.currentState = currentState;
		System.out.println(String.format("Transite to state [%s]", currentState));
	}

	public ${propertyName}() {
		try {
			setSucceed(true);
			setCurrentState(State.${initialState});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	<#list ports?keys as message>
	public void ${message}() {
		handleMessage(${ports.get(message)}.${message});
	}
	</#list>
	

	private void handleMessage(Enum message) {
		// TODO Auto-generated method stub
		System.out.println(String.format("Signal [%s] sent to Property [Prop1CruiseControl]", message.toString()));

		switch (getCurrentState()) {
			<#list states as state>
			case State.${state}: {
				<#list transitions.getBySourceState(state) as transition>
				if (message == ${transition.getTriggers().get(0).getName()})
					setCurrentState(State.${transition.getTarget().getName()});
				</#list>	
			}break;
			</#list>
		}
	}
}
