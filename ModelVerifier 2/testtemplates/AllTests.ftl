package ca.queensu.cs.modelverifier.smarttesting.testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
<#list tests as test>
${test}.class,
</#list>
})
public class AllTests${propertyName} {

}
