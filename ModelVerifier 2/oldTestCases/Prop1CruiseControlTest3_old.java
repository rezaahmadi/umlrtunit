package ca.queensu.cs.modelverifier.smarttesting.testcases;

import static org.junit.Assert.*;

import org.junit.Test;

import ca.queensu.cs.modelverifier.smarttesting.testproperties.Prop1CruiseControl;

public class Prop1CruiseControlTest3_old {

	@Test
	public void test() {
		Prop1CruiseControl obj = new Prop1CruiseControl();
		obj.engineOff();
		obj.disableControl();
		assert(obj.isSucceed());
	}

}
