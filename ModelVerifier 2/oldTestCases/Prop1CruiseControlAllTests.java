package ca.queensu.cs.modelverifier.smarttesting.testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Prop1CruiseControlTest1_old.class, Prop1CruiseControlTest2_old.class, Prop1CruiseControlTest3_old.class,
		Prop1CruiseControlTest4_old.class })
public class Prop1CruiseControlAllTests {

}
