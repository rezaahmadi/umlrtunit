/**
 * 
 */
package ca.queensu.cs.modelverifier.smarttesting.testcases;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ca.queensu.cs.modelverifier.smarttesting.testproperties.Prop1CruiseControl;

/**
 * @author reza
 *
 */
public class Prop1CruiseControlTest1_old {
	@Test
	public void test() {
		Prop1CruiseControl obj = new Prop1CruiseControl();
		obj.brake();
		obj.resume();
		obj.engineOff();
		obj.disableControl();
		assert(obj.isSucceed());
	}

}
